package com.ptfi.sheinspectionphase2.Models;

import android.app.Activity;

import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by senaardyputra on 1/13/17.
 */

public class MenuItemModel {

    private boolean tokoActive;
    private static boolean informationActive;
    private static boolean historyActive;
    private boolean agentActive;
    private boolean categoryActive;
    private boolean myAccountActive;
    private boolean addProductActive;
    private boolean hasRetailProduct;
    private boolean hasGrosirProduct;
    private boolean checkOutEnable;
    private boolean checkoutEnable;
    private boolean menuProductGrosir;
    private boolean menuProductRetail;
    private boolean menuSalesOrder;
    private boolean menuSalesRetur;
    private boolean menuAddProductGrosir;

    public MenuItemModel() {

    }

    public MenuItemModel(boolean tokoActive, boolean agentActive,
                         boolean categoryActive, boolean myAccountActive,
                         boolean addProductActive, boolean hasRetailProduct,
                         boolean hasGrosirProduct, boolean checkOutEnable,
                         boolean checkoutEnable2, boolean menuProductGrosir,
                         boolean menuProductRetail, boolean menuSalesOrder,
                         boolean menuSalesRetur, boolean menuAddProductGrosir) {
        super();
        this.tokoActive = tokoActive;
        this.agentActive = agentActive;
        this.categoryActive = categoryActive;
        this.myAccountActive = myAccountActive;
        this.addProductActive = addProductActive;
        this.hasRetailProduct = hasRetailProduct;
        this.hasGrosirProduct = hasGrosirProduct;
        this.checkOutEnable = checkOutEnable;
        checkoutEnable = checkoutEnable2;
        this.menuProductGrosir = menuProductGrosir;
        this.menuProductRetail = menuProductRetail;
        this.menuSalesOrder = menuSalesOrder;
        this.menuSalesRetur = menuSalesRetur;
        this.menuAddProductGrosir = menuAddProductGrosir;
    }

    public static boolean isHistoryActive() {
        return historyActive;
    }

    public static void setHistoryActive(boolean historyActive) {
        MenuItemModel.historyActive = historyActive;
    }

    public static boolean isInformationActive() {
        return informationActive;
    }

    public static void setInformationActive(boolean informationActived) {
        informationActive = informationActived;
    }

    public boolean isTokoActive() {
        return tokoActive;
    }

    public void setTokoActive(boolean tokoActive) {
        this.tokoActive = tokoActive;
    }

    public boolean isAgentActive() {
        return agentActive;
    }

    public void setAgentActive(boolean agentActive) {
        this.agentActive = agentActive;
    }

    public boolean isCategoryActive() {
        return categoryActive;
    }

    public void setCategoryActive(boolean categoryActive) {
        this.categoryActive = categoryActive;
    }

    public boolean isMyAccountActive() {
        return myAccountActive;
    }

    public void setMyAccountActive(boolean myAccountActive) {
        this.myAccountActive = myAccountActive;
    }

    public boolean isAddProductActive() {
        return addProductActive;
    }

    public void setAddProductActive(boolean addProductActive) {
        this.addProductActive = addProductActive;
    }

    public boolean isHasRetailProduct() {
        return hasRetailProduct;
    }

    public void setHasRetailProduct(boolean hasRetailProduct) {
        this.hasRetailProduct = hasRetailProduct;
    }

    public boolean isHasGrosirProduct() {
        return hasGrosirProduct;
    }

    public void setHasGrosirProduct(boolean grosirProduct) {
        this.hasGrosirProduct = grosirProduct;
    }

    public boolean isCheckOutEnable() {
        return checkOutEnable;
    }

    public void setCheckOutEnable(boolean checkOutEnable) {
        this.checkOutEnable = checkOutEnable;
    }

    public boolean isCheckoutEnable() {
        return checkoutEnable;
    }

    public void setCheckoutEnable(boolean checkoutEnable) {
        this.checkoutEnable = checkoutEnable;
    }

    public boolean isMenuProductGrosir() {
        return menuProductGrosir;
    }

    public void setMenuProductGrosir(boolean menuProductGrosir) {
        this.menuProductGrosir = menuProductGrosir;
    }

    public boolean isMenuProductRetail() {
        return menuProductRetail;
    }

    public void setMenuProductRetail(boolean menuProductRetail) {
        this.menuProductRetail = menuProductRetail;
    }

    public boolean isMenuSalesOrder() {
        return menuSalesOrder;
    }

    public void setMenuSalesOrder(boolean menuSalesOrder) {
        this.menuSalesOrder = menuSalesOrder;
    }

    public boolean isMenuSalesRetur() {
        return menuSalesRetur;
    }

    public void setMenuSalesRetur(boolean menuSalesRetur) {
        this.menuSalesRetur = menuSalesRetur;
    }

    public boolean isMenuAddProductGrosir() {
        return menuAddProductGrosir;
    }

    public void setMenuAddProductGrosir(boolean menuAddProductGrosir) {
        this.menuAddProductGrosir = menuAddProductGrosir;
    }

    public ArrayList<DrawerItem> populateRootMenuPlanned(Activity context) {
        ArrayList<DrawerItem> list = new ArrayList<DrawerItem>();

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.HOME,
                DrawerItem.HOME, "Home", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_home));

//        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.APPLICATION,
//                DrawerItem.APPLICATION, "Apps", DrawerItem.NO_CHILD,
//                DrawerItem.APPLICATION_CHILD, R.drawable.icon_app2));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.DATA_MANAGEMENT,
                DrawerItem.DATA_MANAGEMENT, "Data Management",
                DrawerItem.NO_PARENT, DrawerItem.DATA_MANAGEMENT_CHILD,
                R.drawable.icon_data));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.REPORT,
                DrawerItem.REPORT, "Report", DrawerItem.NO_PARENT,
                DrawerItem.REPORT_CHILD, R.drawable.icon_report));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.INFORMATION,
                DrawerItem.INFORMATION, "Information", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_info2));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.EXIT,
                DrawerItem.EXIT, "Exit", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_logout));

        return list;
    }

    public ArrayList<DrawerItem> populateRootMenuHistory(Activity context) {
        ArrayList<DrawerItem> list = new ArrayList<DrawerItem>();

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.HOME,
                DrawerItem.HOME, "Home", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_home));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.APPLICATION,
                DrawerItem.APPLICATION, "Apps", DrawerItem.NO_CHILD,
                DrawerItem.APPLICATION_CHILD, R.drawable.icon_app2));

//		list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.DATA_MANAGEMENT,
//				DrawerItem.DATA_MANAGEMENT, "Data Management",
//				DrawerItem.NO_PARENT, DrawerItem.DATA_MANAGEMENT_CHILD,
//				R.drawable.icon_data));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.INFORMATION,
                DrawerItem.INFORMATION, "Information", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_info2));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.EXIT,
                DrawerItem.EXIT, "Exit", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_logout));

        return list;
    }

    public ArrayList<DrawerItem> populateApplication() {
        ArrayList<DrawerItem> list = new ArrayList<DrawerItem>();

        list.add(new DrawerItem(DrawerItem.CHILD,
                DrawerItem.PLANNED_SHE_MEETING, DrawerItem.PLANNED_SHE_MEETING,
                "Planned SHE Meeting", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.CHILD,
                DrawerItem.FIVE_M_SAFETY_TALK, DrawerItem.FIVE_M_SAFETY_TALK,
                "5min Safety Talk", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.CHILD,
                DrawerItem.HISTORY, DrawerItem.HISTORY,
                "History Meeting", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateRootMenu5M() {
        ArrayList<DrawerItem> list = new ArrayList<DrawerItem>();

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.HOME,
                DrawerItem.HOME, "Home", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_home));

//        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.APPLICATION,
//                DrawerItem.APPLICATION, "Apps", DrawerItem.NO_CHILD,
//                DrawerItem.APPLICATION_CHILD, R.drawable.icon_app2));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.DATA_MANAGEMENT,
                DrawerItem.DATA_MANAGEMENT, "Data Management",
                DrawerItem.NO_PARENT, DrawerItem.DATA_MANAGEMENT_CHILD,
                R.drawable.icon_data));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.REPORT,
                DrawerItem.REPORT, "Report", DrawerItem.NO_PARENT,
                DrawerItem.REPORT_CHILD, R.drawable.icon_report));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.INFORMATION,
                DrawerItem.INFORMATION, "Information", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_info2));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.EXIT,
                DrawerItem.EXIT, "Exit", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_logout));

        return list;
    }

    public ArrayList<DrawerItem> populateRootMenuInformation() {
        ArrayList<DrawerItem> list = new ArrayList<DrawerItem>();
        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.HOME,
                DrawerItem.HOME, "Home", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_home));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.DATA_MANAGEMENT,
                DrawerItem.DATA_MANAGEMENT, "Data Management",
                DrawerItem.NO_PARENT, DrawerItem.DATA_MANAGEMENT_CHILD,
                R.drawable.icon_data));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.EXIT,
                DrawerItem.EXIT, "Exit", DrawerItem.NO_PARENT,
                DrawerItem.NO_CHILD, R.drawable.icon_logout));

        return list;

    }

    public HashMap<Integer, ArrayList<DrawerItem>> populateChildMenuPlanned() {
        HashMap<Integer, ArrayList<DrawerItem>> childMenu = new HashMap<Integer, ArrayList<DrawerItem>>();

        childMenu.put(DrawerItem.HOME, new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.PLANNED_SHE_MEETING,
//                new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.FIVE_M_SAFETY_TALK,
//                new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.HISTORY,
//                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.INFORMATION, new ArrayList<DrawerItem>());

        ArrayList<DrawerItem> dataManagement = populateDataManagement();

        childMenu.put(DrawerItem.DATA_MANAGEMENT, dataManagement);
        childMenu.put(DrawerItem.REPORT, populateReport());
//        childMenu.put(DrawerItem.APPLICATION, populateApplication());
        childMenu.put(DrawerItem.EXPORT_LOOKUP, populateExportLookup());
        childMenu.put(DrawerItem.EXIT, new ArrayList<DrawerItem>());

        return childMenu;
    }

    public HashMap<Integer, ArrayList<DrawerItem>> populateChildMenuInformation() {
        HashMap<Integer, ArrayList<DrawerItem>> childMenu = new HashMap<Integer, ArrayList<DrawerItem>>();

        childMenu.put(DrawerItem.HOME, new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.PLANNED_SHE_MEETING,
//                new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.FIVE_M_SAFETY_TALK,
//                new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.HISTORY,
//                new ArrayList<DrawerItem>());
//        childMenu.put(DrawerItem.INFORMATION, new ArrayList<DrawerItem>());

        ArrayList<DrawerItem> dataManagement = populateDataManagementInformation();

        childMenu.put(DrawerItem.DATA_MANAGEMENT, dataManagement);
//        childMenu.put(DrawerItem.REPORT, populateReport());
//        childMenu.put(DrawerItem.APPLICATION, populateApplication());
        childMenu.put(DrawerItem.EXPORT_LOOKUP, populateExportLookup());
        childMenu.put(DrawerItem.EXIT, new ArrayList<DrawerItem>());

        return childMenu;
    }

    public HashMap<Integer, ArrayList<DrawerItem>> populateChildMenu5Min() {
        HashMap<Integer, ArrayList<DrawerItem>> childMenu = new HashMap<Integer, ArrayList<DrawerItem>>();

        childMenu.put(DrawerItem.HOME, new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.PLANNED_SHE_MEETING,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.FIVE_M_SAFETY_TALK,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.HISTORY,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.INFORMATION, new ArrayList<DrawerItem>());

        ArrayList<DrawerItem> dataManagement = populateDataManagement5M();

        childMenu.put(DrawerItem.DATA_MANAGEMENT, dataManagement);
        childMenu.put(DrawerItem.REPORT, populateReport());
        childMenu.put(DrawerItem.APPLICATION, populateApplication());
        childMenu.put(DrawerItem.EXPORT_LOOKUP, populateExportLookup());
        childMenu.put(DrawerItem.EXIT, new ArrayList<DrawerItem>());

        return childMenu;
    }

    public HashMap<Integer, ArrayList<DrawerItem>> populateChildMenuHistory() {
        HashMap<Integer, ArrayList<DrawerItem>> childMenu = new HashMap<Integer, ArrayList<DrawerItem>>();

        childMenu.put(DrawerItem.HOME, new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.PLANNED_SHE_MEETING,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.FIVE_M_SAFETY_TALK,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.HISTORY,
                new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.INFORMATION, new ArrayList<DrawerItem>());

        ArrayList<DrawerItem> dataManagement = populateDataManagementHistory();

        childMenu.put(DrawerItem.DATA_MANAGEMENT, dataManagement);
//        childMenu.put(DrawerItem.APPLICATION, populateApplication());
        childMenu.put(DrawerItem.EXIT, new ArrayList<DrawerItem>());

        return childMenu;
    }

    public HashMap<Integer, ArrayList<DrawerItem>> populateSecondLevelChildMenuPlanned() {
        HashMap<Integer, ArrayList<DrawerItem>> childMenu = new HashMap<Integer, ArrayList<DrawerItem>>();

        childMenu.put(DrawerItem.IMPORT_LOOKUP, populateImportLookup());
        childMenu.put(DrawerItem.CLEAR_DATA, new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.EXPORT_LOOKUP, populateExportLookup());
        childMenu.put(DrawerItem.GENERATE_REPORT, new ArrayList<DrawerItem>());
        childMenu.put(DrawerItem.PREVIEW_REPORT, new ArrayList<DrawerItem>());

        return childMenu;
    }

    public ArrayList<DrawerItem> populateDataManagement() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.DATA_MANAGEMENT, "Back", DrawerItem.ROOT,
        // DrawerItem.NO_CHILD, null, null));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.SAVE_DATA,
                DrawerItem.SAVE_DATA, "Save Data",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.CLEAR_DATA,
                DrawerItem.CLEAR_DATA, "New Inspection",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.IMPORT_LOOKUP,
                DrawerItem.IMPORT_LOOKUP, "Import Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.IMPORT_LOOKUP_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.EXPORT_LOOKUP,
                DrawerItem.EXPORT_LOOKUP, "Export Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.EXPORT_LOOKUP_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateDataManagementInformation() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.DATA_MANAGEMENT, "Back", DrawerItem.ROOT,
        // DrawerItem.NO_CHILD, null, null));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.IMPORT_LOOKUP,
                DrawerItem.IMPORT_LOOKUP, "Import Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.IMPORT_LOOKUP_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.EXPORT_LOOKUP,
                DrawerItem.EXPORT_LOOKUP, "Export Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.EXPORT_LOOKUP_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateDataManagement5Minutes() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.DATA_MANAGEMENT, "Back", DrawerItem.ROOT,
        // DrawerItem.NO_CHILD, null, null));

//        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.CLEAR_DATA,
//                DrawerItem.CLEAR_DATA, "New 5 Minutes Safety Talk",
//                DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.IMPORT_LOOKUP,
                DrawerItem.IMPORT_LOOKUP, "Import Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.IMPORT_LOOKUP_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.EXPORT_LOOKUP,
                DrawerItem.EXPORT_LOOKUP, "Export Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.EXPORT_LOOKUP_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateDataManagement5M() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.DATA_MANAGEMENT, "Back", DrawerItem.ROOT,
        // DrawerItem.NO_CHILD, null, null));

        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.CLEAR_DATA,
                DrawerItem.CLEAR_DATA, "New 5 Minutes Safety Talk",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

//		list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.EXPORT_DATA,
//				DrawerItem.EXPORT_DATA, "Export Data",
//				DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.IMPORT_LOOKUP,
                DrawerItem.IMPORT_LOOKUP, "Import Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.IMPORT_LOOKUP_CHILD));

        list.add(new DrawerItem(DrawerItem.PARENT, DrawerItem.EXPORT_LOOKUP,
                DrawerItem.EXPORT_LOOKUP, "Export Lookup",
                DrawerItem.DATA_MANAGEMENT, DrawerItem.EXPORT_LOOKUP_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateDataManagementHistory() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();

//		list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.CLEAR_ALL_HISTORY,
//				DrawerItem.CLEAR_ALL_HISTORY, "Clear All History",
//				DrawerItem.DATA_MANAGEMENT, DrawerItem.NO_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateReport() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON, DrawerItem.REPORT,
        // "Back", DrawerItem.ROOT, DrawerItem.NO_CHILD, null, null));
//        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.PREVIEW_REPORT,
//                DrawerItem.PREVIEW_REPORT, "Preview Report", DrawerItem.REPORT,
//                DrawerItem.NO_CHILD));
        list.add(new DrawerItem(DrawerItem.CHILD, DrawerItem.GENERATE_REPORT,
                DrawerItem.GENERATE_REPORT, "Generate Report",
                DrawerItem.REPORT, DrawerItem.NO_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateImportLookup() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.IMPORT_LOOKUP, "Back", DrawerItem.DATA_MANAGEMENT,
        // DrawerItem.NO_CHILD, null, null));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.IMPORT_LOOKUP_FRESH, DrawerItem.IMPORT_LOOKUP_FRESH,
//                "Fresh", DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.IMPORT_LOOKUP_DEPARTMENT,
//				DrawerItem.IMPORT_LOOKUP_DEPARTMENT, "Department",
//				DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.IMPORT_LOOKUP_SECTION,
//				DrawerItem.IMPORT_LOOKUP_SECTION, "Section",
//				DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.IMPORT_LOOKUP_BUSSINESS_UNIT,
//				DrawerItem.IMPORT_LOOKUP_BUSSINESS_UNIT, "Business Unit",
//				DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.IMPORT_LOOKUP_COMPANY,
//				DrawerItem.IMPORT_LOOKUP_COMPANY, "Company",
//				DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.IMPORT_LOOKUP_MANPOWER,
//                DrawerItem.IMPORT_LOOKUP_MANPOWER, "ManPower",
//                DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
        list.add(new DrawerItem(DrawerItem.CHILD,
                DrawerItem.IMPORT_LOOKUP_MANPOWER,
                DrawerItem.IMPORT_LOOKUP_MANPOWER, "Manpower",
                DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.IMPORT_LOOKUP_DATA,
//                DrawerItem.IMPORT_LOOKUP_DATA, "Look Up",
//                DrawerItem.IMPORT_LOOKUP, DrawerItem.NO_CHILD));

        return list;
    }

    public ArrayList<DrawerItem> populateExportLookup() {
        ArrayList<DrawerItem> list = null;

        list = new ArrayList<DrawerItem>();
        // list.add(new DrawerItem(DrawerItem.BACK_BUTTON,
        // DrawerItem.EXPORT_LOOKUP, "Back", DrawerItem.DATA_MANAGEMENT,
        // DrawerItem.NO_CHILD, null, null));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.EXPORT_LOOKUP_FRESH, DrawerItem.EXPORT_LOOKUP_FRESH,
//                "Fresh", DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.EXPORT_LOOKUP_DEPARTMENT,
//				DrawerItem.EXPORT_LOOKUP_DEPARTMENT, "Department",
//				DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.EXPORT_LOOKUP_SECTION,
//				DrawerItem.EXPORT_LOOKUP_SECTION, "Section",
//				DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.EXPORT_LOOKUP_BUSSINESS_UNIT,
//				DrawerItem.EXPORT_LOOKUP_BUSSINESS_UNIT, "Business Unit",
//				DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//		list.add(new DrawerItem(DrawerItem.CHILD,
//				DrawerItem.EXPORT_LOOKUP_COMPANY,
//				DrawerItem.EXPORT_LOOKUP_COMPANY, "Company",
//				DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.EXPORT_LOOKUP_MANPOWER,
//                DrawerItem.EXPORT_LOOKUP_MANPOWER, "ManPower",
//                DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
        list.add(new DrawerItem(DrawerItem.CHILD,
                DrawerItem.EXPORT_LOOKUP_MANPOWER,
                DrawerItem.EXPORT_LOOKUP_MANPOWER, "Manpower",
                DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));
//        list.add(new DrawerItem(DrawerItem.CHILD,
//                DrawerItem.EXPORT_LOOKUP_DATA,
//                DrawerItem.EXPORT_LOOKUP_DATA, "Look Up",
//                DrawerItem.EXPORT_LOOKUP, DrawerItem.NO_CHILD));

        return list;
    }

}
