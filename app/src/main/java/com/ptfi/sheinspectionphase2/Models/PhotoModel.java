package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class PhotoModel {

    private long id = -1;
    private long reference = -1;
    private int inspectionCode = -1;
    private String fotoPath = "";
    private String fotoImage = "";
    private String comment = "";
    private String date = "";
    private String pic = "";
    private String projectOwner = "";

    public PhotoModel(){

    }

    public PhotoModel(int id, String fotoPath, String fotoImage, String date, String pic, String projectOwner) {
        super();
        this.id = id;
        this.fotoPath = fotoPath;
        this.fotoImage = fotoImage;
        this.date = date;
        this.pic = pic;
        this.projectOwner = projectOwner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFotoPath() {
        return fotoPath;
    }

    public void setFotoPath(String fotoPath) {
        this.fotoPath = fotoPath;
    }

    public String getFotoImage() {
        return fotoImage;
    }

    public void setFotoImage(String fotoImage) {
        this.fotoImage = fotoImage;
    }

    public long getPsmId() {
        return reference;
    }

    public void setPsmId(long psmId) {
        this.reference = psmId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getProjectOwner() {
        return projectOwner;
    }

    public void setProjectOwner(String projectOwner) {
        this.projectOwner = projectOwner;
    }

    public String getDataByIndex(int index){

		 /* 0.Id
		 * 1.fotoPath
		 * 2.comment
		 * 3.fotoImage
		 * 4.reference
		 *
		 */
        String result = "";
        switch (index) {
            case 0:
                if((int)this.id==0)
                    result = "";
                else
                    result = String.valueOf((int)this.id);
                break;
            case 1:
                result = this.fotoPath;
                break;
            case 2:
                result = this.comment;
                break;
            case 3:
                result = this.fotoImage;
                break;
            case 4:
                result = String.valueOf((int)this.reference);
                break;
            default:
                break;
        }
        return result;
    }
}
