package com.ptfi.sheinspectionphase2.Models;

public class CustomSpinnerItem {
	public String lookup;
	public String description;
	public String ref;

	public CustomSpinnerItem() {
		lookup = "";
		description = "";
		ref = "";
	}

	public CustomSpinnerItem(String lookup, String description) {
		this.description = description;
		this.lookup = lookup;
		this.ref = "";
	}

	public CustomSpinnerItem(String lookup, String description, String ref) {
		this.description = description;
		this.lookup = lookup;
		this.ref = ref;
	}

	@Override
	public String toString() {

		return lookup;

	}
}
