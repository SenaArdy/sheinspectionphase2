package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/16/16.
 */

public class LVDailyModel {

    public long id = -1;
    public String lvNo = "";
    public String pic = "";
    public String date = "";
    public String time = "";
    public String insp1 = "";
    public String insp2 = "";
    public String insp3 = "";
    public String insp4 = "";
    public String insp5 = "";
    public String insp6 = "";
    public String insp7 = "";
    public String insp8 = "";
    public String insp9 = "";
    public String insp10 = "";
    public String insp11 = "";
    public String insp12 = "";
    public String insp13 = "";
    public String insp14 = "";
    public String insp15 = "";
    public String insp16 = "";
    public String driverId = "";
    public String signature = "";
    public String dateIns = "";

    public LVDailyModel() {

    }

    public LVDailyModel(long id, String lvNo, String pic, String date, String time, String insp1, String insp2, String insp3, String insp4, String insp5, String insp6,
                        String insp7, String insp8, String insp9, String insp10, String insp11, String insp12, String insp13, String insp14, String insp15, String insp16,
                        String driverId, String signature, String dateIns) {
        this.id = id;
        this.lvNo = lvNo;
        this.pic = pic;
        this.date = date;
        this.time = time;
        this.insp1 = insp1;
        this.insp2 = insp2;
        this.insp3 = insp3;
        this.insp4 = insp4;
        this.insp5 = insp5;
        this.insp6 = insp6;
        this.insp7 = insp7;
        this.insp8 = insp8;
        this.insp9 = insp9;
        this.insp10 = insp10;
        this.insp11 = insp11;
        this.insp12 = insp12;
        this.insp13 = insp13;
        this.insp14 = insp14;
        this.insp15 = insp15;
        this.insp16 = insp16;
        this.driverId = driverId;
        this.signature = signature;
        this.dateIns = dateIns;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLvNo() {
        return lvNo;
    }

    public void setLvNo(String lvNo) {
        this.lvNo = lvNo;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getInsp1() {
        return insp1;
    }

    public void setInsp1(String insp1) {
        this.insp1 = insp1;
    }

    public String getInsp2() {
        return insp2;
    }

    public void setInsp2(String insp2) {
        this.insp2 = insp2;
    }

    public String getInsp3() {
        return insp3;
    }

    public void setInsp3(String insp3) {
        this.insp3 = insp3;
    }

    public String getInsp4() {
        return insp4;
    }

    public void setInsp4(String insp4) {
        this.insp4 = insp4;
    }

    public String getInsp5() {
        return insp5;
    }

    public void setInsp5(String insp5) {
        this.insp5 = insp5;
    }

    public String getInsp6() {
        return insp6;
    }

    public void setInsp6(String insp6) {
        this.insp6 = insp6;
    }

    public String getInsp7() {
        return insp7;
    }

    public void setInsp7(String insp7) {
        this.insp7 = insp7;
    }

    public String getInsp8() {
        return insp8;
    }

    public void setInsp8(String insp8) {
        this.insp8 = insp8;
    }

    public String getInsp9() {
        return insp9;
    }

    public void setInsp9(String insp9) {
        this.insp9 = insp9;
    }

    public String getInsp10() {
        return insp10;
    }

    public void setInsp10(String insp10) {
        this.insp10 = insp10;
    }

    public String getInsp11() {
        return insp11;
    }

    public void setInsp11(String insp11) {
        this.insp11 = insp11;
    }

    public String getInsp12() {
        return insp12;
    }

    public void setInsp12(String insp12) {
        this.insp12 = insp12;
    }

    public String getInsp13() {
        return insp13;
    }

    public void setInsp13(String insp13) {
        this.insp13 = insp13;
    }

    public String getInsp14() {
        return insp14;
    }

    public void setInsp14(String insp14) {
        this.insp14 = insp14;
    }

    public String getInsp15() {
        return insp15;
    }

    public void setInsp15(String insp15) {
        this.insp15 = insp15;
    }

    public String getInsp16() {
        return insp16;
    }

    public void setInsp16(String insp16) {
        this.insp16 = insp16;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }
}
