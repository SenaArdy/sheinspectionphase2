package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 1/13/17.
 */

public class DrawerItem {

    public static final int GENDER_SELECTOR = 1;
    public static final int BACK_BUTTON = 2;
    public static final int PARENT = 3;
    public static final int CHILD = 4;

    public static final int NO_CHILD = -1;
    public static final int NO_PARENT = -1;

    public static final int ROOT = 0;
    public static final int HOME = 1;
    public static final int EXIT = 2;
    public static final int DATA_MANAGEMENT = 3;
    public static final int DATA_MANAGEMENT_CHILD = 4;
    public static final int CLEAR_DATA = 5;
    public static final int REPORT = 6;
    public static final int REPORT_CHILD = 7;
    public static final int PREVIEW_REPORT = 8;
    public static final int GENERATE_REPORT = 9;
    public static final int PLANNED_SHE_MEETING = 10;
    public static final int FIVE_M_SAFETY_TALK = 11;
    public static final int IMPORT_LOOKUP = 12;
    public static final int IMPORT_LOOKUP_CHILD = 13;
    public static final int EXPORT_LOOKUP = 14;
    public static final int EXPORT_LOOKUP_CHILD = 15;
    public static final int IMPORT_LOOKUP_FRESH = 16;
    public static final int EXPORT_LOOKUP_FRESH = 17;
    public static final int INFORMATION = 18;
    public static final int IMPORT_LOOKUP_DATA = 19;
    public static final int EXPORT_LOOKUP_DATA = 20;
    public static final int IMPORT_LOOKUP_SECTION = 21;
    public static final int EXPORT_LOOKUP_SECTION = 22;
    public static final int IMPORT_LOOKUP_BUSSINESS_UNIT = 23;
    public static final int EXPORT_LOOKUP_BUSSINESS_UNIT = 24;
    public static final int IMPORT_LOOKUP_COMPANY = 25;
    public static final int EXPORT_LOOKUP_COMPANY = 26;
    public static final int IMPORT_LOOKUP_MANPOWER = 27;
    public static final int EXPORT_LOOKUP_MANPOWER = 28;
    public static final int EXPORT_CSV_PLANNED = 29;
    public static final int APPLICATION = 29;
    public static final int APPLICATION_CHILD = 30;
    public static final int HISTORY = 30;
    public static final int CLEAR_ALL_HISTORY = 30;
    public static final int EXPORT_DATA = 31;
//    public static final int IMPORT_LOOKUP_SOP = 32;
    public static final int EXPORT_LOOKUP_SOP = 33;
    public static final int IMPORT_LOOKUP_JSA = 34;
    public static final int EXPORT_LOOKUP_JSA = 35;
    public static final int SAVE_DATA = 31;

    private int type;
    private int ancestor;
    private int menu;
    private String title;
    private int parent;
    private int child;
    private int imgResource;

    public DrawerItem() {
        type = CHILD;
        parent = NO_PARENT;
        child = NO_CHILD;
        imgResource = 0;
    }

    /**
     * @param type
     * @param ancestor
     * @param menu
     * @param title
     * @param parent
     * @param child
     */
    public DrawerItem(int type, int ancestor, int menu, String title,
                      int parent, int child) {
        super();
        this.type = type;
        this.ancestor = ancestor;
        this.menu = menu;
        this.title = title;
        this.parent = parent;
        this.child = child;
        this.imgResource = 0;
    }

    /**
     * @param type
     * @param ancestor
     * @param menu
     * @param title
     * @param parent
     * @param child
     * @param imgResource
     */
    public DrawerItem(int type, int ancestor, int menu, String title,
                      int parent, int child, int imgResource) {
        super();
        this.type = type;
        this.ancestor = ancestor;
        this.menu = menu;
        this.title = title;
        this.parent = parent;
        this.child = child;
        this.imgResource = imgResource;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public int getMenu() {
        return menu;
    }

    public void setMenu(int menu) {
        this.menu = menu;
    }

    public int getAncestor() {
        return ancestor;
    }

    public void setAncestor(int ancestor) {
        this.ancestor = ancestor;
    }
}
