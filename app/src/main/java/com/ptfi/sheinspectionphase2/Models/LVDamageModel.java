package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/16/16.
 */

public class LVDamageModel {

    public long id = -1;
    public String lvNo = "";
    public String pic = "";
    public String itemDamage = "";
    public String date = "";
    public String driverId = "";
    public String action = "";
    public String dateIns = "";

    public LVDamageModel() {

    }

    public LVDamageModel(long id, String lvNo, String pic, String itemDamage, String date, String driverId, String action, String dateIns) {
        this.id = id;
        this.lvNo = lvNo;
        this.pic = pic;
        this.itemDamage = itemDamage;
        this.date = date;
        this.driverId = driverId;
        this.action = action;
        this.dateIns = dateIns;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLvNo() {
        return lvNo;
    }

    public void setLvNo(String lvNo) {
        this.lvNo = lvNo;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getItemDamage() {
        return itemDamage;
    }

    public void setItemDamage(String itemDamage) {
        this.itemDamage = itemDamage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }
}
