package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class LVInspectionModel {

    public long id = -1;
    public String lvNo = "";
    public String pic = "";
    public String idNo = "";
    public String department = "";
    public String date = "";
    public String nextPM = "";

    public LVInspectionModel() {

    }

    public LVInspectionModel(long id, String lvNo, String pic, String idNo, String department, String date, String nextPM) {
        this.id = id;
        this.lvNo = lvNo;
        this.pic = pic;
        this.idNo = idNo;
        this.department = department;
        this.date = date;
        this.nextPM = nextPM;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLvNo() {
        return lvNo;
    }

    public void setLvNo(String lvNo) {
        this.lvNo = lvNo;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNextPM() {
        return nextPM;
    }

    public void setNextPM(String nextPM) {
        this.nextPM = nextPM;
    }
}
