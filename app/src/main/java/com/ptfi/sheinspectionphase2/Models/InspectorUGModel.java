package com.ptfi.sheinspectionphase2.Models;

import android.graphics.Bitmap;

/**
 * Created by senaardyputra on 11/23/16.
 */

public class InspectorUGModel {

    public long id  = -1;
    public String name = "";
    public String comment = "";
    public Bitmap sign = null;
    public String dept = "";
    public String idMan = "";
    public String projOwner = "";
    public String projName = "";
    public String dateIns = "";

    public InspectorUGModel() {

    }


    public InspectorUGModel(long id, String name, String comment, Bitmap sign, String dept, String idMan, String projOwner, String projName, String dateIns) {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.sign = sign;
        this.dept = dept;
        this.idMan = idMan;
        this.projOwner = projOwner;
        this.projName = projName;
        this.dateIns = dateIns;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Bitmap getSign() {
        return sign;
    }

    public void setSign(Bitmap sign) {
        this.sign = sign;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getIdMan() {
        return idMan;
    }

    public void setIdMan(String idMan) {
        this.idMan = idMan;
    }

    public String getProjOwner() {
        return projOwner;
    }

    public void setProjOwner(String projOwner) {
        this.projOwner = projOwner;
    }

    public String getProjName() {
        return projName;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }
}
