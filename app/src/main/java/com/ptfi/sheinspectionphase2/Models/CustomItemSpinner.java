package com.ptfi.sheinspectionphase2.Models;


public class CustomItemSpinner {
	public String lookup;
	
	public CustomItemSpinner(){
		lookup = "";
	}
	
	public CustomItemSpinner(String lookup){
		this.lookup = lookup;
	}

	@Override
	public String toString() {
		return lookup;
	}
}
