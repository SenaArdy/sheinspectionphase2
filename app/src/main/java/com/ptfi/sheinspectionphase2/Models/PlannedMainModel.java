package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/24/16.
 */

public class PlannedMainModel {

    public long id = -1;
    public String date = "";
    public String inspector = "";
    public String inspectorID = "";
    public String location = "";
    public String areaOwner = "";

    public PlannedMainModel() {

    }

    public PlannedMainModel(long id, String date, String inspector, String inspectorID, String location, String areaOwner) {
        this.id = id;
        this.date = date;
        this.inspector = inspector;
        this.inspectorID = inspectorID;
        this.location = location;
        this.areaOwner = areaOwner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getInspectorID() {
        return inspectorID;
    }

    public void setInspectorID(String inspectorID) {
        this.inspectorID = inspectorID;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAreaOwner() {
        return areaOwner;
    }

    public void setAreaOwner(String areaOwner) {
        this.areaOwner = areaOwner;
    }
}
