package com.ptfi.sheinspectionphase2.Models;

import android.app.Activity;
import android.graphics.Bitmap;

import com.ptfi.sheinspectionphase2.Database.DataHelper;
import com.ptfi.sheinspectionphase2.Database.DataSource;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class ManpowerModel {

    private long id;
    private String idno;
    private String name;
    private String title;
    private String org;
    private Bitmap sign;
    public String company;
    public String divisi;
    public String department;
    public String section;

    public ManpowerModel() {
        this.id = 0;
        this.idno = "";
        this.name = "";
        this.title = "";
        this.org = "";
        this.sign = null;
        this.company = "";
        this.divisi = "";
        this.department = "";
        this.section = "";
    }

//    public ManpowerModel(long id, String idno, String name, String title, String org, Bitmap sign) {
//        this.id = id;
//        this.idno = idno;
//        this.name = name;
//        this.title = title;
//        this.org = org;
//        this.sign = sign;
//    }

    public ManpowerModel(long id, String obsId, String obsName) {
        super();
        this.id = id;
        this.idno = obsId;
        this.name = obsName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public Bitmap getSign() {
        return sign;
    }

    public void setSign(Bitmap sign) {
        this.sign = sign;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getFormattedString() {
        String res = "";
        res += name + " (" + id + ")";
        return res;
    }

    public static ArrayList<String> getManpowerName(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerName();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerId(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreId();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerOrg(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreOrg();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerCompany(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreCompany();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerDivisi(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreDivisi();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerDept(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreDept();

        ds.close();
        return  results;
    }

    public static ArrayList<String> getManpowerSection(Activity activity) {
        ArrayList<String> results = new ArrayList<String>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getManpowerMoreSection();

        ds.close();
        return  results;
    }

    public static ArrayList<ManpowerModel> getAllManpowerData(Activity activity) {
        ArrayList<ManpowerModel> results = new ArrayList<ManpowerModel>();
        DataSource ds = new DataSource(activity);
        ds.open();

        results = ds.getAllManpowerData();

        ds.close();
        return  results;
    }

    public boolean isContentOfTableExist(Activity activity) {
        boolean isExist = false;
        DataSource ds = new DataSource(activity);
        ds.open();
        isExist = ds.isContentOfTableExist(DataHelper.TABLE_OBSERVER);
        ds.close();
        return isExist;

    }
}
