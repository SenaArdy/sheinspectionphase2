package com.ptfi.sheinspectionphase2.Models;

import android.app.Activity;

import com.ptfi.sheinspectionphase2.Database.DataSource;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class LookUpModel {

    private long id;
    private String potential_risk;
    private String rating;

    public LookUpModel() {
        this.id = -1;
        this.potential_risk = "";
        this.rating = "";
    }

    public LookUpModel(long id, String potential_risk, String rating) {
        this.id = id;
        this.potential_risk = potential_risk;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPotential_risk() {
        return potential_risk;
    }

    public void setPotential_risk(String potential_risk) {
        this.potential_risk = potential_risk;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public static ArrayList<String> getPotentialRisk (Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        ArrayList<String> data = ds.getPotentialRisk();

        ds.close();
        return data;
    }

    public static  ArrayList<String> getRating (Activity mActivity) {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        ArrayList<String> data = ds.getRating();

        ds.close();
        return data;
    }
}
