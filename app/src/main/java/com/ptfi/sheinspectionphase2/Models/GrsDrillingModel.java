package com.ptfi.sheinspectionphase2.Models;

import android.graphics.Bitmap;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class GrsDrillingModel {

    public long gsId;
    public String idInspection;
    public String picName;
    public String picId;
    public String dateInspection;
    public String department;
    public String comment;
    public String date;
    public String signer;
    public String sign;
    public Bitmap signature;

    public GrsDrillingModel() {
        this.gsId = 0;
        this.idInspection = "";
        this.picName = "";
        this.picId = "";
        this.dateInspection = "";
        this.department = "";
        this.comment = "";
        this.date = "";
        this.signer = "";
        this.sign = "";
        this.signature = null;
    }

    public GrsDrillingModel(long gsId, String idInspection, String picName,
                            String picId, String dateInspection, String department,
                            String comment, String date, String signer, String sign,
                            Bitmap signature) {
        this.gsId = gsId;
        this.idInspection = idInspection;
        this.picName = picName;
        this.picId = picId;
        this.dateInspection = dateInspection;
        this.department = department;
        this.comment = comment;
        this.date = date;
        this.signer = signer;
        this.sign = sign;
        this.signature = signature;
    }

    public long getGsId() {
        return gsId;
    }

    public void setGsId(long gsId) {
        this.gsId = gsId;
    }

    public String getIdInspection() {
        return idInspection;
    }

    public void setIdInspection(String idInspection) {
        this.idInspection = idInspection;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName;
    }

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public String getDateInspection() {
        return dateInspection;
    }

    public void setDateInspection(String dateInspection) {
        this.dateInspection = dateInspection;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSigner() {
        return signer;
    }

    public void setSigner(String signer) {
        this.signer = signer;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Bitmap getSignature() {
        return signature;
    }

    public void setSignature(Bitmap signature) {
        this.signature = signature;
    }
}
