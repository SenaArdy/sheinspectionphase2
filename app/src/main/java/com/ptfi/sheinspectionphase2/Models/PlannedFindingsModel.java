package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/24/16.
 */

public class PlannedFindingsModel {

    public long id = -1;
    public long plannedMainId = -1;
    public String inspector ="";
    public String areaOwner= "";
    public String location = "";
    public String date = "";
    public String findings = "";
    public String comment = "";
    public String dateIns = "";
    public String responsible = "";
    public String risk = "";
    public String photoPath = "";
    public int code = -1;
    public int questionCode = -1;
    public int inspectionCode = -1;
    public long reference = -1;

public PlannedFindingsModel(){}

    public PlannedFindingsModel(long id,long plannedMainId, String inspector, String areaOwner, String location, String date, String findings, String comment, String dateIns, String responsible, String risk, String photoPath,
                                long reference, int questionCode, int inspectionCode, int code) {
        this.id = id;
        this.plannedMainId = plannedMainId;
        this.inspector = inspector;
        this.areaOwner = areaOwner;
        this.location = location;
        this.date = date;
        this.findings = findings;
        this.comment = comment;
        this.dateIns = dateIns;
        this.responsible = responsible;
        this.risk = risk;
        this.photoPath = photoPath;
        this.reference = reference;
        this.code = code;
        this.questionCode = questionCode;
        this.inspectionCode = inspectionCode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPlannedMainId() {
        return plannedMainId;
    }

    public void setPlannedMainId(long plannedMainId) {
        this.plannedMainId = plannedMainId;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getAreaOwner() {
        return areaOwner;
    }

    public void setAreaOwner(String areaOwner) {
        this.areaOwner = areaOwner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public long getReference() {
        return reference;
    }

    public void setReference(long reference) {
        this.reference = reference;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(int questionCode) {
        this.questionCode = questionCode;
    }

    public int getInspectionCode() {
        return inspectionCode;
    }

    public void setInspectionCode(int inspectionCode) {
        this.inspectionCode = inspectionCode;
    }
}
