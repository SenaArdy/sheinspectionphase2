package com.ptfi.sheinspectionphase2.Models;

import android.graphics.Bitmap;

/**
 * Created by senaardyputra on 11/24/16.
 */

public class PlannedSignModel {

    public long id = -1;
    public String inspector ="";
    public String areaOwner= "";
    public String location = "";
    public String dateIns = "";
    public String name = "";
    public String idNo = "";
    public String date = "";
    public Bitmap sign = null;
    public String title = "";
    public String code = "";

    public PlannedSignModel() {

    }

    public PlannedSignModel(long id, String inspector, String areaOwner, String location, String dateIns, String name, String idNo, String date, Bitmap sign, String title, String code) {
        this.id = id;
        this.inspector = inspector;
        this.areaOwner = areaOwner;
        this.location = location;
        this.dateIns = dateIns;
        this.name = name;
        this.idNo = idNo;
        this.date = date;
        this.sign = sign;
        this.title = title;
        this.code = code;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getAreaOwner() {
        return areaOwner;
    }

    public void setAreaOwner(String areaOwner) {
        this.areaOwner = areaOwner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Bitmap getSign() {
        return sign;
    }

    public void setSign(Bitmap sign) {
        this.sign = sign;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
