package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class UGDrillingMainModel {

    public long id = -1;
    public String dateIns = "";
    public String projectName = "";
    public String holeId = "";
    public String projectOwner = "";
    public String areaOwner = "";
    public String drillingCoor = "";
    public String drillingCont = "";
    public String ugGeoTech = "";
    public String ugGeo = "";
    public String ugHydro = "";
    public String ugShe = "";
    public String ugVentilation = "";
    public String typeGD = "";
    public String idGD = "";
    public String lastCalibGD = "";

    public UGDrillingMainModel() {

    }

    public UGDrillingMainModel(long id, String dateIns, String projectName, String holeId, String projectOwner, String areaOwner,
                               String drillingCoor, String drillingCont, String ugGeoTech, String ugGeo, String ugHydro, String ugShe, String ugVentilation,
                               String typeGD, String idGD, String lastCalibGD) {
        this.id = id;
        this.dateIns = dateIns;
        this.projectName = projectName;
        this.holeId = holeId;
        this.projectOwner = projectOwner;
        this.areaOwner = areaOwner;
        this.drillingCoor = drillingCoor;
        this.drillingCont = drillingCont;
        this.ugGeoTech = ugGeoTech;
        this.ugGeo = ugGeo;
        this.ugHydro = ugHydro;
        this.ugShe = ugShe;
        this.ugVentilation = ugVentilation;
        this.typeGD = typeGD;
        this.idGD = idGD;
        this.lastCalibGD = lastCalibGD;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getHoleId() {
        return holeId;
    }

    public void setHoleId(String holeId) {
        this.holeId = holeId;
    }

    public String getProjectOwner() {
        return projectOwner;
    }

    public void setProjectOwner(String projectOwner) {
        this.projectOwner = projectOwner;
    }

    public String getAreaOwner() {
        return areaOwner;
    }

    public void setAreaOwner(String areaOwner) {
        this.areaOwner = areaOwner;
    }

    public String getDrillingCoor() {
        return drillingCoor;
    }

    public void setDrillingCoor(String drillingCoor) {
        this.drillingCoor = drillingCoor;
    }

    public String getDrillingCont() {
        return drillingCont;
    }

    public void setDrillingCont(String drillingCont) {
        this.drillingCont = drillingCont;
    }

    public String getUgGeoTech() {
        return ugGeoTech;
    }

    public void setUgGeoTech(String ugGeoTech) {
        this.ugGeoTech = ugGeoTech;
    }

    public String getUgGeo() {
        return ugGeo;
    }

    public void setUgGeo(String ugGeo) {
        this.ugGeo = ugGeo;
    }

    public String getUgHydro() {
        return ugHydro;
    }

    public void setUgHydro(String ugHydro) {
        this.ugHydro = ugHydro;
    }

    public String getUgShe() {
        return ugShe;
    }

    public void setUgShe(String ugShe) {
        this.ugShe = ugShe;
    }

    public String getUgVentilation() {
        return ugVentilation;
    }

    public void setUgVentilation(String ugVentilation) {
        this.ugVentilation = ugVentilation;
    }

    public String getTypeGD() {
        return typeGD;
    }

    public void setTypeGD(String typeGD) {
        this.typeGD = typeGD;
    }

    public String getIdGD() {
        return idGD;
    }

    public void setIdGD(String idGD) {
        this.idGD = idGD;
    }

    public String getLastCalibGD() {
        return lastCalibGD;
    }

    public void setLastCalibGD(String lastCalibGD) {
        this.lastCalibGD = lastCalibGD;
    }
}
