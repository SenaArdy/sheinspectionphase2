package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/16/16.
 */

public class LVWeeklyModel {

    public long id = -1;
    public String lvNo = "";
    public String pic = "";
    public String date = "";
    public String time = "";
    public String insp1 = "";
    public String insp2 = "";
    public String insp3 = "";
    public String insp4 = "";
    public String driverId = "";
    public String signature = "";
    public String superVisor = "";
    public String signature1 = "";
    public String dateIns = "";

    public LVWeeklyModel() {

    }

    public LVWeeklyModel(long id, String lvNo, String pic, String date, String time, String insp1, String insp2, String insp3, String insp4, String driverId,
                         String signature, String superVisor, String signature1, String dateIns) {
        this.id = id;
        this.lvNo = lvNo;
        this.pic = pic;
        this.date = date;
        this.time = time;
        this.insp1 = insp1;
        this.insp2 = insp2;
        this.insp3 = insp3;
        this.insp4 = insp4;
        this.driverId = driverId;
        this.signature = signature;
        this.superVisor = superVisor;
        this.signature1 = signature1;
        this.dateIns = dateIns;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLvNo() {
        return lvNo;
    }

    public void setLvNo(String lvNo) {
        this.lvNo = lvNo;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getInsp1() {
        return insp1;
    }

    public void setInsp1(String insp1) {
        this.insp1 = insp1;
    }

    public String getInsp2() {
        return insp2;
    }

    public void setInsp2(String insp2) {
        this.insp2 = insp2;
    }

    public String getInsp3() {
        return insp3;
    }

    public void setInsp3(String insp3) {
        this.insp3 = insp3;
    }

    public String getInsp4() {
        return insp4;
    }

    public void setInsp4(String insp4) {
        this.insp4 = insp4;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSuperVisor() {
        return superVisor;
    }

    public void setSuperVisor(String superVisor) {
        this.superVisor = superVisor;
    }

    public String getSignature1() {
        return signature1;
    }

    public void setSignature1(String signature1) {
        this.signature1 = signature1;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }
}
