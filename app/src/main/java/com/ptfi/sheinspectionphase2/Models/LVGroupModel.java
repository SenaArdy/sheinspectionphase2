package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class LVGroupModel {

    public long id = -1;
    public String inspector = "";
    public String department = "";
    public String date = "";

    public LVGroupModel() {

    }

    public LVGroupModel(long id, String inspector, String department, String date) {
        this.id = id;
        this.inspector = inspector;
        this.department = department;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
