package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/12/16.
 */

public class FieldMainModel {

    public long id = -1;
    public String company = "";
    public String business = "";
    public String department = "";
    public String section = "";
    public String date = "";
    public String inspector = "";
    public String workArea = "";

    public FieldMainModel() {

    }

    public FieldMainModel(long id, String company, String business, String department, String section, String date, String inspector, String workArea) {
        this.id = id;
        this.company = company;
        this.business = business;
        this.department = department;
        this.section = section;
        this.date = date;
        this.inspector = inspector;
        this.workArea = workArea;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }
}
