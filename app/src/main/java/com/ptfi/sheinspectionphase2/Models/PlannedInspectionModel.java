package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/25/16.
 */

public class PlannedInspectionModel {

    public long id;
    public String inspector;
    public String areaOwner;
    public String location;
    public String date;
    public String risk1;
    public String rating1;
    public String risk2;
    public String rating2;
    public String risk3;
    public String rating3;
    public String risk4;
    public String rating4;
    public String risk5;
    public String rating5;
    public String risk6;
    public String rating6;
    public String risk7;
    public String rating7;
    public String risk8;
    public String rating8;
    public String code;
    public long mainId;

    public PlannedInspectionModel() {

    }

    public PlannedInspectionModel(long id, String inspector, String areaOwner, String location, String date, String risk1, String rating1, String risk2, String rating2, String risk3,
                                  String rating3, String risk4, String rating4, String risk5, String rating5, String risk6, String rating6, String risk7, String rating7, String risk8,
                                  String rating8, String code, long mainId) {
        this.id = id;
        this.inspector = inspector;
        this.areaOwner = areaOwner;
        this.location = location;
        this.date = date;
        this.risk1 = risk1;
        this.rating1 = rating1;
        this.risk2 = risk2;
        this.rating2 = rating2;
        this.risk3 = risk3;
        this.rating3 = rating3;
        this.risk4 = risk4;
        this.rating4 = rating4;
        this.risk5 = risk5;
        this.rating5 = rating5;
        this.risk6 = risk6;
        this.rating6 = rating6;
        this.risk7 = risk7;
        this.rating7 = rating7;
        this.risk8 = risk8;
        this.rating8 = rating8;
        this.code = code;
        this.mainId = mainId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getAreaOwner() {
        return areaOwner;
    }

    public void setAreaOwner(String areaOwner) {
        this.areaOwner = areaOwner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRisk1() {
        return risk1;
    }

    public void setRisk1(String risk1) {
        this.risk1 = risk1;
    }

    public String getRating1() {
        return rating1;
    }

    public void setRating1(String rating1) {
        this.rating1 = rating1;
    }

    public String getRisk2() {
        return risk2;
    }

    public void setRisk2(String risk2) {
        this.risk2 = risk2;
    }

    public String getRating2() {
        return rating2;
    }

    public void setRating2(String rating2) {
        this.rating2 = rating2;
    }

    public String getRisk3() {
        return risk3;
    }

    public void setRisk3(String risk3) {
        this.risk3 = risk3;
    }

    public String getRating3() {
        return rating3;
    }

    public void setRating3(String rating3) {
        this.rating3 = rating3;
    }

    public String getRisk4() {
        return risk4;
    }

    public void setRisk4(String risk4) {
        this.risk4 = risk4;
    }

    public String getRating4() {
        return rating4;
    }

    public void setRating4(String rating4) {
        this.rating4 = rating4;
    }

    public String getRisk5() {
        return risk5;
    }

    public void setRisk5(String risk5) {
        this.risk5 = risk5;
    }

    public String getRating5() {
        return rating5;
    }

    public void setRating5(String rating5) {
        this.rating5 = rating5;
    }

    public String getRisk6() {
        return risk6;
    }

    public void setRisk6(String risk6) {
        this.risk6 = risk6;
    }

    public String getRating6() {
        return rating6;
    }

    public void setRating6(String rating6) {
        this.rating6 = rating6;
    }

    public String getRisk7() {
        return risk7;
    }

    public void setRisk7(String risk7) {
        this.risk7 = risk7;
    }

    public String getRating7() {
        return rating7;
    }

    public void setRating7(String rating7) {
        this.rating7 = rating7;
    }

    public String getRisk8() {
        return risk8;
    }

    public void setRisk8(String risk8) {
        this.risk8 = risk8;
    }

    public String getRating8() {
        return rating8;
    }

    public void setRating8(String rating8) {
        this.rating8 = rating8;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getMainId() {
        return mainId;
    }

    public void setMainId(long mainId) {
        this.mainId = mainId;
    }
}
