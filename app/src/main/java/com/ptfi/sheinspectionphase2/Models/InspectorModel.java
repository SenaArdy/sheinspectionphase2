package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class InspectorModel {
    public long id;
    public String networkID;
    public String name;
    public String orgUnit;
    public String title;

    public InspectorModel(){
        this.networkID = "";

        this.name = "";
        this.title = "";
        this.orgUnit = "";
        this.id = 0;
    }

    public InspectorModel(String networkID, String name, String title, String orgUnit){
        this.networkID = networkID;
        this.name = name;
        this.title = title;
        this.orgUnit = orgUnit;
        this.id = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNetworkID() {
        return networkID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }
}
