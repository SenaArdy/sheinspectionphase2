package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class UGDrillingInspectionModel {

    public long id = -1;
    public String projectName = "";
    public String projectOwner = "";
    public String dateIns = "";
    public String inspection1 = "";
    public String remark1 = "";
    public String inspection2 = "";
    public String remark2 = "";
    public String inspection3 = "";
    public String remark3 = "";
    public String inspection4 = "";
    public String remark4 = "";
    public String inspection5 = "";
    public String remark5 = "";
    public String inspection6 = "";
    public String remark6 = "";
    public String inspection7 = "";
    public String remark7 = "";
    public String inspection8 = "";
    public String remark8 = "";
    public String inspection9 = "";
    public String remark9 = "";
    public String inspection10 = "";
    public String remark10 = "";

    public UGDrillingInspectionModel() {

    }

    public UGDrillingInspectionModel(long id, String projectName, String projectOwner, String dateIns, String inspection1, String remark1, String inspection2,
                                     String remark2, String inspection3, String remark3, String inspection4, String remark4, String inspection5, String remark5, String inspection6,
                                     String remark6, String inspection7, String remark7, String inspection8, String remark8, String inspection9, String remark9, String inspection10,
                                     String remark10) {
        this.id = id;
        this.projectName = projectName;
        this.projectOwner = projectOwner;
        this.dateIns = dateIns;
        this.inspection1 = inspection1;
        this.remark1 = remark1;
        this.inspection2 = inspection2;
        this.remark2 = remark2;
        this.inspection3 = inspection3;
        this.remark3 = remark3;
        this.inspection4 = inspection4;
        this.remark4 = remark4;
        this.inspection5 = inspection5;
        this.remark5 = remark5;
        this.inspection6 = inspection6;
        this.remark6 = remark6;
        this.inspection7 = inspection7;
        this.remark7 = remark7;
        this.inspection8 = inspection8;
        this.remark8 = remark8;
        this.inspection9 = inspection9;
        this.remark9 = remark9;
        this.inspection10 = inspection10;
        this.remark10 = remark10;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectOwner() {
        return projectOwner;
    }

    public void setProjectOwner(String projectOwner) {
        this.projectOwner = projectOwner;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getInspection1() {
        return inspection1;
    }

    public void setInspection1(String inspection1) {
        this.inspection1 = inspection1;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getInspection2() {
        return inspection2;
    }

    public void setInspection2(String inspection2) {
        this.inspection2 = inspection2;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getInspection3() {
        return inspection3;
    }

    public void setInspection3(String inspection3) {
        this.inspection3 = inspection3;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public String getInspection4() {
        return inspection4;
    }

    public void setInspection4(String inspection4) {
        this.inspection4 = inspection4;
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4;
    }

    public String getInspection5() {
        return inspection5;
    }

    public void setInspection5(String inspection5) {
        this.inspection5 = inspection5;
    }

    public String getRemark5() {
        return remark5;
    }

    public void setRemark5(String remark5) {
        this.remark5 = remark5;
    }

    public String getInspection6() {
        return inspection6;
    }

    public void setInspection6(String inspection6) {
        this.inspection6 = inspection6;
    }

    public String getRemark6() {
        return remark6;
    }

    public void setRemark6(String remark6) {
        this.remark6 = remark6;
    }

    public String getInspection7() {
        return inspection7;
    }

    public void setInspection7(String inspection7) {
        this.inspection7 = inspection7;
    }

    public String getRemark7() {
        return remark7;
    }

    public void setRemark7(String remark7) {
        this.remark7 = remark7;
    }

    public String getInspection8() {
        return inspection8;
    }

    public void setInspection8(String inspection8) {
        this.inspection8 = inspection8;
    }

    public String getRemark8() {
        return remark8;
    }

    public void setRemark8(String remark8) {
        this.remark8 = remark8;
    }

    public String getInspection9() {
        return inspection9;
    }

    public void setInspection9(String inspection9) {
        this.inspection9 = inspection9;
    }

    public String getRemark9() {
        return remark9;
    }

    public void setRemark9(String remark9) {
        this.remark9 = remark9;
    }

    public String getInspection10() {
        return inspection10;
    }

    public void setInspection10(String inspection10) {
        this.inspection10 = inspection10;
    }

    public String getRemark10() {
        return remark10;
    }

    public void setRemark10(String remark10) {
        this.remark10 = remark10;
    }
}
