package com.ptfi.sheinspectionphase2.Models;

/**
 * Created by senaardyputra on 11/12/16.
 */

public class FieldFindingsModel {

    public long id = -1;
    public long mainId = -1;
    public String company = "";
    public String dept = "";
    public String section = "";
    public String inspector = "";
    public String dateIns = "";
    public String findings = "";
    public String action = "";
    public String workArea = "";
    public String picture = "";
    public String picturePath = "";
    public String responsible = "";
    public String dateComplete = "";
    public long reference = -1;

    public FieldFindingsModel() {
        this.id = -1;
        this.mainId = -1;
        this.company = "";
        this.dept = "";
        this.section = "";
        this.inspector = "";
        this.dateIns = "";
        this.findings = "";
        this.action = "";
        this.workArea = "";
        this.picture = "";
        this.picturePath = "";
        this.responsible = "";
        this.dateComplete = "";
        this.reference = -1;
    }

    public FieldFindingsModel(long id, long mainId, String company, String dept, String section, String inspector, String dateIns,
                              String findings, String action, String workArea, String picture, String picturePath, String responsible,
                              String dateComplete, long reference) {
        this.id = id;
        this.mainId = mainId;
        this.company = company;
        this.dept = dept;
        this.section = section;
        this.inspector = inspector;
        this.dateIns = dateIns;
        this.findings = findings;
        this.action = action;
        this.workArea = workArea;
        this.picture = picture;
        this.picturePath = picturePath;
        this.responsible = responsible;
        this.dateComplete = dateComplete;
        this.reference = reference;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMainId() {
        return mainId;
    }

    public void setMainId(long mainId) {
        this.mainId = mainId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getWorkArea() {
        return workArea;
    }

    public void setWorkArea(String workArea) {
        this.workArea = workArea;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public String getDateComplete() {
        return dateComplete;
    }

    public void setDateComplete(String dateComplete) {
        this.dateComplete = dateComplete;
    }

    public long getReference() {
        return reference;
    }

    public void setReference(long reference) {
        this.reference = reference;
    }
}
