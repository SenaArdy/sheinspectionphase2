package com.ptfi.sheinspectionphase2.Models;

import com.ptfi.sheinspectionphase2.Utils.Helper;

/**
 * Created by Sena Ardy on 12/12/2015.
 */

public class DataSingleton {
//    private PemeriksaanK3Model k3;
    private String appName;
    //	private static HashMap<String, ArrayList<WetmuckRegularModel>> dataModelEntry;
    private static DataSingleton instance;

    private int selectedYear;
    private int selectedMonth;
    private int selectedDay;
    private int selectedHour;
    private int selectedMinute;

    private boolean isAgreeJSA = false;
    private boolean isAgreeSOP = false;

    // selected panel di details data entry
    private String selectedPanel;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

//    public PemeriksaanK3Model getK3() {
//        return k3;
//    }
//
//    public void setK3(PemeriksaanK3Model k3) {
//        this.k3 = k3;
//    }
//
//    public InspectorPemeriksaanK3Model getMainInspector() {
//        return k3.getInspectorList().get(0);
//    }

    public String getSelectedPanel() {
        return selectedPanel;
    }

    public void setSelectedPanel(String selectedPanel) {
        this.selectedPanel = selectedPanel;
    }

//	protected DataSingleton() {
//		setDataModelEntry(new HashMap<String, ArrayList<WetmuckRegularModel>>());
//	}
//
//	public static void initializeDataModelEntry() {
//		ArrayList<String> locations = Constants.getLocationDictionaryString();
//		for (String location : locations) {
//			String locationType = location.substring(0, 3);
//			if (!getDataModelEntry().containsKey(locationType)) {
//				getDataModelEntry().put(locationType,
//						new ArrayList<WetmuckRegularModel>());
//			}
//		}
//	}

    public static DataSingleton getInstance() {
        if (instance == null) {
            instance = new DataSingleton();
        }

        return instance;
    }

    public String getFormattedDateTime() {
        return Helper.formatNumber(selectedDay) + " "
                + Helper.numToMonthName(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear) + " "
                + Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public String getFormattedDate() {
        return Helper.formatNumber(selectedDay) + " "
                + Helper.numToMonthName(selectedMonth + 1) + " "
                + Helper.formatNumber(selectedYear);
    }

    public String getFormattedTime() {
        return Helper.formatNumber(selectedHour) + ":"
                + Helper.formatNumber(selectedMinute);
    }

    public void setDateTime(int year, int month, int day, int hour, int minutes) {
        selectedDay = day;
        selectedHour = hour;
        selectedYear = year;
        selectedMonth = month;
        selectedMinute = minutes;
    }

    public void setTime(int hour, int minutes) {
        selectedHour = hour;
        selectedMinute = minutes;
    }

    public void setDate(int year, int month, int day) {
        selectedDay = day;
        selectedYear = year;
        selectedMonth = month;
    }

//	public static HashMap<String, ArrayList<WetmuckRegularModel>> getDataModelEntry() {
//		return dataModelEntry;
//	}
//
//	public static void setDataModelEntry(
//			HashMap<String, ArrayList<WetmuckRegularModel>> dataModelEntry) {
//		DataSingleton.dataModelEntry = dataModelEntry;
//	}

    public boolean isAgreeJSA() {
        return isAgreeJSA;
    }

    public void setAgreeJSA(boolean isAgreeJSA) {
        this.isAgreeJSA = isAgreeJSA;
    }

    public boolean isAgreeSOP() {
        return isAgreeSOP;
    }

    public void setAgreeSOP(boolean isAgreeSOP) {
        this.isAgreeSOP = isAgreeSOP;
    }
}
