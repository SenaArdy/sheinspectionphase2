package com.ptfi.sheinspectionphase2.Models;

import android.graphics.Bitmap;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class GrsDetailModel {

    public long id = -1;
    public String pic = "";
    public String dateIns = "";
    public String department = "";
    public String comment = "";
    public String date = "";
    public String signer = "";
    public String sign = "";

    public GrsDetailModel() {

    }

    public GrsDetailModel(long id, String pic, String dateIns, String department, String comment, String date, String signer, String sign) {
        this.id = id;
        this.pic = pic;
        this.dateIns = dateIns;
        this.department = department;
        this.comment = comment;
        this.date = date;
        this.signer = signer;
        this.sign = sign;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDateIns() {
        return dateIns;
    }

    public void setDateIns(String dateIns) {
        this.dateIns = dateIns;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSigner() {
        return signer;
    }

    public void setSigner(String signer) {
        this.signer = signer;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
