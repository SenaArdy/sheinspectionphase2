package com.ptfi.sheinspectionphase2.Adapter;

/**
 * Created by Bacharudin on 09/22/17.
 */

public interface IPhotoAdapter {
    void onItemClicked(int position);
    void onItemDelete(int position);
}
