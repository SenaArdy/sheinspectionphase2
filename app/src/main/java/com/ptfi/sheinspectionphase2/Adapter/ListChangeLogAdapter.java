package com.ptfi.sheinspectionphase2.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;

public class ListChangeLogAdapter extends BaseAdapter {

	private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<String[]> listAboutContent;

	public ListChangeLogAdapter(Activity activity,
								ArrayList<String[]> listAboutContent) {
		super();
		this.activity = activity;
		this.listAboutContent = listAboutContent;
	}

	@Override
	public int getCount() {
		return listAboutContent.size();
	}

	@Override
	public Object getItem(int position) {
		return listAboutContent.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        	convertView = inflater.inflate(R.layout.custom_listview_change_log, null);

        TextView titleTV = (TextView) convertView.findViewById(R.id.title_tv);
        TextView contentTV = (TextView) convertView.findViewById(R.id.content_tv);
        
        String[] listAbout = listAboutContent.get(position);
        titleTV.setText(listAbout[0]);
        contentTV.setText(listAbout[1]);
        
		return convertView;
	}
	
}