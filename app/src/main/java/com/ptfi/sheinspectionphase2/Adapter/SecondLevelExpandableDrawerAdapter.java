package com.ptfi.sheinspectionphase2.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Models.DrawerItem;
import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 1/13/17.
 */

public class SecondLevelExpandableDrawerAdapter extends
        BaseExpandableListAdapter {

    /**
     * LayoutInflater instance for inflating the requested layout in the list
     * view
     */
    private LayoutInflater mInflater;

    private DrawerItem mParentDataSet;
    private ArrayList<DrawerItem> mChildDataSet;

    private Context context;

    /**
     * Default constructor
     */
    public SecondLevelExpandableDrawerAdapter(Context context,
                                              DrawerItem parentDataSet, ArrayList<DrawerItem> childDataSet) {

        mInflater = LayoutInflater.from(context);
        mParentDataSet = parentDataSet;
        mChildDataSet = childDataSet;
        this.context = context;
    }

    private static class ViewHolder {
        LinearLayout itemLayout;
        ImageView backButton;
        TextView title;
        View backLine;
        View topLine;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mChildDataSet.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mChildDataSet.get(childPosition).getType();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View recycledView, ViewGroup parent) {
        ViewHolder holder;
        if (recycledView == null) {

            holder = new ViewHolder();
            recycledView = mInflater.inflate(R.layout.custom_drawer_item,
                    parent, false);
            holder.itemLayout = (LinearLayout) recycledView
                    .findViewById(R.id.itemLayout);
            holder.backButton = (ImageView) recycledView
                    .findViewById(R.id.backBtn);
            holder.title = (TextView) recycledView.findViewById(R.id.title);
            holder.backLine = recycledView.findViewById(R.id.backLine);
            holder.topLine = recycledView.findViewById(R.id.topDivider);
            recycledView.setTag(holder);
        } else {
            holder = (ViewHolder) recycledView.getTag();
        }

        final DrawerItem item = (DrawerItem) getChild(groupPosition,
                childPosition);

        holder.itemLayout.setVisibility(View.VISIBLE);
        if (item.getMenu() == DrawerItem.HOME
                || item.getMenu() == DrawerItem.EXIT) {
            if (item.getMenu() == DrawerItem.HOME)
                holder.backButton.setImageResource(R.drawable.icon_home);
            else if (item.getMenu() == DrawerItem.EXIT)
                holder.backButton.setImageResource(R.drawable.icon_logout);
            holder.backButton.setVisibility(View.VISIBLE);
        } else
            holder.backButton.setVisibility(View.GONE);

        holder.title.setTypeface(null, Typeface.BOLD);
        holder.title.setText(item.getTitle());
        holder.title.setTextSize(12);
        holder.title.setTextColor(context.getResources()
                .getColor(R.color.White));
        holder.backLine.setVisibility(View.GONE);
        holder.topLine.setVisibility(View.GONE);

        holder.itemLayout.setBackgroundResource(R.drawable.background_menu);
        holder.itemLayout.setPadding(30, 0, 0, 0);

        return recycledView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (mChildDataSet == null) {
            return 0;
        } else {
            return mChildDataSet.size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mParentDataSet;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mParentDataSet.getType();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View recycledView, ViewGroup parent) {
        ViewHolder holder;
        if (recycledView == null) {

            holder = new ViewHolder();
            recycledView = mInflater.inflate(R.layout.custom_drawer_item,
                    parent, false);
            holder.itemLayout = (LinearLayout) recycledView
                    .findViewById(R.id.itemLayout);
            holder.backButton = (ImageView) recycledView
                    .findViewById(R.id.backBtn);
            holder.title = (TextView) recycledView.findViewById(R.id.title);
            holder.backLine = recycledView.findViewById(R.id.backLine);
            holder.topLine = recycledView.findViewById(R.id.topDivider);
            recycledView.setTag(holder);

        } else {
            holder = (ViewHolder) recycledView.getTag();
        }

        final DrawerItem item = (DrawerItem) getGroup(groupPosition);
        switch (item.getType()) {
            case DrawerItem.BACK_BUTTON: {
                holder.itemLayout.setVisibility(View.VISIBLE);
                holder.backButton.setImageResource(R.drawable.button_back);
                holder.backButton.setVisibility(View.VISIBLE);
                holder.title.setText(item.getTitle());
                holder.title.setTypeface(null, Typeface.BOLD);
                holder.title.setTextColor(context.getResources().getColor(
                        R.color.observLightGrey1));
                holder.backLine.setVisibility(View.VISIBLE);
            }
            break;
            case DrawerItem.PARENT:
            case DrawerItem.CHILD: {
                holder.itemLayout.setVisibility(View.VISIBLE);
                if (item.getMenu() == DrawerItem.HOME
                        || item.getMenu() == DrawerItem.EXIT) {
                    if (item.getMenu() == DrawerItem.HOME)
                        holder.backButton.setImageResource(R.drawable.icon_home);
                    else if (item.getMenu() == DrawerItem.EXIT)
                        holder.backButton.setImageResource(R.drawable.icon_logout);
                    holder.backButton.setVisibility(View.VISIBLE);
                } else
                    holder.backButton.setVisibility(View.GONE);

                holder.title.setTypeface(null, Typeface.BOLD);
                holder.title.setText(item.getTitle());
                holder.title.setTextSize(15);
                holder.title.setTextColor(context.getResources().getColor(
                        R.color.White));
                holder.backLine.setVisibility(View.GONE);
                holder.topLine.setVisibility(View.GONE);
            }
            break;
        }

        holder.itemLayout.setBackgroundResource(R.drawable.background_menu);
        holder.itemLayout.setPadding(15, 0, 0, 0);

        return recycledView;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }
}
