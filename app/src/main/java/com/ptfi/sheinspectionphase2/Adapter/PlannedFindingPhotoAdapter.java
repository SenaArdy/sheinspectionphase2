package com.ptfi.sheinspectionphase2.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.util.ArrayList;

import static com.ptfi.sheinspectionphase2.R.id.actionTV;

/**
 * Created by Bacharudin on 09/23/17.
 */

public class PlannedFindingPhotoAdapter extends RecyclerView.Adapter<PlannedFindingPhotoAdapter.ViewHolder> {

    private ArrayList<PlannedFindingsModel> plannedFindingsModels;
    private IPhotoAdapter iFindingListCallback;


    public PlannedFindingPhotoAdapter(ArrayList<PlannedFindingsModel> models,
                                      IPhotoAdapter callback) {
        super();
        this.plannedFindingsModels = models;
        iFindingListCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_findings, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(plannedFindingsModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        return plannedFindingsModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView listfindingCV;
        public ImageView pictureIV;
        public TextView findingTV;
        public TextView dateCompleteTV;
        public TextView commentTV;
        public TextView responsibleTV;
        public TextView riskTV;
        public ImageView deleteIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            listfindingCV = (CardView) itemView.findViewById(R.id.listfindingCV);
            pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
            findingTV = (TextView) itemView.findViewById(R.id.findingsTV);
            commentTV = (TextView) itemView.findViewById(R.id.commentTV);
            dateCompleteTV = (TextView) itemView.findViewById(R.id.dateCompleteTV);
            responsibleTV = (TextView) itemView.findViewById(R.id.responsibleTV);
            riskTV = (TextView) itemView.findViewById(R.id.riskTV);
            deleteIcon = (ImageView) itemView.findViewById(R.id.deleteIcon);
        }

        public void bindData(final PlannedFindingsModel model, final int position) {
            findingTV.setText(model.getFindings());
            commentTV.setText(model.getComment());
            dateCompleteTV.setText(model.getDateIns());
            responsibleTV.setText(model.getResponsible());
            riskTV.setText(model.getRisk());

            if (model.getPhotoPath() != null && model.getPhotoPath().equals("")) {
                pictureIV.setImageResource(R.drawable.default_nopicture);
            } else {
                Helper.setPic(pictureIV, model.getPhotoPath());
            }

            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iFindingListCallback.onItemDelete(position);
                }
            });

            listfindingCV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    iFindingListCallback.onItemClicked(position);
                }
            });
        }
    }
}
