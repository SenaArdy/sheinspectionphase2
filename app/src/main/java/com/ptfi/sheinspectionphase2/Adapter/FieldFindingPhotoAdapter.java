package com.ptfi.sheinspectionphase2.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.util.ArrayList;

/**
 * Created by Bacharudin on 09/22/17.
 */

public class FieldFindingPhotoAdapter extends RecyclerView.Adapter<FieldFindingPhotoAdapter.ViewHolder> {

    private ArrayList<FieldFindingsModel> fieldFindingsModels;
    private IPhotoAdapter iFindingListCallback;


    public FieldFindingPhotoAdapter(ArrayList<FieldFindingsModel> models,
                                    IPhotoAdapter callback) {
        super();
        this.fieldFindingsModels = models;
        iFindingListCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_field, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(fieldFindingsModels.get(position), position);
    }

    @Override
    public int getItemCount() {
        return fieldFindingsModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView listCV;
        public ImageView pictureIV;
        public TextView findingTV;
        public TextView actionTV;
        public TextView responsTV;
        public TextView dateCompleteTV;
        public ImageView deleteIcon;

        public ViewHolder(View itemView) {
            super(itemView);

            listCV = (CardView) itemView.findViewById(R.id.listCV);
            pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
            findingTV = (TextView) itemView.findViewById(R.id.findingTV);
            actionTV = (TextView) itemView.findViewById(R.id.actionTV);
            responsTV = (TextView) itemView.findViewById(R.id.responsTV);
            dateCompleteTV = (TextView) itemView.findViewById(R.id.dateCompleteTV);
            deleteIcon = (ImageView) itemView.findViewById(R.id.deleteIcon);
        }

        public void bindData(final FieldFindingsModel model, final int position) {
            findingTV.setText(Helper.trimString(model.getFindings()));
            actionTV.setText(Helper.trimString(model.getAction()));
            responsTV.setText("Responsible : " + model.getResponsible());
            dateCompleteTV.setText("Date Complete : " + model.getDateComplete());

            if (model.getPicturePath() != null) {
                if (model.getPicturePath().equals("")) {
                    pictureIV.setImageResource(R.drawable.default_nopicture);
                } else {
                    Helper.setPic(pictureIV, model.getPicturePath());
                }
            }
            deleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iFindingListCallback.onItemDelete(position);
                }
            });
            listCV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    iFindingListCallback.onItemClicked(position);
                }
            });
        }
    }
}
