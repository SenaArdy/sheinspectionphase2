package com.ptfi.sheinspectionphase2.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;

public class ListAboutAdapter extends BaseAdapter {

	private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<String[]> listAboutContent;
    
	public ListAboutAdapter(Activity activity,
							ArrayList<String[]> listAboutContent) {
		super();
		this.activity = activity;
		this.listAboutContent = listAboutContent;
	}

	@Override
	public int getCount() {
		return listAboutContent.size();
	}

	@Override
	public Object getItem(int position) {
		return listAboutContent.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        	convertView = inflater.inflate(R.layout.list_about, null);

        TextView title1TV = (TextView) convertView.findViewById(R.id.title_1_tv);
        TextView title2TV = (TextView) convertView.findViewById(R.id.title_2_tv);
        TextView contentTV = (TextView) convertView.findViewById(R.id.content_tv);
        TextView subTitleTV = (TextView) convertView.findViewById(R.id.sub_title_tv);

        String[] listAbout = listAboutContent.get(position);
        title1TV.setText(listAbout[0]);
        contentTV.setText(listAbout[1]);
		subTitleTV.setText(listAbout[2]);

		if (listAbout[1].equalsIgnoreCase("")) {
			title2TV.setText(listAbout[0]);
			contentTV.setVisibility(View.GONE);
			title1TV.setVisibility(View.GONE);
			title2TV.setVisibility(View.VISIBLE);
		}
		else {
			contentTV.setVisibility(View.VISIBLE);
			title1TV.setVisibility(View.VISIBLE);
			title2TV.setVisibility(View.GONE);
		}

		return convertView;
	}
	
}