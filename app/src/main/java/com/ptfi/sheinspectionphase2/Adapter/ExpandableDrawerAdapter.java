package com.ptfi.sheinspectionphase2.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.ptfi.sheinspectionphase2.Fragments.NavigationDrawerFragment;
import com.ptfi.sheinspectionphase2.Models.DrawerItem;
import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by senaardyputra on 1/13/17.
 */

public class ExpandableDrawerAdapter extends BaseExpandableListAdapter {

    /**
     * LayoutInflater instance for inflating the requested layout in the list
     * view
     */
    private LayoutInflater mInflater;

    private ArrayList<DrawerItem> mParentDataSet;
    private HashMap<Integer, ArrayList<DrawerItem>> mChildDataSet;
    private HashMap<Integer, ArrayList<DrawerItem>> mSecondLevelChildDataSet;

    private Context context;

    /**
     * Default constructor
     */
    public ExpandableDrawerAdapter(Context context,
                                   ArrayList<DrawerItem> parentDataSet,
                                   HashMap<Integer, ArrayList<DrawerItem>> childDataSet,
                                   HashMap<Integer, ArrayList<DrawerItem>> mSecondLevelChildDataSet) {

        mInflater = LayoutInflater.from(context);
        mParentDataSet = parentDataSet;
        mChildDataSet = childDataSet;
        this.mSecondLevelChildDataSet = mSecondLevelChildDataSet;
        this.context = context;
    }

    private static class ViewHolder {
        LinearLayout itemLayout;
        ImageView backButton;
        TextView title;
        View backLine;
        View topLine;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        int type = mParentDataSet.get(groupPosition).getMenu();
        ArrayList<DrawerItem> childs = mChildDataSet.get(type);
        return childs.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mChildDataSet.get(mParentDataSet.get(groupPosition).getMenu())
                .get(childPosition).getType();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View recycledView, ViewGroup parent) {
        CustExpListview expLV = new CustExpListview(context);
        expLV.setLayoutParams(new ListView.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        DrawerItem child = (DrawerItem) getChild(groupPosition, childPosition);
        SecondLevelExpandableDrawerAdapter secondAdapt = new SecondLevelExpandableDrawerAdapter(
                context, child, mSecondLevelChildDataSet.get(child.getMenu()));

        expLV.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(context, "group : " + groupPosition,
                // Toast.LENGTH_SHORT).show();
                DrawerItem menu = (DrawerItem) parent
                        .getExpandableListAdapter().getGroup(groupPosition);

                int index = parent.getFlatListPosition(ExpandableListView
                        .getPackedPositionForGroup(groupPosition));

                if (menu.getChild() == DrawerItem.NO_CHILD) {
                    NavigationDrawerFragment.selectItem(index, menu);
                } else {
                    // mDrawerListView.expandGroup(groupPosition, true);
                }
                return false;
            }
        });

        expLV.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // Toast.makeText(context, groupPosition + " : " +
                // childPosition,
                // Toast.LENGTH_SHORT).show();
                DrawerItem menu = (DrawerItem) parent
                        .getExpandableListAdapter().getChild(groupPosition,
                                childPosition);

                int index = parent.getFlatListPosition(ExpandableListView
                        .getPackedPositionForChild(groupPosition, childPosition));

                NavigationDrawerFragment.selectItem(index, menu);
                return false;
            }
        });

        expLV.setAdapter(secondAdapt);
        expLV.setGroupIndicator(null);

        // ViewHolder holder;
        // if (recycledView == null) {
        //
        // holder = new ViewHolder();
        // recycledView = mInflater.inflate(R.layout.custom_drawer_item,
        // parent, false);
        // holder.itemLayout = (LinearLayout) recycledView
        // .findViewById(R.id.itemLayout);
        // holder.backButton = (ImageView) recycledView
        // .findViewById(R.id.backBtn);
        // holder.title = (TextView) recycledView.findViewById(R.id.title);
        // holder.backLine = recycledView.findViewById(R.id.backLine);
        // recycledView.setTag(holder);
        // } else {
        // holder = (ViewHolder) recycledView.getTag();
        // }
        //
        // final DrawerItem item = (DrawerItem) getChild(groupPosition,
        // childPosition);
        //
        // holder.itemLayout.setVisibility(View.VISIBLE);
        // if (item.getMenu() == DrawerItem.HOME
        // || item.getMenu() == DrawerItem.EXIT) {
        // if (item.getMenu() == DrawerItem.HOME)
        // holder.backButton.setImageResource(R.drawable.icon_home);
        // else if (item.getMenu() == DrawerItem.EXIT)
        // holder.backButton.setImageResource(R.drawable.icon_logout);
        // holder.backButton.setVisibility(View.VISIBLE);
        // } else
        // holder.backButton.setVisibility(View.GONE);
        //
        // holder.title.setTypeface(null, Typeface.BOLD);
        // holder.title.setText(item.getTitle());
        // holder.title.setTextSize(15);
        // holder.title.setTextColor(context.getResources().getColor(
        // R.color.Yellow));
        // holder.backLine.setVisibility(View.GONE);
        //
        // holder.itemLayout.setBackgroundResource(R.drawable.background_menu);
        // holder.itemLayout.setPadding(15, 0, 0, 0);
        //
        // return recycledView;
        return expLV;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<DrawerItem> childs = mChildDataSet.get(mParentDataSet.get(
                groupPosition).getMenu());
        if (childs == null) {
            return 0;
        } else {
            return childs.size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mParentDataSet.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mParentDataSet.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mParentDataSet.get(groupPosition).getType();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View recycledView, ViewGroup parent) {
        ViewHolder holder;
        if (recycledView == null) {

            holder = new ViewHolder();
            recycledView = mInflater.inflate(R.layout.custom_drawer_item,
                    parent, false);
            holder.itemLayout = (LinearLayout) recycledView
                    .findViewById(R.id.itemLayout);
            holder.backButton = (ImageView) recycledView
                    .findViewById(R.id.backBtn);
            holder.title = (TextView) recycledView.findViewById(R.id.title);
            holder.backLine = recycledView.findViewById(R.id.backLine);
            holder.topLine = recycledView.findViewById(R.id.topDivider);
            recycledView.setTag(holder);

        } else {
            holder = (ViewHolder) recycledView.getTag();
        }

        final DrawerItem item = mParentDataSet.get(groupPosition);
        if (item.getImgResource() != 0) {
            holder.backButton.setImageResource(item.getImgResource());
        } else
            holder.backButton.setVisibility(View.GONE);

        holder.title.setTypeface(null, Typeface.BOLD);
        holder.title.setText(item.getTitle());
        holder.title.setAllCaps(true);
        holder.title.setTextColor(context.getResources().getColor(
                R.color.orangeCivil));

        if (groupPosition == getGroupCount() - 1) {
            holder.backLine.setVisibility(View.VISIBLE);
        } else {
            holder.backLine.setVisibility(View.GONE);
        }

        if (groupPosition == 0) {
            holder.topLine.setVisibility(View.GONE);
        } else {
            holder.topLine.setVisibility(View.VISIBLE);
        }

        holder.itemLayout.setBackgroundResource(R.drawable.background_menu);

        return recycledView;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return true;
    }

    public class CustExpListview extends ExpandableListView {

        int intGroupPosition, intChildPosition, intGroupid;

        public CustExpListview(Context context) {
            super(context);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(960,
                    MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(900,
                    MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
