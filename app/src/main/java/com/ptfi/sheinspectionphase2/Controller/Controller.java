package com.ptfi.sheinspectionphase2.Controller;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.ptfi.sheinspectionphase2.Database.DataHelper;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.FieldMainModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PlannedMainModel;

/**
 * Created by senaardyputra on 4/6/17.
 */

public class Controller extends MultiDexApplication{

    public ManpowerModel manpowerModel = new ManpowerModel();
    public PlannedMainModel plannedMainModel = new PlannedMainModel();
    public FieldMainModel fieldMainModel = new FieldMainModel();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public ManpowerModel getManpowerModel() {
        return manpowerModel;
    }

    public void setManpowerModel(ManpowerModel manpowerModel) {
        this.manpowerModel = manpowerModel;
    }

    public PlannedMainModel getPlannedMainModel() {
        return plannedMainModel;
    }

    public void setPlannedMainModel(PlannedMainModel plannedMainModel) {
        this.plannedMainModel = plannedMainModel;
    }

    public FieldMainModel getFieldMainModel() {
        return fieldMainModel;
    }

    public void setFieldMainModel(FieldMainModel fieldMainModel) {
        this.fieldMainModel = fieldMainModel;
    }
}
