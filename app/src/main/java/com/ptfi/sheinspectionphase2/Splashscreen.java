package com.ptfi.sheinspectionphase2;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.aspose.words.AsposeWordsApplication;
import com.aspose.words.License;
import com.ptfi.sheinspectionphase2.Controller.Controller;
import com.ptfi.sheinspectionphase2.Database.AssetDatabaseOpenHelper;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.CustomSpinnerItem;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Utils.CSVHelper;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class Splashscreen extends RuntimePermissionsActivity {

    private Context context;

    String extStorageDirectory = Environment.getExternalStorageDirectory()
            .getAbsolutePath();
    String basepath = extStorageDirectory + "/" + Constants.ROOT_FOLDER_NAME
            + "/";

    boolean trueManpower = false;

    Controller aController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new AsposeWordsApplication().loadLibs(this.getApplicationContext());
        Splashscreen.this.getWindow().setSoftInputMode
                (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashscreen);

        aController = (Controller) getApplicationContext();

//        try {
//            AssetManager assetManager = this.getResources()
//                    .getAssets();
//            License license = new License();
//                    InputStream is = getResources().openRawResource(R.raw.licence);
//            license.setLicense(is);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        new AsposeWordsApplication().loadLibs(this.getApplicationContext());

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initCheckPermission();
        } else {

            // REPLACE DATA OBSERVER
            InitAllData initTask = new InitAllData();
            initTask.execute();
        }
    }

    private void initCheckPermission() {
        Splashscreen.super.requestAppPermissions(new
                        String[]{Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.RECORD_AUDIO}, R.string
                        .runtime_permissions_txt
                , Constants.REQUEST_ANDROID_PERMISSIONS);
    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        if (requestCode == Constants.REQUEST_ANDROID_PERMISSIONS) {

            // REPLACE DATA OBSERVER
            InitAllData initTask = new InitAllData();
            initTask.execute();
        }
    }

    private void initAsposeLicense() {
        setActivityStatus("Initialize License");
        try {
            AssetManager assetManager = getResources().getAssets();
            License license = new License();
            InputStream is = assetManager.open("Aspose.Words.lic");
            license.setLicense(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createFolder() {
        File ptfi = new File(basepath);
        File sheInspection = new File(basepath + "/"
                + Constants.APP_FOLDER_NAME + "/");
        File importFolder = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/" + Constants.IMPORT_FOLDER_NAME + "/");
        File exportFolder = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/" + Constants.EXPORT_FOLDER_NAME + "/");
        File templateFolder = new File(basepath + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FOLDER_NAME + "/");
        Helper.createDirectory(Constants.EXTERNAL_ROOT_FOLDER_SCDB);

        if (!ptfi.exists())
            ptfi.mkdirs();
        if (!sheInspection.exists())
            sheInspection.mkdirs();
        if (!importFolder.exists())
            importFolder.mkdirs();
        if (!exportFolder.exists())
            exportFolder.mkdirs();
        if (templateFolder.exists()) {
            try {
                Helper.delete(templateFolder);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        templateFolder.mkdirs();

        DataSource ds = new DataSource(this);
        ds.open();
        File importFolderPath, importFolderChecked;

        // POTENTIAL RISK
//        if (ds.getAllPotentialRisk().isEmpty()) {
//            importFolderPath = new File(basepath + "/"
//                    + Constants.APP_FOLDER_NAME + "/"
//                    + Constants.IMPORT_FOLDER_NAME + "/"
//                    + Constants.POTENTIAL_RISK_FILE_NAME);
//            if (!importFolderPath.exists())
//                Helper.copyFileAsset(Constants.POTENTIAL_RISK_FILE_NAME,
//                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//        }

        // LOOK UP
//        if (ds.getAllLookUp().isEmpty()) {
//            importFolderPath = new File(basepath + "/"
//                    + Constants.APP_FOLDER_NAME + "/"
//                    + Constants.IMPORT_FOLDER_NAME + "/"
//                    + Constants.LOOKUP_FILE_NAME);
//            if (!importFolderPath.exists())
//                Helper.copyFileAsset(Constants.LOOKUP_FILE_NAME,
//                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//        }

        // RATING
        if (ds.getAllRating().isEmpty()) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.RATING_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.RATING_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

        // COMPANY
        if (ds.getAllCompany().size() <= 2) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.COMPANY_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.COMPANY_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

        // BUSINESS UNIT
        if (ds.getAllBusiness().size() <= 2) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.BUSINESS_UNIT_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.BUSINESS_UNIT_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

        // DEPARTMENT
        if (ds.getAllDepartment().size() <= 2) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.DEPARTMENT_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.DEPARTMENT_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

        // SECTION
        if (ds.getAllSection().size() <= 2) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.SECTION_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.SECTION_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

        // AVAILABILITY
        if (ds.getAllAvailibity().isEmpty()) {
            importFolderPath = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.AVAILABILITY_FILE_NAME);
            if (!importFolderPath.exists())
                Helper.copyFileAsset(Constants.AVAILABILITY_FILE_NAME,
                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
        }

//        // MANPOWER
//        importFolderChecked = new File(basepath + "/"
//                + Constants.APP_FOLDER_NAME + "/"
//                + Constants.IMPORT_FOLDER_NAME + "/"
//                + Constants.MANPOWER_FILE_NAME);
//        if (!importFolderChecked.exists()) {
//            Helper.copyFileAsset(Constants.MANPOWER_FILE_NAME,
//                    Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//        }

        importFolderPath = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/" + Constants.TEMPLATE_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FILE_NAME);
        if (importFolderPath.exists()) {
            importFolderPath.delete();
        }
        Helper.copyFileAsset(Constants.TEMPLATE_FILE_NAME,
                Constants.TEMPLATE_FOLDER_NAME, Splashscreen.this);

        importFolderPath = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/" + Constants.TEMPLATE_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FIELD_INSPECTION_FILE_NAME);
        if (!importFolderPath.exists()) {
            importFolderPath.delete();
        }
        Helper.copyFileAsset(Constants.TEMPLATE_FIELD_INSPECTION_FILE_NAME,
                Constants.TEMPLATE_FOLDER_NAME, Splashscreen.this);
    }

    /**
     * create folder if it's doesn't exist yet
     **/
    private void initDocumentAgreement() {
        File comPTFI = new File(basepath);
        File appFolder = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/");
        File sopId = new File(basepath + "/" + Constants.APP_FOLDER_NAME + "/"
                + Constants.DOCUMENTS_FOLDER_NAME + "/" + Constants.SOP_ID
                + "/");
        File sopEn = new File(basepath + "/" + Constants.APP_FOLDER_NAME + "/"
                + Constants.DOCUMENTS_FOLDER_NAME + "/" + Constants.SOP_EN
                + "/");
        File documents = new File(basepath + "/" + Constants.APP_FOLDER_NAME
                + "/" + Constants.DOCUMENTS_FOLDER_NAME + "/");

        if (!comPTFI.exists()) {
            comPTFI.mkdirs();
        }
        if (!appFolder.exists()) {
            appFolder.mkdirs();
        }
        if (!documents.exists()) {
            documents.mkdirs();
        }
        if (sopId.exists()) {
            try {
                Helper.delete(sopId);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        sopId.mkdirs();

        if (sopEn.exists()) {
            try {
                Helper.delete(sopEn);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        sopEn.mkdirs();

        if (sopId.listFiles() == null)
            copySOPId();
        else if (sopId.listFiles().length == 0)
            copySOPId();
        if (sopEn.listFiles() == null)
            copySOPEn();
        else if (sopEn.listFiles().length == 0)
            copySOPEn();
    }

    /**
     * copy JSA file from assets to device
     **/
    private void copySOPEn() {
        AssetManager assetManager = getResources().getAssets();
        String[] files = null;
        try {
            files = assetManager.list(Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.DOCUMENTS_FOLDER_NAME + "/" + Constants.SOP_EN);
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        for (int i = 0; i < files.length; i++) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                        + Constants.SOP_EN + "/" + files[i]);
                out = new FileOutputStream(basepath + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                        + Constants.SOP_EN + "/" + files[i]);
                Helper.copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                // Log.e("copy clipart ERROR", e.toString());
                // e.printStackTrace();
            }
        }
    }

    /**
     * copy SOP file from assets to device
     **/
    private void copySOPId() {
        AssetManager assetManager = getResources().getAssets();
        String[] files = null;
        try {
            files = assetManager.list(Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.DOCUMENTS_FOLDER_NAME + "/" + Constants.SOP_ID);
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        for (int i = 0; i < files.length; i++) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                        + Constants.SOP_ID + "/" + files[i]);
                out = new FileOutputStream(basepath + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                        + Constants.SOP_ID + "/" + files[i]);
                Helper.copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                // Log.e("copy clipart ERROR", e.toString());
                // e.printStackTrace();
            }
        }
    }

    private void initObserverData() {
        DataSource dataSource = new DataSource(Splashscreen.this);
        dataSource.open();
        ArrayList<ManpowerModel> observers = dataSource.getAllDataObserver();
        for (ManpowerModel obs : observers) {
            Log.d("OBS", obs.getName());
        }
        if (observers.size() <= 0) {
            AssetDatabaseOpenHelper dbHelper = new AssetDatabaseOpenHelper(
                    Splashscreen.this);

            try {
                dbHelper.createDataBase();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            SQLiteDatabase db = dbHelper.openDataBase();
            Cursor c1 = db.rawQuery("SELECT * FROM MANPOWER", null);
            c1.moveToFirst();
            while (!c1.isAfterLast()) {
                ManpowerModel obs = new ManpowerModel();
                obs.setIdno(c1.getString(0));
                obs.setName(c1.getString(1));
                obs.setTitle(c1.getString(2));
                obs.setOrg(c1.getString(3));

                dataSource.createDataObserver(obs);
                c1.moveToNext();
            }
            c1.close();
        }

        ArrayList<ManpowerModel> manpowers = dataSource.getAllDataObserver();
        for (ManpowerModel obs : manpowers) {
            Log.d("OBS2", obs.getName());
        }

        Constants.setObserverDictionary(manpowers);
        dataSource.close();
    }

    private void setActivityStatus(final String text) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (text != null) {
                    TextView activityText = (TextView) findViewById(R.id.loadingText);
                    if (activityText != null) {
                        activityText.setText(text);
                    }
                }
            }
        });
    }

    private boolean isValidPathAndFolderName(String filePath) {
        if (filePath != null && filePath.length() > 0
                && new File(filePath).exists()) {
            if (filePath.contains("/")
                    && filePath.substring(filePath.length() - 4,
                    filePath.length()).equalsIgnoreCase(".csv")) {
                return true;
            } else
                return false;
        } else
            return false;
    }

    private void setVersionName() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                TextView activityText = (TextView) findViewById(R.id.versionName);
                if (activityText != null) {
                    activityText.setText("Version : "
                            + Helper.getAppVersion(Splashscreen.this));
                }
            }
        });
    }

    private class InitAllData extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Helper.showProgressDialog(MainActivity.this, "Loading",
//                    "Initializing Observer data, it may take a few minutes, please be patient..");
            setActivityStatus("Initialise Data For This Application");
        }

        @Override
        protected Void doInBackground(Void... params) {
            setVersionName();
            createFolder();
            initAsposeLicense();
            initDocumentAgreement();
            checkAppData();
            SharedPreferences prefs;
            prefs = getSharedPreferences(
                    "com.ptfi.sheinspectionphase2.Splashscreen", MODE_PRIVATE);

            if (!Helper.isExistPath(Constants.SCDB_FILE_PREFIX.MANPOWER)) {
                Helper.copyFileFromAssets(
                        Constants.EXTERNAL_ROOT_FOLDER_SCDB,
                        Constants.IMPORT_FOLDER_ON_ASSETS_PATH,
                        Splashscreen.this, Constants.MANPOWER_FILE_NAME);
            }
            List<File> listFiles = Helper.getListFiles(new File(Constants.EXTERNAL_ROOT_FOLDER_SCDB));
            List<String> listFileNames = Helper.getListFileName(listFiles, Constants.SCDB_FILE_PREFIX.MANPOWER);
            if (isValidPathAndFolderName(Constants.EXTERNAL_ROOT_FOLDER_SCDB
                    + Helper.getLatestFileName(listFileNames)) || !aController.getManpowerModel().isContentOfTableExist(Splashscreen.this)) {
                List<String[]> readResultPanel = CSVHelper
                        .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
                                + Helper.getLatestFileName(listFileNames));
                if (!prefs.getString("inspector_version", "").equalsIgnoreCase(Helper.getLatestFileName(listFileNames)) ||
                        !aController.getManpowerModel().isContentOfTableExist(Splashscreen.this)) {
                    if ((listFiles.size() == 1 && listFileNames.get(0).equalsIgnoreCase(Constants.MANPOWER_FILE_NAME)) &&
                            aController.getManpowerModel().isContentOfTableExist(Splashscreen.this)) {
                        trueManpower = true;

                        DataSource dataSource = new DataSource(Splashscreen.this);
                        dataSource.open();
                        setActivityStatus("Load Inspector lookup data");
                        Constants.setObserverDictionary(dataSource.getAllManpowerResources());

                        dataSource.close();
                    } else {
                        setActivityStatus("Updating Inspector lookup data");

                        DataSource dataSource = new DataSource(Splashscreen.this);
                        dataSource.open();
                        dataSource.deleteAllDataObserver();

                        int i = 0;

                        for (String[] strings : readResultPanel) {
                            if (i > 0 && strings.length > 1) {
                                ManpowerModel om = new ManpowerModel();
                                om.setIdno(strings[1]);
                                om.setName(strings[0]);
                                om.setOrg(strings[5]);
                                om.setTitle(strings[2]);
                                om.setCompany(strings[6]);
                                om.setDivisi(strings[5]);
                                om.setDepartment(strings[3]);
                                om.setSection(strings[4]);

                                dataSource.createDataObserver(om);
                                trueManpower = true;
                                Log.d("OBS2", om.getName());
                            }
                            i++;
                        }
                        Constants.setObserverDictionary(dataSource
                                .getAllManpowerResources());

                        dataSource.close();
                        prefs.edit()
                                .putString(
                                        "inspector_version",
                                        Helper.getLatestFileName(listFileNames))
                                .apply();
                    }
                } else {
                    trueManpower = true;

                    DataSource dataSource = new DataSource(Splashscreen.this);
                    dataSource.open();
                    setActivityStatus("Load Inspector lookup data");
                    Constants.setObserverDictionary(dataSource.getAllManpowerResources());

                    dataSource.close();
                }
            }
            if (listFileNames.size() > 1)
                Helper.deleteUnusedFile(listFileNames);
//            initObserverData();

//            File importManpower = new File(basepath + "/"
//                    + Constants.APP_FOLDER_NAME + "/"
//                    + Constants.IMPORT_FOLDER_NAME + "/"
//                    + Constants.MANPOWER_FILE_NAME);

//            File importManpower = new File(basepath + "/"
//                    + Constants.SCDB_LOOKUP_FOLDER_NAME + "/"  + Constants.MANPOWER_SCDB_FILE_NAME);
//            DataSource dataSource = new DataSource(Splashscreen.this);
//
//            List<File> listFiles = Helper.getListFiles(new File(Constants.EXTERNAL_ROOT_FOLDER_SCDB));
//            List<String> listManpowerFileName = Helper.getListFileName(listFiles, Constants.SCDB_FILE_PREFIX.MANPOWER);
//
//            if (!aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() == 0) {
////                if (dataSource.getAllManpowerResources().isEmpty()) {
//                File importFolderChecked = new File(basepath + "/"
//                        + Constants.SCDB_LOOKUP_FOLDER_NAME + "/"
//                        + Constants.MANPOWER_SCDB_FILE_NAME);
//                if (!importFolderChecked.exists()) {
//                    Helper.copyFileAssetSCDB(Constants.MANPOWER_SCDB_FILE_NAME,
//                            Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//                }
////                }
//                Helper.copyFileAssetSCDB(Constants.MANPOWER_SCDB_FILE_NAME,
//                        Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//
//                if (importManpower.exists()) {
//                    dataSource.open();
//                    dataSource.deleteAllDataObserver();
//
//                    String pathPanel = Environment.getExternalStorageDirectory()
//                            .getAbsolutePath();
//                    pathPanel += "/" + Constants.ROOT_FOLDER_NAME + "/"
//                            + Constants.SCDB_LOOKUP_FOLDER_NAME + "/"
//                            + Constants.MANPOWER_SCDB_FILE_NAME;
//
//                    List<String[]> readResultPanel = CSVHelper
//                            .readCSVFromPath(pathPanel);
//
//                    int i = 0;
//
//                    for (String[] strings : readResultPanel) {
//                        if (i > 0 && strings.length > 1) {
//                            ManpowerModel om = new ManpowerModel();
//                            om.setIdno(strings[1]);
//                            om.setName(strings[0]);
//                            om.setOrg(strings[5]);
//                            om.setTitle(strings[2]);
//                            om.setCompany(strings[6]);
//                            om.setDivisi(strings[5]);
//                            om.setDepartment(strings[3]);
//                            om.setSection(strings[4]);
//
//                            dataSource.createDataObserver(om);
//                            trueManpower = true;
//                            Log.d("OBS2", om.getName());
//                        }
//                        i++;
//                    }
////                    importManpower.delete();
//                }
//                Constants.setObserverDictionary(dataSource
//                        .getAllManpowerResources());
//
//                dataSource.close();
//            } else if (aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() > 1) {
//                List<String[]> readResultPanel = CSVHelper
//                        .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                + Helper.getLatestFileName(listManpowerFileName));
//
//                dataSource.open();
//                dataSource.deleteAllDataObserver();
//
//                int i = 0;
//
//                for (String[] strings : readResultPanel) {
//                    if (i > 0 && strings.length >= 6) {
//                        ManpowerModel om = new ManpowerModel();
//                        om.setIdno(strings[1]);
//                        om.setName(strings[0]);
//                        om.setOrg(strings[5]);
//                        om.setTitle(strings[2]);
//                        om.setCompany(strings[6]);
//                        om.setDivisi(strings[5]);
//                        om.setDepartment(strings[3]);
//                        om.setSection(strings[4]);
//
//                        dataSource.createDataObserver(om);
//                        trueManpower = true;
//
//                        Log.d("OBS2", om.getName());
//                    } else {
//                        trueManpower = false;
//                    }
//                    i++;
//
//                    if (strings.length >= 6 && i == readResultPanel.size()) {
//                        Helper.deleteUnusedFile(listManpowerFileName);
//                    }
//                }
////                    importManpower.delete();
//
//                Constants.setObserverDictionary(dataSource
//                        .getAllManpowerResources());
//
//                dataSource.close();
//
//            } else if (!aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() > 1) {
//                List<String[]> readResultPanel = CSVHelper
//                        .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                + Helper.getLatestFileName(listManpowerFileName));
//
//                dataSource.open();
//                dataSource.deleteAllDataObserver();
//
//                int i = 0;
//
//                for (String[] strings : readResultPanel) {
//                    if (i > 0 && strings.length >= 6) {
//                        ManpowerModel om = new ManpowerModel();
//                        om.setIdno(strings[1]);
//                        om.setName(strings[0]);
//                        om.setOrg(strings[5]);
//                        om.setTitle(strings[2]);
//                        om.setCompany(strings[6]);
//                        om.setDivisi(strings[5]);
//                        om.setDepartment(strings[3]);
//                        om.setSection(strings[4]);
//
//                        dataSource.createDataObserver(om);
//                        trueManpower = true;
//
//                        Log.d("OBS2", om.getName());
//                    } else {
//                        trueManpower = false;
//                    }
//                    i++;
//
//                    if (strings.length >= 6 && i == readResultPanel.size()) {
//                        Helper.deleteUnusedFile(listManpowerFileName);
//                    }
//                }
////                    importManpower.delete();
//
//                Constants.setObserverDictionary(dataSource
//                        .getAllManpowerResources());
//
//                dataSource.close();
//
//            } else if (!aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() > 0) {
//                List<String[]> readResultPanel = CSVHelper
//                        .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                + Helper.getLatestFileName(listManpowerFileName));
//
//                dataSource.open();
//                dataSource.deleteAllDataObserver();
//
//                int i = 0;
//
//                for (String[] strings : readResultPanel) {
//                    if (i > 0 && strings.length >= 6) {
//                        ManpowerModel om = new ManpowerModel();
//                        om.setIdno(strings[1]);
//                        om.setName(strings[0]);
//                        om.setOrg(strings[5]);
//                        om.setTitle(strings[2]);
//                        om.setCompany(strings[6]);
//                        om.setDivisi(strings[5]);
//                        om.setDepartment(strings[3]);
//                        om.setSection(strings[4]);
//
//                        dataSource.createDataObserver(om);
//                        trueManpower = true;
//
//                        Log.d("OBS2", om.getName());
//                    } else {
//                        trueManpower = false;
//                    }
//                    i++;
//
//                    if (strings.length >= 6 && i == readResultPanel.size()) {
//                        Helper.deleteUnusedFile(listManpowerFileName);
//                    }
//                }
////                    importManpower.delete();
//
//                Constants.setObserverDictionary(dataSource
//                        .getAllManpowerResources());
//
//                dataSource.close();
//
//            } else if (aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() > 0) {
//                List<String[]> readResultPanel = CSVHelper
//                        .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                + Helper.getLatestFileName(listManpowerFileName));
//
//                dataSource.open();
//                dataSource.deleteAllDataObserver();
//
//                int i = 0;
//
//                for (String[] strings : readResultPanel) {
//                    if (i > 0 && strings.length >= 6) {
//                        ManpowerModel om = new ManpowerModel();
//                        om.setIdno(strings[1]);
//                        om.setName(strings[0]);
//                        om.setOrg(strings[5]);
//                        om.setTitle(strings[2]);
//                        om.setCompany(strings[6]);
//                        om.setDivisi(strings[5]);
//                        om.setDepartment(strings[3]);
//                        om.setSection(strings[4]);
//
//                        dataSource.createDataObserver(om);
//                        trueManpower = true;
//
//                        Log.d("OBS2", om.getName());
//                    } else {
//                        trueManpower = false;
//                    }
//                    i++;
//
//                    if (strings.length >= 6 && i == readResultPanel.size()) {
//                        Helper.deleteUnusedFile(listManpowerFileName);
//                    }
//                }
////                    importManpower.delete();
//
//                Constants.setObserverDictionary(dataSource
//                        .getAllManpowerResources());
//
//                dataSource.close();
//
//            } else if (aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() < 1) {
//                if (!importManpower.exists()) {
//                    Helper.copyFileAssetSCDB(Constants.MANPOWER_SCDB_FILE_NAME,
//                            Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//                }
//
//                if (isValidPathAndFolderName(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                        + Helper.getLatestFileName(listManpowerFileName))) {
//
//                    List<String[]> readResultPanel = CSVHelper
//                            .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                    + Helper.getLatestFileName(listManpowerFileName));
//
//                    dataSource.open();
//                    dataSource.deleteAllDataObserver();
//
//                    int i = 0;
//
//                    for (String[] strings : readResultPanel) {
//                        if (i > 0 && strings.length > 1) {
//                            ManpowerModel om = new ManpowerModel();
//                            om.setIdno(strings[1]);
//                            om.setName(strings[0]);
//                            om.setOrg(strings[5]);
//                            om.setTitle(strings[2]);
//                            om.setCompany(strings[6]);
//                            om.setDivisi(strings[5]);
//                            om.setDepartment(strings[3]);
//                            om.setSection(strings[4]);
//
//                            dataSource.createDataObserver(om);
//                            trueManpower = true;
//
//                            Log.d("OBS2", om.getName());
//                        } else {
//                        trueManpower = false;
//                    }
//                        i++;
//
//                        if (strings.length >= 6 && i == readResultPanel.size()) {
//                            Helper.deleteUnusedFile(listManpowerFileName);
//                        }
//                    }
////                        importManpower.delete();
//
//                    Constants.setObserverDictionary(dataSource
//                            .getAllManpowerResources());
//
//                    dataSource.close();
//                }
//            } else if (aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() < 1) {
//                if (!importManpower.exists()) {
//                    Helper.copyFileAssetSCDB(Constants.MANPOWER_SCDB_FILE_NAME,
//                            Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//                }
//
//                if (isValidPathAndFolderName(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                        + Helper.getLatestFileName(listManpowerFileName))) {
//
//                    List<String[]> readResultPanel = CSVHelper
//                            .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                    + Helper.getLatestFileName(listManpowerFileName));
//
//                    dataSource.open();
//                    dataSource.deleteAllDataObserver();
//
//                    int i = 0;
//
//                    for (String[] strings : readResultPanel) {
//                        if (i > 0 && strings.length > 1) {
//                            ManpowerModel om = new ManpowerModel();
//                            om.setIdno(strings[1]);
//                            om.setName(strings[0]);
//                            om.setOrg(strings[5]);
//                            om.setTitle(strings[2]);
//                            om.setCompany(strings[6]);
//                            om.setDivisi(strings[5]);
//                            om.setDepartment(strings[3]);
//                            om.setSection(strings[4]);
//
//                            dataSource.createDataObserver(om);
//                            trueManpower = true;
//
//                            Log.d("OBS2", om.getName());
//                        }
//                        i++;
//                    }
////                        importManpower.delete();
//
//                    Constants.setObserverDictionary(dataSource
//                            .getAllManpowerResources());
//
//                    dataSource.close();
//                }
//            } else if (!aController.getManpowerModel().isContentOfTableExist(Splashscreen.this) && listManpowerFileName.size() < 1) {
//                if (!importManpower.exists()) {
//                    Helper.copyFileAssetSCDB(Constants.MANPOWER_SCDB_FILE_NAME,
//                            Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//                }
//
//                if (isValidPathAndFolderName(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                        + Helper.getLatestFileName(listManpowerFileName))) {
//
//                    List<String[]> readResultPanel = CSVHelper
//                            .readCSVFromPath(Constants.EXTERNAL_ROOT_FOLDER_SCDB
//                                    + Helper.getLatestFileName(listManpowerFileName));
//
//                    dataSource.open();
//                    dataSource.deleteAllDataObserver();
//
//                    int i = 0;
//
//                    for (String[] strings : readResultPanel) {
//                        if (i > 0 && strings.length > 1) {
//                            ManpowerModel om = new ManpowerModel();
//                            om.setIdno(strings[1]);
//                            om.setName(strings[0]);
//                            om.setOrg(strings[5]);
//                            om.setTitle(strings[2]);
//                            om.setCompany(strings[6]);
//                            om.setDivisi(strings[5]);
//                            om.setDepartment(strings[3]);
//                            om.setSection(strings[4]);
//
//                            dataSource.createDataObserver(om);
//                            trueManpower = true;
//
//                            Log.d("OBS2", om.getName());
//                        }
//                        i++;
//                    }
////                        importManpower.delete();
//
//                    Constants.setObserverDictionary(dataSource
//                            .getAllManpowerResources());
//
//                    dataSource.close();
//                }
//            }


//            dataSource.close();

//            File importLookUp = new File(basepath + "/"
//                    + Constants.APP_FOLDER_NAME + "/"
//                    + Constants.IMPORT_FOLDER_NAME + "/"
//                    + Constants.LOOKUP_FILE_NAME);
//            if (dataSource.getAllLookUp().isEmpty()) {
//                File importFolderChecked = new File(basepath + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.IMPORT_FOLDER_NAME + "/"
//                        + Constants.LOOKUP_FILE_NAME);
//                if (!importFolderChecked.exists()) {
//                    Helper.copyFileAsset(Constants.LOOKUP_FILE_NAME,
//                            Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//                }
//            }
//            Helper.copyFileAsset(Constants.LOOKUP_FILE_NAME,
//                    Constants.IMPORT_FOLDER_NAME, Splashscreen.this);
//            if (importLookUp.exists()) {
//                dataSource.deleteAllDataLookUp();
//
//                String pathPanel = Environment.getExternalStorageDirectory()
//                        .getAbsolutePath();
//                pathPanel += "/" + Constants.ROOT_FOLDER_NAME + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.IMPORT_FOLDER_NAME + "/"
//                        + Constants.LOOKUP_FILE_NAME;
//
//                List<String[]> readResultPanel = CSVHelper
//                        .readCSVFromPath(pathPanel);
//
//                int i = 0;
//
//                for (String[] strings : readResultPanel) {
//                    if (i > 0 && strings.length > 1) {
//                        LookUpModel om = new LookUpModel();
//                        om.setPotential_risk(strings[0]);
//                        om.setRating(strings[1]);
//
//                        dataSource.createLookUp(om);
//                        Log.d("OBS2", om.getPotential_risk());
//                    }
//                    i++;
//                }
//                importLookUp.delete();
//            }
//            Constants.setLookUpDictionary(dataSource
//                    .getAllLookUp());
//            dataSource.close();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (Helper.progressDialog != null)
                Helper.progressDialog.dismiss();

            Intent intent = getIntent();
            if (intent.getExtras() != null
                    && intent.getExtras().getBoolean("isOtherApp")) {

            } else {
                finish();
                Intent intentField = new Intent(Splashscreen.this,
                        MainMenu.class);
                intentField.putExtra("isOtherApp", true);
                intentField.putExtra("trueManpower", trueManpower);
                startActivity(intentField);
            }
        }

        private ArrayList<String> getImportPathList() {
            ArrayList<String> importFilePathList = new ArrayList<String>();

            // COMPANY
            String importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.COMPANY_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // BUSINESS UNIT
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/" + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.BUSINESS_UNIT_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // DEPARTMENT
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.DEPARTMENT_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // SECTION
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.SECTION_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // POTENTIAL RISK
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.POTENTIAL_RISK_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // RATING
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.RATING_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            // AVAILABILITY
            importFilePath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath();
            importFilePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/"
                    + Constants.IMPORT_FOLDER_NAME + "/"
                    + Constants.AVAILABILITY_FILE_NAME;
            if (new File(importFilePath).exists()) {
                List<String[]> readResult = CSVHelper
                        .readCSVFromPath(importFilePath);
                if (readResult != null) {
                    if (readResult.size() > 0) {
                        importFilePathList.add(importFilePath);
                    }
                }
            }

            return importFilePathList;
        }

        /**
         * check whether is new data or not
         **/

        private void checkAppData() {
            final ArrayList<String> importPathList = getImportPathList();
            if (importPathList.size() > 0) {
                for (String filePath : importPathList) {
                    importDataFromFile(filePath);
                    new File(filePath).delete();
                }
            }
            DataSource ds = new DataSource(Splashscreen.this);
            ds.open();

            Constants.setCompanyDictionary(ds.getAllCompany());
//            Constants.setPOTENTIAL_RISK_DICTIONARY(ds.getAllPotentialRisk());
            Constants.setRATING_DICTIONARY(ds.getAllRating());
            Constants.setAVAILABILITY_DICTIONARY(ds.getAllAvailibity());

            ds.close();
        }

        private void importDataFromFile(String filePath) {
            if (filePath != null && filePath.length() > 0) {
                if (filePath.contains("/")
                        && filePath.substring(filePath.length() - 4,
                        filePath.length()).equalsIgnoreCase(".csv")) {
                    String fileName = filePath.substring(
                            filePath.lastIndexOf("/") + 1, filePath.length());
                    if (fileName.equalsIgnoreCase(Constants.COMPANY_FILE_NAME)) {
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataCompany();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }
                                    CustomSpinnerItem data = new CustomSpinnerItem();
                                    data.lookup = strings[0];
                                    data.description = strings[1];

                                    dataSource.createCompany(data);
                                }
                                dataSource.close();
                            }
                        }
                    } else if (fileName
                            .equalsIgnoreCase(Constants.BUSINESS_UNIT_FILE_NAME)) {
                        // STATION STATUS
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataBusiness();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }
                                    CustomSpinnerItem data = new CustomSpinnerItem();
                                    data.lookup = strings[0];
                                    data.description = strings[1];
                                    data.ref = strings[2];

                                    dataSource.createBusinessUnit(data);
                                }
                                dataSource.close();
                            }
                        }
                    } else if (fileName
                            .equalsIgnoreCase(Constants.DEPARTMENT_FILE_NAME)) {
                        // STATION STATUS
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataDepartment();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }
                                    CustomSpinnerItem data = new CustomSpinnerItem();
                                    data.lookup = strings[0];
                                    data.description = strings[1];
                                    data.ref = strings[2];

                                    dataSource.createDepartment(data);
                                }
                                dataSource.close();
                            }
                        }
                    } else if (fileName
                            .equalsIgnoreCase(Constants.SECTION_FILE_NAME)) {
                        // STATION STATUS
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataSection();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }
                                    CustomSpinnerItem data = new CustomSpinnerItem();
                                    data.lookup = strings[0];
                                    data.description = strings[1];
                                    data.ref = strings[2];

                                    dataSource.createSection(data);
                                }
                                dataSource.close();
                            }
                        }
//                    } else if (fileName
//                            .equalsIgnoreCase(Constants.POTENTIAL_RISK_FILE_NAME)) {
//                        // STATION STATUS
//                        if (new File(filePath).exists()) {
//                            List<String[]> readResult = CSVHelper
//                                    .readCSVFromPath(filePath);
//                            if (readResult != null) {
//                                DataSource dataSource = new DataSource(
//                                        Splashscreen.this);
//                                dataSource.open();
//                                dataSource.deleteAllDataPRisk();
//                                int skippedData = 0;
//                                for (String[] strings : readResult) {
//                                    if (skippedData < 1) {
//                                        skippedData++;
//                                        continue;
//                                    }
//
//                                    dataSource.createPRisk(strings[0]);
//                                }
//                                dataSource.close();
//                            }
//                        }
                    } else if (fileName
                            .equalsIgnoreCase(Constants.RATING_FILE_NAME)) {
                        // STATION STATUS
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataRating();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }

                                    dataSource.createRating(strings[0]);
                                }
                                dataSource.close();
                            }
                        }
                    } else if (fileName
                            .equalsIgnoreCase(Constants.AVAILABILITY_FILE_NAME)) {
                        // STATION STATUS
                        if (new File(filePath).exists()) {
                            List<String[]> readResult = CSVHelper
                                    .readCSVFromPath(filePath);
                            if (readResult != null) {
                                DataSource dataSource = new DataSource(
                                        Splashscreen.this);
                                dataSource.open();
                                dataSource.deleteAllDataAvailability();
                                int skippedData = 0;
                                for (String[] strings : readResult) {
                                    if (skippedData < 1) {
                                        skippedData++;
                                        continue;
                                    }

                                    dataSource.createAvailabilty(strings[0]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
