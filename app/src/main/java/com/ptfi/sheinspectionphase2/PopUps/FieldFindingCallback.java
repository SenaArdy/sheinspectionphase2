package com.ptfi.sheinspectionphase2.PopUps;

import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;

/**
 * Created by senaardyputra on 11/12/16.
 */

public interface FieldFindingCallback {
    void onFindingSubmitted(FieldFindingsModel model);
}
