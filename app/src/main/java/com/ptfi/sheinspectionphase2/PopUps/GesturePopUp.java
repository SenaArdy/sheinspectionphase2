package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.Dialog;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by senaardyputra on 11/11/16.
 */

public class GesturePopUp {

    public static void show(final Activity context, final GesturePopUpCallback callback){
        if(context != null){
            context.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.gesture_layout);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                    final GestureOverlayView gesture = (GestureOverlayView) dialog.findViewById(R.id.gestures);
                    ButtonRectangle button_save = (ButtonRectangle) dialog.findViewById(R.id.save_button);
                    ButtonRectangle button_clear = (ButtonRectangle) dialog.findViewById(R.id.clear_button);
                    ButtonRectangle button_close = (ButtonRectangle) dialog.findViewById(R.id.close_button);

                    button_close.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });

                    button_clear.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            gesture.cancelClearAnimation();
                            gesture.clear(true);
                        }
                    });

                    button_save.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            Bitmap bitmap = null;
                            try {
                                Bitmap gestureImg = gesture.getGesture().toBitmap(60, 45,
                                        8, Color.BLACK);

                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                gestureImg.compress(Bitmap.CompressFormat.PNG, 100, bos);
                                byte[] bArray = bos.toByteArray();

                                ByteArrayInputStream imageStreamClient = new ByteArrayInputStream(bArray);
                                bitmap = BitmapFactory.decodeStream(imageStreamClient);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(callback != null){
                                callback.onBitmapSaved(bitmap);
                            }

                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });
        }
    }
}
