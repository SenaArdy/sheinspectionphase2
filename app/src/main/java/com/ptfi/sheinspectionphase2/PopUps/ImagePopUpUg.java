package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by GO on 12/27/2016.
 */
public class ImagePopUpUg {
    private static ImageView picture;

    public static final int IMAGE_POP_UP_TAKE_PICTURE_CODE = 24;
    public static final int IMAGE_POP_UP_GALLERY_CODE = 25;

    private static String imagePath = null;
    private static String deletedImagePath = null;
    private static InputStream originalImage= null;
    private static String temporaryDir = null;

    public static void imagePopUpUg(final Activity mActivity, final int reference, final String photoPath, final String comments, final String date,
                                    final String projectOwner, final ImagePopUpUgCallback callback){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_foto);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                ButtonRectangle clearAllButton = (ButtonRectangle) dialog.findViewById(R.id.deleteBtn);
                ImageView galleryButton = (ImageView) dialog.findViewById(R.id.gallery_icon);
                ImageView takePictureButton = (ImageView) dialog.findViewById(R.id.camera_icon);
                picture = (ImageView) dialog.findViewById(R.id.picture);
                ButtonRectangle cancelButton = (ButtonRectangle) dialog.findViewById(R.id.popupCancelBtn);
                ButtonRectangle submitButton = (ButtonRectangle) dialog.findViewById(R.id.popupSubmitBtn);
                final EditText comment = (EditText) dialog.findViewById(R.id.commentET);

                if (!photoPath.equals("") && !comment.equals("")) {
                    Helper.setPic(picture, photoPath);
                    comment.setText(comments);
                }

                galleryButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        mActivity.startActivityForResult(intent,
                                IMAGE_POP_UP_GALLERY_CODE);
                    }
                });

                takePictureButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String tempDir = Constants.tempPhotosDir;
                        String currentTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                        File exportFolder = new File(tempDir);
                        if (!exportFolder.exists()) {
                            //create specific folder for taken image unless it's already created
                            exportFolder.mkdirs();
                        }

                        String photoName = currentTime+".jpg";

                        Intent intent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);

                        Helper.fileUri = Uri.fromFile(new File(tempDir,
                                photoName));

                        intent.putExtra(
                                MediaStore.EXTRA_OUTPUT,
                                Helper.fileUri);
                        mActivity.startActivityForResult(intent,
                                IMAGE_POP_UP_TAKE_PICTURE_CODE);
                    }
                });

                picture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(imagePath != null) {
                            showPopupPreview(mActivity, imagePath);
                        }
                    }
                });

                clearAllButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        comment.setText("");
                        imagePath = null;
                        picture.setImageResource(R.drawable.no_image);
                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PhotoModel model = new PhotoModel();
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        model.setComment(comment.getText().toString());
                        model.setDate(date);
                        model.setPic(projectOwner);
                        model.setFotoPath(imagePath);

                        ds.insertPhotoData(model, reference);

                        callback.onImageSubmitted();

                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public static void showPopupPreview(final Activity activity,
                                        final String filePath) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_preview_image);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                ImageView iv = (ImageView) dialog
                        .findViewById(R.id.imagePreview);

                Helper.setPic(iv, filePath);

                title.setText(filePath.substring(filePath.lastIndexOf("/") + 1));

                ButtonRectangle closeBtn = (ButtonRectangle) dialog
                        .findViewById(R.id.closeBtn);
                closeBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static ImageView getPicture() {
        return picture;
    }

    public static void setImagePath(String imagePath) {
        ImagePopUpUg.imagePath = imagePath;
    }
}


