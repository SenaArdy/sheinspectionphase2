package com.ptfi.sheinspectionphase2.PopUps;

/**
 * Created by senaardyputra on 11/11/16.
 */

public interface ImagePopUpCallback {
    public void onImageSubmitted();
}
