package com.ptfi.sheinspectionphase2.PopUps;

import android.graphics.Bitmap;

public interface GesturePopUpCallback {
	public void onBitmapSaved(Bitmap bitmap);
}
