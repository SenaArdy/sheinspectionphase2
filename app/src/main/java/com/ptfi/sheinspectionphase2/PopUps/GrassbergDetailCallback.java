package com.ptfi.sheinspectionphase2.PopUps;

/**
 * Created by senaardyputra on 11/11/16.
 */

public interface GrassbergDetailCallback {
    public void onSubmitted(String comment, String date, String name);
}
