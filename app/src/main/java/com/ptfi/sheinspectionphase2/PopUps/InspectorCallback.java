package com.ptfi.sheinspectionphase2.PopUps;

/**
 * Created by senaardyputra on 4/21/17.
 */

public interface InspectorCallback {

    public void onChange();
}
