package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Adapter.ListAboutAdapter;
import com.ptfi.sheinspectionphase2.Adapter.ListChangeLogAdapter;
import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 3/15/17.
 */

public class PopUp {

    public static void showAbout(final Activity activity) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_about);
                ListView listAbout = (ListView) dialog
                        .findViewById(R.id.listAbout);

                String versionName;
                try {
                    versionName = activity.getPackageManager().getPackageInfo(
                            activity.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    versionName = "version name cannot be detected";
                }

				/* this array list is consist of title and content */
                ArrayList<String[]> listAboutContent = new ArrayList<String[]>();
                listAboutContent.add(new String[]{"Version", "", " : " + versionName});
                listAboutContent.add(new String[]{"Release Date", "", " : " +
                        "September 2018"});
                listAboutContent
                        .add(new String[]{"About",
                                "Mobile Application for SHE Inspection is collaboration project between", ""});
                listAboutContent
                        .add(new String[]{
                                "PTFI - Geo Engineering",
                                "Wahyu Sunyoto, Anton Perdana, Munsi Nasution, Iwan Sriyanto, Surya Nugraha, Akhwan Muntaha, Heri Setiono, Persi Laseria.", ""});
                listAboutContent
                        .add(new String[]{
                                "ENJ",
                                "Aditya Pringgoprawiro, Sena Putra, Deni Pria, Bacharudin A.F, Tommy Kurniawan, Semara Rasmaranti.", ""});

                ListAboutAdapter customAdapter = new ListAboutAdapter(activity,
                        listAboutContent);

                listAbout.setAdapter(customAdapter);

                int width;

                WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

                DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
                width = displayMetrics.widthPixels;

                params.width = (int) (width * 0.85);
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                dialog.getWindow().setAttributes(params);

                dialog.show();
            }
        });
    }

    public static void showChangelogs(final Activity activity) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_about);
                ListView listAbout = (ListView) dialog
                        .findViewById(R.id.listAbout);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText("CHANGELOGS");

                String versionName;
                try {
                    versionName = activity.getPackageManager().getPackageInfo(
                            activity.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    versionName = "version name cannot be detected";
                }

				/* this array list is consist of title and content */
                ArrayList<String[]> listAboutContent = new ArrayList<String[]>();
                listAboutContent.add(new String[]{"Version", versionName});
                listAboutContent.add(new String[]{
                        "Global",
                        "Version 2.3 : \n"
                                + "- Current UI support to small mobile device.\n"
                                + "- Udate Manpower (September 14, 2018)."});

                ListChangeLogAdapter customAdapter = new ListChangeLogAdapter(activity,
                        listAboutContent);

                listAbout.setAdapter(customAdapter);

                int width;

                WindowManager.LayoutParams params = dialog.getWindow().getAttributes();

                DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
                width = displayMetrics.widthPixels;

                params.width = (int) (width * 0.85);
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                dialog.getWindow().setAttributes(params);

                dialog.show();
            }
        });
    }
}
