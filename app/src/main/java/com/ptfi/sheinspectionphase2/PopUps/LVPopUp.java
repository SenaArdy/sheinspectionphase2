package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.Dialog;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.LVDailyModel;
import com.ptfi.sheinspectionphase2.Models.LVDamageModel;
import com.ptfi.sheinspectionphase2.Models.LVWeeklyModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

/**
 * Created by senaardyputra on 11/16/16.
 */

public class LVPopUp {

    public static void damagePopUp(final Activity mActivity, final String lvNo, final String pic, final String dateIns) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pop_up_lv_damage);
                dialog.setCanceledOnTouchOutside(false);

                final EditText dateET, itemET, driverET, actionET;
                final TextInputLayout dateTL, itemTL, driverTL, actionTL;

                ButtonRectangle clearAllBtn, cancelBtn, submitBtn;

                dateET = (EditText) dialog.findViewById(R.id.dateET);
                itemET = (EditText) dialog.findViewById(R.id.itemET);
                driverET = (EditText) dialog.findViewById(R.id.driverET);
                actionET = (EditText) dialog.findViewById(R.id.actionET);

                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                itemTL = (TextInputLayout) dialog.findViewById(R.id.itemTL);
                driverTL = (TextInputLayout) dialog.findViewById(R.id.driverTL);
                actionTL = (TextInputLayout) dialog.findViewById(R.id.actionTL);

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateET.setText("");
                        itemET.setText("");
                        driverET.setText("");
                        actionET.setText("");
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        LVDamageModel model = new LVDamageModel();
                        model.setLvNo(lvNo);
                        model.setPic(pic);
                        model.setDateIns(dateIns);
                        model.setDate(dateET.getText().toString());
                        model.setItemDamage(itemET.getText().toString());
                        model.setDate(dateET.getText().toString());
                        model.setAction(actionET.getText().toString());

                        ds.insertLVDamage(model);

                        ds.close();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static void weeklyPopUp(final Activity mActivity, final String lvNo, final String pic, final String dateIns) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pop_up_lv_weekly);
                dialog.setCanceledOnTouchOutside(false);

                final EditText dateET, driverET, supervisorET;
                final TextInputLayout dateTL, supervisorTL, driverTL;

                final MultiStateToggleButton insp1, insp2, insp3, insp4;

                ButtonRectangle clearAllBtn, cancelBtn, submitBtn;

                dateET = (EditText) dialog.findViewById(R.id.dateET);
                driverET = (EditText) dialog.findViewById(R.id.driverET);
                supervisorET = (EditText) dialog.findViewById(R.id.supervisorET);

                insp1 = (MultiStateToggleButton) dialog.findViewById(R.id.insp1);
                insp2 = (MultiStateToggleButton) dialog.findViewById(R.id.insp2);
                insp3 = (MultiStateToggleButton) dialog.findViewById(R.id.insp3);
                insp4 = (MultiStateToggleButton) dialog.findViewById(R.id.insp4);

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateET.setText("");
                        driverET.setText("");
                        supervisorET.setText("");

                        insp1.setValue(-1);
                        insp2.setValue(-1);
                        insp3.setValue(-1);
                        insp4.setValue(-1);
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        LVWeeklyModel model = new LVWeeklyModel();
                        model.setDateIns(dateIns);
                        model.setPic(pic);
                        model.setLvNo(lvNo);
                        model.setDate(dateET.getText().toString());
                        model.setDriverId(driverET.getText().toString());
                        model.setSuperVisor(supervisorET.getText().toString());
                        if(insp1.getValue() == 0) {
                            model.setInsp1("Good");
                        } else {
                            model.setInsp1("Damage");
                        }

                        if(insp2.getValue() == 0) {
                            model.setInsp2("Good");
                        } else {
                            model.setInsp2("Damage");
                        }

                        if(insp3.getValue() == 0) {
                            model.setInsp3("Good");
                        } else {
                            model.setInsp3("Damage");
                        }

                        if(insp4.getValue() == 0) {
                            model.setInsp4("Good");
                        } else {
                            model.setInsp4("Damage");
                        }

                        ds.insertLVWeekly(model);

                        ds.close();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static void dailyPopUp(final Activity mActivity, final String lvNo, final String pic, final String dateIns) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pop_up_lv_daily);
                dialog.setCanceledOnTouchOutside(false);

                final EditText dateET, driverET, timeET;
                final TextInputLayout dateTL, timeTL, driverTL;

                final MultiStateToggleButton insp1, insp2, insp3, insp4, insp5, insp6, insp7, insp8, insp9, insp10,
                        insp11, insp12, insp13, insp14, insp15, insp16;

                ButtonRectangle clearAllBtn, cancelBtn, submitBtn;

                dateET = (EditText) dialog.findViewById(R.id.dateET);
                driverET = (EditText) dialog.findViewById(R.id.driverET);
                timeET = (EditText) dialog.findViewById(R.id.timeET);

                insp1 = (MultiStateToggleButton) dialog.findViewById(R.id.insp1);
                insp2 = (MultiStateToggleButton) dialog.findViewById(R.id.insp2);
                insp3 = (MultiStateToggleButton) dialog.findViewById(R.id.insp3);
                insp4 = (MultiStateToggleButton) dialog.findViewById(R.id.insp4);
                insp5 = (MultiStateToggleButton) dialog.findViewById(R.id.insp5);
                insp6 = (MultiStateToggleButton) dialog.findViewById(R.id.insp6);
                insp7 = (MultiStateToggleButton) dialog.findViewById(R.id.insp7);
                insp8 = (MultiStateToggleButton) dialog.findViewById(R.id.insp8);
                insp9 = (MultiStateToggleButton) dialog.findViewById(R.id.insp9);
                insp10 = (MultiStateToggleButton) dialog.findViewById(R.id.insp10);
                insp11 = (MultiStateToggleButton) dialog.findViewById(R.id.insp11);
                insp12 = (MultiStateToggleButton) dialog.findViewById(R.id.insp12);
                insp13 = (MultiStateToggleButton) dialog.findViewById(R.id.insp13);
                insp14 = (MultiStateToggleButton) dialog.findViewById(R.id.insp14);
                insp15 = (MultiStateToggleButton) dialog.findViewById(R.id.insp15);
                insp16 = (MultiStateToggleButton) dialog.findViewById(R.id.insp16);

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dateET.setText("");
                        driverET.setText("");
                        timeET.setText("");

                        insp1.setValue(-1);
                        insp2.setValue(-1);
                        insp3.setValue(-1);
                        insp4.setValue(-1);
                        insp5.setValue(-1);
                        insp6.setValue(-1);
                        insp7.setValue(-1);
                        insp8.setValue(-1);
                        insp9.setValue(-1);
                        insp10.setValue(-1);
                        insp11.setValue(-1);
                        insp12.setValue(-1);
                        insp13.setValue(-1);
                        insp14.setValue(-1);
                        insp15.setValue(-1);
                        insp16.setValue(-1);
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        LVDailyModel model = new LVDailyModel();
                        model.setDateIns(dateIns);
                        model.setPic(pic);
                        model.setLvNo(lvNo);
                        model.setDate(dateET.getText().toString());
                        model.setDriverId(driverET.getText().toString());
                        model.setTime(timeET.getText().toString());
                        if(insp1.getValue() == 0) {
                            model.setInsp1("Good");
                        } else {
                            model.setInsp1("Damage");
                        }

                        if(insp2.getValue() == 0) {
                            model.setInsp2("Good");
                        } else {
                            model.setInsp2("Damage");
                        }

                        if(insp3.getValue() == 0) {
                            model.setInsp3("Good");
                        } else {
                            model.setInsp3("Damage");
                        }

                        if(insp4.getValue() == 0) {
                            model.setInsp4("Good");
                        } else {
                            model.setInsp4("Damage");
                        }

                        if(insp5.getValue() == 0) {
                            model.setInsp5("Good");
                        } else {
                            model.setInsp5("Damage");
                        }

                        if(insp6.getValue() == 0) {
                            model.setInsp6("Good");
                        } else {
                            model.setInsp6("Damage");
                        }

                        if(insp7.getValue() == 0) {
                            model.setInsp7("Good");
                        } else {
                            model.setInsp7("Damage");
                        }

                        if(insp8.getValue() == 0) {
                            model.setInsp8("Good");
                        } else {
                            model.setInsp8("Damage");
                        }

                        if(insp9.getValue() == 0) {
                            model.setInsp9("Good");
                        } else {
                            model.setInsp9("Damage");
                        }

                        if(insp10.getValue() == 0) {
                            model.setInsp10("Good");
                        } else {
                            model.setInsp10("Damage");
                        }

                        if(insp11.getValue() == 0) {
                            model.setInsp11("Good");
                        } else {
                            model.setInsp11("Damage");
                        }

                        if(insp12.getValue() == 0) {
                            model.setInsp12("Good");
                        } else {
                            model.setInsp12("Damage");
                        }

                        if(insp13.getValue() == 0) {
                            model.setInsp13("Good");
                        } else {
                            model.setInsp13("Damage");
                        }

                        if(insp14.getValue() == 0) {
                            model.setInsp14("Good");
                        } else {
                            model.setInsp14("Damage");
                        }

                        if(insp15.getValue() == 0) {
                            model.setInsp15("Good");
                        } else {
                            model.setInsp15("Damage");
                        }

                        if(insp16.getValue() == 0) {
                            model.setInsp16("Good");
                        } else {
                            model.setInsp16("Damage");
                        }

                        ds.insertLVDaily(model);

                        ds.close();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }
}
