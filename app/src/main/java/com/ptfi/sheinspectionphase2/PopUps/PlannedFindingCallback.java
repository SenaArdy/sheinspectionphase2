package com.ptfi.sheinspectionphase2.PopUps;

import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;

/**
 * Created by GO on 12/10/2016.
 */
public interface PlannedFindingCallback {
    public void onPlannedSubmited(PlannedFindingsModel model);
}
