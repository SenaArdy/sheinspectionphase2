package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by GO on 12/10/2016.
 */
public class PlannedFindingsPopUp {
    public static final int IMAGE_POP_UP_TAKE_PICTURE_CODE = 24;
    public static final int IMAGE_POP_UP_GALLERY_CODE = 25;

    public static String imagePath = null;
    private static String deletedImagePath = null;
    private static InputStream originalImage = null;
    private static String temporaryDir = null;

    static EditText findingsET, actionET, dateET;
    static AutoCompleteTextView responsET;
    static TextInputLayout findingsTL, actionTL, responsTL, dateTL;
    static MultiStateToggleButton findingRisk;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    private static ImageView picture;

    public static boolean validation() {
        boolean status = true;

        if (findingsET.getText().toString().length() == 0) {
            status = false;
            findingsTL.setErrorEnabled(true);
            findingsTL.setError("Please fill finding first");
        }

        if (actionET.getText().toString().length() == 0) {
            status = false;
            actionTL.setErrorEnabled(true);
            actionTL.setError("Please fill action first");
        }

        if (responsET.getText().toString().length() == 0) {
            status = false;
            responsTL.setErrorEnabled(true);
            responsTL.setError("Please fill responsibility first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill date first");
        }

        return status;
    }

    public static void showPlannedFindingsPopUp(final Activity mActivity, final PlannedFindingsModel plannedFindingsModel, final PlannedFindingCallback callback) {
        mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        final Dialog dialog = new Dialog(mActivity);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.pop_up_planned_findings);
                                        dialog.setCanceledOnTouchOutside(false);
                                        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                                        ButtonRectangle clearAllBtn, submitBtn, cancelBtn;

                                        final Calendar c = Calendar.getInstance();
                                        selectedYear = c.get(Calendar.YEAR);
                                        selectedMonth = c.get(Calendar.MONTH);
                                        selectedDate = c.get(Calendar.DAY_OF_MONTH);
                                        DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                                                selectedDate);

                                        findingsTL = (TextInputLayout) dialog.findViewById(R.id.findingsTL);
                                        actionTL = (TextInputLayout) dialog.findViewById(R.id.actionTL);
                                        responsTL = (TextInputLayout) dialog.findViewById(R.id.responsTL);
                                        dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);

                                        findingsET = (EditText) dialog.findViewById(R.id.findingsET);
                                        actionET = (EditText) dialog.findViewById(R.id.actionET);
                                        findingRisk = (MultiStateToggleButton) dialog.findViewById(R.id.findingRisk);
                                        responsET = (AutoCompleteTextView) dialog.findViewById(R.id.responsET);
                                        final ArrayList<String> dataManpower = ManpowerModel.getManpowerName(mActivity);
                                        final ArrayList<String> dataManpowerID = ManpowerModel.getManpowerId(mActivity);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line,
                                                dataManpower);
                                        responsET.setThreshold(1);
                                        responsET.setAdapter(adapter);
                                        responsET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                String key = responsET.getText().toString();
                                                int pos = dataManpower.indexOf(key);

                                                responsET.setText(dataManpower.get(pos));
                                            }
                                        });
                                        dateET = (EditText) dialog.findViewById(R.id.dateET);
                                        dateET.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Helper.showDatePicker(v, (FragmentActivity) mActivity,
                                                        new DatePickerDialog.OnDateSetListener() {

                                                            @Override
                                                            public void onDateSet(DatePicker view, int year,
                                                                                  int monthOfYear, int dayOfMonth) {
                                                                selectedYear = year;
                                                                selectedMonth = monthOfYear;
                                                                selectedDate = dayOfMonth;

                                                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                                                dateET.setText(DataSingleton.getInstance()
                                                                        .getFormattedDate());
                                                            }
                                                        }, selectedYear, selectedMonth, selectedDate);
                                            }
                                        });

                                        findingsTL = (TextInputLayout) dialog.findViewById(R.id.findingsTL);
                                        actionTL = (TextInputLayout) dialog.findViewById(R.id.actionTL);
                                        responsTL = (TextInputLayout) dialog.findViewById(R.id.responsTL);
                                        dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);

                                        ImageView galleryButton = (ImageView) dialog.findViewById(R.id.gallery_icon);
                                        ImageView takePictureButton = (ImageView) dialog.findViewById(R.id.camera_icon);
                                        picture = (ImageView) dialog.findViewById(R.id.picture);

                                        clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                                        submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                                        cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);

                                        imagePath = null;

                                        findingsET.setText(plannedFindingsModel.getFindings());
                                        actionET.setText("");
                                        responsET.setText(plannedFindingsModel.getResponsible());
                                        dateET.setText(plannedFindingsModel.getDate());

                                        if (plannedFindingsModel.photoPath != null && !plannedFindingsModel.photoPath.equalsIgnoreCase("")) {
                                            Helper.setPic(picture, plannedFindingsModel.photoPath);
                                            imagePath = plannedFindingsModel.photoPath;
                                        }

                                        galleryButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Intent intent = new Intent(
                                                        Intent.ACTION_PICK,
                                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                                mActivity.startActivityForResult(intent,
                                                        IMAGE_POP_UP_GALLERY_CODE);

                                            }
                                        });

                                        takePictureButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                String tempDir = Constants.tempPhotosDir;
                                                String currentTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                                                File exportFolder = new File(tempDir);
                                                if (!exportFolder.exists()) {
                                                    //create specific folder of image unless it has been created
                                                    exportFolder.mkdirs();
                                                }

                                                String photoName = currentTime + ".jpg";
                                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                Helper.fileUri = Uri.fromFile(new File(tempDir, photoName));
                                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Helper.fileUri);
                                                mActivity.startActivityForResult(intent, IMAGE_POP_UP_TAKE_PICTURE_CODE);

                                            }
                                        });

                                        picture.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (imagePath != null) {
                                                    showPopupPreview(mActivity, imagePath);
                                                }
                                            }
                                        });

                                        clearAllBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                findingsET.setText("");
                                                actionET.setText("");
                                                responsET.setText("");
                                                dateET.setText("");
                                            }
                                        });

                                        submitBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (validation()) {
                                                    DataSource ds = new DataSource(mActivity);
                                                    ds.open();
                                                    PlannedFindingsModel model = new PlannedFindingsModel();
                                                    model.setDateIns(dateET.getText().toString());
                                                    model.setFindings(findingsET.getText().toString());
                                                    model.setComment(actionET.getText().toString());
                                                    if (findingRisk.getValue() == 0) {
                                                        model.setRisk("High");
                                                    } else if (findingRisk.getValue() == 1) {
                                                        model.setRisk("Med");
                                                    } else {
                                                        model.setRisk("Low");
                                                    }
                                                    model.setResponsible(responsET.getText().toString());
                                                    model.setPhotoPath(imagePath);
                                                    model.setCode(plannedFindingsModel.getCode());
                                                    model.setQuestionCode(plannedFindingsModel.getQuestionCode());
                                                    model.setInspectionCode(plannedFindingsModel.getInspectionCode());

                                                    ds.close();
                                                    callback.onPlannedSubmited(model);

                                                    dialog.dismiss();
                                                }
                                            }

                                        });

                                        cancelBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });

                                        dialog.show();
                                    }
                                }
        );

    }

    public static void showPlannedFindingsPopUp(final Activity mActivity, final boolean isUpdate, final String inspector, final String areaOwner,
                                                final String date, final String location, final int reference, final String findings, final String action, final String photoPath,
                                                final String respons, final String dateComplete, final String risk, final int code, final int questionCode,
                                                final int inspectionCode, final PlannedFindingCallback callback) {
        mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        final Dialog dialog = new Dialog(mActivity);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.pop_up_planned_findings);
                                        dialog.setCanceledOnTouchOutside(false);

                                        ButtonRectangle clearAllBtn, submitBtn, cancelBtn;

                                        final Calendar c = Calendar.getInstance();
                                        selectedYear = c.get(Calendar.YEAR);
                                        selectedMonth = c.get(Calendar.MONTH);
                                        selectedDate = c.get(Calendar.DAY_OF_MONTH);
                                        DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                                                selectedDate);

                                        findingsTL = (TextInputLayout) dialog.findViewById(R.id.findingsTL);
                                        actionTL = (TextInputLayout) dialog.findViewById(R.id.actionTL);
                                        responsTL = (TextInputLayout) dialog.findViewById(R.id.responsTL);
                                        dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);

                                        findingsET = (EditText) dialog.findViewById(R.id.findingsET);
                                        actionET = (EditText) dialog.findViewById(R.id.actionET);
                                        findingRisk = (MultiStateToggleButton) dialog.findViewById(R.id.findingRisk);
                                        responsET = (AutoCompleteTextView) dialog.findViewById(R.id.responsET);
                                        final ArrayList<String> dataManpower = ManpowerModel.getManpowerName(mActivity);
                                        final ArrayList<String> dataManpowerID = ManpowerModel.getManpowerId(mActivity);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line,
                                                dataManpower);
                                        responsET.setThreshold(1);
                                        responsET.setAdapter(adapter);
                                        responsET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                String key = responsET.getText().toString();
                                                int pos = dataManpower.indexOf(key);

                                                responsET.setText(dataManpower.get(pos));
                                            }
                                        });
                                        dateET = (EditText) dialog.findViewById(R.id.dateET);
                                        dateET.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Helper.showDatePicker(v, (FragmentActivity) mActivity,
                                                        new DatePickerDialog.OnDateSetListener() {

                                                            @Override
                                                            public void onDateSet(DatePicker view, int year,
                                                                                  int monthOfYear, int dayOfMonth) {
                                                                selectedYear = year;
                                                                selectedMonth = monthOfYear;
                                                                selectedDate = dayOfMonth;

                                                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                                                dateET.setText(DataSingleton.getInstance()
                                                                        .getFormattedDate());
                                                            }
                                                        }, selectedYear, selectedMonth, selectedDate);
                                            }
                                        });

                                        findingsTL = (TextInputLayout) dialog.findViewById(R.id.findingsTL);
                                        actionTL = (TextInputLayout) dialog.findViewById(R.id.actionTL);
                                        responsTL = (TextInputLayout) dialog.findViewById(R.id.responsTL);
                                        dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);

                                        ImageView galleryButton = (ImageView) dialog.findViewById(R.id.gallery_icon);
                                        ImageView takePictureButton = (ImageView) dialog.findViewById(R.id.camera_icon);
                                        picture = (ImageView) dialog.findViewById(R.id.picture);

                                        clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                                        submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                                        cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);

                                        imagePath = null;

                                        if (isUpdate == true) {
                                            findingsET.setText(findings);
                                            actionET.setText(action);
                                            responsET.setText(respons);
                                            dateET.setText(dateComplete);
                                            if (risk.equals("High")) {
                                                findingRisk.setValue(0);
                                            } else if (risk.equals("Med")) {
                                                findingRisk.setValue(1);
                                            } else {
                                                findingRisk.setValue(2);
                                            }

                                        }

                                        if (photoPath != null && !photoPath.equalsIgnoreCase("")) {
                                            Helper.setPic(picture, photoPath);
                                            imagePath = photoPath;
                                        }

                                        galleryButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Intent intent = new Intent(
                                                        Intent.ACTION_PICK,
                                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                                                mActivity.startActivityForResult(intent,
                                                        IMAGE_POP_UP_GALLERY_CODE);

                                            }
                                        });

                                        takePictureButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                String tempDir = Constants.tempPhotosDir;
                                                String currentTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                                                File exportFolder = new File(tempDir);
                                                if (!exportFolder.exists()) {
                                                    //create specific folder of image unless it has been created
                                                    exportFolder.mkdirs();
                                                }

                                                String photoName = currentTime + ".jpg";
                                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                Helper.fileUri = Uri.fromFile(new File(tempDir, photoName));
                                                intent.putExtra(MediaStore.EXTRA_OUTPUT, Helper.fileUri);
                                                mActivity.startActivityForResult(intent, IMAGE_POP_UP_TAKE_PICTURE_CODE);

                                            }
                                        });

                                        picture.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (imagePath != null) {
                                                    showPopupPreview(mActivity, imagePath);
                                                }
                                            }
                                        });

                                        clearAllBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                findingsET.setText("");
                                                actionET.setText("");
                                                responsET.setText("");
                                                dateET.setText("");
                                            }
                                        });

                                        submitBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (validation()) {
                                                    DataSource ds = new DataSource(mActivity);
                                                    ds.open();
                                                    PlannedFindingsModel model = new PlannedFindingsModel();
                                                    model.setInspector(inspector);
                                                    model.setAreaOwner(areaOwner);
                                                    model.setLocation(location);
                                                    model.setDate(date);
                                                    model.setDateIns(dateET.getText().toString());
                                                    model.setFindings(findingsET.getText().toString());
                                                    model.setComment(actionET.getText().toString());
                                                    if (findingRisk.getValue() == 0) {
                                                        model.setRisk("High");
                                                    } else if (findingRisk.getValue() == 1) {
                                                        model.setRisk("Med");
                                                    } else {
                                                        model.setRisk("Low");
                                                    }
                                                    model.setResponsible(responsET.getText().toString());
                                                    model.setPhotoPath(imagePath);
                                                    model.setCode(code);
                                                    model.setQuestionCode(questionCode);
                                                    model.setInspectionCode(inspectionCode);
                                                    ds.close();
                                                    callback.onPlannedSubmited(model);

                                                    dialog.dismiss();
                                                }
                                            }

                                        });

                                        cancelBtn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });

                                        dialog.show();
                                    }
                                }
        );

    }

    public static void showPopupPreview(final Activity activity, final String filePath) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_preview_image);
                TextView title = (TextView) dialog.findViewById(R.id.title);
                ImageView iv = (ImageView) dialog.findViewById(R.id.imagePreview);
                Helper.setPic(iv, filePath);
                title.setText(filePath.substring(filePath.lastIndexOf("/") + 1));
                ButtonRectangle closeBtn = (ButtonRectangle) dialog.findViewById(R.id.closeBtn);
                closeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });
    }

    public static ImageView getPicture() {
        return picture;
    }

    public static void setImagePath(String imagePath) {
        PlannedFindingsPopUp.imagePath = imagePath;
    }

}
