package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.R;

/**
 * Created by senaardyputra on 12/31/16.
 */

public class InspectorPopUp {

    public static void inspectorPopUps(final Activity mActivity, final int pageCode) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_inspector);
                dialog.setCanceledOnTouchOutside(false);
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                final AutoCompleteTextView nameET;
                final EditText idET, titleET, orgET;
                final ImageView signatureIV;
                LinearLayout signatureView, signatureLayout;
                final TextView signatureWarning;

                final Bitmap[] bitmapSignature = {null};

                nameET = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
                idET = (EditText) dialog.findViewById(R.id.idET);
                titleET = (EditText) dialog.findViewById(R.id.titleET);
                orgET = (EditText) dialog.findViewById(R.id.orgET);

                signatureIV = (ImageView) dialog.findViewById(R.id.signatureIV);

                signatureLayout = (LinearLayout) dialog.findViewById(R.id.signatureLayout);
                signatureView = (LinearLayout) dialog.findViewById(R.id.signatureView);
                signatureWarning = (TextView) dialog.findViewById(R.id.signatureWarning);

                signatureLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GesturePopUp.show(mActivity,
                                new GesturePopUpCallback() {

                                    @Override
                                    public void onBitmapSaved(Bitmap bitmap) {
                                        // TODO Auto-generated method stub
                                        if (bitmap != null) {
                                            signatureIV.setImageBitmap(bitmap);
                                            signatureWarning.setVisibility(View.GONE);
                                            bitmapSignature[0] = bitmap;
                                        } else {
                                            signatureIV.setImageBitmap(null);
                                            signatureWarning.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                    }
                });

                dialog.show();
            }

        });
    }
}
