package com.ptfi.sheinspectionphase2.PopUps;

/**
 * Created by senaardyputra on 11/17/16.
 */

public interface LVPopUpCallback {
    public void onSubmitted();
}
