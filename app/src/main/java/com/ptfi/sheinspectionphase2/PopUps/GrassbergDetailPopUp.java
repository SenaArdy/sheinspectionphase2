package com.ptfi.sheinspectionphase2.PopUps;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.GrsDetailModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by senaardyputra on 11/11/16.
 */

public class GrassbergDetailPopUp {

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    public static void grassbergDetailPopUp(final Activity mActivity, final boolean isUpdate, final String department, final String date, final String pic,
                                            final GrassbergDetailCallback callback) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_grsdetail);
                dialog.setCanceledOnTouchOutside(false);

                final TextView departmentTV, signatureWarning;
                ButtonRectangle clearAll, submitBtn, closeBtn;
                TextInputLayout nameTL, dateTL, commentTL;
                final EditText dateET, commentET;
                final AutoCompleteTextView nameET;
                final  ImageView signatureIV;
                LinearLayout signatureView, signatureLayout;

                final Calendar c = Calendar.getInstance();
                selectedYear = c.get(Calendar.YEAR);
                selectedMonth = c.get(Calendar.MONTH);
                selectedDate = c.get(Calendar.DAY_OF_MONTH);
                DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                        selectedDate);

                final Bitmap[] bitmapSignature = {null};

                departmentTV = (TextView) dialog.findViewById(R.id.departmentTV);
                signatureWarning = (TextView) dialog.findViewById(R.id.signatureWarning);

                nameTL = (TextInputLayout) dialog.findViewById(R.id.nameTL);
                dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
                commentTL = (TextInputLayout) dialog.findViewById(R.id.commentTL);

                nameET = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
                dateET = (EditText) dialog.findViewById(R.id.dateET);
                commentET = (EditText) dialog.findViewById(R.id.commentET);

                signatureIV = (ImageView) dialog.findViewById(R.id.signatureIV);

                signatureLayout = (LinearLayout) dialog.findViewById(R.id.signatureLayout);
                signatureView = (LinearLayout) dialog.findViewById(R.id.signatureView);

                clearAll = (ButtonRectangle) dialog.findViewById(R.id.clearAll);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                closeBtn = (ButtonRectangle) dialog.findViewById(R.id.closeBtn);

                departmentTV.setText(department);

                final ArrayList<String> dataManpower = ManpowerModel.getManpowerName(mActivity);
                final ArrayList<String> dataManpowerID = ManpowerModel.getManpowerId(mActivity);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line,
                        dataManpower);
                nameET.setThreshold(1);
                nameET.setAdapter(adapter);
                nameET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String key = nameET.getText().toString();
                        int pos = dataManpower.indexOf(key);

                        nameET.setText(dataManpower.get(pos));
                    }
                });

                dateET.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Helper.showDatePicker(v, (FragmentActivity) mActivity,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        selectedYear = year;
                                        selectedMonth = monthOfYear;
                                        selectedDate = dayOfMonth;

                                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                        dateET.setText(DataSingleton.getInstance()
                                                .getFormattedDate());
                                    }
                                }, selectedYear, selectedMonth, selectedDate);
                    }
                });

                if(isUpdate == true) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();
                    GrsDetailModel grsModel = ds.getLatestGRSDetail(department, date, pic);
                    ds.close();
                    nameET.setText(grsModel.getSigner());
                    dateET.setText(grsModel.getDate());
                    commentET.setText(grsModel.getComment());
                }

                signatureLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GesturePopUp.show(mActivity,
                                new GesturePopUpCallback() {

                                    @Override
                                    public void onBitmapSaved(Bitmap bitmap) {
                                        // TODO Auto-generated method stub
                                        if (bitmap != null) {
                                            signatureIV.setImageBitmap(bitmap);
                                            signatureWarning.setVisibility(View.GONE);
                                            bitmapSignature[0] = bitmap;
                                        } else {
                                            signatureIV.setImageBitmap(null);
                                            signatureWarning.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                    }
                });

                clearAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nameET.setText("");
                        commentET.setText("");
                        dateET.setText("");

                        // Signature
                        signatureIV.setImageBitmap(null);
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GrsDetailModel model = new GrsDetailModel();
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        model.setSigner(nameET.getText().toString());
                        model.setDate(dateET.getText().toString());
                        model.setComment(commentET.getText().toString());
                        model.setDepartment(department);
                        model.setPic(pic);
                        model.setDateIns(date);

                        // Save Signature
                        model.setSign(Helper.bitmapToString((BitmapDrawable) signatureIV
                                .getDrawable() != null ? ((BitmapDrawable) signatureIV
                                .getDrawable()).getBitmap() : null));

                        ds.insertGrsDetailData(model);

                        ds.close();
                        callback.onSubmitted(commentET.getText().toString(), dateET.getText().toString(), nameET.getText().toString());

                        dialog.dismiss();
                    }
                });

                closeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }
}
