package com.ptfi.sheinspectionphase2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ptfi.sheinspectionphase2.Fragments.FieldFragment;
import com.ptfi.sheinspectionphase2.Fragments.GrassbergFragment;
import com.ptfi.sheinspectionphase2.Fragments.NavigationDrawerFragment;
import com.ptfi.sheinspectionphase2.Fragments.QuickContactFragment;
import com.ptfi.sheinspectionphase2.Models.DrawerItem;
import com.ptfi.sheinspectionphase2.Models.MenuItemModel;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUp;
import com.ptfi.sheinspectionphase2.PopUps.PopUp;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.util.ArrayList;

import br.com.thinkti.android.filechooser.FileChooser;

/**
 * Created by senaardyputra on 1/14/17.
 */

public class GRSActivity extends ActionBarActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks {

    final Activity mActivity = GRSActivity.this;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar toolbar;

    private boolean isInformationActive = false;
    private boolean isHistoryActive = false;

    static GrassbergFragment grsFragment;

    private Helper.Lookup importType;
    private Helper.Lookup lookUp;

    private static final int FILE_SELECT_CODE = 12332;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grs);

        MenuItemModel.setInformationActive(false);
        MenuItemModel.setHistoryActive(false);

        if (savedInstanceState == null) {
            init();
        } else {
            finish();
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage(
                            getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }

    }

    private void showHome() {
        setActionBarTitle("Grassberg Inspection");
        GrassbergFragment home = new GrassbergFragment();
        mNavigationDrawerFragment.initMenu(new MenuItemModel());
        getFragmentManager().beginTransaction().replace(R.id.container, home)
                .commit();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mNavigationDrawerFragment != null)
            mNavigationDrawerFragment.getDrawerToggle().syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // fix android formatted title bug

        int id = item.getItemId();
        switch (id) {
            case R.id.action_about:
                PopUp.showAbout(mActivity);
                break;

            case R.id.action_changelogs:
                PopUp.showChangelogs(mActivity);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        initDrawer();

        // Set a toolbar which will replace the action bar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MenuItemModel.setInformationActive(false);
        MenuItemModel.setHistoryActive(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        if (!Helper.isHistory)
        showHome();
//        else
//            showHistory();
    }

    private void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(
                Html.fromHtml("<small><font color = #e0e0e0>" + title
                        + "</font><small>"));

    }

    private void initDrawer() {
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_drawer);

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(
                R.color.primary_dark_material_dark));

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        mNavigationDrawerFragment.initMenu(new MenuItemModel());
    }

    @Override
    public void onMenuSelected(DrawerItem item) {
        if (item.getMenu() == DrawerItem.HOME
                || item.getMenu() == DrawerItem.FIVE_M_SAFETY_TALK
                || item.getMenu() == DrawerItem.PLANNED_SHE_MEETING
                || item.getMenu() == DrawerItem.EXIT) {
            MenuItemModel.setInformationActive(false);
            MenuItemModel.setHistoryActive(false);
        }
        switch (item.getMenu()) {
            case DrawerItem.HOME: {
                Log.d("HOME", "Masuk");
                Intent intent = new Intent(mActivity, MainMenu.class);
                finish();
                startActivity(intent);
                break;
            }

            case DrawerItem.INFORMATION: {
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
                Intent intent = new Intent(mActivity, InformationActivity.class);
                finish();
                startActivity(intent);
                break;
            }

            case DrawerItem.SAVE_DATA: {
                if (GrassbergFragment.validateSaveData()) {
                    GrassbergFragment.saveData();
                }
                break;
            }

            case DrawerItem.PREVIEW_REPORT: {
                GrassbergFragment.previewClick();
                break;
            }

//            case DrawerItem.IMPORT_LOOKUP_DATA: {
//                importType = importType.newDataLookUp;
//                showFileChooser();
//                break;
//            }
//            case DrawerItem.EXPORT_LOOKUP_DATA: {
//                lookUp = Helper.Lookup.newDataLookUp;
//                Helper.exportLookup(mActivity, lookUp);
//                break;
//            }
            case DrawerItem.EXPORT_LOOKUP_MANPOWER: {
                lookUp = Helper.Lookup.newData;
                Helper.exportLookup(mActivity, lookUp);
                break;
            }
            case DrawerItem.IMPORT_LOOKUP_MANPOWER: {
                importType = importType.newData;
                showFileChooser();
                break;
            }

            case DrawerItem.GENERATE_REPORT: {
                GrassbergFragment.generateClick();
                break;
            }
            case DrawerItem.CLEAR_DATA: {

                break;
            }
            case DrawerItem.EXIT: {
//                Log.d("EXIT", "Masuk");
//                Helper.showPositiveNegativeDialog(this, "Warning",
//                        "Are you sure want to close this application?", "Yes",
//                        "No", new OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                if (!MenuItemModel.isInformationActive())
//                                    PlannedSHEMeetingFragment.saveChanges(
//                                            currentActivity, false);
//                                finish();
//                            }
//                        }, null);
                break;
            }
        }
    }

    @Override
    public ArrayList<DrawerItem> populateDrawerList(DrawerItem item) {
        ArrayList<DrawerItem> list = null;
        MenuItemModel menu = new MenuItemModel();
        list = menu.populateRootMenuPlanned(mActivity);
        //
        switch (item.getMenu()) {
            case DrawerItem.DATA_MANAGEMENT: {
                list = menu.populateDataManagement();
                break;
            }
            case DrawerItem.REPORT: {
                list = menu.populateReport();
                break;
            }
            case DrawerItem.IMPORT_LOOKUP: {
                list = menu.populateImportLookup();
                break;
            }
            case DrawerItem.EXPORT_LOOKUP: {
                list = menu.populateExportLookup();
                break;
            }
        }

        return list;
    }

    private void showFileChooser() {
        Intent intent = new Intent(mActivity, FileChooser.class);
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add(".csv");
        intent.putExtra("storagePath", Constants.SCBD_FOLDER_ON_EXTERNAL_PATH
                + "/" + Constants.SCBD_FOLDER_NAME);
        intent.putStringArrayListExtra("filterFileExtension", extensions);
        mActivity.startActivityForResult(intent, FILE_SELECT_CODE);
    }

    public void datePicker(final View v) {
        GrassbergFragment.datePickers(GRSActivity.this, v);
    }

//    public void removeInspector(final View v) {
//        GrassbergFragment.removeInspector(v);
//    }

//    public void datePicker(final View v) {
//        MainFragment.datePickers(PagerSlidingMenu.this, v);
//    }
//
//    public void datePickerGenerated(final View v) {
//        AllGeneratedData.datePickers(PagerSlidingMenu.this, v);
//    }

    @Override
    public void onBackPressed() {
//        switch (pager.getCurrentItem()) {
//            case 0:
        GrassbergFragment.onBackPressed(this);
//                break;

//            case 1:
//        InformationFragment.onBackPressed(this);
//                break;

//            default:
//                break;
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImagePopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == mActivity.RESULT_OK) {
            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(ImagePopUp.getPicture(), Helper.fileUri.getPath());
                ImagePopUp.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.grs;
        } else if (requestCode == ImagePopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == mActivity.RESULT_OK && data != null) {
            // new File(tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = mActivity.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImagePopUp.setImagePath(picturePath);
            Helper.setPic(ImagePopUp.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.grs;
    }
}
