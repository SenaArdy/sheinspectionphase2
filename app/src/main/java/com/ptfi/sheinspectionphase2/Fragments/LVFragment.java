package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.MainMenu;
import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.Models.LVDailyModel;
import com.ptfi.sheinspectionphase2.Models.LVDamageModel;
import com.ptfi.sheinspectionphase2.Models.LVInspectionModel;
import com.ptfi.sheinspectionphase2.Models.LVWeeklyModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingCallback;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingsPopUp;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUp;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUpCallback;
import com.ptfi.sheinspectionphase2.PopUps.LVPopUp;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class LVFragment extends Fragment {

    protected static FragmentActivity mActivity;
    private View rootView;

    TextView imeiTV, versionTV;

    static EditText lvNoET, picET, idNoET, deptET, dateET, nextPMET;
    static TextInputLayout lvNoTL, picTL, idNoTL, deptTL, dateTL, nextPMTL;

    TextView addDaily, addWeekly, addDamage, addPhoto;

    CardView minimumCV, weeklyCV, damageCV, photoCV;
    LinearLayout minimumLL, weeklyLL, damageLL, photoLL;
    RecyclerView minimumRV, weeklyRV, damageRV, photoRV;

    RecyclerView.LayoutManager layoutManager;

    ArrayList<LVDailyModel> dailyInsModel;
    private static ArrayList<LVDailyModel> dailyInsData = new ArrayList<>();

    ArrayList<LVWeeklyModel> weeklyInsModel;
    private static ArrayList<LVWeeklyModel> weeklyInsData = new ArrayList<>();

    ArrayList<LVDamageModel> damageInsModel;
    private static ArrayList<LVDamageModel> damageInsData = new ArrayList<>();

    private ArrayList<PhotoModel> photoModel;
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lv_inspection, container, false);
        Helper.visiblePage = Helper.visiblePage.lv;

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();
        loadData();

        return rootView;
    }

    public void initComponent() {
        if (rootView != null) {
            lvNoET = (EditText) rootView.findViewById(R.id.lvNoET);
            picET = (EditText) rootView.findViewById(R.id.picET);
            idNoET = (EditText) rootView.findViewById(R.id.idNoET);
            deptET = (EditText) rootView.findViewById(R.id.deptET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            nextPMET = (EditText) rootView.findViewById(R.id.nextPMET);

            lvNoTL = (TextInputLayout) rootView.findViewById(R.id.lvNoTL);
            picTL = (TextInputLayout) rootView.findViewById(R.id.picTL);
            deptTL = (TextInputLayout) rootView.findViewById(R.id.deptTL);
            idNoTL = (TextInputLayout) rootView.findViewById(R.id.idNoTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            nextPMTL = (TextInputLayout) rootView.findViewById(R.id.nextPMTL);

            addDaily = (TextView) rootView.findViewById(R.id.addDaily);
            addWeekly = (TextView) rootView.findViewById(R.id.addWeekly);
            addDamage = (TextView) rootView.findViewById(R.id.addDamage);
            addPhoto = (TextView) rootView.findViewById(R.id.addPhoto);

            minimumCV = (CardView) rootView.findViewById(R.id.minimumCV);
            weeklyCV = (CardView) rootView.findViewById(R.id.weeklyCV);
            damageCV = (CardView) rootView.findViewById(R.id.damageCV);
            photoCV = (CardView) rootView.findViewById(R.id.photoCV);

            minimumLL = (LinearLayout) rootView.findViewById(R.id.minimumLL);
            weeklyLL = (LinearLayout) rootView.findViewById(R.id.weeklyLL);
            damageLL = (LinearLayout) rootView.findViewById(R.id.damageLL);
            photoLL = (LinearLayout) rootView.findViewById(R.id.photoLL);

            minimumCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (minimumLL.getVisibility() == View.GONE) {
                        minimumLL.setVisibility(View.VISIBLE);
                    } else if (minimumLL.getVisibility() == View.VISIBLE) {
                        minimumLL.setVisibility(View.GONE);
                    }
                }
            });

            weeklyCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (weeklyLL.getVisibility() == View.GONE) {
                        weeklyLL.setVisibility(View.VISIBLE);
                    } else if (weeklyLL.getVisibility() == View.VISIBLE) {
                        weeklyLL.setVisibility(View.GONE);
                    }
                }
            });

            damageCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (damageLL.getVisibility() == View.GONE) {
                        damageLL.setVisibility(View.VISIBLE);
                    } else if (damageLL.getVisibility() == View.VISIBLE) {
                        damageLL.setVisibility(View.GONE);
                    }
                }
            });

            photoCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                    } else if (photoLL.getVisibility() == View.VISIBLE) {
                        photoLL.setVisibility(View.GONE);
                    }
                }
            });

            addDaily.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LVPopUp.dailyPopUp(mActivity, lvNoET.getText().toString(), picET.getText().toString(), dateET.getText().toString());
                }
            });

            addWeekly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LVPopUp.weeklyPopUp(mActivity, lvNoET.getText().toString(), picET.getText().toString(), dateET.getText().toString());
                }
            });

            addDamage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LVPopUp.damagePopUp(mActivity, lvNoET.getText().toString(), picET.getText().toString(), dateET.getText().toString());
                }
            });

            addPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImagePopUp.imagePopUp(mActivity, false, Constants.INSPECTION_LVI, "", "", dateET.getText().toString(),
                            picET.getText().toString(), new ImagePopUpCallback() {
                                @Override
                                public void onImageSubmitted() {
                                    recycleView();
                                }
                            });

                }
            });

        }
    }

    public void loadData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        LVInspectionModel model = ds.getLastestLVMain();
        picET.setText(model.getPic());
        lvNoET.setText(model.getLvNo());
        idNoET.setText(model.getIdNo());
        dateET.setText(model.getDate());
        deptET.setText(model.getDepartment());
        nextPMET.setText(model.getNextPM());

        /* ===================
               LV Daily Recycle View
        =================== */
        minimumRV = (RecyclerView) rootView.findViewById(R.id.minimumRV);
        minimumRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        minimumRV.setLayoutManager(layoutManager);
        minimumRV.setItemAnimator(new DefaultItemAnimator());
//        dailyInsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        LVFragment.DailyAdapter dailyAdapter= new LVFragment.DailyAdapter(mActivity, dailyInsData);
        minimumRV.setAdapter(dailyAdapter);

         /* ===================
              LV Weekly Recycle View
        =================== */
        weeklyRV = (RecyclerView) rootView.findViewById(R.id.weeklyRV);
        weeklyRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        weeklyRV.setLayoutManager(layoutManager);
        weeklyRV.setItemAnimator(new DefaultItemAnimator());
//        weeklyInsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        LVFragment.WeeklyAdapter weeklyAdapter= new LVFragment.WeeklyAdapter(mActivity, weeklyInsData);
        weeklyRV.setAdapter(weeklyAdapter);

        /* ===================
              LV Weekly Recycle View
        =================== */
        damageRV = (RecyclerView) rootView.findViewById(R.id.damageRV);
        damageRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        damageRV.setLayoutManager(layoutManager);
        damageRV.setItemAnimator(new DefaultItemAnimator());
//        damageInsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        LVFragment.DamageAdapter damageAdapter= new LVFragment.DamageAdapter(mActivity, damageInsData);
        damageRV.setAdapter(damageAdapter);

        ds.close();
    }

    /* ========================
                   LV Daily Adapter
    ======================== */
    public class DailyAdapter extends RecyclerView.Adapter<LVFragment.DailyAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<LVDailyModel> dailyData;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public TextView driverTV;
            public TextView dateTV;
            public TextView timeTV;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.driverTV = (TextView) itemView.findViewById(R.id.driverTV);
                this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
                this.timeTV = (TextView) itemView.findViewById(R.id.timeTV);

                listCV.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
//                if (fieldFindingsModel != null) {
//                int position = getAdapterPosition();
//                    String finding = fieldData.get(position).getFindings();

            }
        }

        public DailyAdapter(Activity mActivity, ArrayList<LVDailyModel> dailyData) {
            super();
            this.mActivity = mActivity;
            this.dailyData = dailyData;
        }

        @Override
        public LVFragment.DailyAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_lv_daily, parent, false);
            LVFragment.DailyAdapter.dataViewHolder dataViewHolder = new LVFragment.DailyAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(LVFragment.DailyAdapter.dataViewHolder holder, int position) {
            TextView driverTV = holder.driverTV;
            TextView timeTV = holder.timeTV;
            TextView dateTV = holder.dateTV;

            driverTV.setText(dailyData.get(position).getDriverId());
            timeTV.setText(dailyData.get(position).getTime());
            dateTV.setText(dailyData.get(position).getDate());
        }

        @Override
        public int getItemCount() {
            return dailyData.size();
        }

    }

    public void recycleViewDaily() {
        minimumRV = (RecyclerView) rootView.findViewById(R.id.minimumRV);
        minimumRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        minimumRV.setLayoutManager(layoutManager);
        minimumRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(getActivity());
        ds.open();
//        fieldFindingsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        ds.close();

        LVFragment.DailyAdapter dailyAdapter= new LVFragment.DailyAdapter(mActivity, dailyInsData);
        minimumRV.setAdapter(dailyAdapter);
    }

    /* ========================
                   LV Weekly Adapter
    ======================== */
    public class WeeklyAdapter extends RecyclerView.Adapter<LVFragment.WeeklyAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<LVWeeklyModel> weeklyData;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public TextView driverTV;
            public TextView dateTV;
            public TextView supervisorIDTV;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.driverTV = (TextView) itemView.findViewById(R.id.driverTV);
                this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
                this.supervisorIDTV = (TextView) itemView.findViewById(R.id.supervisorIDTV);

                listCV.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
//                if (fieldFindingsModel != null) {
//                int position = getAdapterPosition();
//                    String finding = fieldData.get(position).getFindings();

            }
        }

        public WeeklyAdapter(Activity mActivity, ArrayList<LVWeeklyModel> weeklyData) {
            super();
            this.mActivity = mActivity;
            this.weeklyData = weeklyData;
        }

        @Override
        public LVFragment.WeeklyAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_lv_weekly, parent, false);
            LVFragment.WeeklyAdapter.dataViewHolder dataViewHolder = new LVFragment.WeeklyAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(LVFragment.WeeklyAdapter.dataViewHolder holder, int position) {
            TextView driverTV = holder.driverTV;
            TextView supervisorIDTV = holder.supervisorIDTV;
            TextView dateTV = holder.dateTV;

            driverTV.setText(weeklyData.get(position).getDriverId());
            supervisorIDTV.setText(weeklyData.get(position).getSuperVisor());
            dateTV.setText(weeklyData.get(position).getDate());
        }

        @Override
        public int getItemCount() {
            return weeklyData.size();
        }

    }

    public void recycleViewWeekly() {
        weeklyRV = (RecyclerView) rootView.findViewById(R.id.weeklyRV);
        weeklyRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        weeklyRV.setLayoutManager(layoutManager);
        weeklyRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(getActivity());
        ds.open();
//        fieldFindingsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        ds.close();

        LVFragment.WeeklyAdapter weeklyAdapter= new LVFragment.WeeklyAdapter(mActivity, weeklyInsData);
        weeklyRV.setAdapter(weeklyAdapter);
    }

    /* ========================
                   LV Damage Adapter
    ======================== */
    public class DamageAdapter extends RecyclerView.Adapter<LVFragment.DamageAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<LVDamageModel> damageData;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public TextView driverTV;
            public TextView dateTV;
            public TextView actionTV;
            public TextView itemTV;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.driverTV = (TextView) itemView.findViewById(R.id.driverTV);
                this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
                this.actionTV = (TextView) itemView.findViewById(R.id.actionTV);
                this.itemTV = (TextView) itemView.findViewById(R.id.itemTV);

                listCV.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
//                if (fieldFindingsModel != null) {
//                int position = getAdapterPosition();
//                    String finding = fieldData.get(position).getFindings();

            }
        }

        public DamageAdapter(Activity mActivity, ArrayList<LVDamageModel> damageData) {
            super();
            this.mActivity = mActivity;
            this.damageData = damageData;
        }

        @Override
        public LVFragment.DamageAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_lv_damage, parent, false);
            LVFragment.DamageAdapter.dataViewHolder dataViewHolder = new LVFragment.DamageAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(LVFragment.DamageAdapter.dataViewHolder holder, int position) {
            TextView driverTV = holder.driverTV;
            TextView actionTV = holder.actionTV;
            TextView dateTV = holder.dateTV;
            TextView itemTV = holder.itemTV;

            driverTV.setText(damageData.get(position).getDriverId());
            actionTV.setText(damageData.get(position).getAction());
            dateTV.setText(damageData.get(position).getDate());
            itemTV.setText(damageData.get(position).getItemDamage());
        }

        @Override
        public int getItemCount() {
            return damageData.size();
        }

    }

    public void recycleViewDamage() {
        damageRV = (RecyclerView) rootView.findViewById(R.id.damageRV);
        damageRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        damageRV.setLayoutManager(layoutManager);
        damageRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(getActivity());
        ds.open();
//        fieldFindingsData = ds.getFieldFindingsData(companyET.getText().toString(), departmentET.getText().toString(), sectionET.getText().toString(),
//                inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD);
        ds.close();

        LVFragment.DamageAdapter damageAdapter= new LVFragment.DamageAdapter(mActivity, damageInsData);
        damageRV.setAdapter(damageAdapter);
    }

    /* ========================
                   Photo Adapter
    ======================== */
    public class PhotoAdapter extends RecyclerView.Adapter<LVFragment.PhotoAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<PhotoModel> photoModel;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public final ImageView pictureIV;
            public TextView commentTV;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
                this.commentTV = (TextView) itemView.findViewById(R.id.commentTV);

                listCV.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (photoModel != null) {
                    int position = getAdapterPosition();
                    String fotoPath = photoModel.get(position).getFotoPath();
                    String comments = photoModel.get(position).getComment();

                    ImagePopUp.imagePopUp(mActivity, true, Constants.INSPECTION_LVI, fotoPath, comments, dateET.getText().toString(),
                            picET.getText().toString(), new ImagePopUpCallback() {
                                @Override
                                public void onImageSubmitted() {
                                    recycleView();
                                }
                            });

                } else {

                }
            }
        }


        public PhotoAdapter(Activity mActivity, ArrayList<PhotoModel> photoModel) {
            super();
            this.mActivity = mActivity;
            this.photoModel = photoModel;
        }

        @Override
        public LVFragment.PhotoAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_photo, parent, false);
            LVFragment.PhotoAdapter.dataViewHolder dataViewHolder = new LVFragment.PhotoAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(LVFragment.PhotoAdapter.dataViewHolder holder, int position) {
            TextView commentTV = holder.commentTV;
            ImageView pictureIV = holder.pictureIV;

            commentTV.setText(photoModel.get(position).getComment());
            if (photoModel.get(position).getFotoPath().equals("")) {
                pictureIV.setImageResource(R.drawable.default_nopicture);
            } else {
                Helper.setPic(pictureIV, photoModel.get(position).getFotoPath());
            }
        }

        @Override
        public int getItemCount() {
            return photoModel.size();
        }
    }


    public void recycleView() {
        photoRV = (RecyclerView) rootView.findViewById(R.id.photoRV);
        photoRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        photoRV.setLayoutManager(layoutManager);
        photoRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(getActivity());
        ds.open();
        photoData = ds.getPhotoData(Constants.INSPECTION_LVI,
                dateET.getText().toString(), picET.getText().toString());
        ds.close();

        LVFragment.PhotoAdapter photoAdapter= new LVFragment.PhotoAdapter(getActivity(), photoData);
        photoRV.setAdapter(photoAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImagePopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {
            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(ImagePopUp.getPicture(), Helper.fileUri.getPath());
                ImagePopUp.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.lv;
        } else if (requestCode == ImagePopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK && data != null) {
            // new File(tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImagePopUp.setImagePath(picturePath);
            Helper.setPic(ImagePopUp.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.lv;
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent  = new Intent(mActivity, MainMenu.class);
                mActivity.finish();
                mActivity.startActivity(intent);
            }
        });

        builder.show();
    }

}
