package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import org.w3c.dom.Text;

/**
 * Created by senaardyputra on 11/15/16.
 */

public class LVGroupFragment extends Fragment{

    protected static FragmentActivity mActivity;
    private View rootView;

    TextView imeiTV, versionTV;

    static EditText inspectorET, idET, dateET;
    static TextInputLayout inspectorTL, idTL, dateTL;
    static TextView addChecklist, addFinding;

    CardView checklistCV, findingCV;
    LinearLayout checklistLL, findingLL;
    RecyclerView checklistRV, findingRV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lv_group, container, false);
        Helper.visiblePage = Helper.visiblePage.lvGrouping;

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();

        return rootView;
    }

    private void initComponent() {
        if(rootView != null) {
            inspectorET = (EditText) rootView.findViewById(R.id.inspectorET);
            idET = (EditText) rootView.findViewById(R.id.idET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);

            inspectorTL = (TextInputLayout) rootView.findViewById(R.id.inspectorTL);
            idTL = (TextInputLayout) rootView.findViewById(R.id.idTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);

            checklistCV = (CardView) rootView.findViewById(R.id.checklistCV);
            findingCV = (CardView) rootView.findViewById(R.id.findingCV);

            checklistLL = (LinearLayout) rootView.findViewById(R.id.checklistLL);
            findingLL = (LinearLayout) rootView.findViewById(R.id.findingLL);

            checklistCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checklistLL.getVisibility() == View.GONE) {
                        checklistLL.setVisibility(View.VISIBLE);
                    } else if (checklistLL.getVisibility() == View.VISIBLE) {
                        checklistLL.setVisibility(View.GONE);
                    }
                }
            });

            findingCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (findingLL.getVisibility() == View.GONE) {
                        findingLL.setVisibility(View.VISIBLE);
                    } else if (findingLL.getVisibility() == View.VISIBLE) {
                        findingLL.setVisibility(View.GONE);
                    }
                }
            });

            addChecklist = (TextView) rootView.findViewById(R.id.addChecklist);
            addFinding = (TextView) rootView.findViewById(R.id.addFinding);

            addChecklist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            addFinding.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("SAVE AND EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (validationMain()) {
//                    saveData();
//                    mActivity.finish();
//                }
            }
        });

        builder.show();
    }

}
