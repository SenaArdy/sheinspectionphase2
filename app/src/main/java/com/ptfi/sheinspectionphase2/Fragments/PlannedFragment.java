package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.Font;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.sheinspectionphase2.Adapter.IPhotoAdapter;
import com.ptfi.sheinspectionphase2.Adapter.PlannedFindingPhotoAdapter;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.MainMenu;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;
import com.ptfi.sheinspectionphase2.Models.PlannedInspectionModel;
import com.ptfi.sheinspectionphase2.Models.PlannedMainModel;
import com.ptfi.sheinspectionphase2.Models.PlannedSignModel;
import com.ptfi.sheinspectionphase2.PopUps.GesturePopUp;
import com.ptfi.sheinspectionphase2.PopUps.GesturePopUpCallback;
import com.ptfi.sheinspectionphase2.PopUps.InspectorCallback;
import com.ptfi.sheinspectionphase2.PopUps.PlannedFindingCallback;
import com.ptfi.sheinspectionphase2.PopUps.PlannedFindingsPopUp;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.CSVHelper;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import org.honorato.multistatetogglebutton.SecondMultiToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static java.lang.String.valueOf;

/**
 * Created by senaardyputra on 11/24/16.
 */

public class PlannedFragment extends Fragment {

    private static FragmentActivity mActivity;
    public static View rootView;

    TextView imeiTV, versionTV;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    static String version;

    ArrayList<ManpowerModel> manpowers = new ArrayList<ManpowerModel>();
    static ArrayList<ManpowerModel> selectedInspectorList = new ArrayList<ManpowerModel>();
    ArrayList<ManpowerModel> selectedAreaOwnerList = new ArrayList<ManpowerModel>();
    ManpowerModel siteSpv;
    ManpowerModel deptMgr;
    ManpowerModel areaSafety;
    ManpowerModel selectedObserver = null;

    private Dialog activeDialog;

    private boolean isEmptySignature = true;

    public static int workArea = 1;
    public static int structureFloor = 2;
    public static int machineryTools = 3;
    public static int Electrical = 4;
    public static int Personel = 5;
    public static int fireSafety = 6;
    public static int firstAid = 7;
    public static int Enviromental = 8;
    public static int Communication = 9;
    public static int fallProtection = 10;

    static EditText dateET, locationET, dateSiteET, dateDeptET, dateSafeET, siteSVET, deptManagerET, deptSafeET;
    private static TextInputLayout inspectorTL, dateTL, locationTL, areaOwnerTL, siteSVTL, deptManagerTL, deptSafeTL, dateSiteTL, dateDeptTL, dateSafeTL;

    static EditText inspectorET, areaOwnerET;

    static SecondMultiToggleButton inspWaRisk1, inspWaRate1, inspWaRisk2, inspWaRate2, inspWaRisk3, inspWaRate3, inspWaRisk4, inspWaRate4,
            inspWaRisk5, inspWaRate5, inspWaRisk6, inspWaRate6;

    static SecondMultiToggleButton inspStRisk1, inspStRate1, inspStRisk2, inspStRate2, inspStRisk3, inspStRate3, inspStRisk4, inspStRate4, inspStRisk5,
            inspStRate5;

    static SecondMultiToggleButton inspMtRisk1, inspMtRate1, inspMtRisk2, inspMtRate2, inspMtRisk3, inspMtRate3, inspMtRisk4, inspMtRate4,
            inspMtRisk5, inspMtRate5, inspMtRisk6, inspMtRate6, inspMtRisk7, inspMtRate7;

    static SecondMultiToggleButton inspElRisk1, inspElRate1, inspElRisk2, inspElRate2, inspElRisk3, inspElRate3, inspElRisk4, inspElRate4, inspElRisk5, inspElRate5;

    static SecondMultiToggleButton inspPeRisk1, inspPeRate1, inspPeRisk2, inspPeRate2, inspPeRisk3, inspPeRate3, inspPeRisk4, inspPeRate4, inspPeRisk5, inspPeRate5,
            inspPeRisk6, inspPeRate6, inspPeRisk7, inspPeRate7, inspPeRisk8, inspPeRate8;

    static SecondMultiToggleButton inspFSRisk1, inspFSRate1, inspFSRisk2, inspFSRate2, inspFSRisk3, inspFSRate3, inspFSRisk4, inspFSRate4, inspFSRisk5, inspFSRate5;

    static SecondMultiToggleButton inspFARisk1, inspFARate1, inspFARisk2, inspFARate2, inspFARisk3, inspFARate3;

    static SecondMultiToggleButton inspENRisk1, inspENRate1, inspENRisk2, inspENRate2, inspENRisk3, inspENRate3, inspENRisk4, inspENRate4;

    static SecondMultiToggleButton inspCOMRisk1, inspCOMRate1, inspCOMRisk2, inspCOMRate2, inspCOMRisk3, inspCOMRate3, inspCOMRisk4, inspCOMRate4, inspCOMRisk5, inspCOMRate5,
            inspCOMRisk6, inspCOMRate6, inspCOMRisk7, inspCOMRate7;

    static SecondMultiToggleButton inspFPRisk1, inspFPRate1, inspFPRisk2, inspFPRate2, inspFPRisk3, inspFPRate3, inspFPRisk4, inspFPRate4, inspFPRisk5, inspFPRate5,
            inspFPRisk6, inspFPRate6;

    TextView pl_findingsWa, pl_findingsSt, pl_findingsMt, pl_findingsEl, pl_findingsPe, pl_findingsFs, pl_findingsFa, pl_findingsEn,
            pl_findingsCom, pl_findingsFp, pl_waTV, pl_stTV, pl_mtTV, pl_elTV, pl_peTV, pl_fsTV, pl_faTV, pl_enTV, pl_comTV, pl_fpTV, addFindings;

    ImageView waIV, sfIV, mtIV, elIV, peIV, fsaIV, FAIV, enIV, comIV, FPIV, findingsIV;

/*HEADER CARDVIEW*/

    CardView pl_waCV, pl_stCV, pl_mtCV, pl_elCV, pl_peCV, pl_fsCV, pl_faCV, pl_enCV, pl_comCV, pl_fpCV, photoCV, findingCV;

    LinearLayout pl_waLL, pl_stLL, pl_mtLL, pl_elLL, pl_peLL, pl_fsLL, pl_faLL, pl_enLL, pl_comLL, pl_fpLL, photoLL, findingsLL;

//    ButtonRectangle generateBtn, previewBtn, clearBtn;

    static RecyclerView findingsRV;

    static RecyclerView.LayoutManager layoutManager;

    ImageView minInspector, minAreaOwner;

    ArrayList<PlannedFindingsModel> plannedFindingsModels;
    private static ArrayList<PlannedFindingsModel> plannedFindingsData = new ArrayList<>();

    private static ArrayList<PhotoModel> photoData = new ArrayList<>();
    private Helper.visiblePage visiblePage;

    public static int INSPECTION_CODE;

    private static PlannedMainModel plannedMainModel;
    private static PlannedFindingPhotoAdapter plannedFindingPhotoAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_planned_inspection, container, false);
        Helper.visiblePage = Helper.visiblePage.planned;

        DataSource ds = new DataSource(getActivity());
        ds.open();
        plannedMainModel = ds.getLastestPlannedMain();
        plannedFindingsData = ds.getAllPlannedFindings(plannedMainModel.getId());

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);
        if (ds.checkTableFindings().isEmpty()) {
            INSPECTION_CODE = 1;
        } else {
            INSPECTION_CODE = Integer.valueOf(ds.getLastPlannedFindingCode());
        }
        ds.close();

        initComponent();
        initRecycleView();
        headerLine();
        manpowers = Constants.getObserverDictionary();
//        grsDrillingDetailClick();
        loadData();
        multiStateToggleButton();

        return rootView;
    }

    private void initRecycleView() {
        plannedFindingPhotoAdapter = new PlannedFindingPhotoAdapter(plannedFindingsData, new IPhotoAdapter() {

            @Override
            public void onItemClicked(int position) {
                onListViewClick(position, plannedFindingsData.get(position));
            }

            @Override
            public void onItemDelete(int position) {
                onListViewDelete(position);
            }
        });

        findingsRV.setAdapter(plannedFindingPhotoAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        findingsRV.setLayoutManager(layoutManager);
    }

    private void onListViewClick(final int position, PlannedFindingsModel plannedFindingsModel) {
        if (validateSaveData()) {
            PlannedFindingsPopUp.showPlannedFindingsPopUp(mActivity, plannedFindingsModel,
                    new PlannedFindingCallback() {
                        @Override
                        public void onPlannedSubmited(PlannedFindingsModel model) {
                            plannedFindingsData.set(position, model);
                            plannedFindingPhotoAdapter.notifyDataSetChanged();
                        }
                    });
        }
    }

    private void onListViewDelete(final int position) {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to delete this finding?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                plannedFindingsData.remove(position);
                plannedFindingPhotoAdapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void initComponent() {
        if (rootView != null) {
            findingsRV = (RecyclerView) rootView.findViewById(R.id.findingsRV);

            /* == INSPECTOR FILL == */
            inspectorET = (EditText) rootView.findViewById(R.id.inspectorET);
            inspectorET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAreaOwnerPopUp(Constants.SEARCH_TYPE_INSPECTOR, "Inspector", new InspectorCallback() {
                        @Override
                        public void onChange() {

                        }
                    });
                }
            });

            locationET = (EditText) rootView.findViewById(R.id.locationET);
            areaOwnerET = (EditText) rootView.findViewById(R.id.areaOwnerET);
            areaOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAreaOwnerPopUp(Constants.SEARCH_TYPE_AREA_OWNER, "Area Owner", new InspectorCallback() {
                        @Override
                        public void onChange() {

                        }
                    });
                }
            });

            dateET = (EditText) rootView.findViewById(R.id.dateET);
            siteSVET = (EditText) rootView.findViewById(R.id.siteSVET);
            siteSVET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopUpInspector(Constants.SEARCH_TYPE_SITE_SPV);
                }
            });
            deptManagerET = (EditText) rootView.findViewById(R.id.deptManagerET);
            deptManagerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopUpInspector(Constants.SEARCH_TYPE_DETP_MGR);
                }
            });
            deptSafeET = (EditText) rootView.findViewById(R.id.deptSafeET);
            deptSafeET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopUpInspector(Constants.SEARCH_TYPE_AREA_SAFETY);
                }
            });
            dateSiteET = (EditText) rootView.findViewById(R.id.dateSiteET);
            dateDeptET = (EditText) rootView.findViewById(R.id.dateDeptET);
            dateSafeET = (EditText) rootView.findViewById(R.id.dateSafeET);

            inspectorTL = (TextInputLayout) rootView.findViewById(R.id.inspectorTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            locationTL = (TextInputLayout) rootView.findViewById(R.id.locationTL);
            locationTL.clearFocus();
            locationET.clearFocus();
            View view = mActivity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), Intent.FLAG_ACTIVITY_SINGLE_TOP);
            }

            areaOwnerTL = (TextInputLayout) rootView.findViewById(R.id.areaOwnerTL);
            siteSVTL = (TextInputLayout) rootView.findViewById(R.id.siteSVTL);
            deptManagerTL = (TextInputLayout) rootView.findViewById(R.id.deptManagerTL);
            deptSafeTL = (TextInputLayout) rootView.findViewById(R.id.deptSafeTL);
            dateSiteTL = (TextInputLayout) rootView.findViewById(R.id.dateSiteTL);
            dateDeptTL = (TextInputLayout) rootView.findViewById(R.id.dateDeptTL);
            dateSafeTL = (TextInputLayout) rootView.findViewById(R.id.dateSafeTL);

            //Multitoggle Button

            /* == WORK AREA == */
            inspWaRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk1);
            inspWaRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate1);
            inspWaRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate2);
            inspWaRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk2);
            inspWaRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate3);
            inspWaRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk3);
            inspWaRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate4);
            inspWaRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk4);
            inspWaRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate5);
            inspWaRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk5);
            inspWaRate6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRate6);
            inspWaRisk6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspWaRisk6);

            pl_findingsWa = (TextView) rootView.findViewById(R.id.pl_findingsWa);
            pl_findingsWa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    inspWaRate1.setValue(-1);
                    inspWaRate2.setValue(-1);
                    inspWaRate3.setValue(-1);
                    inspWaRate4.setValue(-1);
                    inspWaRate5.setValue(-1);
                    inspWaRate6.setValue(-1);

                    inspWaRisk1.setValue(-1);
                    inspWaRisk2.setValue(-1);
                    inspWaRisk3.setValue(-1);
                    inspWaRisk4.setValue(-1);
                    inspWaRisk5.setValue(-1);
                    inspWaRisk6.setValue(-1);
                }
            });

            /* == STRUCTURE == */
            inspStRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRisk1);
            inspStRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRate1);
            inspStRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRisk2);
            inspStRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRate2);
            inspStRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRisk3);
            inspStRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRate3);
            inspStRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRisk4);
            inspStRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRate4);
            inspStRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRisk5);
            inspStRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspStRate5);

            pl_findingsSt = (TextView) rootView.findViewById(R.id.pl_findingsSt);
            pl_findingsSt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspStRisk1.setValue(-1);
                    inspStRisk2.setValue(-1);
                    inspStRisk3.setValue(-1);
                    inspStRisk4.setValue(-1);
                    inspStRisk5.setValue(-1);

                    inspStRate1.setValue(-1);
                    inspStRate2.setValue(-1);
                    inspStRate3.setValue(-1);
                    inspStRate4.setValue(-1);
                    inspStRate5.setValue(-1);
                }
            });

            /* == MACHINERY == */
            inspMtRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk1);
            inspMtRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate1);
            inspMtRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk2);
            inspMtRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate2);
            inspMtRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk3);
            inspMtRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate3);
            inspMtRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk4);
            inspMtRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate4);
            inspMtRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk5);
            inspMtRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate5);
            inspMtRisk6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk6);
            inspMtRate6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate6);
            inspMtRisk7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRisk7);
            inspMtRate7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspMtRate7);

            pl_findingsMt = (TextView) rootView.findViewById(R.id.pl_findingsMt);
            pl_findingsMt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspMtRisk1.setValue(-1);
                    inspMtRisk2.setValue(-1);
                    inspMtRisk3.setValue(-1);
                    inspMtRisk4.setValue(-1);
                    inspMtRisk5.setValue(-1);
                    inspMtRisk6.setValue(-1);
                    inspMtRisk7.setValue(-1);

                    inspMtRate1.setValue(-1);
                    inspMtRate2.setValue(-1);
                    inspMtRate3.setValue(-1);
                    inspMtRate4.setValue(-1);
                    inspMtRate5.setValue(-1);
                    inspMtRate6.setValue(-1);
                    inspMtRate7.setValue(-1);
                }
            });

            /* == ELECTRICAL == */
            inspElRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRisk1);
            inspElRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRate1);
            inspElRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRisk2);
            inspElRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRate2);
            inspElRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRisk3);
            inspElRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRate3);
            inspElRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRisk4);
            inspElRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRate4);
            inspElRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRisk5);
            inspElRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspElRate5);

            pl_findingsEl = (TextView) rootView.findViewById(R.id.pl_findingsEl);
            pl_findingsEl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspElRisk1.setValue(-1);
                    inspElRisk2.setValue(-1);
                    inspElRisk3.setValue(-1);
                    inspElRisk4.setValue(-1);
                    inspElRisk5.setValue(-1);

                    inspElRate1.setValue(-1);
                    inspElRate2.setValue(-1);
                    inspElRate3.setValue(-1);
                    inspElRate4.setValue(-1);
                    inspElRate5.setValue(-1);
                }
            });

            /* == PERSONEL == */
            inspPeRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk1);
            inspPeRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate1);
            inspPeRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk2);
            inspPeRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate2);
            inspPeRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk3);
            inspPeRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate3);
            inspPeRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk4);
            inspPeRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate4);
            inspPeRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk5);
            inspPeRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate5);
            inspPeRisk6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk6);
            inspPeRate6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate6);
            inspPeRisk7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk7);
            inspPeRate7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate7);
            inspPeRisk8 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRisk8);
            inspPeRate8 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspPeRate8);

            pl_findingsPe = (TextView) rootView.findViewById(R.id.pl_findingsPe);
            pl_findingsPe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspPeRisk1.setValue(-1);
                    inspPeRisk2.setValue(-1);
                    inspPeRisk3.setValue(-1);
                    inspPeRisk4.setValue(-1);
                    inspPeRisk5.setValue(-1);
                    inspPeRisk6.setValue(-1);
                    inspPeRisk7.setValue(-1);
                    inspPeRisk8.setValue(-1);

                    inspPeRate1.setValue(-1);
                    inspPeRate2.setValue(-1);
                    inspPeRate3.setValue(-1);
                    inspPeRate4.setValue(-1);
                    inspPeRate5.setValue(-1);
                    inspPeRate6.setValue(-1);
                    inspPeRate7.setValue(-1);
                    inspPeRate8.setValue(-1);
                }
            });

            /* == FIRE SAFETY == */
            inspFSRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRisk1);
            inspFSRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRate1);
            inspFSRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRisk2);
            inspFSRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRate2);
            inspFSRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRisk3);
            inspFSRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRate3);
            inspFSRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRisk4);
            inspFSRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRate4);
            inspFSRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRisk5);
            inspFSRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFSRate5);

            pl_findingsFs = (TextView) rootView.findViewById(R.id.pl_findingsFs);
            pl_findingsFs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspFSRisk1.setValue(-1);
                    inspFSRisk2.setValue(-1);
                    inspFSRisk3.setValue(-1);
                    inspFSRisk4.setValue(-1);
                    inspFSRisk5.setValue(-1);

                    inspFSRate1.setValue(-1);
                    inspFSRate2.setValue(-1);
                    inspFSRate3.setValue(-1);
                    inspFSRate4.setValue(-1);
                    inspFSRate5.setValue(-1);
                }
            });

            /* == FIRST AID == */
            inspFARisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARisk1);
            inspFARate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARate1);
            inspFARisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARisk2);
            inspFARate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARisk2);
            inspFARisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARisk3);
            inspFARate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFARate3);

            pl_findingsFa = (TextView) rootView.findViewById(R.id.pl_findingsFa);
            pl_findingsFa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspFARisk1.setValue(-1);
                    inspFARisk2.setValue(-1);
                    inspFARisk3.setValue(-1);

                    inspFARate1.setValue(-1);
                    inspFARate2.setValue(-1);
                    inspFARate3.setValue(-1);
                }
            });

            /* == ENVIRONMENT == */
            inspENRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRisk1);
            inspENRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRate1);
            inspENRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRisk2);
            inspENRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRate2);
            inspENRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRisk3);
            inspENRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRate3);
            inspENRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRisk4);
            inspENRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspENRate4);

            pl_findingsEn = (TextView) rootView.findViewById(R.id.pl_findingsEn);
            pl_findingsEn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspENRisk1.setValue(-1);
                    inspENRisk2.setValue(-1);
                    inspENRisk3.setValue(-1);
                    inspENRisk4.setValue(-1);

                    inspENRate1.setValue(-1);
                    inspENRate2.setValue(-1);
                    inspENRate3.setValue(-1);
                    inspENRate4.setValue(-1);
                }
            });

            /* == COMMUNICATION == */
            inspCOMRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk1);
            inspCOMRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate1);
            inspCOMRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk2);
            inspCOMRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate2);
            inspCOMRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk3);
            inspCOMRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate3);
            inspCOMRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk4);
            inspCOMRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate4);
            inspCOMRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk5);
            inspCOMRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate5);
            inspCOMRisk6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk6);
            inspCOMRate6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate6);
            inspCOMRisk7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRisk7);
            inspCOMRate7 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspCOMRate7);

            pl_findingsCom = (TextView) rootView.findViewById(R.id.pl_findingsCom);
            pl_findingsCom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspCOMRisk1.setValue(-1);
                    inspCOMRisk2.setValue(-1);
                    inspCOMRisk3.setValue(-1);
                    inspCOMRisk4.setValue(-1);
                    inspCOMRisk5.setValue(-1);
                    inspCOMRisk6.setValue(-1);
                    inspCOMRisk7.setValue(-1);

                    inspCOMRate1.setValue(-1);
                    inspCOMRate2.setValue(-1);
                    inspCOMRate3.setValue(-1);
                    inspCOMRate4.setValue(-1);
                    inspCOMRate5.setValue(-1);
                    inspCOMRate6.setValue(-1);
                    inspCOMRate7.setValue(-1);
                }
            });

            /* == FALL PROTECTION == */
            inspFPRisk1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk1);
            inspFPRate1 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate1);
            inspFPRisk2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk2);
            inspFPRate2 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate2);
            inspFPRisk3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk3);
            inspFPRate3 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate3);
            inspFPRisk4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk4);
            inspFPRate4 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate4);
            inspFPRisk5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk5);
            inspFPRate5 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate5);
            inspFPRisk6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRisk6);
            inspFPRate6 = (SecondMultiToggleButton) rootView.findViewById(R.id.inspFPRate6);

            pl_findingsFp = (TextView) rootView.findViewById(R.id.pl_findingsFp);
            pl_findingsFp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inspFPRisk1.setValue(-1);
                    inspFPRisk2.setValue(-1);
                    inspFPRisk3.setValue(-1);
                    inspFPRisk4.setValue(-1);
                    inspFPRisk5.setValue(-1);
                    inspFPRisk6.setValue(-1);

                    inspFPRate1.setValue(-1);
                    inspFPRate2.setValue(-1);
                    inspFPRate3.setValue(-1);
                    inspFPRate4.setValue(-1);
                    inspFPRate5.setValue(-1);
                    inspFPRate6.setValue(-1);
                }
            });

            addFindings = (TextView) rootView.findViewById(R.id.addFindingsTV);
            addFindings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PlannedFindingsPopUp.showPlannedFindingsPopUp(mActivity, new PlannedFindingsModel(),
                            new PlannedFindingCallback() {
                                @Override
                                public void onPlannedSubmited(PlannedFindingsModel model) {
                                    plannedFindingsData.add(0, model);
                                    plannedFindingPhotoAdapter.notifyDataSetChanged();
                                }
                            });
                }
            });

            minInspector = (ImageView) rootView.findViewById(R.id.minInspector);
            minInspector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeManpowerList(inspectorET);
                }
            });
            minAreaOwner = (ImageView) rootView.findViewById(R.id.minAreaOwner);
            minAreaOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeManpowerList(areaOwnerET);
                }
            });

            // Date
            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());
            dateTL.setError(null);

            locationET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (locationET.getText().length() > 0)
                        locationTL.setError(null);
                }
            });
        }
    }

    public static void generateClick() {
        if (validationData()) {
            saveData();
            GenerateAsync generate = new GenerateAsync();
            generate.execute(true);
        }
    }

    public static void previewClick() {
        if (validationData()) {
            saveData();
            GenerateAsync generate = new GenerateAsync();
            generate.execute(false);
        }
    }

    static class GenerateAsync extends AsyncTask<Boolean, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            generateReportPlanned(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();
        }

    }

    static String passCheck = Html.fromHtml("&#10004;").toString();

    static String folderDirectory = "";
    private static String previewFilePath;
    private static boolean isPreview = false;
    private static final int PREVIEW_CODE_SHE_PLANNED = 404;

    public static void generateReportPlanned(final boolean isGenerate) {

        String reportPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        reportPath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FILE_NAME;

        //generate file name format
//        String formattedID = PlannedFragment.formatIDInspector(selectedInspectorList.get(0).getIdno());
        String time = new SimpleDateFormat("HHmm").format(new Date());
        String fullPath = "";
        String fileName = Helper.ddMMMYYYYtoYYYYMMdd(dateET.getText().toString()) + time + "_PSI_" + formatIDInspector(selectedInspectorList.get(0).getIdno());
        if (isGenerate) {
            fullPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/"
                    + Constants.ROOT_FOLDER_NAME
                    + "/"
                    + Constants.APP_FOLDER_NAME
                    + "/"
                    + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
            File exportFolder = new File(fullPath);
            exportFolder.mkdir();
        }

        //create image folder
        String imageFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/"
                + Constants.ROOT_FOLDER_NAME
                + "/"
                + Constants.APP_FOLDER_NAME
                + "/"
                + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
        File imageFolder = new File(imageFolderPath);

        String docName = Helper.ddMMMYYYYtoYYYYMMdd(dateET.getText().toString()) + time + "_PSI_" + formatIDInspector(selectedInspectorList.get(0).getIdno());

        if (isGenerate) {
            exportCsv(fullPath + "/", docName + ".csv");
        }

        try {
            Document doc = new Document(reportPath);
            DocumentBuilder builder = new DocumentBuilder(doc);
            doc.getRange().replace("location_header_id", locationET.getText().toString(), true, true);
            doc.getRange().replace("date_header_id", dateET.getText().toString(), true, true);
            doc.getRange().replace("inspector_header_id", inspectorET.getText().toString(), true, true);
            doc.getRange().replace("area_header_id", areaOwnerET.getText().toString(), true, true);

            PackageInfo pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            doc.getRange().replace("version_id", "Version : " + version, true, true);

            TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
            String deviceID = telephonyManager.getDeviceId();
            doc.getRange().replace("device_id", "Imei : " + deviceID, true, true);


            DataSource ds = new DataSource(mActivity);
            ds.open();

            /* ==================================
                                    Work Area
            ================================== */
            final String potenWa1, potenWa2, potenWa3, potenWa4, potenWa5, potenWa6;
            final String ratingWa1, ratingWa2, ratingWa3, ratingWa4, ratingWa5, ratingWa6;
            PlannedInspectionModel workAreaData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Work Area");
            if (workAreaData != null) {
                if (workAreaData.getRisk1() == null) {
                    potenWa1 = "";
                } else {
                    potenWa1 = workAreaData.getRisk1();
                }
                if (workAreaData.getRisk2() == null) {
                    potenWa2 = "";
                } else {
                    potenWa2 = workAreaData.getRisk2();
                }
                if (workAreaData.getRisk3() == null) {
                    potenWa3 = "";
                } else {
                    potenWa3 = workAreaData.getRisk3();
                }
                if (workAreaData.getRisk4() == null) {
                    potenWa4 = "";
                } else {
                    potenWa4 = workAreaData.getRisk4();
                }
                if (workAreaData.getRisk5() == null) {
                    potenWa5 = "";
                } else {
                    potenWa5 = workAreaData.getRisk5();
                }
                if (workAreaData.getRisk6() == null) {
                    potenWa6 = "";
                } else {
                    potenWa6 = workAreaData.getRisk6();
                }

                doc.getRange().replace("pr_1", potenWa1, true, true);
                doc.getRange().replace("pr_2", potenWa2, true, true);
                doc.getRange().replace("pr_3", potenWa3, true, true);
                doc.getRange().replace("pr_4", potenWa4, true, true);
                doc.getRange().replace("pr_5", potenWa5, true, true);
                doc.getRange().replace("pr_6", potenWa6, true, true);

                if (workAreaData.getRating1() == null) {
                    ratingWa1 = "";
                } else {
                    ratingWa1 = workAreaData.getRating1();
                }
                if (workAreaData.getRating2() == null) {
                    ratingWa2 = "";
                } else {
                    ratingWa2 = workAreaData.getRating2();
                }
                if (workAreaData.getRating3() == null) {
                    ratingWa3 = "";
                } else {
                    ratingWa3 = workAreaData.getRating3();
                }
                if (workAreaData.getRating4() == null) {
                    ratingWa4 = "";
                } else {
                    ratingWa4 = workAreaData.getRating4();
                }
                if (workAreaData.getRating5() == null) {
                    ratingWa5 = "";
                } else {
                    ratingWa5 = workAreaData.getRating5();
                }
                if (workAreaData.getRating6() == null) {
                    ratingWa6 = "";
                } else {
                    ratingWa6 = workAreaData.getRating6();
                }

                if (ratingWa1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_1", passCheck, true, true);
                    doc.getRange().replace("g_1", "", true, true);
                    doc.getRange().replace("na1", "", true, true);
                } else if (ratingWa1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_1", "", true, true);
                    doc.getRange().replace("g_1", passCheck, true, true);
                    doc.getRange().replace("na1", "", true, true);
                } else if (ratingWa1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_1", "", true, true);
                    doc.getRange().replace("g_1", "", true, true);
                    doc.getRange().replace("na1", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_1", "", true, true);
                    doc.getRange().replace("g_1", "", true, true);
                    doc.getRange().replace("na1", "", true, true);
                }

                if (ratingWa2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_2", passCheck, true, true);
                    doc.getRange().replace("g_2", "", true, true);
                    doc.getRange().replace("na2", "", true, true);
                } else if (ratingWa2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_2", "", true, true);
                    doc.getRange().replace("g_2", passCheck, true, true);
                    doc.getRange().replace("na2", "", true, true);
                } else if (ratingWa2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_2", "", true, true);
                    doc.getRange().replace("g_2", "", true, true);
                    doc.getRange().replace("na2", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_2", "", true, true);
                    doc.getRange().replace("g_2", "", true, true);
                    doc.getRange().replace("na2", "", true, true);
                }

                if (ratingWa3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_3", passCheck, true, true);
                    doc.getRange().replace("g_3", "", true, true);
                    doc.getRange().replace("na3", "", true, true);
                } else if (ratingWa3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_3", "", true, true);
                    doc.getRange().replace("g_3", passCheck, true, true);
                    doc.getRange().replace("na3", "", true, true);
                } else if (ratingWa3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_3", "", true, true);
                    doc.getRange().replace("g_3", "", true, true);
                    doc.getRange().replace("na3", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_3", "", true, true);
                    doc.getRange().replace("g_3", "", true, true);
                    doc.getRange().replace("na3", "", true, true);
                }

                if (ratingWa4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_4", passCheck, true, true);
                    doc.getRange().replace("g_4", "", true, true);
                    doc.getRange().replace("na4", "", true, true);
                } else if (ratingWa4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_4", "", true, true);
                    doc.getRange().replace("g_4", passCheck, true, true);
                    doc.getRange().replace("na4", "", true, true);
                } else if (ratingWa4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_4", "", true, true);
                    doc.getRange().replace("g_4", "", true, true);
                    doc.getRange().replace("na4", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_4", "", true, true);
                    doc.getRange().replace("g_4", "", true, true);
                    doc.getRange().replace("na4", "", true, true);
                }

                if (ratingWa5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_5", passCheck, true, true);
                    doc.getRange().replace("g_5", "", true, true);
                    doc.getRange().replace("na5", "", true, true);
                } else if (ratingWa5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_5", "", true, true);
                    doc.getRange().replace("g_5", passCheck, true, true);
                    doc.getRange().replace("na5", "", true, true);
                } else if (ratingWa5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_5", "", true, true);
                    doc.getRange().replace("g_5", "", true, true);
                    doc.getRange().replace("na5", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_5", "", true, true);
                    doc.getRange().replace("g_5", "", true, true);
                    doc.getRange().replace("na5", "", true, true);
                }

                if (ratingWa6.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_6", passCheck, true, true);
                    doc.getRange().replace("g_6", "", true, true);
                    doc.getRange().replace("na6", "", true, true);
                } else if (ratingWa6.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_6", "", true, true);
                    doc.getRange().replace("g_6", passCheck, true, true);
                    doc.getRange().replace("na6", "", true, true);
                } else if (ratingWa6.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_6", "", true, true);
                    doc.getRange().replace("g_6", "", true, true);
                    doc.getRange().replace("na6", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_6", "", true, true);
                    doc.getRange().replace("g_6", "", true, true);
                    doc.getRange().replace("na6", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_1", "", true, true);
                doc.getRange().replace("pr_2", "", true, true);
                doc.getRange().replace("pr_3", "", true, true);
                doc.getRange().replace("pr_4", "", true, true);
                doc.getRange().replace("pr_5", "", true, true);
                doc.getRange().replace("pr_6", "", true, true);

                doc.getRange().replace("nw_1", "", true, true);
                doc.getRange().replace("g_1", "", true, true);
                doc.getRange().replace("na1", "", true, true);

                doc.getRange().replace("nw_2", "", true, true);
                doc.getRange().replace("g_2", "", true, true);
                doc.getRange().replace("na2", "", true, true);

                doc.getRange().replace("nw_3", "", true, true);
                doc.getRange().replace("g_3", "", true, true);
                doc.getRange().replace("na3", "", true, true);

                doc.getRange().replace("nw_4", "", true, true);
                doc.getRange().replace("g_4", "", true, true);
                doc.getRange().replace("na4", "", true, true);

                doc.getRange().replace("nw_5", "", true, true);
                doc.getRange().replace("g_5", "", true, true);
                doc.getRange().replace("na5", "", true, true);

                doc.getRange().replace("nw_6", "", true, true);
                doc.getRange().replace("g_6", "", true, true);
                doc.getRange().replace("na6", "", true, true);
            }

              /* ==================================
                                    Structures
            ================================== */
            final String potenSt1, potenSt2, potenSt3, potenSt4, potenSt5;
            final String ratingSt1, ratingSt2, ratingSt3, ratingSt4, ratingSt5;
            PlannedInspectionModel structureData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Structure");
            if (structureData != null) {
                if (structureData.getRisk1() == null) {
                    potenSt1 = "";
                } else {
                    potenSt1 = structureData.getRisk1();
                }
                if (structureData.getRisk2() == null) {
                    potenSt2 = "";
                } else {
                    potenSt2 = structureData.getRisk2();
                }
                if (structureData.getRisk3() == null) {
                    potenSt3 = "";
                } else {
                    potenSt3 = structureData.getRisk3();
                }
                if (structureData.getRisk4() == null) {
                    potenSt4 = "";
                } else {
                    potenSt4 = structureData.getRisk4();
                }
                if (structureData.getRisk5() == null) {
                    potenSt5 = "";
                } else {
                    potenSt5 = structureData.getRisk5();
                }

                doc.getRange().replace("pr_7", potenSt1, true, true);
                doc.getRange().replace("pr_8", potenSt2, true, true);
                doc.getRange().replace("pr_9", potenSt3, true, true);
                doc.getRange().replace("pr_10", potenSt4, true, true);
                doc.getRange().replace("pr_11", potenSt5, true, true);

                if (structureData.getRating1() == null) {
                    ratingSt1 = "";
                } else {
                    ratingSt1 = structureData.getRating1();
                }
                if (structureData.getRating2() == null) {
                    ratingSt2 = "";
                } else {
                    ratingSt2 = structureData.getRating2();
                }
                if (structureData.getRating3() == null) {
                    ratingSt3 = "";
                } else {
                    ratingSt3 = structureData.getRating3();
                }
                if (structureData.getRating4() == null) {
                    ratingSt4 = "";
                } else {
                    ratingSt4 = structureData.getRating4();
                }
                if (structureData.getRating5() == null) {
                    ratingSt5 = "";
                } else {
                    ratingSt5 = structureData.getRating5();
                }

                if (ratingSt1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_7", passCheck, true, true);
                    doc.getRange().replace("g_7", "", true, true);
                    doc.getRange().replace("na7", "", true, true);
                } else if (ratingSt1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_7", "", true, true);
                    doc.getRange().replace("g_7", passCheck, true, true);
                    doc.getRange().replace("na7", "", true, true);
                } else if (ratingSt1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_7", "", true, true);
                    doc.getRange().replace("g_7", "", true, true);
                    doc.getRange().replace("na7", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_7", "", true, true);
                    doc.getRange().replace("g_7", "", true, true);
                    doc.getRange().replace("na7", "", true, true);
                }

                if (ratingSt2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_8", passCheck, true, true);
                    doc.getRange().replace("g_8", "", true, true);
                    doc.getRange().replace("na8", "", true, true);
                } else if (ratingSt2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_8", "", true, true);
                    doc.getRange().replace("g_8", passCheck, true, true);
                    doc.getRange().replace("na8", "", true, true);
                } else if (ratingSt2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_8", "", true, true);
                    doc.getRange().replace("g_8", "", true, true);
                    doc.getRange().replace("na8", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_8", "", true, true);
                    doc.getRange().replace("g_8", "", true, true);
                    doc.getRange().replace("na8", "", true, true);
                }

                if (ratingSt3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_9", passCheck, true, true);
                    doc.getRange().replace("g_9", "", true, true);
                    doc.getRange().replace("na9", "", true, true);
                } else if (ratingSt3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_9", "", true, true);
                    doc.getRange().replace("g_9", passCheck, true, true);
                    doc.getRange().replace("na9", "", true, true);
                } else if (ratingSt3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_9", "", true, true);
                    doc.getRange().replace("g_9", "", true, true);
                    doc.getRange().replace("na9", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_9", "", true, true);
                    doc.getRange().replace("g_9", "", true, true);
                    doc.getRange().replace("na9", "", true, true);
                }

                if (ratingSt4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_10", passCheck, true, true);
                    doc.getRange().replace("g_10", "", true, true);
                    doc.getRange().replace("na10", "", true, true);
                } else if (ratingSt4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_10", "", true, true);
                    doc.getRange().replace("g_10", passCheck, true, true);
                    doc.getRange().replace("na10", "", true, true);
                } else if (ratingSt4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_10", "", true, true);
                    doc.getRange().replace("g_10", "", true, true);
                    doc.getRange().replace("na10", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_10", "", true, true);
                    doc.getRange().replace("g_10", "", true, true);
                    doc.getRange().replace("na10", "", true, true);
                }

                if (ratingSt5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_11", passCheck, true, true);
                    doc.getRange().replace("g_11", "", true, true);
                    doc.getRange().replace("na11", "", true, true);
                } else if (ratingSt5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_11", "", true, true);
                    doc.getRange().replace("g_11", passCheck, true, true);
                    doc.getRange().replace("na11", "", true, true);
                } else if (ratingSt5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_11", "", true, true);
                    doc.getRange().replace("g_11", "", true, true);
                    doc.getRange().replace("na11", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_11", "", true, true);
                    doc.getRange().replace("g_11", "", true, true);
                    doc.getRange().replace("na11", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_7", "", true, true);
                doc.getRange().replace("pr_8", "", true, true);
                doc.getRange().replace("pr_9", "", true, true);
                doc.getRange().replace("pr_10", "", true, true);
                doc.getRange().replace("pr_11", "", true, true);

                doc.getRange().replace("nw_7", "", true, true);
                doc.getRange().replace("g_7", "", true, true);
                doc.getRange().replace("na7", "", true, true);

                doc.getRange().replace("nw_8", "", true, true);
                doc.getRange().replace("g_8", "", true, true);
                doc.getRange().replace("na8", "", true, true);

                doc.getRange().replace("nw_9", "", true, true);
                doc.getRange().replace("g_9", "", true, true);
                doc.getRange().replace("na9", "", true, true);

                doc.getRange().replace("nw_10", "", true, true);
                doc.getRange().replace("g_10", "", true, true);
                doc.getRange().replace("na10", "", true, true);

                doc.getRange().replace("nw_11", "", true, true);
                doc.getRange().replace("g_11", "", true, true);
                doc.getRange().replace("na11", "", true, true);
            }

            /* ==================================
                                    Machinery
            ================================== */
            final String potenMt1, potenMt2, potenMt3, potenMt4, potenMt5, potenMt6, potenMt7;
            final String ratingMt1, ratingMt2, ratingMt3, ratingMt4, ratingMt5, ratingMt6, ratingMt7;
            PlannedInspectionModel machineryData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Machinery");
            if (machineryData != null) {
                if (machineryData.getRisk1() == null) {
                    potenMt1 = "";
                } else {
                    potenMt1 = machineryData.getRisk1();
                }
                if (machineryData.getRisk2() == null) {
                    potenMt2 = "";
                } else {
                    potenMt2 = machineryData.getRisk2();
                }
                if (machineryData.getRisk3() == null) {
                    potenMt3 = "";
                } else {
                    potenMt3 = machineryData.getRisk3();
                }
                if (machineryData.getRisk4() == null) {
                    potenMt4 = "";
                } else {
                    potenMt4 = machineryData.getRisk4();
                }
                if (machineryData.getRisk5() == null) {
                    potenMt5 = "";
                } else {
                    potenMt5 = machineryData.getRisk5();
                }
                if (machineryData.getRisk6() == null) {
                    potenMt6 = "";
                } else {
                    potenMt6 = machineryData.getRisk6();
                }
                if (machineryData.getRisk7() == null) {
                    potenMt7 = "";
                } else {
                    potenMt7 = machineryData.getRisk7();
                }

                doc.getRange().replace("pr_12", potenMt1, true, true);
                doc.getRange().replace("pr_13", potenMt2, true, true);
                doc.getRange().replace("pr_14", potenMt3, true, true);
                doc.getRange().replace("pr_15", potenMt4, true, true);
                doc.getRange().replace("pr_16", potenMt5, true, true);
                doc.getRange().replace("pr_17", potenMt6, true, true);
                doc.getRange().replace("pr_18", potenMt7, true, true);

                if (machineryData.getRating1() == null) {
                    ratingMt1 = "";
                } else {
                    ratingMt1 = machineryData.getRating1();
                }
                if (machineryData.getRating2() == null) {
                    ratingMt2 = "";
                } else {
                    ratingMt2 = machineryData.getRating2();
                }
                if (machineryData.getRating3() == null) {
                    ratingMt3 = "";
                } else {
                    ratingMt3 = machineryData.getRating3();
                }
                if (machineryData.getRating4() == null) {
                    ratingMt4 = "";
                } else {
                    ratingMt4 = machineryData.getRating4();
                }
                if (machineryData.getRating5() == null) {
                    ratingMt5 = "";
                } else {
                    ratingMt5 = machineryData.getRating5();
                }
                if (machineryData.getRating6() == null) {
                    ratingMt6 = "";
                } else {
                    ratingMt6 = machineryData.getRating6();
                }
                if (machineryData.getRating7() == null) {
                    ratingMt7 = "";
                } else {
                    ratingMt7 = machineryData.getRating7();
                }

                if (ratingMt1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_12", passCheck, true, true);
                    doc.getRange().replace("g_12", "", true, true);
                    doc.getRange().replace("na12", "", true, true);
                } else if (ratingMt1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_12", "", true, true);
                    doc.getRange().replace("g_12", passCheck, true, true);
                    doc.getRange().replace("na12", "", true, true);
                } else if (ratingMt1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_12", "", true, true);
                    doc.getRange().replace("g_12", "", true, true);
                    doc.getRange().replace("na12", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_12", "", true, true);
                    doc.getRange().replace("g_12", "", true, true);
                    doc.getRange().replace("na12", "", true, true);
                }

                if (ratingMt2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_13", passCheck, true, true);
                    doc.getRange().replace("g_13", "", true, true);
                    doc.getRange().replace("na13", "", true, true);
                } else if (ratingMt2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_13", "", true, true);
                    doc.getRange().replace("g_13", passCheck, true, true);
                    doc.getRange().replace("na13", "", true, true);
                } else if (ratingMt2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_13", "", true, true);
                    doc.getRange().replace("g_13", "", true, true);
                    doc.getRange().replace("na13", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_13", "", true, true);
                    doc.getRange().replace("g_13", "", true, true);
                    doc.getRange().replace("na13", "", true, true);
                }

                if (ratingMt3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_14", passCheck, true, true);
                    doc.getRange().replace("g_14", "", true, true);
                    doc.getRange().replace("na14", "", true, true);
                } else if (ratingMt3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_14", "", true, true);
                    doc.getRange().replace("g_14", passCheck, true, true);
                    doc.getRange().replace("na14", "", true, true);
                } else if (ratingMt3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_14", "", true, true);
                    doc.getRange().replace("g_14", "", true, true);
                    doc.getRange().replace("na14", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_14", "", true, true);
                    doc.getRange().replace("g_14", "", true, true);
                    doc.getRange().replace("na14", "", true, true);
                }

                if (ratingMt4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_15", passCheck, true, true);
                    doc.getRange().replace("g_15", "", true, true);
                    doc.getRange().replace("na15", "", true, true);
                } else if (ratingMt4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_15", "", true, true);
                    doc.getRange().replace("g_15", passCheck, true, true);
                    doc.getRange().replace("na15", "", true, true);
                } else if (ratingMt4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_15", "", true, true);
                    doc.getRange().replace("g_15", "", true, true);
                    doc.getRange().replace("na15", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_15", "", true, true);
                    doc.getRange().replace("g_15", "", true, true);
                    doc.getRange().replace("na15", "", true, true);
                }

                if (ratingMt5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_16", passCheck, true, true);
                    doc.getRange().replace("g_16", "", true, true);
                    doc.getRange().replace("na16", "", true, true);
                } else if (ratingMt5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_16", "", true, true);
                    doc.getRange().replace("g_16", passCheck, true, true);
                    doc.getRange().replace("na16", "", true, true);
                } else if (ratingMt5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_16", "", true, true);
                    doc.getRange().replace("g_16", "", true, true);
                    doc.getRange().replace("na16", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_16", "", true, true);
                    doc.getRange().replace("g_16", "", true, true);
                    doc.getRange().replace("na16", "", true, true);
                }

                if (ratingMt6.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_17", passCheck, true, true);
                    doc.getRange().replace("g_17", "", true, true);
                    doc.getRange().replace("na17", "", true, true);
                } else if (ratingMt6.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_17", "", true, true);
                    doc.getRange().replace("g_17", passCheck, true, true);
                    doc.getRange().replace("na17", "", true, true);
                } else if (ratingMt6.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_17", "", true, true);
                    doc.getRange().replace("g_17", "", true, true);
                    doc.getRange().replace("na17", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_17", "", true, true);
                    doc.getRange().replace("g_17", "", true, true);
                    doc.getRange().replace("na17", "", true, true);
                }

                if (ratingMt7.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_18", passCheck, true, true);
                    doc.getRange().replace("g_18", "", true, true);
                    doc.getRange().replace("na18", "", true, true);
                } else if (ratingMt7.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_18", "", true, true);
                    doc.getRange().replace("g_18", passCheck, true, true);
                    doc.getRange().replace("na18", "", true, true);
                } else if (ratingMt7.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_18", "", true, true);
                    doc.getRange().replace("g_18", "", true, true);
                    doc.getRange().replace("na18", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_18", "", true, true);
                    doc.getRange().replace("g_18", "", true, true);
                    doc.getRange().replace("na18", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_12", "", true, true);
                doc.getRange().replace("pr_13", "", true, true);
                doc.getRange().replace("pr_14", "", true, true);
                doc.getRange().replace("pr_15", "", true, true);
                doc.getRange().replace("pr_16", "", true, true);
                doc.getRange().replace("pr_17", "", true, true);
                doc.getRange().replace("pr_18", "", true, true);

                doc.getRange().replace("nw_12", "", true, true);
                doc.getRange().replace("g_12", "", true, true);
                doc.getRange().replace("na12", "", true, true);

                doc.getRange().replace("nw_13", "", true, true);
                doc.getRange().replace("g_13", "", true, true);
                doc.getRange().replace("na13", "", true, true);

                doc.getRange().replace("nw_14", "", true, true);
                doc.getRange().replace("g_14", "", true, true);
                doc.getRange().replace("na14", "", true, true);

                doc.getRange().replace("nw_15", "", true, true);
                doc.getRange().replace("g_15", "", true, true);
                doc.getRange().replace("na15", "", true, true);

                doc.getRange().replace("nw_16", "", true, true);
                doc.getRange().replace("g_16", "", true, true);
                doc.getRange().replace("na16", "", true, true);

                doc.getRange().replace("nw_17", "", true, true);
                doc.getRange().replace("g_17", "", true, true);
                doc.getRange().replace("na17", "", true, true);

                doc.getRange().replace("nw_18", "", true, true);
                doc.getRange().replace("g_18", "", true, true);
                doc.getRange().replace("na18", "", true, true);
            }

            /* ==================================
                                    Electrical
            ================================== */
            final String potenEl1, potenEl2, potenEl3, potenEl4, potenEl5;
            final String ratingEl1, ratingEl2, ratingEl3, ratingEl4, ratingEl5;
            PlannedInspectionModel electricalData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Electrical");
            if (electricalData != null) {
                if (electricalData.getRisk1() == null) {
                    potenEl1 = "";
                } else {
                    potenEl1 = electricalData.getRisk1();
                }
                if (electricalData.getRisk2() == null) {
                    potenEl2 = "";
                } else {
                    potenEl2 = electricalData.getRisk2();
                }
                if (electricalData.getRisk3() == null) {
                    potenEl3 = "";
                } else {
                    potenEl3 = electricalData.getRisk3();
                }
                if (electricalData.getRisk4() == null) {
                    potenEl4 = "";
                } else {
                    potenEl4 = electricalData.getRisk4();
                }
                if (electricalData.getRisk5() == null) {
                    potenEl5 = "";
                } else {
                    potenEl5 = electricalData.getRisk5();
                }

                doc.getRange().replace("pr_19", potenEl1, true, true);
                doc.getRange().replace("pr_20", potenEl2, true, true);
                doc.getRange().replace("pr_21", potenEl3, true, true);
                doc.getRange().replace("pr_22", potenEl4, true, true);
                doc.getRange().replace("pr_23", potenEl5, true, true);

                if (electricalData.getRating1() == null) {
                    ratingEl1 = "";
                } else {
                    ratingEl1 = electricalData.getRating1();
                }
                if (electricalData.getRating2() == null) {
                    ratingEl2 = "";
                } else {
                    ratingEl2 = electricalData.getRating2();
                }
                if (electricalData.getRating3() == null) {
                    ratingEl3 = "";
                } else {
                    ratingEl3 = electricalData.getRating3();
                }
                if (electricalData.getRating4() == null) {
                    ratingEl4 = "";
                } else {
                    ratingEl4 = electricalData.getRating4();
                }
                if (electricalData.getRating5() == null) {
                    ratingEl5 = "";
                } else {
                    ratingEl5 = electricalData.getRating5();
                }

                if (ratingEl1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_19", passCheck, true, true);
                    doc.getRange().replace("g_19", "", true, true);
                    doc.getRange().replace("na19", "", true, true);
                } else if (ratingEl1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_19", "", true, true);
                    doc.getRange().replace("g_19", passCheck, true, true);
                    doc.getRange().replace("na19", "", true, true);
                } else if (ratingEl1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_19", "", true, true);
                    doc.getRange().replace("g_19", "", true, true);
                    doc.getRange().replace("na19", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_19", "", true, true);
                    doc.getRange().replace("g_19", "", true, true);
                    doc.getRange().replace("na19", "", true, true);
                }

                if (ratingEl2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_20", passCheck, true, true);
                    doc.getRange().replace("g_20", "", true, true);
                    doc.getRange().replace("na20", "", true, true);
                } else if (ratingEl2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_20", "", true, true);
                    doc.getRange().replace("g_20", passCheck, true, true);
                    doc.getRange().replace("na20", "", true, true);
                } else if (ratingEl2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_20", "", true, true);
                    doc.getRange().replace("g_20", "", true, true);
                    doc.getRange().replace("na20", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_20", "", true, true);
                    doc.getRange().replace("g_20", "", true, true);
                    doc.getRange().replace("na20", "", true, true);
                }

                if (ratingEl3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_21", passCheck, true, true);
                    doc.getRange().replace("g_21", "", true, true);
                    doc.getRange().replace("na21", "", true, true);
                } else if (ratingEl3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_21", "", true, true);
                    doc.getRange().replace("g_21", passCheck, true, true);
                    doc.getRange().replace("na21", "", true, true);
                } else if (ratingEl3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_21", "", true, true);
                    doc.getRange().replace("g_21", "", true, true);
                    doc.getRange().replace("na21", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_21", "", true, true);
                    doc.getRange().replace("g_21", "", true, true);
                    doc.getRange().replace("na21", "", true, true);
                }

                if (ratingEl4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_22", passCheck, true, true);
                    doc.getRange().replace("g_22", "", true, true);
                    doc.getRange().replace("na22", "", true, true);
                } else if (ratingEl4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_22", "", true, true);
                    doc.getRange().replace("g_22", passCheck, true, true);
                    doc.getRange().replace("na22", "", true, true);
                } else if (ratingEl4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_22", "", true, true);
                    doc.getRange().replace("g_22", "", true, true);
                    doc.getRange().replace("na22", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_22", "", true, true);
                    doc.getRange().replace("g_22", "", true, true);
                    doc.getRange().replace("na22", "", true, true);
                }

                if (ratingEl5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_23", passCheck, true, true);
                    doc.getRange().replace("g_23", "", true, true);
                    doc.getRange().replace("na23", "", true, true);
                } else if (ratingEl5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_23", "", true, true);
                    doc.getRange().replace("g_23", passCheck, true, true);
                    doc.getRange().replace("na23", "", true, true);
                } else if (ratingEl5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_23", "", true, true);
                    doc.getRange().replace("g_23", "", true, true);
                    doc.getRange().replace("na23", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_23", "", true, true);
                    doc.getRange().replace("g_23", "", true, true);
                    doc.getRange().replace("na23", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_19", "", true, true);
                doc.getRange().replace("pr_20", "", true, true);
                doc.getRange().replace("pr_21", "", true, true);
                doc.getRange().replace("pr_22", "", true, true);
                doc.getRange().replace("pr_23", "", true, true);

                doc.getRange().replace("nw_19", "", true, true);
                doc.getRange().replace("g_19", "", true, true);
                doc.getRange().replace("na19", "", true, true);

                doc.getRange().replace("nw_20", "", true, true);
                doc.getRange().replace("g_20", "", true, true);
                doc.getRange().replace("na20", "", true, true);

                doc.getRange().replace("nw_21", "", true, true);
                doc.getRange().replace("g_21", "", true, true);
                doc.getRange().replace("na21", "", true, true);

                doc.getRange().replace("nw_22", "", true, true);
                doc.getRange().replace("g_22", "", true, true);
                doc.getRange().replace("na22", "", true, true);

                doc.getRange().replace("nw_23", "", true, true);
                doc.getRange().replace("g_23", "", true, true);
                doc.getRange().replace("na23", "", true, true);
            }

             /* ==================================
                                    Personel
            ================================== */
            final String potenPe1, potenPe2, potenPe3, potenPe4, potenPe5, potenPe6, potenPe7, potenPe8;
            final String ratingPe1, ratingPe2, ratingPe3, ratingPe4, ratingPe5, ratingPe6, ratingPe7, ratingPe8;
            PlannedInspectionModel peronelData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Personel");
            if (peronelData != null) {
                if (peronelData.getRisk1() == null) {
                    potenPe1 = "";
                } else {
                    potenPe1 = peronelData.getRisk1();
                }
                if (peronelData.getRisk2() == null) {
                    potenPe2 = "";
                } else {
                    potenPe2 = peronelData.getRisk2();
                }
                if (peronelData.getRisk3() == null) {
                    potenPe3 = "";
                } else {
                    potenPe3 = peronelData.getRisk3();
                }
                if (peronelData.getRisk4() == null) {
                    potenPe4 = "";
                } else {
                    potenPe4 = peronelData.getRisk4();
                }
                if (peronelData.getRisk5() == null) {
                    potenPe5 = "";
                } else {
                    potenPe5 = peronelData.getRisk5();
                }
                if (peronelData.getRisk6() == null) {
                    potenPe6 = "";
                } else {
                    potenPe6 = peronelData.getRisk6();
                }
                if (peronelData.getRisk7() == null) {
                    potenPe7 = "";
                } else {
                    potenPe7 = peronelData.getRisk7();
                }
                if (peronelData.getRisk8() == null) {
                    potenPe8 = "";
                } else {
                    potenPe8 = peronelData.getRisk8();
                }

                doc.getRange().replace("pr_24", potenPe1, true, true);
                doc.getRange().replace("pr_25", potenPe2, true, true);
                doc.getRange().replace("pr_26", potenPe3, true, true);
                doc.getRange().replace("pr_27", potenPe4, true, true);
                doc.getRange().replace("pr_28", potenPe5, true, true);
                doc.getRange().replace("pr_29", potenPe6, true, true);
                doc.getRange().replace("pr_30", potenPe7, true, true);
                doc.getRange().replace("pr_31", potenPe8, true, true);

                if (peronelData.getRating1() == null) {
                    ratingPe1 = "";
                } else {
                    ratingPe1 = peronelData.getRating1();
                }
                if (peronelData.getRating2() == null) {
                    ratingPe2 = "";
                } else {
                    ratingPe2 = peronelData.getRating2();
                }
                if (peronelData.getRating3() == null) {
                    ratingPe3 = "";
                } else {
                    ratingPe3 = peronelData.getRating3();
                }
                if (peronelData.getRating4() == null) {
                    ratingPe4 = "";
                } else {
                    ratingPe4 = peronelData.getRating4();
                }
                if (peronelData.getRating5() == null) {
                    ratingPe5 = "";
                } else {
                    ratingPe5 = peronelData.getRating5();
                }
                if (peronelData.getRating6() == null) {
                    ratingPe6 = "";
                } else {
                    ratingPe6 = peronelData.getRating6();
                }
                if (peronelData.getRating7() == null) {
                    ratingPe7 = "";
                } else {
                    ratingPe7 = peronelData.getRating7();
                }
                if (peronelData.getRating8() == null) {
                    ratingPe8 = "";
                } else {
                    ratingPe8 = peronelData.getRating8();
                }

                if (ratingPe1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_24", passCheck, true, true);
                    doc.getRange().replace("g_24", "", true, true);
                    doc.getRange().replace("na24", "", true, true);
                } else if (ratingPe1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_24", "", true, true);
                    doc.getRange().replace("g_24", passCheck, true, true);
                    doc.getRange().replace("na24", "", true, true);
                } else if (ratingPe1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_24", "", true, true);
                    doc.getRange().replace("g_24", "", true, true);
                    doc.getRange().replace("na24", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_24", "", true, true);
                    doc.getRange().replace("g_24", "", true, true);
                    doc.getRange().replace("na24", "", true, true);
                }

                if (ratingPe2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_25", passCheck, true, true);
                    doc.getRange().replace("g_25", "", true, true);
                    doc.getRange().replace("na25", "", true, true);
                } else if (ratingPe2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_25", "", true, true);
                    doc.getRange().replace("g_25", passCheck, true, true);
                    doc.getRange().replace("na25", "", true, true);
                } else if (ratingPe2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_25", "", true, true);
                    doc.getRange().replace("g_25", "", true, true);
                    doc.getRange().replace("na25", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_25", "", true, true);
                    doc.getRange().replace("g_25", "", true, true);
                    doc.getRange().replace("na25", "", true, true);
                }

                if (ratingPe3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_26", passCheck, true, true);
                    doc.getRange().replace("g_26", "", true, true);
                    doc.getRange().replace("na26", "", true, true);
                } else if (ratingPe3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_26", "", true, true);
                    doc.getRange().replace("g_26", passCheck, true, true);
                    doc.getRange().replace("na26", "", true, true);
                } else if (ratingPe3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_26", "", true, true);
                    doc.getRange().replace("g_26", "", true, true);
                    doc.getRange().replace("na26", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_26", "", true, true);
                    doc.getRange().replace("g_26", "", true, true);
                    doc.getRange().replace("na26", "", true, true);
                }

                if (ratingPe4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_27", passCheck, true, true);
                    doc.getRange().replace("g_27", "", true, true);
                    doc.getRange().replace("na27", "", true, true);
                } else if (ratingPe4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_27", "", true, true);
                    doc.getRange().replace("g_27", passCheck, true, true);
                    doc.getRange().replace("na27", "", true, true);
                } else if (ratingPe4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_27", "", true, true);
                    doc.getRange().replace("g_27", "", true, true);
                    doc.getRange().replace("na27", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_27", "", true, true);
                    doc.getRange().replace("g_27", "", true, true);
                    doc.getRange().replace("na27", "", true, true);
                }

                if (ratingPe5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_28", passCheck, true, true);
                    doc.getRange().replace("g_28", "", true, true);
                    doc.getRange().replace("na28", "", true, true);
                } else if (ratingPe5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_28", "", true, true);
                    doc.getRange().replace("g_28", passCheck, true, true);
                    doc.getRange().replace("na28", "", true, true);
                } else if (ratingPe5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_28", "", true, true);
                    doc.getRange().replace("g_28", "", true, true);
                    doc.getRange().replace("na28", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_28", "", true, true);
                    doc.getRange().replace("g_28", "", true, true);
                    doc.getRange().replace("na28", "", true, true);
                }

                if (ratingPe6.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_29", passCheck, true, true);
                    doc.getRange().replace("g_29", "", true, true);
                    doc.getRange().replace("na29", "", true, true);
                } else if (ratingPe6.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_29", "", true, true);
                    doc.getRange().replace("g_29", passCheck, true, true);
                    doc.getRange().replace("na29", "", true, true);
                } else if (ratingPe6.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_29", "", true, true);
                    doc.getRange().replace("g_29", "", true, true);
                    doc.getRange().replace("na29", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_29", "", true, true);
                    doc.getRange().replace("g_29", "", true, true);
                    doc.getRange().replace("na29", "", true, true);
                }

                if (ratingPe7.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_30", passCheck, true, true);
                    doc.getRange().replace("g_30", "", true, true);
                    doc.getRange().replace("na30", "", true, true);
                } else if (ratingPe7.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_30", "", true, true);
                    doc.getRange().replace("g_30", passCheck, true, true);
                    doc.getRange().replace("na30", "", true, true);
                } else if (ratingPe7.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_30", "", true, true);
                    doc.getRange().replace("g_30", "", true, true);
                    doc.getRange().replace("na30", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_30", "", true, true);
                    doc.getRange().replace("g_30", "", true, true);
                    doc.getRange().replace("na30", "", true, true);
                }

                if (ratingPe8.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_31", passCheck, true, true);
                    doc.getRange().replace("g_31", "", true, true);
                    doc.getRange().replace("na31", "", true, true);
                } else if (ratingPe8.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_31", "", true, true);
                    doc.getRange().replace("g_31", passCheck, true, true);
                    doc.getRange().replace("na31", "", true, true);
                } else if (ratingPe8.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_31", "", true, true);
                    doc.getRange().replace("g_31", "", true, true);
                    doc.getRange().replace("na31", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_31", "", true, true);
                    doc.getRange().replace("g_31", "", true, true);
                    doc.getRange().replace("na31", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_24", "", true, true);
                doc.getRange().replace("pr_25", "", true, true);
                doc.getRange().replace("pr_26", "", true, true);
                doc.getRange().replace("pr_27", "", true, true);
                doc.getRange().replace("pr_28", "", true, true);
                doc.getRange().replace("pr_29", "", true, true);
                doc.getRange().replace("pr_30", "", true, true);
                doc.getRange().replace("pr_31", "", true, true);

                doc.getRange().replace("nw_24", "", true, true);
                doc.getRange().replace("g_24", "", true, true);
                doc.getRange().replace("na24", "", true, true);

                doc.getRange().replace("nw_2", "", true, true);
                doc.getRange().replace("g_25", "", true, true);
                doc.getRange().replace("na25", "", true, true);

                doc.getRange().replace("nw_26", "", true, true);
                doc.getRange().replace("g_26", "", true, true);
                doc.getRange().replace("na26", "", true, true);

                doc.getRange().replace("nw_27", "", true, true);
                doc.getRange().replace("g_27", "", true, true);
                doc.getRange().replace("na27", "", true, true);

                doc.getRange().replace("nw_28", "", true, true);
                doc.getRange().replace("g_28", "", true, true);
                doc.getRange().replace("na28", "", true, true);

                doc.getRange().replace("nw_29", "", true, true);
                doc.getRange().replace("g_29", "", true, true);
                doc.getRange().replace("na29", "", true, true);

                doc.getRange().replace("nw_30", "", true, true);
                doc.getRange().replace("g_30", "", true, true);
                doc.getRange().replace("na30", "", true, true);

                doc.getRange().replace("nw_31", "", true, true);
                doc.getRange().replace("g_31", "", true, true);
                doc.getRange().replace("na31", "", true, true);
            }

            /* ==================================
                                    Fire Safety
            ================================== */
            final String potenFS1, potenFS2, potenFS3, potenFS4, potenFS5;
            final String ratingFS1, ratingFS2, ratingFS3, ratingFS4, ratingFS5;
            PlannedInspectionModel fireSafetyData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Fire Safety");
            if (fireSafetyData != null) {
                if (fireSafetyData.getRisk1() == null) {
                    potenFS1 = "";
                } else {
                    potenFS1 = fireSafetyData.getRisk1();
                }
                if (fireSafetyData.getRisk2() == null) {
                    potenFS2 = "";
                } else {
                    potenFS2 = fireSafetyData.getRisk2();
                }
                if (fireSafetyData.getRisk3() == null) {
                    potenFS3 = "";
                } else {
                    potenFS3 = fireSafetyData.getRisk3();
                }
                if (fireSafetyData.getRisk4() == null) {
                    potenFS4 = "";
                } else {
                    potenFS4 = fireSafetyData.getRisk4();
                }
                if (fireSafetyData.getRisk5() == null) {
                    potenFS5 = "";
                } else {
                    potenFS5 = fireSafetyData.getRisk5();
                }

                doc.getRange().replace("pr_32", potenFS1, true, true);
                doc.getRange().replace("pr_33", potenFS2, true, true);
                doc.getRange().replace("pr_34", potenFS3, true, true);
                doc.getRange().replace("pr_35", potenFS4, true, true);
                doc.getRange().replace("pr_36", potenFS5, true, true);

                if (fireSafetyData.getRating1() == null) {
                    ratingFS1 = "";
                } else {
                    ratingFS1 = fireSafetyData.getRating1();
                }
                if (fireSafetyData.getRating2() == null) {
                    ratingFS2 = "";
                } else {
                    ratingFS2 = fireSafetyData.getRating2();
                }
                if (fireSafetyData.getRating3() == null) {
                    ratingFS3 = "";
                } else {
                    ratingFS3 = fireSafetyData.getRating3();
                }
                if (fireSafetyData.getRating4() == null) {
                    ratingFS4 = "";
                } else {
                    ratingFS4 = fireSafetyData.getRating4();
                }
                if (fireSafetyData.getRating5() == null) {
                    ratingFS5 = "";
                } else {
                    ratingFS5 = fireSafetyData.getRating5();
                }

                if (ratingFS1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_32", passCheck, true, true);
                    doc.getRange().replace("g_32", "", true, true);
                    doc.getRange().replace("na32", "", true, true);
                } else if (ratingFS1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_32", "", true, true);
                    doc.getRange().replace("g_32", passCheck, true, true);
                    doc.getRange().replace("na32", "", true, true);
                } else if (ratingFS1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_32", "", true, true);
                    doc.getRange().replace("g_32", "", true, true);
                    doc.getRange().replace("na32", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_32", "", true, true);
                    doc.getRange().replace("g_32", "", true, true);
                    doc.getRange().replace("na32", "", true, true);
                }

                if (ratingFS2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_33", passCheck, true, true);
                    doc.getRange().replace("g_33", "", true, true);
                    doc.getRange().replace("na33", "", true, true);
                } else if (ratingFS2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_33", "", true, true);
                    doc.getRange().replace("g_33", passCheck, true, true);
                    doc.getRange().replace("na33", "", true, true);
                } else if (ratingFS2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_33", "", true, true);
                    doc.getRange().replace("g_33", "", true, true);
                    doc.getRange().replace("na33", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_33", "", true, true);
                    doc.getRange().replace("g_33", "", true, true);
                    doc.getRange().replace("na33", "", true, true);
                }

                if (ratingFS3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_34", passCheck, true, true);
                    doc.getRange().replace("g_34", "", true, true);
                    doc.getRange().replace("na34", "", true, true);
                } else if (ratingFS3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_34", "", true, true);
                    doc.getRange().replace("g_34", passCheck, true, true);
                    doc.getRange().replace("na34", "", true, true);
                } else if (ratingFS3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_34", "", true, true);
                    doc.getRange().replace("g_34", "", true, true);
                    doc.getRange().replace("na34", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_34", "", true, true);
                    doc.getRange().replace("g_34", "", true, true);
                    doc.getRange().replace("na34", "", true, true);
                }

                if (ratingFS4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_35", passCheck, true, true);
                    doc.getRange().replace("g_35", "", true, true);
                    doc.getRange().replace("na35", "", true, true);
                } else if (ratingFS4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_35", "", true, true);
                    doc.getRange().replace("g_35", passCheck, true, true);
                    doc.getRange().replace("na35", "", true, true);
                } else if (ratingFS4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_35", "", true, true);
                    doc.getRange().replace("g_35", "", true, true);
                    doc.getRange().replace("na35", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_35", "", true, true);
                    doc.getRange().replace("g_35", "", true, true);
                    doc.getRange().replace("na35", "", true, true);
                }

                if (ratingFS5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_36", passCheck, true, true);
                    doc.getRange().replace("g_36", "", true, true);
                    doc.getRange().replace("na36", "", true, true);
                } else if (ratingFS5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_36", "", true, true);
                    doc.getRange().replace("g_36", passCheck, true, true);
                    doc.getRange().replace("na36", "", true, true);
                } else if (ratingFS5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_36", "", true, true);
                    doc.getRange().replace("g_36", "", true, true);
                    doc.getRange().replace("na36", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_36", "", true, true);
                    doc.getRange().replace("g_36", "", true, true);
                    doc.getRange().replace("na36", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_32", "", true, true);
                doc.getRange().replace("pr_33", "", true, true);
                doc.getRange().replace("pr_34", "", true, true);
                doc.getRange().replace("pr_35", "", true, true);
                doc.getRange().replace("pr_36", "", true, true);

                doc.getRange().replace("nw_32", "", true, true);
                doc.getRange().replace("g_32", "", true, true);
                doc.getRange().replace("na32", "", true, true);

                doc.getRange().replace("nw_33", "", true, true);
                doc.getRange().replace("g_33", "", true, true);
                doc.getRange().replace("na33", "", true, true);

                doc.getRange().replace("nw_34", "", true, true);
                doc.getRange().replace("g_34", "", true, true);
                doc.getRange().replace("na34", "", true, true);

                doc.getRange().replace("nw_35", "", true, true);
                doc.getRange().replace("g_35", "", true, true);
                doc.getRange().replace("na35", "", true, true);

                doc.getRange().replace("nw_36", "", true, true);
                doc.getRange().replace("g_36", "", true, true);
                doc.getRange().replace("na36", "", true, true);
            }

             /* ==================================
                                     First Aid
            ================================== */
            final String potenFA1, potenFA2, potenFA3;
            final String ratingFA1, ratingFA2, ratingFA3;
            PlannedInspectionModel firstAidData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "First Aid");
            if (firstAidData != null) {
                if (firstAidData.getRisk1() == null) {
                    potenFA1 = "";
                } else {
                    potenFA1 = firstAidData.getRisk1();
                }
                if (firstAidData.getRisk2() == null) {
                    potenFA2 = "";
                } else {
                    potenFA2 = firstAidData.getRisk2();
                }
                if (firstAidData.getRisk3() == null) {
                    potenFA3 = "";
                } else {
                    potenFA3 = firstAidData.getRisk3();
                }

                doc.getRange().replace("pr_37", potenFA1, true, true);
                doc.getRange().replace("pr_38", potenFA2, true, true);
                doc.getRange().replace("pr_39", potenFA3, true, true);

                if (firstAidData.getRating1() == null) {
                    ratingFA1 = "";
                } else {
                    ratingFA1 = firstAidData.getRating1();
                }
                if (firstAidData.getRating2() == null) {
                    ratingFA2 = "";
                } else {
                    ratingFA2 = firstAidData.getRating2();
                }
                if (firstAidData.getRating3() == null) {
                    ratingFA3 = "";
                } else {
                    ratingFA3 = firstAidData.getRating3();
                }

                if (ratingFA1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_37", passCheck, true, true);
                    doc.getRange().replace("g_37", "", true, true);
                    doc.getRange().replace("na37", "", true, true);
                } else if (ratingFA1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_37", "", true, true);
                    doc.getRange().replace("g_37", passCheck, true, true);
                    doc.getRange().replace("na37", "", true, true);
                } else if (ratingFA1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_37", "", true, true);
                    doc.getRange().replace("g_37", "", true, true);
                    doc.getRange().replace("na37", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_37", "", true, true);
                    doc.getRange().replace("g_37", "", true, true);
                    doc.getRange().replace("na37", "", true, true);
                }

                if (ratingFA2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_38", passCheck, true, true);
                    doc.getRange().replace("g_38", "", true, true);
                    doc.getRange().replace("na38", "", true, true);
                } else if (ratingFA2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_38", "", true, true);
                    doc.getRange().replace("g_38", passCheck, true, true);
                    doc.getRange().replace("na38", "", true, true);
                } else if (ratingFA2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_38", "", true, true);
                    doc.getRange().replace("g_38", "", true, true);
                    doc.getRange().replace("na38", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_38", "", true, true);
                    doc.getRange().replace("g_38", "", true, true);
                    doc.getRange().replace("na38", "", true, true);
                }

                if (ratingFA3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_39", passCheck, true, true);
                    doc.getRange().replace("g_39", "", true, true);
                    doc.getRange().replace("na39", "", true, true);
                } else if (ratingFA3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_39", "", true, true);
                    doc.getRange().replace("g_39", passCheck, true, true);
                    doc.getRange().replace("na39", "", true, true);
                } else if (ratingFA3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_39", "", true, true);
                    doc.getRange().replace("g_39", "", true, true);
                    doc.getRange().replace("na39", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_39", "", true, true);
                    doc.getRange().replace("g_39", "", true, true);
                    doc.getRange().replace("na39", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_37", "", true, true);
                doc.getRange().replace("pr_38", "", true, true);
                doc.getRange().replace("pr_39", "", true, true);

                doc.getRange().replace("nw_37", "", true, true);
                doc.getRange().replace("g_37", "", true, true);
                doc.getRange().replace("na37", "", true, true);

                doc.getRange().replace("nw_38", "", true, true);
                doc.getRange().replace("g_38", "", true, true);
                doc.getRange().replace("na38", "", true, true);

                doc.getRange().replace("nw_39", "", true, true);
                doc.getRange().replace("g_39", "", true, true);
                doc.getRange().replace("na39", "", true, true);
            }

            /* ==================================
                                    Enviromental
            ================================== */
            final String potenEn1, potenEn2, potenEn3, potenEn4;
            final String ratingEn1, ratingEn2, ratingEn3, ratingEn4;
            PlannedInspectionModel enviroData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Environmental");
            if (enviroData != null) {
                if (enviroData.getRisk1() == null) {
                    potenEn1 = "";
                } else {
                    potenEn1 = enviroData.getRisk1();
                }
                if (enviroData.getRisk1() == null) {
                    potenEn2 = "";
                } else {
                    potenEn2 = enviroData.getRisk2();
                }
                if (enviroData.getRisk1() == null) {
                    potenEn3 = "";
                } else {
                    potenEn3 = enviroData.getRisk3();
                }
                if (enviroData.getRisk1() == null) {
                    potenEn4 = "";
                } else {
                    potenEn4 = enviroData.getRisk4();
                }

                doc.getRange().replace("pr_40", potenEn1, true, true);
                doc.getRange().replace("pr_41", potenEn2, true, true);
                doc.getRange().replace("pr_42", potenEn3, true, true);
                doc.getRange().replace("pr_43", potenEn4, true, true);

                if (enviroData.getRating1() == null) {
                    ratingEn1 = "";
                } else {
                    ratingEn1 = enviroData.getRating1();
                }
                if (enviroData.getRating2() == null) {
                    ratingEn2 = "";
                } else {
                    ratingEn2 = enviroData.getRating2();
                }
                if (enviroData.getRating3() == null) {
                    ratingEn3 = "";
                } else {
                    ratingEn3 = enviroData.getRating3();
                }
                if (enviroData.getRating4() == null) {
                    ratingEn4 = "";
                } else {
                    ratingEn4 = enviroData.getRating4();
                }

                if (ratingEn1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_40", passCheck, true, true);
                    doc.getRange().replace("g_40", "", true, true);
                    doc.getRange().replace("na40", "", true, true);
                } else if (ratingEn1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_40", "", true, true);
                    doc.getRange().replace("g_40", passCheck, true, true);
                    doc.getRange().replace("na40", "", true, true);
                } else if (ratingEn1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_40", "", true, true);
                    doc.getRange().replace("g_40", "", true, true);
                    doc.getRange().replace("na40", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_40", "", true, true);
                    doc.getRange().replace("g_40", "", true, true);
                    doc.getRange().replace("na40", "", true, true);
                }

                if (ratingEn2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_41", passCheck, true, true);
                    doc.getRange().replace("g_41", "", true, true);
                    doc.getRange().replace("na41", "", true, true);
                } else if (ratingEn2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_41", "", true, true);
                    doc.getRange().replace("g_41", passCheck, true, true);
                    doc.getRange().replace("na41", "", true, true);
                } else if (ratingEn2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_41", "", true, true);
                    doc.getRange().replace("g_41", "", true, true);
                    doc.getRange().replace("na41", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_41", "", true, true);
                    doc.getRange().replace("g_41", "", true, true);
                    doc.getRange().replace("na41", "", true, true);
                }

                if (ratingEn3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_42", passCheck, true, true);
                    doc.getRange().replace("g_42", "", true, true);
                    doc.getRange().replace("na42", "", true, true);
                } else if (ratingEn3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_42", "", true, true);
                    doc.getRange().replace("g_42", passCheck, true, true);
                    doc.getRange().replace("na42", "", true, true);
                } else if (ratingEn3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_42", "", true, true);
                    doc.getRange().replace("g_42", "", true, true);
                    doc.getRange().replace("na42", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_42", "", true, true);
                    doc.getRange().replace("g_42", "", true, true);
                    doc.getRange().replace("na42", "", true, true);
                }

                if (ratingEn4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_43", passCheck, true, true);
                    doc.getRange().replace("g_43", "", true, true);
                    doc.getRange().replace("na43", "", true, true);
                } else if (ratingEn4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_43", "", true, true);
                    doc.getRange().replace("g_43", passCheck, true, true);
                    doc.getRange().replace("na43", "", true, true);
                } else if (ratingEn4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_43", "", true, true);
                    doc.getRange().replace("g_43", "", true, true);
                    doc.getRange().replace("na43", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_43", "", true, true);
                    doc.getRange().replace("g_43", "", true, true);
                    doc.getRange().replace("na43", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_40", "", true, true);
                doc.getRange().replace("pr_41", "", true, true);
                doc.getRange().replace("pr_42", "", true, true);
                doc.getRange().replace("pr_43", "", true, true);

                doc.getRange().replace("nw_40", "", true, true);
                doc.getRange().replace("g_40", "", true, true);
                doc.getRange().replace("na40", "", true, true);

                doc.getRange().replace("nw_41", "", true, true);
                doc.getRange().replace("g_41", "", true, true);
                doc.getRange().replace("na41", "", true, true);

                doc.getRange().replace("nw_42", "", true, true);
                doc.getRange().replace("g_42", "", true, true);
                doc.getRange().replace("na42", "", true, true);

                doc.getRange().replace("nw_43", "", true, true);
                doc.getRange().replace("g_43", "", true, true);
                doc.getRange().replace("na43", "", true, true);
            }

             /* ==================================
                                   Communication
            ================================== */
            final String potenCom1, potenCom2, potenCom3, potenCom4, potenCom5, potenCom6, potenCom7;
            final String ratingCom1, ratingCom2, ratingCom3, ratingCom4, ratingCom5, ratingCom6, ratingCom7;
            PlannedInspectionModel communicationData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Communication");
            if (communicationData != null) {
                if (communicationData.getRisk1() == null) {
                    potenCom1 = "";
                } else {
                    potenCom1 = communicationData.getRisk1();
                }
                if (communicationData.getRisk2() == null) {
                    potenCom2 = "";
                } else {
                    potenCom2 = communicationData.getRisk2();
                }
                if (communicationData.getRisk3() == null) {
                    potenCom3 = "";
                } else {
                    potenCom3 = communicationData.getRisk3();
                }
                if (communicationData.getRisk4() == null) {
                    potenCom4 = "";
                } else {
                    potenCom4 = communicationData.getRisk4();
                }
                if (communicationData.getRisk5() == null) {
                    potenCom5 = "";
                } else {
                    potenCom5 = communicationData.getRisk5();
                }
                if (communicationData.getRisk6() == null) {
                    potenCom6 = "";
                } else {
                    potenCom6 = communicationData.getRisk6();
                }
                if (communicationData.getRisk7() == null) {
                    potenCom7 = "";
                } else {
                    potenCom7 = communicationData.getRisk7();
                }

                doc.getRange().replace("pr_44", potenCom1, true, true);
                doc.getRange().replace("pr_45", potenCom2, true, true);
                doc.getRange().replace("pr_46", potenCom3, true, true);
                doc.getRange().replace("pr_47", potenCom4, true, true);
                doc.getRange().replace("pr_48", potenCom5, true, true);
                doc.getRange().replace("pr_49", potenCom6, true, true);
                doc.getRange().replace("pr_50", potenCom7, true, true);

                if (communicationData.getRating1() == null) {
                    ratingCom1 = "";
                } else {
                    ratingCom1 = communicationData.getRating1();
                }
                if (communicationData.getRating2() == null) {
                    ratingCom2 = "";
                } else {
                    ratingCom2 = communicationData.getRating2();
                }
                if (communicationData.getRating3() == null) {
                    ratingCom3 = "";
                } else {
                    ratingCom3 = communicationData.getRating3();
                }
                if (communicationData.getRating4() == null) {
                    ratingCom4 = "";
                } else {
                    ratingCom4 = communicationData.getRating4();
                }
                if (communicationData.getRating5() == null) {
                    ratingCom5 = "";
                } else {
                    ratingCom5 = communicationData.getRating5();
                }
                if (communicationData.getRating6() == null) {
                    ratingCom6 = "";
                } else {
                    ratingCom6 = communicationData.getRating6();
                }
                if (communicationData.getRating7() == null) {
                    ratingCom7 = "";
                } else {
                    ratingCom7 = communicationData.getRating7();
                }

                if (ratingCom1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_44", passCheck, true, true);
                    doc.getRange().replace("g_44", "", true, true);
                    doc.getRange().replace("na44", "", true, true);
                } else if (ratingCom1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_44", "", true, true);
                    doc.getRange().replace("g_44", passCheck, true, true);
                    doc.getRange().replace("na44", "", true, true);
                } else if (ratingCom1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_44", "", true, true);
                    doc.getRange().replace("g_44", "", true, true);
                    doc.getRange().replace("na44", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_44", "", true, true);
                    doc.getRange().replace("g_44", "", true, true);
                    doc.getRange().replace("na44", "", true, true);
                }

                if (ratingCom2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_45", passCheck, true, true);
                    doc.getRange().replace("g_45", "", true, true);
                    doc.getRange().replace("na45", "", true, true);
                } else if (ratingCom2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_45", "", true, true);
                    doc.getRange().replace("g_45", passCheck, true, true);
                    doc.getRange().replace("na45", "", true, true);
                } else if (ratingCom2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_45", "", true, true);
                    doc.getRange().replace("g_45", "", true, true);
                    doc.getRange().replace("na45", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_45", "", true, true);
                    doc.getRange().replace("g_45", "", true, true);
                    doc.getRange().replace("na45", "", true, true);
                }

                if (ratingCom3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_46", passCheck, true, true);
                    doc.getRange().replace("g_46", "", true, true);
                    doc.getRange().replace("na46", "", true, true);
                } else if (ratingCom3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_46", "", true, true);
                    doc.getRange().replace("g_46", passCheck, true, true);
                    doc.getRange().replace("na46", "", true, true);
                } else if (ratingCom3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_46", "", true, true);
                    doc.getRange().replace("g_46", "", true, true);
                    doc.getRange().replace("na46", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_46", "", true, true);
                    doc.getRange().replace("g_46", "", true, true);
                    doc.getRange().replace("na46", "", true, true);
                }

                if (ratingCom4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_47", passCheck, true, true);
                    doc.getRange().replace("g_47", "", true, true);
                    doc.getRange().replace("na47", "", true, true);
                } else if (ratingCom4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_47", "", true, true);
                    doc.getRange().replace("g_47", passCheck, true, true);
                    doc.getRange().replace("na47", "", true, true);
                } else if (ratingCom4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_47", "", true, true);
                    doc.getRange().replace("g_47", "", true, true);
                    doc.getRange().replace("na47", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_47", "", true, true);
                    doc.getRange().replace("g_47", "", true, true);
                    doc.getRange().replace("na47", "", true, true);
                }

                if (ratingCom5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_48", passCheck, true, true);
                    doc.getRange().replace("g_48", "", true, true);
                    doc.getRange().replace("na48", "", true, true);
                } else if (ratingCom5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_48", "", true, true);
                    doc.getRange().replace("g_48", passCheck, true, true);
                    doc.getRange().replace("na48", "", true, true);
                } else if (ratingCom5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_48", "", true, true);
                    doc.getRange().replace("g_48", "", true, true);
                    doc.getRange().replace("na48", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_48", "", true, true);
                    doc.getRange().replace("g_48", "", true, true);
                    doc.getRange().replace("na48", "", true, true);
                }

                if (ratingCom6.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_49", passCheck, true, true);
                    doc.getRange().replace("g_49", "", true, true);
                    doc.getRange().replace("na49", "", true, true);
                } else if (ratingCom6.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_49", "", true, true);
                    doc.getRange().replace("g_49", passCheck, true, true);
                    doc.getRange().replace("na49", "", true, true);
                } else if (ratingCom6.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_49", "", true, true);
                    doc.getRange().replace("g_49", "", true, true);
                    doc.getRange().replace("na49", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_49", "", true, true);
                    doc.getRange().replace("g_49", "", true, true);
                    doc.getRange().replace("na49", "", true, true);
                }

                if (ratingCom7.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_50", passCheck, true, true);
                    doc.getRange().replace("g_50", "", true, true);
                    doc.getRange().replace("na50", "", true, true);
                } else if (ratingCom7.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_50", "", true, true);
                    doc.getRange().replace("g_50", passCheck, true, true);
                    doc.getRange().replace("na50", "", true, true);
                } else if (ratingCom7.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_50", "", true, true);
                    doc.getRange().replace("g_50", "", true, true);
                    doc.getRange().replace("na50", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_50", "", true, true);
                    doc.getRange().replace("g_50", "", true, true);
                    doc.getRange().replace("na50", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_44", "", true, true);
                doc.getRange().replace("pr_45", "", true, true);
                doc.getRange().replace("pr_46", "", true, true);
                doc.getRange().replace("pr_47", "", true, true);
                doc.getRange().replace("pr_48", "", true, true);
                doc.getRange().replace("pr_49", "", true, true);
                doc.getRange().replace("pr_50", "", true, true);

                doc.getRange().replace("nw_44", "", true, true);
                doc.getRange().replace("g_44", "", true, true);
                doc.getRange().replace("na44", "", true, true);

                doc.getRange().replace("nw_45", "", true, true);
                doc.getRange().replace("g_45", "", true, true);
                doc.getRange().replace("na45", "", true, true);

                doc.getRange().replace("nw_46", "", true, true);
                doc.getRange().replace("g_46", "", true, true);
                doc.getRange().replace("na46", "", true, true);

                doc.getRange().replace("nw_47", "", true, true);
                doc.getRange().replace("g_47", "", true, true);
                doc.getRange().replace("na47", "", true, true);

                doc.getRange().replace("nw_48", "", true, true);
                doc.getRange().replace("g_48", "", true, true);
                doc.getRange().replace("na48", "", true, true);

                doc.getRange().replace("nw_49", "", true, true);
                doc.getRange().replace("g_49", "", true, true);
                doc.getRange().replace("na49", "", true, true);

                doc.getRange().replace("nw_50", "", true, true);
                doc.getRange().replace("g_50", "", true, true);
                doc.getRange().replace("na50", "", true, true);
            }

            /* ==================================
                                  Fall Protection
            ================================== */
            final String potenFP1, potenFP2, potenFP3, potenFP4, potenFP5, potenFP6;
            final String ratingFP1, ratingFP2, ratingFP3, ratingFP4, ratingFP5, ratingFP6;
            PlannedInspectionModel fallProtectionData = ds.getLastDataInspection(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Fall Protection");
            if (fallProtectionData != null) {
                if (fallProtectionData.getRisk1() == null) {
                    potenFP1 = "";
                } else {
                    potenFP1 = fallProtectionData.getRisk1();
                }
                if (fallProtectionData.getRisk2() == null) {
                    potenFP2 = "";
                } else {
                    potenFP2 = fallProtectionData.getRisk2();
                }
                if (fallProtectionData.getRisk3() == null) {
                    potenFP3 = "";
                } else {
                    potenFP3 = fallProtectionData.getRisk3();
                }
                if (fallProtectionData.getRisk4() == null) {
                    potenFP4 = "";
                } else {
                    potenFP4 = fallProtectionData.getRisk4();
                }
                if (fallProtectionData.getRisk5() == null) {
                    potenFP5 = "";
                } else {
                    potenFP5 = fallProtectionData.getRisk5();
                }
                if (fallProtectionData.getRisk6() == null) {
                    potenFP6 = "";
                } else {
                    potenFP6 = fallProtectionData.getRisk6();
                }

                doc.getRange().replace("pr_51", potenFP1, true, true);
                doc.getRange().replace("pr_52", potenFP2, true, true);
                doc.getRange().replace("pr_53", potenFP3, true, true);
                doc.getRange().replace("pr_54", potenFP4, true, true);
                doc.getRange().replace("pr_55", potenFP5, true, true);
                doc.getRange().replace("pr_56", potenFP6, true, true);

                if (fallProtectionData.getRating1() == null) {
                    ratingFP1 = "";
                } else {
                    ratingFP1 = fallProtectionData.getRating1();
                }
                if (fallProtectionData.getRating2() == null) {
                    ratingFP2 = "";
                } else {
                    ratingFP2 = fallProtectionData.getRating2();
                }
                if (fallProtectionData.getRating3() == null) {
                    ratingFP3 = "";
                } else {
                    ratingFP3 = fallProtectionData.getRating3();
                }
                if (fallProtectionData.getRating4() == null) {
                    ratingFP4 = "";
                } else {
                    ratingFP4 = fallProtectionData.getRating4();
                }
                if (fallProtectionData.getRating5() == null) {
                    ratingFP5 = "";
                } else {
                    ratingFP5 = fallProtectionData.getRating5();
                }
                if (fallProtectionData.getRating6() == null) {
                    ratingFP6 = "";
                } else {
                    ratingFP6 = fallProtectionData.getRating6();
                }

                if (ratingFP1.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_51", passCheck, true, true);
                    doc.getRange().replace("g_51", "", true, true);
                    doc.getRange().replace("na51", "", true, true);
                } else if (ratingFP1.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_51", "", true, true);
                    doc.getRange().replace("g_51", passCheck, true, true);
                    doc.getRange().replace("na51", "", true, true);
                } else if (ratingFP1.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_51", "", true, true);
                    doc.getRange().replace("g_51", "", true, true);
                    doc.getRange().replace("na51", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_51", "", true, true);
                    doc.getRange().replace("g_51", "", true, true);
                    doc.getRange().replace("na51", "", true, true);
                }

                if (ratingFP2.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_52", passCheck, true, true);
                    doc.getRange().replace("g_52", "", true, true);
                    doc.getRange().replace("na52", "", true, true);
                } else if (ratingFP2.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_52", "", true, true);
                    doc.getRange().replace("g_52", passCheck, true, true);
                    doc.getRange().replace("na52", "", true, true);
                } else if (ratingFP2.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_52", "", true, true);
                    doc.getRange().replace("g_52", "", true, true);
                    doc.getRange().replace("na52", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_52", "", true, true);
                    doc.getRange().replace("g_52", "", true, true);
                    doc.getRange().replace("na52", "", true, true);
                }

                if (ratingFP3.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_53", passCheck, true, true);
                    doc.getRange().replace("g_53", "", true, true);
                    doc.getRange().replace("na53", "", true, true);
                } else if (ratingFP3.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_53", "", true, true);
                    doc.getRange().replace("g_53", passCheck, true, true);
                    doc.getRange().replace("na53", "", true, true);
                } else if (ratingFP3.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_53", "", true, true);
                    doc.getRange().replace("g_53", "", true, true);
                    doc.getRange().replace("na53", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_53", "", true, true);
                    doc.getRange().replace("g_53", "", true, true);
                    doc.getRange().replace("na53", "", true, true);
                }

                if (ratingFP4.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_54", passCheck, true, true);
                    doc.getRange().replace("g_54", "", true, true);
                    doc.getRange().replace("na54", "", true, true);
                } else if (ratingFP4.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_54", "", true, true);
                    doc.getRange().replace("g_54", passCheck, true, true);
                    doc.getRange().replace("na54", "", true, true);
                } else if (ratingFP4.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_54", "", true, true);
                    doc.getRange().replace("g_54", "", true, true);
                    doc.getRange().replace("na54", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_54", "", true, true);
                    doc.getRange().replace("g_54", "", true, true);
                    doc.getRange().replace("na54", "", true, true);
                }

                if (ratingFP5.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_55", passCheck, true, true);
                    doc.getRange().replace("g_55", "", true, true);
                    doc.getRange().replace("na55", "", true, true);
                } else if (ratingFP5.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_55", "", true, true);
                    doc.getRange().replace("g_55", passCheck, true, true);
                    doc.getRange().replace("na55", "", true, true);
                } else if (ratingFP5.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_55", "", true, true);
                    doc.getRange().replace("g_55", "", true, true);
                    doc.getRange().replace("na55", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_55", "", true, true);
                    doc.getRange().replace("g_55", "", true, true);
                    doc.getRange().replace("na55", "", true, true);
                }

                if (ratingFP6.equalsIgnoreCase("NW")) {
                    doc.getRange().replace("nw_56", passCheck, true, true);
                    doc.getRange().replace("g_56", "", true, true);
                    doc.getRange().replace("na56", "", true, true);
                } else if (ratingFP6.equalsIgnoreCase("Good")) {
                    doc.getRange().replace("nw_56", "", true, true);
                    doc.getRange().replace("g_56", passCheck, true, true);
                    doc.getRange().replace("na56", "", true, true);
                } else if (ratingFP6.equalsIgnoreCase("N/A")) {
                    doc.getRange().replace("nw_56", "", true, true);
                    doc.getRange().replace("g_56", "", true, true);
                    doc.getRange().replace("na56", passCheck, true, true);
                } else {
                    doc.getRange().replace("nw_56", "", true, true);
                    doc.getRange().replace("g_56", "", true, true);
                    doc.getRange().replace("na56", "", true, true);
                }
            } else {
                doc.getRange().replace("pr_51", "", true, true);
                doc.getRange().replace("pr_52", "", true, true);
                doc.getRange().replace("pr_53", "", true, true);
                doc.getRange().replace("pr_54", "", true, true);
                doc.getRange().replace("pr_55", "", true, true);
                doc.getRange().replace("pr_56", "", true, true);

                doc.getRange().replace("nw_51", "", true, true);
                doc.getRange().replace("g_51", "", true, true);
                doc.getRange().replace("na51", "", true, true);

                doc.getRange().replace("nw_52", "", true, true);
                doc.getRange().replace("g_52", "", true, true);
                doc.getRange().replace("na52", "", true, true);

                doc.getRange().replace("nw_53", "", true, true);
                doc.getRange().replace("g_53", "", true, true);
                doc.getRange().replace("na53", "", true, true);

                doc.getRange().replace("nw_54", "", true, true);
                doc.getRange().replace("g_54", "", true, true);
                doc.getRange().replace("na54", "", true, true);

                doc.getRange().replace("nw_55", "", true, true);
                doc.getRange().replace("g_55", "", true, true);
                doc.getRange().replace("na55", "", true, true);

                doc.getRange().replace("nw_5", "", true, true);
                doc.getRange().replace("g_56", "", true, true);
                doc.getRange().replace("na56", "", true, true);

                doc.getRange().replace("nw_57", "", true, true);
                doc.getRange().replace("g_57", "", true, true);
                doc.getRange().replace("na57", "", true, true);
            }

            Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 2, true);

            plannedMainModel = ds.getLastestPlannedMain();
            ArrayList<PlannedFindingsModel> findingsData = ds.getAllPlannedFindings(plannedMainModel.getId());
            Row templateRow = (Row) sampleIdTable.getLastRow();
            int count = 0;

            int photoDummyIndex = 1;
            Row dataRow;
            Row clonedRow;
            RowCollection photoTableRows = sampleIdTable.getRows();
            clonedRow = (Row) photoTableRows.get(photoDummyIndex);

            String categoryQuestion = "";

            int counts = 0;
            for (int i = 0; i < findingsData.size(); i++) {
                counts++;
                dataRow = (Row) clonedRow.deepClone(true);
                sampleIdTable.getRows().add(dataRow);

                clonedRow.getRowFormat().setAllowBreakAcrossPages(true);
//                    View v = dataFindings.getChildAt(i);

                // Items #
                Cell cell = dataRow.getCells().get(0);
//                for (Run run : cell.getFirstParagraph().getRuns()) {
//                    // Set some font formatting properties
//                    Font font = run.getFont();
//                    font.setColor(Color.BLACK);
//                }
//                cell.getFirstParagraph().getRuns().clear();
//                cell.getCellFormat().getShading()
//                        .setBackgroundPatternColor(Color.WHITE);
//                cell.getFirstParagraph().appendChild(
//                        new Run(doc, valueOf(counts)));

                if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Work Area & Housekeeping - Building, Floors, Mining)";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Work Area & Housekeeping - Stacking & Storage)";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Work Area & Housekeeping - Work Set Up (Ergonomics))";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Work Area & Housekeeping - Walkways)";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Work Area & Housekeeping - General Cleanliness)";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 6) {
                    categoryQuestion = "(Work Area & Housekeeping - Ground Support Conditions)";
                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Work Area & Housekeeping)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Structures & Floors - Coloring & Demarcation)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Structures & Floors - Windows & Screens)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Structures & Floors - Drainage)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Structures & Floors - Water Lines & Valves)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Structures & Floors - Sanitary Facilities)";
                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Structures & Floors)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Machinary Tools - Guard in place)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Machinary Tools - Serviceable Guard)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Machinary Tools - Pinch Point)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Machinary Tools - Correct for the job)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Machinary Tools - Good Condition)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 6) {
                    categoryQuestion = "(Machinary Tools - Pre-op Checklist)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 7) {
                    categoryQuestion = "(Machinary Tools - Licenses & Permits)";
                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Machinary Tools)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Electrical - Power & Extention Cables)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Electrical - Panel, Switches, Plug & Outles)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Electrical - Power & Equipment Cables)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Electrical - Labeling)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Electrical - Color Coding & Demarcation)";
                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Electrical)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Personel - Correct PPE in use)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Personel - Work Activity)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Personel - Supervisor Dialy Check)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Personel - Special Safety Instructions)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Personel - Licenses & Permits)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 6) {
                    categoryQuestion = "(Personel - Correct Lifting & Handling)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 7) {
                    categoryQuestion = "(Personel - JSA / SOP Available)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 8) {
                    categoryQuestion = "(Personel - Gas Detector)";
                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Personel)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Fire Safety - Nearest Fire Extinguisher)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Fire Safety - Color Coding & Demarcation)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Fire Safety - Sufficient Number & Types)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Fire Safety - Visible & Accessible)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Fire Safety - ERG Inspection Tag)";
                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Fire Safety)";
                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(First Aid - First Aid Kit)";
                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(First Aid - Eyewash Station)";
                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(First Aid - Stretcher)";
                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(First Aid)";
                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Environmental - Lighting)";
                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Environmental - Ventilation)";
                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Environmental - Noise)";
                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Environmental - Hazard Materials)";
                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Environmental)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Communication - Telephone / Radio)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Communication - Emergency Procedure)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Communication - Personal Protective Equipment)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Communication - No Smoke)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Communication - JSA / SOP Posted)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 6) {
                    categoryQuestion = "(Communication - Emergency Exit)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 7) {
                    categoryQuestion = "(Communication - Other)";
                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Communication";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 1) {
                    categoryQuestion = "(Fall Protection - Ladders)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 2) {
                    categoryQuestion = "(Fall Protection - Scaffolds)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 3) {
                    categoryQuestion = "(Fall Protection - Stairways & Ramps)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 4) {
                    categoryQuestion = "(Fall Protection - Lanyards)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 5) {
                    categoryQuestion = "(Fall Protection - 4 Point Harness / Safety Belt)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 6) {
                    categoryQuestion = "(Fall Protection - Safety Blocks)";
                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 0) {
                    categoryQuestion = "(Fall Protection)";
                } else if (findingsData.get(i).getCode() == 0) {
                    categoryQuestion = "";
                }

                // Findings
                Cell cellFindngs = dataRow.getCells().get(0);
                for (Run run : cellFindngs.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cellFindngs.getFirstParagraph().getRuns().clear();
                cellFindngs.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cellFindngs.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(categoryQuestion + " - " + findingsData.get(i).getFindings())));

                // Comments
                Cell cellComment = dataRow.getCells().get(1);
                for (Run run : cellComment.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cellComment.getFirstParagraph().getRuns().clear();
                cellComment.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cellComment.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(findingsData.get(i).getComment())));

                // Photo
                cell = dataRow.getCells().get(2);
                cell.getFirstParagraph().getRuns().clear();
                if (findingsData.get(i).getPhotoPath() != null) {
                    DocumentBuilder builders = new DocumentBuilder(doc);
                    CellFormat format = cell.getCellFormat();
                    double width = format.getWidth();
                    double height = cell.getParentRow().getRowFormat()
                            .getHeight();
                    builder.moveTo(cell.getFirstChild());

                    Bitmap imgBitmap = null;
                    if (isGenerate) {
                        imgBitmap = BitmapFactory.decodeFile(findingsData.get(i).getPhotoPath());
                    } else {
                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(findingsData.get(i).getPhotoPath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 90,
                                            150);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 180,
                                            150);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 0,
                                            150);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // Shape image = builder.insertImage(imgBitmap);
                    ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                    imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                            Constants.COMPRESS_PERCENTAGE, tempImage);
                    Bitmap compressedBitmap = BitmapFactory
                            .decodeStream(new ByteArrayInputStream(
                                    tempImage.toByteArray()));
                    Shape image = builder.insertImage(compressedBitmap);

                    double freePageWidth = width;
                    double freePageHeight = height;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData().getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth
                            || size.getHeightPoints() > freePageHeight;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }

                    if (isGenerate) {
//                                        if (!imageFolder.exists()) {
//                                            imageFolder.mkdir();
//                                        }
//
//                                        // MOVE IMAGE TO REPORT FOLDER
//                                        File old = new File(findingsData.get(i).getPhotoPath());
//                                        String name = old.getName();
////
//                                        InputStream in = new FileInputStream(findingsData.get(i).getPhotoPath());
//                                        OutputStream out = new FileOutputStream(
//                                                imageFolderPath + "/" + name);
//                                        Helper.copyFile(in, out);
//                                        old.delete();
//
//                                        PlannedFindingsModel fm = findingsData.get(i);
//                                        fm.setPhotoPath(imageFolderPath + "/" + name);
//                                        findingsData.set(i, fm);
                    } else {
                        for (Run run : cell.getFirstParagraph().getRuns()) {
//                            // Set some font formatting properties
                            Font font = run.getFont();
                            font.setColor(Color.BLACK);
                        }
                        cell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        cell.getFirstParagraph().appendChild(new Run(doc, ""));
                    }
                }

                // Potential Risk
                Cell cellRisk = dataRow.getCells().get(3);
                for (Run run : cellRisk.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cellRisk.getFirstParagraph().getRuns().clear();
                cellRisk.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cellRisk.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(findingsData.get(i).getRisk())));

                // Potential Risk
                Cell cellRP = dataRow.getCells().get(4);
                for (Run run : cellRP.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cellRP.getFirstParagraph().getRuns().clear();
                cellRP.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cellRP.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(findingsData.get(i).getResponsible())));

                // Date Complete
                Cell cellDate = dataRow.getCells().get(5);
                for (Run run : cellDate.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cellDate.getFirstParagraph().getRuns().clear();
                cellDate.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cellDate.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(findingsData.get(i).getDate())));

//                sampleIdTable.appendChild(clonedRow);

            }

            Row dummyRow = photoTableRows.get(1);
            dummyRow.remove();

            String supervisorID, supervisorDate, managerID, managerDate, safetyID, safetyDate;
            Table signTable = (Table) doc.getChild(NodeType.TABLE, 3, true);

            PlannedSignModel supervisorData = ds.getAllPlannedSign(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Supervisor");
            if (supervisorData != null) {
                if (supervisorData.getIdNo() != null) {
                    supervisorID = supervisorData.getIdNo();
                } else {
                    supervisorID = "";
                }
                doc.getRange().replace("employee_id_1", supervisorID, true, true);

                if (supervisorData.getDate() != null) {
                    supervisorDate = supervisorData.getDate();
                } else {
                    supervisorDate = "";
                }
                doc.getRange().replace("date_id_1", supervisorDate, true, true);


                if (supervisorData.getSign() != null) {
                    Row row4 = (Row) signTable.getRows().get(1);
                    Cell dataCell4 = row4.getCells().get(1);
                    CellFormat format = dataCell4.getCellFormat();
                    double width = format.getWidth();
                    builder.moveTo(dataCell4.getFirstParagraph());
                    Shape image = builder.insertImage(supervisorData.getSign());

                    double freePageWidth = width;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData()
                            .getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        Log.d("IMAGE", "widthlonger : " + ""
                                + " | ratio : " + ratio);

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }
                }

            } else {
                doc.getRange().replace("employee_id_1", "", true, true);
                doc.getRange().replace("date_id_1", "", true, true);
            }

            PlannedSignModel managerData = ds.getAllPlannedSign(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Manager");
            if (managerData != null) {
                if (managerData.getIdNo() == null) {
                    managerID = "";
                } else {
                    managerID = managerData.getIdNo();
                }
                doc.getRange().replace("employee_id_2", managerID, true, true);

                if (managerData.getIdNo() == null) {
                    managerDate = "";
                } else {
                    managerDate = managerData.getDate();
                }

                doc.getRange().replace("date_id_2", managerDate, true, true);

                if (managerData.getSign() != null) {
                    Row row4 = (Row) signTable.getRows().get(2);
                    Cell dataCell4 = row4.getCells().get(1);
                    CellFormat format = dataCell4.getCellFormat();
                    double width = format.getWidth();
                    builder.moveTo(dataCell4.getFirstParagraph());
                    Shape image = builder.insertImage(managerData.getSign());

                    double freePageWidth = width;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData()
                            .getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        Log.d("IMAGE", "widthlonger : " + ""
                                + " | ratio : " + ratio);

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }
                }

            } else {
                doc.getRange().replace("employee_id_2", "", true, true);
                doc.getRange().replace("date_id_2", "", true, true);
            }

            PlannedSignModel safetyData = ds.getAllPlannedSign(inspectorET.getText().toString(),
                    areaOwnerET.getText().toString(), locationET.getText().toString(), dateET.getText().toString(), "Safety");
            if (safetyData != null) {
                if (safetyData.getIdNo() == null) {
                    safetyID = "";
                } else {
                    safetyID = safetyData.getIdNo();
                }
                doc.getRange().replace("employee_id_3", safetyID, true, true);

                if (safetyData.getIdNo() == null) {
                    safetyDate = "";
                } else {
                    safetyDate = safetyData.getDate();
                }
                doc.getRange().replace("date_id_3", safetyDate, true, true);

                if (safetyData.getSign() != null) {
                    Row row4 = (Row) signTable.getRows().get(3);
                    Cell dataCell4 = row4.getCells().get(1);
                    CellFormat format = dataCell4.getCellFormat();
                    double width = format.getWidth();
                    builder.moveTo(dataCell4.getFirstParagraph());
                    Shape image = builder.insertImage(safetyData.getSign());

                    double freePageWidth = width;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData()
                            .getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        Log.d("IMAGE", "widthlonger : " + ""
                                + " | ratio : " + ratio);

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }
                }
            } else {
                doc.getRange().replace("employee_id_3", "", true, true);
                doc.getRange().replace("date_id_3", "", true, true);
            }

            if (isGenerate) {
                fullPath += "/" + fileName;
                doc.save(fullPath + ".doc");
                folderDirectory = fullPath;
                Helper.showPopUpMessage(mActivity, "Success",
                        "Report has been successfully generated in "
                                + folderDirectory, null);

                String previewFolder = Helper.getPathForPreviewFile();
                String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
                        + Constants.FILE_TYPE_PDF;
                Helper.checkAndCreateDirectory(previewFolder);
                previewFilePath = previewFolder + "/" + fileNamePdf;
                doc.save(previewFilePath);

                File file = new File(previewFilePath);
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(Uri.fromFile(file), type);
                mActivity.startActivityForResult(intent,
                        PREVIEW_CODE_SHE_PLANNED);
                Helper.progressDialog.dismiss();
                isPreview = true;
            } else {
                String previewFolder = Helper.getPathForPreviewFile();
                String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
                        + Constants.FILE_TYPE_PDF;
                Helper.checkAndCreateDirectory(previewFolder);
                previewFilePath = previewFolder + "/" + fileNamePdf;
                doc.save(previewFilePath);

                File file = new File(previewFilePath);
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(Uri.fromFile(file), type);
                mActivity.startActivityForResult(intent,
                        PREVIEW_CODE_SHE_PLANNED);
                Helper.progressDialog.dismiss();
                isPreview = true;
            }

//            if (isGenerate) {
//
//                return Constants.REPORT_GENERATED;
//            } else {
//
//                return Constants.REPORT_PREVIEW;
//            }

        } catch (Exception e) {

        }
    }

    public static boolean validateSaveData() {
        boolean status = true;

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date Inspection");
        }

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector");
        }

        if (areaOwnerET.getText().toString().length() == 0) {
            status = false;
            areaOwnerTL.setErrorEnabled(true);
            areaOwnerTL.setError("Please fill Area Owner");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("Please fill Location Inspection");
        }

        return status;
    }

    private void addFindingsPhoto() {
        PlannedFindingsPopUp.showPlannedFindingsPopUp(mActivity, new PlannedFindingsModel(),
                new PlannedFindingCallback() {
                    @Override
                    public void onPlannedSubmited(PlannedFindingsModel model) {
                        plannedFindingsData.add(0, model);
                        plannedFindingPhotoAdapter.notifyDataSetChanged();
                    }
                });
    }

    public void multiStateToggleButton() {
        /* WORK AREA */
        inspWaRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspWaRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspWaRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspWaRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspWaRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspWaRate6.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspWaRate6.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* STRUCTURE */
        inspStRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspStRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspStRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspStRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspStRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspStRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspStRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspStRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspStRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspStRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* MACHINERY TOOLS */
        inspMtRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate6.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate6.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspMtRate7.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspMtRate7.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* ELECTRICAL */
        inspElRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspElRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspElRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspElRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspElRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspElRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspElRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspElRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspElRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspElRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* PERSONEL */
        inspPeRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate6.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate6.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate7.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate7.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspPeRate8.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspPeRate8.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* FIRE SAFETY */
        inspFSRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFSRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFSRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFSRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFSRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFSRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFSRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFSRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFSRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFSRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* FIRST AID */
        inspFARate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFARate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFARate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFARate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFARate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFARate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* ENVIRONMENT */
        inspENRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspENRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspENRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspENRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspENRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspENRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspENRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspENRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* COMMUNICATION */
        inspCOMRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate6.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate6.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspCOMRate7.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspCOMRate7.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        /* FALL PROTECTION */
        inspFPRate1.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate1.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFPRate2.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate2.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFPRate3.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate3.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFPRate4.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate4.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFPRate5.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate5.getValue() == 0) {
                    addFindingsPhoto();
                }
            }
        });

        inspFPRate6.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (inspFPRate6.getValue() == 1) {
                    addFindingsPhoto();
                }
            }
        });
    }

    private void removeManpowerList(EditText target) {
        String listOfManpower = target.getText().toString();
        if (listOfManpower.length() > 0) {
            String[] manpowerColl = listOfManpower.split("; ");
            StringBuilder newManpowerList = new StringBuilder();
            newManpowerList.append("");
            for (int i = 0; i < manpowerColl.length - 1; i++) {
                newManpowerList.append(manpowerColl[i]);
                newManpowerList.append("; ");
            }
            listOfManpower = newManpowerList.toString();
            target.setText(listOfManpower);
            switch (target.getId()) {
                case R.id.inspectorET:
                    selectedInspectorList.remove(selectedInspectorList.size() - 1);
                    break;
                case R.id.areaOwnerET:
                    selectedAreaOwnerList.remove(selectedAreaOwnerList.size() - 1);
                    break;
                default:
                    break;
            }
        }
    }

    public static String formatIDInspector(String id) {
        String formattedID = "";

        for (int i = 0; i < (10 - id.length()); i++) {
            formattedID += "0";
        }
        formattedID += id;
        return formattedID;
    }

    public void showAreaOwnerPopUp(final int type, final String titlePopUp, final InspectorCallback callback) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_inspector_field);

                TextView titlePopUpTV;

                final EditText idET, titleET, descET;

                final AutoCompleteTextView nameET;

                TextInputLayout nameTL, idTL, titleTL, descTL;

                ButtonRectangle clearAllBtn, submitBtn, cancelBtn;

                titlePopUpTV = (TextView) dialog.findViewById(R.id.titlePopUpTV);

                idET = (EditText) dialog.findViewById(R.id.idET);
                nameET = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
                titleET = (EditText) dialog.findViewById(R.id.titleET);
                descET = (EditText) dialog.findViewById(R.id.orgET);

                nameTL = (TextInputLayout) dialog.findViewById(R.id.nameTL);
                idTL = (TextInputLayout) dialog.findViewById(R.id.idTL);
                titleTL = (TextInputLayout) dialog.findViewById(R.id.titleTL);
                descTL = (TextInputLayout) dialog.findViewById(R.id.orgTL);

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);

                titlePopUpTV.setText(titlePopUp);

                final ArrayList<String> data = new ArrayList<String>();
                for (int i = 0; i < manpowers.size(); i++) {
                    data.add(manpowers.get(i).getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, data);
                nameET.setThreshold(1);
                nameET.setAdapter(adapter);
                nameET.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        String key = (String) nameET.getText().toString();
                        Log.d("WTF", key);
                        int pos = data.indexOf(key);

                        selectedObserver = manpowers.get(pos);
                        nameET.setText(selectedObserver.getName());
                        idET.setText(selectedObserver.getIdno());
                        titleET.setText(selectedObserver.getTitle());
                        descET.setText(selectedObserver.getOrg());
                    }
                });

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nameET.setText("");
                        idET.setText("");
                        titleET.setText("");
                        descET.setText("");
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (nameET.length() < 1 || idET.length() < 1) {
//                            warningLayout.setVisibility(View.VISIBLE);
                        } else {
                            String text = "";
                            PlannedSignModel ins = new PlannedSignModel();
                            ins.setIdNo(idET.getText().toString());
                            ins.setName(nameET.getText().toString().toUpperCase());
                            ins.setTitle(titleET.getText().toString());
                            ins.setInspector(inspectorET.getText().toString());
                            ins.setAreaOwner(areaOwnerET.getText().toString());
                            ins.setLocation(locationET.getText().toString());
                            ins.setDateIns(dateET.getText().toString());
//                            ins.setDate(dateSignET.getText().toString());
                            ManpowerModel obs = new ManpowerModel();
                            obs.setIdno(idET.getText().toString());
                            obs.setName(nameET.getText().toString());
                            obs.setTitle(titleET.getText().toString());
                            obs.setOrg(descET.getText().toString());
                            selectedObserver = obs;
                            switch (type) {
                                case Constants.SEARCH_TYPE_INSPECTOR:
                                    selectedInspectorList.add(selectedObserver);
                                    text = inspectorET.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    String[] inspectors = text.split(";");
                                    inspectorET.setText(text);
                                    inspectorTL.setError(null);
                                    break;
                                case Constants.SEARCH_TYPE_AREA_OWNER:
                                    selectedAreaOwnerList.add(selectedObserver);
                                    text = areaOwnerET.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    areaOwnerET.setText(text);
                                    areaOwnerTL.setError(null);
                                    callback.onChange();
                                    break;
                                default:
                                    break;

                            }

                            dialog.dismiss();
                        }
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public void showPopUpInspector(final int type) {
        isEmptySignature = true;
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_planned_inspector);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

//        final AutoCompleteTextView searchET = (AutoCompleteTextView) dialog.findViewById(R.id.searchET);
//        final Spinner searchSpinner = (Spinner) dialog.findViewById(R.id.searchSpinner);
        final AutoCompleteTextView name = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
        final EditText idno = (EditText) dialog.findViewById(R.id.idET);
        final EditText title = (EditText) dialog.findViewById(R.id.titleET);
        final EditText org = (EditText) dialog.findViewById(R.id.orgET);
        final EditText dateSignET = (EditText) dialog.findViewById(R.id.dateET);
        final TextInputLayout dateTL = (TextInputLayout) dialog.findViewById(R.id.dateTL);
        dateSignET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.showDatePicker(v, (FragmentActivity) mActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                dateSignET.setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
            }
        });

        final ArrayList<String> data = new ArrayList<String>();

        for (int i = 0; i < manpowers.size(); i++) {
            data.add(manpowers.get(i).getName());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, data);
        name.setThreshold(1);
        name.setAdapter(adapter);
        name.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String key = (String) name.getText().toString();
                Log.d("WTF", key);
                int pos = data.indexOf(key);

                selectedObserver = manpowers.get(pos);
                name.setText(selectedObserver.getName());
                idno.setText(selectedObserver.getIdno());
                title.setText(selectedObserver.getTitle());
                org.setText(selectedObserver.getOrg());
            }
        });

        final LinearLayout signatureLayout = (LinearLayout) dialog
                .findViewById(R.id.signatureLayout);
        final TextView signatureWarning = (TextView) dialog
                .findViewById(R.id.signatureWarning);
        final LinearLayout warningLayout = (LinearLayout) dialog.findViewById(R.id.warningLayoutinsp);
        final LinearLayout signatureView = (LinearLayout) dialog.findViewById(R.id.signatureView);
        final ImageView signatureImage = (ImageView) dialog.findViewById(R.id.signatureIV);
        final ArrayList<View> mandatoryField = new ArrayList<View>();
        mandatoryField.add(name);
        mandatoryField.add(idno);

        switch (type) {
            case Constants.SEARCH_TYPE_AREA_OWNER:
            case Constants.SEARCH_TYPE_INSPECTOR:
//                signatureView.setVisibility(View.GONE);
//                break;
            default:
                break;
        }
        signatureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GesturePopUp.show(getActivity(),
                        new GesturePopUpCallback() {
                            @Override
                            public void onBitmapSaved(Bitmap bitmap) {
                                if (bitmap != null) {
                                    isEmptySignature = false;
                                    signatureImage.setImageBitmap(bitmap);
                                    ((TextView) dialog.findViewById(R.id.signatureWarning))
                                            .setVisibility(View.GONE);
                                } else {
                                    isEmptySignature = true;
                                    signatureImage.setImageBitmap(null);
                                    ((TextView) dialog.findViewById(R.id.signatureWarning))
                                            .setVisibility(View.VISIBLE);
                                }
                            }
                        }
                );
            }
        });

        if (warningLayout != null) {
            warningLayout.setVisibility(View.GONE);
            ButtonRectangle deleteBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    name.setText("");
                    idno.setText("");
                    title.setText("");
                    org.setText("");
                }
            });
        }

        ButtonRectangle okBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean allMandatoryFieldFilled = true;
                for (View _mandatoryField : mandatoryField) {
                    if (((EditText) _mandatoryField).length() < 1) {
                        allMandatoryFieldFilled = false;
                        break;
                    }
                }
                DataSource ds = new DataSource(mActivity);
                ds.open();
                PlannedSignModel ins = new PlannedSignModel();

                if (!allMandatoryFieldFilled) {
                    warningLayout.setVisibility(View.VISIBLE);
                } else {
                    if ((type == Constants.SEARCH_TYPE_AREA_OWNER || type == Constants.SEARCH_TYPE_INSPECTOR)
                            || !isEmptySignature) {
                        if (dateSignET.getText().toString().length() != 0) {
                            String text = "";
                            ins.setIdNo(idno.getText().toString());
                            ins.setName(name.getText().toString().toUpperCase());
                            ins.setTitle(title.getText().toString());
                            ins.setInspector(inspectorET.getText().toString());
                            ins.setAreaOwner(areaOwnerET.getText().toString());
                            ins.setLocation(locationET.getText().toString());
                            ins.setDateIns(dateET.getText().toString());
                            ins.setDate(dateSignET.getText().toString());
                            ManpowerModel manpower = new ManpowerModel();
                            manpower.setIdno(idno.getText().toString());
                            manpower.setName(name.getText().toString().toUpperCase());
                            manpower.setTitle(title.getText().toString());
                            selectedObserver = manpower;
                            switch (type) {
                                case Constants.SEARCH_TYPE_AREA_OWNER:
                                    selectedAreaOwnerList.add(selectedObserver);
                                    text = areaOwnerET.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    areaOwnerET.setText(text);
                                    areaOwnerTL.setError(null);
                                    break;

                                case Constants.SEARCH_TYPE_AREA_SAFETY:
                                    areaSafety = selectedObserver;
                                    areaSafety
                                            .setSign(((BitmapDrawable) signatureImage
                                                    .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                                    .getDrawable()).getBitmap() : null);
                                    ins.setSign(((BitmapDrawable) signatureImage
                                            .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                            .getDrawable()).getBitmap() : null);
                                    deptSafeET
                                            .setText(selectedObserver.getName()
                                                    + " ("
                                                    + selectedObserver.getIdno()
                                                    + "); ");
                                    dateSafeET.setText(dateSignET.getText().toString());
                                    ins.setCode("Safety");

                                    break;
                                case Constants.SEARCH_TYPE_DETP_MGR:
                                    deptMgr = selectedObserver;
                                    deptMgr.setSign(((BitmapDrawable) signatureImage
                                            .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                            .getDrawable()).getBitmap() : null);
                                    ins.setSign(((BitmapDrawable) signatureImage
                                            .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                            .getDrawable()).getBitmap() : null);
                                    deptManagerET
                                            .setText(selectedObserver.getName()
                                                    + " ("
                                                    + selectedObserver.getIdno()
                                                    + ") ");
                                    dateDeptET.setText(dateSignET.getText().toString());
                                    ins.setCode("Manager");
                                    break;
                                case Constants.SEARCH_TYPE_INSPECTOR:
                                    selectedInspectorList.add(selectedObserver);
                                    text = inspectorET.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    String[] inspectors = text.split(";");
                                    inspectorET.setText(text);
                                    inspectorTL.setError(null);
                                    break;
                                case Constants.SEARCH_TYPE_SITE_SPV:
                                    siteSpv = selectedObserver;
                                    siteSpv.setSign(((BitmapDrawable) signatureImage
                                            .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                            .getDrawable()).getBitmap() : null);
                                    ins.setSign(((BitmapDrawable) signatureImage
                                            .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                            .getDrawable()).getBitmap() : null);
                                    siteSVET
                                            .setText(selectedObserver.getName()
                                                    + " ("
                                                    + selectedObserver.getIdno()
                                                    + "); ");
                                    dateSiteET.setText(dateSignET.getText().toString());
                                    ins.setCode("Supervisor");
                                    break;
                                case Constants.SEARCH_TYPE_SPI_POPUP:
                                    ((EditText) activeDialog
                                            .findViewById(R.id.responsET))
                                            .setText(selectedObserver.getName()
                                                    + " ("
                                                    + selectedObserver.getIdno()
                                                    + "); ");
                                    break;
                                default:
                                    break;
                            }
                            dialog.dismiss();
                        } else {
                            dateTL.setErrorEnabled(true);
                            dateTL.setError("Please fill date first");
                        }
                    } else {
                        Helper.showPositiveNegativeDialog(
                                getActivity(),
                                String.valueOf("Warning"),
                                String.valueOf("The signature still empty. Are you sure want to continue?"),
                                String.valueOf("Yes"),
                                String.valueOf("No"),
                                null,
                                null,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int which) {
                                        String text = "";

                                        PlannedSignModel ins = new PlannedSignModel();
                                        ins.setIdNo(idno.getText().toString());
                                        ins.setName(name.getText().toString().toUpperCase());
                                        ins.setTitle(title.getText().toString());
                                        ins.setInspector(inspectorET.getText().toString());
                                        ins.setAreaOwner(areaOwnerET.getText().toString());
                                        ins.setLocation(locationET.getText().toString());
                                        ins.setDateIns(dateET.getText().toString());
                                        ins.setDate(dateSignET.getText().toString());
                                        ManpowerModel manpower = new ManpowerModel();
                                        manpower.setIdno(idno.getText().toString());
                                        manpower.setName(name.getText().toString().toUpperCase());
                                        manpower.setTitle(title.getText().toString());
                                        selectedObserver = manpower;
                                        switch (type) {
                                            case Constants.SEARCH_TYPE_AREA_OWNER:
                                                selectedAreaOwnerList.add(selectedObserver);
                                                text = areaOwnerET.getText().toString();
                                                text += selectedObserver.getName() + " ("
                                                        + selectedObserver.getIdno() + "); ";
                                                areaOwnerET.setText(text);
                                                areaOwnerTL.setError(null);
                                                break;

                                            case Constants.SEARCH_TYPE_AREA_SAFETY:
                                                areaSafety = selectedObserver;
                                                areaSafety
                                                        .setSign(((BitmapDrawable) signatureImage
                                                                .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                                                .getDrawable()).getBitmap() : null);
                                                deptSafeET
                                                        .setText(selectedObserver.getName()
                                                                + " ("
                                                                + selectedObserver.getIdno()
                                                                + "); ");
                                                ins.setCode("Safety");
                                                break;
                                            case Constants.SEARCH_TYPE_DETP_MGR:
                                                deptMgr = selectedObserver;
                                                deptMgr.setSign(((BitmapDrawable) signatureImage
                                                        .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                                        .getDrawable()).getBitmap() : null);
                                                deptManagerET
                                                        .setText(selectedObserver.getName()
                                                                + " ("
                                                                + selectedObserver.getIdno()
                                                                + ") ");
                                                ins.setCode("Manager");
                                                break;
                                            case Constants.SEARCH_TYPE_INSPECTOR:
                                                selectedInspectorList.add(selectedObserver);
                                                text = inspectorET.getText().toString();
                                                text += selectedObserver.getName() + " ("
                                                        + selectedObserver.getIdno() + "); ";
                                                String[] inspectors = text.split(";");
                                                inspectorET.setText(text);
                                                inspectorTL.setError(null);
                                                break;
                                            case Constants.SEARCH_TYPE_SITE_SPV:
                                                siteSpv = selectedObserver;
                                                siteSpv.setSign(((BitmapDrawable) signatureImage
                                                        .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                                        .getDrawable()).getBitmap() : null);
                                                siteSVET
                                                        .setText(selectedObserver.getName()
                                                                + " ("
                                                                + selectedObserver.getIdno()
                                                                + "); ");
                                                ins.setCode("Supervisor");
                                                break;
                                            case Constants.SEARCH_TYPE_SPI_POPUP:
                                                ((EditText) activeDialog
                                                        .findViewById(R.id.responsET))
                                                        .setText(selectedObserver.getName()
                                                                + " ("
                                                                + selectedObserver.getIdno()
                                                                + "); ");
                                                break;
                                            default:
                                                break;


                                        }
                                        dialog.dismiss();
                                        dialogInterface.dismiss();
                                    }
                                }
                        );

                    }
                    ds.insertPlannedSign(ins);

                    ds.close();
                }
            }
        });

        ButtonRectangle cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void datePickers(final Activity MainActivity, final View rootView) {
        switch (rootView.getId()) {
            case R.id.dateET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;
            case R.id.dateSiteET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;
            case R.id.dateDeptET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;
            case R.id.dateSafeET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;

            default:
                break;
        }
    }

    public void headerLine() {
        if (rootView != null) {
            pl_waTV = (TextView) rootView.findViewById(R.id.pl_waTV);
            pl_stTV = (TextView) rootView.findViewById(R.id.pl_stTV);
            pl_mtTV = (TextView) rootView.findViewById(R.id.pl_mtTV);
            pl_elTV = (TextView) rootView.findViewById(R.id.pl_elTV);
            pl_peTV = (TextView) rootView.findViewById(R.id.pl_peTV);
            pl_fsTV = (TextView) rootView.findViewById(R.id.pl_fsTV);
            pl_faTV = (TextView) rootView.findViewById(R.id.pl_faTV);
            pl_enTV = (TextView) rootView.findViewById(R.id.pl_enTV);
            pl_comTV = (TextView) rootView.findViewById(R.id.pl_comTV);
            pl_fpTV = (TextView) rootView.findViewById(R.id.pl_fpTV);

            pl_waCV = (CardView) rootView.findViewById(R.id.pl_waCV);
            pl_stCV = (CardView) rootView.findViewById(R.id.pl_stCV);
            pl_mtCV = (CardView) rootView.findViewById(R.id.pl_mtCV);
            pl_elCV = (CardView) rootView.findViewById(R.id.pl_elCV);
            pl_peCV = (CardView) rootView.findViewById(R.id.pl_peCV);
            pl_fsCV = (CardView) rootView.findViewById(R.id.pl_fsCV);
            pl_faCV = (CardView) rootView.findViewById(R.id.pl_faCV);
            pl_enCV = (CardView) rootView.findViewById(R.id.pl_enCV);
            pl_comCV = (CardView) rootView.findViewById(R.id.pl_comCV);
            pl_fpCV = (CardView) rootView.findViewById(R.id.pl_fpCV);
            findingCV = (CardView) rootView.findViewById(R.id.findingCV);

            pl_waLL = (LinearLayout) rootView.findViewById(R.id.pl_waLL);
            pl_stLL = (LinearLayout) rootView.findViewById(R.id.pl_stLL);
            pl_mtLL = (LinearLayout) rootView.findViewById(R.id.pl_mtLL);
            pl_elLL = (LinearLayout) rootView.findViewById(R.id.pl_elLL);
            pl_peLL = (LinearLayout) rootView.findViewById(R.id.pl_peLL);
            pl_fsLL = (LinearLayout) rootView.findViewById(R.id.pl_fsLL);
            pl_faLL = (LinearLayout) rootView.findViewById(R.id.pl_faLL);
            pl_enLL = (LinearLayout) rootView.findViewById(R.id.pl_enLL);
            pl_comLL = (LinearLayout) rootView.findViewById(R.id.pl_comLL);
            pl_fpLL = (LinearLayout) rootView.findViewById(R.id.pl_fpLL);
            findingsLL = (LinearLayout) rootView.findViewById(R.id.findingsLL);

            waIV = (ImageView) rootView.findViewById(R.id.waIV);
            sfIV = (ImageView) rootView.findViewById(R.id.sfIV);
            mtIV = (ImageView) rootView.findViewById(R.id.mtIV);
            elIV = (ImageView) rootView.findViewById(R.id.elIV);
            fsaIV = (ImageView) rootView.findViewById(R.id.fsaIV);
            FAIV = (ImageView) rootView.findViewById(R.id.FAIV);
            comIV = (ImageView) rootView.findViewById(R.id.comIV);
            FPIV = (ImageView) rootView.findViewById(R.id.FPIV);
            findingsIV = (ImageView) rootView.findViewById(R.id.findingIV);
            peIV = (ImageView) rootView.findViewById(R.id.peIV);
            enIV = (ImageView) rootView.findViewById(R.id.enIV);

            pl_waTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_waLL.getVisibility() == View.GONE) {
                            pl_waLL.setVisibility(View.VISIBLE);
                            waIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_waLL.getVisibility() == View.VISIBLE) {
                            pl_waLL.setVisibility(View.GONE);
                            waIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            waIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_waLL.getVisibility() == View.GONE) {
                            pl_waLL.setVisibility(View.VISIBLE);
                            waIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_waLL.getVisibility() == View.VISIBLE) {
                            pl_waLL.setVisibility(View.GONE);
                            waIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_stTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_stLL.getVisibility() == View.GONE) {
                            pl_stLL.setVisibility(View.VISIBLE);
                            sfIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_stLL.getVisibility() == View.VISIBLE) {
                            pl_stLL.setVisibility(View.GONE);
                            sfIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            sfIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_stLL.getVisibility() == View.GONE) {
                            pl_stLL.setVisibility(View.VISIBLE);
                            sfIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_stLL.getVisibility() == View.VISIBLE) {
                            pl_stLL.setVisibility(View.GONE);
                            sfIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_mtTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_mtLL.getVisibility() == View.GONE) {
                            pl_mtLL.setVisibility(View.VISIBLE);
                            mtIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_mtLL.getVisibility() == View.VISIBLE) {
                            pl_mtLL.setVisibility(View.GONE);
                            mtIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            mtIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_mtLL.getVisibility() == View.GONE) {
                            pl_mtLL.setVisibility(View.VISIBLE);
                            mtIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_mtLL.getVisibility() == View.VISIBLE) {
                            pl_mtLL.setVisibility(View.GONE);
                            mtIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_elTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_elLL.getVisibility() == View.GONE) {
                            pl_elLL.setVisibility(View.VISIBLE);
                            elIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_elLL.getVisibility() == View.VISIBLE) {
                            pl_elLL.setVisibility(View.GONE);
                            elIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            elIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_elLL.getVisibility() == View.GONE) {
                            pl_elLL.setVisibility(View.VISIBLE);
                            elIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_elLL.getVisibility() == View.VISIBLE) {
                            pl_elLL.setVisibility(View.GONE);
                            elIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_peTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_peLL.getVisibility() == View.GONE) {
                            pl_peLL.setVisibility(View.VISIBLE);
                            peIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_peLL.getVisibility() == View.VISIBLE) {
                            pl_peLL.setVisibility(View.GONE);
                            elIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            peIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_peLL.getVisibility() == View.GONE) {
                            pl_peLL.setVisibility(View.VISIBLE);
                            peIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_peLL.getVisibility() == View.VISIBLE) {
                            pl_peLL.setVisibility(View.GONE);
                            elIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_fsTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_fsLL.getVisibility() == View.GONE) {
                            pl_fsLL.setVisibility(View.VISIBLE);
                            fsaIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_fsLL.getVisibility() == View.VISIBLE) {
                            pl_fsLL.setVisibility(View.GONE);
                            fsaIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            fsaIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_fsLL.getVisibility() == View.GONE) {
                            pl_fsLL.setVisibility(View.VISIBLE);
                            fsaIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_fsLL.getVisibility() == View.VISIBLE) {
                            pl_fsLL.setVisibility(View.GONE);
                            fsaIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_faTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_faLL.getVisibility() == View.GONE) {
                            pl_faLL.setVisibility(View.VISIBLE);
                            FAIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_faLL.getVisibility() == View.VISIBLE) {
                            pl_faLL.setVisibility(View.GONE);
                            FAIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            FAIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_faLL.getVisibility() == View.GONE) {
                            pl_faLL.setVisibility(View.VISIBLE);
                            FAIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_faLL.getVisibility() == View.VISIBLE) {
                            pl_faLL.setVisibility(View.GONE);
                            FAIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_enTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_enLL.getVisibility() == View.GONE) {
                            pl_enLL.setVisibility(View.VISIBLE);
                            enIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_enLL.getVisibility() == View.VISIBLE) {
                            pl_enLL.setVisibility(View.GONE);
                            enIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            enIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_enLL.getVisibility() == View.GONE) {
                            pl_enLL.setVisibility(View.VISIBLE);
                            enIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_enLL.getVisibility() == View.VISIBLE) {
                            pl_enLL.setVisibility(View.GONE);
                            enIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_comTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_comLL.getVisibility() == View.GONE) {
                            pl_comLL.setVisibility(View.VISIBLE);
                            comIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_comLL.getVisibility() == View.VISIBLE) {
                            pl_comLL.setVisibility(View.GONE);
                            comIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            comIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_comLL.getVisibility() == View.GONE) {
                            pl_comLL.setVisibility(View.VISIBLE);
                            comIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_comLL.getVisibility() == View.VISIBLE) {
                            pl_comLL.setVisibility(View.GONE);
                            comIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            pl_fpTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_fpLL.getVisibility() == View.GONE) {
                            pl_fpLL.setVisibility(View.VISIBLE);
                            FPIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_fpLL.getVisibility() == View.VISIBLE) {
                            pl_fpLL.setVisibility(View.GONE);
                            FPIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            FPIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (pl_fpLL.getVisibility() == View.GONE) {
                            pl_fpLL.setVisibility(View.VISIBLE);
                            FPIV.setImageResource(R.drawable.collapse_logo);
                        } else if (pl_fpLL.getVisibility() == View.VISIBLE) {
                            pl_fpLL.setVisibility(View.GONE);
                            FPIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            findingCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (findingsLL.getVisibility() == View.GONE) {
                            findingsLL.setVisibility(View.VISIBLE);
                            findingsIV.setImageResource(R.drawable.collapse_logo);
                        } else if (findingsLL.getVisibility() == View.VISIBLE) {
                            findingsLL.setVisibility(View.GONE);
                            findingsIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

            findingsIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mainValidation()) {
                        if (findingsLL.getVisibility() == View.GONE) {
                            findingsLL.setVisibility(View.VISIBLE);
                            findingsIV.setImageResource(R.drawable.collapse_logo);
                        } else if (findingsLL.getVisibility() == View.VISIBLE) {
                            findingsLL.setVisibility(View.GONE);
                            findingsIV.setImageResource(R.drawable.expand_logo);
                        }
                    }
                }
            });

        }
    }

    public boolean mainValidation() {
        boolean status = true;

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date first");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("Please fill Location first");
        }

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector first");
        }

        if (areaOwnerET.getText().toString().length() == 0) {
            status = false;
            areaOwnerTL.setErrorEnabled(true);
            areaOwnerTL.setError("Please fill Area Owner first");
        }

        return status;
    }

    /* validation data */
    public static boolean validationData() {
        boolean status = true;

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill date first");
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("Please fill location first");
        }

        if (areaOwnerET.getText().toString().length() == 0) {
            status = false;
            areaOwnerTL.setErrorEnabled(true);
            areaOwnerTL.setError("Please fill Area Owner first");
        }

        return status;
    }

    /* Load Inspection */
    public void loadData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

         /* MAIN MODEL */

        PlannedMainModel model = ds.getLastestPlannedMain();
        dateET.setText(model.getDate());
        dateTL.setError(null);
        inspectorET.setText(model.getInspector());
        inspectorTL.setError(null);
        String[] inspectors = model.getInspector().split(";");
        for (String inspector : inspectors) {
            inspector = inspector.trim();
            if (inspector.length() > 0) {
                String inspectorName = inspector.split("\\(")[0];
                inspectorName = inspectorName.trim();
                boolean found = false;
                for (ManpowerModel manpower : manpowers) {
                    if (manpower.getName().equalsIgnoreCase(inspectorName)) {
                        selectedInspectorList.add(manpower);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    ManpowerModel selectedManpower = new ManpowerModel();
                    String _inspectorID = inspector.split("\\(")[1];
                    String manpowerID = _inspectorID.substring(0,
                            _inspectorID.length() - 1);
                    selectedManpower.setIdno(manpowerID);
                    selectedManpower.setName(inspectorName);
                    selectedInspectorList.add(selectedManpower);
                }
            }
        }

        areaOwnerET.setText(model.getAreaOwner());
        areaOwnerTL.setError(null);

        String[] areaOwners = model.getAreaOwner().split(";");
        for (String areaOwner : areaOwners) {
            areaOwner = areaOwner.trim();
            if (areaOwner.length() > 0) {
                String areaOwnerName = areaOwner.split("\\(")[0];
                areaOwnerName = areaOwnerName.trim();
                boolean found = false;
                for (ManpowerModel manpower : manpowers) {
                    if (manpower.getName().equalsIgnoreCase(areaOwnerName)) {
                        selectedAreaOwnerList.add(manpower);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    ManpowerModel selectedManpower = new ManpowerModel();
                    String _inspectorID = areaOwner.split("\\(")[1];
                    String manpowerID = _inspectorID.substring(0,
                            _inspectorID.length() - 1);
                    selectedManpower.setIdno(manpowerID);
                    selectedManpower.setName(areaOwnerName);
                    selectedAreaOwnerList.add(selectedManpower);
                }
            }
        }
        locationET.setText(model.getLocation());
        locationTL.setError(null);


         /* === PLANNED SHE WORK AREA ===  */

        PlannedInspectionModel workArea = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Work Area");
        if (workArea != null) {
            if (workArea.getRisk1().equals("High")) {
                inspWaRisk1.setValue(0);
            } else if (workArea.getRisk1().equals("Med")) {
                inspWaRisk1.setValue(1);
            } else if (workArea.getRisk1().equals("Low")) {
                inspWaRisk1.setValue(2);
            } else {
                inspWaRisk1.setValue(-1);
            }

            if (workArea.getRating1().equals("NW")) {
                inspWaRate1.setValue(0);
            } else if (workArea.getRating1().equals("Good")) {
                inspWaRate1.setValue(1);
            } else if (workArea.getRating1().equals("N/A")) {
                inspWaRate1.setValue(2);
            } else {
                inspWaRate1.setValue(-1);
            }


            if (workArea.getRisk2().equals("High")) {
                inspWaRisk2.setValue(0);
            } else if (workArea.getRisk2().equals("Med")) {
                inspWaRisk2.setValue(1);
            } else if (workArea.getRisk2().equals("Low")) {
                inspWaRisk2.setValue(2);
            } else {
                inspWaRisk2.setValue(-1);
            }

            if (workArea.getRating2().equals("NW")) {
                inspWaRate2.setValue(0);
            } else if (workArea.getRating2().equals("Good")) {
                inspWaRate2.setValue(1);
            } else if (workArea.getRating2().equals("N/A")) {
                inspWaRate2.setValue(2);
            } else {
                inspWaRate2.setValue(-1);
            }


            if (workArea.getRisk3().equals("High")) {
                inspWaRisk3.setValue(0);
            } else if (workArea.getRisk3().equals("Med")) {
                inspWaRisk3.setValue(1);
            } else if (workArea.getRisk3().equals("Low")) {
                inspWaRisk3.setValue(2);
            } else {
                inspWaRisk3.setValue(-1);
            }

            if (workArea.getRating3().equals("NW")) {
                inspWaRate3.setValue(0);
            } else if (workArea.getRating3().equals("Good")) {
                inspWaRate3.setValue(1);
            } else if (workArea.getRating3().equals("N/A")) {
                inspWaRate3.setValue(2);
            } else {
                inspWaRate3.setValue(-1);
            }


            if (workArea.getRisk4().equals("High")) {
                inspWaRisk4.setValue(0);
            } else if (workArea.getRisk4().equals("Med")) {
                inspWaRisk4.setValue(1);
            } else if (workArea.getRisk4().equals("Low")) {
                inspWaRisk4.setValue(2);
            } else {
                inspWaRisk4.setValue(-1);
            }

            if (workArea.getRating4().equals("NW")) {
                inspWaRate4.setValue(0);
            } else if (workArea.getRating4().equals("Good")) {
                inspWaRate4.setValue(1);
            } else if (workArea.getRating4().equals("N/A")) {
                inspWaRate4.setValue(2);
            } else {
                inspWaRate4.setValue(-1);
            }


            if (workArea.getRisk5().equals("High")) {
                inspWaRisk5.setValue(0);
            } else if (workArea.getRisk5().equals("Med")) {
                inspWaRisk5.setValue(1);
            } else if (workArea.getRisk5().equals("Low")) {
                inspWaRisk5.setValue(2);
            } else {
                inspWaRisk5.setValue(-1);
            }

            if (workArea.getRating5().equals("NW")) {
                inspWaRate5.setValue(0);
            } else if (workArea.getRating5().equals("Good")) {
                inspWaRate5.setValue(1);
            } else if (workArea.getRating5().equals("N/A")) {
                inspWaRate5.setValue(2);
            } else {
                inspWaRate5.setValue(-1);
            }


            if (workArea.getRisk6().equals("High")) {
                inspWaRisk6.setValue(0);
            } else if (workArea.getRisk6().equals("Med")) {
                inspWaRisk6.setValue(1);
            } else if (workArea.getRisk6().equals("Low")) {
                inspWaRisk6.setValue(2);
            } else {
                inspWaRisk6.setValue(-1);
            }

            if (workArea.getRating6().equals("NW")) {
                inspWaRate6.setValue(0);
            } else if (workArea.getRating6().equals("Good")) {
                inspWaRate6.setValue(1);
            } else if (workArea.getRating6().equals("N/A")) {
                inspWaRate6.setValue(2);
            } else {
                inspWaRate6.setValue(-1);
            }

         /* === STRUCTURE === */

            PlannedInspectionModel structureFloor = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Structure");
            if (structureFloor != null) {
                if (structureFloor.getRisk1().equals("High")) {
                    inspStRisk1.setValue(0);
                } else if (structureFloor.getRisk1().equals("Med")) {
                    inspStRisk1.setValue(1);
                } else if (structureFloor.getRisk1().equals("Low")) {
                    inspStRisk1.setValue(2);
                } else {
                    inspStRisk1.setValue(-1);
                }

                if (structureFloor.getRating1().equals("NW")) {
                    inspStRate1.setValue(0);
                } else if (structureFloor.getRating1().equals("Good")) {
                    inspStRate1.setValue(1);
                } else if (structureFloor.getRating1().equals("N/A")) {
                    inspStRate1.setValue(2);
                } else {
                    inspStRate1.setValue(-1);
                }

                if (structureFloor.getRisk2().equals("High")) {
                    inspStRisk2.setValue(0);
                } else if (structureFloor.getRisk2().equals("Med")) {
                    inspStRisk2.setValue(1);
                } else if (structureFloor.getRisk2().equals("Low")) {
                    inspStRisk2.setValue(2);
                } else {
                    inspStRisk2.setValue(-1);
                }

                if (structureFloor.getRating1().equals("NW")) {
                    inspStRate2.setValue(0);
                } else if (structureFloor.getRating2().equals("Good")) {
                    inspStRate2.setValue(1);
                } else if (structureFloor.getRating2().equals("N/A")) {
                    inspStRate2.setValue(2);
                } else {
                    inspStRate2.setValue(-1);
                }


                if (structureFloor.getRisk3().equals("High")) {
                    inspStRisk3.setValue(0);
                } else if (structureFloor.getRisk3().equals("Med")) {
                    inspStRisk3.setValue(1);
                } else if (structureFloor.getRisk3().equals("Low")) {
                    inspStRisk3.setValue(2);
                } else {
                    inspStRisk3.setValue(-1);
                }

                if (structureFloor.getRating3().equals("NW")) {
                    inspStRate3.setValue(0);
                } else if (structureFloor.getRating3().equals("Good")) {
                    inspStRate3.setValue(1);
                } else if (structureFloor.getRating3().equals("N/A")) {
                    inspStRate3.setValue(2);
                } else {
                    inspStRate3.setValue(-1);
                }

                if (structureFloor.getRisk4().equals("High")) {
                    inspStRisk4.setValue(0);
                } else if (structureFloor.getRisk4().equals("Med")) {
                    inspStRisk4.setValue(1);
                } else if (structureFloor.getRisk4().equals("Low")) {
                    inspStRisk4.setValue(2);
                } else {
                    inspStRisk4.setValue(-1);
                }

                if (structureFloor.getRating4().equals("NW")) {
                    inspStRate4.setValue(0);
                } else if (structureFloor.getRating4().equals("Good")) {
                    inspStRate4.setValue(1);
                } else if (structureFloor.getRating4().equals("N/A")) {
                    inspStRate4.setValue(2);
                } else {
                    inspStRate4.setValue(-1);
                }

                if (structureFloor.getRisk5().equals("High")) {
                    inspStRisk5.setValue(0);
                } else if (structureFloor.getRisk5().equals("Med")) {
                    inspStRisk5.setValue(1);
                } else if (structureFloor.getRisk5().equals("Low")) {
                    inspStRisk5.setValue(2);
                } else {
                    inspStRisk5.setValue(-1);
                }

                if (structureFloor.getRating5().equals("NW")) {
                    inspStRate5.setValue(0);
                } else if (structureFloor.getRating5().equals("Good")) {
                    inspStRate5.setValue(1);
                } else if (structureFloor.getRating5().equals("N/A")) {
                    inspStRate5.setValue(2);
                } else {
                    inspStRate5.setValue(-1);
                }

            }

             /* === MACHINERY TOOLS === */

            PlannedInspectionModel machineryTools = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Machinery");
            if (machineryTools != null) {
                if (machineryTools.getRisk1().equals("High")) {
                    inspMtRisk1.setValue(0);
                } else if (machineryTools.getRisk1().equals("Med")) {
                    inspMtRisk1.setValue(1);
                } else if (machineryTools.getRisk1().equals("Low")) {
                    inspMtRisk1.setValue(2);
                } else {
                    inspMtRisk1.setValue(-1);
                }

                if (machineryTools.getRating1().equals("NW")) {
                    inspMtRate1.setValue(0);
                } else if (machineryTools.getRating1().equals("Good")) {
                    inspMtRate1.setValue(1);
                } else if (machineryTools.getRating1().equals("N/A")) {
                    inspMtRate1.setValue(2);
                } else {
                    inspMtRate1.setValue(-1);
                }

                if (machineryTools.getRisk2().equals("High")) {
                    inspMtRisk2.setValue(0);
                } else if (machineryTools.getRisk2().equals("Medium")) {
                    inspMtRisk2.setValue(1);
                } else if (machineryTools.getRisk2().equals("Low")) {
                    inspMtRisk2.setValue(2);
                } else {
                    inspMtRisk2.setValue(-1);
                }

                if (machineryTools.getRating2().equals("NW")) {
                    inspMtRate2.setValue(0);
                } else if (machineryTools.getRating2().equals("Good")) {
                    inspMtRate2.setValue(1);
                } else if (machineryTools.getRating1().equals("N/A")) {
                    inspMtRate2.setValue(2);
                } else {
                    inspMtRate2.setValue(-1);
                }

                if (machineryTools.getRisk3().equals("High")) {
                    inspMtRisk3.setValue(0);
                } else if (machineryTools.getRisk3().equals("Med")) {
                    inspMtRisk3.setValue(1);
                } else if (machineryTools.getRisk3().equals("Low")) {
                    inspMtRisk3.setValue(2);
                } else {
                    inspMtRisk3.setValue(-1);
                }

                if (machineryTools.getRating3().equals("NW")) {
                    inspMtRate3.setValue(0);
                } else if (machineryTools.getRating3().equals("Good")) {
                    inspMtRate3.setValue(1);
                } else if (machineryTools.getRating3().equals("N/A")) {
                    inspMtRate3.setValue(2);
                } else {
                    inspMtRate3.setValue(-1);
                }

                if (machineryTools.getRisk4().equals("High")) {
                    inspMtRisk4.setValue(0);
                } else if (machineryTools.getRisk4().equals("Med")) {
                    inspMtRisk4.setValue(1);
                } else if (machineryTools.getRisk4().equals("Low")) {
                    inspMtRisk4.setValue(2);
                } else {
                    inspMtRisk4.setValue(-1);
                }

                if (machineryTools.getRating4().equals("NW")) {
                    inspMtRate4.setValue(0);
                } else if (machineryTools.getRating4().equals("Good")) {
                    inspMtRate4.setValue(1);
                } else if (machineryTools.getRating4().equals("N/A")) {
                    inspMtRate4.setValue(2);
                } else {
                    inspMtRate4.setValue(-1);
                }

                if (machineryTools.getRisk5().equals("High")) {
                    inspMtRisk5.setValue(0);
                } else if (machineryTools.getRisk5().equals("Med")) {
                    inspMtRisk5.setValue(1);
                } else if (machineryTools.getRisk5().equals("Low")) {
                    inspMtRisk5.setValue(2);
                } else {
                    inspMtRisk5.setValue(-1);
                }

                if (machineryTools.getRating5().equals("NW")) {
                    inspMtRate5.setValue(0);
                } else if (machineryTools.getRating5().equals("Good")) {
                    inspMtRate5.setValue(1);
                } else if (machineryTools.getRating5().equals("N/A")) {
                    inspMtRate5.setValue(2);
                } else {
                    inspMtRate5.setValue(-1);
                }

                if (machineryTools.getRisk6().equals("High")) {
                    inspMtRisk6.setValue(0);
                } else if (machineryTools.getRisk6().equals("Med")) {
                    inspMtRisk6.setValue(1);
                } else if (machineryTools.getRisk6().equals("Low")) {
                    inspMtRisk6.setValue(2);
                } else {
                    inspMtRisk6.setValue(-1);
                }

                if (machineryTools.getRating6().equals("NW")) {
                    inspMtRate6.setValue(0);
                } else if (machineryTools.getRating6().equals("Good")) {
                    inspMtRate6.setValue(1);
                } else if (machineryTools.getRating6().equals("N/A")) {
                    inspMtRate6.setValue(2);
                } else {
                    inspMtRate6.setValue(-1);
                }

                if (machineryTools.getRisk7().equals("High")) {
                    inspMtRisk7.setValue(0);
                } else if (machineryTools.getRisk7().equals("Med")) {
                    inspMtRisk7.setValue(1);
                } else if (machineryTools.getRisk1().equals("Low")) {
                    inspMtRisk7.setValue(2);
                } else {
                    inspMtRisk7.setValue(-1);
                }

                if (machineryTools.getRating7().equals("NW")) {
                    inspMtRate7.setValue(0);
                } else if (machineryTools.getRating7().equals("Good")) {
                    inspMtRate7.setValue(1);
                } else if (machineryTools.getRating7().equals("N/A")) {
                    inspMtRate7.setValue(2);
                } else {
                    inspMtRate7.setValue(-1);
                }


            }

             /* === ELECTRICAL === */

            PlannedInspectionModel electrical = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Electrical");
            if (electrical != null) {
                if (electrical.getRisk1().equals("High")) {
                    inspElRisk1.setValue(0);
                } else if (electrical.getRisk1().equals("Med")) {
                    inspElRisk1.setValue(1);
                } else if (electrical.getRisk1().equals("Low")) {
                    inspElRisk1.setValue(2);
                } else {
                    inspElRisk1.setValue(-1);
                }

                if (electrical.getRating1().equals("NW")) {
                    inspElRate1.setValue(0);
                } else if (electrical.getRating1().equals("Good")) {
                    inspElRate1.setValue(1);
                } else if (electrical.getRating1().equals("N/A")) {
                    inspElRate1.setValue(2);
                } else {
                    inspElRate1.setValue(-1);
                }

                if (electrical.getRisk2().equals("High")) {
                    inspElRisk2.setValue(0);
                } else if (electrical.getRisk2().equals("Med")) {
                    inspElRisk2.setValue(1);
                } else if (electrical.getRisk2().equals("Low")) {
                    inspElRisk2.setValue(2);
                } else {
                    inspElRisk2.setValue(-1);
                }

                if (electrical.getRating2().equals("NW")) {
                    inspElRate2.setValue(0);
                } else if (electrical.getRating2().equals("Good")) {
                    inspElRate2.setValue(1);
                } else if (electrical.getRating2().equals("N/A")) {
                    inspElRate2.setValue(2);
                } else {
                    inspElRate2.setValue(-1);
                }

                if (electrical.getRisk3().equals("High")) {
                    inspElRisk3.setValue(0);
                } else if (electrical.getRisk3().equals("Med")) {
                    inspElRisk3.setValue(1);
                } else if (electrical.getRisk3().equals("Low")) {
                    inspElRisk3.setValue(2);
                } else {
                    inspElRisk3.setValue(-1);
                }

                if (electrical.getRating3().equals("NW")) {
                    inspElRate3.setValue(0);
                } else if (electrical.getRating3().equals("Good")) {
                    inspElRate3.setValue(1);
                } else if (electrical.getRating3().equals("N/A")) {
                    inspElRate3.setValue(2);
                } else {
                    inspElRate3.setValue(-1);
                }

                if (electrical.getRisk4().equals("High")) {
                    inspElRisk4.setValue(0);
                } else if (electrical.getRisk4().equals("Med")) {
                    inspElRisk4.setValue(1);
                } else if (electrical.getRisk4().equals("Low")) {
                    inspElRisk4.setValue(2);
                } else {
                    inspElRisk4.setValue(-1);
                }

                if (electrical.getRating4().equals("NW")) {
                    inspElRate4.setValue(0);
                } else if (electrical.getRating4().equals("Good")) {
                    inspElRate4.setValue(1);
                } else if (electrical.getRating4().equals("N/A")) {
                    inspElRate4.setValue(2);
                } else {
                    inspElRate4.setValue(-1);
                }

                if (electrical.getRisk5().equals("High")) {
                    inspElRisk5.setValue(0);
                } else if (electrical.getRisk5().equals("Med")) {
                    inspElRisk5.setValue(1);
                } else if (electrical.getRisk5().equals("Low")) {
                    inspElRisk5.setValue(2);
                } else {
                    inspElRisk5.setValue(-1);
                }

                if (electrical.getRating5().equals("NW")) {
                    inspElRate5.setValue(0);
                } else if (electrical.getRating5().equals("Good")) {
                    inspElRate5.setValue(1);
                } else if (electrical.getRating5().equals("N/A")) {
                    inspElRate5.setValue(2);
                } else {
                    inspElRate5.setValue(-1);
                }


            }

             /* === PERSONEL === */

            PlannedInspectionModel personel = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Personel");
            if (personel != null) {
                if (personel.getRisk1().equals("High")) {
                    inspPeRisk1.setValue(0);
                } else if (personel.getRisk1().equals("Med")) {
                    inspPeRisk1.setValue(1);
                } else if (personel.getRisk1().equals("Low")) {
                    inspPeRisk1.setValue(2);
                } else {
                    inspPeRisk1.setValue(-1);
                }

                if (personel.getRating1().equals("NW")) {
                    inspPeRate1.setValue(0);
                } else if (personel.getRating1().equals("Good")) {
                    inspPeRate1.setValue(1);
                } else if (personel.getRating1().equals("N/A")) {
                    inspPeRate1.setValue(2);
                } else {
                    inspPeRate1.setValue(-1);
                }

                if (personel.getRisk2().equals("High")) {
                    inspPeRisk2.setValue(0);
                } else if (personel.getRisk2().equals("Med")) {
                    inspPeRisk2.setValue(1);
                } else if (personel.getRisk2().equals("Low")) {
                    inspPeRisk2.setValue(2);
                } else {
                    inspPeRisk2.setValue(-1);
                }

                if (personel.getRating2().equals("NW")) {
                    inspPeRate2.setValue(0);
                } else if (personel.getRating2().equals("Good")) {
                    inspPeRate2.setValue(1);
                } else if (personel.getRating2().equals("N/A")) {
                    inspPeRate2.setValue(2);
                } else {
                    inspPeRate2.setValue(-1);
                }

                if (personel.getRisk3().equals("High")) {
                    inspPeRisk3.setValue(0);
                } else if (personel.getRisk3().equals("Med")) {
                    inspPeRisk3.setValue(1);
                } else if (personel.getRisk3().equals("Low")) {
                    inspPeRisk3.setValue(2);
                } else {
                    inspPeRisk3.setValue(-1);
                }

                if (personel.getRating3().equals("NW")) {
                    inspPeRate3.setValue(0);
                } else if (personel.getRating3().equals("Good")) {
                    inspPeRate3.setValue(1);
                } else if (personel.getRating3().equals("N/A")) {
                    inspPeRate3.setValue(2);
                } else {
                    inspPeRate3.setValue(-1);
                }

                if (personel.getRisk4().equals("High")) {
                    inspPeRisk4.setValue(0);
                } else if (personel.getRisk4().equals("Med")) {
                    inspPeRisk4.setValue(1);
                } else if (personel.getRisk4().equals("Low")) {
                    inspPeRisk4.setValue(2);
                } else {
                    inspPeRisk4.setValue(-1);
                }

                if (personel.getRating4().equals("NW")) {
                    inspPeRate4.setValue(0);
                } else if (personel.getRating4().equals("Good")) {
                    inspPeRate4.setValue(1);
                } else if (personel.getRating4().equals("N/A")) {
                    inspPeRate4.setValue(2);
                } else {
                    inspPeRate4.setValue(-1);
                }

                if (personel.getRisk5().equals("High")) {
                    inspPeRisk5.setValue(0);
                } else if (personel.getRisk5().equals("Med")) {
                    inspPeRisk5.setValue(1);
                } else if (personel.getRisk5().equals("Low")) {
                    inspPeRisk5.setValue(2);
                } else {
                    inspPeRisk5.setValue(-1);
                }

                if (personel.getRating5().equals("NW")) {
                    inspPeRate5.setValue(0);
                } else if (personel.getRating5().equals("Good")) {
                    inspPeRate5.setValue(1);
                } else if (personel.getRating5().equals("N/A")) {
                    inspPeRate5.setValue(2);
                } else {
                    inspPeRate5.setValue(-1);
                }

                if (personel.getRisk6().equals("High")) {
                    inspPeRisk6.setValue(0);
                } else if (personel.getRisk6().equals("Med")) {
                    inspPeRisk6.setValue(1);
                } else if (personel.getRisk6().equals("Low")) {
                    inspPeRisk6.setValue(2);
                } else {
                    inspPeRisk6.setValue(-1);
                }

                if (personel.getRating6().equals("NW")) {
                    inspPeRate6.setValue(0);
                } else if (personel.getRating6().equals("Good")) {
                    inspPeRate6.setValue(1);
                } else if (personel.getRating6().equals("N/A")) {
                    inspPeRate6.setValue(2);
                } else {
                    inspPeRate6.setValue(-1);
                }

                if (personel.getRisk7().equals("High")) {
                    inspPeRisk7.setValue(0);
                } else if (personel.getRisk7().equals("Med")) {
                    inspPeRisk7.setValue(1);
                } else if (personel.getRisk7().equals("Low")) {
                    inspPeRisk7.setValue(2);
                } else {
                    inspPeRisk7.setValue(-1);
                }

                if (personel.getRating7().equals("NW")) {
                    inspPeRate7.setValue(0);
                } else if (personel.getRating7().equals("Good")) {
                    inspPeRate7.setValue(1);
                } else if (personel.getRating7().equals("N/A")) {
                    inspPeRate7.setValue(2);
                } else {
                    inspPeRate7.setValue(-1);
                }

                if (personel.getRisk8().equals("High")) {
                    inspPeRisk8.setValue(0);
                } else if (personel.getRisk8().equals("Med")) {
                    inspPeRisk8.setValue(1);
                } else if (personel.getRisk8().equals("Low")) {
                    inspPeRisk8.setValue(2);
                } else {
                    inspPeRisk8.setValue(-1);
                }

                if (personel.getRating8().equals("NW")) {
                    inspPeRate8.setValue(0);
                } else if (personel.getRating8().equals("Good")) {
                    inspPeRate8.setValue(1);
                } else if (personel.getRating8().equals("N/A")) {
                    inspPeRate8.setValue(2);
                } else {
                    inspPeRate8.setValue(-1);
                }


                 /* === FIRE SAFETY === */

                PlannedInspectionModel fireSafety = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Fire Safety");
                if (fireSafety != null) {
                    if (fireSafety.getRisk1().equals("High")) {
                        inspFSRisk1.setValue(0);
                    } else if (fireSafety.getRisk1().equals("Med")) {
                        inspFSRisk1.setValue(1);
                    } else if (fireSafety.getRisk1().equals("Low")) {
                        inspFSRisk1.setValue(2);
                    } else {
                        inspFSRisk1.setValue(-1);
                    }

                    if (fireSafety.getRating1().equals("NW")) {
                        inspFSRate1.setValue(0);
                    } else if (fireSafety.getRating1().equals("Good")) {
                        inspFSRate1.setValue(1);
                    } else if (fireSafety.getRating1().equals("N/A")) {
                        inspFSRate1.setValue(2);
                    } else {
                        inspFSRate1.setValue(-1);
                    }

                    if (fireSafety.getRisk2().equals("High")) {
                        inspFSRisk2.setValue(0);
                    } else if (fireSafety.getRisk2().equals("Med")) {
                        inspFSRisk2.setValue(1);
                    } else if (fireSafety.getRisk2().equals("Low")) {
                        inspFSRisk2.setValue(2);
                    } else {
                        inspFSRisk2.setValue(-1);
                    }

                    if (fireSafety.getRating2().equals("NW")) {
                        inspFSRate2.setValue(0);
                    } else if (fireSafety.getRating2().equals("Good")) {
                        inspFSRate2.setValue(1);
                    } else if (fireSafety.getRating2().equals("N/A")) {
                        inspFSRate2.setValue(2);
                    } else {
                        inspFSRate2.setValue(-1);
                    }

                    if (fireSafety.getRisk3().equals("High")) {
                        inspFSRisk3.setValue(0);
                    } else if (fireSafety.getRisk3().equals("Med")) {
                        inspFSRisk3.setValue(1);
                    } else if (fireSafety.getRisk3().equals("Low")) {
                        inspFSRisk3.setValue(2);
                    } else {
                        inspFSRisk3.setValue(-1);
                    }

                    if (fireSafety.getRating3().equals("NW")) {
                        inspFSRate3.setValue(0);
                    } else if (fireSafety.getRating3().equals("Good")) {
                        inspFSRate3.setValue(1);
                    } else if (fireSafety.getRating3().equals("N/A")) {
                        inspFSRate3.setValue(2);
                    } else {
                        inspFSRate3.setValue(-1);
                    }

                    if (fireSafety.getRisk4().equals("High")) {
                        inspFSRisk4.setValue(0);
                    } else if (fireSafety.getRisk4().equals("Med")) {
                        inspFSRisk4.setValue(1);
                    } else if (fireSafety.getRisk4().equals("Low")) {
                        inspFSRisk4.setValue(2);
                    } else {
                        inspFSRisk4.setValue(-1);
                    }

                    if (fireSafety.getRating4().equals("NW")) {
                        inspFSRate4.setValue(0);
                    } else if (fireSafety.getRating4().equals("Good")) {
                        inspFSRate4.setValue(1);
                    } else if (fireSafety.getRating4().equals("N/A")) {
                        inspFSRate4.setValue(2);
                    } else {
                        inspFSRate4.setValue(-1);
                    }

                    if (fireSafety.getRisk5().equals("High")) {
                        inspFSRisk5.setValue(0);
                    } else if (fireSafety.getRisk5().equals("Med")) {
                        inspFSRisk5.setValue(1);
                    } else if (fireSafety.getRisk5().equals("Low")) {
                        inspFSRisk5.setValue(2);
                    } else {
                        inspFSRisk5.setValue(-1);
                    }

                    if (fireSafety.getRating5().equals("NW")) {
                        inspFSRate5.setValue(0);
                    } else if (fireSafety.getRating5().equals("Good")) {
                        inspFSRate5.setValue(1);
                    } else if (fireSafety.getRating5().equals("N/A")) {
                        inspFSRate5.setValue(2);
                    } else {
                        inspFSRate5.setValue(-1);
                    }

                     /* === FIRST AID === */

                    PlannedInspectionModel firstAid = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "First Aid");
                    if (firstAid != null) {
                        if (firstAid.getRisk1().equals("High")) {
                            inspFARisk1.setValue(0);
                        } else if (firstAid.getRisk1().equals("Med")) {
                            inspFARisk1.setValue(1);
                        } else if (firstAid.getRisk1().equals("Low")) {
                            inspFARisk1.setValue(2);
                        } else {
                            inspFARisk1.setValue(-1);
                        }

                        if (firstAid.getRating1().equals("NW")) {
                            inspFARate1.setValue(0);
                        } else if (firstAid.getRating1().equals("Good")) {
                            inspFARate1.setValue(1);
                        } else if (firstAid.getRating1().equals("N/A")) {
                            inspFARate1.setValue(2);
                        } else {
                            inspFARate1.setValue(-1);
                        }

                        if (firstAid.getRisk2().equals("High")) {
                            inspFARisk2.setValue(0);
                        } else if (firstAid.getRisk2().equals("Med")) {
                            inspFARisk2.setValue(1);
                        } else if (firstAid.getRisk2().equals("Low")) {
                            inspFARisk2.setValue(2);
                        } else {
                            inspFARisk2.setValue(-1);
                        }

                        if (firstAid.getRating2().equals("NW")) {
                            inspFARate2.setValue(0);
                        } else if (firstAid.getRating2().equals("Good")) {
                            inspFARate2.setValue(1);
                        } else if (firstAid.getRating2().equals("N/A")) {
                            inspFARate2.setValue(2);
                        } else {
                            inspFARate2.setValue(-1);
                        }

                        if (firstAid.getRisk3().equals("High")) {
                            inspFARisk3.setValue(0);
                        } else if (firstAid.getRisk3().equals("Med")) {
                            inspFARisk3.setValue(1);
                        } else if (firstAid.getRisk3().equals("Low")) {
                            inspFARisk3.setValue(2);
                        } else {
                            inspFARisk3.setValue(-1);
                        }

                        if (firstAid.getRating3().equals("NW")) {
                            inspFARate3.setValue(0);
                        } else if (firstAid.getRating3().equals("Good")) {
                            inspFARate3.setValue(1);
                        } else if (firstAid.getRating3().equals("N/A")) {
                            inspFARate3.setValue(2);
                        } else {
                            inspFARate3.setValue(-1);
                        }
                    }

                     /* === ENVIRONMENTAL === */

                    PlannedInspectionModel environment = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Environmental");
                    if (environment != null) {
                        if (environment.getRisk1().equals("High")) {
                            inspENRisk1.setValue(0);
                        } else if (environment.getRisk1().equals("Med")) {
                            inspENRisk1.setValue(1);
                        } else if (environment.getRisk1().equals("Low")) {
                            inspENRisk1.setValue(2);
                        } else {
                            inspENRisk1.setValue(-1);
                        }

                        if (environment.getRating1().equals("NW")) {
                            inspENRate1.setValue(0);
                        } else if (environment.getRating1().equals("Good")) {
                            inspENRate1.setValue(1);
                        } else if (environment.getRating1().equals("N/A")) {
                            inspENRate1.setValue(2);
                        } else {
                            inspENRate1.setValue(-1);
                        }

                        if (environment.getRisk2().equals("High")) {
                            inspENRisk2.setValue(0);
                        } else if (environment.getRisk2().equals("Med")) {
                            inspENRisk2.setValue(1);
                        } else if (environment.getRisk2().equals("Low")) {
                            inspENRisk2.setValue(2);
                        } else {
                            inspENRisk2.setValue(-1);
                        }

                        if (environment.getRating2().equals("NW")) {
                            inspENRate2.setValue(0);
                        } else if (environment.getRating2().equals("Good")) {
                            inspENRate2.setValue(1);
                        } else if (environment.getRating2().equals("N/A")) {
                            inspENRate2.setValue(2);
                        } else {
                            inspENRate2.setValue(-1);
                        }

                        if (environment.getRisk3().equals("High")) {
                            inspENRisk3.setValue(0);
                        } else if (environment.getRisk3().equals("Med")) {
                            inspENRisk3.setValue(1);
                        } else if (environment.getRisk3().equals("Low")) {
                            inspENRisk3.setValue(2);
                        } else {
                            inspENRisk3.setValue(-1);
                        }

                        if (environment.getRating3().equals("NW")) {
                            inspENRate3.setValue(0);
                        } else if (environment.getRating3().equals("Good")) {
                            inspENRate3.setValue(1);
                        } else if (environment.getRating3().equals("N/A")) {
                            inspENRate3.setValue(2);
                        } else {
                            inspENRate3.setValue(-1);
                        }

                        if (environment.getRisk4().equals("High")) {
                            inspENRisk4.setValue(0);
                        } else if (environment.getRisk4().equals("Med")) {
                            inspENRisk4.setValue(1);
                        } else if (environment.getRisk4().equals("Low")) {
                            inspENRisk4.setValue(2);
                        } else {
                            inspENRisk4.setValue(-1);
                        }

                        if (environment.getRating4().equals("NW")) {
                            inspENRate4.setValue(0);
                        } else if (environment.getRating4().equals("Good")) {
                            inspENRate4.setValue(1);
                        } else if (environment.getRating4().equals("N/A")) {
                            inspENRate4.setValue(2);
                        } else {
                            inspENRate4.setValue(-1);
                        }

                    }

                     /* === COMMUNICATION === */

                    PlannedInspectionModel communication = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Communication");
                    if (communication != null) {
                        if (communication.getRisk1().equals("High")) {
                            inspCOMRisk1.setValue(0);
                        } else if (communication.getRisk1().equals("Med")) {
                            inspCOMRisk1.setValue(1);
                        } else if (communication.getRisk1().equals("Low")) {
                            inspCOMRisk1.setValue(2);
                        } else {
                            inspCOMRisk1.setValue(-1);
                        }

                        if (communication.getRating1().equals("NW")) {
                            inspCOMRate1.setValue(0);
                        } else if (communication.getRating1().equals("Good")) {
                            inspCOMRate1.setValue(1);
                        } else if (communication.getRating1().equals("N/A")) {
                            inspCOMRate1.setValue(2);
                        } else {
                            inspCOMRate1.setValue(-1);
                        }

                        if (communication.getRisk1().equals("High")) {
                            inspCOMRisk2.setValue(0);
                        } else if (communication.getRisk1().equals("Med")) {
                            inspCOMRisk2.setValue(1);
                        } else if (communication.getRisk1().equals("Low")) {
                            inspCOMRisk2.setValue(2);
                        } else {
                            inspCOMRisk2.setValue(-1);
                        }

                        if (communication.getRating2().equals("NW")) {
                            inspCOMRate2.setValue(0);
                        } else if (communication.getRating2().equals("Good")) {
                            inspCOMRate2.setValue(1);
                        } else if (communication.getRating2().equals("N/A")) {
                            inspCOMRate2.setValue(2);
                        } else {
                            inspCOMRate2.setValue(-1);
                        }

                        if (communication.getRisk3().equals("High")) {
                            inspCOMRisk3.setValue(0);
                        } else if (communication.getRisk3().equals("Med")) {
                            inspCOMRisk3.setValue(1);
                        } else if (communication.getRisk3().equals("Low")) {
                            inspCOMRisk3.setValue(2);
                        } else {
                            inspCOMRisk3.setValue(-1);
                        }

                        if (communication.getRating3().equals("NW")) {
                            inspCOMRate3.setValue(0);
                        } else if (communication.getRating3().equals("Good")) {
                            inspCOMRate3.setValue(1);
                        } else if (communication.getRating3().equals("N/A")) {
                            inspCOMRate3.setValue(2);
                        } else {
                            inspCOMRate3.setValue(-1);
                        }

                        if (communication.getRisk4().equals("High")) {
                            inspCOMRisk4.setValue(0);
                        } else if (communication.getRisk4().equals("Med")) {
                            inspCOMRisk4.setValue(1);
                        } else if (communication.getRisk4().equals("Low")) {
                            inspCOMRisk4.setValue(2);
                        } else {
                            inspCOMRisk4.setValue(-1);
                        }

                        if (communication.getRating4().equals("NW")) {
                            inspCOMRate4.setValue(0);
                        } else if (communication.getRating4().equals("Good")) {
                            inspCOMRate4.setValue(1);
                        } else if (communication.getRating4().equals("N/A")) {
                            inspCOMRate4.setValue(2);
                        } else {
                            inspCOMRate4.setValue(-1);
                        }

                        if (communication.getRisk5().equals("High")) {
                            inspCOMRisk5.setValue(0);
                        } else if (communication.getRisk5().equals("Med")) {
                            inspCOMRisk5.setValue(1);
                        } else if (communication.getRisk5().equals("Low")) {
                            inspCOMRisk5.setValue(2);
                        } else {
                            inspCOMRisk5.setValue(-1);
                        }

                        if (communication.getRating5().equals("NW")) {
                            inspCOMRate5.setValue(0);
                        } else if (communication.getRating5().equals("Good")) {
                            inspCOMRate5.setValue(1);
                        } else if (communication.getRating5().equals("N/A")) {
                            inspCOMRate5.setValue(2);
                        } else {
                            inspCOMRate5.setValue(-1);
                        }

                        if (communication.getRisk6().equals("High")) {
                            inspCOMRisk6.setValue(0);
                        } else if (communication.getRisk6().equals("Med")) {
                            inspCOMRisk6.setValue(1);
                        } else if (communication.getRisk6().equals("Low")) {
                            inspCOMRisk6.setValue(2);
                        } else {
                            inspCOMRisk6.setValue(-1);
                        }

                        if (communication.getRating6().equals("NW")) {
                            inspCOMRate6.setValue(0);
                        } else if (communication.getRating6().equals("Good")) {
                            inspCOMRate6.setValue(1);
                        } else if (communication.getRating6().equals("N/A")) {
                            inspCOMRate6.setValue(2);
                        } else {
                            inspCOMRate6.setValue(-1);
                        }

                        if (communication.getRisk7().equals("High")) {
                            inspCOMRisk7.setValue(0);
                        } else if (communication.getRisk7().equals("Med")) {
                            inspCOMRisk7.setValue(1);
                        } else if (communication.getRisk7().equals("Low")) {
                            inspCOMRisk7.setValue(2);
                        } else {
                            inspCOMRisk7.setValue(-1);
                        }

                        if (communication.getRating7().equals("NW")) {
                            inspCOMRate7.setValue(0);
                        } else if (communication.getRating7().equals("Good")) {
                            inspCOMRate7.setValue(1);
                        } else if (communication.getRating7().equals("N/A")) {
                            inspCOMRate7.setValue(2);
                        } else {
                            inspCOMRate7.setValue(-1);
                        }

                    }

                     /* === FALL PROTECTION === */

                    PlannedInspectionModel fallProtect = ds.getLastDataInspection(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(), "Fall Protection");
                    if (fallProtect != null) {
                        if (fallProtect.getRisk1().equals("High")) {
                            inspFPRisk1.setValue(0);
                        } else if (fallProtect.getRisk1().equals("Med")) {
                            inspFPRisk1.setValue(1);
                        } else if (fallProtect.getRisk1().equals("Low")) {
                            inspFPRisk1.setValue(2);
                        } else {
                            inspFPRisk1.setValue(-1);
                        }

                        if (fallProtect.getRating1().equals("NW")) {
                            inspFPRate1.setValue(0);
                        } else if (fallProtect.getRating1().equals("Good")) {
                            inspFPRate1.setValue(1);
                        } else if (fallProtect.getRating1().equals("N/A")) {
                            inspFPRate1.setValue(2);
                        } else {
                            inspFPRate1.setValue(-1);
                        }

                        if (fallProtect.getRisk2().equals("High")) {
                            inspFPRisk2.setValue(0);
                        } else if (fallProtect.getRisk2().equals("Med")) {
                            inspFPRisk2.setValue(1);
                        } else if (fallProtect.getRisk2().equals("Low")) {
                            inspFPRisk2.setValue(2);
                        } else {
                            inspFPRisk2.setValue(-1);
                        }

                        if (fallProtect.getRating2().equals("NW")) {
                            inspFPRate2.setValue(0);
                        } else if (fallProtect.getRating2().equals("Good")) {
                            inspFPRate2.setValue(1);
                        } else if (fallProtect.getRating2().equals("N/A")) {
                            inspFPRate2.setValue(2);
                        } else {
                            inspFPRate2.setValue(-1);
                        }

                        if (fallProtect.getRisk3().equals("High")) {
                            inspFPRisk3.setValue(0);
                        } else if (fallProtect.getRisk3().equals("Med")) {
                            inspFPRisk3.setValue(1);
                        } else if (fallProtect.getRisk3().equals("Low")) {
                            inspFPRisk3.setValue(2);
                        } else {
                            inspFPRisk3.setValue(-1);
                        }

                        if (fallProtect.getRating3().equals("NW")) {
                            inspFPRate3.setValue(0);
                        } else if (fallProtect.getRating3().equals("Good")) {
                            inspFPRate3.setValue(1);
                        } else if (fallProtect.getRating3().equals("N/A")) {
                            inspFPRate3.setValue(2);
                        } else {
                            inspFPRate3.setValue(-1);
                        }

                        if (fallProtect.getRisk4().equals("High")) {
                            inspFPRisk4.setValue(0);
                        } else if (fallProtect.getRisk4().equals("Med")) {
                            inspFPRisk4.setValue(1);
                        } else if (fallProtect.getRisk4().equals("Low")) {
                            inspFPRisk4.setValue(2);
                        } else {
                            inspFPRisk4.setValue(-1);
                        }

                        if (fallProtect.getRating4().equals("NW")) {
                            inspFPRate4.setValue(0);
                        } else if (fallProtect.getRating4().equals("Good")) {
                            inspFPRate4.setValue(1);
                        } else if (fallProtect.getRating4().equals("N/A")) {
                            inspFPRate4.setValue(2);
                        } else {
                            inspFPRate4.setValue(-1);
                        }

                        if (fallProtect.getRisk5().equals("High")) {
                            inspFPRisk5.setValue(0);
                        } else if (fallProtect.getRisk5().equals("Med")) {
                            inspFPRisk5.setValue(1);
                        } else if (fallProtect.getRisk5().equals("Low")) {
                            inspFPRisk5.setValue(2);
                        } else {
                            inspFPRisk5.setValue(-1);
                        }

                        if (fallProtect.getRating5().equals("NW")) {
                            inspFPRate5.setValue(0);
                        } else if (fallProtect.getRating5().equals("Good")) {
                            inspFPRate5.setValue(1);
                        } else if (fallProtect.getRating5().equals("N/A")) {
                            inspFPRate5.setValue(2);
                        } else {
                            inspFPRate5.setValue(-1);
                        }

                        if (fallProtect.getRisk6().equals("High")) {
                            inspFPRisk6.setValue(0);
                        } else if (fallProtect.getRisk6().equals("Med")) {
                            inspFPRisk6.setValue(1);
                        } else if (fallProtect.getRisk6().equals("Low")) {
                            inspFPRisk6.setValue(2);
                        } else {
                            inspFPRisk6.setValue(-1);
                        }

                        if (fallProtect.getRating1().equals("NW")) {
                            inspFPRate6.setValue(0);
                        } else if (fallProtect.getRating6().equals("Good")) {
                            inspFPRate6.setValue(1);
                        } else if (fallProtect.getRating6().equals("N/A")) {
                            inspFPRate6.setValue(2);
                        } else {
                            inspFPRate6.setValue(-1);
                        }

                    }
                }

                PlannedSignModel signSPVModel = ds.getAllPlannedSign(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(),
                        "Supervisor");
                if (signSPVModel != null) {
                    siteSVET.setText(signSPVModel.getName());
                    dateSiteET.setText(signSPVModel.getDate());
                }

                PlannedSignModel signManagerModel = ds.getAllPlannedSign(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(),
                        "Manager");
                if (signManagerModel != null) {
                    deptManagerET.setText(signManagerModel.getName());
                    dateDeptET.setText(signManagerModel.getDate());
                }

                PlannedSignModel signSafetyModel = ds.getAllPlannedSign(model.getInspector(), model.getAreaOwner(), model.getLocation(), model.getDate(),
                        "Safety");
                if (signSafetyModel != null) {
                    deptSafeET.setText(signSafetyModel.getName());
                    dateSafeET.setText(signSafetyModel.getDate());
                }


                ds.close();
            }
        }
    }
    /* Load Finding */

    /* Save data Main Inspection */
    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        /* === MAIN INSPECTOR === */

        PlannedMainModel mainModel = new PlannedMainModel();
        mainModel.setDate(dateET.getText().toString());
        mainModel.setInspector(inspectorET.getText().toString());
        String inspectorID = ds.getManpowerIdData(inspectorET.getText().toString());
        mainModel.setInspectorID(inspectorID);
        mainModel.setAreaOwner(areaOwnerET.getText().toString());
        mainModel.setLocation(locationET.getText().toString());

        long insertedId = ds.insertPlannedMain(mainModel);
        for (int i = 0; i < plannedFindingsData.size(); i++) {
            ds.insertPlannedFindings(plannedFindingsData.get(i), insertedId);
        }

        /* Save data inspection inspection */

        /* === WORK AREA === */

        PlannedInspectionModel workArea = new PlannedInspectionModel();
        workArea.setDate(dateET.getText().toString());
        workArea.setInspector(inspectorET.getText().toString());
        workArea.setAreaOwner(areaOwnerET.getText().toString());
        workArea.setLocation(locationET.getText().toString());
        workArea.setCode("Work Area");

        if (inspWaRisk1.getValue() == 0) {
            workArea.setRisk1("High");
        } else if (inspWaRisk1.getValue() == 1) {
            workArea.setRisk1("Med");
        } else if (inspWaRisk1.getValue() == 2) {
            workArea.setRisk1("Low");
        } else {
            workArea.setRisk1("");
        }

        if (inspWaRate1.getValue() == 0) {
            workArea.setRating1("NW");
        } else if (inspWaRate1.getValue() == 1) {
            workArea.setRating1("Good");
        } else if (inspWaRate1.getValue() == 2) {
            workArea.setRating1("N/A");
        } else {
            workArea.setRating1("");
        }

        if (inspWaRisk2.getValue() == 0) {
            workArea.setRisk2("High");
        } else if (inspWaRisk2.getValue() == 1) {
            workArea.setRisk2("Med");
        } else if (inspWaRisk2.getValue() == 2) {
            workArea.setRisk2("Low");
        } else {
            workArea.setRisk2("");
        }

        if (inspWaRate2.getValue() == 0) {
            workArea.setRating2("NW");
        } else if (inspWaRate2.getValue() == 1) {
            workArea.setRating2("Good");
        } else if (inspWaRate2.getValue() == 2) {
            workArea.setRating2("N/A");
        } else {
            workArea.setRating2("");
        }

        if (inspWaRisk3.getValue() == 0) {
            workArea.setRisk3("High");
        } else if (inspWaRisk3.getValue() == 1) {
            workArea.setRisk3("Med");
        } else if (inspWaRisk3.getValue() == 2) {
            workArea.setRisk3("Low");
        } else {
            workArea.setRisk3("");
        }

        if (inspWaRate3.getValue() == 0) {
            workArea.setRating3("NW");
        } else if (inspWaRate3.getValue() == 1) {
            workArea.setRating3("Good");
        } else if (inspWaRate3.getValue() == 2) {
            workArea.setRating3("N/A");
        } else {
            workArea.setRating3("");
        }

        if (inspWaRisk4.getValue() == 0) {
            workArea.setRisk4("High");
        } else if (inspWaRisk4.getValue() == 1) {
            workArea.setRisk4("Med");
        } else if (inspWaRisk4.getValue() == 2) {
            workArea.setRisk4("Low");
        } else {
            workArea.setRisk4("");
        }

        if (inspWaRate4.getValue() == 0) {
            workArea.setRating4("NW");
        } else if (inspWaRate4.getValue() == 1) {
            workArea.setRating4("Good");
        } else if (inspWaRate4.getValue() == 2) {
            workArea.setRating4("N/A");
        } else {
            workArea.setRating4("");
        }

        if (inspWaRisk5.getValue() == 0) {
            workArea.setRisk5("High");
        } else if (inspWaRisk5.getValue() == 1) {
            workArea.setRisk5("Med");
        } else if (inspWaRisk5.getValue() == 2) {
            workArea.setRisk5("Low");
        } else {
            workArea.setRisk5("");
        }

        if (inspWaRate5.getValue() == 0) {
            workArea.setRating5("NW");
        } else if (inspWaRate5.getValue() == 1) {
            workArea.setRating5("Good");
        } else if (inspWaRate5.getValue() == 2) {
            workArea.setRating5("N/A");
        } else {
            workArea.setRating5("");
        }

        if (inspWaRisk6.getValue() == 0) {
            workArea.setRisk6("High");
        } else if (inspWaRisk6.getValue() == 1) {
            workArea.setRisk6("Med");
        } else if (inspWaRisk6.getValue() == 2) {
            workArea.setRisk6("Low");
        } else {
            workArea.setRisk6("");
        }

        if (inspWaRate6.getValue() == 0) {
            workArea.setRating6("NW");
        } else if (inspWaRate6.getValue() == 1) {
            workArea.setRating6("Good");
        } else if (inspWaRate6.getValue() == 2) {
            workArea.setRating6("N/A");
        } else {
            workArea.setRating6("");
        }

        ds.insertPlannedInspection(workArea);

        /* === STRUCTURE === */

        PlannedInspectionModel structureFloor = new PlannedInspectionModel();
        structureFloor.setDate(dateET.getText().toString());
        structureFloor.setInspector(inspectorET.getText().toString());
        structureFloor.setAreaOwner(areaOwnerET.getText().toString());
        structureFloor.setLocation(locationET.getText().toString());
        structureFloor.setCode("Structure");

        if (inspStRisk1.getValue() == 0) {
            structureFloor.setRisk1("High");
        } else if (inspStRisk1.getValue() == 1) {
            structureFloor.setRisk1("Med");
        } else if (inspStRisk1.getValue() == 2) {
            structureFloor.setRisk1("Low");
        } else {
            structureFloor.setRisk1("");
        }

        if (inspStRate1.getValue() == 0) {
            structureFloor.setRating1("NW");
        } else if (inspStRate1.getValue() == 1) {
            structureFloor.setRating1("Good");
        } else if (inspStRate1.getValue() == 2) {
            structureFloor.setRating1("N/A");
        } else {
            structureFloor.setRating1("");
        }

        if (inspStRisk2.getValue() == 0) {
            structureFloor.setRisk2("High");
        } else if (inspStRisk2.getValue() == 1) {
            structureFloor.setRisk2("Med");
        } else if (inspStRisk2.getValue() == 2) {
            structureFloor.setRisk2("Low");
        } else {
            structureFloor.setRisk2("");
        }

        if (inspStRate2.getValue() == 0) {
            structureFloor.setRating2("NW");
        } else if (inspStRate2.getValue() == 1) {
            structureFloor.setRating2("Good");
        } else if (inspStRate2.getValue() == 2) {
            structureFloor.setRating2("N/A");
        } else {
            structureFloor.setRating2("");
        }

        if (inspStRisk3.getValue() == 0) {
            structureFloor.setRisk3("High");
        } else if (inspStRisk3.getValue() == 1) {
            structureFloor.setRisk3("Med");
        } else if (inspStRisk3.getValue() == 2) {
            structureFloor.setRisk3("Low");
        } else {
            structureFloor.setRisk3("");
        }

        if (inspStRate3.getValue() == 0) {
            structureFloor.setRating3("NW");
        } else if (inspStRate3.getValue() == 1) {
            structureFloor.setRating3("Good");
        } else if (inspStRate3.getValue() == 2) {
            structureFloor.setRating3("N/A");
        } else {
            structureFloor.setRating3("");
        }

        if (inspStRisk4.getValue() == 0) {
            structureFloor.setRisk4("High");
        } else if (inspStRisk4.getValue() == 1) {
            structureFloor.setRisk4("Med");
        } else if (inspStRisk4.getValue() == 2) {
            structureFloor.setRisk4("Low");
        } else {
            structureFloor.setRisk4("");
        }

        if (inspStRate4.getValue() == 0) {
            structureFloor.setRating4("NW");
        } else if (inspStRate4.getValue() == 1) {
            structureFloor.setRating4("Good");
        } else if (inspStRate4.getValue() == 2) {
            structureFloor.setRating4("N/A");
        } else {
            structureFloor.setRating4("");
        }

        if (inspStRisk5.getValue() == 0) {
            structureFloor.setRisk5("High");
        } else if (inspStRisk5.getValue() == 1) {
            structureFloor.setRisk5("Med");
        } else if (inspStRisk5.getValue() == 2) {
            structureFloor.setRisk5("Low");
        } else {
            structureFloor.setRisk5("");
        }

        if (inspStRate5.getValue() == 0) {
            structureFloor.setRating5("NW");
        } else if (inspStRate5.getValue() == 1) {
            structureFloor.setRating5("Good");
        } else if (inspStRate5.getValue() == 2) {
            structureFloor.setRating5("N/A");
        } else {
            structureFloor.setRating5("");
        }

        ds.insertPlannedInspection(structureFloor);

        /* === MACHINERY TOOLS === */

        PlannedInspectionModel machineryTools = new PlannedInspectionModel();
        machineryTools.setDate(dateET.getText().toString());
        machineryTools.setInspector(inspectorET.getText().toString());
        machineryTools.setAreaOwner(areaOwnerET.getText().toString());
        machineryTools.setLocation(locationET.getText().toString());
        machineryTools.setCode("Machinery");

        if (inspMtRisk1.getValue() == 0) {
            machineryTools.setRisk1("High");
        } else if (inspMtRisk1.getValue() == 1) {
            machineryTools.setRisk1("Med");
        } else if (inspMtRisk1.getValue() == 2) {
            machineryTools.setRisk1("Low");
        } else {
            machineryTools.setRisk1("");
        }

        if (inspMtRate1.getValue() == 0) {
            machineryTools.setRating1("NW");
        } else if (inspMtRate1.getValue() == 1) {
            machineryTools.setRating1("Good");
        } else if (inspMtRate1.getValue() == 2) {
            machineryTools.setRating1("N/A");
        } else {
            machineryTools.setRating1("");
        }

        if (inspMtRisk2.getValue() == 0) {
            machineryTools.setRisk2("High");
        } else if (inspMtRisk2.getValue() == 1) {
            machineryTools.setRisk2("Med");
        } else if (inspMtRisk2.getValue() == 2) {
            machineryTools.setRisk2("Low");
        } else {
            machineryTools.setRisk2("");
        }

        if (inspMtRate2.getValue() == 0) {
            machineryTools.setRating2("NW");
        } else if (inspMtRate2.getValue() == 1) {
            machineryTools.setRating2("Good");
        } else if (inspMtRate2.getValue() == 2) {
            machineryTools.setRating2("N/A");
        } else {
            machineryTools.setRating2("");
        }

        if (inspMtRisk3.getValue() == 0) {
            machineryTools.setRisk3("High");
        } else if (inspMtRisk3.getValue() == 1) {
            machineryTools.setRisk3("Med");
        } else if (inspMtRisk3.getValue() == 2) {
            machineryTools.setRisk3("Low");
        } else {
            machineryTools.setRisk3("");
        }

        if (inspMtRate3.getValue() == 0) {
            machineryTools.setRating3("NW");
        } else if (inspMtRate1.getValue() == 1) {
            machineryTools.setRating3("Good");
        } else if (inspMtRate1.getValue() == 2) {
            machineryTools.setRating3("N/A");
        } else {
            machineryTools.setRating3("");
        }

        if (inspMtRisk4.getValue() == 0) {
            machineryTools.setRisk4("High");
        } else if (inspMtRisk4.getValue() == 1) {
            machineryTools.setRisk4("Med");
        } else if (inspMtRisk4.getValue() == 2) {
            machineryTools.setRisk4("Low");
        } else {
            machineryTools.setRisk4("");
        }

        if (inspMtRate4.getValue() == 0) {
            machineryTools.setRating4("NW");
        } else if (inspMtRate4.getValue() == 1) {
            machineryTools.setRating4("Good");
        } else if (inspMtRate4.getValue() == 2) {
            machineryTools.setRating4("N/A");
        } else {
            machineryTools.setRating4("");
        }

        if (inspMtRisk5.getValue() == 0) {
            machineryTools.setRisk5("High");
        } else if (inspMtRisk5.getValue() == 1) {
            machineryTools.setRisk5("Med");
        } else if (inspMtRisk5.getValue() == 2) {
            machineryTools.setRisk5("Low");
        } else {
            machineryTools.setRisk5("");
        }

        if (inspMtRate5.getValue() == 0) {
            machineryTools.setRating5("NW");
        } else if (inspMtRate5.getValue() == 1) {
            machineryTools.setRating5("Good");
        } else if (inspMtRate5.getValue() == 2) {
            machineryTools.setRating5("N/A");
        } else {
            machineryTools.setRating5("");
        }

        if (inspMtRisk6.getValue() == 0) {
            machineryTools.setRisk6("High");
        } else if (inspMtRisk6.getValue() == 1) {
            machineryTools.setRisk6("Med");
        } else if (inspMtRisk6.getValue() == 2) {
            machineryTools.setRisk6("Low");
        } else {
            machineryTools.setRisk6("");
        }

        if (inspMtRate6.getValue() == 0) {
            machineryTools.setRating6("NW");
        } else if (inspMtRate6.getValue() == 1) {
            machineryTools.setRating6("Good");
        } else if (inspMtRate6.getValue() == 2) {
            machineryTools.setRating6("N/A");
        } else {
            machineryTools.setRating6("");
        }

        if (inspMtRisk7.getValue() == 0) {
            machineryTools.setRisk7("High");
        } else if (inspMtRisk7.getValue() == 1) {
            machineryTools.setRisk7("Med");
        } else if (inspMtRisk7.getValue() == 2) {
            machineryTools.setRisk7("Low");
        } else {
            machineryTools.setRisk7("");
        }

        if (inspMtRate7.getValue() == 0) {
            machineryTools.setRating7("NW");
        } else if (inspMtRate7.getValue() == 1) {
            machineryTools.setRating7("Good");
        } else if (inspMtRate7.getValue() == 2) {
            machineryTools.setRating7("N/A");
        } else {
            machineryTools.setRating7("");
        }

        ds.insertPlannedInspection(machineryTools);


        /* === ELECTRICAL === */

        PlannedInspectionModel electrical = new PlannedInspectionModel();
        electrical.setDate(dateET.getText().toString());
        electrical.setInspector(inspectorET.getText().toString());
        electrical.setAreaOwner(areaOwnerET.getText().toString());
        electrical.setLocation(locationET.getText().toString());
        electrical.setCode("Electrical");

        if (inspElRisk1.getValue() == 0) {
            electrical.setRisk1("High");
        } else if (inspElRisk1.getValue() == 1) {
            electrical.setRisk1("Med");
        } else if (inspElRisk1.getValue() == 2) {
            electrical.setRisk1("Low");
        } else {
            electrical.setRisk1("");
        }

        if (inspElRate1.getValue() == 0) {
            electrical.setRating1("NW");
        } else if (inspElRate1.getValue() == 1) {
            electrical.setRating1("Good");
        } else if (inspElRate1.getValue() == 2) {
            electrical.setRating1("N/A");
        } else {
            electrical.setRating1("");
        }

        if (inspElRisk2.getValue() == 0) {
            electrical.setRisk2("High");
        } else if (inspElRisk2.getValue() == 1) {
            electrical.setRisk2("Med");
        } else if (inspElRisk2.getValue() == 2) {
            electrical.setRisk2("Low");
        } else {
            electrical.setRisk2("");
        }

        if (inspElRate2.getValue() == 0) {
            electrical.setRating2("NW");
        } else if (inspElRate2.getValue() == 1) {
            electrical.setRating2("Good");
        } else if (inspElRate2.getValue() == 2) {
            electrical.setRating2("N/A");
        } else {
            electrical.setRating2("");
        }

        if (inspElRisk3.getValue() == 0) {
            electrical.setRisk3("High");
        } else if (inspElRisk3.getValue() == 1) {
            electrical.setRisk3("Med");
        } else if (inspElRisk3.getValue() == 2) {
            electrical.setRisk3("Low");
        } else {
            electrical.setRisk3("");
        }

        if (inspElRate3.getValue() == 0) {
            electrical.setRating3("NW");
        } else if (inspElRate3.getValue() == 1) {
            electrical.setRating3("Good");
        } else if (inspElRate3.getValue() == 2) {
            electrical.setRating3("N/A");
        } else {
            electrical.setRating3("");
        }

        if (inspElRisk4.getValue() == 0) {
            electrical.setRisk4("High");
        } else if (inspElRisk4.getValue() == 1) {
            electrical.setRisk4("Med");
        } else if (inspElRisk4.getValue() == 2) {
            electrical.setRisk4("Low");
        } else {
            electrical.setRisk4("");
        }

        if (inspElRate4.getValue() == 0) {
            electrical.setRating4("NW");
        } else if (inspElRate4.getValue() == 1) {
            electrical.setRating4("Good");
        } else if (inspElRate4.getValue() == 2) {
            electrical.setRating4("N/A");
        } else {
            electrical.setRating4("");
        }

        if (inspElRisk5.getValue() == 0) {
            electrical.setRisk5("High");
        } else if (inspElRisk5.getValue() == 1) {
            electrical.setRisk5("Med");
        } else if (inspElRisk5.getValue() == 2) {
            electrical.setRisk5("Low");
        } else {
            electrical.setRisk5("");
        }

        if (inspElRate5.getValue() == 0) {
            electrical.setRating5("NW");
        } else if (inspElRate5.getValue() == 1) {
            electrical.setRating5("Good");
        } else if (inspElRate5.getValue() == 2) {
            electrical.setRating5("N/A");
        } else {
            electrical.setRating5("");
        }

        ds.insertPlannedInspection(electrical);

        /* === PERSONAL === */

        PlannedInspectionModel personel = new PlannedInspectionModel();
        personel.setDate(dateET.getText().toString());
        personel.setInspector(inspectorET.getText().toString());
        personel.setAreaOwner(areaOwnerET.getText().toString());
        personel.setLocation(locationET.getText().toString());
        personel.setCode("Personel");

        if (inspPeRisk1.getValue() == 0) {
            personel.setRisk1("High");
        } else if (inspPeRisk1.getValue() == 1) {
            personel.setRisk1("Med");
        } else if (inspPeRisk1.getValue() == 2) {
            personel.setRisk1("Low");
        } else {
            personel.setRisk1("");
        }

        if (inspPeRate1.getValue() == 0) {
            personel.setRating1("NW");
        } else if (inspPeRate1.getValue() == 1) {
            personel.setRating1("Good");
        } else if (inspPeRate1.getValue() == 2) {
            personel.setRating1("N/A");
        } else {
            personel.setRating1("");
        }

        if (inspPeRisk2.getValue() == 0) {
            personel.setRisk2("High");
        } else if (inspPeRisk2.getValue() == 1) {
            personel.setRisk2("Med");
        } else if (inspPeRisk2.getValue() == 2) {
            personel.setRisk2("Low");
        } else {
            personel.setRisk2("");
        }

        if (inspPeRate2.getValue() == 0) {
            personel.setRating2("NW");
        } else if (inspPeRate2.getValue() == 1) {
            personel.setRating2("Good");
        } else if (inspPeRate2.getValue() == 2) {
            personel.setRating2("N/A");
        } else {
            personel.setRating2("");
        }

        if (inspPeRisk3.getValue() == 0) {
            personel.setRisk3("High");
        } else if (inspPeRisk3.getValue() == 1) {
            personel.setRisk3("Med");
        } else if (inspPeRisk3.getValue() == 2) {
            personel.setRisk3("Low");
        } else {
            personel.setRisk3("");
        }

        if (inspPeRate3.getValue() == 0) {
            personel.setRating3("NW");
        } else if (inspPeRate3.getValue() == 1) {
            personel.setRating3("Good");
        } else if (inspPeRate3.getValue() == 2) {
            personel.setRating3("N/A");
        } else {
            personel.setRating3("");
        }

        if (inspPeRisk4.getValue() == 0) {
            personel.setRisk4("High");
        } else if (inspPeRisk4.getValue() == 1) {
            personel.setRisk4("Med");
        } else if (inspPeRisk4.getValue() == 2) {
            personel.setRisk4("Low");
        } else {
            personel.setRisk4("");
        }

        if (inspPeRate4.getValue() == 0) {
            personel.setRating4("NW");
        } else if (inspPeRate4.getValue() == 1) {
            personel.setRating4("Good");
        } else if (inspPeRate4.getValue() == 2) {
            personel.setRating4("N/A");
        } else {
            personel.setRating4("");
        }

        if (inspPeRisk5.getValue() == 0) {
            personel.setRisk5("High");
        } else if (inspPeRisk5.getValue() == 1) {
            personel.setRisk5("Med");
        } else if (inspPeRisk5.getValue() == 2) {
            personel.setRisk5("Low");
        } else {
            personel.setRisk5("");
        }

        if (inspPeRate5.getValue() == 0) {
            personel.setRating5("NW");
        } else if (inspPeRate5.getValue() == 1) {
            personel.setRating5("Good");
        } else if (inspPeRate5.getValue() == 2) {
            personel.setRating5("N/A");
        } else {
            personel.setRating5("");
        }

        if (inspPeRisk6.getValue() == 0) {
            personel.setRisk6("High");
        } else if (inspPeRisk6.getValue() == 1) {
            personel.setRisk6("Med");
        } else if (inspPeRisk6.getValue() == 2) {
            personel.setRisk6("Low");
        } else {
            personel.setRisk6("");
        }

        if (inspPeRate6.getValue() == 0) {
            personel.setRating6("NW");
        } else if (inspPeRate6.getValue() == 1) {
            personel.setRating6("Good");
        } else if (inspPeRate6.getValue() == 2) {
            personel.setRating6("N/A");
        } else {
            personel.setRating6("");
        }

        if (inspPeRisk7.getValue() == 0) {
            personel.setRisk7("High");
        } else if (inspPeRisk7.getValue() == 1) {
            personel.setRisk7("Med");
        } else if (inspPeRisk7.getValue() == 2) {
            personel.setRisk7("Low");
        } else {
            personel.setRisk7("");
        }

        if (inspPeRate7.getValue() == 0) {
            personel.setRating7("NW");
        } else if (inspPeRate7.getValue() == 1) {
            personel.setRating7("Good");
        } else if (inspPeRate7.getValue() == 2) {
            personel.setRating7("N/A");
        } else {
            personel.setRating7("");
        }

        if (inspPeRisk8.getValue() == 0) {
            personel.setRisk8("High");
        } else if (inspPeRisk8.getValue() == 1) {
            personel.setRisk8("Med");
        } else if (inspPeRisk8.getValue() == 2) {
            personel.setRisk8("Low");
        } else {
            personel.setRisk8("");
        }

        if (inspPeRate8.getValue() == 0) {
            personel.setRating8("NW");
        } else if (inspPeRate8.getValue() == 1) {
            personel.setRating8("Good");
        } else if (inspPeRate8.getValue() == 2) {
            personel.setRating8("N/A");
        } else {
            personel.setRating8("");
        }

        ds.insertPlannedInspection(personel);

        /* === FIRE SAFETY === */

        PlannedInspectionModel fireSafety = new PlannedInspectionModel();
        fireSafety.setDate(dateET.getText().toString());
        fireSafety.setInspector(inspectorET.getText().toString());
        fireSafety.setAreaOwner(areaOwnerET.getText().toString());
        fireSafety.setLocation(locationET.getText().toString());
        fireSafety.setCode("Fire Safety");

        if (inspFSRisk1.getValue() == 0) {
            fireSafety.setRisk1("High");
        } else if (inspFSRisk1.getValue() == 1) {
            fireSafety.setRisk1("Med");
        } else if (inspFSRisk1.getValue() == 2) {
            fireSafety.setRisk1("Low");
        } else {
            fireSafety.setRisk1("");
        }

        if (inspFSRate1.getValue() == 0) {
            fireSafety.setRating1("NW");
        } else if (inspFSRate1.getValue() == 1) {
            fireSafety.setRating1("Good");
        } else if (inspFSRate1.getValue() == 2) {
            fireSafety.setRating1("N/A");
        } else {
            fireSafety.setRating1("");
        }

        if (inspFSRisk2.getValue() == 0) {
            fireSafety.setRisk2("High");
        } else if (inspFSRisk2.getValue() == 1) {
            fireSafety.setRisk2("Med");
        } else if (inspFSRisk2.getValue() == 2) {
            fireSafety.setRisk2("Low");
        } else {
            fireSafety.setRisk2("");
        }

        if (inspFSRate2.getValue() == 0) {
            fireSafety.setRating2("NW");
        } else if (inspFSRate2.getValue() == 1) {
            fireSafety.setRating2("Good");
        } else if (inspFSRate2.getValue() == 2) {
            fireSafety.setRating2("N/A");
        } else {
            fireSafety.setRating2("");
        }

        if (inspFSRisk3.getValue() == 0) {
            fireSafety.setRisk3("High");
        } else if (inspFSRisk3.getValue() == 1) {
            fireSafety.setRisk3("Med");
        } else if (inspFSRisk3.getValue() == 2) {
            fireSafety.setRisk3("Low");
        } else {
            fireSafety.setRisk3("");
        }

        if (inspFSRate3.getValue() == 0) {
            fireSafety.setRating3("NW");
        } else if (inspFSRate3.getValue() == 1) {
            fireSafety.setRating3("Good");
        } else if (inspFSRate3.getValue() == 2) {
            fireSafety.setRating3("N/A");
        } else {
            fireSafety.setRating3("");
        }

        if (inspFSRisk4.getValue() == 0) {
            fireSafety.setRisk4("High");
        } else if (inspFSRisk4.getValue() == 1) {
            fireSafety.setRisk4("Med");
        } else if (inspFSRisk4.getValue() == 2) {
            fireSafety.setRisk4("Low");
        } else {
            fireSafety.setRisk4("");
        }

        if (inspFSRate4.getValue() == 0) {
            fireSafety.setRating4("NW");
        } else if (inspFSRate4.getValue() == 1) {
            fireSafety.setRating4("Good");
        } else if (inspFSRate4.getValue() == 2) {
            fireSafety.setRating4("N/A");
        } else {
            fireSafety.setRating4("");
        }

        if (inspFSRisk5.getValue() == 0) {
            fireSafety.setRisk5("High");
        } else if (inspFSRisk5.getValue() == 1) {
            fireSafety.setRisk5("Med");
        } else if (inspFSRisk5.getValue() == 2) {
            fireSafety.setRisk5("Low");
        } else {
            fireSafety.setRisk5("");
        }

        if (inspFSRate5.getValue() == 0) {
            fireSafety.setRating5("NW");
        } else if (inspFSRate5.getValue() == 1) {
            fireSafety.setRating5("Good");
        } else if (inspFSRate5.getValue() == 2) {
            fireSafety.setRating5("N/A");
        } else {
            fireSafety.setRating5("");
        }

        ds.insertPlannedInspection(fireSafety);

        /* === FIRST AID === */

        PlannedInspectionModel firstAid = new PlannedInspectionModel();
        firstAid.setDate(dateET.getText().toString());
        firstAid.setInspector(inspectorET.getText().toString());
        firstAid.setAreaOwner(areaOwnerET.getText().toString());
        firstAid.setLocation(locationET.getText().toString());
        firstAid.setCode("First Aid");

        if (inspFARisk1.getValue() == 0) {
            firstAid.setRisk1("High");
        } else if (inspFARisk1.getValue() == 1) {
            firstAid.setRisk1("Med");
        } else if (inspFARisk1.getValue() == 2) {
            firstAid.setRisk1("Low");
        } else {
            firstAid.setRisk1("");
        }

        if (inspFARate1.getValue() == 0) {
            firstAid.setRating1("NW");
        } else if (inspFARate1.getValue() == 1) {
            firstAid.setRating1("Good");
        } else if (inspFARate1.getValue() == 2) {
            firstAid.setRating1("N/A");
        } else {
            firstAid.setRating1("");
        }

        if (inspFARisk2.getValue() == 0) {
            firstAid.setRisk2("High");
        } else if (inspFARisk2.getValue() == 1) {
            firstAid.setRisk2("Med");
        } else if (inspFARisk2.getValue() == 2) {
            firstAid.setRisk2("Low");
        } else {
            firstAid.setRisk2("");
        }

        if (inspFARate2.getValue() == 0) {
            fireSafety.setRating2("NW");
        } else if (inspFARate2.getValue() == 1) {
            firstAid.setRating2("Good");
        } else if (inspFARate2.getValue() == 2) {
            firstAid.setRating2("N/A");
        } else {
            firstAid.setRating2("");
        }

        if (inspFARisk3.getValue() == 0) {
            firstAid.setRisk3("High");
        } else if (inspFARisk3.getValue() == 1) {
            firstAid.setRisk3("Med");
        } else if (inspFARisk3.getValue() == 2) {
            firstAid.setRisk3("Low");
        } else {
            firstAid.setRisk3("");
        }

        if (inspFARate3.getValue() == 0) {
            fireSafety.setRating3("NW");
        } else if (inspFARate3.getValue() == 1) {
            firstAid.setRating3("Good");
        } else if (inspFARate3.getValue() == 2) {
            firstAid.setRating3("N/A");
        } else {
            firstAid.setRating3("");
        }

        ds.insertPlannedInspection(firstAid);

        /* === ENVIRONMENTAL === */

        PlannedInspectionModel environment = new PlannedInspectionModel();
        environment.setDate(dateET.getText().toString());
        environment.setInspector(inspectorET.getText().toString());
        environment.setAreaOwner(areaOwnerET.getText().toString());
        environment.setLocation(locationET.getText().toString());
        environment.setCode("Environmental");

        if (inspENRisk1.getValue() == 0) {
            environment.setRisk1("High");
        } else if (inspENRisk1.getValue() == 1) {
            environment.setRisk1("Med");
        } else if (inspENRisk1.getValue() == 2) {
            environment.setRisk1("Low");
        } else {
            environment.setRisk1("");
        }

        if (inspENRate1.getValue() == 0) {
            environment.setRating1("NW");
        } else if (inspENRate1.getValue() == 1) {
            environment.setRating1("Good");
        } else if (inspENRate1.getValue() == 2) {
            environment.setRating1("N/A");
        } else {
            environment.setRating1("");
        }

        if (inspENRisk2.getValue() == 0) {
            environment.setRisk2("High");
        } else if (inspENRisk2.getValue() == 1) {
            environment.setRisk2("Med");
        } else if (inspENRisk2.getValue() == 2) {
            environment.setRisk2("Low");
        } else {
            environment.setRisk2("");
        }

        if (inspENRate2.getValue() == 0) {
            environment.setRating2("NW");
        } else if (inspENRate2.getValue() == 1) {
            environment.setRating2("Good");
        } else if (inspENRate2.getValue() == 2) {
            environment.setRating2("N/A");
        } else {
            environment.setRating2("");
        }

        if (inspENRisk3.getValue() == 0) {
            environment.setRisk3("High");
        } else if (inspENRisk3.getValue() == 1) {
            environment.setRisk3("Med");
        } else if (inspENRisk3.getValue() == 2) {
            environment.setRisk3("Low");
        } else {
            environment.setRisk3("");
        }

        if (inspENRate3.getValue() == 0) {
            environment.setRating3("NW");
        } else if (inspENRate3.getValue() == 1) {
            environment.setRating3("Good");
        } else if (inspENRate3.getValue() == 2) {
            environment.setRating3("N/A");
        } else {
            environment.setRating3("");
        }

        if (inspENRisk4.getValue() == 0) {
            environment.setRisk4("High");
        } else if (inspENRisk4.getValue() == 1) {
            environment.setRisk4("Med");
        } else if (inspENRisk4.getValue() == 2) {
            environment.setRisk4("Low");
        } else {
            environment.setRisk4("");
        }

        if (inspENRate4.getValue() == 0) {
            environment.setRating4("NW");
        } else if (inspENRate4.getValue() == 1) {
            environment.setRating4("Good");
        } else if (inspENRate4.getValue() == 2) {
            environment.setRating4("N/A");
        } else {
            environment.setRating4("");
        }

        ds.insertPlannedInspection(environment);

        /* === COMMUNCATIONS === */

        PlannedInspectionModel communication = new PlannedInspectionModel();
        communication.setDate(dateET.getText().toString());
        communication.setInspector(inspectorET.getText().toString());
        communication.setAreaOwner(areaOwnerET.getText().toString());
        communication.setLocation(locationET.getText().toString());
        communication.setCode("Communication");

        if (inspCOMRisk1.getValue() == 0) {
            communication.setRisk1("High");
        } else if (inspCOMRisk1.getValue() == 1) {
            communication.setRisk1("Med");
        } else if (inspCOMRisk1.getValue() == 2) {
            communication.setRisk1("Low");
        } else {
            communication.setRisk1("");
        }

        if (inspCOMRate1.getValue() == 0) {
            communication.setRating1("NW");
        } else if (inspCOMRate1.getValue() == 1) {
            communication.setRating1("Good");
        } else if (inspCOMRate1.getValue() == 2) {
            communication.setRating1("N/A");
        } else {
            communication.setRating1("");
        }

        if (inspCOMRisk2.getValue() == 0) {
            communication.setRisk2("High");
        } else if (inspCOMRisk2.getValue() == 1) {
            communication.setRisk2("Med");
        } else if (inspCOMRisk2.getValue() == 2) {
            communication.setRisk2("Low");
        } else {
            communication.setRisk2("");
        }

        if (inspCOMRate2.getValue() == 0) {
            communication.setRating2("NW");
        } else if (inspCOMRate2.getValue() == 1) {
            communication.setRating2("Good");
        } else if (inspCOMRate2.getValue() == 2) {
            communication.setRating2("N/A");
        } else {
            communication.setRating2("");
        }

        if (inspCOMRisk3.getValue() == 0) {
            communication.setRisk3("High");
        } else if (inspCOMRisk3.getValue() == 1) {
            communication.setRisk3("Med");
        } else if (inspCOMRisk3.getValue() == 2) {
            communication.setRisk3("Low");
        } else {
            communication.setRisk3("");
        }

        if (inspCOMRate3.getValue() == 0) {
            communication.setRating3("NW");
        } else if (inspCOMRate3.getValue() == 1) {
            communication.setRating3("Good");
        } else if (inspCOMRate3.getValue() == 2) {
            communication.setRating3("N/A");
        } else {
            communication.setRating3("");
        }

        if (inspCOMRisk4.getValue() == 0) {
            communication.setRisk4("High");
        } else if (inspCOMRisk4.getValue() == 1) {
            communication.setRisk4("Med");
        } else if (inspCOMRisk4.getValue() == 2) {
            communication.setRisk4("Low");
        } else {
            communication.setRisk4("");
        }

        if (inspCOMRate4.getValue() == 0) {
            communication.setRating4("NW");
        } else if (inspCOMRate4.getValue() == 1) {
            communication.setRating4("Good");
        } else if (inspCOMRate4.getValue() == 2) {
            communication.setRating4("N/A");
        } else {
            communication.setRating4("");
        }

        if (inspCOMRisk5.getValue() == 0) {
            communication.setRisk5("High");
        } else if (inspCOMRisk5.getValue() == 1) {
            communication.setRisk5("Med");
        } else if (inspCOMRisk5.getValue() == 2) {
            communication.setRisk5("Low");
        } else {
            communication.setRisk5("");
        }

        if (inspCOMRate5.getValue() == 0) {
            communication.setRating5("NW");
        } else if (inspCOMRate5.getValue() == 1) {
            communication.setRating5("Good");
        } else if (inspCOMRate5.getValue() == 2) {
            communication.setRating5("N/A");
        } else {
            communication.setRating5("");
        }

        if (inspCOMRisk6.getValue() == 0) {
            communication.setRisk6("High");
        } else if (inspCOMRisk6.getValue() == 1) {
            communication.setRisk6("Med");
        } else if (inspCOMRisk6.getValue() == 2) {
            communication.setRisk6("Low");
        } else {
            communication.setRisk6("");
        }

        if (inspCOMRate6.getValue() == 0) {
            communication.setRating6("NW");
        } else if (inspCOMRate6.getValue() == 1) {
            communication.setRating6("Good");
        } else if (inspCOMRate6.getValue() == 2) {
            communication.setRating6("N/A");
        } else {
            communication.setRating6("");
        }

        if (inspCOMRisk7.getValue() == 0) {
            communication.setRisk7("High");
        } else if (inspCOMRisk7.getValue() == 1) {
            communication.setRisk7("Med");
        } else if (inspCOMRisk7.getValue() == 2) {
            communication.setRisk7("Low");
        } else {
            communication.setRisk7("");
        }

        if (inspCOMRate7.getValue() == 0) {
            communication.setRating7("NW");
        } else if (inspCOMRate7.getValue() == 1) {
            communication.setRating7("Good");
        } else if (inspCOMRate7.getValue() == 2) {
            communication.setRating7("N/A");
        } else {
            communication.setRating7("");
        }

        ds.insertPlannedInspection(communication);

        /* === FALL PROTECTION === */

        PlannedInspectionModel fallProtection = new PlannedInspectionModel();
        fallProtection.setDate(dateET.getText().toString());
        fallProtection.setInspector(inspectorET.getText().toString());
        fallProtection.setAreaOwner(areaOwnerET.getText().toString());
        fallProtection.setLocation(locationET.getText().toString());
        fallProtection.setCode("Fall Protection");

        if (inspFPRisk1.getValue() == 0) {
            fallProtection.setRisk1("High");
        } else if (inspFPRisk1.getValue() == 1) {
            fallProtection.setRisk1("Med");
        } else if (inspFPRisk1.getValue() == 2) {
            fallProtection.setRisk1("Low");
        } else {
            fallProtection.setRisk1("");
        }

        if (inspFPRate1.getValue() == 0) {
            fallProtection.setRating1("NW");
        } else if (inspFPRate1.getValue() == 1) {
            fallProtection.setRating1("Good");
        } else if (inspFPRate1.getValue() == 2) {
            fallProtection.setRating1("N/A");
        } else {
            fallProtection.setRating1("");
        }

        if (inspFPRisk2.getValue() == 0) {
            fallProtection.setRisk2("High");
        } else if (inspFPRisk2.getValue() == 1) {
            fallProtection.setRisk2("Med");
        } else if (inspFPRisk2.getValue() == 2) {
            fallProtection.setRisk2("Low");
        } else {
            fallProtection.setRisk2("");
        }

        if (inspFPRate2.getValue() == 0) {
            fallProtection.setRating2("NW");
        } else if (inspFPRate2.getValue() == 1) {
            fallProtection.setRating2("Good");
        } else if (inspFPRate2.getValue() == 2) {
            fallProtection.setRating2("N/A");
        } else {
            fallProtection.setRating2("");
        }

        if (inspFPRisk3.getValue() == 0) {
            fallProtection.setRisk3("High");
        } else if (inspFPRisk3.getValue() == 1) {
            fallProtection.setRisk3("Med");
        } else if (inspFPRisk3.getValue() == 2) {
            fallProtection.setRisk3("Low");
        } else {
            fallProtection.setRisk3("");
        }

        if (inspFPRate3.getValue() == 0) {
            fallProtection.setRating3("NW");
        } else if (inspFPRate3.getValue() == 1) {
            fallProtection.setRating3("Good");
        } else if (inspFPRate3.getValue() == 2) {
            fallProtection.setRating3("N/A");
        } else {
            fallProtection.setRating3("");
        }

        if (inspFPRisk4.getValue() == 0) {
            fallProtection.setRisk4("High");
        } else if (inspFPRisk4.getValue() == 1) {
            fallProtection.setRisk4("Med");
        } else if (inspFPRisk4.getValue() == 2) {
            fallProtection.setRisk4("Low");
        } else {
            fallProtection.setRisk4("");
        }

        if (inspFPRate4.getValue() == 0) {
            fallProtection.setRating4("NW");
        } else if (inspFPRate4.getValue() == 1) {
            fallProtection.setRating4("Good");
        } else if (inspFPRate4.getValue() == 2) {
            fallProtection.setRating4("N/A");
        } else {
            fallProtection.setRating4("");
        }

        if (inspFPRisk5.getValue() == 0) {
            fallProtection.setRisk5("High");
        } else if (inspFPRisk5.getValue() == 1) {
            fallProtection.setRisk5("Med");
        } else if (inspFPRisk5.getValue() == 2) {
            fallProtection.setRisk5("Low");
        } else {
            fallProtection.setRisk5("");
        }

        if (inspFPRate5.getValue() == 0) {
            fallProtection.setRating5("NW");
        } else if (inspFPRate5.getValue() == 1) {
            fallProtection.setRating5("Good");
        } else if (inspFPRate5.getValue() == 2) {
            fallProtection.setRating5("N/A");
        } else {
            fallProtection.setRating5("");
        }

        if (inspFPRisk6.getValue() == 0) {
            fallProtection.setRisk6("High");
        } else if (inspFPRisk6.getValue() == 1) {
            fallProtection.setRisk6("Med");
        } else if (inspFPRisk6.getValue() == 2) {
            fallProtection.setRisk6("Low");
        } else {
            fallProtection.setRisk6("");
        }

        if (inspFPRate6.getValue() == 0) {
            fallProtection.setRating6("NW");
        } else if (inspFPRate6.getValue() == 1) {
            fallProtection.setRating6("Good");
        } else if (inspFPRate6.getValue() == 2) {
            fallProtection.setRating6("N/A");
        } else {
            fallProtection.setRating6("");
        }

        ds.insertPlannedInspection(fallProtection);

//        Toast.makeText(mActivity, "Data Saved", Toast.LENGTH_LONG);

        ds.close();

    }

    public static void clearData() {
        plannedFindingsData.clear();
        plannedFindingPhotoAdapter.notifyDataSetChanged();

        dateET.setText("");
        dateTL.setError(null);
        dateSafeET.setText("");
        dateDeptET.setText("");
        dateSiteET.setText("");
        inspectorET.setText("");
        inspectorTL.setError(null);
        areaOwnerET.setText("");
        areaOwnerTL.setError(null);
        siteSVET.setText("");
        deptManagerET.setText("");
        deptSafeET.setText("");
        locationET.setText("");
        locationTL.setError(null);

        // Work Area
        inspWaRate1.setValue(-1);
        inspWaRate2.setValue(-1);
        inspWaRate3.setValue(-1);
        inspWaRate4.setValue(-1);
        inspWaRate5.setValue(-1);
        inspWaRate6.setValue(-1);

        inspWaRisk1.setValue(-1);
        inspWaRisk2.setValue(-1);
        inspWaRisk3.setValue(-1);
        inspWaRisk4.setValue(-1);
        inspWaRisk5.setValue(-1);
        inspWaRisk6.setValue(-1);

        // Structures
        inspStRate1.setValue(-1);
        inspStRate2.setValue(-1);
        inspStRate3.setValue(-1);
        inspStRate4.setValue(-1);
        inspStRate5.setValue(-1);

        inspStRisk1.setValue(-1);
        inspStRisk2.setValue(-1);
        inspStRisk3.setValue(-1);
        inspStRisk4.setValue(-1);
        inspStRisk5.setValue(-1);

        // Machinery Tools
        inspMtRate1.setValue(-1);
        inspMtRate2.setValue(-1);
        inspMtRate3.setValue(-1);
        inspMtRate4.setValue(-1);
        inspMtRate5.setValue(-1);
        inspMtRate6.setValue(-1);
        inspMtRate7.setValue(-1);

        inspMtRisk1.setValue(-1);
        inspMtRisk2.setValue(-1);
        inspMtRisk3.setValue(-1);
        inspMtRisk4.setValue(-1);
        inspMtRisk5.setValue(-1);
        inspMtRisk6.setValue(-1);
        inspMtRisk7.setValue(-1);

        // Electrical
        inspElRate1.setValue(-1);
        inspElRate2.setValue(-1);
        inspElRate3.setValue(-1);
        inspElRate4.setValue(-1);
        inspElRate5.setValue(-1);

        inspElRisk1.setValue(-1);
        inspElRisk2.setValue(-1);
        inspElRisk3.setValue(-1);
        inspElRisk4.setValue(-1);
        inspElRisk5.setValue(-1);

        // Personel
        inspPeRate1.setValue(-1);
        inspPeRate2.setValue(-1);
        inspPeRate3.setValue(-1);
        inspPeRate4.setValue(-1);
        inspPeRate5.setValue(-1);
        inspPeRate6.setValue(-1);
        inspPeRate7.setValue(-1);
        inspPeRate8.setValue(-1);

        inspPeRisk1.setValue(-1);
        inspPeRisk2.setValue(-1);
        inspPeRisk3.setValue(-1);
        inspPeRisk4.setValue(-1);
        inspPeRisk5.setValue(-1);
        inspPeRisk6.setValue(-1);
        inspPeRisk7.setValue(-1);
        inspPeRisk8.setValue(-1);

        // Fire Safety
        inspFSRate1.setValue(-1);
        inspFSRate2.setValue(-1);
        inspFSRate3.setValue(-1);
        inspFSRate4.setValue(-1);
        inspFSRate5.setValue(-1);

        inspFSRisk1.setValue(-1);
        inspFSRisk2.setValue(-1);
        inspFSRisk3.setValue(-1);
        inspFSRisk4.setValue(-1);
        inspFSRisk5.setValue(-1);

        // First Aid Kit
        inspFARate1.setValue(-1);
        inspFARate2.setValue(-1);
        inspFARate3.setValue(-1);

        inspFARisk1.setValue(-1);
        inspFARisk2.setValue(-1);
        inspFARisk3.setValue(-1);

        // Environmental
        inspENRate1.setValue(-1);
        inspENRate2.setValue(-1);
        inspENRate3.setValue(-1);
        inspENRate4.setValue(-1);

        inspENRisk1.setValue(-1);
        inspENRisk2.setValue(-1);
        inspENRisk3.setValue(-1);
        inspENRisk4.setValue(-1);

        // Communication
        inspCOMRate1.setValue(-1);
        inspCOMRate2.setValue(-1);
        inspCOMRate3.setValue(-1);
        inspCOMRate4.setValue(-1);
        inspCOMRate5.setValue(-1);
        inspCOMRate6.setValue(-1);
        inspCOMRate7.setValue(-1);

        inspCOMRisk1.setValue(-1);
        inspCOMRisk2.setValue(-1);
        inspCOMRisk3.setValue(-1);
        inspCOMRisk4.setValue(-1);
        inspCOMRisk5.setValue(-1);
        inspCOMRisk6.setValue(-1);
        inspCOMRisk7.setValue(-1);

        // Fall Protection
        inspFPRate1.setValue(-1);
        inspFPRate2.setValue(-1);
        inspFPRate3.setValue(-1);
        inspFPRate4.setValue(-1);
        inspFPRate5.setValue(-1);
        inspFPRate6.setValue(-1);

        inspFPRisk1.setValue(-1);
        inspFPRisk2.setValue(-1);
        inspFPRisk3.setValue(-1);
        inspFPRisk4.setValue(-1);
        inspFPRisk5.setValue(-1);
        inspFPRisk6.setValue(-1);

        selectedInspectorList.clear();

        DataSource ds = new DataSource(mActivity);
        ds.open();
        if (!ds.checkTableFindings().isEmpty()) {
            INSPECTION_CODE = Integer.valueOf(ds.getLastPlannedFindingCode()) + 1;
        } else {
            INSPECTION_CODE = 1;
        }
        ds.close();
    }

    /* On Activity Result */

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PlannedFindingsPopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {
            if (Helper.fileUri != null) {
//                new file (tempPhotoDir).mkdirs();
                Helper.setPic(PlannedFindingsPopUp.getPicture(), Helper.fileUri.getPath());
                PlannedFindingsPopUp.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.planned;
        } else if (requestCode == PlannedFindingsPopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK && data != null) {
//            new file (tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            PlannedFindingsPopUp.setImagePath(picturePath);
            Helper.setPic(PlannedFindingsPopUp.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.planned;
    }

    public void exportCsv() {
        ArrayList<String[]> transformedData = new ArrayList<String[]>();

        String location = locationET.getText().toString();

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

        String[] header7 = {"No", "Date", "Inspector", "Location",
                "Area Owner", "AppVersion", "IMEI"};
        transformedData.add(header7);

        String areaOwner = areaOwnerET.getText().toString();
        for (int i = 0; i < selectedAreaOwnerList.size(); i++) {
            areaOwner += selectedAreaOwnerList.get(i).getFormattedString();
            if (i < selectedAreaOwnerList.size() - 1) {
                areaOwner += "; ";
            }
        }

        DataSource ds = new DataSource(mActivity);
        ds.open();
        ArrayList<PlannedMainModel> mainData = ds.getAllPlannedMain();
        ds.close();
        int count = 0;
        for (int i = 0; i < selectedInspectorList.size(); i++) {
            count++;
            String[] content = {String.valueOf(count),
                    mainData.get(i).getDate(),
                    selectedInspectorList.get(i).getIdno(), location, mainData.get(i).getAreaOwner(),
                    version, telephonyManager.getDeviceId()};
            transformedData.add(content);
        }

        // GENERATE FILE NAME FORMAT
        String formattedID = formatIDInspector(selectedInspectorList.get(0).getIdno());
//                areaOwnerET.getText().toString();
        String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        String fullPath = "";
        String fileName = "SHE_INSPECTION_PSI_" + time + "_" + formattedID
                + ".csv";
        fullPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/" + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.EXPORT_FOLDER_NAME + "/";

        if (CSVHelper.writeCSV(fullPath, fileName, transformedData)) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getActivity(),
                            "Export CSV berhasil", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getActivity(),
                            "Export CSV gagal", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static void exportCsv(String path, String filename) {
        ArrayList<String[]> transformedData = new ArrayList<String[]>();

        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);

        String inspector = inspectorET.getText().toString();
        String areaOwner = areaOwnerET.getText().toString();
        String location = locationET.getText().toString();

        String[] header7 = {"InspectorName", "InspectorID", "InspectionType", "InspectionDate", "InspectionTime", "Location",
                "AreaOwner", "CategoryFinding", "Finding", "Action", "PictureName",
                "ResponsiblePerson", "DateComplete", "Acknowledgement", "AppVersion", "IMEI"};
        transformedData.add(header7);

        DataSource ds = new DataSource(mActivity);
        ds.open();
        plannedMainModel = ds.getLastestPlannedMain();
        ArrayList<PlannedFindingsModel> fm = ds.getAllPlannedFindings(plannedMainModel.getId());
        ds.close();

        // 1 Inspector 1 Finding
        for (int o = 0; o < selectedInspectorList.size(); o++) {
            if (fm.size() == 0) {
                String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "PSI",
                        dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), locationET.getText().toString(),
                        areaOwnerET.getText().toString(),
                        "",
                        "",
                        "",
                        "",
                        "", "", siteSVET.getText().toString() + deptManagerET.getText().toString()
                        + deptSafeET.getText().toString(), version, "IMEI " + telephonyManager.getDeviceId()};
                transformedData.add(content);
            } else {
                for (int i = 0; i < fm.size(); i++) {
                    if (fm.get(i).getPhotoPath() != null) {
                        String categoryFinding = "";
                        if (fm.get(i).getCode() > 0 && fm.get(i).getQuestionCode() > 0)
                            categoryFinding = fm.get(i).getCode() + "." + fm.get(i).getQuestionCode();
                        String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "PSI",
                                dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), locationET.getText().toString(),
                                areaOwnerET.getText().toString(),
                                categoryFinding,
                                fm.get(i).getFindings(),
                                fm.get(i).getComment(),
                                fm.get(i).getPhotoPath().substring(
                                        fm.get(i).getPhotoPath().lastIndexOf("/") + 1,
                                        fm.get(i).getPhotoPath().length()),
                                fm.get(i).getResponsible(), fm.get(i).getDate(), siteSVET.getText().toString() + deptManagerET.getText().toString()
                                + deptSafeET.getText().toString(), version, "IMEI " + telephonyManager.getDeviceId()};
                        transformedData.add(content);
                    } else if (fm.get(i).getPhotoPath() == null) {
                        String categoryFinding = "";
                        if (fm.get(i).getCode() > 0 && fm.get(i).getQuestionCode() > 0)
                            categoryFinding = fm.get(i).getCode() + "." + fm.get(i).getQuestionCode();
                        String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "PSI",
                                dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), locationET.getText().toString(),
                                areaOwnerET.getText().toString(),
                                categoryFinding,
                                fm.get(i).getFindings(),
                                fm.get(i).getComment(),
                                "",
                                fm.get(i).getResponsible(), fm.get(i).getDate(), siteSVET.getText().toString() + deptManagerET.getText().toString()
                                + deptSafeET.getText().toString(), version, "IMEI " + telephonyManager.getDeviceId()};
                        transformedData.add(content);
                    }
                }
            }
        }

        if (CSVHelper.writeCSV(path, filename, transformedData)) {
            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
            // "Export CSV berhasil\n file tersimpan di folder : "
            // + exportPath + fileName, null);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mActivity,
                            "Export CSV berhasil", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
            // "Export CSV gagal", null);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mActivity,
                            "Export CSV gagal", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    /* Back press */
    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveData();
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validationData()) {
                    saveData();
                    Intent intent = new Intent(mActivity, MainMenu.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                } else {
                    Intent intent = new Intent(mActivity, MainMenu.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }
        });
        builder.show();

    }

}
