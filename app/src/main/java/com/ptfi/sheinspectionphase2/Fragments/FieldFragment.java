package com.ptfi.sheinspectionphase2.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.Font;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.sheinspectionphase2.Adapter.FieldFindingPhotoAdapter;
import com.ptfi.sheinspectionphase2.Adapter.IPhotoAdapter;
import com.ptfi.sheinspectionphase2.Controller.Controller;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.MainMenu;
import com.ptfi.sheinspectionphase2.Models.CustomSpinnerItem;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.Models.FieldMainModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingCallback;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingsPopUp;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.CSVHelper;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static java.lang.String.valueOf;

/**
 * Created by senaardyputra on 11/11/16.
 */

public class FieldFragment extends Fragment {

    protected static FragmentActivity mActivity;

    static View rootView;

    static TextView imeiTV, versionTV;

    private static TextInputLayout companyTL, businessTL, departmentTL, sectionTL, dateTL, workAreaTL, inspectorTL;

    private static EditText companyET, businessET, departmentET, sectionET, dateET, workAreaET, inspectorET;

    private int selectedSpinner = 1;
    private int selectedBusinessSpinner = 1;
    private int selectedDepartmentSpinner = 1;
    private int selectedSectionSpinner = 1;
    int companyEnteringTimes = 0;
    int bussinessEnteringTimes = 0;
    int departmentEnteringTimes = 0;
    int sectionEnteringTimes = 0;
    private int loopSection = 0;
    private int loopDept = 0;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;
    public static Spinner companySpinner;
    public static Spinner businessSpinner;
    public static Spinner departmentSpinner;
    public static Spinner sectionSpinner;
    public static ArrayList<CustomSpinnerItem> companyData;
    public static ArrayList<CustomSpinnerItem> businessData;
    public static ArrayList<CustomSpinnerItem> departmentData;
    public static ArrayList<CustomSpinnerItem> sectionData;
    public static ArrayList<Integer> selectedRecordCompany = new ArrayList<Integer>();
    public static ArrayList<Integer> selectedRecordBusiness = new ArrayList<Integer>();
    public static ArrayList<Integer> selectedRecordDepartment = new ArrayList<Integer>();
    public static ArrayList<Integer> selectedRecordSection = new ArrayList<Integer>();
    //    private static ButtonRectangle generateBtn, previewBtn, clearBtn;
    CardView photoCV;

    static String version;

    LinearLayout photoLL;

    ImageView addFindings, deleteName, addName;

    static RecyclerView photoRV;
    static RecyclerView.LayoutManager layoutManager;

    public static ArrayList<FieldFindingsModel> fieldFindingsData = new ArrayList<>();
    ArrayList<ManpowerModel> manpowers = new ArrayList<ManpowerModel>();
    static ManpowerModel selectedObserver = null;
    private Dialog activeDialog;
    static ArrayList<ManpowerModel> selectedInspectorList = new ArrayList<ManpowerModel>();
    ArrayList<ManpowerModel> selectedResPersonList = new ArrayList<ManpowerModel>();

    private static final int OPEN_PREVIEW = 323;
    private static final int PREVIEW_CODE_SHE_PLANNED = 404;
    private static final int PREVIEW_CODE_SHE_UG = 405;

    static ImageView expandIcon;

    private static FieldMainModel mainModel;
    private Controller aController;
    private static FieldFindingPhotoAdapter fieldFindingPhotoAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_field_inspection, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Helper.visiblePage = Helper.visiblePage.field;

        DataSource ds = new DataSource(getActivity());
        ds.open();
        aController = (Controller) getActivity().getApplicationContext();
        mainModel = ds.getLastesFieldMain();
        fieldFindingsData = ds.getFieldFindingsData(mainModel.getId());
        ds.close();

        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();
        initRecycleView();
        manpowers = Constants.getObserverDictionary();
        loadData();
        spinnerField();

        return rootView;
    }

    private void initRecycleView() {
        fieldFindingPhotoAdapter = new FieldFindingPhotoAdapter(fieldFindingsData, new IPhotoAdapter() {

            @Override
            public void onItemClicked(int position) {
                onListViewClick(position, fieldFindingsData.get(position));
            }

            @Override
            public void onItemDelete(int position) {
                onListViewDelete(position);
            }
        });

        photoRV.setAdapter(fieldFindingPhotoAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        photoRV.setLayoutManager(layoutManager);
    }

    private void onListViewClick(final int position, FieldFindingsModel fieldFindingsModel) {
        if (validateSaveData()) {
            FieldFindingsPopUp.showFieldFindingsPopUp(mActivity, fieldFindingsModel,
                    new FieldFindingCallback() {
                        @Override
                        public void onFindingSubmitted(FieldFindingsModel model) {
                            fieldFindingsData.set(position, model);
                            fieldFindingPhotoAdapter.notifyDataSetChanged();
                        }
                    });
        }
    }

    private void onListViewDelete(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to delete this finding?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fieldFindingsData.remove(position);
                fieldFindingPhotoAdapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void initComponent() {
        if (rootView != null) {
            photoRV = (RecyclerView) rootView.findViewById(R.id.photoRV);
            companyTL = (TextInputLayout) rootView.findViewById(R.id.companyTL);
            businessTL = (TextInputLayout) rootView.findViewById(R.id.businessTL);
            departmentTL = (TextInputLayout) rootView.findViewById(R.id.departmentTL);
            sectionTL = (TextInputLayout) rootView.findViewById(R.id.sectionTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            workAreaTL = (TextInputLayout) rootView.findViewById(R.id.workAreaTL);
            inspectorTL = (TextInputLayout) rootView.findViewById(R.id.inspectorTL);

            companyET = (EditText) rootView.findViewById(R.id.companyET);
            businessET = (EditText) rootView.findViewById(R.id.businessET);
            departmentET = (EditText) rootView.findViewById(R.id.departmentET);
            sectionET = (EditText) rootView.findViewById(R.id.sectionET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            workAreaET = (EditText) rootView.findViewById(R.id.workAreaET);
            inspectorET = (EditText) rootView.findViewById(R.id.inspectorET);

            photoCV = (CardView) rootView.findViewById(R.id.photoCV);

            addName = (ImageView) rootView.findViewById(R.id.addName);
            addName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInspectorPopUp(Constants.SEARCH_TYPE_INSPECTOR, "Inspector");
                }
            });

            deleteName = (ImageView) rootView.findViewById(R.id.deleteName);

            addFindings = (ImageView) rootView.findViewById(R.id.addFindings);

            photoLL = (LinearLayout) rootView.findViewById(R.id.photoLL);

            inspectorET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (validationMain()) {
                    showInspectorPopUp(Constants.SEARCH_TYPE_INSPECTOR, "Inspector");
//                    }
                }
            });

            expandIcon = (ImageView) rootView.findViewById(R.id.expandIcon);

            photoCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                        expandIcon.setImageResource(R.drawable.collapse_logo);
                    } else {
                        photoLL.setVisibility(View.GONE);
                        expandIcon.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            expandIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                        expandIcon.setImageResource(R.drawable.collapse_logo);
                    } else {
                        photoLL.setVisibility(View.GONE);
                        expandIcon.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            addFindings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateSaveData()) {
                        FieldFindingsPopUp.showFieldFindingsPopUp(mActivity, new FieldFindingsModel(),
                                new FieldFindingCallback() {
                                    @Override
                                    public void onFindingSubmitted(FieldFindingsModel model) {
                                        fieldFindingsData.add(0, model);
                                        fieldFindingPhotoAdapter.notifyDataSetChanged();
                                    }
                                });
                    }
                }
            });

//            addFindings.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (validateSaveData()) {
//                        FieldFindingsPopUp.showFieldFindingsPopUp(mActivity, false, companyET.getText().toString(), departmentET.getText().toString(),
//                                sectionET.getText().toString(), inspectorET.getText().toString(), dateET.getText().toString(), Constants.INSPECTION_FIELD, "", "", "", "", "", "",
//                                new FieldFindingCallback() {
//                                    @Override
//                                    public void onPlannedSubmited(FieldFindingsModel model) {
//                                        fieldFindingsData.add(model);
//                                        recycleView();
//                                    }
//                                });
//                    }
//                }
//            });

        }

        // Date
        final Calendar c = Calendar.getInstance();
        selectedYear = c.get(Calendar.YEAR);
        selectedMonth = c.get(Calendar.MONTH);
        selectedDate = c.get(Calendar.DAY_OF_MONTH);
        DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                selectedDate);
        dateET.setText(DataSingleton.getInstance().getFormattedDate());
        dateTL.setError(null);
    }


    public static boolean validateSaveData() {
        boolean status = true;

        if (companyET.getText().toString().length() == 0) {
            status = false;
            companyTL.setErrorEnabled(true);
            companyTL.setError("Please choose Company first");
        } else {
            companyTL.setError(null);
        }

        if (businessET.getText().toString().length() == 0) {
            status = false;
            businessTL.setErrorEnabled(true);
            businessTL.setError("Please choose Business Unit first");
        } else {
            businessTL.setError(null);
        }

        if (departmentET.getText().toString().length() == 0) {
            status = false;
            departmentTL.setErrorEnabled(true);
            departmentTL.setError("Please choose Department first");
        } else {
            departmentTL.setError(null);
        }

        if (sectionET.getText().toString().length() == 0) {
            status = false;
            sectionTL.setErrorEnabled(true);
            sectionTL.setError("Please choose Section first");
        } else {
            sectionTL.setError(null);
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date Inspection first");
        } else {
            dateTL.setError(null);
        }

        if (workAreaET.getText().toString().length() == 0) {
            status = false;
            workAreaTL.setErrorEnabled(true);
            workAreaTL.setError("Please fill Work Area first");
        } else {
            workAreaTL.setError(null);
        }

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector first");
        } else {
            inspectorTL.setError(null);
        }

        return status;
    }

    public void spinnerField() {
        final DataSource ds = new DataSource(getActivity());
        ds.open();

        companyData = ds.getAllCompany();
        companySpinner = (Spinner) rootView.findViewById(R.id.companySpinner);

        Helper.CustomSpinnerAdapter adapter = new Helper().new CustomSpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item, companyData);
        companySpinner.setAdapter(adapter);
        companySpinner.setSelection(1);
        companySpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        Log.e("Company", "selected");
                        ds.open();
                        // untuk row pertama tidak bisa diselect (header)
                        if (pos != 0) {
                            selectedRecordCompany.add(pos);
                            selectedSpinner = selectedBusinessSpinner != 1 ? selectedBusinessSpinner
                                    : 1;
                            updateSpinner(R.id.businessSpinner, ds
                                    .getBusinessByCompany(parent
                                            .getItemAtPosition(pos).toString()), rootView);
                            selectedSpinner = 1;
                            selectedBusinessSpinner = 1;
                        } else {
                            companySpinner.setSelection(selectedRecordCompany
                                    .get(selectedRecordCompany.size() - 1));
                        }
                        if (companyEnteringTimes >= 1) {
                            companyET.setText(((CustomSpinnerItem) parent
                                    .getSelectedItem()).description);
                        }
                        ds.close();
                        companyEnteringTimes++;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // Do nothing, just another required interface
                        // callback
                    }
                });

        // populate spinner business unit
        businessData = ds.getBusinessByCompany("-");
        // Constants.getBusinessDictionary("-",
        // getActivity());
        adapter = new Helper().new CustomSpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item, businessData);
        businessSpinner = (Spinner) rootView.findViewById(R.id.businessSpinner);

        businessSpinner.setAdapter(adapter);
        businessSpinner.setSelection(1);
        businessSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        Log.e("Business", "selected");
                        ds.open();
                        // untuk row pertama tidak bisa diselect (header)
                        if (pos != 0) {
                            selectedRecordBusiness.add(pos);
                            selectedSpinner = selectedDepartmentSpinner != 1 ? selectedDepartmentSpinner
                                    : 1;
                            updateSpinner(R.id.departmentSpinner, ds
                                    .getDepartmentByBusiness(parent
                                            .getItemAtPosition(pos).toString()), rootView);
                            selectedSpinner = 1;
                            loopDept++;
                            if (loopDept == 2) {
                                selectedDepartmentSpinner = 1;
                                loopDept = 0;
                            }
                        } else {
                            businessSpinner.setSelection(selectedRecordBusiness
                                    .get(selectedRecordBusiness.size() - 1));
                        }
                        if (bussinessEnteringTimes >= 2) {
                            businessET.setText(((CustomSpinnerItem) parent
                                    .getSelectedItem()).description);
                        }
                        bussinessEnteringTimes++;
                        ds.close();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // Do nothing, just another required interface
                        // callback
                    }
                });

        // populate spinner department
        departmentData = ds.getDepartmentByBusiness("-");
        // Constants.getDepartmentDictionary("-",
        // getActivity());
        adapter = new Helper().new CustomSpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item, departmentData);
        departmentSpinner = (Spinner) rootView.findViewById(R.id.departmentSpinner);
        departmentSpinner.setAdapter(adapter);
        departmentSpinner.setSelection(1);

        departmentSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        Log.e("Department", "selected");
                        ds.open();
                        // untuk row pertama tidak bisa diselect (header)
                        if (pos != 0) {
                            selectedRecordDepartment.add(pos);
                            selectedSpinner = selectedSectionSpinner != 1 ? selectedSectionSpinner
                                    : 1;
                            updateSpinner(R.id.sectionSpinner, ds
                                    .getSectionByDepartment(parent
                                            .getItemAtPosition(pos).toString()), rootView);
                            selectedSpinner = 1;
                            loopSection++;
                            if (loopSection == 2) {
                                selectedSectionSpinner = 1;
                                loopSection = 0;
                            }
                        } else {
                            departmentSpinner
                                    .setSelection(selectedRecordDepartment
                                            .get(selectedRecordDepartment
                                                    .size() - 1));
                        }
                        if (departmentEnteringTimes >= 3) {
                            departmentET.setText(((CustomSpinnerItem) parent
                                    .getSelectedItem()).description);
                        }
                        departmentEnteringTimes++;
                        ds.close();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // Do nothing, just another required interface
                        // callback
                    }
                });

        // populate spinner section
        sectionData = ds.getSectionByDepartment("-");
        // Constants.getDepartmentDictionary("-",
        // getActivity());
        adapter = new Helper().new CustomSpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item, sectionData);
        sectionSpinner = (Spinner) rootView.findViewById(R.id.sectionSpinner);
        sectionSpinner.setAdapter(adapter);
        sectionSpinner.setSelection(1);

        sectionSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // untuk row pertama tidak bisa diselect (header)
                        Log.e("Section", "selected");
                        if (pos != 0) {
                            selectedRecordSection.add(pos);
                        } else {
                            sectionSpinner.setSelection(selectedRecordSection
                                    .get(selectedRecordSection.size() - 1));
                        }
                        if (sectionEnteringTimes >= 4) {
                            sectionET.setText(((CustomSpinnerItem) parent
                                    .getSelectedItem()).description);
                        }
                        sectionEnteringTimes++;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // Do nothing, just another required interface
                        // callback
                    }
                });
        ds.close();

        companyET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                companySpinner.performClick();
            }
        });

        businessET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessSpinner.performClick();
            }
        });

        departmentET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departmentSpinner.performClick();
            }
        });

        sectionET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sectionSpinner.performClick();
            }
        });
    }

    public void updateSpinner(int id, ArrayList<CustomSpinnerItem> listNewData, final View rootView) {
        Spinner spinner = (Spinner) rootView.findViewById(id);
        Helper.CustomSpinnerAdapter adapter = new Helper().new CustomSpinnerAdapter(
                getActivity(),
                android.R.layout.simple_spinner_item, listNewData);
        spinner.setAdapter(adapter);
        spinner.setSelection(selectedSpinner);
        selectedSpinner = 1;
    }

    public static void previewReport() {
        saveData();
//        Reporting.generateReportField(mActivity, inspectorET.getText().toString(), dateET.getText().toString(), departmentET.getText().toString(),
//                sectionET.getText().toString(), companyET.getText().toString(), workAreaET.getText().toString(), versionTV.getText().toString(),
//                false, fieldFindingsData);

        GenerateAsync generate = new GenerateAsync();
        generate.execute(false);
    }

    public static void genereateReport() {
        if (validationData()) {
            saveData();
//        Reporting.generateReportField(mActivity, inspectorET.getText().toString(), dateET.getText().toString(), departmentET.getText().toString(),
//                sectionET.getText().toString(), companyET.getText().toString(), workAreaET.getText().toString(), versionTV.getText().toString(),
//                true, fieldFindingsData);

            GenerateAsync generate = new GenerateAsync();
            generate.execute(true);
        }
    }


    /* validation data */
    public static boolean validationData() {
        boolean status = true;

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill date first");
        }

        return status;
    }

//    public void populateSpinner(){
//        //company
//        final DataSource ds = new DataSource(getActivity());
//        ds.open();
//
//        companyData = ds.getAllCompany();
//        companySpinner = (Spinner) rootView.findViewById(R.id.co)
//
//    }

    public static void datePickers(final Activity MainActivity, final View rootView) {
        switch (rootView.getId()) {
            case R.id.dateET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;

            default:
                break;
        }
    }

    @SuppressLint("MissingPermission")
    public static void exportCsv(String path, String filename) {
        ArrayList<String[]> transformedData = new ArrayList<String[]>();
        String department = departmentET.getText().toString();
        String section = sectionET.getText().toString();
        String company = companyET.getText().toString();
        String workArea = workAreaET.getText().toString();

        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String[] header7 = {"InspectorName", "InspectorID", "InspectionType", "InspectionDate", "InspectionTime", "Location",
                "AreaOwner", "CategoryFinding", "Finding", "Action", "PictureName",
                "ResponsiblePerson", "DateComplete", "Acknowledgement", "AppVersion", "IMEI"};
        transformedData.add(header7);

        DataSource ds = new DataSource(mActivity);
        ds.open();
        FieldMainModel lastData = ds.getLastesFieldMain();
        ArrayList<FieldFindingsModel> fm = ds.getFieldFindingsData(lastData.getId());
        ds.close();

        for (int o = 0; o < selectedInspectorList.size(); o++) {
            if (fm.size() == 0) {
                String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "FSI",
                        dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), workAreaET.getText().toString(), "", "",
                        "",
                        "",
                        "",
                        "", "", "", version, "IMEI " + telephonyManager.getDeviceId()};
                transformedData.add(content);
            }
            for (int i = 0; i < fm.size(); i++) {
                if (fm.get(i).getPicturePath() != null) {
                    String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "FSI",
                            dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), workAreaET.getText().toString(), "", "",
                            fm.get(i).getFindings(),
                            fm.get(i).getAction(),
                            fm.get(i).getPicturePath().substring(
                                    fm.get(i).getPicturePath().lastIndexOf("/") + 1,
                                    fm.get(i).getPicturePath().length()),
                            fm.get(i).getResponsible(), fm.get(i).getDateComplete(), "", version, "IMEI " + telephonyManager.getDeviceId()};
                    transformedData.add(content);
                } else if (fm.get(i).getPicturePath() == null) {
                    String[] content = {selectedInspectorList.get(o).getName(), formatIDInspector(selectedInspectorList.get(o).getIdno()), "FSI",
                            dateET.getText().toString(), new SimpleDateFormat("HH:mm").format(new Date()), workAreaET.getText().toString(), "", "",
                            fm.get(i).getFindings(),
                            fm.get(i).getAction(),
                            "",
                            fm.get(i).getResponsible(), fm.get(i).getDateComplete(), "", version, "IMEI " + telephonyManager.getDeviceId()};
                    transformedData.add(content);
                }
            }
        }

//        for (int i = 0; i < fm.size(); i++) {
//            if (fm.get(i).getPicturePath() != null) {
//                count++;
//                String[] content = {
//                        String.valueOf(count),
//                        fm.get(i).getFindings(),
//                        fm.get(i).getAction(),
//                        fm.get(i).getPicturePath().substring(
//                                fm.get(i).getPicturePath().lastIndexOf("/") + 1,
//                                fm.get(i).getPicturePath().length()),
//                        fm.get(i).getResponsible(), fm.get(i).getDateComplete() };
//                transformedData.add(content);
//            }
//        }
//
//        transformedData.add(new String[] { null });
//
//        String[] versions = { "App Version", version};
//        transformedData.add(versions);
//        String[] deviceID = { "Device IMEI", "'" + telephonyManager.getDeviceId() };
//        transformedData.add(deviceID);
//        String[] header1 = { "Company", company };
//        transformedData.add(header1);
//        String[] header2 = { "Department", department };
//        transformedData.add(header2);
//        String[] header3 = { "Section", section };
//        transformedData.add(header3);
//        String[] header4 = { "Date",
//                new SimpleDateFormat("dd-MMM-yyyy").format(new Date()) };
//        transformedData.add(header4);
//        String[] header5 = { "Inspector", inspectorET.getText().toString() };
//        transformedData.add(header5);
//        String[] header6 = { "Work Area / Location",
//                workArea};
//        transformedData.add(header6);

        if (CSVHelper.writeCSV(path, filename, transformedData)) {
            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
            // "Export CSV berhasil\n file tersimpan di folder : "
            // + exportPath + fileName, null);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mActivity,
                            "Export CSV berhasil", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
            // "Export CSV gagal", null);
            mActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mActivity,
                            "Export CSV gagal", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void exportCsv() {
        ArrayList<String[]> transformedData = new ArrayList<String[]>();
        String department = departmentET.getText().toString();
        String section = sectionET.getText().toString();
        String company = companyET.getText().toString();
        String workArea = workAreaET.getText().toString();

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);

//         String[] version = { "App Version", versionName };
//         transformedData.add(version);
//         String[] deviceID = { "Device IMEI", telephonyManager.getDeviceId()
//         };
//         transformedData.add(deviceID);
//
//         transformedData.add(new String[] { null });
        String[] header7 = {"No", "Company", "Department", "Section", "Date",
                "Inspector", "Work Area / Location", "Version", "IMEI"};
        transformedData.add(header7);

        DataSource ds = new DataSource(mActivity);
        ds.open();

        ArrayList<FieldMainModel> mainField = ds.getAllDataFieldMain();

        for (int i = 0; i < selectedInspectorList.size(); i++) {
            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            String[] content = {String.valueOf(i + 1), mainField.get(i).getCompany(), mainField.get(i).getDepartment(),
                    mainField.get(i).getSection(),
                    mainField.get(i).getDate(),
                    selectedInspectorList.get(i).getIdno(),
                    mainField.get(i).getWorkArea(), version, "'" + telephonyManager.getDeviceId()};
            transformedData.add(content);

        }

        // GENERATE FILE NAME FORMAT
        String path = "";
        String filename = "";
        String formattedID = inspectorET.getText().toString();
        String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        filename = "SHE_INSPECTION_PSI_" + time + "_" + formattedID + ".csv";
        path = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/" + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.EXPORT_FOLDER_NAME + "/";

        if (CSVHelper.writeCSV(path, filename, transformedData)) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getActivity(),
                            "Export CSV berhasil", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(getActivity(),
                            "Export CSV gagal", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void showInspectorPopUp(final int type, final String titlePopUp) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_inspector_field);

                TextView titlePopUpTV;

                final EditText idET, titleET, descET;

                final AutoCompleteTextView nameET;

                TextInputLayout nameTL, idTL, titleTL, descTL;

                ButtonRectangle clearAllBtn, submitBtn, cancelBtn;

                titlePopUpTV = (TextView) dialog.findViewById(R.id.titlePopUpTV);

                idET = (EditText) dialog.findViewById(R.id.idET);
                nameET = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
                titleET = (EditText) dialog.findViewById(R.id.titleET);
                descET = (EditText) dialog.findViewById(R.id.orgET);

                nameTL = (TextInputLayout) dialog.findViewById(R.id.nameTL);
                idTL = (TextInputLayout) dialog.findViewById(R.id.idTL);
                titleTL = (TextInputLayout) dialog.findViewById(R.id.titleTL);
                descTL = (TextInputLayout) dialog.findViewById(R.id.orgTL);

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);

                titlePopUpTV.setText(titlePopUp);

                final ArrayList<String> data = new ArrayList<String>();
                for (int i = 0; i < manpowers.size(); i++) {
                    data.add(manpowers.get(i).getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, data);
                nameET.setThreshold(1);
                nameET.setAdapter(adapter);
                nameET.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        String key = (String) nameET.getText().toString();
                        Log.d("WTF", key);
                        int pos = data.indexOf(key);

                        selectedObserver = manpowers.get(pos);
                        nameET.setText(selectedObserver.getName());
                        idET.setText(selectedObserver.getIdno());
                        titleET.setText(selectedObserver.getTitle());
                        descET.setText(selectedObserver.getOrg());
                    }
                });

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nameET.setText("");
                        idET.setText("");
                        titleET.setText("");
                        descET.setText("");
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (nameET.length() < 1 || idET.length() < 1) {
//                            warningLayout.setVisibility(View.VISIBLE);
                        } else {
                            String text = "";
                            ManpowerModel obs = new ManpowerModel();
                            obs.setIdno(idET.getText().toString());
                            obs.setName(nameET.getText().toString());
                            obs.setTitle(titleET.getText().toString());
                            obs.setOrg(descET.getText().toString());
                            if (selectedObserver != null) {
                                obs.setCompany(selectedObserver.getCompany());
                                obs.setDivisi(selectedObserver.getDivisi());
                                obs.setDepartment(selectedObserver.getDepartment());
                                obs.setSection(selectedObserver.getSection());
                            } else {

                            }

                            selectedObserver = obs;
                            switch (type) {
                                case Constants.SEARCH_TYPE_FPI_POPUP:
                                    selectedResPersonList.add(selectedObserver);
                                    EditText et = ((EditText) activeDialog
                                            .findViewById(R.id.inspectorET));
                                    text = et.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    et.setText(text);
                                    break;
                                case Constants.SEARCH_TYPE_INSPECTOR:
                                    selectedInspectorList.add(selectedObserver);
                                    text = inspectorET.getText().toString();
                                    text += selectedObserver.getName() + " ("
                                            + selectedObserver.getIdno() + "); ";
                                    inspectorET.setText(text);
                                    inspectorTL.setError(null);
                                    break;
                                default:
                                    break;
                            }

                            if (selectedObserver.getCompany() != null) {
                                if (selectedInspectorList.get(0).getCompany() != null) {
                                    companyET.setText(selectedInspectorList.get(0).getCompany());
                                }
                                if (selectedInspectorList.get(0).getDivisi() != null) {
                                    businessET.setText(selectedInspectorList.get(0).getDivisi());
                                }
                                if (selectedInspectorList.get(0).getDepartment() != null) {
                                    departmentET.setText(selectedInspectorList.get(0).getDepartment());
                                }
                                if (selectedInspectorList.get(0).getSection() != null) {
                                    sectionET.setText(selectedInspectorList.get(0).getSection());
                                }

                                selectedObserver = null;
                            } else {
                                companyET.setText("");
                                businessET.setText("");
                                departmentET.setText("");
                                sectionET.setText("");
                            }


                            dialog.dismiss();
                        }
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static void removeInspector(View v) {
        if (!selectedInspectorList.isEmpty()) {
            selectedInspectorList.remove(selectedInspectorList.size() - 1);
            String inspectors = "";
            for (ManpowerModel om : selectedInspectorList) {
                inspectors += om.getName() + " (" + om.getIdno() + "); ";
            }
            inspectorET.setText(inspectors);
            inspectorTL.setError(null);

            if (selectedInspectorList.size() > 0) {
                if (selectedInspectorList.get(0).getCompany() != null) {
                    companyET.setText(selectedInspectorList.get(0).getCompany());
                }
                if (selectedInspectorList.get(0).getDivisi() != null) {
                    businessET.setText(selectedInspectorList.get(0).getDivisi());
                }
                if (selectedInspectorList.get(0).getDepartment() != null) {
                    departmentET.setText(selectedInspectorList.get(0).getDepartment());
                }
                if (selectedInspectorList.get(0).getSection() != null) {
                    sectionET.setText(selectedInspectorList.get(0).getSection());
                }
            }

            if (inspectors.equalsIgnoreCase("")) {
                companyET.setText("");
                businessET.setText("");
                departmentET.setText("");
                sectionET.setText("");
            }
        }
    }

    public static boolean validationMain() {
        boolean status = true;

        if (companyET.getText().toString().length() == 0) {
            status = false;
            companyTL.setErrorEnabled(true);
            companyTL.setError("Please fill Company first");
        }

        if (businessET.getText().toString().length() == 0) {
            status = false;
            businessTL.setErrorEnabled(true);
            businessTL.setError("Please fill Business first");
        }

        if (departmentET.getText().toString().length() == 0) {
            status = false;
            departmentTL.setErrorEnabled(true);
            departmentTL.setError("Please fill Department first");
        }

        if (sectionET.getText().toString().length() == 0) {
            status = false;
            sectionTL.setErrorEnabled(true);
            sectionTL.setError("Please fill Section first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date Inspection first");
        }

        if (workAreaET.getText().toString().length() == 0) {
            status = false;
            workAreaTL.setErrorEnabled(true);
            workAreaTL.setError("Please fill Work Area first");
        }

        if (inspectorET.getText().toString().length() == 0) {
            status = false;
            inspectorTL.setErrorEnabled(true);
            inspectorTL.setError("Please fill Inspector first");
        }

        return status;
    }

    public void loadData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        /* ===================
                         Load Main Data
        =================== */
        companyET.setText(mainModel.getCompany());
        businessET.setText(mainModel.getBusiness());
        departmentET.setText(mainModel.getDepartment());
        sectionET.setText(mainModel.getSection());
        dateET.setText(mainModel.getDate());
        dateTL.setError(null);
        workAreaET.setText(mainModel.getWorkArea());
        inspectorET.setText(mainModel.getInspector());
        inspectorTL.setError(null);

        String[] inspectors = mainModel.getInspector().split(";");
        for (String inspector : inspectors) {
            inspector = inspector.trim();
            if (inspector.length() > 0) {
                String inspectorName = inspector.split("\\(")[0];
                inspectorName = inspectorName.trim();
                for (ManpowerModel manpower : manpowers) {
                    if (manpower.getName().equalsIgnoreCase(inspectorName)) {
                        selectedInspectorList.add(manpower);
                    }
                }
                if (selectedInspectorList.size() < 1) {
                    if (mainModel.getInspector().split(";").length > 0) {
                        String newInspector;
                        newInspector = mainModel.getInspector().split(";")[0]
                                .replace(")", "");
                        newInspector = newInspector.replace("(", "-");
                        if (newInspector.split("-").length > 1)
                            selectedInspectorList.add(new ManpowerModel(-1,
                                    newInspector.split("-")[1], newInspector
                                    .split("-")[0]));
                    }
                }
            }
        }

        /* ===================
                      Load Data Recycle View
        =================== */
        photoRV = (RecyclerView) rootView.findViewById(R.id.photoRV);
        photoRV.setHasFixedSize(true);

//        layoutManager = new LinearLayoutManager(getActivity());
//        photoRV.setLayoutManager(layoutManager);
//        photoRV.setItemAnimator(new DefaultItemAnimator());

        ds.close();

//        FieldFragment.FieldAdapter photoAdapter = new FieldFragment.FieldAdapter(getActivity(), fieldFindingsData);
//        photoRV.setAdapter(photoAdapter);

        ds.close();
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        FieldMainModel model = new FieldMainModel();
        model.setCompany(companyET.getText().toString());
        model.setBusiness(businessET.getText().toString());
        model.setDepartment(departmentET.getText().toString());
        model.setSection(sectionET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setInspector(inspectorET.getText().toString());
        model.setWorkArea(workAreaET.getText().toString());

        long insertedData = ds.insertFieldMainData(model);

        for (int i = 0; i < fieldFindingsData.size(); i++) {
            ds.insertFieldFindingsData(fieldFindingsData.get(i), insertedData);
        }

//        Toast.makeText(mActivity, "Data Saved", Toast.LENGTH_LONG);

        ds.close();
    }

    /* =======================
                 Generate Report Field
        ======================= */
    private static class GenerateAsync extends AsyncTask<Boolean, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            generateReportField(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();
        }
    }

    public static String formatIDInspector(String id) {
        String formattedID = "";

        for (int i = 0; i < (10 - id.length()); i++) {
            formattedID += "0";
        }
        formattedID += id;
        return formattedID;
    }

    public static void generateReportField(final boolean isGenerate) {

        String inspector = inspectorET.getText().toString();
        String date = dateET.getText().toString();
        String dept = departmentET.getText().toString();
        String section = sectionET.getText().toString();
        String company = companyET.getText().toString();
        String workArea = workAreaET.getText().toString();
        String version = versionTV.getText().toString();

        String reportPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath();
        reportPath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FIELD_INSPECTION_FILE_NAME;

        // GENERATE FORMATTED FILE NAME
        String fileName;
        String fileNamePdf = "";
        String formattedID = inspector;
        String time = new SimpleDateFormat("HHmm").format(new Date());
        String docName = Helper.ddMMMYYYYtoYYYYMMdd(date) + time + "_PSI_" + formatIDInspector(selectedInspectorList.get(0).getIdno());
        String folderPath;
        if (isGenerate) {
            folderPath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/"
                    + Constants.ROOT_FOLDER_NAME
                    + "/"
                    + Constants.APP_FOLDER_NAME
                    + "/"
                    + Constants.EXPORT_FOLDER_NAME + "/" + docName;
            File folder = new File(folderPath);
            folder.mkdir();

            fileName = folder + "/" + docName + ".doc";
            fileNamePdf = folder + "/" + docName + ".pdf";
            exportCsv(folderPath + "/", docName + ".csv");
        } else
            fileName = Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/"
                    + Constants.ROOT_FOLDER_NAME
                    + "/"
                    + Constants.APP_FOLDER_NAME
                    + "/"
                    + Constants.EXPORT_FOLDER_NAME
                    + "/"
                    + "report_she_field_inspection_preview.pdf";

        // CREATE IMAGE FOLDER
        String imageFolderPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath()
                + "/"
                + Constants.ROOT_FOLDER_NAME
                + "/"
                + Constants.APP_FOLDER_NAME
                + "/"
                + Constants.EXPORT_FOLDER_NAME + "/" + docName;
        File imageFolder = new File(imageFolderPath);

        try {
            Document doc = new Document(reportPath);

            // header report
            doc.getRange().replace(
                    "departmet_section_id",
                    dept
                            + (section.equalsIgnoreCase("-") ? "" : " - "
                            + section), true, true);
            doc.getRange().replace("date_id", date, true, true);

            TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
            String deviceID = telephonyManager.getDeviceId();

            doc.getRange().replace("device_id", "Device IMEI : " + deviceID,
                    true, true);
            doc.getRange().replace("version_id",
                    "SHE Inspection Version " + version, true, true);
            doc.getRange().replace("inspectors_id", inspector, true, true);
            doc.getRange().replace("work_area_id", workArea, true, true);

            // content
            Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 1, true);
            Row templateRow = (Row) sampleIdTable.getLastRow();
            TextView columnTV;
            DataSource ds = new DataSource(mActivity);
            ds.open();
            FieldMainModel lastData = ds.getLastesFieldMain();
            ArrayList<FieldFindingsModel> fieldFindingsData = ds.getFieldFindingsData(lastData.getId());
            ds.close();

            int photoDummyIndex = 1;

            int count = 0;
            Row dataRow;
            Row clonedRow;
            RowCollection photoTableRows = sampleIdTable.getRows();
            clonedRow = (Row) photoTableRows.get(photoDummyIndex);
            for (int i = 0; i < fieldFindingsData.size(); i++) {
                count++;
                dataRow = (Row) clonedRow.deepClone(true);
                sampleIdTable.getRows().add(dataRow);
                dataRow.getRowFormat().setAllowBreakAcrossPages(true);
//                    View v = dataFindings.getChildAt(i);

                // column 1
                Cell cell = dataRow.getCells().get(0);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(count)));

                // column 2
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.INSPECTION);
                cell = dataRow.getCells().get(1);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getFindings()));

                // column 3
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_ACTION);
                cell = dataRow.getCells().get(2);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getAction()));

                // column 4
                cell = dataRow.getCells().get(3);
                cell.getFirstParagraph().getRuns().clear();
                if (fieldFindingsData.get(i).getPicturePath() != null) {
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    CellFormat format = cell.getCellFormat();
                    double width = format.getWidth();
                    double height = cell.getParentRow().getRowFormat()
                            .getHeight();
                    builder.moveTo(cell.getFirstChild());

                    Bitmap imgBitmap = null;
                    if (isGenerate) {
                        imgBitmap = BitmapFactory.decodeFile(fieldFindingsData.get(i).getPicturePath());
                    } else {
                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(fieldFindingsData.get(i).getPicturePath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 90,
                                            150);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 180,
                                            150);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 0,
                                            150);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // Shape image = builder.insertImage(imgBitmap);
                    ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                    imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                            Constants.COMPRESS_PERCENTAGE, tempImage);
                    Bitmap compressedBitmap = BitmapFactory
                            .decodeStream(new ByteArrayInputStream(
                                    tempImage.toByteArray()));
                    Shape image = builder.insertImage(compressedBitmap);

                    double freePageWidth = width;
                    double freePageHeight = height;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData().getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth
                            || size.getHeightPoints() > freePageHeight;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }

                    if (isGenerate) {
//                        if (!imageFolder.exists()) {
//                            imageFolder.mkdir();
//                        }
//
//                        // MOVE IMAGE TO REPORT FOLDER
//                        File old = new File(fieldFindingsData.get(i).getPicturePath());
//                        String name = old.getName();
////
//                        InputStream in = new FileInputStream(fieldFindingsData.get(i).getPicturePath());
//                        OutputStream out = new FileOutputStream(
//                                imageFolderPath + "/" + name);
//                        Helper.copyFile(in, out);
//                        old.delete();
//
//                        FieldFindingsModel fm = fieldFindingsData.get(i);
//                        fm.setPicturePath(imageFolderPath + "/" + name);
//                        fieldFindingsData.set(i, fm);
                    } else {
                        for (Run run : cell.getFirstParagraph().getRuns()) {
//                            // Set some font formatting properties
                            Font font = run.getFont();
                            font.setColor(Color.BLACK);
                        }
                        cell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        cell.getFirstParagraph().appendChild(new Run(doc, ""));
                    }
                }

                // column 5
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_RESPONSIBLE);
                cell = dataRow.getCells().get(4);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getResponsible()));

                // column 6
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_DATE);
                cell = dataRow.getCells().get(5);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getDateComplete()));

//                    sampleIdTable.appendChild(dataRow);

            }

            Row dummyRow = photoTableRows.get(1);
            dummyRow.remove();

            doc.save(fileName);
            if (isGenerate)
                doc.save(fileNamePdf);

            File file;
            if (isGenerate) {
                file = new File(fileNamePdf);
            } else {
                file = new File(fileName);
            }


            // IF IS PREVIEW, SET RESULT
            if (isGenerate) {
                Helper.showPopUpMessage(mActivity, "",
                        "A report generated\n file has been saved in folder : "
                                + fileName, null);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(Uri.fromFile(file), type);
                mActivity.startActivityForResult(intent, OPEN_PREVIEW);
                Helper.progressDialog.dismiss();
            } else {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                String type = "application/pdf";
                intent.setDataAndType(Uri.fromFile(file), type);
                mActivity.startActivityForResult(intent, OPEN_PREVIEW);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FieldFindingsPopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {
            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(FieldFindingsPopUp.getPicture(), Helper.fileUri.getPath());
                FieldFindingsPopUp.setImagePath(Helper.fileUri.getPath());
//                ImagePopUp.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.field;
        } else if (requestCode == FieldFindingsPopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK && data != null) {
            // new File(tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            FieldFindingsPopUp.setImagePath(picturePath);
            Helper.setPic(FieldFindingsPopUp.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.field;
    }

    public static void clearData() {
//        DataSource ds = new DataSource(mActivity);
//        ds.open();
//        ds.deleteAllFindingField(companyET.getText().toString(), departmentET.getText().toString(),
//                sectionET.getText().toString(), dateET.getText().toString());
//        ds.close();
        fieldFindingsData.clear();
        fieldFindingPhotoAdapter.notifyDataSetChanged();

        sectionET.setText("");
        departmentET.setText("");
        businessET.setText("");
        companyET.setText("");

        dateET.setText("");
        dateTL.setError(null);
        workAreaET.setText("");
        inspectorET.setText("");
        inspectorTL.setError(null);

        selectedObserver = new ManpowerModel();
        selectedInspectorList = new ArrayList<>();
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveData();
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveData();
                Intent intent = new Intent(mActivity, MainMenu.class);
                mActivity.startActivity(intent);
                mActivity.finish();
            }
        });

        builder.show();
    }

}
