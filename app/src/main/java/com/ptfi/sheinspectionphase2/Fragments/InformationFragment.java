package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Models.LookUpModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Splashscreen;
import com.ptfi.sheinspectionphase2.Utils.CSVHelper;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.thinkti.android.filechooser.FileChooser;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class InformationFragment extends Fragment {

    static protected FragmentActivity mActivity;

    private View rootView;

    CardView manpowerCV;

    LinearLayout manpowerData, manpowerDataLL;

    TextView imeiTV, versionTV, manpowerDataTV;

    private Helper.Lookup importType;
    private Helper.Lookup lookUp;

    private static final int FILE_SELECT_CODE = 12332;

    String extStorageDirectory = Environment.getExternalStorageDirectory()
            .getAbsolutePath();
    String basepath = extStorageDirectory + "/" + Constants.ROOT_FOLDER_NAME
            + "/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_information, container, false);
        Helper.visiblePage = Helper.visiblePage.data_management;

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();
        initLayout();

        return rootView;
    }

    private void initComponent() {
        if (rootView != null) {
//            lookUpDataTV = (TextView) rootView.findViewById(R.id.lookUpDataTV);
            manpowerDataTV = (TextView) rootView.findViewById(R.id.manpowerDataTV);

            manpowerDataLL = (LinearLayout) rootView.findViewById(R.id.manpowerDataLL);
//            lookUpDataLL = (LinearLayout) rootView.findViewById(R.id.lookUpDataLL);

            manpowerData = (LinearLayout) rootView.findViewById(R.id.manpowerData);
//            lookUpData = (LinearLayout) rootView.findViewById(R.id.lookUpData);

            manpowerCV = (CardView) rootView.findViewById(R.id.manpowerCV);
//            lookUpCV = (CardView) rootView.findViewById(R.id.lookUpCV);

            manpowerCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (manpowerData.getVisibility() == View.GONE) {
                        manpowerData.setVisibility(View.VISIBLE);
                    } else if (manpowerData.getVisibility() == View.VISIBLE) {
                        manpowerData.setVisibility(View.GONE);
                    }
                }
            });

//            lookUpCV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (lookUpData.getVisibility() == View.GONE) {
//                        lookUpData.setVisibility(View.VISIBLE);
//                    } else if (lookUpData.getVisibility() == View.VISIBLE) {
//                        lookUpData.setVisibility(View.GONE);
//                    }
//                }
//            });

//            exportCSV = (LinearLayout) rootView.findViewById(R.id.exportCSV);
//            exportLookUpCSV = (LinearLayout) rootView.findViewById(R.id.exportLookUpCSV);
//            importCSV = (LinearLayout) rootView.findViewById(R.id.importCSV);
//            importLookUpCSV = (LinearLayout) rootView.findViewById(R.id.importLookUpCSV);

//            exportCSV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    lookUp = Helper.Lookup.newData;
//                    Helper.exportLookup(getActivity(), lookUp);
//                }
//            });

//            exportLookUpCSV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    lookUp = Helper.Lookup.newDataLookUp;
//                    Helper.exportLookup(getActivity(), lookUp);
//                }
//            });

//            importCSV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    importType = importType.newData;
//                    showFileChooser();
//                }
//            });

//            importLookUpCSV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    importType = importType.newDataLookUp;
//                    showFileChooser();
//                }
//            });

        }
    }

    private void initLayout() {
        InitLayoutAsyncTask initLayout = new InitLayoutAsyncTask();
        initLayout.execute();
    }

    public class InitLayoutAsyncTask extends AsyncTask<Void, Integer, Void> {
        File importFolderPath;
//		String importFolderPaths;

        public InitLayoutAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Void... params) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String[] listLookupFileName = new String[]{
                            Constants.MANPOWER_FILE_NAME};

                    TextView[] listTextViewTotalColumn = new TextView[]{
                            manpowerDataTV};

                    LinearLayout[] listTableExampleLookup = new LinearLayout[]{
                            manpowerDataLL};

                    for (int h = 0; h < listLookupFileName.length; h++) {
                        importFolderPath = new File(Constants.EXTERNAL_ROOT_FOLDER + "/"
                                + Constants.IMPORT_FOLDER_NAME + "/"
                                + listLookupFileName[h]);
                        if (!importFolderPath.exists())
                            Helper.copyFileAsset(listLookupFileName[h],
                                    Constants.IMPORT_FOLDER_NAME, mActivity);

                        List<String[]> readResult = CSVHelper
                                .readCSVFromPath(importFolderPath.getAbsolutePath());
                        if (readResult != null) {
                            String[] header = null;
                            String[] example;

                            if (readResult.size() > 0) {
                                header = readResult.get(0);
                            }
                            if (readResult.size() > 1) {
                                example = readResult.get(1);
                            } else {
                                example = new String[header.length];
                            }

                            if (header != null)
                                listTextViewTotalColumn[h].setText("Total Column : "
                                        + String.valueOf(header.length));
                            int px = Math.round(TypedValue.applyDimension(
                                    TypedValue.COMPLEX_UNIT_DIP, 5, mActivity
                                            .getResources().getDisplayMetrics()));

                            listTableExampleLookup[h].removeAllViews();
                            for (int i = 0; i < header.length; i++) {
                                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(
                                        ViewGroup.LayoutParams.WRAP_CONTENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT);

                                LinearLayout headerLayout = new LinearLayout(
                                        mActivity);
                                headerLayout.setLayoutParams(layoutParams);
                                headerLayout.setOrientation(LinearLayout.VERTICAL);
                                LinearLayout.LayoutParams tvParamsWrap = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                LinearLayout.LayoutParams tvParamsMatch = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);

                                LinearLayout contentLayout = new LinearLayout(
                                        mActivity);
                                contentLayout.setLayoutParams(layoutParams);
                                contentLayout.setPadding(px, px, px, px);

                                TextView headerTV = new TextView(mActivity);
                                headerTV.setBackgroundResource(R.drawable.bordered_background_dark_gray);
                                headerTV.setGravity(Gravity.CENTER);
                                headerTV.setPadding(px, px, px, px);
                                headerTV.setText(header[i]);
                                headerTV.setTextColor(getResources().getColor(R.color.White));
                                headerLayout.addView(headerTV);

                                TextView exampleTV = new TextView(mActivity);
                                exampleTV
                                        .setBackgroundResource(R.drawable.transparent_bordered_background_white);
                                exampleTV.setGravity(Gravity.CENTER);
                                exampleTV.setPadding(px, px, px, px);
                                exampleTV.setText(example[i]);

                                if (header[i] != null && example[i] != null) {
                                    if (header[i].length() >= example[i].length()) {
                                        headerTV.setLayoutParams(tvParamsWrap);
                                        exampleTV.setLayoutParams(tvParamsMatch);
                                    } else {
                                        headerTV.setLayoutParams(tvParamsMatch);
                                        exampleTV.setLayoutParams(tvParamsWrap);
                                    }
                                } else {

                                }

                                headerLayout.addView(exampleTV);
                                listTableExampleLookup[h].addView(headerLayout);
                            }
                        }
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();

        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(getActivity(), FileChooser.class);
        ArrayList<String> extensions = new ArrayList<String>();
        extensions.add(".csv");
        intent.putExtra("storagePath", Constants.IMPORT_FOLDER_ON_EXTERNAL_PATH
                + "/" + Constants.IMPORT_FOLDER_NAME);
        intent.putStringArrayListExtra("filterFileExtension", extensions);
        getActivity().startActivityForResult(intent, FILE_SELECT_CODE);
    }

    public class ImportFileAsyncTask extends AsyncTask<Void, Integer, Void> {
        String path;
        String title = "";
        String errorMessage = "";
        Helper.CustomProgressDialog pd;
        boolean statusImport = true;

        public ImportFileAsyncTask(String path) {
            this.path = path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = Helper.getCostumProgressDialog(getActivity());
            pd.getDialog().show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (new File(path).exists()) {
                List<String[]> readResult = CSVHelper.readCSVFromPath(path);
                if (readResult != null) {

                    int skippedData = 0;
                    DataSource dataSource = new DataSource(
                            getActivity());
                    dataSource.open();
                    switch (importType) {
                        case newData:
                            title = "Manpower";
                            pd.setTextTitle("Import Manpower Look Up Data");
                            if (readResult.get(0).length < 2) {
                                statusImport = false;
                                Helper.showPopUpMessage(
                                        getActivity(),
                                        "Error",
                                        "Number of column in csv is not equal with database table, number of column should be 4",
                                        null);
                            } else {
                                File importManpower = new File(basepath + "/"
                                        + Constants.APP_FOLDER_NAME + "/"
                                        + Constants.IMPORT_FOLDER_NAME + "/"
                                        + Constants.MANPOWER_FILE_NAME);
                                dataSource.deleteAllDataObserver();

                                String pathPanel = Environment.getExternalStorageDirectory()
                                        .getAbsolutePath();

                                List<String[]> readResultPanel = CSVHelper
                                        .readCSVFromPath(pathPanel);

                                int i = 0;

                                for (String[] strings : readResultPanel) {
                                    if (i > 0 && strings.length > 1) {
                                        ManpowerModel om = new ManpowerModel();
                                        om.setIdno(strings[1]);
                                        om.setName(strings[0]);
                                        om.setOrg(strings[3]);
                                        om.setTitle(strings[2]);

                                        dataSource.createDataObserver(om);
                                        Log.d("OBS2", om.getName());
                                    }
                                    i++;
                                    importManpower.delete();
                                    Constants.setObserverDictionary(dataSource
                                            .getAllManpowerResources());
                                    dataSource.close();

                                    pd.setTextProgress("Import Manpower data row " + skippedData + " from " + readResult.size() + " row");
                                    pd.setProgress((skippedData / readResult.size()) * 100);
                                }
//                                Fragment_MainActivity.saveChanges(get, false);
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),
                                        Splashscreen.class);
                                startActivity(intent);
                                getActivity().finish();

                            }
                            break;
//
                        case newDataLookUp:
                            title = "Data Look Up";
                            pd.setTextTitle("Import Data Look Up");
                            if (readResult.get(0).length < 2) {
                                statusImport = false;
                                Helper.showPopUpMessage(
                                        getActivity(),
                                        "Error",
                                        "Number of column in csv is not equal with database table, number of column should be 4",
                                        null);
                            } else {
                                File importManpower = new File(basepath + "/"
                                        + Constants.APP_FOLDER_NAME + "/"
                                        + Constants.IMPORT_FOLDER_NAME + "/"
                                        + Constants.LOOKUP_FILE_NAME);
                                dataSource.deleteAllDataLookUp();

                                String pathPanel = Environment.getExternalStorageDirectory()
                                        .getAbsolutePath();

                                List<String[]> readResultPanel = CSVHelper
                                        .readCSVFromPath(pathPanel);

                                int i = 0;

                                for (String[] strings : readResultPanel) {
                                    if (i > 0 && strings.length > 1) {
                                        LookUpModel om = new LookUpModel();
                                        om.setPotential_risk(strings[1]);
                                        om.setRating(strings[0]);

                                        dataSource.createLookUp(om);
                                        Log.d("OBS2", om.getPotential_risk());
                                    }
                                    i++;
                                    importManpower.delete();
                                    Constants.setLookUpDictionary(dataSource
                                            .getAllLookUp());
                                    dataSource.close();

                                    pd.setTextProgress("Import Look Up Data row " + skippedData + " from " + readResult.size() + " row");
                                    pd.setProgress((skippedData / readResult.size()) * 100);
                                }
//                                Fragment_MainActivity.saveChanges(get, false);
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(),
                                        Splashscreen.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                            break;

                    }
                    dataSource.close();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pd.getDialog().dismiss();
            if (statusImport)
                Helper.showPopUpMessage(
                        getActivity(),
                        "INFORMATION",
                        "Import "
                                + title
                                + " successed"
                                + (errorMessage.equalsIgnoreCase("") ? ""
                                : ("\nwith some row cannot import because mine area id is empty at row : \n" + errorMessage)),
                        null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_SELECT_CODE && resultCode == getActivity().RESULT_OK) {
            String fileSelected = data.getStringExtra("fileSelected");
            ImportFileAsyncTask exec = new ImportFileAsyncTask(fileSelected);
            exec.execute();
            Helper.visiblePage = Helper.visiblePage.data_management;
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

}
