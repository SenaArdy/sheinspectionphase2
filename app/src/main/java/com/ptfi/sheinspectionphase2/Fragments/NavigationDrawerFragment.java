package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.ptfi.sheinspectionphase2.Adapter.ExpandableDrawerAdapter;
import com.ptfi.sheinspectionphase2.Models.DrawerItem;
import com.ptfi.sheinspectionphase2.Models.MenuItemModel;
import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by senaardyputra on 1/13/17.
 */

public class NavigationDrawerFragment extends Fragment{

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private static NavigationDrawerCallbacks mCallbacks;

    public ActionBarDrawerToggle getDrawerToggle() {
        return mDrawerToggle;
    }

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private static DrawerLayout mDrawerLayout;
    private static ExpandableListView mDrawerListView;
    private static View mFragmentContainerView;

    private static int mCurrentSelectedPosition = 0;

    private static ExpandableDrawerAdapter drawerAdapter;
    private static ArrayList<DrawerItem> parentDataSet;
    private HashMap<Integer, ArrayList<DrawerItem>> childDataSet;
    private HashMap<Integer, ArrayList<DrawerItem>> secondLevelChildDataSet;
    private static ArrayList<ArrayList<DrawerItem>> previousDrawerItems = new ArrayList<ArrayList<DrawerItem>>();

    private static DrawerItem selectedItem;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of
        // actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mDrawerListView = (ExpandableListView) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView
                .setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                    @Override
                    public boolean onGroupClick(ExpandableListView parent,
                                                View v, int groupPosition, long id) {
                        DrawerItem menu = (DrawerItem) parent
                                .getExpandableListAdapter().getGroup(
                                        groupPosition);

                        int index = parent.getFlatListPosition(ExpandableListView
                                .getPackedPositionForGroup(groupPosition));

                        if (menu.getChild() == DrawerItem.NO_CHILD) {
                            selectItem(index, menu);
                        } else {
                            // mDrawerListView.expandGroup(groupPosition, true);
                        }
                        return false;
                    }
                });

        mDrawerListView
                .setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                    @Override
                    public boolean onChildClick(ExpandableListView parent,
                                                View v, int groupPosition, int childPosition,
                                                long id) {
                        // Toast.makeText(getActivity(),
                        // groupPosition + " : " + childPosition,
                        // Toast.LENGTH_SHORT).show();
                        DrawerItem menu = (DrawerItem) parent
                                .getExpandableListAdapter().getChild(
                                        groupPosition, childPosition);

                        int index = parent
                                .getFlatListPosition(ExpandableListView
                                        .getPackedPositionForChild(
                                                groupPosition, childPosition));

                        selectItem(index, menu);
                        return false;
                    }
                });

        return mDrawerListView;
    }

    public void initMenu(MenuItemModel menu) {
        if (menu != null) {
            parentDataSet = menu.populateRootMenuPlanned(this.getActivity());
            childDataSet = menu.populateChildMenuPlanned();
            secondLevelChildDataSet = menu
                    .populateSecondLevelChildMenuPlanned();

            drawerAdapter = new ExpandableDrawerAdapter(this.getActivity(),
                    parentDataSet, childDataSet, secondLevelChildDataSet);

            mDrawerListView.setAdapter(drawerAdapter);
            mDrawerListView.setItemChecked(0, true);
        }
    }

    public void initMenuHistory(MenuItemModel menu) {
        if (menu != null) {
            parentDataSet = menu.populateRootMenuHistory(this.getActivity());
            childDataSet = menu.populateChildMenuHistory();
            secondLevelChildDataSet = new HashMap<Integer, ArrayList<DrawerItem>>();

            drawerAdapter = new ExpandableDrawerAdapter(this.getActivity(),
                    parentDataSet, childDataSet, secondLevelChildDataSet);

            mDrawerListView.setAdapter(drawerAdapter);
            mDrawerListView.setItemChecked(0, true);
        }
    }

    public void initMenu5M(MenuItemModel menu) {
        if (menu != null) {
            parentDataSet = menu.populateRootMenu5M();
            childDataSet = menu.populateChildMenu5Min();
            secondLevelChildDataSet = menu
                    .populateSecondLevelChildMenuPlanned();

            drawerAdapter = new ExpandableDrawerAdapter(this.getActivity(),
                    parentDataSet, childDataSet, secondLevelChildDataSet);

            mDrawerListView.setAdapter(drawerAdapter);
            mDrawerListView.setItemChecked(0, true);
        }
    }

    public void initMenuInformation(MenuItemModel menu) {
        if (menu != null) {
            parentDataSet = menu.populateRootMenuInformation();
            childDataSet = menu.populateChildMenuInformation();
            secondLevelChildDataSet = new HashMap<Integer, ArrayList<DrawerItem>>();

            drawerAdapter = new ExpandableDrawerAdapter(this.getActivity(),
                    parentDataSet, childDataSet, secondLevelChildDataSet);

            mDrawerListView.setAdapter(drawerAdapter);
            mDrawerListView.setItemChecked(0, true);
        }
    }

    /**
     * Users of this fragment must call this method to set up the navigation
     * drawer interactions.
     *
     * @param fragmentId
     *            The android:id of this fragment in its activity's layout.
     * @param drawerLayout
     *            The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                toolbar, /* nav drawer image to replace 'Up' caret */
                R.string.openDrawer, /* "open drawer" description for accessibility */
                R.string.closeDrawer /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                if (mCallbacks != null && selectedItem != null) {
                    mCallbacks.onMenuSelected(selectedItem);
                }

                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls
                // onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                selectedItem = null;

                if (!isAdded()) {
                    return;
                }
                getActivity().supportInvalidateOptionsMenu(); // calls
                // onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerListView
                .setBackgroundResource(R.drawable.bordered_background_darkgrey);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public static void selectItem(int position, DrawerItem item) {
        mCurrentSelectedPosition = position;

        if (parentDataSet != null) {
            if (item != null) {
                if (item.getType() == DrawerItem.CHILD) {
                    if (mDrawerLayout != null) {
                        mDrawerLayout.closeDrawer(mFragmentContainerView);
                    }

                    if (mDrawerListView != null) {
                        mDrawerListView.setItemChecked(position,
                                item.getMenu() != DrawerItem.EXIT);
                    }

                    selectedItem = item;
                } else if (item.getType() == DrawerItem.BACK_BUTTON) {
                    selectedItem = null;

                    if (previousDrawerItems.size() > 0) {
                        repopulateDrawerItems(previousDrawerItems
                                .get(previousDrawerItems.size() - 1));
                        previousDrawerItems
                                .remove(previousDrawerItems.size() - 1);
                    }
                    if (mDrawerListView != null) {
                        mDrawerListView.setItemChecked(position, false);
                    }
                } else if (item.getType() == DrawerItem.PARENT) {
                    selectedItem = null;

                    if (mDrawerListView != null) {
                        mDrawerListView.setItemChecked(position, false);
                    }

                    previousDrawerItems.add(copyItems(parentDataSet));
                    if (mCallbacks != null) {
                        repopulateDrawerItems(mCallbacks
                                .populateDrawerList(item));
                    }
                }
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    "Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Callbacks interface that all activities using this fragment must
     * implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onMenuSelected(DrawerItem item);

        ArrayList<DrawerItem> populateDrawerList(DrawerItem item);
    }

    private static void repopulateDrawerItems(ArrayList<DrawerItem> list) {
        if (list != null) {
            parentDataSet.removeAll(parentDataSet);
            for (DrawerItem drawerItem : list) {
                parentDataSet.add(drawerItem);
            }
            drawerAdapter.notifyDataSetChanged();
        }
    }

    private static ArrayList<DrawerItem> copyItems(ArrayList<DrawerItem> list) {
        ArrayList<DrawerItem> newList = null;
        if (list != null) {
            newList = new ArrayList<DrawerItem>();
            for (DrawerItem drawerItem : list) {
                if (drawerItem != null) {
                    DrawerItem item = new DrawerItem(drawerItem.getType(),
                            drawerItem.getAncestor(), drawerItem.getMenu(),
                            drawerItem.getTitle(), drawerItem.getParent(),
                            drawerItem.getChild(), drawerItem.getImgResource());
                    newList.add(item);
                } else {
                    newList.add(drawerItem);
                }
            }
        }
        return newList;
    }
}
