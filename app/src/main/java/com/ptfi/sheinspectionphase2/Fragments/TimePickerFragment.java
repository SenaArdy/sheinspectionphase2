package com.ptfi.sheinspectionphase2.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import java.util.Calendar;

@SuppressLint("ValidFragment") 
public class TimePickerFragment extends DialogFragment {
	private FragmentActivity fragmentActivity;
	private OnTimeSetListener onTimeSetListener;
	private int hourOfDay = 12;
	private int minute = 0;
	private boolean is24Hour = true;
	
	public TimePickerFragment(FragmentActivity fragmentActivity, OnTimeSetListener onTimeSetListener, int hourOfDay, int minute, boolean is24Hour){
		this.onTimeSetListener = onTimeSetListener;
		this.fragmentActivity = fragmentActivity;
		this.hourOfDay = hourOfDay;
		this.minute = minute;
		this.is24Hour = is24Hour;
	}
	
	public TimePickerFragment(FragmentActivity fragmentActivity, OnTimeSetListener onTimeSetListener){
		this.onTimeSetListener = onTimeSetListener;
		this.fragmentActivity = fragmentActivity;	
		final Calendar c = Calendar.getInstance();
		hourOfDay = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);		
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Create a new instance of DatePickerDialog and return it
		return new TimePickerDialog(fragmentActivity, onTimeSetListener, hourOfDay, minute, is24Hour);	
	}				
}
