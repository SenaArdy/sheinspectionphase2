package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.CellVerticalAlignment;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.MainMenu;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.Models.InspectorUGModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingInspectionModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingMainModel;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingCallback;
import com.ptfi.sheinspectionphase2.PopUps.FieldFindingsPopUp;
import com.ptfi.sheinspectionphase2.PopUps.GesturePopUp;
import com.ptfi.sheinspectionphase2.PopUps.GesturePopUpCallback;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUp;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUpCallback;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUpUg;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUpUgCallback;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;
import com.ptfi.sheinspectionphase2.Utils.Reporting;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by senaardyputra on 11/14/16.
 */

public class UGFragment extends Fragment {

    static protected FragmentActivity mActivity;
    private static View rootView;
    TextView imeiTV, versionTV;
    ImageView addButton;
    private boolean isEmptySignature = true;
//    private static ButtonRectangle generateBtn, exportBtn, previewBtn, clearBtn;
    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    /* ====================
                    Inspector Fill
    ==================== */
    static EditText dateInsET, projNameET, holeIdET, projOwnerET, areaOwnerET, drillCoorET, drillContET, ugGeoTechET, ugHydroET,
            ugSheET, ugVentilationET, ugGeologyET;

    static TextInputLayout dateInsTL, projNameTL, holeIdTL, projOwnerTL, areaOwnerTL, drillCoorTL, drillContTL, ugGeoTechTL, ugHydroTL,
            ugSheTL, ugVentilationTL,  ugGeologyTL;

    /* ======================
                    Gas Detector
    ======================= */
    static EditText typeET, idET, lastCalibET;
    static TextInputLayout typeTL, idTL, lastCalibTL;
    static Spinner typeSP;

    /* ======================
             Daily Pre-Start Check
    ====================== */
    static MultiStateToggleButton inspPre1, inspPre2;
    static EditText remarkPre1ET, remarkPre2ET;
    static TextInputLayout remarkPre1TL, remarkPre2TL;

    /* ======================
                         Electrical
    ======================*/
    static MultiStateToggleButton inspElec1, inspElec2, inspElec3, inspElec4, inspElec5, inspElec6, inspElec7;
    static EditText remarkElec1ET, remarkElec2ET, remarkElec3ET, remarkElec4ET, remarkElec5ET, remarkElec6ET, remarkElec7ET;
    static TextInputLayout remarkElec1TL, remarkElec2TL, remarkElec3TL, remarkElec4TL, remarkElec5TL, remarkElec6TL, remarkElec7TL;

    /* ======================
                   Safety Equipment
   ====================== =*/
    static MultiStateToggleButton inspSE1, inspSE2, inspSE3, inspSE4, inspSE5, inspSE6, inspSE7, inspSE8, inspSE9, inspSE10;
    static EditText remarkSE1ET, remarkSE2ET, remarkSE3ET, remarkSE4ET, remarkSE5ET, remarkSE6ET, remarkSE7ET, remarkSE8ET,
            remarkSE9ET, remarkSE10ET;
    static TextInputLayout remarkSE1TL, remarkSE2TL, remarkSE3TL, remarkSE4TL, remarkSE5TL, remarkSE6TL, remarkSE7TL,
            remarkSE8TL, remarkSE9TL, remarkSE10TL;

    /* ======================
                           Signs
    ====================== =*/
    static MultiStateToggleButton inspSi1, inspSi2, inspSi3, inspSi3_1, inspSi3_2, inspSi3_3, inspSi3_4;
    static EditText remarkSi1ET, remarkSi2ET, remarkSi3ET, remarkSi3_1ET, remarkSi3_2ET, remarkSi3_3ET, remarkSi3_4ET;
    static TextInputLayout remarkSi1TL, remarkSi2TL, remarkSi3TL, remarkSi3_1TL, remarkSi3_2TL, remarkSi3_3TL, remarkSi3_4TL;

    /* ======================
                        Cleanliness
    ====================== */
    static MultiStateToggleButton inspCl1, inspCl2, inspCl3, inspCl4, inspCl5, inspCl6, inspCl7, inspCl8, inspCl9, inspCl10;
    static EditText remarkCl1ET, remarkCl2ET, remarkCl3ET, remarkCl4ET, remarkCl5ET, remarkCl6ET, remarkCl7ET, remarkCl8ET, remarkCl9ET,
            remarkCl10ET;
    static TextInputLayout remarkCl1TL, remarkCl2TL, remarkCl3TL, remarkCl4TL, remarkCl5TL, remarkCl6TL, remarkCl7TL, remarkCl8TL, remarkCl9TL,
            remarkCl10TL;

    /* ======================
                      Feed Frame
    ====================== */
    static MultiStateToggleButton inspFF1, inspFF2, inspFF3, inspFF4, inspFF5, inspFF6, inspFF7, inspFF8, inspFF9, inspFF10;
    static EditText remarkFF1ET, remarkFF2ET, remarkFF3ET, remarkFF4ET, remarkFF5ET, remarkFF6ET, remarkFF7ET, remarkFF8ET, remarkFF9ET,
            remarkFF10ET;
    static TextInputLayout remarkFF1TL, remarkFF2TL, remarkFF3TL, remarkFF4TL, remarkFF5TL, remarkFF6TL, remarkFF7TL, remarkFF8TL,
            remarkFF9TL, remarkFF10TL;

    /* ======================
                          Control
    ====================== */
    static MultiStateToggleButton inspCnt1, inspCnt2, inspCnt3;
    static EditText remarkCnt1ET, remarkCnt2ET, remarkCnt3ET;
    static TextInputLayout remarkCnt1TL, remarkCnt2TL, remarkCnt3TL;

    /* ======================
                 Wireline Equipment
   ====================== */
    static MultiStateToggleButton inspWE1, inspWE2, inspWE3, inspWE4;
    static EditText remarkWE1ET, remarkWE2ET, remarkWE3ET, remarkWE4ET;
    static TextInputLayout remarkWE1TL, remarkWE2TL, remarkWE3TL, remarkWE4TL;

    /* ======================
                Circulation System
    ====================== */
    static MultiStateToggleButton inspCS1, inspCS2, inspCS3, inspCS4, inspCS5, inspCS6, inspCS7;
    static EditText remarkCS1ET, remarkCS2ET, remarkCS3ET, remarkCS4ET, remarkCS5ET, remarkCS6ET, remarkCS7ET;
    static TextInputLayout remarkCS1TL, remarkCS2TL, remarkCS3TL, remarkCS4TL, remarkCS5TL, remarkCS6TL, remarkCS7TL;

    /* ======================
          Power Pack - Control Panel
    ====================== */
    static MultiStateToggleButton inspCP1, inspCP2, inspCP3, inspCP4, inspCP5, inspCP6, inspCP7, inspCP8, inspCP9;
    static EditText remarkCP1ET, remarkCP2ET, remarkCP3ET, remarkCP4ET, remarkCP5ET, remarkCP6ET, remarkCP7ET, remarkCP8ET, remarkCP9ET;
    static TextInputLayout remarkCP1TL, remarkCP2TL, remarkCP3TL, remarkCP4TL, remarkCP5TL, remarkCP6TL, remarkCP7TL, remarkCP8TL, remarkCP9TL;

    /* ======================
                       Handtools
    ====================== */
    static MultiStateToggleButton inspHT1, inspHT2, inspHT3, inspHT4;
    static EditText remarkHT1ET, remarkHT2ET, remarkHT3ET, remarkHT4ET;
    static TextInputLayout remarkHT1TL, remarkHT2TL, remarkHT3TL, remarkHT4TL;

    /* ======================
                   Work Procedure
    ====================== */
    static MultiStateToggleButton inspWP1, inspWP2, inspWP3, inspWP4, inspWP5;
    static EditText remarkWP1ET, remarkWP2ET, remarkWP3ET, remarkWP4ET, remarkWP5ET;
    static TextInputLayout remarkWP1TL, remarkWP2TL, remarkWP3TL, remarkWP4TL, remarkWP5TL;

    /* ======================
                          Photo
    ====================== */
    static LinearLayout photoLL;
    static RecyclerView photoRV;
    static CardView photoCV;
    static RecyclerView.LayoutManager layoutManager;

    /* ======================
                      Header CV
    ====================== */
    CardView gdCV, dpCV, elecCV, seCV, siCV, clCV, ffCV, cntCV, weCV, csCV, cpCV, htCV, wpCV;
    LinearLayout gdLL, dpLL, elecLL, seLL, siLL, clLL, ffLL, cntLL, weLL, csLL, cpLL, htLL, wpLL;

    private ArrayList<PhotoModel> photoModel;
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();
    PhotoAdapter photoAdapter;

    ArrayList<ManpowerModel> manpowers = new ArrayList<ManpowerModel>();
    ManpowerModel selectedObserver = null;

    ImageView psIV, elIV, seIV, siIV, clIV, ffIV, cntIV, weIV, csIV, ppIV, htIV, wpIV, findingIV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ug, container, false);
        Helper.visiblePage = Helper.visiblePage.ug;

        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        /* =======================
                             Version
        ======================= */
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

         /* =======================
                                Imei
        ======================= */
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();
        headerLine();
        loadData();
        manpowers = Constants.getObserverDictionary();

        return rootView;
    }

    public void initComponent() {
        if(rootView != null) {
            /* ====================
                           Inspector Fill
            ==================== */
            dateInsET = (EditText) rootView.findViewById(R.id.dateInsET);
            projNameET = (EditText) rootView.findViewById(R.id.projNameET);
            holeIdET = (EditText) rootView.findViewById(R.id.holeIdET);
            projOwnerET = (EditText) rootView.findViewById(R.id.projOwnerET);
            areaOwnerET = (EditText) rootView.findViewById(R.id.areaOwnerET);
            drillCoorET = (EditText) rootView.findViewById(R.id.drillingCoorET);
            drillContET = (EditText) rootView.findViewById(R.id.drillingConctET);
            ugGeoTechET = (EditText) rootView.findViewById(R.id.ugGeoTechET);
            ugHydroET = (EditText) rootView.findViewById(R.id.ugHydroET);
            ugSheET = (EditText) rootView.findViewById(R.id.ugSheET);
            ugVentilationET = (EditText) rootView.findViewById(R.id.ugVentilationET);
            ugGeologyET = (EditText) rootView.findViewById(R.id.ugGeologyET);

            dateInsTL = (TextInputLayout) rootView.findViewById(R.id.dateInsTL);
            projNameTL = (TextInputLayout) rootView.findViewById(R.id.projNameTL);
            holeIdTL = (TextInputLayout) rootView.findViewById(R.id.holeIdTL);
            projOwnerTL = (TextInputLayout) rootView.findViewById(R.id.projOwnerTL);
            areaOwnerTL = (TextInputLayout) rootView.findViewById(R.id.areaOwnerTL);
            drillCoorTL = (TextInputLayout) rootView.findViewById(R.id.drillingCoorTL);
            drillContTL = (TextInputLayout) rootView.findViewById(R.id.drillingConctTL);
            ugGeoTechTL = (TextInputLayout) rootView.findViewById(R.id.ugGeoTechTL);
            ugHydroTL = (TextInputLayout) rootView.findViewById(R.id.ugHydroTL);
            ugSheTL = (TextInputLayout) rootView.findViewById(R.id.ugSheTL);
            ugVentilationTL = (TextInputLayout) rootView.findViewById(R.id.ugVentilationTL);
            ugGeologyTL = (TextInputLayout) rootView.findViewById(R.id.ugGeologyTL);

//            generateBtn = (ButtonRectangle) rootView.findViewById(R.id.generateBtn);
////            exportBtn = (ButtonRectangle) rootView.findViewById(R.id.exportBtn);
//            previewBtn = (ButtonRectangle) rootView.findViewById(R.id.previewBtn);
//            clearBtn = (ButtonRectangle) rootView.findViewById(R.id.clearBtn);
//
//            previewBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    saveData();
//                    Reporting.generateReportUG(mActivity, projOwnerET.getText().toString(), projNameET.getText().toString(), holeIdET.getText().toString(),
//                            dateInsET.getText().toString(), false);
//                }
//            });
//
//
//            generateBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    saveData();
//                    Reporting.generateReportUG(mActivity, projOwnerET.getText().toString(), projNameET.getText().toString(), holeIdET.getText().toString(),
//                            dateInsET.getText().toString(), true);
//                }
//            });
//
//            clearBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    clearData();
//                }
//            });

              /* =======================
                                    Photo
            ======================= */
            addButton = (ImageView) rootView.findViewById(R.id.addPhoto);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImagePopUpUg.imagePopUpUg(mActivity, Constants.INSPECTION_UG_DRILLING_PRE, "", "", dateInsET.getText().toString(),
                            projOwnerET.getText().toString(), new ImagePopUpUgCallback() {
                                @Override
                                public void onImageSubmitted() {
                                    recycleView();
                                }
                            });
                }
            });

            areaOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (areaOwnerET.getText().toString().length() == 0) {
                        showInspector("Area Owner", false);
                    } else {
                        showInspector("Area Owner", true);
                    }
                }
            });

            projOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (projOwnerET.getText().toString().length() ==0) {
                        showInspector("Project Owner", false);
                    } else {
                        showInspector("Project Owner", true);
                    }
                }
            });

            ugGeologyET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ugGeologyET.getText().toString().length() ==0) {
                        showInspector("Geology", false);
                    } else {
                        showInspector("Geology", true);
                    }

                }
            });

            ugGeoTechET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ugGeoTechET.getText().toString().length() ==0) {
                        showInspector("GeoTech", false);
                    } else {
                        showInspector("GeoTech", true);
                    }

                }
            });

            ugHydroET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ugHydroET.getText().toString().length() ==0) {
                        showInspector("Hydrology", false);
                    } else {
                        showInspector("Hydrology", true);
                    }

                }
            });

            ugVentilationET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ugVentilationET.getText().toString().length() ==0) {
                        showInspector("Ventilation", false);
                    } else {
                        showInspector("Ventilation", true);
                    }

                }
            });

            ugSheET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ugSheET.getText().toString().length() ==0) {
                        showInspector("UG SHE", false);
                    } else {
                        showInspector("UG SHE", true);
                    }

                }
            });

            drillCoorET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drillCoorET.getText().toString().length() ==0) {
                        showInspector("Drilling Coordinator", false);
                    } else {
                        showInspector("Drilling Coordinator", true);
                    }

                }
            });

            drillContET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drillContET.getText().toString().length() ==0) {
                        showInspector("Drilling Contractor", false);
                    } else {
                        showInspector("Drilling Contractor", true);
                    }

                }
            });

            /* ====================
                           Gas Detector
            ==================== */
            typeET = (EditText) rootView.findViewById(R.id.typeET);
            typeET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    typeSP.performClick();
                }
            });
            idET = (EditText) rootView.findViewById(R.id.idET);
            lastCalibET = (EditText) rootView.findViewById(R.id.lastCalibET);
            typeSP = (Spinner) rootView.findViewById(R.id.typeSP);
            final ArrayList<String> typeAdapter = new ArrayList<String>();
            typeAdapter.add("ITX Multi (6 Sensor Gas)");
            typeAdapter.add("MX-6 Hybrid (6 Sensor Gas + Infra Red)");

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line,
                    typeAdapter);
            typeSP.setAdapter(adapter);

            typeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    typeET.setText(typeSP.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            typeTL = (TextInputLayout) rootView.findViewById(R.id.typeTL);
            idTL = (TextInputLayout) rootView.findViewById(R.id.idTL);
            lastCalibTL = (TextInputLayout) rootView.findViewById(R.id.lastCalibTL);

            /* ====================
                   Daily Pre-Start Check
            ==================== */
            inspPre1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspPre1);
            inspPre2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspPre2);

            remarkPre1ET = (EditText) rootView.findViewById(R.id.remarkPre1ET);
            remarkPre2ET = (EditText) rootView.findViewById(R.id.remarkPre2ET);

            remarkPre1TL = (TextInputLayout) rootView.findViewById(R.id.remarkPre1TL);
            remarkPre2TL = (TextInputLayout) rootView.findViewById(R.id.remarkPre2TL);

            /* ====================
                              Electrical
            ==================== */
            inspElec1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec1);
            inspElec2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec2);
            inspElec3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec3);
            inspElec4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec4);
            inspElec5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec5);
            inspElec6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec6);
            inspElec7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspElec7);

            remarkElec1ET = (EditText) rootView.findViewById(R.id.remarkElec1ET);
            remarkElec2ET = (EditText) rootView.findViewById(R.id.remarkElec2ET);
            remarkElec3ET = (EditText) rootView.findViewById(R.id.remarkElec3ET);
            remarkElec4ET = (EditText) rootView.findViewById(R.id.remarkElec4ET);
            remarkElec5ET = (EditText) rootView.findViewById(R.id.remarkElec5ET);
            remarkElec6ET = (EditText) rootView.findViewById(R.id.remarkElec6ET);
            remarkElec7ET = (EditText) rootView.findViewById(R.id.remarkElec7ET);

            remarkElec1TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec1TL);
            remarkElec2TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec2TL);
            remarkElec3TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec3TL);
            remarkElec4TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec4TL);
            remarkElec5TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec5TL);
            remarkElec6TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec6TL);
            remarkElec7TL = (TextInputLayout) rootView.findViewById(R.id.remarkElec7TL);

             /* ====================
                        Safety Equipment
            ==================== */
            inspSE1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE1);
            inspSE2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE2);
            inspSE3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE3);
            inspSE4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE4);
            inspSE5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE5);
            inspSE6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE6);
            inspSE7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE7);
            inspSE8 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE8);
            inspSE9 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE9);
            inspSE10 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSE10);

            remarkSE1ET = (EditText) rootView.findViewById(R.id.remarkSE1ET);
            remarkSE2ET = (EditText) rootView.findViewById(R.id.remarkSE2ET);
            remarkSE3ET = (EditText) rootView.findViewById(R.id.remarkSE3ET);
            remarkSE4ET = (EditText) rootView.findViewById(R.id.remarkSE4ET);
            remarkSE5ET = (EditText) rootView.findViewById(R.id.remarkSE5ET);
            remarkSE6ET = (EditText) rootView.findViewById(R.id.remarkSE6ET);
            remarkSE7ET = (EditText) rootView.findViewById(R.id.remarkSE7ET);
            remarkSE8ET = (EditText) rootView.findViewById(R.id.remarkSE8ET);
            remarkSE9ET = (EditText) rootView.findViewById(R.id.remarkSE9ET);
            remarkSE10ET = (EditText) rootView.findViewById(R.id.remarkSE10ET);

            remarkSE1TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE1TL);
            remarkSE2TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE2TL);
            remarkSE3TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE3TL);
            remarkSE4TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE4TL);
            remarkSE5TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE5TL);
            remarkSE6TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE6TL);
            remarkSE7TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE7TL);
            remarkSE8TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE8TL);
            remarkSE9TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE9TL);
            remarkSE10TL = (TextInputLayout) rootView.findViewById(R.id.remarkSE10TL);

            /* ====================
                                Signs
            ==================== */
            inspSi1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi1);
            inspSi2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi2);
            inspSi3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi3);
            inspSi3_1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi3_1);
            inspSi3_2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi3_2);
            inspSi3_3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi3_3);
            inspSi3_4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspSi3_4);

            remarkSi1ET = (EditText) rootView.findViewById(R.id.remarkSi1ET);
            remarkSi2ET = (EditText) rootView.findViewById(R.id.remarkSi2ET);
            remarkSi3ET = (EditText) rootView.findViewById(R.id.remarkSi3ET);
            remarkSi3_1ET = (EditText) rootView.findViewById(R.id.remarkSi3_1ET);
            remarkSi3_2ET = (EditText) rootView.findViewById(R.id.remarkSi3_2ET);
            remarkSi3_3ET = (EditText) rootView.findViewById(R.id.remarkSi3_3ET);
            remarkSi3_4ET = (EditText) rootView.findViewById(R.id.remarkSi3_4ET);

            remarkSi1TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi1TL);
            remarkSi2TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi2TL);
            remarkSi3TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi3TL);
            remarkSi3_1TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi3_1TL);
            remarkSi3_2TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi3_2TL);
            remarkSi3_3TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi3_3TL);
            remarkSi3_4TL = (TextInputLayout) rootView.findViewById(R.id.remarkSi3_4TL);

            /* ====================
                            Cleanliness
            ==================== */
            inspCl1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl1);
            inspCl2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl2);
            inspCl3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl3);
            inspCl4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl4);
            inspCl5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl5);
            inspCl6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl6);
            inspCl7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl7);
            inspCl8 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl8);
            inspCl9 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl9);
            inspCl10 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCl10);

            remarkCl1ET = (EditText) rootView.findViewById(R.id.remarkCl1ET);
            remarkCl2ET = (EditText) rootView.findViewById(R.id.remarkCl2ET);
            remarkCl3ET = (EditText) rootView.findViewById(R.id.remarkCl3ET);
            remarkCl4ET = (EditText) rootView.findViewById(R.id.remarkCl4ET);
            remarkCl5ET = (EditText) rootView.findViewById(R.id.remarkCl5ET);
            remarkCl6ET = (EditText) rootView.findViewById(R.id.remarkCl6ET);
            remarkCl7ET = (EditText) rootView.findViewById(R.id.remarkCl7ET);
            remarkCl8ET = (EditText) rootView.findViewById(R.id.remarkCl8ET);
            remarkCl9ET = (EditText) rootView.findViewById(R.id.remarkCl9ET);
            remarkCl10ET = (EditText) rootView.findViewById(R.id.remarkCl10ET);

            remarkCl1TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl1TL);
            remarkCl2TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl2TL);
            remarkCl3TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl3TL);
            remarkCl4TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl4TL);
            remarkCl5TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl5TL);
            remarkCl6TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl6TL);
            remarkCl7TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl7TL);
            remarkCl8TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl8TL);
            remarkCl9TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl9TL);
            remarkCl10TL = (TextInputLayout) rootView.findViewById(R.id.remarkCl10TL);

            /* ====================
                            Feed Frame
            ==================== */
            inspFF1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF1);
            inspFF2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF2);
            inspFF3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF3);
            inspFF4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF4);
            inspFF5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF5);
            inspFF6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF6);
            inspFF7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF7);
            inspFF8 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF8);
            inspFF9 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF9);
            inspFF10 = (MultiStateToggleButton) rootView.findViewById(R.id.inspFF10);

            remarkFF1ET = (EditText) rootView.findViewById(R.id.remarkFF1ET);
            remarkFF2ET = (EditText) rootView.findViewById(R.id.remarkFF2ET);
            remarkFF3ET = (EditText) rootView.findViewById(R.id.remarkFF3ET);
            remarkFF4ET = (EditText) rootView.findViewById(R.id.remarkFF4ET);
            remarkFF5ET = (EditText) rootView.findViewById(R.id.remarkFF5ET);
            remarkFF6ET = (EditText) rootView.findViewById(R.id.remarkFF6ET);
            remarkFF7ET = (EditText) rootView.findViewById(R.id.remarkFF7ET);
            remarkFF8ET = (EditText) rootView.findViewById(R.id.remarkFF8ET);
            remarkFF9ET = (EditText) rootView.findViewById(R.id.remarkFF9ET);
            remarkFF10ET = (EditText) rootView.findViewById(R.id.remarkFF10ET);

            remarkFF1TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF1TL);
            remarkFF2TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF2TL);
            remarkFF3TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF3TL);
            remarkFF4TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF4TL);
            remarkFF5TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF5TL);
            remarkFF6TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF6TL);
            remarkFF7TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF7TL);
            remarkFF8TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF8TL);
            remarkFF9TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF9TL);
            remarkFF10TL = (TextInputLayout) rootView.findViewById(R.id.remarkFF10TL);

            /* ====================
                              Control
            ==================== */
            inspCnt1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCnt1);
            inspCnt2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCnt2);
            inspCnt3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCnt3);

            remarkCnt1ET = (EditText) rootView.findViewById(R.id.remarkCnt1ET);
            remarkCnt2ET = (EditText) rootView.findViewById(R.id.remarkCnt2ET);
            remarkCnt3ET = (EditText) rootView.findViewById(R.id.remarkCnt3ET);

            remarkCnt1TL = (TextInputLayout) rootView.findViewById(R.id.remarkCnt1TL);
            remarkCnt2TL = (TextInputLayout) rootView.findViewById(R.id.remarkCnt2TL);
            remarkCnt3TL = (TextInputLayout) rootView.findViewById(R.id.remarkCnt3TL);

             /* ====================
                       Wireline Equipment
            ==================== */
            inspWE1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWE1);
            inspWE2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWE1);
            inspWE3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWE1);
            inspWE4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWE1);

            remarkWE1ET = (EditText) rootView.findViewById(R.id.remarkWE1ET);
            remarkWE2ET = (EditText) rootView.findViewById(R.id.remarkWE3ET);
            remarkWE3ET = (EditText) rootView.findViewById(R.id.remarkWE3ET);
            remarkWE4ET = (EditText) rootView.findViewById(R.id.remarkWE4ET);

            remarkWE1TL = (TextInputLayout) rootView.findViewById(R.id.remarkWE1TL);
            remarkWE2TL = (TextInputLayout) rootView.findViewById(R.id.remarkWE2TL);
            remarkWE3TL = (TextInputLayout) rootView.findViewById(R.id.remarkWE3TL);
            remarkWE4TL = (TextInputLayout) rootView.findViewById(R.id.remarkWE4TL);

            /* ======================
                        Circulation System
            ====================== */
            inspCS1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS1);
            inspCS2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS2);
            inspCS3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS3);
            inspCS4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS4);
            inspCS5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS5);
            inspCS6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS6);
            inspCS7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCS7);

            remarkCS1ET = (EditText) rootView.findViewById(R.id.remarkCS1ET);
            remarkCS2ET = (EditText) rootView.findViewById(R.id.remarkCS2ET);
            remarkCS3ET = (EditText) rootView.findViewById(R.id.remarkCS3ET);
            remarkCS4ET = (EditText) rootView.findViewById(R.id.remarkCS4ET);
            remarkCS5ET = (EditText) rootView.findViewById(R.id.remarkCS5ET);
            remarkCS6ET = (EditText) rootView.findViewById(R.id.remarkCS6ET);
            remarkCS7ET = (EditText) rootView.findViewById(R.id.remarkCS7ET);

            remarkCS1TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS1TL);
            remarkCS2TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS2TL);
            remarkCS3TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS3TL);
            remarkCS4TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS4TL);
            remarkCS5TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS5TL);
            remarkCS6TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS6TL);
            remarkCS7TL = (TextInputLayout) rootView.findViewById(R.id.remarkCS7TL);

            /* ======================
                   Power Pack - Control Panel
            ====================== */
            inspCP1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP1);
            inspCP2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP2);
            inspCP3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP3);
            inspCP4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP4);
            inspCP5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP5);
            inspCP6 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP6);
            inspCP7 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP7);
            inspCP8 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP8);
            inspCP9 = (MultiStateToggleButton) rootView.findViewById(R.id.inspCP9);

            remarkCP1ET = (EditText) rootView.findViewById(R.id.remarkCP1ET);
            remarkCP2ET = (EditText) rootView.findViewById(R.id.remarkCP2ET);
            remarkCP3ET = (EditText) rootView.findViewById(R.id.remarkCP3ET);
            remarkCP4ET = (EditText) rootView.findViewById(R.id.remarkCP4ET);
            remarkCP5ET = (EditText) rootView.findViewById(R.id.remarkCP5ET);
            remarkCP6ET = (EditText) rootView.findViewById(R.id.remarkCP6ET);
            remarkCP7ET = (EditText) rootView.findViewById(R.id.remarkCP7ET);
            remarkCP8ET = (EditText) rootView.findViewById(R.id.remarkCP8ET);
            remarkCP9ET = (EditText) rootView.findViewById(R.id.remarkCP9ET);

            remarkCP1TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP1TL);
            remarkCP2TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP2TL);
            remarkCP3TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP3TL);
            remarkCP4TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP4TL);
            remarkCP5TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP5TL);
            remarkCP6TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP6TL);
            remarkCP7TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP7TL);
            remarkCP8TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP8TL);
            remarkCP9TL = (TextInputLayout) rootView.findViewById(R.id.remarkCP9TL);

            /* ======================
                                Handtools
            ====================== */
            inspHT1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspHT1);
            inspHT2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspHT2);
            inspHT3 = (MultiStateToggleButton) rootView.findViewById(R.id.inspHT3);
            inspHT4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspHT4);

            remarkHT1ET = (EditText) rootView.findViewById(R.id.remarkHT1ET);
            remarkHT2ET = (EditText) rootView.findViewById(R.id.remarkHT2ET);
            remarkHT3ET = (EditText) rootView.findViewById(R.id.remarkHT3ET);
            remarkHT4ET = (EditText) rootView.findViewById(R.id.remarkHT4ET);

            remarkHT1TL = (TextInputLayout) rootView.findViewById(R.id.remarkHT1TL);
            remarkHT2TL = (TextInputLayout) rootView.findViewById(R.id.remarkHT2TL);
            remarkHT3TL = (TextInputLayout) rootView.findViewById(R.id.remarkHT3TL);
            remarkHT4TL = (TextInputLayout) rootView.findViewById(R.id.remarkHT4TL);

            /* ======================
                           Work Procedure
            ====================== */
            inspWP1 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWP1);
            inspWP2 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWP2);
            inspWP3  = (MultiStateToggleButton) rootView.findViewById(R.id.inspWP3);
            inspWP4 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWP4);
            inspWP5 = (MultiStateToggleButton) rootView.findViewById(R.id.inspWP5);

            remarkWP1ET = (EditText) rootView.findViewById(R.id.remarkWP1ET);
            remarkWP2ET = (EditText) rootView.findViewById(R.id.remarkWP2ET);
            remarkWP3ET = (EditText) rootView.findViewById(R.id.remarkWP3ET);
            remarkWP4ET = (EditText) rootView.findViewById(R.id.remarkWP4ET);
            remarkWP5ET = (EditText) rootView.findViewById(R.id.remarkWP5ET);

            remarkWP1TL = (TextInputLayout) rootView.findViewById(R.id.remarkWP1TL);
            remarkWP2TL = (TextInputLayout) rootView.findViewById(R.id.remarkWP2TL);
            remarkWP3TL = (TextInputLayout) rootView.findViewById(R.id.remarkWP3TL);
            remarkWP4TL = (TextInputLayout) rootView.findViewById(R.id.remarkWP4TL);
            remarkWP5TL = (TextInputLayout) rootView.findViewById(R.id.remarkWP5TL);

            // Date
            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateInsET.setText(DataSingleton.getInstance().getFormattedDate());

        }
    }

        public static boolean validateSaveData() {
            boolean status = true;

            if (dateInsET.getText().toString().length() == 0) {
                status = false;
                dateInsTL.setErrorEnabled(true);
                dateInsTL.setError("Please fill Date Inspection First");
            }

            if (projNameET.getText().toString().length() == 0) {
                status = false;
                projNameTL.setErrorEnabled(true);
                projNameTL.setError("Please fill Project Name First");
            }

            if (holeIdET.getText().toString().length() == 0) {
                status = false;
                holeIdTL.setErrorEnabled(true);
                holeIdTL.setError("Please fill Hole Id First");
            }

            return status;
        }

    public static void datePickers(final Activity MainActivity, final View rootView) {
        switch (rootView.getId()) {
            case R.id.dateInsET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;

            default:
                break;
        }
    }

    public void headerLine() {
        if (rootView != null) {
            gdCV = (CardView) rootView.findViewById(R.id.gdCV);
            dpCV = (CardView) rootView.findViewById(R.id.dpCV);
            elecCV = (CardView) rootView.findViewById(R.id.elecCV);
            seCV = (CardView) rootView.findViewById(R.id.seCV);
            siCV = (CardView) rootView.findViewById(R.id.siCV);
            clCV = (CardView) rootView.findViewById(R.id.clCV);
            ffCV = (CardView) rootView.findViewById(R.id.ffCV);
            cntCV = (CardView) rootView.findViewById(R.id.cntCV);
            weCV = (CardView) rootView.findViewById(R.id.weCV);
            csCV = (CardView) rootView.findViewById(R.id.csCV);
            cpCV = (CardView) rootView.findViewById(R.id.cpCV);
            htCV = (CardView) rootView.findViewById(R.id.htCV);
            wpCV = (CardView) rootView.findViewById(R.id.wpCV);
            photoCV = (CardView) rootView.findViewById(R.id.photoCV);

            gdLL = (LinearLayout) rootView.findViewById(R.id.gdLL);
            dpLL = (LinearLayout) rootView.findViewById(R.id.dpLL);
            elecLL = (LinearLayout) rootView.findViewById(R.id.elecLL);
            seLL = (LinearLayout) rootView.findViewById(R.id.seLL);
            siLL = (LinearLayout) rootView.findViewById(R.id.siLL);
            clLL = (LinearLayout) rootView.findViewById(R.id.clLL);
            ffLL = (LinearLayout) rootView.findViewById(R.id.ffLL);
            cntLL = (LinearLayout) rootView.findViewById(R.id.cntLL);
            weLL = (LinearLayout) rootView.findViewById(R.id.weLL);
            csLL = (LinearLayout) rootView.findViewById(R.id.csLL);
            cpLL = (LinearLayout) rootView.findViewById(R.id.cpLL);
            htLL = (LinearLayout) rootView.findViewById(R.id.htLL);
            wpLL = (LinearLayout) rootView.findViewById(R.id.wpLL);
            photoLL = (LinearLayout) rootView.findViewById(R.id.photoLL);

            psIV = (ImageView) rootView.findViewById(R.id.psIV);
            elIV = (ImageView) rootView.findViewById(R.id.elIV);
            cntIV = (ImageView) rootView.findViewById(R.id.cntIV);
            ppIV = (ImageView) rootView.findViewById(R.id.ppIV);
            weIV = (ImageView) rootView.findViewById(R.id.weIV);
            wpIV = (ImageView) rootView.findViewById(R.id.wpIV);
            seIV = (ImageView) rootView.findViewById(R.id.seIV);
            siIV = (ImageView) rootView.findViewById(R.id.siIV);
            clIV = (ImageView) rootView.findViewById(R.id.clIV);
            ffIV = (ImageView) rootView.findViewById(R.id.ffIV);
            csIV = (ImageView) rootView.findViewById(R.id.csIV);
            htIV = (ImageView) rootView.findViewById(R.id.htIV);
            findingIV = (ImageView) rootView.findViewById(R.id.findingIV);

            gdCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (gdLL.getVisibility() == View.GONE) {
                        gdLL.setVisibility(View.VISIBLE);
                    } else {
                        gdLL.setVisibility(View.GONE);
                    }
                }
            });

            dpCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dpLL.getVisibility() == View.GONE) {
                        dpLL.setVisibility(View.VISIBLE);
                        psIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        dpLL.setVisibility(View.GONE);
                        psIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            seCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (seLL.getVisibility() == View.GONE) {
                        seLL.setVisibility(View.VISIBLE);
                        seIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        seLL.setVisibility(View.GONE);
                        seIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            elecCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (elecLL.getVisibility() == View.GONE) {
                        elecLL.setVisibility(View.VISIBLE);
                        elIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        elecLL.setVisibility(View.GONE);
                        elIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            siCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (siLL.getVisibility() == View.GONE) {
                        siLL.setVisibility(View.VISIBLE);
                        siIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        siLL.setVisibility(View.GONE);
                        siIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            clCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clLL.getVisibility() == View.GONE) {
                        clLL.setVisibility(View.VISIBLE);
                        clIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        clLL.setVisibility(View.GONE);
                        clIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            ffCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ffLL.getVisibility() == View.GONE) {
                        ffLL.setVisibility(View.VISIBLE);
                        ffIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        ffLL.setVisibility(View.GONE);
                        ffIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            cntCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cntLL.getVisibility() == View.GONE) {
                        cntLL.setVisibility(View.VISIBLE);
                        cntIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        cntLL.setVisibility(View.GONE);
                        cntIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            weCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (weLL.getVisibility() == View.GONE) {
                        weLL.setVisibility(View.VISIBLE);
                        weIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        weLL.setVisibility(View.GONE);
                        weIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            csCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (csLL.getVisibility() == View.GONE) {
                        csLL.setVisibility(View.VISIBLE);
                        csIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        csLL.setVisibility(View.GONE);
                        csIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            cpCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (cpLL.getVisibility() == View.GONE) {
                        cpLL.setVisibility(View.VISIBLE);
                        ppIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        cpLL.setVisibility(View.GONE);
                        ppIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });


            photoCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                        findingIV.setImageResource(R.drawable.collapse_logo);
                    } else if (photoLL.getVisibility() == View.VISIBLE) {
                        photoLL.setVisibility(View.GONE);
                        findingIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            htCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (htLL.getVisibility() == View.GONE) {
                        htLL.setVisibility(View.VISIBLE);
                        htIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        htLL.setVisibility(View.GONE);
                        htIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            wpCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (wpLL.getVisibility() == View.GONE) {
                        wpLL.setVisibility(View.VISIBLE);
                        wpIV.setImageResource(R.drawable.collapse_logo);
                    } else {
                        wpLL.setVisibility(View.GONE);
                        wpIV.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

        }
    }

    public void showInspector(final String dept, final boolean isUpdate) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Dialog dialog = new Dialog(mActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pop_up_ug_inspector);
                dialog.setCanceledOnTouchOutside(false);

                final EditText idET, titleET, commentET;
                final AutoCompleteTextView nameET;
                TextInputLayout nameTL, idTL, titleTL, commentTL;
                final LinearLayout signatureLayout = (LinearLayout) dialog
                        .findViewById(R.id.signatureLayout);
                final ImageView signatureImage = (ImageView) dialog
                        .findViewById(R.id.signatureIV);

                ButtonRectangle clearAllBtn, submitBtn, cancelBtn;

                nameET = (AutoCompleteTextView) dialog.findViewById(R.id.nameET);
                idET = (EditText) dialog.findViewById(R.id.idET);
                titleET = (EditText) dialog.findViewById(R.id.titleET);
                commentET = (EditText) dialog.findViewById(R.id.commentET);

                final ArrayList<String> data = new ArrayList<String>();
                for (int i = 0; i < manpowers.size(); i++) {
                    data.add(manpowers.get(i).getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, data);
                nameET.setThreshold(1);
                nameET.setAdapter(adapter);
                nameET.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        String key = (String) nameET.getText().toString();
                        Log.d("WTF", key);
                        int pos = data.indexOf(key);

                        selectedObserver = manpowers.get(pos);
                        nameET.setText(selectedObserver.getName());
                        idET.setText(selectedObserver.getIdno());
                        titleET.setText(selectedObserver.getTitle());
//                        commentET.setText(selectedObserver.getOrg());
                    }
                });

                clearAllBtn = (ButtonRectangle) dialog.findViewById(R.id.clearAllBtn);
                submitBtn = (ButtonRectangle) dialog.findViewById(R.id.submitBtn);
                cancelBtn = (ButtonRectangle) dialog.findViewById(R.id.cancelBtn);

                //signature
                signatureLayout.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        GesturePopUp.show(getActivity(),
                                new GesturePopUpCallback() {

                                    @Override
                                    public void onBitmapSaved(Bitmap bitmap) {
                                        // TODO Auto-generated method stub
                                        if (bitmap != null) {
                                            isEmptySignature = false;
                                            signatureImage.setImageBitmap(bitmap);
                                            ((TextView) dialog
                                                    .findViewById(R.id.signatureWarning))
                                                    .setVisibility(View.GONE);
                                        } else {
                                            isEmptySignature = true;
                                            signatureImage.setImageBitmap(null);
                                            ((TextView) dialog
                                                    .findViewById(R.id.signatureWarning))
                                                    .setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                    }
                });

                if(isUpdate) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();
                    InspectorUGModel model = ds.getInspectorUG(projOwnerET.getText().toString(), projNameET.getText().toString(),
                            dateInsET.getText().toString(), dept);
                    nameET.setText(model.getName());
                    idET.setText(model.getIdMan());
                    commentET.setText(model.getComment());
                    ds.close();
                }

                clearAllBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nameET.setText("");
                        idET.setText("");
                        commentET.setText("");
                        titleET.setText("");
                    }
                });

                submitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        InspectorUGModel model = new InspectorUGModel();
                        model.setProjOwner(projOwnerET.getText().toString());
                        model.setProjName(projNameET.getText().toString());
                        model.setDateIns(dateInsET.getText().toString());
                        model.setDept(dept);
                        model.setName(nameET.getText().toString());
                        model.setIdMan(idET.getText().toString());
                        model.setComment(commentET.getText().toString());
                        model.setSign(((BitmapDrawable) signatureImage
                                .getDrawable()) != null ? ((BitmapDrawable) signatureImage
                                .getDrawable()).getBitmap() : null);

                        ds.insertSignUG(model);

                        if (dept.equals("Geology")) {
                            ugGeologyET.setText(nameET.getText().toString());
                        } else if (dept.equals("Hydrology")) {
                            ugHydroET.setText(nameET.getText().toString());
                        } else if (dept.equals("Ventilation")) {
                            ugVentilationET.setText(nameET.getText().toString());
                        } else if (dept.equals("Drilling Contractor")) {
                            drillContET.setText(nameET.getText().toString());
                        } else if (dept.equals("Drilling Coordinator")) {
                            drillCoorET.setText(nameET.getText().toString());
                        } else if (dept.equals("GeoTech")) {
                            ugGeoTechET.setText(nameET.getText().toString());
                        } else if (dept.equals("UG SHE")) {
                            ugSheET.setText(nameET.getText().toString());
                        } else if (dept.equals("Project Owner")) {
                            projOwnerET.setText(nameET.getText().toString());
                        } else if (dept.equals("Area Owner")) {
                            areaOwnerET.setText(nameET.getText().toString());
                        }

                        dialog.dismiss();
                    }
                });

                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }

    public static boolean validationData() {
        boolean status = true;

        if(dateInsET.getText().toString().length() == 0) {
            status = false;
            dateInsTL.setErrorEnabled(true);
            dateInsTL.setError("Please fill Date Inspection");
        }

        if(projNameET.getText().toString().length() == 0) {
            status = false;
            projNameTL.setErrorEnabled(true);
            projNameTL.setError("Please fill Project Name");
        }

        if(holeIdET.getText().toString().length() == 0) {
            status = false;
            holeIdTL.setErrorEnabled(true);
            holeIdTL.setError("Please fill Hole Id");
        }

        if(projOwnerET.getText().toString().length() == 0) {
            status = false;
            projOwnerTL.setErrorEnabled(true);
            projOwnerTL.setError("Please fill Project Owner");
        }

        if(areaOwnerET.getText().toString().length() == 0) {
            status = false;
            areaOwnerTL.setErrorEnabled(true);
            areaOwnerTL.setError("Please fill Area Owner");
        }

        if(drillCoorET.getText().toString().length() == 0) {
            status = false;
            drillCoorTL.setErrorEnabled(true);
            drillCoorTL.setError("Please fill Drilling Coordinator");
        }

        if(ugGeoTechET.getText().toString().length() == 0) {
            status = false;
            ugGeoTechTL.setErrorEnabled(true);
            ugGeoTechTL.setError("Please fill UG Geo Technical");
        }

        if(ugGeologyET.getText().toString().length() == 0) {
            status = false;
            ugGeologyTL.setErrorEnabled(true);
            ugGeologyTL.setError("Please fill UG Geology");
        }

        if(ugHydroET.getText().toString().length() == 0) {
            status = false;
            ugHydroTL.setErrorEnabled(true);
            ugHydroTL.setError("Please fill UG Hydrology");
        }

        if(ugSheET.getText().toString().length() == 0) {
            status = false;
            ugSheTL.setErrorEnabled(true);
            ugSheTL.setError("Please fill UG SHE Geo Engineering");
        }

        if(ugVentilationET.getText().toString().length() == 0) {
            status = false;
            ugVentilationTL.setErrorEnabled(true);
            ugVentilationTL.setError("Please fill UG Ventilasi");
        }

        if(typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("Please fill Gas Detector Type");
        }

        if(idET.getText().toString().length() == 0) {
            status = false;
            idTL.setErrorEnabled(true);
            idTL.setError("Please fill Gas Detector ID");
        }

        if(lastCalibET.getText().toString().length() == 0) {
            status = false;
            lastCalibTL.setErrorEnabled(true);
            lastCalibTL.setError("Please fill Last Calibration Smoke Detector");
        }

        return status;
    }

    public static void generateClick() {
        saveData();
//        Reporting.generateReportUG(mActivity, projOwnerET.getText().toString(), projNameET.getText().toString(), holeIdET.getText().toString(),
//                            dateInsET.getText().toString(), true);
        GenerateAsync generate = new GenerateAsync();
        generate.execute(true);
    }

    public static void previewClick() {
        saveData();
//        Reporting.generateReportUG(mActivity, projOwnerET.getText().toString(), projNameET.getText().toString(), holeIdET.getText().toString(),
//                dateInsET.getText().toString(), false);
        GenerateAsync generate = new GenerateAsync();
        generate.execute(false);
    }

    static class GenerateAsync extends AsyncTask<Boolean, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            generateReportUG(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();
        }

    }

    static String passCheck = Html.fromHtml("&#10004;").toString();

    static String folderDirectory = "";
    private static String previewFilePath;
    private static boolean isPreview = false;
    private static final int PREVIEW_CODE_SHE_UG = 405;

    public static void generateReportUG(final boolean isGenerate) {

                String templateDocPath = Constants.basepath + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FILE_UG_DRILL_PRE;

                // delete all templates file on template folder
                Helper.deleteAllFilesOnDirectory(Constants.extStorageDirectory
                        + "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME);

                // copy wanted template on to template folder
                Helper.copyFolderAndContent(Constants.extStorageDirectory + "/"
                                + Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        mActivity);

                String templatePath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();
                templatePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FILE_UG_DRILL_PRE;

                try {
                    final Document doc = new Document(templateDocPath);
                    DocumentBuilder builder = new DocumentBuilder(doc);

                    TelephonyManager telephonyManager = (TelephonyManager) mActivity
                            .getSystemService(Context.TELEPHONY_SERVICE);
                    String versionName;
                    try {
                        versionName = mActivity.getPackageManager().getPackageInfo(
                                mActivity.getPackageName(), 0).versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        versionName = "version name cannot be detected";
                    }

                    doc.getRange().replace("_device_id",
                            "device IMEI : " + telephonyManager.getDeviceId(), true,
                            true);
                    doc.getRange().replace("_version_application",
                            "SHE Inspection Application Version : " + versionName,
                            true, true);

                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    UGDrillingMainModel mainModel = ds.getDataUGReport(projOwnerET.getText().toString(), projNameET.getText().toString(), dateInsET.getText().toString(),
                            holeIdET.getText().toString());
                    doc.getRange().replace("_inspection_date", dateInsET.getText().toString(), true, true);
                    doc.getRange().replace("_project_owner", projOwnerET.getText().toString(), true, true);
                    doc.getRange().replace("_project_name", projNameET.getText().toString(), true, true);
                    doc.getRange().replace("_hole_id",  holeIdET.getText().toString(), true, true);
                    if (mainModel != null) {
                        doc.getRange().replace("_area_owner", mainModel.getAreaOwner(), true, true);
                        doc.getRange().replace("_gas_detector_type", mainModel.getTypeGD(), true, true);
                        doc.getRange().replace("_gas_detector_id", mainModel.getIdGD(), true, true);
                        doc.getRange().replace("_gas_detector_last_calibration", mainModel.getLastCalibGD(), true, true);
                    } else {
                        doc.getRange().replace("_area_owner", "", true, true);
                        doc.getRange().replace("_gas_detector_type", "", true, true);
                        doc.getRange().replace("_gas_detector_id", "", true, true);
                        doc.getRange().replace("_gas_detector_last_calibration", "", true, true);
                    }

            /* Daily Pre-Check */
                    UGDrillingInspectionModel inspectModel = ds.getDataPre(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Daily Pre */
                    if (inspectModel != null) {
                        if (inspectModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(3);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (inspectModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(3);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(3);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_pre_1", inspectModel.getRemark1(), true, true);

            /* Inspection 2 Daily Pre */
                        if (inspectModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(4);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (inspectModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(4);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(4);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_pre_2", inspectModel.getRemark2(), true, true);

            /*  Electrical */
                        UGDrillingInspectionModel elecModel = ds.getDataElectrical(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());
            /* Inspection 1 Electrical */
                        if (elecModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(6);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(6);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(6);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_1", elecModel.getRemark1(), true, true);

            /* Inspection 2 Electrical */
                        if (elecModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(7);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(7);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(7);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_2", elecModel.getRemark2(), true, true);

            /* Inspection 3 Electrical */
                        if (elecModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(8);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(8);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(8);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_3", elecModel.getRemark3(), true, true);

            /* Inspection 4 Electrical */
                        if (elecModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(9);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(9);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(9);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_4", elecModel.getRemark4(), true, true);

            /* Inspection 5 Electrical */
                        if (elecModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(10);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(10);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(10);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_5", elecModel.getRemark5(), true, true);

            /* Inspection 6 Electrical */
                        if (elecModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(11);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(11);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(11);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_6", elecModel.getRemark6(), true, true);

            /* Inspection 7 Electrical */
                        if (elecModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(12);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (elecModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(12);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(12);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_elec_7", elecModel.getRemark7(), true, true);

            /*  Safety Equipment  */
                        UGDrillingInspectionModel seModel = ds.getDataSafetyEquipment(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());
            /* Inspection 1 Safety Equipment */
                        if (seModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(14);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(14);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(14);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_1", seModel.getRemark1(), true, true);

            /* Inspection 2 Safety Equipment */
                        if (seModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(15);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(15);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(15);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_2", seModel.getRemark2(), true, true);

            /* Inspection 3 Safety Equipment */
                        if (seModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(16);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(16);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(16);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_3", seModel.getRemark3(), true, true);

            /* Inspection 4 Safety Equipment */
                        if (seModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(17);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(17);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(17);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_4", seModel.getRemark4(), true, true);

            /* Inspection 5 Safety Equipment */
                        if (seModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(18);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(18);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(18);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_5", seModel.getRemark5(), true, true);

             /* Inspection 6 Safety Equipment */
                        if (seModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(19);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(19);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(19);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_6", seModel.getRemark6(), true, true);

            /* Inspection 7 Safety Equipment */
                        if (seModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(20);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(20);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(20);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_7", seModel.getRemark7(), true, true);

            /* Inspection 8 Safety Equipment */
                        if (seModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(21);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection8().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(21);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(21);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_8", seModel.getRemark8(), true, true);

            /* Inspection 9 Safety Equipment */
                        if (seModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(22);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection9().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(22);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(22);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_9", seModel.getRemark9(), true, true);

            /* Inspection 10 Safety Equipment */
                        if (seModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(23);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (seModel.getInspection10().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(23);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(23);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_se_10", seModel.getRemark10(), true, true);

            /* Signs Model */
                        UGDrillingInspectionModel signsModel = ds.getDataSigns(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());
            /* Inspection 1 Signs */
                        if (signsModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(25);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(25);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(25);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_1", signsModel.getRemark1(), true, true);

            /* Inspection 2 Signs */
                        if (signsModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(26);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(26);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(26);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_2", signsModel.getRemark2(), true, true);

            /* Inspection 3 Signs */
                        if (signsModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(27);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(27);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(27);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_3", signsModel.getRemark3(), true, true);

            /* Inspection 3_1 Signs */
                        if (signsModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(28);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(28);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(28);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_3_1", signsModel.getRemark4(), true, true);

            /* Inspection 3_2 Signs */
                        if (signsModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(29);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(29);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(29);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_3_2", signsModel.getRemark5(), true, true);

            /* Inspection 3_3 Signs */
                        if (signsModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(30);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(30);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(30);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_3_3", signsModel.getRemark6(), true, true);

            /* Inspection 3_4 Signs */
                        if (signsModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(31);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (signsModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(31);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(31);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_si_3_4", signsModel.getRemark7(), true, true);

            /* Cleanliness Model */
                        UGDrillingInspectionModel cleanModel = ds.getDataCleanliness(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Cleanliness */
                        if (cleanModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(33);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(33);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(33);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_1", cleanModel.getRemark1(), true, true);

            /* Inspection 2 Cleanliness */
                        if (cleanModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(34);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(34);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(34);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_2", cleanModel.getRemark2(), true, true);

            /* Inspection 3 Cleanliness */
                        if (cleanModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(35);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(35);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(35);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_3", cleanModel.getRemark3(), true, true);

            /* Inspection 4 Cleanliness */
                        if (cleanModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(36);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(36);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(36);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_4", cleanModel.getRemark4(), true, true);

            /* Inspection 5 Cleanliness */
                        if (cleanModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(37);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(37);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(37);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_5", cleanModel.getRemark5(), true, true);

            /* Inspection 6 Cleanliness */
                        if (cleanModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(38);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(38);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(38);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_6", cleanModel.getRemark6(), true, true);

            /* Inspection 7 Cleanliness */
                        if (cleanModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(39);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(39);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(39);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_7", cleanModel.getRemark7(), true, true);

            /* Inspection 8 Cleanliness */
                        if (cleanModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(40);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection8().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(40);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(40);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_8", cleanModel.getRemark8(), true, true);

            /* Inspection 9 Cleanliness */
                        if (cleanModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(41);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection9().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(41);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(41);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_9", cleanModel.getRemark9(), true, true);

            /* Inspection 10 Cleanliness */
                        if (cleanModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(42);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cleanModel.getInspection10().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(42);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(42);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cl_10", cleanModel.getRemark10(), true, true);

            /* Feed Frame Model */
                        UGDrillingInspectionModel feedModel = ds.getDataFeedFrame(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Feed Frame */
                        if (feedModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(44);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(44);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(44);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_1", feedModel.getRemark1(), true, true);

            /* Inspection 2 Feed Frame */
                        if (feedModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(45);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(45);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(45);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_2", feedModel.getRemark2(), true, true);

            /* Inspection 3 Feed Frame */
                        if (feedModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(46);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(46);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(46);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_3", feedModel.getRemark3(), true, true);

            /* Inspection 4 Feed Frame */
                        if (feedModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(47);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(47);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(47);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_4", feedModel.getRemark4(), true, true);

            /* Inspection 5 Feed Frame */
                        if (feedModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(48);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(48);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(48);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_5", feedModel.getRemark5(), true, true);

            /* Inspection 6 Feed Frame */
                        if (feedModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(49);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(49);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(49);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_6", feedModel.getRemark6(), true, true);

            /* Inspection 7 Feed Frame */
                        if (feedModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(50);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(50);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(50);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_7", feedModel.getRemark7(), true, true);

            /* Inspection 8 Feed Frame */
                        if (feedModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(51);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection8().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(51);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(51);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_8", feedModel.getRemark8(), true, true);

            /* Inspection 9 Feed Frame */
                        if (feedModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(52);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection9().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(52);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(52);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_9", feedModel.getRemark9(), true, true);

            /* Inspection 10 Feed Frame */
                        if (feedModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(53);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (feedModel.getInspection10().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(53);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(53);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ff_10", feedModel.getRemark10(), true, true);

            /* Control Model */
                        UGDrillingInspectionModel cntModel = ds.getDataControl(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Control */
                        if (cntModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(55);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cntModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(55);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(55);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cnt_1", cntModel.getRemark1(), true, true);

            /* Inspection 2 Control */
                        if (cntModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(56);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cntModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(56);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(56);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cnt_2", cntModel.getRemark2(), true, true);

            /* Inspection 3 Control */
                        if (cntModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(57);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cntModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(57);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(57);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cnt_3", cntModel.getRemark3(), true, true);

               /* Wire Equipment Model */
                        UGDrillingInspectionModel weModel = ds.getDataWireline(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Wire Equipment */
                        if (weModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(59);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (weModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(59);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(59);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_we_1", weModel.getRemark1(), true, true);

            /* Inspection 2 Wire Equipment */
                        if (weModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(60);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (weModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(60);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(60);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_we_2", weModel.getRemark2(), true, true);

            /* Inspection 3 Wire Equipment */
                        if (weModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(61);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (weModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(61);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(61);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_we_3", weModel.getRemark3(), true, true);

            /* Inspection 4 Wire Equipment */
                        if (weModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(62);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (weModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(62);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(62);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_we_4", weModel.getRemark4(), true, true);

            /* Circulation System Model */
                        UGDrillingInspectionModel csModel = ds.getDataCirculation(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Circulation System */
                        if (csModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(64);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(64);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(64);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_1", csModel.getRemark1(), true, true);

            /* Inspection 2 Circulation System */
                        if (csModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(65);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(65);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(65);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_2", csModel.getRemark2(), true, true);

            /* Inspection 3 Circulation System */
                        if (csModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(66);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(66);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(66);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_3", csModel.getRemark3(), true, true);

            /* Inspection 4 Circulation System */
                        if (csModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(67);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(67);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(67);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_4", csModel.getRemark4(), true, true);

            /* Inspection 5 Circulation System */
                        if (csModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(68);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(68);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(68);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_5", csModel.getRemark5(), true, true);

            /* Inspection 6 Circulation System */
                        if (csModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(69);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(69);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(69);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_6", csModel.getRemark6(), true, true);

            /* Inspection 7 Circulation System */
                        if (csModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(70);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (csModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(70);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(70);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cs_7", csModel.getRemark7(), true, true);

            /* Control Panel Model */
                        UGDrillingInspectionModel cpModel = ds.getDataControlPanel(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Control Panel */
                        if (cpModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(72);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(72);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(72);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_1", cpModel.getRemark1(), true, true);

            /* Inspection 2 Control Panel */
                        if (cpModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(73);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(73);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(73);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_2", cpModel.getRemark2(), true, true);

            /* Inspection 3 Control Panel */
                        if (cpModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(74);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(74);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(74);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_3", cpModel.getRemark3(), true, true);

            /* Inspection 4 Control Panel */
                        if (cpModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(75);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(75);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(75);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_4", cpModel.getRemark4(), true, true);

            /* Inspection 5 Control Panel */
                        if (cpModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(76);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(76);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(76);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_5", cpModel.getRemark5(), true, true);

            /* Inspection 6 Control Panel */
                        if (cpModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(77);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection6().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(77);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(77);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_6", cpModel.getRemark6(), true, true);

            /* Inspection 7 Control Panel */
                        if (cpModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(78);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection7().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(78);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(78);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_7", cpModel.getRemark7(), true, true);

            /* Inspection 8 Control Panel */
                        if (cpModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(79);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection8().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(79);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(79);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_8", cpModel.getRemark8(), true, true);

            /* Inspection 9 Control Panel */
                        if (cpModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(80);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (cpModel.getInspection9().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(80);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(80);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_cp_9", cpModel.getRemark9(), true, true);

            /* Handtools Model */
                        UGDrillingInspectionModel htModel = ds.getDataHandtools(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Handtools */
                        if (htModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(82);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (htModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(82);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(82);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ht_1", htModel.getRemark1(), true, true);

            /* Inspection 2 Handtools */
                        if (htModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(83);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (htModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(83);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(83);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ht_2", htModel.getRemark2(), true, true);

            /* Inspection 3 Handtools */
                        if (htModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(84);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (htModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(84);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(84);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ht_3", htModel.getRemark3(), true, true);

            /* Inspection 4 Handtools */
                        if (htModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(85);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (htModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(85);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(85);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_ht_4", htModel.getRemark4(), true, true);

            /* Work Procedure Model */
                        UGDrillingInspectionModel wpModel = ds.getDataWorkProcedure(projOwnerET.getText().toString(),
                                projNameET.getText().toString(), dateInsET.getText().toString());

            /* Inspection 1 Work Procedure */
                        if (wpModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(87);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (wpModel.getInspection1().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(87);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(87);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_wp_1", wpModel.getRemark1(), true, true);

            /* Inspection 2 Work Procedure */
                        if (wpModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(88);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (wpModel.getInspection2().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(88);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(88);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_wp_2", wpModel.getRemark2(), true, true);

            /* Inspection 3 Work Procedure */
                        if (wpModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(89);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (wpModel.getInspection3().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(89);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(89);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_wp_3", wpModel.getRemark3(), true, true);

            /* Inspection 4 Work Procedure */
                        if (wpModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(90);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (wpModel.getInspection4().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(90);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(90);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_wp_4", wpModel.getRemark4(), true, true);

            /* Inspection 5 Work Procedure */
                        if (wpModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(91);
                            Cell dataCell = row.getCells().get(1);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else if (wpModel.getInspection5().toString().equalsIgnoreCase("No")) {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(91);
                            Cell dataCell = row.getCells().get(2);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        } else {
                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
                            Row row = (Row) table.getRows().get(91);
                            Cell dataCell = row.getCells().get(3);

                            dataCell.getCellFormat().getShading()
                                    .setBackgroundPatternColor(Color.WHITE);
                            dataCell.getCellFormat().setVerticalAlignment(
                                    CellVerticalAlignment.CENTER);
                            dataCell.getFirstParagraph().getRuns().clear();
                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
                        }
                        doc.getRange().replace("Remarks_wp_5", wpModel.getRemark5(), true, true);

                    } else {

                    }

            /* Inspector Table */
            /* Hydrology */
                    InspectorUGModel hydroModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Hydrology");
                    if (hydroModel != null) {
                        doc.getRange().replace("_comment_ug_hydrology", hydroModel.getComment(), true, true);
                        doc.getRange().replace("_name_6_id", hydroModel.getName() + "(" + hydroModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", hydroModel.getDateIns(), true, true);
                        if (hydroModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(11);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(hydroModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_ug_hydrology", "", true, true);
                        doc.getRange().replace("_name_6_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

              /* Geology */
                    InspectorUGModel geoModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Geology");
                    if (geoModel != null) {
                        doc.getRange().replace("_comment_ug_geology", geoModel.getComment(), true, true);
                        doc.getRange().replace("_name_7_id", geoModel.getName() + "(" + geoModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", geoModel.getDateIns(), true, true);
                        if (geoModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(13);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(geoModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_ug_geology", "", true, true);
                        doc.getRange().replace("_name_7_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* GeoTech */
                    InspectorUGModel geoTechModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "GeoTech");
                    if (geoTechModel != null) {
                        doc.getRange().replace("_comment_ug_geotechnical", geoTechModel.getComment(), true, true);
                        doc.getRange().replace("_name_5_id", geoTechModel.getName() + "(" + geoTechModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", geoTechModel.getDateIns(), true, true);
                        if (geoTechModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(9);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(geoTechModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_ug_geotechnical", "", true, true);
                        doc.getRange().replace("_name_5_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* Ventilation */
                    InspectorUGModel venModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Ventilation");
                    if (venModel != null) {
                        doc.getRange().replace("_comment_ug_ventilation", venModel.getComment(), true, true);
                        doc.getRange().replace("_name_9_id", venModel.getName() + "(" + venModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", venModel.getDateIns(), true, true);
                        if (venModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(17);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(venModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_ug_ventilation", "", true, true);
                        doc.getRange().replace("_name_9_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* UG SHE */
                    InspectorUGModel ugSheModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "UG SHE");
                    if (ugSheModel != null) {
                        doc.getRange().replace("_comment_ug_she_geoservices", ugSheModel.getComment(), true, true);
                        doc.getRange().replace("_name_8_id", ugSheModel.getName() + "(" + ugSheModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", ugSheModel.getDateIns(), true, true);
                        if (ugSheModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(15);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(ugSheModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_ug_she_geoservices", "", true, true);
                        doc.getRange().replace("_name_8_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* Drilling Coordinator */
                    InspectorUGModel coorModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Drilling Coordinator");
                    if (coorModel != null) {
                        doc.getRange().replace("_comment_drilling_coordinator", coorModel.getComment(), true, true);
                        doc.getRange().replace("_name_3_id", coorModel.getName() + "(" + coorModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", coorModel.getDateIns(), true, true);
                        if (coorModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(5);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(coorModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_drilling_coordinator", "", true, true);
                        doc.getRange().replace("_name_3_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* Drilling Contractor */
                    InspectorUGModel contModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Drilling Contractor");
                    if (contModel != null) {
                        doc.getRange().replace("_comment_drilling_contractor", contModel.getComment(), true, true);
                        doc.getRange().replace("_name_4_id", contModel.getName() + "(" + contModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", contModel.getDateIns(), true, true);
                        if (contModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(7);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(contModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_drilling_contractor", "", true, true);
                        doc.getRange().replace("_name_4_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* Area Owner */
                    InspectorUGModel aoModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Area Owner");
                    if (aoModel != null) {
                        doc.getRange().replace("_comment_area_owner", aoModel.getComment(), true, true);
                        doc.getRange().replace("_name_1_id", aoModel.getName() + "(" + aoModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", aoModel.getDateIns(), true, true);
                        if (aoModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(1);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(aoModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_area_owner", "", true, true);
                        doc.getRange().replace("_name_1_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }

            /* Project Owner */
                    InspectorUGModel poModel = ds.getInspectorUG(projOwnerET.getText().toString(),
                            projNameET.getText().toString(), dateInsET.getText().toString(), "Project Owner");
                    if (poModel != null) {
                        doc.getRange().replace("_comment_project_drilling_owner", poModel.getComment(), true, true);
                        doc.getRange().replace("_name_2_id", poModel.getName() + "(" + poModel.getIdMan() + ")", true, true);
                        doc.getRange().replace("_inspection_date", poModel.getDateIns(), true, true);
                        if (poModel.getSign() != null) {
                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
                            Row row1 = (Row) table1.getRows().get(3);
                            Cell dataCell1 = row1.getCells().get(4);
                            CellFormat format = dataCell1.getCellFormat();
                            double width = format.getWidth();
                            builder.moveTo(dataCell1.getFirstParagraph());
                            Shape image = builder.insertImage(poModel
                                    .getSign());

                            double freePageWidth = width;

                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.d("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }
                    } else {
                        doc.getRange().replace("_comment_project_drilling_owner", "", true, true);
                        doc.getRange().replace("_name_2_id", "", true, true);
                        doc.getRange().replace("_inspection_date", "", true, true);
                    }


            /* Photo Table */
                    Table photoTableDoc = (Table) doc.getChild(NodeType.TABLE, 3, true);
                    RowCollection photoTableRows = photoTableDoc.getRows();
                    int photoRowStartingIndex = 2;
                    int photoRowDummyIndex = 1;
                    int photoColStartingIndex = 0;
                    int photoRowCount = 0;

                    ArrayList<PhotoModel> fotoModel = ds.getPhotoData(Constants.INSPECTION_UG_DRILLING_PRE, dateInsET.getText().toString(),
                            projOwnerET.getText().toString());
                    ds.close();

                    int count = 0;
                    Row dataRow;
                    Row clonedRow;
                    clonedRow = (Row) photoTableRows.get(photoRowDummyIndex);
                    for (int a = 0; a < fotoModel.size(); a++) {
                        dataRow = (Row) clonedRow.deepClone(true);
                        photoTableDoc.getRows().add(dataRow);
                        count++;

                        dataRow.getRowFormat().setAllowBreakAcrossPages(true);

                        // get the cell for each attribute of foto (number, photo, and
                        // comment)
                        Cell photoNumberCell = dataRow.getCells().get(
                                photoColStartingIndex + 0);
                        Cell photoPhotoCell = dataRow.getCells().get(
                                photoColStartingIndex + 1);
                        Cell photoCommentCell = dataRow.getCells().get(
                                photoColStartingIndex + 2);

                        // set each cell background to white
                        photoNumberCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoPhotoCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoCommentCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);

                        // clear each cell
                        photoNumberCell.getFirstParagraph().getRuns().clear();
                        photoPhotoCell.getFirstParagraph().getRuns().clear();
                        photoCommentCell.getFirstParagraph().getRuns().clear();

                        // fill the data to each cell
                        // number data
                        photoNumberCell.getFirstParagraph().appendChild(
                                new Run(doc, String.valueOf(count)));

                        // photo
                        CellFormat format = photoPhotoCell.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(photoPhotoCell.getFirstParagraph());
                        // Bitmap imgBitmap =
                        // Helper.getBitmapFromFile(foto.getFotoPath());
                        Bitmap imgBitmap = null;

                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(fotoModel.get(a).getFotoPath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 90,
                                            (int) width);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 180,
                                            (int) width);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 0,
                                            (int) width);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (imgBitmap != null) {
                            // Shape image = builder.insertImage(imgBitmap);
                            ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                            imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                                    Constants.COMPRESS_PERCENTAGE, tempImage);
                            Bitmap compressedBitmap = BitmapFactory
                                    .decodeStream(new ByteArrayInputStream(tempImage
                                            .toByteArray()));
                            Shape image = builder.insertImage(compressedBitmap);

                            double freePageWidth = width;
                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData().getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth / size.getWidthPoints();

                                Log.e("IMAGE", "widthlonger : " + "" + " | ratio : "
                                        + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }

                        // comment
                        photoCommentCell.getFirstParagraph().appendChild(
                                new Run(doc, fotoModel.get(a).getComment()));

                        photoRowCount++;
                    }

                    Row dummyRow = photoTableRows.get(photoRowDummyIndex);
                    dummyRow.remove();

//
//            Row dummyRow = photoTableRows.get(photoRowDummyIndex);
//            dummyRow.remove();

//            Table tableComment = (Table) doc.getChild(NodeType.TABLE, 4, true);
//            RowCollection rows = tableComment.getRows();
//
//            insertImage(rows.get(1).getCells().get(4), builder,
//                    listSignature[1], doc);
//            insertImage(rows.get(3).getCells().get(4), builder,
//                    listSignature[0], doc);
//            insertImage(rows.get(5).getCells().get(4), builder,
//                    listSignature[2], doc);
//            insertImage(rows.get(7).getCells().get(4), builder,
//                    listSignature[3], doc);
//            insertImage(rows.get(9).getCells().get(4), builder,
//                    listSignature[4], doc);
//            insertImage(rows.get(11).getCells().get(4), builder,
//                    listSignature[5], doc);
//            insertImage(rows.get(13).getCells().get(4), builder,
//                    listSignature[6], doc);
//            insertImage(rows.get(15).getCells().get(4), builder,
//                    listSignature[7], doc);
//            insertImage(rows.get(17).getCells().get(4), builder,
//                    listSignature[8], doc);

                    // CREATING FOLDER (DIRECTORY)
//            if (isGenerate) {
//                // GENERATE FILE NAME FORMAT
//                // String formattedID =
//                // formatIDInspector(selectedPIC.getObsId());
//                String currentTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm")
//                        .format(new Date());
//                String baseFolderName = currentTime + "_"
//                        + Constants.UGD_PRE_FOLDER_MARK_NAME;
//                // + "_"
//                // + formattedID;
//
//                String fileName = Constants.UGD_PRE_REPORT_FILE_NAME;
//
//                String thisReportPath = baseReportPath + "/" + baseFolderName;
//                Helper.checkAndCreateDirectory(thisReportPath);
//                folderDirectory = thisReportPath;
//
//                doc.save(thisReportPath + "/" + fileName
//                        + Constants.FILE_TYPE_DOCX);
//                doc.save(thisReportPath + "/" + fileName
//                        + Constants.FILE_TYPE_PDF);
//
//                // copy photos to folderDirectory
////                for (FotoModel foto : fotoList) {
////                    if (null != foto.getFotoPath()) {
////                        InputStream is;
////                        try {
////                            // trying to copy image to folderDirectory based on
////                            // its path
////                            is = new FileInputStream(foto.getFotoPath());
////
////                            File fileImage = new File(foto.getFotoPath());
////                            if (fileImage.exists()) {
////                                String photoDirectory = folderDirectory + "/"
////                                        + Constants.PHOTO_FOLDER_NAME;
////                                new File(photoDirectory).mkdirs();
////                                String outputPath = photoDirectory
////                                        + File.separator + foto.getId()
////                                        + ".jpg";
////                                OutputStream os = new FileOutputStream(
////                                        outputPath);
////                                Helper.copyFile(is, os);
////                                // foto.setFotoPath(outputPath);
////                            }
////
////                        } catch (FileNotFoundException e) {
////                            // TODO Auto-generated catch block
//////                            Log.e("Error create photo folder", e.getMessage());
////                            e.printStackTrace();
////                        } catch (IOException e) {
////                            // TODO Auto-generated catch block
////                            e.printStackTrace();
//////                            Log.e("Error create photo folder", e.getMessage());
////                        }
////                    }
////                }
//                Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME + "/");
//                return Constants.REPORT_GENERATED;
//            } else {
//                // preview
//                String previewFolder = Helper.getPathForPreviewFile();
//                String fileNamePdf = Constants.UGD_PRE_PREVIEW_FILE_NAME
//                        + Constants.FILE_TYPE_PDF;
//                Helper.checkAndCreateDirectory(previewFolder);
//                previewFilePath = previewFolder + "/" + fileNamePdf;
//                doc.save(previewFilePath);
//                Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME + "/");
//
//                return Constants.REPORT_PREVIEW;
//            }

                    String fullPath = "";
                    String currentTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm")
                            .format(new Date());
                    String fileName = currentTime + "_UG_" + Constants.UGD_PRE_FOLDER_MARK_NAME;
                    if (isGenerate) {
                        fullPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                                + "/"
                                + Constants.ROOT_FOLDER_NAME
                                + "/"
                                + Constants.APP_FOLDER_NAME
                                + "/"
                                + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
                    }

                    if (isGenerate) {
                        doc.save(fullPath + ".doc");
                        folderDirectory = fullPath;
                        fullPath += "/" + fileName;
                        Helper.showPopUpMessage(mActivity, "Success",
                                "Report has been successfully generated in "
                                        + folderDirectory, null);
                    } else {
                        String previewFolder = Helper.getPathForPreviewFile();
                        String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
                                + Constants.FILE_TYPE_PDF;
                        Helper.checkAndCreateDirectory(previewFolder);
                        previewFilePath = previewFolder + "/" + fileNamePdf;
                        doc.save(previewFilePath);

                        File file = new File(previewFilePath);
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(Intent.ACTION_VIEW);
                        String type = "application/pdf";
                        intent.setDataAndType(Uri.fromFile(file), type);
                        mActivity.startActivityForResult(intent,
                                PREVIEW_CODE_SHE_UG);
                        isPreview = true;
                    }

                } catch (Exception ex) {
//            Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
//                    + Constants.APP_FOLDER_NAME + "/"
//                    + Constants.TEMPLATE_FOLDER_NAME + "/");
//            return Constants.REPORT_FAILED;
                }

    }

    public void loadData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        /* =====================
                     UG Driliing Main
        ===================== */
        UGDrillingMainModel mainModel = ds.getLastestUGDrilling();

        dateInsET.setText(mainModel.getDateIns());
        projNameET.setText(mainModel.getProjectName());
        holeIdET.setText(mainModel.getHoleId());
        projOwnerET.setText(mainModel.getProjectOwner());
        areaOwnerET.setText(mainModel.getAreaOwner());
        drillCoorET.setText(mainModel.getDrillingCoor());
        drillContET.setText(mainModel.getDrillingCont());
        ugGeoTechET.setText(mainModel.getUgGeoTech());
        ugGeologyET.setText(mainModel.getUgGeo());
        ugHydroET.setText(mainModel.getUgHydro());
        ugSheET.setText(mainModel.getUgShe());
        ugVentilationET.setText(mainModel.getUgVentilation());
        typeET.setText(mainModel.getTypeGD());
        idET.setText(mainModel.getIdGD());
        lastCalibET.setText(mainModel.getLastCalibGD());

         /* =====================
               UG Daily Pre-Start Check
        ===================== */
        UGDrillingInspectionModel dailyPre = ds.getDataPre(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyPre != null) {
            if (dailyPre.getInspection1().equals("Yes")) {
                inspPre1.setValue(0);
            } else if (dailyPre.getInspection1().equals("No")) {
                inspPre1.setValue(1);
            } else {
                inspPre1.setValue(2);
            }
            remarkPre1ET.setText(dailyPre.getRemark1());

            if (dailyPre.getInspection2().equals("Yes")) {
                inspPre2.setValue(0);
            } else if (dailyPre.getInspection2().equals("No")) {
                inspPre2.setValue(1);
            } else {
                inspPre2.setValue(2);
            }
            remarkPre2ET.setText(dailyPre.getRemark2());
        }

         /* =====================
                        UG Electrical
        ===================== */
        UGDrillingInspectionModel dailyElec = ds.getDataElectrical(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyElec != null) {
            if (dailyElec.getInspection1().equals("Yes")) {
                inspElec1.setValue(0);
            } else if (dailyElec.getInspection1().equals("No")) {
                inspElec1.setValue(1);
            } else {
                inspElec1.setValue(2);
            }
            remarkElec1ET.setText(dailyElec.getRemark1());

            if (dailyElec.getInspection2().equals("Yes")) {
                inspElec2.setValue(0);
            } else if (dailyElec.getInspection2().equals("No")) {
                inspElec2.setValue(1);
            } else {
                inspElec2.setValue(2);
            }
            remarkElec2ET.setText(dailyElec.getRemark2());

            if (dailyElec.getInspection3().equals("Yes")) {
                inspElec3.setValue(0);
            } else if (dailyElec.getInspection3().equals("No")) {
                inspElec3.setValue(1);
            } else {
                inspElec3.setValue(2);
            }
            remarkElec3ET.setText(dailyElec.getRemark3());

            if (dailyElec.getInspection4().equals("Yes")) {
                inspElec4.setValue(0);
            } else if (dailyElec.getInspection4().equals("No")) {
                inspElec4.setValue(1);
            } else {
                inspElec4.setValue(2);
            }
            remarkElec4ET.setText(dailyElec.getRemark4());

            if (dailyElec.getInspection5().equals("Yes")) {
                inspElec5.setValue(0);
            } else if (dailyElec.getInspection5().equals("No")) {
                inspElec5.setValue(1);
            } else {
                inspElec5.setValue(2);
            }
            remarkElec5ET.setText(dailyElec.getRemark5());

            if (dailyElec.getInspection6().equals("Yes")) {
                inspElec6.setValue(0);
            } else if (dailyElec.getInspection6().equals("No")) {
                inspElec6.setValue(1);
            } else {
                inspElec6.setValue(2);
            }
            remarkElec6ET.setText(dailyElec.getRemark6());

            if (dailyElec.getInspection7().equals("Yes")) {
                inspElec7.setValue(0);
            } else if (dailyElec.getInspection7().equals("No")) {
                inspElec7.setValue(1);
            } else {
                inspElec7.setValue(2);
            }
            remarkElec7ET.setText(dailyElec.getRemark7());
        }

        /* =====================
                 UG Safety Equipment
        ===================== */
        UGDrillingInspectionModel dailySE = ds.getDataSafetyEquipment(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailySE != null) {
            if (dailySE.getInspection1().equals("Yes")) {
                inspSE1.setValue(0);
            } else if (dailySE.getInspection1().equals("No")) {
                inspSE1.setValue(1);
            } else {
                inspSE1.setValue(2);
            }
            remarkSE1ET.setText(dailySE.getRemark1());

            if (dailySE.getInspection2().equals("Yes")) {
                inspSE2.setValue(0);
            } else if (dailySE.getInspection2().equals("No")) {
                inspSE2.setValue(1);
            } else {
                inspSE2.setValue(2);
            }
            remarkSE2ET.setText(dailySE.getRemark2());

            if (dailySE.getInspection3().equals("Yes")) {
                inspSE3.setValue(0);
            } else if (dailySE.getInspection3().equals("No")) {
                inspSE3.setValue(1);
            } else {
                inspSE3.setValue(2);
            }
            remarkSE3ET.setText(dailySE.getRemark3());

            if (dailySE.getInspection4().equals("Yes")) {
                inspSE4.setValue(0);
            } else if (dailySE.getInspection4().equals("No")) {
                inspSE4.setValue(1);
            } else {
                inspSE4.setValue(2);
            }
            remarkSE4ET.setText(dailySE.getRemark4());

            if (dailySE.getInspection5().equals("Yes")) {
                inspSE5.setValue(0);
            } else if (dailySE.getInspection5().equals("No")) {
                inspSE5.setValue(1);
            } else {
                inspSE5.setValue(2);
            }
            remarkSE5ET.setText(dailySE.getRemark5());

            if (dailySE.getInspection6().equals("Yes")) {
                inspSE6.setValue(0);
            } else if (dailySE.getInspection6().equals("No")) {
                inspSE6.setValue(1);
            } else {
                inspSE6.setValue(2);
            }
            remarkSE6ET.setText(dailySE.getRemark6());

            if (dailySE.getInspection7().equals("Yes")) {
                inspSE7.setValue(0);
            } else if (dailySE.getInspection7().equals("No")) {
                inspSE7.setValue(1);
            } else {
                inspSE7.setValue(2);
            }
            remarkSE7ET.setText(dailySE.getRemark7());

            if (dailySE.getInspection8().equals("Yes")) {
                inspSE8.setValue(0);
            } else if (dailySE.getInspection8().equals("No")) {
                inspSE8.setValue(1);
            } else {
                inspSE8.setValue(2);
            }
            remarkSE8ET.setText(dailySE.getRemark8());

            if (dailySE.getInspection9().equals("Yes")) {
                inspSE9.setValue(0);
            } else if (dailySE.getInspection9().equals("No")) {
                inspSE9.setValue(1);
            } else {
                inspSE9.setValue(2);
            }
            remarkSE9ET.setText(dailySE.getRemark9());

            if (dailySE.getInspection10().equals("Yes")) {
                inspSE10.setValue(0);
            } else if (dailySE.getInspection10().equals("No")) {
                inspSE10.setValue(1);
            } else {
                inspSE10.setValue(2);
            }
            remarkSE10ET.setText(dailySE.getRemark10());
        }

        /* =====================
                            UG Signs
        ===================== */
        UGDrillingInspectionModel dailySI = ds.getDataSigns(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailySI != null) {
            if (dailySI.getInspection1().equals("Yes")) {
                inspSi1.setValue(0);
            } else if (dailySI.getInspection1().equals("No")) {
                inspSi1.setValue(1);
            } else {
                inspSi1.setValue(2);
            }
            remarkSi1ET.setText(dailySI.getRemark1());

            if (dailySI.getInspection2().equals("Yes")) {
                inspSi2.setValue(0);
            } else if (dailySI.getInspection2().equals("No")) {
                inspSi2.setValue(1);
            } else {
                inspSi2.setValue(2);
            }
            remarkSi2ET.setText(dailySI.getRemark2());

            if (dailySI.getInspection3().equals("Yes")) {
                inspSi3.setValue(0);
            } else if (dailySI.getInspection3().equals("No")) {
                inspSi3.setValue(1);
            } else {
                inspSi3.setValue(2);
            }
            remarkSi3ET.setText(dailySI.getRemark3());

            if (dailySI.getInspection4().equals("Yes")) {
                inspSi3_1.setValue(0);
            } else if (dailySI.getInspection4().equals("No")) {
                inspSi3_1.setValue(1);
            } else {
                inspSi3_1.setValue(2);
            }
            remarkSi3_1ET.setText(dailySI.getRemark4());

            if (dailySI.getInspection5().equals("Yes")) {
                inspSi3_2.setValue(0);
            } else if (dailySI.getInspection5().equals("No")) {
                inspSi3_2.setValue(1);
            } else {
                inspSi3_2.setValue(2);
            }
            remarkSi3_2ET.setText(dailySI.getRemark5());

            if (dailySI.getInspection6().equals("Yes")) {
                inspSi3_3.setValue(0);
            } else if (dailySI.getInspection6().equals("No")) {
                inspSi3_3.setValue(1);
            } else {
                inspSi3_3.setValue(2);
            }
            remarkSi3_3ET.setText(dailySI.getRemark6());

            if (dailySI.getInspection7().equals("Yes")) {
                inspSi3_4.setValue(0);
            } else if (dailySI.getInspection7().equals("No")) {
                inspSi3_4.setValue(1);
            } else {
                inspSi3_4.setValue(2);
            }
            remarkSi3_4ET.setText(dailySI.getRemark7());
        }

        /* =====================
                       UG Cleanliness
        ===================== */
        UGDrillingInspectionModel dailyCL = ds.getDataCleanliness(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyCL != null) {
            if (dailyCL.getInspection1().equals("Yes")) {
                inspCl1.setValue(0);
            } else if (dailyCL.getInspection1().equals("No")) {
                inspCl1.setValue(1);
            } else {
                inspCl1.setValue(2);
            }
            remarkCl1ET.setText(dailyCL.getRemark1());

            if (dailyCL.getInspection2().equals("Yes")) {
                inspCl2.setValue(0);
            } else if (dailyCL.getInspection2().equals("No")) {
                inspCl2.setValue(1);
            } else {
                inspCl2.setValue(2);
            }
            remarkCl2ET.setText(dailyCL.getRemark2());

            if (dailyCL.getInspection3().equals("Yes")) {
                inspCl3.setValue(0);
            } else if (dailyCL.getInspection3().equals("No")) {
                inspCl3.setValue(1);
            } else {
                inspCl3.setValue(2);
            }
            remarkCl3ET.setText(dailyCL.getRemark3());

            if (dailyCL.getInspection4().equals("Yes")) {
                inspCl4.setValue(0);
            } else if (dailyCL.getInspection4().equals("No")) {
                inspCl4.setValue(1);
            } else {
                inspCl4.setValue(2);
            }
            remarkCl4ET.setText(dailyCL.getRemark4());

            if (dailyCL.getInspection5().equals("Yes")) {
                inspCl5.setValue(0);
            } else if (dailyCL.getInspection5().equals("No")) {
                inspCl5.setValue(1);
            } else {
                inspCl5.setValue(2);
            }
            remarkCl5ET.setText(dailyCL.getRemark5());

            if (dailyCL.getInspection6().equals("Yes")) {
                inspCl6.setValue(0);
            } else if (dailyCL.getInspection6().equals("No")) {
                inspCl6.setValue(1);
            } else {
                inspCl6.setValue(2);
            }
            remarkCl6ET.setText(dailyCL.getRemark6());

            if (dailyCL.getInspection7().equals("Yes")) {
                inspCl7.setValue(0);
            } else if (dailyCL.getInspection7().equals("No")) {
                inspCl7.setValue(1);
            } else {
                inspCl7.setValue(2);
            }
            remarkCl7ET.setText(dailyCL.getRemark7());

            if (dailyCL.getInspection8().equals("Yes")) {
                inspCl8.setValue(0);
            } else if (dailyCL.getInspection8().equals("No")) {
                inspCl8.setValue(1);
            } else {
                inspCl8.setValue(2);
            }
            remarkCl8ET.setText(dailyCL.getRemark8());

            if (dailyCL.getInspection9().equals("Yes")) {
                inspCl9.setValue(0);
            } else if (dailyCL.getInspection9().equals("No")) {
                inspCl9.setValue(1);
            } else {
                inspCl9.setValue(2);
            }
            remarkCl9ET.setText(dailyCL.getRemark9());

            if (dailyCL.getInspection10().equals("Yes")) {
                inspCl10.setValue(0);
            } else if (dailyCL.getInspection10().equals("No")) {
                inspCl10.setValue(1);
            } else {
                inspCl10.setValue(2);
            }
            remarkCl10ET.setText(dailyCL.getRemark10());
        }

        /* =====================
                      UG Feed Frame
        ===================== */
        UGDrillingInspectionModel dailyFF = ds.getDataFeedFrame(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyFF != null) {
            if (dailyFF.getInspection1().equals("Yes")) {
                inspFF1.setValue(0);
            } else if (dailyFF.getInspection1().equals("No")) {
                inspFF1.setValue(1);
            } else {
                inspFF1.setValue(2);
            }
            remarkFF1ET.setText(dailyFF.getRemark1());

            if (dailyFF.getInspection2().equals("Yes")) {
                inspFF2.setValue(0);
            } else if (dailyFF.getInspection2().equals("No")) {
                inspFF2.setValue(1);
            } else {
                inspFF2.setValue(2);
            }
            remarkFF2ET.setText(dailyFF.getRemark2());

            if (dailyFF.getInspection3().equals("Yes")) {
                inspFF3.setValue(0);
            } else if (dailyFF.getInspection3().equals("No")) {
                inspFF3.setValue(1);
            } else {
                inspFF3.setValue(2);
            }
            remarkFF3ET.setText(dailyFF.getRemark3());

            if (dailyFF.getInspection4().equals("Yes")) {
                inspFF4.setValue(0);
            } else if (dailyFF.getInspection4().equals("No")) {
                inspFF4.setValue(1);
            } else {
                inspFF4.setValue(2);
            }
            remarkFF4ET.setText(dailyFF.getRemark4());

            if (dailyFF.getInspection5().equals("Yes")) {
                inspFF5.setValue(0);
            } else if (dailyFF.getInspection5().equals("No")) {
                inspFF5.setValue(1);
            } else {
                inspFF5.setValue(2);
            }
            remarkFF5ET.setText(dailyFF.getRemark5());

            if (dailyFF.getInspection6().equals("Yes")) {
                inspFF6.setValue(0);
            } else if (dailyFF.getInspection6().equals("No")) {
                inspFF6.setValue(1);
            } else {
                inspFF6.setValue(2);
            }
            remarkFF6ET.setText(dailyFF.getRemark6());

            if (dailyFF.getInspection7().equals("Yes")) {
                inspFF7.setValue(0);
            } else if (dailyFF.getInspection7().equals("No")) {
                inspFF7.setValue(1);
            } else {
                inspFF7.setValue(2);
            }
            remarkFF7ET.setText(dailyFF.getRemark7());

            if (dailyFF.getInspection8().equals("Yes")) {
                inspFF8.setValue(0);
            } else if (dailyFF.getInspection8().equals("No")) {
                inspFF8.setValue(1);
            } else {
                inspFF8.setValue(2);
            }
            remarkFF8ET.setText(dailyFF.getRemark8());

            if (dailyFF.getInspection9().equals("Yes")) {
                inspFF9.setValue(0);
            } else if (dailyFF.getInspection9().equals("No")) {
                inspFF9.setValue(1);
            } else {
                inspFF9.setValue(2);
            }
            remarkFF9ET.setText(dailyFF.getRemark9());

            if (dailyFF.getInspection10().equals("Yes")) {
                inspFF10.setValue(0);
            } else if (dailyFF.getInspection10().equals("No")) {
                inspFF10.setValue(1);
            } else {
                inspFF10.setValue(2);
            }
            remarkFF10ET.setText(dailyFF.getRemark10());
        }

        /* =====================
                        UG Control
        ===================== */
        UGDrillingInspectionModel dailyCnt = ds.getDataControl(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyCnt != null) {
            if (dailyCnt.getInspection1().equals("Yes")) {
                inspCnt1.setValue(0);
            } else if (dailyCnt.getInspection1().equals("No")) {
                inspCnt1.setValue(1);
            } else {
                inspCnt1.setValue(2);
            }
            remarkCnt1ET.setText(dailyCnt.getRemark1());

            if (dailyCnt.getInspection2().equals("Yes")) {
                inspCnt2.setValue(0);
            } else if (dailyCnt.getInspection2().equals("No")) {
                inspCnt2.setValue(1);
            } else {
                inspCnt2.setValue(2);
            }
            remarkCnt2ET.setText(dailyCnt.getRemark2());

            if (dailyCnt.getInspection3().equals("Yes")) {
                inspCnt3.setValue(0);
            } else if (dailyCnt.getInspection3().equals("No")) {
                inspCnt3.setValue(1);
            } else {
                inspCnt3.setValue(2);
            }
            remarkCnt3ET.setText(dailyCnt.getRemark3());

        /* =====================
                 UG Wireline Equipment
        ===================== */
            UGDrillingInspectionModel dailyWE = ds.getDataWireline(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

            if (dailyWE.getInspection1().equals("Yes")) {
                inspWE1.setValue(0);
            } else if (dailyWE.getInspection1().equals("No")) {
                inspWE1.setValue(1);
            } else {
                inspWE1.setValue(2);
            }
            remarkWE1ET.setText(dailyWE.getRemark1());

            if (dailyWE.getInspection2().equals("Yes")) {
                inspWE2.setValue(0);
            } else if (dailyWE.getInspection2().equals("No")) {
                inspWE2.setValue(1);
            } else {
                inspWE2.setValue(2);
            }
            remarkWE2ET.setText(dailyWE.getRemark2());

            if (dailyWE.getInspection3().equals("Yes")) {
                inspWE3.setValue(0);
            } else if (dailyWE.getInspection3().equals("No")) {
                inspWE3.setValue(1);
            } else {
                inspWE3.setValue(2);
            }
            remarkWE3ET.setText(dailyWE.getRemark3());

            if (dailyWE.getInspection4().equals("Yes")) {
                inspWE4.setValue(0);
            } else if (dailyWE.getInspection4().equals("No")) {
                inspWE4.setValue(1);
            } else {
                inspWE4.setValue(2);
            }
            remarkWE4ET.setText(dailyWE.getRemark4());
        }

        /* =====================
                 UG Circulation System
        ===================== */
        UGDrillingInspectionModel dailyCS = ds.getDataCirculation(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyCS != null) {
            if (dailyCS.getInspection1().equals("Yes")) {
                inspCS1.setValue(0);
            } else if (dailyCS.getInspection1().equals("No")) {
                inspCS1.setValue(1);
            } else {
                inspCS1.setValue(2);
            }
            remarkCS1ET.setText(dailyCS.getRemark1());

            if (dailyCS.getInspection2().equals("Yes")) {
                inspCS2.setValue(0);
            } else if (dailyCS.getInspection2().equals("No")) {
                inspCS2.setValue(1);
            } else {
                inspCS2.setValue(2);
            }
            remarkCS2ET.setText(dailyCS.getRemark2());

            if (dailyCS.getInspection3().equals("Yes")) {
                inspCS3.setValue(0);
            } else if (dailyCS.getInspection3().equals("No")) {
                inspCS3.setValue(1);
            } else {
                inspCS3.setValue(2);
            }
            remarkCS3ET.setText(dailyCS.getRemark3());

            if (dailyCS.getInspection4().equals("Yes")) {
                inspCS4.setValue(0);
            } else if (dailyCS.getInspection4().equals("No")) {
                inspCS4.setValue(1);
            } else {
                inspCS4.setValue(2);
            }
            remarkCS4ET.setText(dailyCS.getRemark4());

            if (dailyCS.getInspection5().equals("Yes")) {
                inspCS5.setValue(0);
            } else if (dailyCS.getInspection5().equals("No")) {
                inspCS5.setValue(1);
            } else {
                inspCS5.setValue(2);
            }
            remarkCS5ET.setText(dailyCS.getRemark5());

            if (dailyCS.getInspection6().equals("Yes")) {
                inspCS6.setValue(0);
            } else if (dailyCS.getInspection6().equals("No")) {
                inspCS6.setValue(1);
            } else {
                inspCS6.setValue(2);
            }
            remarkCS6ET.setText(dailyCS.getRemark6());

            if (dailyCS.getInspection7().equals("Yes")) {
                inspCS7.setValue(0);
            } else if (dailyCS.getInspection7().equals("No")) {
                inspCS7.setValue(1);
            } else {
                inspCS7.setValue(2);
            }
            remarkCS7ET.setText(dailyCS.getRemark7());
        }

        /* =====================
                    UG Control Panel
        ===================== */
        UGDrillingInspectionModel dailyCP = ds.getDataControlPanel(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyCP != null) {
            if (dailyCP.getInspection1().equals("Yes")) {
                inspCP1.setValue(0);
            } else if (dailyCP.getInspection1().equals("No")) {
                inspCP1.setValue(1);
            } else {
                inspCP1.setValue(2);
            }
            remarkCP1ET.setText(dailyCP.getRemark1());

            if (dailyCP.getInspection2().equals("Yes")) {
                inspCP2.setValue(0);
            } else if (dailyCP.getInspection2().equals("No")) {
                inspCP2.setValue(1);
            } else {
                inspCP2.setValue(2);
            }
            remarkCP2ET.setText(dailyCP.getRemark2());

            if (dailyCP.getInspection3().equals("Yes")) {
                inspCP3.setValue(0);
            } else if (dailyCP.getInspection3().equals("No")) {
                inspCP3.setValue(1);
            } else {
                inspCP3.setValue(2);
            }
            remarkCP3ET.setText(dailyCP.getRemark3());

            if (dailyCP.getInspection4().equals("Yes")) {
                inspCP4.setValue(0);
            } else if (dailyCP.getInspection4().equals("No")) {
                inspCP4.setValue(1);
            } else {
                inspCP4.setValue(2);
            }
            remarkCP4ET.setText(dailyCP.getRemark4());

            if (dailyCP.getInspection5().equals("Yes")) {
                inspCP5.setValue(0);
            } else if (dailyCP.getInspection5().equals("No")) {
                inspCP5.setValue(1);
            } else {
                inspCP5.setValue(2);
            }
            remarkCP5ET.setText(dailyCP.getRemark5());

            if (dailyCP.getInspection6().equals("Yes")) {
                inspCP6.setValue(0);
            } else if (dailyCP.getInspection6().equals("No")) {
                inspCP6.setValue(1);
            } else {
                inspCP6.setValue(2);
            }
            remarkCP6ET.setText(dailyCP.getRemark6());

            if (dailyCP.getInspection7().equals("Yes")) {
                inspCP7.setValue(0);
            } else if (dailyCP.getInspection7().equals("No")) {
                inspCP7.setValue(1);
            } else {
                inspCP7.setValue(2);
            }
            remarkCP7ET.setText(dailyCP.getRemark7());

            if (dailyCP.getInspection8().equals("Yes")) {
                inspCP8.setValue(0);
            } else if (dailyCP.getInspection8().equals("No")) {
                inspCP8.setValue(1);
            } else {
                inspCP8.setValue(2);
            }
            remarkCP8ET.setText(dailyCP.getRemark8());

            if (dailyCP.getInspection9().equals("Yes")) {
                inspCP9.setValue(0);
            } else if (dailyCP.getInspection9().equals("No")) {
                inspCP9.setValue(1);
            } else {
                inspCP9.setValue(2);
            }
            remarkCP9ET.setText(dailyCP.getRemark9());
        }

        /* =====================
                       UG Handtools
        ===================== */
        UGDrillingInspectionModel dailyHT = ds.getDataHandtools(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyHT != null) {
            if (dailyHT.getInspection1().equals("Yes")) {
                inspHT1.setValue(0);
            } else if (dailyHT.getInspection1().equals("No")) {
                inspHT1.setValue(1);
            } else {
                inspHT1.setValue(2);
            }
            remarkHT1ET.setText(dailyHT.getRemark1());

            if (dailyHT.getInspection2().equals("Yes")) {
                inspHT2.setValue(0);
            } else if (dailyHT.getInspection2().equals("No")) {
                inspHT2.setValue(1);
            } else {
                inspHT2.setValue(2);
            }
            remarkHT2ET.setText(dailyHT.getRemark2());

            if (dailyHT.getInspection3().equals("Yes")) {
                inspHT3.setValue(0);
            } else if (dailyHT.getInspection3().equals("No")) {
                inspHT3.setValue(1);
            } else {
                inspHT3.setValue(2);
            }
            remarkHT3ET.setText(dailyHT.getRemark3());

            if (dailyHT.getInspection4().equals("Yes")) {
                inspHT4.setValue(0);
            } else if (dailyHT.getInspection4().equals("No")) {
                inspHT4.setValue(1);
            } else {
                inspHT4.setValue(2);
            }
            remarkHT4ET.setText(dailyHT.getRemark4());
        }

        /* =====================
                   UG Work Procedure
        ===================== */
        UGDrillingInspectionModel dailyWP = ds.getDataWorkProcedure(mainModel.getProjectOwner(), mainModel.getProjectName(), mainModel.getDateIns());

        if (dailyWP != null) {
            if (dailyWP.getInspection1().equals("Yes")) {
                inspWP1.setValue(0);
            } else if (dailyWP.getInspection1().equals("No")) {
                inspWP1.setValue(1);
            } else {
                inspWP1.setValue(2);
            }
            remarkWP1ET.setText(dailyWP.getRemark1());

            if (dailyWP.getInspection2().equals("Yes")) {
                inspWP2.setValue(0);
            } else if (dailyWP.getInspection2().equals("No")) {
                inspWP2.setValue(1);
            } else {
                inspWP2.setValue(2);
            }
            remarkWP2ET.setText(dailyWP.getRemark2());

            if (dailyWP.getInspection3().equals("Yes")) {
                inspWP3.setValue(0);
            } else if (dailyWP.getInspection3().equals("No")) {
                inspWP3.setValue(1);
            } else {
                inspWP3.setValue(2);
            }
            remarkWP3ET.setText(dailyWP.getRemark3());

            if (dailyWP.getInspection4().equals("Yes")) {
                inspWP4.setValue(0);
            } else if (dailyWP.getInspection4().equals("No")) {
                inspWP4.setValue(1);
            } else {
                inspWP4.setValue(2);
            }
            remarkWP4ET.setText(dailyWP.getRemark4());

            if (dailyWP.getInspection5().equals("Yes")) {
                inspWP5.setValue(0);
            } else if (dailyWP.getInspection5().equals("No")) {
                inspWP5.setValue(1);
            } else {
                inspWP5.setValue(2);
            }
            remarkWP5ET.setText(dailyWP.getRemark5());

            recycleView();
            ds.close();

        }

//        recycleView(mainModel.getDateIns(), mainModel.getProjectOwner());
//        recycleView();
    }

    public static class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<PhotoModel> photoModel;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public final ImageView pictureIV;
            public TextView commentTV;
            public ImageView deleteIcon;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
                this.commentTV = (TextView) itemView.findViewById(R.id.commentTV);
                this.deleteIcon = (ImageView) itemView.findViewById(R.id.deleteIcon);

                listCV.setOnClickListener(this);
                deleteIcon.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (photoModel != null) {
                    int position = getAdapterPosition();
                    final String fotoPath = photoModel.get(position).getFotoPath();
                    final String comments = photoModel.get(position).getComment();

                    switch (v.getId()) {
                        case R.id.listCV:
                            ImagePopUpUg.imagePopUpUg(mActivity, Constants.INSPECTION_UG_DRILLING_PRE, fotoPath, comments, dateInsET.getText().toString(),
                                    projOwnerET.getText().toString(), new ImagePopUpUgCallback() {
                                        @Override
                                        public void onImageSubmitted() {
                                            recycleView();
                                        }
                                    });
                            break;

                        case R.id.deleteIcon:
                            final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
                            builder.setIcon(R.drawable.warning_logo);
                            builder.setTitle("Warning");
                            builder.setMessage("Are you sure to delete this photo?");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();
                                    ds.deletePhoto(fotoPath, comments, dateInsET.getText().toString(),
                                            projOwnerET.getText().toString(), Constants.INSPECTION_UG_DRILLING_PRE);
                                    ds.close();
                                    recycleView();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            builder.show();
                            break;

                        default:
                            break;
                    }
                }
            }
        }


        public PhotoAdapter(Activity mActivity, ArrayList<PhotoModel> photoModel) {
            super();
            this.mActivity = mActivity;
            this.photoModel = photoModel;
        }

        @Override
        public PhotoAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_photo, parent, false);
            PhotoAdapter.dataViewHolder dataViewHolder = new PhotoAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(PhotoAdapter.dataViewHolder holder, int position) {
            TextView commentTV = holder.commentTV;
            ImageView pictureIV = holder.pictureIV;

            commentTV.setText(photoModel.get(position).getComment());
            if (photoModel.get(position).getFotoPath().equals("")) {
                pictureIV.setImageResource(R.drawable.default_nopicture);
            } else {
                Helper.setPic(pictureIV, photoModel.get(position).getFotoPath());
            }
        }

        @Override
        public int getItemCount() {
            return photoModel.size();
        }
    }

    public static void recycleView() {
        photoRV = (RecyclerView) rootView.findViewById(R.id.photoRV);
        photoRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(mActivity);
        photoRV.setLayoutManager(layoutManager);
        photoRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(mActivity);
        ds.open();
        photoData = ds.getPhotoData(Constants.INSPECTION_UG_DRILLING_PRE,
                dateInsET.getText().toString(), projOwnerET.getText().toString());
        ds.close();

        UGFragment.PhotoAdapter photoAdapter= new UGFragment.PhotoAdapter(mActivity, photoData);
        photoRV.setAdapter(photoAdapter);
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        /* ====================
                        Main Inspector
        ==================== */
        UGDrillingMainModel mainModel = new UGDrillingMainModel();
        mainModel.setDateIns(dateInsET.getText().toString());
        mainModel.setProjectName(projNameET.getText().toString());
        mainModel.setProjectOwner(projOwnerET.getText().toString());
        mainModel.setHoleId(holeIdET.getText().toString());
        mainModel.setAreaOwner(areaOwnerET.getText().toString());
        mainModel.setDrillingCoor(drillCoorET.getText().toString());
        mainModel.setDrillingCont(drillContET.getText().toString());
        mainModel.setUgGeoTech(ugGeoTechET.getText().toString());
        mainModel.setUgGeo(ugGeologyET.getText().toString());
        mainModel.setUgHydro(ugHydroET.getText().toString());
        mainModel.setUgShe(ugSheET.getText().toString());
        mainModel.setUgVentilation(ugVentilationET.getText().toString());
        mainModel.setTypeGD(typeET.getText().toString());
        mainModel.setIdGD(idET.getText().toString());
        mainModel.setLastCalibGD(lastCalibET.getText().toString());

        ds.insertUGDrillMain(mainModel);

          /* ====================
               UG Daily Pre-Start Check
        ==================== */
        UGDrillingInspectionModel dailyPre = new UGDrillingInspectionModel();
        dailyPre.setProjectName(projNameET.getText().toString());
        dailyPre.setProjectOwner(projOwnerET.getText().toString());
        dailyPre.setDateIns(dateInsET.getText().toString());

        if (inspPre1.getValue() == 0) {
            dailyPre.setInspection1("Yes");
        } else if (inspPre1.getValue() == 1) {
            dailyPre.setInspection1("No");
        } else {
            dailyPre.setInspection1("N/A");
        }
        dailyPre.setRemark1(remarkPre1ET.getText().toString());

        if (inspPre2.getValue() == 0) {
            dailyPre.setInspection2("Yes");
        } else if (inspPre2.getValue() == 1) {
            dailyPre.setInspection2("No");
        } else {
            dailyPre.setInspection2("N/A");
        }
        dailyPre.setRemark2(remarkPre1ET.getText().toString());

        ds.insertUGDailyPre(dailyPre);

         /* ====================
                        UG Electrical
        ==================== */
        UGDrillingInspectionModel dailyElec = new UGDrillingInspectionModel();
        dailyElec.setProjectName(projNameET.getText().toString());
        dailyElec.setProjectOwner(projOwnerET.getText().toString());
        dailyElec.setDateIns(dateInsET.getText().toString());

        if (inspElec1.getValue() == 0) {
            dailyElec.setInspection1("Yes");
        } else if (inspElec1.getValue() == 1) {
            dailyElec.setInspection1("No");
        } else {
            dailyElec.setInspection1("N/A");
        }
        dailyElec.setRemark1(remarkElec1ET.getText().toString());

        if (inspElec2.getValue() == 0) {
            dailyElec.setInspection2("Yes");
        } else if (inspElec2.getValue() == 1) {
            dailyElec.setInspection2("No");
        } else {
            dailyElec.setInspection2("N/A");
        }
        dailyElec.setRemark2(remarkElec2ET.getText().toString());

        if (inspElec3.getValue() == 0) {
            dailyElec.setInspection3("Yes");
        } else if (inspElec3.getValue() == 1) {
            dailyElec.setInspection3("No");
        } else {
            dailyElec.setInspection3("N/A");
        }
        dailyElec.setRemark3(remarkElec3ET.getText().toString());

        if (inspElec4.getValue() == 0) {
            dailyElec.setInspection4("Yes");
        } else if (inspElec4.getValue() == 1) {
            dailyElec.setInspection4("No");
        } else {
            dailyElec.setInspection4("N/A");
        }
        dailyElec.setRemark4(remarkElec4ET.getText().toString());

        if (inspElec5.getValue() == 0) {
            dailyElec.setInspection5("Yes");
        } else if (inspElec5.getValue() == 1) {
            dailyElec.setInspection5("No");
        } else {
            dailyElec.setInspection5("N/A");
        }
        dailyElec.setRemark5(remarkElec5ET.getText().toString());

        if (inspElec6.getValue() == 0) {
            dailyElec.setInspection6("Yes");
        } else if (inspElec6.getValue() == 1) {
            dailyElec.setInspection6("No");
        } else {
            dailyElec.setInspection6("N/A");
        }
        dailyElec.setRemark6(remarkElec6ET.getText().toString());

        if (inspElec7.getValue() == 0) {
            dailyElec.setInspection7("Yes");
        } else if (inspElec7.getValue() == 1) {
            dailyElec.setInspection7("No");
        } else {
            dailyElec.setInspection7("N/A");
        }
        dailyElec.setRemark7(remarkElec7ET.getText().toString());

        ds.insertUGElectrical(dailyElec);

        /* ====================
                UG Safety Equipment
        ==================== */
        UGDrillingInspectionModel dailySE = new UGDrillingInspectionModel();
        dailySE.setProjectName(projNameET.getText().toString());
        dailySE.setProjectOwner(projOwnerET.getText().toString());
        dailySE.setDateIns(dateInsET.getText().toString());

        if (inspSE1.getValue() == 0) {
            dailySE.setInspection1("Yes");
        } else if (inspSE1.getValue() == 1) {
            dailySE.setInspection1("No");
        } else {
            dailySE.setInspection1("N/A");
        }
        dailySE.setRemark1(remarkSE1ET.getText().toString());

        if (inspSE2.getValue() == 0) {
            dailySE.setInspection2("Yes");
        } else if (inspSE2.getValue() == 1) {
            dailySE.setInspection2("No");
        } else {
            dailySE.setInspection2("N/A");
        }
        dailySE.setRemark2(remarkSE2ET.getText().toString());

        if (inspSE3.getValue() == 0) {
            dailySE.setInspection3("Yes");
        } else if (inspSE3.getValue() == 1) {
            dailySE.setInspection3("No");
        } else {
            dailySE.setInspection3("N/A");
        }
        dailySE.setRemark3(remarkSE3ET.getText().toString());

        if (inspSE4.getValue() == 0) {
            dailySE.setInspection4("Yes");
        } else if (inspSE4.getValue() == 1) {
            dailySE.setInspection4("No");
        } else {
            dailySE.setInspection4("N/A");
        }
        dailySE.setRemark4(remarkSE4ET.getText().toString());

        if (inspSE5.getValue() == 0) {
            dailySE.setInspection5("Yes");
        } else if (inspSE5.getValue() == 1) {
            dailySE.setInspection5("No");
        } else {
            dailySE.setInspection5("N/A");
        }
        dailySE.setRemark5(remarkSE5ET.getText().toString());

        if (inspSE6.getValue() == 0) {
            dailySE.setInspection6("Yes");
        } else if (inspSE6.getValue() == 1) {
            dailySE.setInspection6("No");
        } else {
            dailySE.setInspection6("N/A");
        }
        dailySE.setRemark6(remarkSE6ET.getText().toString());

        if (inspSE7.getValue() == 0) {
            dailySE.setInspection7("Yes");
        } else if (inspSE7.getValue() == 1) {
            dailySE.setInspection7("No");
        } else {
            dailySE.setInspection7("N/A");
        }
        dailySE.setRemark7(remarkSE7ET.getText().toString());

        if (inspSE8.getValue() == 0) {
            dailySE.setInspection8("Yes");
        } else if (inspSE8.getValue() == 1) {
            dailySE.setInspection8("No");
        } else {
            dailySE.setInspection8("N/A");
        }
        dailySE.setRemark8(remarkSE8ET.getText().toString());

        if (inspSE9.getValue() == 0) {
            dailySE.setInspection9("Yes");
        } else if (inspSE9.getValue() == 1) {
            dailySE.setInspection9("No");
        } else {
            dailySE.setInspection9("N/A");
        }
        dailySE.setRemark9(remarkSE9ET.getText().toString());

        if (inspSE10.getValue() == 0) {
            dailySE.setInspection10("Yes");
        } else if (inspSE10.getValue() == 1) {
            dailySE.setInspection10("No");
        } else {
            dailySE.setInspection10("N/A");
        }
        dailySE.setRemark10(remarkSE10ET.getText().toString());

        ds.insertUGSafertyEquip(dailySE);

        /* ====================
                          UG Signs
        ==================== */
        UGDrillingInspectionModel dailySigns = new UGDrillingInspectionModel();
        dailySigns.setProjectName(projNameET.getText().toString());
        dailySigns.setProjectOwner(projOwnerET.getText().toString());
        dailySigns.setDateIns(dateInsET.getText().toString());

        if (inspSi1.getValue() == 0) {
            dailySigns.setInspection1("Yes");
        } else if (inspSi1.getValue() == 1) {
            dailySigns.setInspection1("No");
        } else {
            dailySigns.setInspection1("N/A");
        }
        dailySigns.setRemark1(remarkSi1ET.getText().toString());

        if (inspSi2.getValue() == 0) {
            dailySigns.setInspection2("Yes");
        } else if (inspSi2.getValue() == 1) {
            dailySigns.setInspection2("No");
        } else {
            dailySigns.setInspection2("N/A");
        }
        dailySigns.setRemark2(remarkSi2ET.getText().toString());

        if (inspSi3.getValue() == 0) {
            dailySigns.setInspection3("Yes");
        } else if (inspSi3.getValue() == 1) {
            dailySigns.setInspection3("No");
        } else {
            dailySigns.setInspection3("N/A");
        }
        dailySigns.setRemark3(remarkSi3ET.getText().toString());

        if (inspSi3_1.getValue() == 0) {
            dailySigns.setInspection4("Yes");
        } else if (inspSi3_1.getValue() == 1_1) {
            dailySigns.setInspection4("No");
        } else {
            dailySigns.setInspection4("N/A");
        }
        dailySigns.setRemark4(remarkSi3_1ET.getText().toString());

        if (inspSi3_2.getValue() == 0) {
            dailySigns.setInspection5("Yes");
        } else if (inspSi3_2.getValue() == 1_2) {
            dailySigns.setInspection5("No");
        } else {
            dailySigns.setInspection5("N/A");
        }
        dailySigns.setRemark5(remarkSi3_2ET.getText().toString());

        if (inspSi3_3.getValue() == 0) {
            dailySigns.setInspection6("Yes");
        } else if (inspSi3_3.getValue() == 1_3) {
            dailySigns.setInspection6("No");
        } else {
            dailySigns.setInspection6("N/A");
        }
        dailySigns.setRemark6(remarkSi3_3ET.getText().toString());

        if (inspSi3_4.getValue() == 0) {
            dailySigns.setInspection7("Yes");
        } else if (inspSi3_4.getValue() == 1_4) {
            dailySigns.setInspection7("No");
        } else {
            dailySigns.setInspection7("N/A");
        }
        dailySigns.setRemark7(remarkSi3_4ET.getText().toString());

        ds.insertUGSigns(dailySigns);

        /* ====================
                       UG Cleanliness
        ==================== */
        UGDrillingInspectionModel dailyCL = new UGDrillingInspectionModel();
        dailyCL.setProjectName(projNameET.getText().toString());
        dailyCL.setProjectOwner(projOwnerET.getText().toString());
        dailyCL.setDateIns(dateInsET.getText().toString());

        if (inspCl1.getValue() == 0) {
            dailyCL.setInspection1("Yes");
        } else if (inspCl1.getValue() == 1) {
            dailyCL.setInspection1("No");
        } else {
            dailyCL.setInspection1("N/A");
        }
        dailyCL.setRemark1(remarkCl1ET.getText().toString());

        if (inspCl2.getValue() == 0) {
            dailyCL.setInspection2("Yes");
        } else if (inspCl2.getValue() == 1) {
            dailyCL.setInspection2("No");
        } else {
            dailyCL.setInspection2("N/A");
        }
        dailyCL.setRemark2(remarkCl2ET.getText().toString());

        if (inspCl3.getValue() == 0) {
            dailyCL.setInspection3("Yes");
        } else if (inspCl3.getValue() == 1) {
            dailyCL.setInspection3("No");
        } else {
            dailyCL.setInspection3("N/A");
        }
        dailyCL.setRemark3(remarkCl3ET.getText().toString());

        if (inspCl4.getValue() == 0) {
            dailyCL.setInspection4("Yes");
        } else if (inspCl4.getValue() == 1) {
            dailyCL.setInspection4("No");
        } else {
            dailyCL.setInspection4("N/A");
        }
        dailyCL.setRemark4(remarkCl4ET.getText().toString());

        if (inspCl5.getValue() == 0) {
            dailyCL.setInspection5("Yes");
        } else if (inspCl5.getValue() == 1) {
            dailyCL.setInspection5("No");
        } else {
            dailyCL.setInspection5("N/A");
        }
        dailyCL.setRemark5(remarkCl5ET.getText().toString());

        if (inspCl6.getValue() == 0) {
            dailyCL.setInspection6("Yes");
        } else if (inspCl6.getValue() == 1) {
            dailyCL.setInspection6("No");
        } else {
            dailyCL.setInspection6("N/A");
        }
        dailyCL.setRemark6(remarkCl6ET.getText().toString());

        if (inspCl7.getValue() == 0) {
            dailyCL.setInspection7("Yes");
        } else if (inspCl7.getValue() == 1) {
            dailyCL.setInspection7("No");
        } else {
            dailyCL.setInspection7("N/A");
        }
        dailyCL.setRemark7(remarkCl7ET.getText().toString());

        if (inspCl8.getValue() == 0) {
            dailyCL.setInspection8("Yes");
        } else if (inspCl8.getValue() == 1) {
            dailyCL.setInspection8("No");
        } else {
            dailyCL.setInspection8("N/A");
        }
        dailyCL.setRemark8(remarkCl8ET.getText().toString());

        if (inspCl9.getValue() == 0) {
            dailyCL.setInspection9("Yes");
        } else if (inspCl9.getValue() == 1) {
            dailyCL.setInspection9("No");
        } else {
            dailyCL.setInspection9("N/A");
        }
        dailyCL.setRemark9(remarkCl9ET.getText().toString());

        if (inspCl10.getValue() == 0) {
            dailyCL.setInspection10("Yes");
        } else if (inspCl10.getValue() == 1) {
            dailyCL.setInspection10("No");
        } else {
            dailyCL.setInspection10("N/A");
        }
        dailyCL.setRemark10(remarkCl10ET.getText().toString());

        ds.insertUGCleanliness(dailyCL);

        /* ====================
                     UG Feed Frame
        ==================== */
        UGDrillingInspectionModel dailyFF = new UGDrillingInspectionModel();
        dailyFF.setProjectName(projNameET.getText().toString());
        dailyFF.setProjectOwner(projOwnerET.getText().toString());
        dailyFF.setDateIns(dateInsET.getText().toString());

        if (inspFF1.getValue() == 0) {
            dailyFF.setInspection1("Yes");
        } else if (inspFF1.getValue() == 1) {
            dailyFF.setInspection1("No");
        } else {
            dailyFF.setInspection1("N/A");
        }
        dailyFF.setRemark1(remarkFF1ET.getText().toString());

        if (inspFF2.getValue() == 0) {
            dailyFF.setInspection2("Yes");
        } else if (inspFF2.getValue() == 1) {
            dailyFF.setInspection2("No");
        } else {
            dailyFF.setInspection2("N/A");
        }
        dailyFF.setRemark2(remarkFF2ET.getText().toString());

        if (inspFF3.getValue() == 0) {
            dailyFF.setInspection3("Yes");
        } else if (inspFF3.getValue() == 1) {
            dailyFF.setInspection3("No");
        } else {
            dailyFF.setInspection3("N/A");
        }
        dailyFF.setRemark3(remarkFF3ET.getText().toString());

        if (inspFF4.getValue() == 0) {
            dailyFF.setInspection4("Yes");
        } else if (inspFF4.getValue() == 1) {
            dailyFF.setInspection4("No");
        } else {
            dailyFF.setInspection4("N/A");
        }
        dailyFF.setRemark4(remarkFF4ET.getText().toString());

        if (inspFF5.getValue() == 0) {
            dailyFF.setInspection5("Yes");
        } else if (inspFF5.getValue() == 1) {
            dailyFF.setInspection5("No");
        } else {
            dailyFF.setInspection5("N/A");
        }
        dailyFF.setRemark5(remarkFF5ET.getText().toString());

        if (inspFF6.getValue() == 0) {
            dailyFF.setInspection6("Yes");
        } else if (inspFF6.getValue() == 1) {
            dailyFF.setInspection6("No");
        } else {
            dailyFF.setInspection6("N/A");
        }
        dailyFF.setRemark6(remarkFF6ET.getText().toString());

        if (inspFF7.getValue() == 0) {
            dailyFF.setInspection7("Yes");
        } else if (inspFF7.getValue() == 1) {
            dailyFF.setInspection7("No");
        } else {
            dailyFF.setInspection7("N/A");
        }
        dailyFF.setRemark7(remarkFF7ET.getText().toString());

        if (inspFF8.getValue() == 0) {
            dailyFF.setInspection8("Yes");
        } else if (inspFF8.getValue() == 1) {
            dailyFF.setInspection8("No");
        } else {
            dailyFF.setInspection8("N/A");
        }
        dailyFF.setRemark8(remarkFF8ET.getText().toString());

        if (inspFF9.getValue() == 0) {
            dailyFF.setInspection9("Yes");
        } else if (inspFF9.getValue() == 1) {
            dailyFF.setInspection9("No");
        } else {
            dailyFF.setInspection9("N/A");
        }
        dailyFF.setRemark9(remarkFF9ET.getText().toString());

        if (inspFF10.getValue() == 0) {
            dailyFF.setInspection10("Yes");
        } else if (inspFF10.getValue() == 1) {
            dailyFF.setInspection10("No");
        } else {
            dailyFF.setInspection10("N/A");
        }
        dailyFF.setRemark10(remarkFF10ET.getText().toString());

        ds.insertUGFeedFrame(dailyFF);

        /* ====================
                        UG Control
        ==================== */
        UGDrillingInspectionModel dailyCnt = new UGDrillingInspectionModel();
        dailyCnt.setProjectName(projNameET.getText().toString());
        dailyCnt.setProjectOwner(projOwnerET.getText().toString());
        dailyCnt.setDateIns(dateInsET.getText().toString());

        if (inspCnt1.getValue() == 0) {
            dailyCnt.setInspection1("Yes");
        } else if (inspCnt1.getValue() == 1) {
            dailyCnt.setInspection1("No");
        } else {
            dailyCnt.setInspection1("N/A");
        }
        dailyCnt.setRemark1(remarkCnt1ET.getText().toString());

        if (inspCnt2.getValue() == 0) {
            dailyCnt.setInspection2("Yes");
        } else if (inspCnt2.getValue() == 1) {
            dailyCnt.setInspection2("No");
        } else {
            dailyCnt.setInspection2("N/A");
        }
        dailyCnt.setRemark2(remarkCnt2ET.getText().toString());

        if (inspCnt3.getValue() == 0) {
            dailyCnt.setInspection3("Yes");
        } else if (inspCnt3.getValue() == 1) {
            dailyCnt.setInspection3("No");
        } else {
            dailyCnt.setInspection3("N/A");
        }
        dailyCnt.setRemark3(remarkCnt3ET.getText().toString());

        ds.insertUGControl(dailyCnt);

        /* ====================
               UG Wireline Equipment
        ==================== */
        UGDrillingInspectionModel dailyWE = new UGDrillingInspectionModel();
        dailyWE.setProjectName(projNameET.getText().toString());
        dailyWE.setProjectOwner(projOwnerET.getText().toString());
        dailyWE.setDateIns(dateInsET.getText().toString());

        if (inspWE1.getValue() == 0) {
            dailyWE.setInspection1("Yes");
        } else if (inspWE1.getValue() == 1) {
            dailyWE.setInspection1("No");
        } else {
            dailyWE.setInspection1("N/A");
        }
        dailyWE.setRemark1(remarkWE1ET.getText().toString());

        if (inspWE2.getValue() == 0) {
            dailyWE.setInspection2("Yes");
        } else if (inspWE2.getValue() == 1) {
            dailyWE.setInspection2("No");
        } else {
            dailyWE.setInspection2("N/A");
        }
        dailyWE.setRemark2(remarkWE2ET.getText().toString());

        if (inspWE3.getValue() == 0) {
            dailyWE.setInspection3("Yes");
        } else if (inspWE3.getValue() == 1) {
            dailyWE.setInspection3("No");
        } else {
            dailyWE.setInspection3("N/A");
        }
        dailyWE.setRemark3(remarkWE3ET.getText().toString());

        if (inspWE4.getValue() == 0) {
            dailyWE.setInspection4("Yes");
        } else if (inspWE4.getValue() == 1) {
            dailyWE.setInspection4("No");
        } else {
            dailyWE.setInspection4("N/A");
        }
        dailyWE.setRemark4(remarkWE4ET.getText().toString());

        ds.insertUGWireline(dailyWE);

        /* ====================
               UG Circulation System
        ==================== */
        UGDrillingInspectionModel dailyCS = new UGDrillingInspectionModel();
        dailyCS.setProjectName(projNameET.getText().toString());
        dailyCS.setProjectOwner(projOwnerET.getText().toString());
        dailyCS.setDateIns(dateInsET.getText().toString());

        if (inspCS1.getValue() == 0) {
            dailyCS.setInspection1("Yes");
        } else if (inspCS1.getValue() == 1) {
            dailyCS.setInspection1("No");
        } else {
            dailyCS.setInspection1("N/A");
        }
        dailyCS.setRemark1(remarkCS1ET.getText().toString());

        if (inspCS2.getValue() == 0) {
            dailyCS.setInspection2("Yes");
        } else if (inspCS2.getValue() == 1) {
            dailyCS.setInspection2("No");
        } else {
            dailyCS.setInspection2("N/A");
        }
        dailyCS.setRemark2(remarkCS2ET.getText().toString());

        if (inspCS3.getValue() == 0) {
            dailyCS.setInspection3("Yes");
        } else if (inspCS3.getValue() == 1) {
            dailyCS.setInspection3("No");
        } else {
            dailyCS.setInspection3("N/A");
        }
        dailyCS.setRemark3(remarkCS3ET.getText().toString());

        if (inspCS4.getValue() == 0) {
            dailyCS.setInspection4("Yes");
        } else if (inspCS4.getValue() == 1) {
            dailyCS.setInspection4("No");
        } else {
            dailyCS.setInspection4("N/A");
        }
        dailyCS.setRemark4(remarkCS4ET.getText().toString());

        if (inspCS5.getValue() == 0) {
            dailyCS.setInspection5("Yes");
        } else if (inspCS5.getValue() == 1) {
            dailyCS.setInspection5("No");
        } else {
            dailyCS.setInspection5("N/A");
        }
        dailyCS.setRemark5(remarkCS5ET.getText().toString());

        if (inspCS6.getValue() == 0) {
            dailyCS.setInspection6("Yes");
        } else if (inspCS6.getValue() == 1) {
            dailyCS.setInspection6("No");
        } else {
            dailyCS.setInspection6("N/A");
        }
        dailyCS.setRemark6(remarkCS6ET.getText().toString());

        if (inspCS7.getValue() == 0) {
            dailyCS.setInspection7("Yes");
        } else if (inspCS7.getValue() == 1) {
            dailyCS.setInspection7("No");
        } else {
            dailyCS.setInspection7("N/A");
        }
        dailyCS.setRemark7(remarkCS7ET.getText().toString());

        ds.insertUGCirculation(dailyCS);

        /* ====================
                   UG Control Panel
        ==================== */
        UGDrillingInspectionModel dailyCP = new UGDrillingInspectionModel();
        dailyCP.setProjectName(projNameET.getText().toString());
        dailyCP.setProjectOwner(projOwnerET.getText().toString());
        dailyCP.setDateIns(dateInsET.getText().toString());

        if (inspCP1.getValue() == 0) {
            dailyCP.setInspection1("Yes");
        } else if (inspCP1.getValue() == 1) {
            dailyCP.setInspection1("No");
        } else {
            dailyCP.setInspection1("N/A");
        }
        dailyCP.setRemark1(remarkCP1ET.getText().toString());

        if (inspCP2.getValue() == 0) {
            dailyCP.setInspection2("Yes");
        } else if (inspCP2.getValue() == 1) {
            dailyCP.setInspection2("No");
        } else {
            dailyCP.setInspection2("N/A");
        }
        dailyCP.setRemark2(remarkCP2ET.getText().toString());

        if (inspCP3.getValue() == 0) {
            dailyCP.setInspection3("Yes");
        } else if (inspCP3.getValue() == 1) {
            dailyCP.setInspection3("No");
        } else {
            dailyCP.setInspection3("N/A");
        }
        dailyCP.setRemark3(remarkCP3ET.getText().toString());

        if (inspCP4.getValue() == 0) {
            dailyCP.setInspection4("Yes");
        } else if (inspCP4.getValue() == 1) {
            dailyCP.setInspection4("No");
        } else {
            dailyCP.setInspection4("N/A");
        }
        dailyCP.setRemark4(remarkCP4ET.getText().toString());

        if (inspCP5.getValue() == 0) {
            dailyCP.setInspection5("Yes");
        } else if (inspCP5.getValue() == 1) {
            dailyCP.setInspection5("No");
        } else {
            dailyCP.setInspection5("N/A");
        }
        dailyCP.setRemark5(remarkCP5ET.getText().toString());

        if (inspCP6.getValue() == 0) {
            dailyCP.setInspection6("Yes");
        } else if (inspCP3.getValue() == 1) {
            dailyCP.setInspection3("No");
        } else {
            dailyCP.setInspection6("N/A");
        }
        dailyCP.setRemark6(remarkCP6ET.getText().toString());

        if (inspCP7.getValue() == 0) {
            dailyCP.setInspection7("Yes");
        } else if (inspCP7.getValue() == 1) {
            dailyCP.setInspection7("No");
        } else {
            dailyCP.setInspection7("N/A");
        }
        dailyCP.setRemark7(remarkCP7ET.getText().toString());

        if (inspCP8.getValue() == 0) {
            dailyCP.setInspection8("Yes");
        } else if (inspCP8.getValue() == 1) {
            dailyCP.setInspection8("No");
        } else {
            dailyCP.setInspection8("N/A");
        }
        dailyCP.setRemark8(remarkCP8ET.getText().toString());

        if (inspCP9.getValue() == 0) {
            dailyCP.setInspection9("Yes");
        } else if (inspCP9.getValue() == 1) {
            dailyCP.setInspection9("No");
        } else {
            dailyCP.setInspection9("N/A");
        }
        dailyCP.setRemark9(remarkCP9ET.getText().toString());

        ds.insertUGControlPanel(dailyCP);

        /* ====================
                      UG Handtools
        ==================== */
        UGDrillingInspectionModel dailyHT = new UGDrillingInspectionModel();
        dailyHT.setProjectName(projNameET.getText().toString());
        dailyHT.setProjectOwner(projOwnerET.getText().toString());
        dailyHT.setDateIns(dateInsET.getText().toString());

        if (inspHT1.getValue() == 0) {
            dailyHT.setInspection1("Yes");
        } else if (inspHT1.getValue() == 1) {
            dailyHT.setInspection1("No");
        } else {
            dailyHT.setInspection1("N/A");
        }
        dailyHT.setRemark1(remarkHT1ET.getText().toString());

        if (inspHT2.getValue() == 0) {
            dailyHT.setInspection2("Yes");
        } else if (inspHT2.getValue() == 1) {
            dailyHT.setInspection2("No");
        } else {
            dailyHT.setInspection2("N/A");
        }
        dailyHT.setRemark2(remarkHT2ET.getText().toString());

        if (inspHT3.getValue() == 0) {
            dailyHT.setInspection3("Yes");
        } else if (inspHT3.getValue() == 1) {
            dailyHT.setInspection3("No");
        } else {
            dailyHT.setInspection3("N/A");
        }
        dailyHT.setRemark3(remarkHT3ET.getText().toString());

        if (inspHT4.getValue() == 0) {
            dailyHT.setInspection5("Yes");
        } else if (inspHT4.getValue() == 1) {
            dailyHT.setInspection5("No");
        } else {
            dailyHT.setInspection5("N/A");
        }
        dailyHT.setRemark5(remarkHT4ET.getText().toString());

        ds.insertUGHandtools(dailyHT);

        /* ====================
                  UG Work Procedure
        ==================== */
        UGDrillingInspectionModel dailyWP = new UGDrillingInspectionModel();
        dailyWP.setProjectName(projNameET.getText().toString());
        dailyWP.setProjectOwner(projOwnerET.getText().toString());
        dailyWP.setDateIns(dateInsET.getText().toString());

        if (inspWP1.getValue() == 0) {
            dailyWP.setInspection1("Yes");
        } else if (inspWP1.getValue() == 1) {
            dailyWP.setInspection1("No");
        } else {
            dailyWP.setInspection1("N/A");
        }
        dailyWP.setRemark1(remarkWP1ET.getText().toString());

        if (inspWP2.getValue() == 0) {
            dailyWP.setInspection2("Yes");
        } else if (inspWP2.getValue() == 1) {
            dailyWP.setInspection2("No");
        } else {
            dailyWP.setInspection2("N/A");
        }
        dailyWP.setRemark2(remarkWP2ET.getText().toString());

        if (inspWP3.getValue() == 0) {
            dailyWP.setInspection3("Yes");
        } else if (inspWP3.getValue() == 1) {
            dailyWP.setInspection3("No");
        } else {
            dailyWP.setInspection3("N/A");
        }
        dailyWP.setRemark3(remarkWP3ET.getText().toString());

        if (inspWP4.getValue() == 0) {
            dailyWP.setInspection4("Yes");
        } else if (inspWP4.getValue() == 1) {
            dailyWP.setInspection4("No");
        } else {
            dailyWP.setInspection4("N/A");
        }
        dailyWP.setRemark4(remarkWP4ET.getText().toString());

        if (inspWP5.getValue() == 0) {
            dailyWP.setInspection5("Yes");
        } else if (inspWP5.getValue() == 1) {
            dailyWP.setInspection5("No");
        } else {
            dailyWP.setInspection5("N/A");
        }
        dailyWP.setRemark5(remarkWP5ET.getText().toString());

        ds.insertUGWorkProcedure(dailyWP);

        ds.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImagePopUpUg.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {
            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(ImagePopUpUg.getPicture(), Helper.fileUri.getPath());
                ImagePopUpUg.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.ug;
        } else if (requestCode == ImagePopUpUg.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK && data != null) {
            // new File(tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImagePopUpUg.setImagePath(picturePath);
            Helper.setPic(ImagePopUpUg.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.ug;
    }

    public static void clearData() {
        dateInsET.setText("");
        areaOwnerET.setText("");
        projNameET.setText("");
        projOwnerET.setText("");
        holeIdET.setText("");
        drillContET.setText("");
        drillCoorET.setText("");
        ugGeologyET.setText("");
        ugGeoTechET.setText("");
        ugHydroET.setText("");
        ugSheET.setText("");
        ugVentilationET.setText("");

        typeET.setText("");
        lastCalibET.setText("");
        idET.setText("");

        remarkPre1ET.setText("");
        remarkPre2ET.setText("");

        inspPre1.setValue(-1);
        inspPre2.setValue(-1);

        remarkElec1ET.setText("");
        remarkElec2ET.setText("");
        remarkElec3ET.setText("");
        remarkElec4ET.setText("");
        remarkElec5ET.setText("");
        remarkElec6ET.setText("");
        remarkElec7ET.setText("");

        inspElec1.setValue(-1);
        inspElec2.setValue(-1);
        inspElec3.setValue(-1);
        inspElec4.setValue(-1);
        inspElec5.setValue(-1);
        inspElec6.setValue(-1);
        inspElec7.setValue(-1);

        remarkSE1ET.setText("");
        remarkSE2ET.setText("");
        remarkSE3ET.setText("");
        remarkSE4ET.setText("");
        remarkSE5ET.setText("");
        remarkSE6ET.setText("");
        remarkSE7ET.setText("");
        remarkSE8ET.setText("");
        remarkSE9ET.setText("");
        remarkSE10ET.setText("");

        inspSE1.setValue(-1);
        inspSE2.setValue(-1);
        inspSE3.setValue(-1);
        inspSE4.setValue(-1);
        inspSE5.setValue(-1);
        inspSE6.setValue(-1);
        inspSE7.setValue(-1);
        inspSE8.setValue(-1);
        inspSE9.setValue(-1);
        inspSE10.setValue(-1);

        remarkSi1ET.setText("");
        remarkSi2ET.setText("");
        remarkSi3ET.setText("");
        remarkSi3_1ET.setText("");
        remarkSi3_2ET.setText("");
        remarkSi3_3ET.setText("");
        remarkSi3_4ET.setText("");

        inspSi1.setValue(-1);
        inspSi2.setValue(-1);
        inspSi3.setValue(-1);
        inspSi3_1.setValue(-1);
        inspSi3_2.setValue(-1);
        inspSi3_3.setValue(-1);
        inspSi3_4.setValue(-1);

        remarkCl1ET.setText("");
        remarkCl2ET.setText("");
        remarkCl3ET.setText("");
        remarkCl4ET.setText("");
        remarkCl5ET.setText("");
        remarkCl6ET.setText("");
        remarkCl7ET.setText("");
        remarkCl8ET.setText("");
        remarkCl9ET.setText("");
        remarkCl10ET.setText("");

        inspCl1.setValue(-1);
        inspCl2.setValue(-1);
        inspCl3.setValue(-1);
        inspCl4.setValue(-1);
        inspCl5.setValue(-1);
        inspCl6.setValue(-1);
        inspCl7.setValue(-1);
        inspCl8.setValue(-1);
        inspCl9.setValue(-1);
        inspCl10.setValue(-1);

        remarkFF1ET.setText("");
        remarkFF2ET.setText("");
        remarkFF3ET.setText("");
        remarkFF4ET.setText("");
        remarkFF5ET.setText("");
        remarkFF6ET.setText("");
        remarkFF7ET.setText("");
        remarkFF8ET.setText("");
        remarkFF9ET.setText("");
        remarkFF10ET.setText("");

        inspFF1.setValue(-1);
        inspFF2.setValue(-1);
        inspFF3.setValue(-1);
        inspFF4.setValue(-1);
        inspFF5.setValue(-1);
        inspFF6.setValue(-1);
        inspFF7.setValue(-1);
        inspFF8.setValue(-1);
        inspFF9.setValue(-1);
        inspFF10.setValue(-1);

        remarkCnt1ET.setText("");
        remarkCnt2ET.setText("");
        remarkCnt3ET.setText("");

        inspCnt1.setValue(-1);
        inspCnt2.setValue(-1);
        inspCnt3.setValue(-1);

        remarkWE1ET.setText("");
        remarkWE2ET.setText("");
        remarkWE3ET.setText("");
        remarkWE4ET.setText("");

        inspWE1.setValue(-1);
        inspWE2.setValue(-1);
        inspWE3.setValue(-1);
        inspWE4.setValue(-1);

        remarkCS1ET.setText("");
        remarkCS2ET.setText("");
        remarkCS3ET.setText("");
        remarkCS4ET.setText("");
        remarkCS5ET.setText("");
        remarkCS6ET.setText("");
        remarkCS7ET.setText("");

        inspCS1.setValue(-1);
        inspCS2.setValue(-1);
        inspCS3.setValue(-1);
        inspCS4.setValue(-1);
        inspCS5.setValue(-1);
        inspCS6.setValue(-1);
        inspCS7.setValue(-1);

        remarkCP1ET.setText("");
        remarkCP2ET.setText("");
        remarkCP3ET.setText("");
        remarkCP4ET.setText("");
        remarkCP5ET.setText("");
        remarkCP6ET.setText("");
        remarkCP7ET.setText("");
        remarkCP8ET.setText("");
        remarkCP9ET.setText("");

        inspCP1.setValue(-1);
        inspCP2.setValue(-1);
        inspCP3.setValue(-1);
        inspCP4.setValue(-1);
        inspCP5.setValue(-1);
        inspCP6.setValue(-1);
        inspCP7.setValue(-1);
        inspCP8.setValue(-1);
        inspCP9.setValue(-1);

        remarkHT1ET.setText("");
        remarkHT2ET.setText("");
        remarkHT3ET.setText("");
        remarkHT4ET.setText("");

        inspHT1.setValue(-1);
        inspHT2.setValue(-1);
        inspHT3.setValue(-1);
        inspHT4.setValue(-1);

        remarkWP1ET.setText("");
        remarkWP2ET.setText("");
        remarkWP3ET.setText("");
        remarkWP4ET.setText("");
        remarkWP5ET.setText("");

        inspWP1.setValue(-1);
        inspWP2.setValue(-1);
        inspWP3.setValue(-1);
        inspWP4.setValue(-1);
        inspWP5.setValue(-1);

        recycleView();
    }


    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (dateInsET.getText().toString().length() != 0 && projNameET.getText().toString().length() != 0
//                        && projOwnerET.getText().toString().length() != 0 && holeIdET.getText().toString().length() != 0
//                        && areaOwnerET.getText().toString().length() != 0 && drillContET.getText().toString().length() != 0
//                        && drillCoorET.getText().toString().length() != 0 && ugGeoTechET.getText().toString().length() != 0
//                        && ugHydroET.getText().toString().length() != 0 && ugGeologyET.getText().toString().length() != 0
//                        && ugSheET.getText().toString().length() != 0 && ugVentilationET.getText().toString().length() != 0
//                        && idET.getText().toString().length() != 0 && lastCalibET.getText().toString().length() != 0) {
                    saveData();
                    mActivity.finish();
//                } else {
//                    mActivity.finish();
//                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (dateInsET.getText().toString().length() != 0 && projNameET.getText().toString().length() != 0
//                        && projOwnerET.getText().toString().length() != 0 && holeIdET.getText().toString().length() != 0
//                        && areaOwnerET.getText().toString().length() != 0 && drillContET.getText().toString().length() != 0
//                        && drillCoorET.getText().toString().length() != 0 && ugGeoTechET.getText().toString().length() != 0
//                        && ugHydroET.getText().toString().length() != 0 && ugGeologyET.getText().toString().length() != 0
//                        && ugSheET.getText().toString().length() != 0 && ugVentilationET.getText().toString().length() != 0
//                        && idET.getText().toString().length() != 0 && lastCalibET.getText().toString().length() != 0) {
                    saveData();
                    Intent intent = new Intent(mActivity, MainMenu.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
//                } else {
//                    Intent intent = new Intent(mActivity, MainMenu.class);
//                    mActivity.startActivity(intent);
//                    mActivity.finish();
//                }
            }
        });

        builder.show();
    }

}
