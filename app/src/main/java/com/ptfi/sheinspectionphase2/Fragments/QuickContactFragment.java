package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.ptfi.sheinspectionphase2.Adapter.ListChangeLogAdapter;
import com.ptfi.sheinspectionphase2.R;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 5/11/16.
 */
public class QuickContactFragment extends DialogFragment {

    public static QuickContactFragment newInstance() {

        return new QuickContactFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (getDialog() != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            getDialog().getWindow().setBackgroundDrawableResource(R.color.DarkOrange);
        }

        View root = inflater.inflate(R.layout.fragment_quick_contact, container, false);
        ViewPager pager = (ViewPager) root.findViewById(R.id.pager);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) root.findViewById(R.id.tabs);
        ContactPagerAdapter adapter = new ContactPagerAdapter(getActivity(), getActivity());
        pager.setAdapter(adapter);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);

        return root;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart() {
        super.onStart();

        // change dialog width
        if (getDialog() != null) {
            int fullWidth;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                fullWidth = size.x;
            } else {
                Display display = getActivity().getWindowManager().getDefaultDisplay();
                fullWidth = display.getWidth();
            }

            final int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                    .getDisplayMetrics());
            int w = fullWidth - padding;
            int h = getDialog().getWindow().getAttributes().height;
            getDialog().getWindow().setLayout(w, h);
        }
    }

    public static class ContactPagerAdapter extends PagerAdapter implements PagerSlidingTabStrip.CustomTabProvider {

        private final String[] TITLES = {
                "About",
                "Changelog"
        };

        private final Context mContext;
        Activity mActivity;

        public ContactPagerAdapter(Context context, Activity activity) {
            super();
            mContext = context;
            this.mActivity = activity;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ListView Lv = (ListView) LayoutInflater.from(mContext).inflate(R.layout.fragment_quickcontact, container, false);
            ArrayList<String[]> listAboutContent = new ArrayList<String[]>();

            if (position == 0 ) {
                listAboutContent.add(new String[]{"About", "Mobile Application for SHE Inspection Collaborated by"});
                listAboutContent.add(new String[]{"GeoEngineering", "1. Wahyu Sunyoto" + "\n2. Anton Perdana"
                        + "\n3. Munsi Nasution" + "\n4. Surya Nugraha" + "\n5. Persi Laseria R" + "\n6. Dewa Widyanto" + "\n6. Sukaerang"});
                listAboutContent.add(new String[]{"ENJ", "1. Aditya Pringgoprawiro"
                        + "\n2. Tommy Kurniawan" + "\n3. Semara Rasmaranti"
                        + "\n4. Sena Ardy Putra"});
                listAboutContent.add(new String[]{"Release Date", "-"});
                ListAdapter customAdapter = new ListChangeLogAdapter(mActivity, listAboutContent);

                Lv.setAdapter(customAdapter);
            } else {
                listAboutContent.add(new String[]{"Global", "Change Look & Feel for Menu Button, etc"});
                listAboutContent.add(new String[]{"SHE Inspection", "-  New Simple Layout, New button Menu, and New Looks"});
                ListAdapter customAdapter = new ListChangeLogAdapter(mActivity, listAboutContent);

                Lv.setAdapter(customAdapter);
            }

            container.addView(Lv);
            return Lv;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            container.removeView((View) view);
        }

        @Override
        public boolean isViewFromObject(View v, Object o) {
            return v == o;
        }

        @Override
        public View getCustomTabView(ViewGroup parent, int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, parent, false);
            ((TextView) tab.findViewById(R.id.psts_tab_title)).setText(TITLES[position]);
            return tab;
        }

        @Override
        public void tabSelected(View tab) {
            //Callback with the tab on his selected state. It is up to the developer to change anything on it.
        }

        @Override
        public void tabUnselected(View tab) {
            //Callback with the tab on his unselected state. It is up to the developer to change anything on it.
        }
    }
}
