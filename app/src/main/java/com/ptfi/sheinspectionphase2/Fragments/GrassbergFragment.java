package com.ptfi.sheinspectionphase2.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.MainMenu;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Models.GrsDetailModel;
import com.ptfi.sheinspectionphase2.Models.GrsDrillingModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.PopUps.GrassbergDetailCallback;
import com.ptfi.sheinspectionphase2.PopUps.GrassbergDetailPopUp;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUp;
import com.ptfi.sheinspectionphase2.PopUps.ImagePopUpCallback;
import com.ptfi.sheinspectionphase2.R;
import com.ptfi.sheinspectionphase2.Utils.ButtonRectangle;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;
import com.ptfi.sheinspectionphase2.Utils.Reporting;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class GrassbergFragment extends Fragment {

    static protected FragmentActivity mActivity;

    public View rootView;

    CardView hydroCV, geoTechCV,  geoCV, engineeringCV, pontilCV, operationCV, grsCV, photoCV, grassbergCV, geoEngCV;

    static TextInputLayout picTL, dateTL, idTL;

    static AutoCompleteTextView picET;

    static EditText dateET, idET;

    LinearLayout photoLL, grassbergLL;

    ImageView addButton;

    static RecyclerView photoRV;
    RecyclerView.LayoutManager layoutManager;

    private ArrayList<PhotoModel> photoModel;
    private static ArrayList<PhotoModel> photoData = new ArrayList<>();
    PhotoAdapter photoAdapter;

    TextView imeiTV, versionTV;

    private static TextView commentHydroTV, dateHydroTV, nameHydroTV, commentGeoTV, dateGeoTV, nameGeoTV, commentGeoTechTV, dateGeoTechTV, nameGeoTechTV,
            commentEngTV, dateEngTV, nameEngTV, commentPontilTV, datePontilTV, namePontilTV, commentGrsTV, dateGrsTV, nameGrsTV, commentGeoEngTV,
            dateGeoEngTV, nameGeoEngTV, commentOprTV, dateOprTV, nameOprTV;

//    private static ButtonRectangle generateBtn, exportBtn, previewBtn, clearBtn;

    private GrsDrillingModel grsDrillingModel;
    private GrsDetailModel grsDetailGeology;
    private GrsDetailModel grsDetailHydro;
    private GrsDetailModel grsDetailGeoTech;
    private GrsDetailModel grsDetailEngineering;
    private GrsDetailModel grsDetailPontil;
    private GrsDetailModel grsDetailGrs;
    private GrsDetailModel grsDetailGeoEngineering;
    private GrsDetailModel grsDetailOperation;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    static ImageView expandIcon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_grassberg, container, false);
        Helper.visiblePage = Helper.visiblePage.grs;

        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        // Version
        versionTV = (TextView) rootView.findViewById(R.id.versionTV);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiTV = (TextView) rootView.findViewById(R.id.imeiTV);
        imeiTV.setText("Device IMEI : " + deviceID);

        initComponent();
        grsDrillingDetailClick();
        loadData();

        return rootView;
    }

    public static boolean validateSaveData() {
        boolean status = true;

        if (picET.getText().toString().length() == 0) {
            status = false;
            picTL.setErrorEnabled(true);
            picTL.setError("Please fill Person In Charge first");
        }

        if (idET.getText().toString().length() == 0) {
            status = false;
            idTL.setErrorEnabled(true);
            idTL.setError("Please fill Person In Charge first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date Inspection first");
        }

        return status;
    }

    public void initComponent() {
        if(rootView != null) {
            // Department Inspection
            hydroCV = (CardView) rootView.findViewById(R.id.hydroCV);
            geoTechCV = (CardView) rootView.findViewById(R.id.geoTechCV);
            geoCV = (CardView) rootView.findViewById(R.id.geoCV);
            engineeringCV = (CardView) rootView.findViewById(R.id.engineeringCV);
            pontilCV = (CardView) rootView.findViewById(R.id.pontilCV);
            operationCV = (CardView) rootView.findViewById(R.id.operationCV);
            grsCV = (CardView) rootView.findViewById(R.id.grsCV);
            geoEngCV = (CardView) rootView.findViewById(R.id.geoEngCV);

            // Linear Header Inspection
            photoCV = (CardView) rootView.findViewById(R.id.pressPhotoCV);
            grassbergCV = (CardView) rootView.findViewById(R.id.pressGrsCV);
            photoLL = (LinearLayout) rootView.findViewById(R.id.photoLL);
            grassbergLL = (LinearLayout) rootView.findViewById(R.id.grassbergLL);
            expandIcon = (ImageView) rootView.findViewById(R.id.expandIcon);

            photoCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                        expandIcon.setImageResource(R.drawable.collapse_logo);
                    } else {
                        photoLL.setVisibility(View.GONE);
                        expandIcon.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            expandIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (photoLL.getVisibility() == View.GONE) {
                        photoLL.setVisibility(View.VISIBLE);
                        expandIcon.setImageResource(R.drawable.collapse_logo);
                    } else {
                        photoLL.setVisibility(View.GONE);
                        expandIcon.setImageResource(R.drawable.expand_logo);
                    }
                }
            });

            // Grassberg Fill Inspection
            picTL = (TextInputLayout) rootView.findViewById(R.id.picTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            idTL = (TextInputLayout) rootView.findViewById(R.id.idTL);

            dateET = (EditText) rootView.findViewById(R.id.dateET);

            idET = (EditText) rootView.findViewById(R.id.idET);

            picET = (AutoCompleteTextView) rootView.findViewById(R.id.picET);
            final ArrayList<String> dataManpower = ManpowerModel.getManpowerName(getActivity());
            final ArrayList<String> dataManpowerID = ManpowerModel.getManpowerId(getActivity());
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line,
                    dataManpower);
            picET.setThreshold(1);
            picET.setAdapter(adapter);
            picET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String key = picET.getText().toString();
                    int pos = dataManpower.indexOf(key);

                    picET.setText(dataManpower.get(pos));
                    idET.setText(dataManpowerID.get(pos));
                }
            });

            // Department Data
            commentHydroTV = (TextView) rootView.findViewById(R.id.commentHydroTV);
            dateHydroTV = (TextView) rootView.findViewById(R.id.dateHydroTV);
            nameHydroTV = (TextView) rootView.findViewById(R.id.nameHydroTV);

            commentGeoTV = (TextView) rootView.findViewById(R.id.commentGeoTV);
            dateGeoTV = (TextView) rootView.findViewById(R.id.dateGeoTV);
            nameGeoTV = (TextView) rootView.findViewById(R.id.nameGeoTV);

            commentGeoTechTV = (TextView) rootView.findViewById(R.id.commentGeoTechTV);
            dateGeoTechTV = (TextView) rootView.findViewById(R.id.dateGeoTechTV);
            nameGeoTechTV = (TextView) rootView.findViewById(R.id.nameGeoTechTV);

            commentEngTV = (TextView) rootView.findViewById(R.id.commentEngTV);
            dateEngTV = (TextView) rootView.findViewById(R.id.dateEngTV);
            nameEngTV = (TextView) rootView.findViewById(R.id.nameEngTV);

            commentPontilTV = (TextView) rootView.findViewById(R.id.commentPontilTV);
            datePontilTV = (TextView) rootView.findViewById(R.id.datePontilTV);
            namePontilTV = (TextView) rootView.findViewById(R.id.namePontilTV);

            commentGrsTV = (TextView) rootView.findViewById(R.id.commentGrsTV);
            dateGrsTV = (TextView) rootView.findViewById(R.id.dateGrsTV);
            nameGrsTV = (TextView) rootView.findViewById(R.id.nameGrsTV);

            commentOprTV = (TextView) rootView.findViewById(R.id.commentOprTV);
            dateOprTV = (TextView) rootView.findViewById(R.id.dateOprTV);
            nameOprTV = (TextView) rootView.findViewById(R.id.nameOprTV);

            commentGeoEngTV = (TextView) rootView.findViewById(R.id.commentGeoEngTV);
            dateGeoEngTV = (TextView) rootView.findViewById(R.id.dateGeoEngTV);
            nameGeoEngTV = (TextView) rootView.findViewById(R.id.nameGeoEngTV);

            addButton = (ImageView) rootView.findViewById(R.id.addPhoto);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImagePopUp.imagePopUp(mActivity, false, Constants.INSPECTION_GRS_DRILLING, "", "", dateET.getText().toString(),
                            picET.getText().toString(), new ImagePopUpCallback() {
                                @Override
                                public void onImageSubmitted() {
                                    recycleView();
                                }
                            });
                }
            });

//            generateBtn = (ButtonRectangle) rootView.findViewById(R.id.generateBtn);
//            previewBtn = (ButtonRectangle) rootView.findViewById(R.id.previewBtn);
//            clearBtn = (ButtonRectangle) rootView.findViewById(R.id.clearBtn);
//
//            generateBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Reporting.generateReportGRS(mActivity, picET.getText().toString(), dateET.getText().toString(),true);
//                }
//            });
//
//            previewBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Reporting.generateReportGRS(mActivity, picET.getText().toString(), dateET.getText().toString(),false);
//                }
//            });
//
//            clearBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    clearAll();
//                }
//            });

            // Date
            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());
        }
    }

    public static void generateClick() {
        saveData();
//        Reporting.generateReportGRS(mActivity, picET.getText().toString(), dateET.getText().toString(),true);
        GenerateAsync generate = new GenerateAsync();
        generate.execute(true);
    }

    public static void previewClick() {
        saveData();
//        Reporting.generateReportGRS(mActivity, picET.getText().toString(), dateET.getText().toString(),false);
        GenerateAsync generate = new GenerateAsync();
        generate.execute(false);
    }

    static class GenerateAsync extends AsyncTask<Boolean, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Helper.showProgressDialog(mActivity);
        }

        @Override
        protected Void doInBackground(Boolean... params) {
            generateReportGRS(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Helper.progressDialog.dismiss();
        }

    }

    static String baseReportPath = Constants.basepath + "/"
            + Constants.APP_FOLDER_NAME + "/" + Constants.EXPORT_FOLDER_NAME
            + "/" + Constants.GRS_DRILL_FOLDER_NAME;

    static String folderDirectory = "";
    private static String previewFilePath;
    private static boolean isPreview = false;
    static List<PhotoModel> fotoList = new ArrayList<PhotoModel>();
    private static final int OPEN_PREVIEW = 323;

    public static void generateReportGRS(final boolean isGenerate) {

                Helper.deleteAllFilesOnDirectory(Constants.extStorageDirectory
                        + "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME);

                // copy wanted template on to template folder
                Helper.copyFolderAndContent(Constants.extStorageDirectory + "/"
                                + Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        mActivity);

                String templatePath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();
                templatePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FILE_GS_DRILLING;

                try {
                    final Document doc = new Document(templatePath);
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE,
                            0, true);
                    Row templateRow;
                    Cell cell;
                    int count = 1;
                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    // Details Table
                    /* ========================
                                Department Hydrology
                    ======================== */
                    GrsDetailModel hydro = ds.getLatestGRSDetail("Department Hydrology", dateET.getText().toString(), picET.getText().toString());
                    doc.getRange().replace("DEPARTMENT_1", hydro.getDepartment(), true, true);
                    doc.getRange().replace("Comment_1", hydro.getComment(), true, true);
                    doc.getRange().replace("Date_1", hydro.getDate(), true, true);
                    if (hydro.getSign() != null && !hydro.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(hydro.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                     /* ========================
                                Department Geology
                    ======================== */
                    GrsDetailModel geology = ds.getLatestGRSDetail("Department Geology", dateET.getText().toString(), picET.getText().toString());
                    doc.getRange().replace("DEPARTMENT_2", geology.getDepartment(), true, true);
                    doc.getRange().replace("Comment_2", geology.getComment(), true, true);
                    doc.getRange().replace("Date_2", geology.getDate(), true, true);
                    if (geology.getSign() != null && !geology.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(2);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(geology.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department GeoTechnical
                    ======================== */
                    GrsDetailModel GeoTechnical = ds.getLatestGRSDetail("Department GeoTechnical", dateET.getText().toString(), picET.getText().toString());
                    doc.getRange().replace("DEPARTMENT_3", GeoTechnical.getDepartment(), true, true);
                    doc.getRange().replace("Comment_3", GeoTechnical.getComment(), true, true);
                    doc.getRange().replace("Date_3", GeoTechnical.getDate(), true, true);
                    if (GeoTechnical.getSign() != null && !GeoTechnical.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(3);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(GeoTechnical.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department Operation
                    ======================== */
                    GrsDetailModel Operation = ds.getLatestGRSDetail("Department Operation (SRMP)", dateET.getText().toString(), picET.getText().toString());
                    doc.getRange().replace("DEPARTMENT_4", Operation.getDepartment(), true, true);
                    doc.getRange().replace("Comment_4", Operation.getComment(), true, true);
                    doc.getRange().replace("Date_4", Operation.getDate(), true, true);
                    if (Operation.getSign() != null && !Operation.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(4);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Operation.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department Operation
                    ======================== */
                    GrsDetailModel Engineering = ds.getLatestGRSDetail("Department Engineering (SRMP)", dateET.getText().toString(), picET.getText().toString());
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_5", Engineering.getDepartment(), true, true);
                    doc.getRange().replace("Comment_5", Engineering.getComment(), true, true);
                    doc.getRange().replace("Date_5", Engineering.getDate(), true, true);
                    if (Engineering.getSign() != null && !Engineering.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(5);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Engineering.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                                 Department Pontil
                    ======================== */
                    GrsDetailModel Pontil = ds.getLatestGRSDetail("GRS Drainage Group", dateET.getText().toString(), picET.getText().toString());
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_6", Pontil.getDepartment(), true, true);
                    doc.getRange().replace("Comment_6", Pontil.getComment(), true, true);
                    doc.getRange().replace("Date_6", Pontil.getDate(), true, true);
                    if (Pontil.getSign() != null && !Pontil.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(6);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Pontil.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                                 Department GRS
                    ======================== */
                    GrsDetailModel GRS = ds.getLatestGRSDetail("GRS Drainage Group", dateET.getText().toString(), picET.getText().toString());
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_7", GRS.getDepartment(), true, true);
                    doc.getRange().replace("Comment_7", GRS.getComment(), true, true);
                    doc.getRange().replace("Date_7", GRS.getDate(), true, true);
                    if (GRS.getSign() != null && !GRS.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(7);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(GRS
                                .getSign());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                              Department GeoEng
                    ======================== */
                    GrsDetailModel GeoEng = ds.getLatestGRSDetail("Department OH&S Geo Engineering", dateET.getText().toString(), picET.getText().toString());
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_8", GeoEng.getDepartment(), true, true);
                    doc.getRange().replace("Comment_8", GeoEng.getComment(), true, true);
                    doc.getRange().replace("Date_8", GeoEng.getDate(), true, true);
                    if (GeoEng.getSign() != null && !GeoEng.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(8);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(GeoEng.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ====================
                                        PHOTO
                    ==================== */
                    Table photoTableDoc = (Table) doc.getChild(NodeType.TABLE,
                            1, true);
                    RowCollection photoTableRows = photoTableDoc.getRows();
                    int photoRowStartingIndex = 2;
                    int photoRowDummyIndex = 1;
                    int photoColStartingIndex = 0;
                    int photoRowCount = 0;

                    int counts = 0;
                    ArrayList<PhotoModel> fotoModel = ds.getPhotoData(Constants.INSPECTION_GRS_DRILLING, dateET.getText().toString(), picET.getText().toString());
                    ds.close();

                    Row dataRow;
                    Row clonedRow;
                    clonedRow = (Row) photoTableRows
                            .get(photoRowDummyIndex);
                    for (int a = 0; a < fotoModel.size(); a ++) {
                        dataRow = (Row) clonedRow.deepClone(true);
                        photoTableDoc.getRows().add(dataRow);
                        counts++;
//                        if (photoRowCount > 0) {
                        // copy 1st data row
//                        } else {
//                            // 1st row of table is dummy row (for copy cell
//                            // purpose)
//                            dataRow = (Row) photoTableRows
//                                    .get(photoRowStartingIndex);
//                        }

                        dataRow.getRowFormat().setAllowBreakAcrossPages(true);

                        // get the cell for each attribute of foto (number,
                        // photo, and comment)
                        Cell photoNumberCell = dataRow.getCells().get(
                                photoColStartingIndex + 0);
                        Cell photoPhotoCell = dataRow.getCells().get(
                                photoColStartingIndex + 1);
                        Cell photoCommentCell = dataRow.getCells().get(
                                photoColStartingIndex + 2);

                        // set each cell background to white
                        photoNumberCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoPhotoCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoCommentCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);

                        // clear each cell
                        photoNumberCell.getFirstParagraph().getRuns().clear();
                        photoPhotoCell.getFirstParagraph().getRuns().clear();
                        photoCommentCell.getFirstParagraph().getRuns().clear();

                        // fill the data to each cell
                        // number data
                        photoNumberCell.getFirstParagraph()
                                .appendChild(
                                        new Run(doc, valueOf(counts)));

                        // photo
                        CellFormat format = photoPhotoCell.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(photoPhotoCell.getFirstParagraph());
                        Bitmap imgBitmap = null;// Helper.getBitmapFromFile(foto.getFotoPath());

                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(fotoModel.get(a).getFotoPath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 90, (int) width);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 180, (int) width);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 0, (int) width);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (imgBitmap != null) {
                            // Shape image = builder.insertImage(imgBitmap);
                            ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                            imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                                    Constants.COMPRESS_PERCENTAGE, tempImage);
                            Bitmap compressedBitmap = BitmapFactory
                                    .decodeStream(new ByteArrayInputStream(
                                            tempImage.toByteArray()));
                            Shape image = builder.insertImage(compressedBitmap);

                            double freePageWidth = width;
                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.e("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }

                        // comment
                        photoCommentCell.getFirstParagraph().appendChild(
                                new Run(doc, fotoModel.get(a).getComment()));

//                        if (photoRowCount > 0) {
//                            // insert row
//                        }
                        photoRowCount++;
                    }

//                    photoTableDoc.getRows().removeAt(1);

                    Row dummyRow = photoTableRows.get(1);
                    dummyRow.remove();

                    // SAVE FILE
                    if (isGenerate) {
                        // GENERATE FILE NAME FORMAT
                        String baseName = Constants.GRSD_REPORT_FILE_NAME;

                        String formattedID = picET.getText().toString();
                        String currentTime = new SimpleDateFormat(
                                "yyyy_MM_dd_HH_mm").format(new Date());
                        String baseFolderName = currentTime + "_"
                                + Constants.GRSD_FOLDER_MARK_NAME + "_"
                                + formattedID;

                        String thisReportPath = baseReportPath + File.separator
                                + baseFolderName;
                        Helper.checkAndCreateDirectory(thisReportPath);
                        folderDirectory = thisReportPath;

                        String previewFolder = thisReportPath + "/"
                                + Constants.PREVIEW_FOLDER_NAME;

                        doc.save(thisReportPath + "/" + baseName
                                + Constants.FILE_TYPE_DOCX);
                        doc.save(thisReportPath + "/" + baseName
                                + Constants.FILE_TYPE_PDF);

                        // copy photos to folderDirectory
                        // photo
                        for (PhotoModel foto : fotoList) {
                            if (null != foto.getFotoPath()) {
                                InputStream is;
                                try {
                                    // trying to copy image to folderDirectory
                                    // based on its path
                                    is = new FileInputStream(foto.getFotoPath());

                                    File fileImage = new File(foto
                                            .getFotoPath());
                                    if (fileImage.exists()) {
                                        String photoDirectory = folderDirectory
                                                + "/"
                                                + Constants.PHOTO_FOLDER_NAME;
                                        new File(photoDirectory).mkdirs();
                                        String outputPath = photoDirectory
                                                + File.separator + foto.getId()
                                                + ".jpg";
                                        OutputStream os = new FileOutputStream(
                                                outputPath);
                                        Helper.copyFile(is, os);
                                        // foto.setFotoPath(outputPath);
                                    }

                                } catch (FileNotFoundException e) {
                                    // TODO Auto-generated catch block
//                                    Log.e("Error create photo folder",
//                                            e.getMessage());
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
//                                    Log.e("Error create photo folder",
//                                            e.getMessage());
                                }
                            }
                        }

                    } else {
                        // preview
                        String previewFolder = Helper.getPathForPreviewFile();
                        String fileNamePdf = Constants.GRSD_PREVIEW_FILE_NAME
                                + Constants.FILE_TYPE_PDF;

                        Helper.checkAndCreateDirectory(previewFolder);
                        previewFilePath = previewFolder + "/" + fileNamePdf;
                        doc.save(previewFilePath);
                    }

                    String reportPath = folderDirectory + "/"
                            + Constants.GRSD_REPORT_FILE_NAME
                            + Constants.FILE_TYPE_DOCX;

                    File file = new File(isGenerate ? reportPath
                            : previewFilePath);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    String type = isGenerate ? "application/msword"
                            : "application/pdf";
                    intent.setDataAndType(Uri.fromFile(file), type);

                    Helper.progressDialog.dismiss();
                    if (isGenerate) {
                        Helper.showPopUpMessage(mActivity, "",
                                "A report generated\n file has been saved in directory : "
                                        + folderDirectory, null);
                    } else {
                        isPreview = true;
                        mActivity.setResult(mActivity.RESULT_OK, intent);
                    }
                    mActivity.startActivityForResult(intent, OPEN_PREVIEW);
					/*
					 * new Handler().postDelayed(new Runnable() {
					 *
					 * @Override public void run() {
					 * Helper.deleteFileOnThisPath(
					 * previewFilePath+"/"+Constants.GRSD_PREVIEW_FILE_NAME +
					 * Constants.FILE_TYPE_PDF); //delete preview folder File
					 * previewDir = new File(previewFilePath); boolean status =
					 * Helper.deleteDirectory(previewDir);
					 * Log.e("Preview Directory Delete Status",""+status); } },
					 * 3000*5);
					 */
                } catch (Exception ex) {
                    new Exception(ex.getMessage());
                }

    }

    public static void datePickers(final Activity MainActivity, final View rootView) {
        switch (rootView.getId()) {
            case R.id.dateET:
                Helper.showDatePicker(rootView, (FragmentActivity) MainActivity,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                selectedYear = year;
                                selectedMonth = monthOfYear;
                                selectedDate = dayOfMonth;

                                DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                                ((EditText) rootView).setText(DataSingleton.getInstance()
                                        .getFormattedDate());
                            }
                        }, selectedYear, selectedMonth, selectedDate);
                break;

            default:
                break;
        }
    }

    public static void clearAll() {
        picET.setText("");
        idET.setText("");
        dateET.setText("");

        nameEngTV.setText("");
        dateEngTV.setText("");
        commentEngTV.setText("");

        nameGeoEngTV.setText("");
        dateGeoEngTV.setText("");
        commentGeoEngTV.setText("");

        nameGeoTechTV.setText("");
        dateGeoTechTV.setText("");
        commentGeoTechTV.setText("");

        nameHydroTV.setText("");
        dateHydroTV.setText("");
        commentHydroTV.setText("");

        namePontilTV.setText("");
        datePontilTV.setText("");
        commentPontilTV.setText("");

        nameOprTV.setText("");
        dateOprTV.setText("");
        commentOprTV.setText("");

        nameGrsTV.setText("");
        dateGrsTV.setText("");
        commentGrsTV.setText("");

        nameGeoTV.setText("");
        dateGeoTV.setText("");
        commentGeoTechTV.setText("");

//        DataSource ds = new DataSource(getActivity());
//        ds.open();
//        photoData = ds.getPhotoData(Constants.INSPECTION_GRS_DRILLING,
//                dateET.getText().toString(), picET.getText().toString());
//        ds.close();

//        PhotoAdapter photoAdapter= new PhotoAdapter(getActivity(), photoData);
        photoRV.setAdapter(null);
    }

    public void grsDrillingDetailClick() {
        // Hydrology Cardview
        hydroCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentHydroTV.getText().toString().equals("") && dateHydroTV.getText().toString().equals("") &&
                        nameHydroTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department Hydrology", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentHydroTV.setText( "Comment : "+ comment);
                                    dateHydroTV.setText("Date :" + date);
                                    nameHydroTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department Hydrology", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentHydroTV.setText( "Comment : "+ comment);
                                    dateHydroTV.setText("Date :" + date);
                                    nameHydroTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Geology Cardview
        geoCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentGeoTV.getText().toString().equals("") && dateGeoTV.getText().toString().equals("") &&
                        nameGeoTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department Geology", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoTV.setText( "Comment : "+ comment);
                                    dateGeoTV.setText("Date :" + date);
                                    nameGeoTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department Geology", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoTV.setText( "Comment : "+ comment);
                                    dateGeoTV.setText("Date :" + date);
                                    nameGeoTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Geo Technical Cardview
        geoTechCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentGeoTechTV.getText().toString().equals("") && dateGeoTechTV.getText().toString().equals("") &&
                        nameGeoTechTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department GeoTechnical", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoTechTV.setText( "Comment : "+ comment);
                                    dateGeoTechTV.setText("Date :" + date);
                                    nameGeoTechTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department GeoTechnical", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoTechTV.setText( "Comment : "+ comment);
                                    dateGeoTechTV.setText("Date :" + date);
                                    nameGeoTechTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Engineering Cardview
        engineeringCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentEngTV.getText().toString().equals("") && dateEngTV.getText().toString().equals("") &&
                        nameEngTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department Engineering (SRMP)", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentEngTV.setText( "Comment : "+ comment);
                                    dateEngTV.setText("Date :" + date);
                                    nameEngTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department Engineering (SRMP)", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentEngTV.setText( "Comment : "+ comment);
                                    dateEngTV.setText("Date :" + date);
                                    nameEngTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Operation Cardview
        operationCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentOprTV.getText().toString().equals("") && dateOprTV.getText().toString().equals("") &&
                        nameOprTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department Operation (SRMP)", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentOprTV.setText( "Comment : "+ comment);
                                    dateOprTV.setText("Date :" + date);
                                    nameOprTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department Operation (SRMP)", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentOprTV.setText( "Comment : "+ comment);
                                    dateOprTV.setText("Date :" + date);
                                    nameOprTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Geo Engineering Cardview
        geoEngCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentGeoEngTV.getText().toString().equals("") && dateGeoEngTV.getText().toString().equals("") &&
                        nameGeoEngTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Department OH&S Geo Engineering", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoEngTV.setText( "Comment : "+ comment);
                                    dateGeoEngTV.setText("Date :" + date);
                                    nameGeoEngTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Department OH&S Geo Engineering", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGeoEngTV.setText( "Comment : "+ comment);
                                    dateGeoEngTV.setText("Date :" + date);
                                    nameGeoEngTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Grassberg Cardview
        grsCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentGrsTV.getText().toString().equals("") && dateGrsTV.getText().toString().equals("") &&
                        nameGrsTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "GRS Drainage Group", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGrsTV.setText( "Comment : "+ comment);
                                    dateGrsTV.setText("Date :" + date);
                                    nameGrsTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "GRS Drainage Group", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentGrsTV.setText( "Comment : "+ comment);
                                    dateGrsTV.setText("Date :" + date);
                                    nameGrsTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });

        // Pontil Cardview
        pontilCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentPontilTV.getText().toString().equals("") && datePontilTV.getText().toString().equals("") &&
                        namePontilTV.getText().toString().equals("")) {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, false, "Contractor Pontil", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentPontilTV.setText( "Comment : "+ comment);
                                    datePontilTV.setText("Date :" + date);
                                    namePontilTV.setText("Name : " + name);
                                }
                            });
                } else {
                    GrassbergDetailPopUp.grassbergDetailPopUp(mActivity, true, "Contractor Pontil", dateET.getText().toString(),
                            picET.getText().toString(), new GrassbergDetailCallback() {
                                @Override
                                public void onSubmitted(String comment, String date, String name) {
                                    commentPontilTV.setText( "Comment : "+ comment);
                                    datePontilTV.setText("Date :" + date);
                                    namePontilTV.setText("Name : " + name);
                                }
                            });
                }
            }
        });
    }

    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.dataViewHolder> {

        Activity mActivity;
        private ArrayList<PhotoModel> photoModel;

        public class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public final CardView listCV;
            public final ImageView pictureIV;
            public TextView commentTV;
            public ImageView deleteIcon;

            public dataViewHolder(View itemView) {
                super(itemView);
                listCV = (CardView) itemView.findViewById(R.id.listCV);
                this.pictureIV = (ImageView) itemView.findViewById(R.id.pictureIV);
                this.commentTV = (TextView) itemView.findViewById(R.id.commentTV);
                this.deleteIcon = (ImageView) itemView.findViewById(R.id.deleteIcon);

                listCV.setOnClickListener(this);
                deleteIcon.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                if (photoModel != null) {
                    int position = getAdapterPosition();
                    final String fotoPath = photoModel.get(position).getFotoPath();
                    final String comments = photoModel.get(position).getComment();

                    switch (v.getId()) {
                        case R.id.listCV :
                            ImagePopUp.imagePopUp(mActivity, true, Constants.INSPECTION_GRS_DRILLING, fotoPath, comments, dateET.getText().toString(),
                                    picET.getText().toString(), new ImagePopUpCallback() {
                                        @Override
                                        public void onImageSubmitted() {
                                            recycleView();
                                        }
                                    });
                            break;

                        case R.id.deleteIcon :
                            final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
                            builder.setIcon(R.drawable.warning_logo);
                            builder.setTitle("Warning");
                            builder.setMessage("Are you sure to delete this photo?");

                            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    DataSource ds = new DataSource(mActivity);
                                    ds.open();
                                    ds.deletePhoto(fotoPath, comments, dateET.getText().toString(),
                                            picET.getText().toString(), Constants.INSPECTION_GRS_DRILLING);
                                    ds.close();
                                    recycleView();
                                }
                            });

                            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            builder.show();

                            break;

                        default:
                            break;
                    }
                }
            }
        }


        public PhotoAdapter(Activity mActivity, ArrayList<PhotoModel> photoModel) {
            super();
            this.mActivity = mActivity;
            this.photoModel = photoModel;
        }

        @Override
        public PhotoAdapter.dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_photo, parent, false);
            PhotoAdapter.dataViewHolder dataViewHolder = new PhotoAdapter.dataViewHolder(view);
            return dataViewHolder;
        }

        @Override
        public void onBindViewHolder(PhotoAdapter.dataViewHolder holder, int position) {
            TextView commentTV = holder.commentTV;
            ImageView pictureIV = holder.pictureIV;

            commentTV.setText(photoModel.get(position).getComment());
            if (photoModel.get(position).getFotoPath().equals("")) {
                pictureIV.setImageResource(R.drawable.default_nopicture);
            } else {
                Helper.setPic(pictureIV, photoModel.get(position).getFotoPath());
            }
        }

        @Override
        public int getItemCount() {
            return photoModel.size();
        }
    }


    public void recycleView() {
        photoRV = (RecyclerView) rootView.findViewById(R.id.photoRV);
        photoRV.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        photoRV.setLayoutManager(layoutManager);
        photoRV.setItemAnimator(new DefaultItemAnimator());

        DataSource ds = new DataSource(getActivity());
        ds.open();
        photoData = ds.getPhotoData(Constants.INSPECTION_GRS_DRILLING,
                dateET.getText().toString(), picET.getText().toString());
        ds.close();

        PhotoAdapter photoAdapter= new PhotoAdapter(getActivity(), photoData);
        photoRV.setAdapter(photoAdapter);
    }

    public void loadData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();
        // Main Grs Drilling Inspection
        grsDrillingModel = ds.getLatestGRSDrill();
//        if (grsDrillingModel.getGsId() != -1) {
        if (grsDrillingModel != null) {
            picET.setText(grsDrillingModel.getPicName());
            idET.setText(grsDrillingModel.getPicId());
            dateET.setText(grsDrillingModel.getDate());
        }

        if (grsDrillingModel != null) {
            // Department Hydro
            grsDetailHydro = ds.getLatestGRSDetail("Department Hydrology", grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailHydro != null) {
                commentHydroTV.setText("Comment :  "+ grsDetailHydro.getComment());
                dateHydroTV.setText("Date : " + grsDetailHydro.getDate());
                nameHydroTV.setText("Name : " + grsDetailHydro.getSigner());
            }

            // Department Geology
            grsDetailGeology = ds.getLatestGRSDetail("Department Geology", grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailGeology != null) {
                commentGeoTV.setText("Comment : " + grsDetailGeology.getComment());
                dateGeoTV.setText("Date : " + grsDetailGeology.getDate());
                nameGeoTV.setText("Name : " + grsDetailGeology.getSigner());
            }

            // Department Geo Technical
            grsDetailGeoTech = ds.getLatestGRSDetail("Department GeoTechnical", grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailGeoTech != null) {
                commentGeoTechTV.setText("Comment : " + grsDetailGeoTech.getComment());
                dateGeoTechTV.setText("Date : " + grsDetailGeoTech.getDate());
                nameGeoTechTV.setText("Name : " + grsDetailGeoTech.getSigner());
            }

            // Department Operation
            grsDetailOperation = ds.getLatestGRSDetail("Department Operation (SRMP)",
                    grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailOperation != null) {
                commentOprTV.setText("Comment : " + grsDetailOperation.getComment());
                dateOprTV.setText("Date : " + grsDetailOperation.getDate());
                nameOprTV.setText("Name : " + grsDetailOperation.getSigner());
            }

            // Department Engineering
            grsDetailEngineering = ds.getLatestGRSDetail("Department Engineering (SRMP)",
                    grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailEngineering != null) {
                commentEngTV.setText("Comment : " + grsDetailEngineering.getComment());
                dateEngTV.setText("Date : " + grsDetailEngineering.getDate());
                nameEngTV.setText("Name : " + grsDetailEngineering.getSigner());
            }

            // Department Pontil
            grsDetailPontil = ds.getLatestGRSDetail("Contractor Pontil", grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailPontil != null) {
                commentPontilTV.setText("Comment : " + grsDetailPontil.getComment());
                datePontilTV.setText("Date : " + grsDetailPontil.getDate());
                namePontilTV.setText("Name : " + grsDetailPontil.getSigner());
            }

            // Department Grassberg
            grsDetailGrs = ds.getLatestGRSDetail("GRS Drainage Group", grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailGrs != null) {
                commentGrsTV.setText("Comment : " + grsDetailGeology.getComment());
                dateGrsTV.setText("Date : " + grsDetailGeology.getDate());
                nameGrsTV.setText("Name : " + grsDetailGeology.getSigner());
            }

            // Department Geo Engineering
            grsDetailGeoEngineering = ds.getLatestGRSDetail("Department OH&S Geo Engineering",
                    grsDrillingModel.getDate(), grsDrillingModel.getPicName());
            if (grsDetailGeoEngineering != null) {
                commentGeoEngTV.setText("Comment : " + grsDetailGeoEngineering.getComment());
                dateGeoEngTV.setText("Date : " + grsDetailGeoEngineering.getDate());
                nameGeoEngTV.setText("Name : " + grsDetailGeoEngineering.getSigner());
            }
        }

        // Photo
        recycleView();

        ds.close();
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        // Main Grassberg Drilling Inspection
        GrsDrillingModel model = new GrsDrillingModel();

        model.setPicName(picET.getText().toString());
        model.setPicId(idET.getText().toString());
        model.setDate(dateET.getText().toString());

        ds.insertGrsDrilling(model);

        ds.close();
    }

    public static boolean validation() {
        boolean status = true;

        if (picET.getText().toString().length() == 0) {
            status = false;
            picTL.setErrorEnabled(true);
            picTL.setError("Please fill Person in Charge first");
        }

        if (idET.getText().toString().length() == 0) {
            status = false;
            idTL.setErrorEnabled(true);
            idTL.setError("Please fill Person in Charge Id first");
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("Please fill Date first");
        }

        return status;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImagePopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {
            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(ImagePopUp.getPicture(), Helper.fileUri.getPath());
                ImagePopUp.setImagePath(Helper.fileUri.getPath());
            }
            Helper.visiblePage = Helper.visiblePage.grs;
        } else if (requestCode == ImagePopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK && data != null) {
            // new File(tempPhotoDir).mkdirs();
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImagePopUp.setImagePath(picturePath);
            Helper.setPic(ImagePopUp.getPicture(), picturePath);
        }
        Helper.visiblePage = Helper.visiblePage.grs;
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (picET.getText().toString().length() != 0 && idET.getText().toString().length() != 0
//                        && dateET.getText().toString().length() != 0) {
                    saveData();
                    mActivity.finish();
//                } else {
//                    mActivity.finish();
//                }
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("BACK TO MENU", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (picET.getText().toString().length() != 0 && idET.getText().toString().length() != 0
//                        && dateET.getText().toString().length() != 0) {
                if(validateSaveData()) {
                    saveData();
                    Intent intent = new Intent(mActivity, MainMenu.class);
                    mActivity.startActivity(intent);
                    mActivity.finish();
                }
            }
        });

        builder.show();
    }

}
