package com.ptfi.sheinspectionphase2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.ptfi.sheinspectionphase2.Fragments.InformationFragment;
import com.ptfi.sheinspectionphase2.Fragments.LVFragment;
import com.ptfi.sheinspectionphase2.Fragments.UGFragment;
import com.ptfi.sheinspectionphase2.Utils.Helper;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by senaardyputra on 11/17/16.
 */

public class LVActivity extends ActionBarActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tabs)
    PagerSlidingTabStrip tabs;
    @InjectView(R.id.pager)
    ViewPager pager;

    private Activity mActivity = LVActivity.this;

    private MyPagerAdapter adapter;
    private Drawable oldBackground = null;
    private int currentColor;
    private SystemBarTintManager mTintManager;

    LVFragment lvFragment;
    InformationFragment informationFragment;
    int currentPage = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_sliding);

        // Sliding Tab
        ButterKnife.inject(mActivity);
        setSupportActionBar(toolbar);
        // create our manager instance after the content view is set
        mTintManager = new SystemBarTintManager(this);
        // enable status bar tint
        mTintManager.setStatusBarTintEnabled(true);

        //initialize fragment;
        lvFragment = new LVFragment();
        informationFragment = new InformationFragment();

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(2);
        tabs.setViewPager(pager);
        tabs.setIndicatorColor(getResources().getColor(R.color.White));
        tabs.setIndicatorHeight(5);
        tabs.setTextColor(getResources().getColor(R.color.White));
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setCurrentItem(0);
        changeColor(getResources().getColor(R.color.SeaGreen));

        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
                if (position == 0) {
                    Toast.makeText(mActivity, "Tab reselected: " + "Light Vehicle Inspection", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mActivity, "Tab reselected: " + "Management Data", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        informationApp = (ImageView) findViewById(R.id.informationApp);
//        informationApp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        customActionbar();
        return true;
    }

    private void customActionbar() {
        LayoutInflater customBar = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = customBar.inflate(R.layout.header_all_pages, null);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setDisplayShowHomeEnabled(false);
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(false);
        actionbar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        actionbar.setCustomView(actionBarView);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_contact2:
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//                return true;
//
//            case R.id.action_contact:
//                QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public void aboutChangelog(View v) {
//        QuickContactFragment.newInstance().show(getSupportFragmentManager(), "QuickContactFragment");
//    }

    private void changeColor(int newColor) {
        tabs.setBackgroundColor(newColor);
        mTintManager.setTintColor(newColor);
        // change ActionBar color just if an ActionBar is available
        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(getResources().getColor(android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {
            getSupportActionBar().setBackgroundDrawable(ld);
        } else {
            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);
        }

        oldBackground = ld;
        currentColor = newColor;
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"Light Vehicle Inspection", "Data Management"};
//        private Fragment[] fragment = new Fragment[]{lvFragment, informationFragment};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return lvFragment;

                case 1:
//                    return informationFragment;

            }
//           return fragment[position];
            return null;
        }

    }

//    public void datePicker(final View v) {
//        MainFragment.datePickers(PagerSlidingMenu.this, v);
//    }
//
//    public void datePickerGenerated(final View v) {
//        AllGeneratedData.datePickers(PagerSlidingMenu.this, v);
//    }

    @Override
    public void onBackPressed() {
        switch (pager.getCurrentItem()) {
            case 0:
                LVFragment.onBackPressed(this);
                break;

            case 1:
                InformationFragment.onBackPressed(this);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentPage == 0) {
            if(lvFragment.isVisible()) {
                lvFragment.onActivityResult(requestCode, resultCode, data);
                Helper.visiblePage = Helper.visiblePage.lv;
            }
        } else if (currentPage == 1) {
            informationFragment.onActivityResult(requestCode, resultCode, data);
            Helper.visiblePage = Helper.visiblePage.data_management;
        }
    }
}
