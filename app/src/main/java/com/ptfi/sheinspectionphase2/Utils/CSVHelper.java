package com.ptfi.sheinspectionphase2.Utils;

import android.app.Activity;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class CSVHelper {

    private static String QUOTE = "\"";
    private static String ESCAPED_QUOTE = "\"\"";
    private static String ENTER = "\n";
    private static String SPACE = " ";
    private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

    public static boolean isEmpty(char[] array) {
        if (array == null || array.length == 0) {
            return true;
        }
        return false;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static int indexOfAny(String str, char[] searchChars) {
        if (isEmpty(str) || isEmpty(searchChars)) {
            return -1;
        }
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            for (int j = 0; j < searchChars.length; j++) {
                if (searchChars[j] == ch) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static class Csv {
        public static String Escape(String s) {
            if (s.contains(QUOTE))
                s = s.replace(QUOTE, ESCAPED_QUOTE);

            if (indexOfAny(s, CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            if (s.contains(ENTER))
                s = s.replace(ENTER, SPACE);

            return s;
        }
    }

    public static void appendCSV(String path,String filename, List<String[]> values)
    {
        List<String[]> previousList = CSVHelper.readCSVFromPath(path);
        previousList.addAll(values);
        CSVHelper.writeCSV(path,filename, previousList);
    }

    public static boolean writeCSV(String path,String filename,List<String[]> values)
    {
        boolean status = true;
        File f = new File(path);
        if(!(f.exists() && f.isDirectory()))
        {
            f.mkdirs();
        }

        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(path+filename));
            writer.writeAll(values);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }
    public static List<String[]> readCSVFromPath(String path)
    {
        List<String[]> returnValue=null;

        try {
            CSVReader reader = new CSVReader(new FileReader(path));
            if(reader != null)
            {
                returnValue = reader.readAll();
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return returnValue;

    }

    public static void exportLookup(final Activity activity,
                                    ArrayList<String[]> listLookupValue, Helper.Lookup lookupType) {
        ArrayList<String[]> transformedData = new ArrayList<String[]>();
        String[] header = null;
        String exportTypeName = "";

        switch (lookupType) {
            case newData:
                header = new String[]{"Description", "LookUp", "Title", "Organization"};
                transformedData.add(header);

                for (String[] string : listLookupValue) {
                    String[] data = {Csv.Escape(string[0]), Csv.Escape(string[1]),
                            Csv.Escape(string[2]), Csv.Escape(string[3])};
                    transformedData.add(data);
                }
                exportTypeName = "_Data_Manpower";
                break;

            case newDataLookUp:
                header = new String[]{"Potential_Risk", "LookUp"};
                transformedData.add(header);

                for (String[] string : listLookupValue) {
                    String[] data = {Csv.Escape(string[0]), Csv.Escape(string[1])};
                    transformedData.add(data);
                }
                exportTypeName = "_Data_Look_Up";
                break;

//            case findings:
//                header = new String[]{"Company", "Department", "Section", "Inspector", "Date Inspection", "Findings", "Action", "Responsible", "Date Complete", "Work Area", "App Version", "Device IMEI"};
//                transformedData.add(header);
//
//                for (String[] string : listLookupValue) {
//                    String[] data = {Csv.Escape(string[2]), Csv.Escape(string[3]), Csv.Escape(string[4]), Csv.Escape(string[5]), Csv.Escape(string[6]), Csv.Escape(string[7]),
//                            Csv.Escape(string[8]), Csv.Escape(string[12]), Csv.Escape(string[13]), Csv.Escape(string[9]), Csv.Escape(string[14]), Csv.Escape(string[15])};
//                    transformedData.add(data);
//                }
//                exportTypeName = "FSI";
//                break;
//
//            default:
//                break;
        }

        String exportPath = Constants.EXPORT_FOLDER_ON_EXTERNAL_PATH
                + "/" + Constants.LOOKUP_FOLDER_NAME + "/";
        int i = 0;
        boolean isContinueAc = true;
        String filename = "";
        while (isContinueAc) {
            filename = "SHE_INSPECTION_" +
//					(i == 0 ? (exportTypeName + ".csv")
//					: (exportTypeName + "_") + String.valueOf(i))
                    exportTypeName + "_" + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()) + ".csv";

            File checkFileAcquire = new File(exportPath + filename);

            if (checkFileAcquire.exists() && isContinueAc) {
                i++;
            } else
                isContinueAc = false;
        }

//        boolean success = true;
//        if (!CSVHelper.writeCSV(exportPath, filename, transformedData)) {
//            success = false;
//        }
//
//        if (success) {
//            Helper.showPopUpMessage(activity, "Notification",
//                    "Export Csv Success.\nFile saved in folder " + exportPath,
//                    null);
//        } else {
//            Helper.showPopUpMessage(activity, "Notification",
//                    "Export Csv Failed", null);
//        }
    }

}
