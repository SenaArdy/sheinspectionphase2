package com.ptfi.sheinspectionphase2.Utils;

import android.os.Environment;

import com.ptfi.sheinspectionphase2.Models.CustomSpinnerItem;
import com.ptfi.sheinspectionphase2.Models.InspectorModel;
import com.ptfi.sheinspectionphase2.Models.LookUpModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class Constants {

    public static String extStorageDirectory = Environment
            .getExternalStorageDirectory().getAbsolutePath();

    public static String basepath = extStorageDirectory + "/"
            + Constants.ROOT_FOLDER_NAME + "/";

    public static String JSA = "JSA";
    public static String SOP = "SOP";
    public static final String SOP_EN_PDF = "SOP-GS-ALL-140517_INSPEKSI LAPANGAN_DRAFT EN.pdf";
    public static final String SOP_ID_PDF = "SOP-GS-ALL-140517_INSPEKSI LAPANGAN_DRAFT.pdf";
    public static final String SOP_ID_PDF_NEW = "SOP Divisi Geo Services - Pengelolaan & Operasional Kendaraan Ringan Di Area PTFI.pdf";

    public static final int REPORT_GENERATED = 1;
    public static final int REPORT_PREVIEW = 2;
    public static final int REPORT_FAILED = 3;
    public static final int REQUEST_ANDROID_PERMISSIONS = 200;

    public static final int COMPRESS_PERCENTAGE = 0;

    public static String SOP_ID = "SOP Indonesia";
    public static String SOP_EN = "SOP English";

    public static final String ROOT_FOLDER_NAME = "com.PTFI";
    public static final String APP_FOLDER_NAME = "SHE Inspection 2";
    public static final String DOCUMENTS_FOLDER_NAME = "Documents";
    public static final String DB_FOLDER_NAME = "Database";
    public static final String IMAGES_FOLDER_NAME = "Images";
    public static final String IMPORT_FOLDER_NAME = "Import";
    public static final String EXPORT_FOLDER_NAME = "Export";
    public static final String TEMPLATE_FOLDER_NAME = "Template";
    public static final String PHOTO_FOLDER_NAME = "Foto";
    public static final String SCBD_FOLDER_NAME = "SCBD";
    public static final String SCDB_LOOKUP_FOLDER_NAME = "SCDB Lookup";

    public static final String EXTERNAL_ROOT_FOLDER_SCDB = Environment
            .getExternalStorageDirectory().getPath()
            + "/" + ROOT_FOLDER_NAME + "/"
            + SCDB_LOOKUP_FOLDER_NAME + "/";

    public static final String IMPORT_FOLDER_ON_ASSETS_PATH = ROOT_FOLDER_NAME
            + "/"
            + APP_FOLDER_NAME
            + "/"
            + IMPORT_FOLDER_NAME;

    public static final class SCDB_FILE_PREFIX {
        public static final String MANPOWER = "Manpower";
    }

    public static final String COMPANY_FILE_NAME = "COMPANY.csv";
    public static final String BUSINESS_UNIT_FILE_NAME = "BUSINESS_UNIT.csv";
    public static final String DEPARTMENT_FILE_NAME = "DEPARTMENT.csv";
    public static final String SECTION_FILE_NAME = "SECTION.csv";
    public static final String MANPOWER_FILE_NAME = "Manpower_20180914.csv";
    public static final String MANPOWER_SCDB_FILE_NAME = "Manpower_20180914.csv";
    public static final String AVAILABILITY_FILE_NAME = "AVAILABILITY.csv";
    public static final String RATING_FILE_NAME = "RATING.csv";
    public static final String POTENTIAL_RISK_FILE_NAME = "POTENTIAL_RISK.csv";
    public static final String LOOKUP_FILE_NAME = "LOOKUP.csv";

    public static final String TEMPLATE_FILE_NAME = "she_inspection_report_template.doc";
    public static final String TEMPLATE_FIELD_INSPECTION_FILE_NAME = "field_inspection_report_template.doc";
    public static final String TEMPLATE_EX = "test2.docx";
    public static final String TEMPLATE_FILE_LIGHT_VEHICLE = "light_vehicle_inspection_report_template.doc";
    public static final String TEMPLATE_FILE_GS_DRILLING = "gs_drilling_report_template.docx";
    public static final String TEMPLATE_FILE_UG_DRILL_POST = "ug_drilling_post_report_template.doc";
    public static final String TEMPLATE_FILE_UG_DRILL_PRE = "ug_drilling_pre_report_template.doc";
    public static final String TEMPLATE_FILE_LV_GROUP_INSPECTION = "template_lv_group_inspection.docx";

    public static final String GRSD_FOLDER_MARK_NAME = "GRSD";
    public static final String UGD_PRE_FOLDER_MARK_NAME = "UGD_PRE";
    public static final String UGD_POST_FOLDER_MARK_NAME = "UGD_POST";
    public static final String UGD_PRE_PREVIEW_FILE_NAME = "preview_ugd_pre";

    public static final String FILE_TYPE_DOC = ".doc";
    public static final String FILE_TYPE_DOCX = ".docx";
    public static final String FILE_TYPE_PDF = ".pdf";

    public static final String LVI_FOLDER_NAME = "Light Vehicle Inspection";
    public static final String GRS_DRILL_FOLDER_NAME = "GRS Drilling Inspection";
    public static final String UGD_INSPECTION_FOLDER_NAME = "UG Drilling Inspection";
    public static final String UGD_POST_INSPECTION_FOLDER_NAME = "Post Inspection";
    public static final String UGD_PRE_INSPECTION_FOLDER_NAME = "Pre Inspection";
    public static final String PREVIEW_FOLDER_NAME = "preview";
    public static final String GRSD_PREVIEW_FILE_NAME = "preview_gsd";

    public static final String LVI_PREVIEW_FILE_NAME = "preview_lvi";
    public static final String SHE_PLANNED_PREVIEW_FILE_NAME = "preview_she_planned";
    public static final String LVI_FOLDER_MARK_NAME = "LVI";
    public static final String LVI_REPORT_FILE_NAME = "Report";
    public static final String GRSD_REPORT_FILE_NAME = "Report";
    public static final String UGD_PRE_REPORT_FILE_NAME = "Report";

    public static final int INSPECTION_PLANNED = 1;
    public static final int INSPECTION_FIELD = 2;
    public static final int INSPECTION_LVI = 3;
    public static final int INSPECTION_GRS_DRILLING = 4;
    public static final int INSPECTION_UG_DRILLING_PRE = 5;
    public static final int INSPECTION_UG_DRILLING_POST = 6;

    public static final int SEARCH_TYPE_INSPECTOR = 7;
    public static final int SEARCH_TYPE_AREA_OWNER = 8;
    public static final int SEARCH_TYPE_SITE_SPV = 9;
    public static final int SEARCH_TYPE_DETP_MGR = 10;
    public static final int SEARCH_TYPE_AREA_SAFETY = 11;
    public static final int SEARCH_TYPE_SPI_POPUP = 12;
    public static final int SEARCH_TYPE_FPI_POPUP = 13;
    public static final int SEARCH_TYPE_PIC = 14;
    public static final int SEARCH_TYPE_DRIVER_DAILY = 15;
    public static final int SEARCH_TYPE_DRIVER_WEEKLY = 16;
    public static final int SEARCH_TYPE_DRIVER_DAMAGE = 17;
    public static final int SEARCH_TYPE_SUPERVISOR = 18;
    public static final int SEARCH_TYPE_SIGNER_OFF = 19;


    // PHOTO GRS DRILLING
    public static final String KEY_FILE_PATH_PREVIEW = "preview";
    public static final String KEY_FOLDER_PATH = "folderpath";
    public static String tempPhotosDir = basepath + "/" + APP_FOLDER_NAME + "/"
            + EXPORT_FOLDER_NAME + "/" + "temp_photos/";

    // ======================================
    // ROOT FOLDER FILES
    // ======================================
    public static final String EXTERNAL_ROOT_FOLDER = Environment
            .getExternalStorageDirectory().getPath()
            + "/com.PTFI/"
            + APP_FOLDER_NAME + "/";

    public static final String EXPORT_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + EXPORT_FOLDER_NAME;

    public static final String LOOKUP_FOLDER_NAME = "Export Look Up";

    public static final String IMPORT_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER + IMPORT_FOLDER_NAME;

    public static final String SCBD_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER + SCBD_FOLDER_NAME;

    // =======================================
    // DATABASE
    // =======================================
    public static final String DB_FOLDER_ON_EXTERNAL_PATH = EXTERNAL_ROOT_FOLDER
            + DB_FOLDER_NAME;

    private static ArrayList<ManpowerModel> OBSERVER_DICTIONARY = new ArrayList<ManpowerModel>();
    private static ArrayList<LookUpModel> LOOKUP_DICTIONARY = new ArrayList<LookUpModel>();
    private static ArrayList<String> RATING_DICTIONARY = new ArrayList<String>();
    private static ArrayList<InspectorModel> INSPECTOR_DICTIONARY = new ArrayList<InspectorModel>();
    private static ArrayList<String> POTENTIAL_RISK_DICTIONARY = new ArrayList<String>();
    private static ArrayList<String> MANPOWER_DICTIONARY = new ArrayList<String>();
    private static ArrayList<String> AVAILABILITY_DICTIONARY = new ArrayList<String>();
    private static ArrayList<CustomSpinnerItem> COMPANY_DICTIONARY = new ArrayList<CustomSpinnerItem>();

    public static ArrayList<ManpowerModel> getObserverDictionary() {
        return OBSERVER_DICTIONARY;
    }

    public static void setObserverDictionary(ArrayList<ManpowerModel> obs) {
        OBSERVER_DICTIONARY = obs;
    }

    public static void setLookUpDictionary(ArrayList<LookUpModel> obs) {
        LOOKUP_DICTIONARY = obs;
    }

    /**
     * @param rATING_DICTIONARY
     *            the rATING_DICTIONARY to set
     */
    public static void setRATING_DICTIONARY(ArrayList<String> rATING_DICTIONARY) {
        RATING_DICTIONARY = rATING_DICTIONARY;
    }

    public static ArrayList<String> getRatingDictionary() {
        return RATING_DICTIONARY;
    }

    public static ArrayList<String> getManpowerDictionary() {
        return MANPOWER_DICTIONARY;
    }

    public static void setManpowerDictionary(ArrayList<String> manpower) {
        MANPOWER_DICTIONARY = manpower;
    }

    public static ArrayList<String> getAVAILABILITY_DICTIONARY() {
        return AVAILABILITY_DICTIONARY;
    }

    public static void setAVAILABILITY_DICTIONARY(
            ArrayList<String> aVAILABILITY_DICTIONARY) {
        AVAILABILITY_DICTIONARY = aVAILABILITY_DICTIONARY;
    }

    public static ArrayList<InspectorModel> getInspectorDictionary() {
        return INSPECTOR_DICTIONARY;
    }

    public static void setInspectorDictionary(ArrayList<InspectorModel> inspect) {
        INSPECTOR_DICTIONARY = inspect;
    }

    public static void setPOTENTIAL_RISK_DICTIONARY(
            ArrayList<String> pOTENTIAL_RISK_DICTIONARY) {
        POTENTIAL_RISK_DICTIONARY = pOTENTIAL_RISK_DICTIONARY;
    }

    public static void setCompanyDictionary(ArrayList<CustomSpinnerItem> company) {
        COMPANY_DICTIONARY = company;
    }

    public static ArrayList<String> getPotentialRiskDictionary() {
        return POTENTIAL_RISK_DICTIONARY;
    }
}
