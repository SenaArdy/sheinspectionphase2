package com.ptfi.sheinspectionphase2.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aspose.words.Cell;
import com.aspose.words.CellFormat;
import com.aspose.words.CellVerticalAlignment;
import com.aspose.words.Document;
import com.aspose.words.DocumentBuilder;
import com.aspose.words.Font;
import com.aspose.words.ImageSize;
import com.aspose.words.NodeType;
import com.aspose.words.Paragraph;
import com.aspose.words.ParagraphAlignment;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Shape;
import com.aspose.words.Table;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Fragments.FieldFragment;
import com.ptfi.sheinspectionphase2.Fragments.PlannedFragment;
import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.Models.FieldMainModel;
import com.ptfi.sheinspectionphase2.Models.GrsDetailModel;
import com.ptfi.sheinspectionphase2.Models.InspectorUGModel;
import com.ptfi.sheinspectionphase2.Models.LVDailyModel;
import com.ptfi.sheinspectionphase2.Models.LVDamageModel;
import com.ptfi.sheinspectionphase2.Models.LVWeeklyModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;
import com.ptfi.sheinspectionphase2.Models.PlannedInspectionModel;
import com.ptfi.sheinspectionphase2.Models.PlannedSignModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingInspectionModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingMainModel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.lang.String.valueOf;

/**
 * Created by senaardyputra on 11/22/16.
 */

public class Reporting {

    static String baseReportPath = Constants.basepath + "/"
            + Constants.APP_FOLDER_NAME + "/" + Constants.EXPORT_FOLDER_NAME
            + "/" + Constants.GRS_DRILL_FOLDER_NAME;

    private static final int OPEN_PREVIEW = 323;
    private static final int PREVIEW_CODE_SHE_PLANNED = 404;
    private static final int PREVIEW_CODE_SHE_UG = 405;

    static String folderDirectory = "";
    private static String previewFilePath;
    private static boolean isPreview = false;
    static ArrayList<ManpowerModel> selectedInspectorList = new ArrayList<ManpowerModel>();

    static List<PhotoModel> fotoList = new ArrayList<PhotoModel>();

    static String passCheck = Html.fromHtml("&#10004;").toString();

    public static void generateReportGRS(final Activity mActivity, final String pic, final String date, final boolean isGenerate) {
        // generate or preview
        Helper.showProgressDialog(mActivity);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                // delete all templates file on template folder
                Helper.deleteAllFilesOnDirectory(Constants.extStorageDirectory
                        + "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME);

                // copy wanted template on to template folder
                Helper.copyFolderAndContent(Constants.extStorageDirectory + "/"
                                + Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        Constants.ROOT_FOLDER_NAME + "/"
                                + Constants.APP_FOLDER_NAME + "/"
                                + Constants.TEMPLATE_FOLDER_NAME,
                        mActivity);

                String templatePath = Environment.getExternalStorageDirectory()
                        .getAbsolutePath();
                templatePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FILE_GS_DRILLING;

                try {
                    final Document doc = new Document(templatePath);
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE,
                            0, true);
                    Row templateRow;
                    Cell cell;
                    int count = 1;
                    DataSource ds = new DataSource(mActivity);
                    ds.open();

                    // Details Table
                    /* ========================
                                Department Hydrology
                    ======================== */
                    GrsDetailModel hydro = ds.getLatestGRSDetail("Department Hydrology", date, pic);
                    doc.getRange().replace("DEPARTMENT_1", hydro.getDepartment(), true, true);
                    doc.getRange().replace("Comment_1", hydro.getComment(), true, true);
                    doc.getRange().replace("Date_1", hydro.getDate(), true, true);
                    if (hydro.getSign() != null && !hydro.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(1);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(hydro.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                     /* ========================
                                Department Geology
                    ======================== */
                    GrsDetailModel geology = ds.getLatestGRSDetail("Department Geology", date, pic);
                    doc.getRange().replace("DEPARTMENT_2", geology.getDepartment(), true, true);
                    doc.getRange().replace("Comment_2", geology.getComment(), true, true);
                    doc.getRange().replace("Date_2", geology.getDate(), true, true);
                    if (geology.getSign() != null && !geology.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(2);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(geology.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department GeoTechnical
                    ======================== */
                    GrsDetailModel GeoTechnical = ds.getLatestGRSDetail("Department GeoTechnical", date, pic);
                    doc.getRange().replace("DEPARTMENT_3", GeoTechnical.getDepartment(), true, true);
                    doc.getRange().replace("Comment_3", GeoTechnical.getComment(), true, true);
                    doc.getRange().replace("Date_3", GeoTechnical.getDate(), true, true);
                    if (GeoTechnical.getSign() != null && !GeoTechnical.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(3);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(GeoTechnical.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department Operation
                    ======================== */
                    GrsDetailModel Operation = ds.getLatestGRSDetail("Department Operation (SRMP)", date, pic);
                    doc.getRange().replace("DEPARTMENT_4", Operation.getDepartment(), true, true);
                    doc.getRange().replace("Comment_4", Operation.getComment(), true, true);
                    doc.getRange().replace("Date_4", Operation.getDate(), true, true);
                    if (Operation.getSign() != null && !Operation.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(4);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Operation.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                             Department Operation
                    ======================== */
                    GrsDetailModel Engineering = ds.getLatestGRSDetail("Department Engineering (SRMP)", date, pic);
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_5", Engineering.getDepartment(), true, true);
                    doc.getRange().replace("Comment_5", Engineering.getComment(), true, true);
                    doc.getRange().replace("Date_5", Engineering.getDate(), true, true);
                    if (Engineering.getSign() != null && !Engineering.getSign().equals("")) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(5);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Engineering.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                                 Department Pontil
                    ======================== */
                    GrsDetailModel Pontil = ds.getLatestGRSDetail("GRS Drainage Group", date, pic);
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_6", Pontil.getDepartment(), true, true);
                    doc.getRange().replace("Comment_6", Pontil.getComment(), true, true);
                    doc.getRange().replace("Date_6", Pontil.getDate(), true, true);
                    if (Pontil.getSign() != null && !Pontil.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(6);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(Pontil.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                                 Department GRS
                    ======================== */
                    GrsDetailModel GRS = ds.getLatestGRSDetail("GRS Drainage Group", date, pic);
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_7", GRS.getDepartment(), true, true);
                    doc.getRange().replace("Comment_7", GRS.getComment(), true, true);
                    doc.getRange().replace("Date_7", GRS.getDate(), true, true);
                    if (GRS.getSign() != null && !GRS.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(7);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(GRS
                                .getSign());

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ========================
                              Department GeoEng
                    ======================== */
                    GrsDetailModel GeoEng = ds.getLatestGRSDetail("Department OH&S Geo Engineering", date, pic);
//                    if (Operation.getDepartment().equals(""))
                    doc.getRange().replace("DEPARTMENT_8", GeoEng.getDepartment(), true, true);
                    doc.getRange().replace("Comment_8", GeoEng.getComment(), true, true);
                    doc.getRange().replace("Date_8", GeoEng.getDate(), true, true);
                    if (GeoEng.getSign() != null && !GeoEng.getSign().equals("") ) {
                        Table table1 = (Table) doc.getChild(NodeType.TABLE, 0, true);
                        Row row1 = (Row) table1.getRows().get(8);
                        Cell dataCell1 = row1.getCells().get(4);
                        CellFormat format = dataCell1.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(dataCell1.getFirstParagraph());
                        Shape image = builder.insertImage(Helper.stringToBitmap(GeoEng.getSign()));

                        double freePageWidth = width;

                        // Is one of the sides of this image too big for the
                        // page?
                        ImageSize size = image.getImageData()
                                .getImageSize();
                        boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                        if (exceedsMaxPageSize) {
                            // Calculate the ratio to fit the page size
                            double ratio = freePageWidth
                                    / size.getWidthPoints();

                            Log.d("IMAGE", "widthlonger : " + ""
                                    + " | ratio : " + ratio);

                            // Set the new size.
                            image.setWidth(size.getWidthPoints() * ratio);
                            image.setHeight(size.getHeightPoints() * ratio);
                        }
                    }

                    /* ====================
                                        PHOTO
                    ==================== */
                    Table photoTableDoc = (Table) doc.getChild(NodeType.TABLE,
                            1, true);
                    RowCollection photoTableRows = photoTableDoc.getRows();
                    int photoRowStartingIndex = 2;
                    int photoRowDummyIndex = 1;
                    int photoColStartingIndex = 0;
                    int photoRowCount = 0;

                    int counts = 0;
                    ArrayList<PhotoModel> fotoModel = ds.getPhotoData(Constants.INSPECTION_GRS_DRILLING, date, pic);
                    ds.close();

                    Row dataRow;
                    Row clonedRow;
                    clonedRow = (Row) photoTableRows
                            .get(photoRowDummyIndex);
                    for (int a = 0; a < fotoModel.size(); a ++) {
                        dataRow = (Row) clonedRow.deepClone(true);
                        photoTableDoc.getRows().add(dataRow);
                        counts++;
//                        if (photoRowCount > 0) {
                        // copy 1st data row
//                        } else {
//                            // 1st row of table is dummy row (for copy cell
//                            // purpose)
//                            dataRow = (Row) photoTableRows
//                                    .get(photoRowStartingIndex);
//                        }

                        dataRow.getRowFormat().setAllowBreakAcrossPages(true);

                        // get the cell for each attribute of foto (number,
                        // photo, and comment)
                        Cell photoNumberCell = dataRow.getCells().get(
                                photoColStartingIndex + 0);
                        Cell photoPhotoCell = dataRow.getCells().get(
                                photoColStartingIndex + 1);
                        Cell photoCommentCell = dataRow.getCells().get(
                                photoColStartingIndex + 2);

                        // set each cell background to white
                        photoNumberCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoPhotoCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        photoCommentCell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);

                        // clear each cell
                        photoNumberCell.getFirstParagraph().getRuns().clear();
                        photoPhotoCell.getFirstParagraph().getRuns().clear();
                        photoCommentCell.getFirstParagraph().getRuns().clear();

                        // fill the data to each cell
                        // number data
                        photoNumberCell.getFirstParagraph()
                                .appendChild(
                                        new Run(doc, valueOf(counts)));

                        // photo
                        CellFormat format = photoPhotoCell.getCellFormat();
                        double width = format.getWidth();
                        builder.moveTo(photoPhotoCell.getFirstParagraph());
                        Bitmap imgBitmap = null;// Helper.getBitmapFromFile(foto.getFotoPath());

                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(fotoModel.get(a).getFotoPath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 90, (int) width);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 180, (int) width);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(
                                            fotoModel.get(a).getFotoPath(), 0, (int) width);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (imgBitmap != null) {
                            // Shape image = builder.insertImage(imgBitmap);
                            ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                            imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                                    Constants.COMPRESS_PERCENTAGE, tempImage);
                            Bitmap compressedBitmap = BitmapFactory
                                    .decodeStream(new ByteArrayInputStream(
                                            tempImage.toByteArray()));
                            Shape image = builder.insertImage(compressedBitmap);

                            double freePageWidth = width;
                            // Is one of the sides of this image too big for the
                            // page?
                            ImageSize size = image.getImageData()
                                    .getImageSize();
                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;

                            if (exceedsMaxPageSize) {
                                // Calculate the ratio to fit the page size
                                double ratio = freePageWidth
                                        / size.getWidthPoints();

                                Log.e("IMAGE", "widthlonger : " + ""
                                        + " | ratio : " + ratio);

                                // Set the new size.
                                image.setWidth(size.getWidthPoints() * ratio);
                                image.setHeight(size.getHeightPoints() * ratio);
                            }
                        }

                        // comment
                        photoCommentCell.getFirstParagraph().appendChild(
                                new Run(doc, fotoModel.get(a).getComment()));

//                        if (photoRowCount > 0) {
//                            // insert row
//                        }
                        photoRowCount++;
                    }

//                    photoTableDoc.getRows().removeAt(1);

                    Row dummyRow = photoTableRows.get(1);
                    dummyRow.remove();

                    // SAVE FILE
                    if (isGenerate) {
                        // GENERATE FILE NAME FORMAT
                        String baseName = Constants.GRSD_REPORT_FILE_NAME;

                        String formattedID = pic;
                        String currentTime = new SimpleDateFormat(
                                "yyyy_MM_dd_HH_mm").format(new Date());
                        String baseFolderName = currentTime + "_"
                                + Constants.GRSD_FOLDER_MARK_NAME + "_"
                                + formattedID;

                        String thisReportPath = baseReportPath + File.separator
                                + baseFolderName;
                        Helper.checkAndCreateDirectory(thisReportPath);
                        folderDirectory = thisReportPath;

                        String previewFolder = thisReportPath + "/"
                                + Constants.PREVIEW_FOLDER_NAME;

                        doc.save(thisReportPath + "/" + baseName
                                + Constants.FILE_TYPE_DOCX);

                        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
                            doc.save(thisReportPath + "/" + baseName
                                + Constants.FILE_TYPE_PDF);

                        // copy photos to folderDirectory
                        // photo
                        for (PhotoModel foto : fotoList) {
                            if (null != foto.getFotoPath()) {
                                InputStream is;
                                try {
                                    // trying to copy image to folderDirectory
                                    // based on its path
                                    is = new FileInputStream(foto.getFotoPath());

                                    File fileImage = new File(foto
                                            .getFotoPath());
                                    if (fileImage.exists()) {
                                        String photoDirectory = folderDirectory
                                                + "/"
                                                + Constants.PHOTO_FOLDER_NAME;
                                        new File(photoDirectory).mkdirs();
                                        String outputPath = photoDirectory
                                                + File.separator + foto.getId()
                                                + ".jpg";
                                        OutputStream os = new FileOutputStream(
                                                outputPath);
                                        Helper.copyFile(is, os);
                                        // foto.setFotoPath(outputPath);
                                    }

                                } catch (FileNotFoundException e) {
                                    // TODO Auto-generated catch block
//                                    Log.e("Error create photo folder",
//                                            e.getMessage());
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
//                                    Log.e("Error create photo folder",
//                                            e.getMessage());
                                }
                            }
                        }

                    } else {
                        // preview
                        String previewFolder = Helper.getPathForPreviewFile();
                        String fileNamePdf = Constants.GRSD_PREVIEW_FILE_NAME
                                +
                                (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M ?
                                Constants.FILE_TYPE_PDF :
                                Constants.FILE_TYPE_DOCX);

                        Helper.checkAndCreateDirectory(previewFolder);
                        previewFilePath = previewFolder + "/" + fileNamePdf;
                        doc.save(previewFilePath);
                    }

                    String reportPath = folderDirectory + "/"
                            + Constants.GRSD_REPORT_FILE_NAME
                            + Constants.FILE_TYPE_DOCX;

                    File file = new File(isGenerate ? reportPath
                            : previewFilePath);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    String type = isGenerate ? "application/msword"
                            : android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M ? "application/msword" : "application/pdf";
                    intent.setDataAndType(Uri.fromFile(file), type);

                    Helper.progressDialog.dismiss();
                    if (isGenerate) {
                        Helper.showPopUpMessage(mActivity, "",
                                "A report generated\n file has been saved in directory : "
                                        + folderDirectory, null);
                    } else {
                        isPreview = true;
                        mActivity.setResult(mActivity.RESULT_OK, intent);
                    }
                    mActivity.startActivityForResult(intent, OPEN_PREVIEW);
					/*
					 * new Handler().postDelayed(new Runnable() {
					 *
					 * @Override public void run() {
					 * Helper.deleteFileOnThisPath(
					 * previewFilePath+"/"+Constants.GRSD_PREVIEW_FILE_NAME +
					 * Constants.FILE_TYPE_PDF); //delete preview folder File
					 * previewDir = new File(previewFilePath); boolean status =
					 * Helper.deleteDirectory(previewDir);
					 * Log.e("Preview Directory Delete Status",""+status); } },
					 * 3000*5);
					 */
                } catch (Exception ex) {
                    new Exception(ex.getMessage());
                }
            }
        }, 500);

    }

    /* =======================
             Generate Report Field
    ======================= */
    public static void generateReportField(final Activity mActivity, final String inspector, final String date, final String dept, final String section,
                                           final String company, final String workArea, final String version, final boolean isGenerate,
                                           final ArrayList<FieldFindingsModel> fieldFindingsData) {

        String reportPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath();
        reportPath += "/" + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FIELD_INSPECTION_FILE_NAME;

        // GENERATE FORMATTED FILE NAME
        String fileName;
        String fileNamePdf = "";
        String formattedID = inspector;
        String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        String docName = time + "_FSI_" + formattedID;
        String folderPath;
        if (isGenerate) {
            folderPath = Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/"
                    + Constants.ROOT_FOLDER_NAME
                    + "/"
                    + Constants.APP_FOLDER_NAME
                    + "/"
                    + Constants.EXPORT_FOLDER_NAME + "/" + docName;
            File folder = new File(folderPath);
            folder.mkdir();

            fileName = folder + "/" + docName + ".doc";
            fileNamePdf = folder + "/" + docName + ".pdf";
            FieldFragment.exportCsv(folderPath + "/", docName + ".csv");
        } else
            fileName = Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/"
                    + Constants.ROOT_FOLDER_NAME
                    + "/"
                    + Constants.APP_FOLDER_NAME
                    + "/"
                    + Constants.EXPORT_FOLDER_NAME
                    + "/"
                    + "report_she_field_inspection_preview.pdf";

        // CREATE IMAGE FOLDER
        String imageFolderPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath()
                + "/"
                + Constants.ROOT_FOLDER_NAME
                + "/"
                + Constants.APP_FOLDER_NAME
                + "/"
                + Constants.EXPORT_FOLDER_NAME + "/" + docName;
        File imageFolder = new File(imageFolderPath);

        try {
            Document doc = new Document(reportPath);

            // header report
            doc.getRange().replace(
                    "departmet_section_id",
                    dept
                            + (section.equalsIgnoreCase("-") ? "" : " - "
                            + section), true, true);
            doc.getRange().replace("date_id", date, true, true);

            TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
            String deviceID = telephonyManager.getDeviceId();

            doc.getRange().replace("device_id", "Device IMEI : " + deviceID,
                    true, true);
            doc.getRange().replace("version_id",
                    "SHE Inspection Version " + version, true, true);
            doc.getRange().replace("inspectors_id", inspector, true, true);
            doc.getRange().replace("work_area_id", workArea, true, true);

            // content
            Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 1, true);
            Row templateRow = (Row) sampleIdTable.getLastRow();
            TextView columnTV;
            DataSource ds = new DataSource(mActivity);
            ds.open();

//            ArrayList<FieldFindingsModel>  = ds.getFieldFindingsData(company, dept,
//                    section, inspector, date, Constants.INSPECTION_FIELD);
            ds.close();

            int photoDummyIndex = 1;

            int count = 0;
            Row dataRow;
            Row clonedRow;
            RowCollection photoTableRows = sampleIdTable.getRows();
            clonedRow = (Row) photoTableRows.get(photoDummyIndex);
            for (int i= 0; i < fieldFindingsData.size(); i ++ ) {
                count++;
                dataRow = (Row) clonedRow.deepClone(true);
                sampleIdTable.getRows().add(dataRow);
                dataRow.getRowFormat().setAllowBreakAcrossPages(true);
//                    View v = dataFindings.getChildAt(i);

                // column 1
                Cell cell = dataRow.getCells().get(0);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, valueOf(count)));

                // column 2
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.INSPECTION);
                cell = dataRow.getCells().get(1);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getFindings()));

                // column 3
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_ACTION);
                cell = dataRow.getCells().get(2);
                for (Run run : cell.getFirstParagraph().getRuns()) {
                    // Set some font formatting properties
                    Font font = run.getFont();
                    font.setColor(Color.BLACK);
                }
                cell.getFirstParagraph().getRuns().clear();
                cell.getCellFormat().getShading()
                        .setBackgroundPatternColor(Color.WHITE);
                cell.getFirstParagraph().appendChild(
                        new Run(doc, fieldFindingsData.get(i).getAction()));

                // column 4
                cell = dataRow.getCells().get(3);
                cell.getFirstParagraph().getRuns().clear();
                if (!fieldFindingsData.get(i).getPicturePath()
                        .equalsIgnoreCase("")) {
                    DocumentBuilder builder = new DocumentBuilder(doc);
                    CellFormat format = cell.getCellFormat();
                    double width = format.getWidth();
                    double height = cell.getParentRow().getRowFormat()
                            .getHeight();
                    builder.moveTo(cell.getFirstChild());

                    Bitmap imgBitmap = null;
                    if(isGenerate) {
                        imgBitmap = BitmapFactory.decodeFile(fieldFindingsData.get(i).getPicturePath());
                    } else {
                        try {
                            ExifInterface ei;
                            ei = new ExifInterface(fieldFindingsData.get(i).getPicturePath());
                            int orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 90,
                                            150);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 180,
                                            150);
                                    break;
                                default:
                                    imgBitmap = Helper.rotateImage(fieldFindingsData.get(i).getPicturePath(), 0,
                                            150);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    // Shape image = builder.insertImage(imgBitmap);
                    ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
                    imgBitmap.compress(Bitmap.CompressFormat.JPEG,
                            Constants.COMPRESS_PERCENTAGE, tempImage);
                    Bitmap compressedBitmap = BitmapFactory
                            .decodeStream(new ByteArrayInputStream(
                                    tempImage.toByteArray()));
                    Shape image = builder.insertImage(compressedBitmap);

                    double freePageWidth = width;
                    double freePageHeight = height;

                    // Is one of the sides of this image too big for the
                    // page?
                    ImageSize size = image.getImageData().getImageSize();
                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth
                            || size.getHeightPoints() > freePageHeight;

                    if (exceedsMaxPageSize) {
                        // Calculate the ratio to fit the page size
                        double ratio = freePageWidth
                                / size.getWidthPoints();

                        // Set the new size.
                        image.setWidth(size.getWidthPoints() * ratio);
                        image.setHeight(size.getHeightPoints() * ratio);
                    }

                    if (isGenerate) {
                        if (!imageFolder.exists()) {
                            imageFolder.mkdir();
                        }

                        // MOVE IMAGE TO REPORT FOLDER
                        File old = new File(fieldFindingsData.get(i).getPicturePath());
                        String name = old.getName();
//
                        InputStream in = new FileInputStream(fieldFindingsData.get(i).getPicturePath());
                        OutputStream out = new FileOutputStream(
                                imageFolderPath + "/" + name);
                        Helper.copyFile(in, out);
                        old.delete();

                        FieldFindingsModel fm = fieldFindingsData.get(i);
                        fm.setPicturePath(imageFolderPath + "/" + name);
                        fieldFindingsData.set(i, fm);
                    }
                    else {
                        for (Run run : cell.getFirstParagraph().getRuns()) {
//                            // Set some font formatting properties
                            Font font = run.getFont();
                            font.setColor(Color.BLACK);
                        }
                        cell.getCellFormat().getShading()
                                .setBackgroundPatternColor(Color.WHITE);
                        cell.getFirstParagraph().appendChild(new Run(doc, ""));
                    }

                    // column 5
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_RESPONSIBLE);
                    cell = dataRow.getCells().get(4);
                    for (Run run : cell.getFirstParagraph().getRuns()) {
                        // Set some font formatting properties
                        Font font = run.getFont();
                        font.setColor(Color.BLACK);
                    }
                    cell.getFirstParagraph().getRuns().clear();
                    cell.getCellFormat().getShading()
                            .setBackgroundPatternColor(Color.WHITE);
                    cell.getFirstParagraph().appendChild(
                            new Run(doc, fieldFindingsData.get(i).getResponsible()));

                    // column 6
//                    columnTV = (TextView) v
//                            .findViewWithTag(Constants.FIELD_DATE);
                    cell = dataRow.getCells().get(5);
                    for (Run run : cell.getFirstParagraph().getRuns()) {
                        // Set some font formatting properties
                        Font font = run.getFont();
                        font.setColor(Color.BLACK);
                    }
                    cell.getFirstParagraph().getRuns().clear();
                    cell.getCellFormat().getShading()
                            .setBackgroundPatternColor(Color.WHITE);
                    cell.getFirstParagraph().appendChild(
                            new Run(doc, fieldFindingsData.get(i).getDateComplete()));

//                    sampleIdTable.appendChild(dataRow);
                }
            }

            Row dummyRow = photoTableRows.get(1);
            dummyRow.remove();

            doc.save(fileName);
            if (isGenerate)
                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
                    doc.save(fileNamePdf);

            File file;
            if (isGenerate) {
                file = android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M ? new File(fileName) : new File(fileNamePdf);
            } else {
                file = new File(fileName);
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            String type = android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M ? "application/msword" : "application/pdf";
            intent.setDataAndType(Uri.fromFile(file), type);

            // IF IS PREVIEW, SET RESULT
            if (isGenerate) {
                Helper.showPopUpMessage(mActivity, "",
                        "A report generated\n file has been saved in folder : "
                                + fileName, null);
                mActivity.startActivity(intent);
            } else {
                mActivity.startActivityForResult(intent, OPEN_PREVIEW);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

//    public void exportCsv(String path, String filename) {
//        ArrayList<String[]> transformedData = new ArrayList<String[]>();
//        String department = departmentET.getText().toString();
//        String section = sectionET.getText().toString();
//        String company = companyET.getText().toString();
//        String workArea = locationET.getText().toString();
//
//        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
//
//        String[] versions = { "App Version", version, "Action" };
//        transformedData.add(versions);
//        String[] deviceID = { "Device IMEI", "'" + telephonyManager.getDeviceId() };
//        transformedData.add(deviceID);
//        String[] header1 = { "Company", company };
//        transformedData.add(header1);
//        String[] header2 = { "Department", department };
//        transformedData.add(header2);
//        String[] header3 = { "Section", section };
//        transformedData.add(header3);
//        String[] header4 = { "Date",
//                new SimpleDateFormat("dd-MMM-yyyy").format(new Date()) };
//        transformedData.add(header4);
//        String[] header5 = { "Inspector", inspectorET.getText().toString() };
//        transformedData.add(header5);
//        String[] header6 = { "Work Area / Location",
//                locationET.getText().toString() };
//        transformedData.add(header6);
//        transformedData.add(new String[] { null });
//        String[] header7 = { "No", "Finding", "Action", "PictureName",
//                "ResponsiblePerson", "DateComplete" };
//        transformedData.add(header7);
//
//        int count = 0;
//        for (int i = 0; i < inspectionData.size(); i++) {
//            FieldInspectionModel fm = inspectionData.get(i);
//            if (!fm.isEmptyData()) {
//                count++;
//                String[] content = {
//                        String.valueOf(count),
//                        fm.getfieldFinding(),
//                        fm.getfieldAction(),
//                        fm.getfieldPicturePath().substring(
//                                fm.getfieldPicturePath().lastIndexOf("/") + 1,
//                                fm.getfieldPicturePath().length()),
//                        fm.getfieldResponsible(), fm.getfieldDateComplete() };
//                transformedData.add(content);
//            }
//        }
//        if (CSVHelper.writeCSV(path, filename, transformedData)) {
//            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
//            // "Export CSV berhasil\n file tersimpan di folder : "
//            // + exportPath + fileName, null);
//            mActivity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    Toast.makeText(mActivity,
//                            "Export CSV berhasil", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//        } else {
//            // Helper.showPopUpMessage(FieldSHEInspectionActivity.this, "",
//            // "Export CSV gagal", null);
//            mActivity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    Toast.makeText(mActivity,
//                            "Export CSV gagal", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//    }

    /* ========================
            Generate Report UG Drilling
    ======================== */
//    public static void generateReportUG(final Activity mActivity, final String projectOwner, final String projectName, final String holeId,
//                                        final String dateIns, final boolean isGenerate) {
//
//        Helper.showProgressDialog(mActivity);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                String templateDocPath = Constants.basepath + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FILE_UG_DRILL_PRE;
//
//                // delete all templates file on template folder
//                Helper.deleteAllFilesOnDirectory(Constants.extStorageDirectory
//                        + "/" + Constants.ROOT_FOLDER_NAME + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME);
//
//                // copy wanted template on to template folder
//                Helper.copyFolderAndContent(Constants.extStorageDirectory + "/"
//                                + Constants.ROOT_FOLDER_NAME + "/"
//                                + Constants.APP_FOLDER_NAME + "/"
//                                + Constants.TEMPLATE_FOLDER_NAME,
//                        Constants.ROOT_FOLDER_NAME + "/"
//                                + Constants.APP_FOLDER_NAME + "/"
//                                + Constants.TEMPLATE_FOLDER_NAME,
//                        mActivity);
//
//                String templatePath = Environment.getExternalStorageDirectory()
//                        .getAbsolutePath();
//                templatePath += "/" + Constants.ROOT_FOLDER_NAME + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FILE_UG_DRILL_PRE;
//
//                try {
//                    final Document doc = new Document(templateDocPath);
//                    DocumentBuilder builder = new DocumentBuilder(doc);
//
//                    TelephonyManager telephonyManager = (TelephonyManager) mActivity
//                            .getSystemService(Context.TELEPHONY_SERVICE);
//                    String versionName;
//                    try {
//                        versionName = mActivity.getPackageManager().getPackageInfo(
//                                mActivity.getPackageName(), 0).versionName;
//                    } catch (PackageManager.NameNotFoundException e) {
//                        versionName = "version name cannot be detected";
//                    }
//
//                    doc.getRange().replace("_device_id",
//                            "device IMEI : " + telephonyManager.getDeviceId(), true,
//                            true);
//                    doc.getRange().replace("_version_application",
//                            "SHE Inspection Application Version : " + versionName,
//                            true, true);
//
//                    DataSource ds = new DataSource(mActivity);
//                    ds.open();
//
//                    UGDrillingMainModel mainModel = ds.getDataUGReport(projectOwner, projectName, dateIns, holeId);
//                    doc.getRange().replace("_inspection_date", dateIns, true, true);
//                    doc.getRange().replace("_project_owner", projectOwner, true, true);
//                    doc.getRange().replace("_project_name", projectName, true, true);
//                    doc.getRange().replace("_hole_id", holeId, true, true);
//                    if (mainModel != null) {
//                        doc.getRange().replace("_area_owner", mainModel.getAreaOwner(), true, true);
//                        doc.getRange().replace("_gas_detector_type", mainModel.getTypeGD(), true, true);
//                        doc.getRange().replace("_gas_detector_id", mainModel.getIdGD(), true, true);
//                        doc.getRange().replace("_gas_detector_last_calibration", mainModel.getLastCalibGD(), true, true);
//                    } else {
//                        doc.getRange().replace("_area_owner", "", true, true);
//                        doc.getRange().replace("_gas_detector_type", "", true, true);
//                        doc.getRange().replace("_gas_detector_id", "", true, true);
//                        doc.getRange().replace("_gas_detector_last_calibration", "", true, true);
//                    }
//
//            /* Daily Pre-Check */
//                    UGDrillingInspectionModel inspectModel = ds.getDataPre(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Daily Pre */
//                    if (inspectModel != null) {
//                        if (inspectModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(3);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (inspectModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(3);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(3);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_pre_1", inspectModel.getRemark1(), true, true);
//
//            /* Inspection 2 Daily Pre */
//                        if (inspectModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(4);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (inspectModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(4);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(4);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_pre_2", inspectModel.getRemark2(), true, true);
//
//            /*  Electrical */
//                        UGDrillingInspectionModel elecModel = ds.getDataElectrical(projectOwner, projectName, dateIns);
//            /* Inspection 1 Electrical */
//                        if (elecModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(6);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(6);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(6);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_1", elecModel.getRemark1(), true, true);
//
//            /* Inspection 2 Electrical */
//                        if (elecModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(7);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(7);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(7);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_2", elecModel.getRemark2(), true, true);
//
//            /* Inspection 3 Electrical */
//                        if (elecModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(8);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(8);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(8);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_3", elecModel.getRemark3(), true, true);
//
//            /* Inspection 4 Electrical */
//                        if (elecModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(9);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(9);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(9);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_4", elecModel.getRemark4(), true, true);
//
//            /* Inspection 5 Electrical */
//                        if (elecModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(10);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(10);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(10);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_5", elecModel.getRemark5(), true, true);
//
//            /* Inspection 6 Electrical */
//                        if (elecModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(11);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(11);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(11);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_6", elecModel.getRemark6(), true, true);
//
//            /* Inspection 7 Electrical */
//                        if (elecModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(12);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (elecModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(12);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(12);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_elec_7", elecModel.getRemark7(), true, true);
//
//            /*  Safety Equipment  */
//                        UGDrillingInspectionModel seModel = ds.getDataSafetyEquipment(projectOwner, projectName, dateIns);
//            /* Inspection 1 Safety Equipment */
//                        if (seModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(14);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(14);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(14);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_1", seModel.getRemark1(), true, true);
//
//            /* Inspection 2 Safety Equipment */
//                        if (seModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(15);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(15);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(15);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_2", seModel.getRemark2(), true, true);
//
//            /* Inspection 3 Safety Equipment */
//                        if (seModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(16);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(16);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(16);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_3", seModel.getRemark3(), true, true);
//
//            /* Inspection 4 Safety Equipment */
//                        if (seModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(17);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(17);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(17);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_4", seModel.getRemark4(), true, true);
//
//            /* Inspection 5 Safety Equipment */
//                        if (seModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(18);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(18);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(18);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_5", seModel.getRemark5(), true, true);
//
//             /* Inspection 6 Safety Equipment */
//                        if (seModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(19);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(19);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(19);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_6", seModel.getRemark6(), true, true);
//
//            /* Inspection 7 Safety Equipment */
//                        if (seModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(20);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(20);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(20);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_7", seModel.getRemark7(), true, true);
//
//            /* Inspection 8 Safety Equipment */
//                        if (seModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(21);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection8().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(21);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(21);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_8", seModel.getRemark8(), true, true);
//
//            /* Inspection 9 Safety Equipment */
//                        if (seModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(22);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection9().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(22);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(22);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_9", seModel.getRemark9(), true, true);
//
//            /* Inspection 10 Safety Equipment */
//                        if (seModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(23);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (seModel.getInspection10().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(23);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(23);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_se_10", seModel.getRemark10(), true, true);
//
//            /* Signs Model */
//                        UGDrillingInspectionModel signsModel = ds.getDataSigns(projectOwner, projectName, dateIns);
//            /* Inspection 1 Signs */
//                        if (signsModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(25);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(25);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(25);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_1", signsModel.getRemark1(), true, true);
//
//            /* Inspection 2 Signs */
//                        if (signsModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(26);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(26);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(26);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_2", signsModel.getRemark2(), true, true);
//
//            /* Inspection 3 Signs */
//                        if (signsModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(27);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(27);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(27);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_3", signsModel.getRemark3(), true, true);
//
//            /* Inspection 3_1 Signs */
//                        if (signsModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(28);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(28);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(28);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_3_1", signsModel.getRemark4(), true, true);
//
//            /* Inspection 3_2 Signs */
//                        if (signsModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(29);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(29);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(29);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_3_2", signsModel.getRemark5(), true, true);
//
//            /* Inspection 3_3 Signs */
//                        if (signsModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(30);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(30);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(30);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_3_3", signsModel.getRemark6(), true, true);
//
//            /* Inspection 3_4 Signs */
//                        if (signsModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(31);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (signsModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(31);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(31);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_si_3_4", signsModel.getRemark7(), true, true);
//
//            /* Cleanliness Model */
//                        UGDrillingInspectionModel cleanModel = ds.getDataCleanliness(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Cleanliness */
//                        if (cleanModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(33);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(33);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(33);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_1", cleanModel.getRemark1(), true, true);
//
//            /* Inspection 2 Cleanliness */
//                        if (cleanModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(34);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(34);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(34);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_2", cleanModel.getRemark2(), true, true);
//
//            /* Inspection 3 Cleanliness */
//                        if (cleanModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(35);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(35);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(35);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_3", cleanModel.getRemark3(), true, true);
//
//            /* Inspection 4 Cleanliness */
//                        if (cleanModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(36);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(36);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(36);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_4", cleanModel.getRemark4(), true, true);
//
//            /* Inspection 5 Cleanliness */
//                        if (cleanModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(37);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(37);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(37);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_5", cleanModel.getRemark5(), true, true);
//
//            /* Inspection 6 Cleanliness */
//                        if (cleanModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(38);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(38);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(38);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_6", cleanModel.getRemark6(), true, true);
//
//            /* Inspection 7 Cleanliness */
//                        if (cleanModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(39);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(39);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(39);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_7", cleanModel.getRemark7(), true, true);
//
//            /* Inspection 8 Cleanliness */
//                        if (cleanModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(40);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection8().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(40);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(40);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_8", cleanModel.getRemark8(), true, true);
//
//            /* Inspection 9 Cleanliness */
//                        if (cleanModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(41);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection9().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(41);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(41);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_9", cleanModel.getRemark9(), true, true);
//
//            /* Inspection 10 Cleanliness */
//                        if (cleanModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(42);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cleanModel.getInspection10().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(42);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(42);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cl_10", cleanModel.getRemark10(), true, true);
//
//            /* Feed Frame Model */
//                        UGDrillingInspectionModel feedModel = ds.getDataFeedFrame(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Feed Frame */
//                        if (feedModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(44);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(44);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(44);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_1", feedModel.getRemark1(), true, true);
//
//            /* Inspection 2 Feed Frame */
//                        if (feedModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(45);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(45);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(45);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_2", feedModel.getRemark2(), true, true);
//
//            /* Inspection 3 Feed Frame */
//                        if (feedModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(46);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(46);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(46);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_3", feedModel.getRemark3(), true, true);
//
//            /* Inspection 4 Feed Frame */
//                        if (feedModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(47);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(47);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(47);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_4", feedModel.getRemark4(), true, true);
//
//            /* Inspection 5 Feed Frame */
//                        if (feedModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(48);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(48);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(48);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_5", feedModel.getRemark5(), true, true);
//
//            /* Inspection 6 Feed Frame */
//                        if (feedModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(49);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(49);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(49);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_6", feedModel.getRemark6(), true, true);
//
//            /* Inspection 7 Feed Frame */
//                        if (feedModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(50);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(50);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(50);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_7", feedModel.getRemark7(), true, true);
//
//            /* Inspection 8 Feed Frame */
//                        if (feedModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(51);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection8().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(51);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(51);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_8", feedModel.getRemark8(), true, true);
//
//            /* Inspection 9 Feed Frame */
//                        if (feedModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(52);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection9().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(52);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(52);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_9", feedModel.getRemark9(), true, true);
//
//            /* Inspection 10 Feed Frame */
//                        if (feedModel.getInspection10().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(53);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (feedModel.getInspection10().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(53);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(53);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ff_10", feedModel.getRemark10(), true, true);
//
//            /* Control Model */
//                        UGDrillingInspectionModel cntModel = ds.getDataControl(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Control */
//                        if (cntModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(55);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cntModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(55);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(55);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cnt_1", cntModel.getRemark1(), true, true);
//
//            /* Inspection 2 Control */
//                        if (cntModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(56);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cntModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(56);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(56);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cnt_2", cntModel.getRemark2(), true, true);
//
//            /* Inspection 3 Control */
//                        if (cntModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(57);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cntModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(57);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(57);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cnt_3", cntModel.getRemark3(), true, true);
//
//               /* Wire Equipment Model */
//                        UGDrillingInspectionModel weModel = ds.getDataWireline(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Wire Equipment */
//                        if (weModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(59);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (weModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(59);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(59);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_we_1", weModel.getRemark1(), true, true);
//
//            /* Inspection 2 Wire Equipment */
//                        if (weModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(60);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (weModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(60);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(60);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_we_2", weModel.getRemark2(), true, true);
//
//            /* Inspection 3 Wire Equipment */
//                        if (weModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(61);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (weModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(61);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(61);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_we_3", weModel.getRemark3(), true, true);
//
//            /* Inspection 4 Wire Equipment */
//                        if (weModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(62);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (weModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(62);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(62);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_we_4", weModel.getRemark4(), true, true);
//
//            /* Circulation System Model */
//                        UGDrillingInspectionModel csModel = ds.getDataCirculation(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Circulation System */
//                        if (csModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(64);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(64);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(64);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_1", csModel.getRemark1(), true, true);
//
//            /* Inspection 2 Circulation System */
//                        if (csModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(65);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(65);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(65);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_2", csModel.getRemark2(), true, true);
//
//            /* Inspection 3 Circulation System */
//                        if (csModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(66);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(66);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(66);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_3", csModel.getRemark3(), true, true);
//
//            /* Inspection 4 Circulation System */
//                        if (csModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(67);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(67);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(67);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_4", csModel.getRemark4(), true, true);
//
//            /* Inspection 5 Circulation System */
//                        if (csModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(68);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(68);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(68);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_5", csModel.getRemark5(), true, true);
//
//            /* Inspection 6 Circulation System */
//                        if (csModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(69);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(69);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(69);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_6", csModel.getRemark6(), true, true);
//
//            /* Inspection 7 Circulation System */
//                        if (csModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(70);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (csModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(70);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(70);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cs_7", csModel.getRemark7(), true, true);
//
//            /* Control Panel Model */
//                        UGDrillingInspectionModel cpModel = ds.getDataControlPanel(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Control Panel */
//                        if (cpModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(72);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(72);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(72);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_1", cpModel.getRemark1(), true, true);
//
//            /* Inspection 2 Control Panel */
//                        if (cpModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(73);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(73);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(73);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_2", cpModel.getRemark2(), true, true);
//
//            /* Inspection 3 Control Panel */
//                        if (cpModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(74);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(74);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(74);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_3", cpModel.getRemark3(), true, true);
//
//            /* Inspection 4 Control Panel */
//                        if (cpModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(75);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(75);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(75);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_4", cpModel.getRemark4(), true, true);
//
//            /* Inspection 5 Control Panel */
//                        if (cpModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(76);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(76);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(76);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_5", cpModel.getRemark5(), true, true);
//
//            /* Inspection 6 Control Panel */
//                        if (cpModel.getInspection6().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(77);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection6().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(77);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(77);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_6", cpModel.getRemark6(), true, true);
//
//            /* Inspection 7 Control Panel */
//                        if (cpModel.getInspection7().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(78);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection7().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(78);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(78);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_7", cpModel.getRemark7(), true, true);
//
//            /* Inspection 8 Control Panel */
//                        if (cpModel.getInspection8().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(79);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection8().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(79);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(79);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_8", cpModel.getRemark8(), true, true);
//
//            /* Inspection 9 Control Panel */
//                        if (cpModel.getInspection9().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(80);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (cpModel.getInspection9().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(80);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(80);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_cp_9", cpModel.getRemark9(), true, true);
//
//            /* Handtools Model */
//                        UGDrillingInspectionModel htModel = ds.getDataHandtools(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Handtools */
//                        if (htModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(82);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (htModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(82);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(82);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ht_1", htModel.getRemark1(), true, true);
//
//            /* Inspection 2 Handtools */
//                        if (htModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(83);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (htModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(83);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(83);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ht_2", htModel.getRemark2(), true, true);
//
//            /* Inspection 3 Handtools */
//                        if (htModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(84);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (htModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(84);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(84);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ht_3", htModel.getRemark3(), true, true);
//
//            /* Inspection 4 Handtools */
//                        if (htModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(85);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (htModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(85);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(85);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_ht_4", htModel.getRemark4(), true, true);
//
//            /* Work Procedure Model */
//                        UGDrillingInspectionModel wpModel = ds.getDataWorkProcedure(projectOwner, projectName, dateIns);
//
//            /* Inspection 1 Work Procedure */
//                        if (wpModel.getInspection1().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(87);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (wpModel.getInspection1().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(87);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(87);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_wp_1", wpModel.getRemark1(), true, true);
//
//            /* Inspection 2 Work Procedure */
//                        if (wpModel.getInspection2().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(88);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (wpModel.getInspection2().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(88);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(88);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_wp_2", wpModel.getRemark2(), true, true);
//
//            /* Inspection 3 Work Procedure */
//                        if (wpModel.getInspection3().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(89);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (wpModel.getInspection3().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(89);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(89);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_wp_3", wpModel.getRemark3(), true, true);
//
//            /* Inspection 4 Work Procedure */
//                        if (wpModel.getInspection4().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(90);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (wpModel.getInspection4().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(90);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(90);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_wp_4", wpModel.getRemark4(), true, true);
//
//            /* Inspection 5 Work Procedure */
//                        if (wpModel.getInspection5().toString().equalsIgnoreCase("Yes")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(91);
//                            Cell dataCell = row.getCells().get(1);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else if (wpModel.getInspection5().toString().equalsIgnoreCase("No")) {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(91);
//                            Cell dataCell = row.getCells().get(2);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            Table table = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                            Row row = (Row) table.getRows().get(91);
//                            Cell dataCell = row.getCells().get(3);
//
//                            dataCell.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell.getFirstParagraph().getRuns().clear();
//                            dataCell.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        }
//                        doc.getRange().replace("Remarks_wp_5", wpModel.getRemark5(), true, true);
//
//                    } else {
//
//                    }
//
//            /* Inspector Table */
//            /* Hydrology */
//                    InspectorUGModel hydroModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Hydrology");
//                    if (hydroModel != null) {
//                        doc.getRange().replace("_comment_ug_hydrology", hydroModel.getComment(), true, true);
//                        doc.getRange().replace("_name_6_id", hydroModel.getName() + "(" + hydroModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", hydroModel.getDateIns(), true, true);
//                        if (hydroModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(11);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(hydroModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_ug_hydrology", "", true, true);
//                        doc.getRange().replace("_name_6_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//              /* Geology */
//                    InspectorUGModel geoModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Geology");
//                    if (geoModel != null) {
//                        doc.getRange().replace("_comment_ug_geology", geoModel.getComment(), true, true);
//                        doc.getRange().replace("_name_7_id", geoModel.getName() + "(" + geoModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", geoModel.getDateIns(), true, true);
//                        if (geoModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(13);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(geoModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_ug_geology", "", true, true);
//                        doc.getRange().replace("_name_7_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* GeoTech */
//                    InspectorUGModel geoTechModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "GeoTech");
//                    if (geoTechModel != null) {
//                        doc.getRange().replace("_comment_ug_geotechnical", geoTechModel.getComment(), true, true);
//                        doc.getRange().replace("_name_5_id", geoTechModel.getName() + "(" + geoTechModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", geoTechModel.getDateIns(), true, true);
//                        if (geoTechModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(9);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(geoTechModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_ug_geotechnical", "", true, true);
//                        doc.getRange().replace("_name_5_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* Ventilation */
//                    InspectorUGModel venModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Ventilation");
//                    if (venModel != null) {
//                        doc.getRange().replace("_comment_ug_ventilation", venModel.getComment(), true, true);
//                        doc.getRange().replace("_name_9_id", venModel.getName() + "(" + venModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", venModel.getDateIns(), true, true);
//                        if (venModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(17);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(venModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_ug_ventilation", "", true, true);
//                        doc.getRange().replace("_name_9_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* UG SHE */
//                    InspectorUGModel ugSheModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "UG SHE");
//                    if (ugSheModel != null) {
//                        doc.getRange().replace("_comment_ug_she_geoservices", ugSheModel.getComment(), true, true);
//                        doc.getRange().replace("_name_8_id", ugSheModel.getName() + "(" + ugSheModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", ugSheModel.getDateIns(), true, true);
//                        if (ugSheModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(15);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(ugSheModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_ug_she_geoservices", "", true, true);
//                        doc.getRange().replace("_name_8_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* Drilling Coordinator */
//                    InspectorUGModel coorModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Drilling Coordinator");
//                    if (coorModel != null) {
//                        doc.getRange().replace("_comment_drilling_coordinator", coorModel.getComment(), true, true);
//                        doc.getRange().replace("_name_3_id", coorModel.getName() + "(" + coorModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", coorModel.getDateIns(), true, true);
//                        if (coorModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(5);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(coorModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_drilling_coordinator", "", true, true);
//                        doc.getRange().replace("_name_3_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* Drilling Contractor */
//                    InspectorUGModel contModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Drilling Contractor");
//                    if (contModel != null) {
//                        doc.getRange().replace("_comment_drilling_contractor", contModel.getComment(), true, true);
//                        doc.getRange().replace("_name_4_id", contModel.getName() + "(" + contModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", contModel.getDateIns(), true, true);
//                        if (contModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(7);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(contModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_drilling_contractor", "", true, true);
//                        doc.getRange().replace("_name_4_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* Area Owner */
//                    InspectorUGModel aoModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Area Owner");
//                    if (aoModel != null) {
//                        doc.getRange().replace("_comment_area_owner", aoModel.getComment(), true, true);
//                        doc.getRange().replace("_name_1_id", aoModel.getName() + "(" + aoModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", aoModel.getDateIns(), true, true);
//                        if (aoModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(1);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(aoModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_area_owner", "", true, true);
//                        doc.getRange().replace("_name_1_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//            /* Project Owner */
//                    InspectorUGModel poModel = ds.getInspectorUG(projectOwner, projectName, dateIns, "Project Owner");
//                    if (poModel != null) {
//                        doc.getRange().replace("_comment_project_drilling_owner", poModel.getComment(), true, true);
//                        doc.getRange().replace("_name_2_id", poModel.getName() + "(" + poModel.getIdMan() + ")", true, true);
//                        doc.getRange().replace("_inspection_date", poModel.getDateIns(), true, true);
//                        if (poModel.getSign() != null) {
//                            Table table1 = (Table) doc.getChild(NodeType.TABLE, 4, true);
//                            Row row1 = (Row) table1.getRows().get(3);
//                            Cell dataCell1 = row1.getCells().get(4);
//                            CellFormat format = dataCell1.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell1.getFirstParagraph());
//                            Shape image = builder.insertImage(poModel
//                                    .getSign());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    } else {
//                        doc.getRange().replace("_comment_project_drilling_owner", "", true, true);
//                        doc.getRange().replace("_name_2_id", "", true, true);
//                        doc.getRange().replace("_inspection_date", "", true, true);
//                    }
//
//
//            /* Photo Table */
//                    Table photoTableDoc = (Table) doc.getChild(NodeType.TABLE, 3, true);
//                    RowCollection photoTableRows = photoTableDoc.getRows();
//                    int photoRowStartingIndex = 2;
//                    int photoRowDummyIndex = 1;
//                    int photoColStartingIndex = 0;
//                    int photoRowCount = 0;
//
//                    ArrayList<PhotoModel> fotoModel = ds.getPhotoData(Constants.INSPECTION_UG_DRILLING_PRE, dateIns, projectOwner);
//                    ds.close();
//
//                    int count = 0;
//                    Row dataRow;
//                    Row clonedRow;
//                    clonedRow = (Row) photoTableRows.get(photoRowDummyIndex);
//                    for (int a = 0; a < fotoModel.size(); a++) {
//                        dataRow = (Row) clonedRow.deepClone(true);
//                        photoTableDoc.getRows().add(dataRow);
//                        count++;
//
//                        dataRow.getRowFormat().setAllowBreakAcrossPages(true);
//
//                        // get the cell for each attribute of foto (number, photo, and
//                        // comment)
//                        Cell photoNumberCell = dataRow.getCells().get(
//                                photoColStartingIndex + 0);
//                        Cell photoPhotoCell = dataRow.getCells().get(
//                                photoColStartingIndex + 1);
//                        Cell photoCommentCell = dataRow.getCells().get(
//                                photoColStartingIndex + 2);
//
//                        // set each cell background to white
//                        photoNumberCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        photoPhotoCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        photoCommentCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//
//                        // clear each cell
//                        photoNumberCell.getFirstParagraph().getRuns().clear();
//                        photoPhotoCell.getFirstParagraph().getRuns().clear();
//                        photoCommentCell.getFirstParagraph().getRuns().clear();
//
//                        // fill the data to each cell
//                        // number data
//                        photoNumberCell.getFirstParagraph().appendChild(
//                                new Run(doc, String.valueOf(count)));
//
//                        // photo
//                        CellFormat format = photoPhotoCell.getCellFormat();
//                        double width = format.getWidth();
//                        builder.moveTo(photoPhotoCell.getFirstParagraph());
//                        // Bitmap imgBitmap =
//                        // Helper.getBitmapFromFile(foto.getFotoPath());
//                        Bitmap imgBitmap = null;
//
//                        try {
//                            ExifInterface ei;
//                            ei = new ExifInterface(fotoModel.get(a).getFotoPath());
//                            int orientation = ei.getAttributeInt(
//                                    ExifInterface.TAG_ORIENTATION,
//                                    ExifInterface.ORIENTATION_NORMAL);
//
//                            switch (orientation) {
//                                case ExifInterface.ORIENTATION_ROTATE_90:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 90,
//                                            (int) width);
//                                    break;
//                                case ExifInterface.ORIENTATION_ROTATE_180:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 180,
//                                            (int) width);
//                                    break;
//                                default:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 0,
//                                            (int) width);
//                                    break;
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        if (imgBitmap != null) {
//                            // Shape image = builder.insertImage(imgBitmap);
//                            ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
//                            imgBitmap.compress(Bitmap.CompressFormat.JPEG,
//                                    Constants.COMPRESS_PERCENTAGE, tempImage);
//                            Bitmap compressedBitmap = BitmapFactory
//                                    .decodeStream(new ByteArrayInputStream(tempImage
//                                            .toByteArray()));
//                            Shape image = builder.insertImage(compressedBitmap);
//
//                            double freePageWidth = width;
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData().getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth / size.getWidthPoints();
//
//                                Log.e("IMAGE", "widthlonger : " + "" + " | ratio : "
//                                        + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//
//                        // comment
//                        photoCommentCell.getFirstParagraph().appendChild(
//                                new Run(doc, fotoModel.get(a).getComment()));
//
//                        photoRowCount++;
//                    }
//
//                    Row dummyRow = photoTableRows.get(photoRowDummyIndex);
//                    dummyRow.remove();
//
////
////            Row dummyRow = photoTableRows.get(photoRowDummyIndex);
////            dummyRow.remove();
//
////            Table tableComment = (Table) doc.getChild(NodeType.TABLE, 4, true);
////            RowCollection rows = tableComment.getRows();
////
////            insertImage(rows.get(1).getCells().get(4), builder,
////                    listSignature[1], doc);
////            insertImage(rows.get(3).getCells().get(4), builder,
////                    listSignature[0], doc);
////            insertImage(rows.get(5).getCells().get(4), builder,
////                    listSignature[2], doc);
////            insertImage(rows.get(7).getCells().get(4), builder,
////                    listSignature[3], doc);
////            insertImage(rows.get(9).getCells().get(4), builder,
////                    listSignature[4], doc);
////            insertImage(rows.get(11).getCells().get(4), builder,
////                    listSignature[5], doc);
////            insertImage(rows.get(13).getCells().get(4), builder,
////                    listSignature[6], doc);
////            insertImage(rows.get(15).getCells().get(4), builder,
////                    listSignature[7], doc);
////            insertImage(rows.get(17).getCells().get(4), builder,
////                    listSignature[8], doc);
//
//                    // CREATING FOLDER (DIRECTORY)
////            if (isGenerate) {
////                // GENERATE FILE NAME FORMAT
////                // String formattedID =
////                // formatIDInspector(selectedPIC.getObsId());
////                String currentTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm")
////                        .format(new Date());
////                String baseFolderName = currentTime + "_"
////                        + Constants.UGD_PRE_FOLDER_MARK_NAME;
////                // + "_"
////                // + formattedID;
////
////                String fileName = Constants.UGD_PRE_REPORT_FILE_NAME;
////
////                String thisReportPath = baseReportPath + "/" + baseFolderName;
////                Helper.checkAndCreateDirectory(thisReportPath);
////                folderDirectory = thisReportPath;
////
////                doc.save(thisReportPath + "/" + fileName
////                        + Constants.FILE_TYPE_DOCX);
////                doc.save(thisReportPath + "/" + fileName
////                        + Constants.FILE_TYPE_PDF);
////
////                // copy photos to folderDirectory
//////                for (FotoModel foto : fotoList) {
//////                    if (null != foto.getFotoPath()) {
//////                        InputStream is;
//////                        try {
//////                            // trying to copy image to folderDirectory based on
//////                            // its path
//////                            is = new FileInputStream(foto.getFotoPath());
//////
//////                            File fileImage = new File(foto.getFotoPath());
//////                            if (fileImage.exists()) {
//////                                String photoDirectory = folderDirectory + "/"
//////                                        + Constants.PHOTO_FOLDER_NAME;
//////                                new File(photoDirectory).mkdirs();
//////                                String outputPath = photoDirectory
//////                                        + File.separator + foto.getId()
//////                                        + ".jpg";
//////                                OutputStream os = new FileOutputStream(
//////                                        outputPath);
//////                                Helper.copyFile(is, os);
//////                                // foto.setFotoPath(outputPath);
//////                            }
//////
//////                        } catch (FileNotFoundException e) {
//////                            // TODO Auto-generated catch block
////////                            Log.e("Error create photo folder", e.getMessage());
//////                            e.printStackTrace();
//////                        } catch (IOException e) {
//////                            // TODO Auto-generated catch block
//////                            e.printStackTrace();
////////                            Log.e("Error create photo folder", e.getMessage());
//////                        }
//////                    }
//////                }
////                Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
////                        + Constants.APP_FOLDER_NAME + "/"
////                        + Constants.TEMPLATE_FOLDER_NAME + "/");
////                return Constants.REPORT_GENERATED;
////            } else {
////                // preview
////                String previewFolder = Helper.getPathForPreviewFile();
////                String fileNamePdf = Constants.UGD_PRE_PREVIEW_FILE_NAME
////                        + Constants.FILE_TYPE_PDF;
////                Helper.checkAndCreateDirectory(previewFolder);
////                previewFilePath = previewFolder + "/" + fileNamePdf;
////                doc.save(previewFilePath);
////                Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
////                        + Constants.APP_FOLDER_NAME + "/"
////                        + Constants.TEMPLATE_FOLDER_NAME + "/");
////
////                return Constants.REPORT_PREVIEW;
////            }
//
//                    String fullPath = "";
//                    String currentTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm")
//                            .format(new Date());
//                    String fileName = currentTime + "_UG_" + Constants.UGD_PRE_FOLDER_MARK_NAME;
//                    if (isGenerate) {
//                        fullPath = Environment.getExternalStorageDirectory().getAbsolutePath()
//                                + "/"
//                                + Constants.ROOT_FOLDER_NAME
//                                + "/"
//                                + Constants.APP_FOLDER_NAME
//                                + "/"
//                                + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
//                    }
//
//                    if (isGenerate) {
//                        doc.save(fullPath + ".doc");
//                        folderDirectory = fullPath;
//                        fullPath += "/" + fileName;
//                        Helper.showPopUpMessage(mActivity, "Success",
//                                "Report has been successfully generated in "
//                                        + folderDirectory, null);
//                    } else {
//                        String previewFolder = Helper.getPathForPreviewFile();
//                        String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
//                                + Constants.FILE_TYPE_PDF;
//                        Helper.checkAndCreateDirectory(previewFolder);
//                        previewFilePath = previewFolder + "/" + fileNamePdf;
//                        doc.save(previewFilePath);
//
//                        File file = new File(previewFilePath);
//                        Intent intent = new Intent();
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.setAction(Intent.ACTION_VIEW);
//                        String type = "application/pdf";
//                        intent.setDataAndType(Uri.fromFile(file), type);
//                        mActivity.startActivityForResult(intent,
//                                PREVIEW_CODE_SHE_UG);
//                        isPreview = true;
//                    }
//
//                } catch (Exception ex) {
////            Helper.deleteAllFilesOnDirectory(Constants.basepath + "/"
////                    + Constants.APP_FOLDER_NAME + "/"
////                    + Constants.TEMPLATE_FOLDER_NAME + "/");
////            return Constants.REPORT_FAILED;
//                }
//            }
//        }, 500);
//
//    }

    /* ========================
                Generate Report LV
    ======================== */
//    public static int generateReportLV(final Activity mActivity, final String lvNo, final String date, final String pic, final boolean isGenerate) {
//        Helper.prepareTemplateDirectory(mActivity);
//
//                String templateDocPath = Constants.basepath + "/"
//                        + Constants.APP_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FOLDER_NAME + "/"
//                        + Constants.TEMPLATE_FILE_LIGHT_VEHICLE;
//
//                try {
//                    final Document doc = new Document(templateDocPath);
//                    DocumentBuilder builder = new DocumentBuilder(doc);
//
//                    DataSource ds = new DataSource(mActivity);
//                    ds.open();
//
//                    int photoRowStartingIndex = 2;
//                    int photoRowDummyIndex = 1;
//                    int photoColStartingIndex = 0;
//                    int photoRowCount = 0;
//
//            /* Daily Table */
//                    ArrayList<LVDailyModel> dailyModel = ds.getAllDataLVDaily(lvNo, pic, date);
//                    Table table = (Table) doc.getChild(NodeType.TABLE, 0, true);
//
//                    for (int d = 0; d < dailyModel.size(); d++) {
//
//
//                /* Date Daily */
//                        Row rowDate = (Row) table.getRows().get(0);
//                        Cell dataCellDate = rowDate.getCells().get(3 + d);
//                        dataCellDate.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCellDate.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCellDate.getFirstParagraph().getRuns().clear();
//                        dataCellDate.getFirstParagraph().appendChild(new Run(doc, dailyModel.get(d).getDate()));
//
//                /* Time Daily */
//                        Row rowTime = (Row) table.getRows().get(1);
//                        Cell dataCellTime = rowTime.getCells().get(3 + d);
//                        dataCellTime.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCellTime.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCellTime.getFirstParagraph().getRuns().clear();
//                        dataCellTime.getFirstParagraph().appendChild(new Run(doc, dailyModel.get(d).getTime()));
//
//                /* Inspection 1 Daily */
//                        Row row1 = (Row) table.getRows().get(2);
//                        Cell dataCell1 = row1.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp1().equalsIgnoreCase("Good")) {
//                            dataCell1.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell1.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell1.getFirstParagraph().getRuns().clear();
//                            dataCell1.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell1.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell1.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell1.getFirstParagraph().getRuns().clear();
//                            dataCell1.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 2 Daily */
//                        Row row2 = (Row) table.getRows().get(3);
//                        Cell dataCell2 = row2.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp2().equalsIgnoreCase("Good")) {
//                            dataCell2.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell2.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell2.getFirstParagraph().getRuns().clear();
//                            dataCell2.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell2.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell2.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell2.getFirstParagraph().getRuns().clear();
//                            dataCell2.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                 /* Inspection 3 Daily */
//                        Row row3 = (Row) table.getRows().get(4);
//                        Cell dataCell3 = row3.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp3().equalsIgnoreCase("Good")) {
//                            dataCell3.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell3.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell3.getFirstParagraph().getRuns().clear();
//                            dataCell3.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell3.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell3.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell3.getFirstParagraph().getRuns().clear();
//                            dataCell3.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 4 Daily */
//                        Row row4 = (Row) table.getRows().get(5);
//                        Cell dataCell4 = row4.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp4().equalsIgnoreCase("Good")) {
//                            dataCell4.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell4.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell4.getFirstParagraph().getRuns().clear();
//                            dataCell4.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell4.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell4.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell4.getFirstParagraph().getRuns().clear();
//                            dataCell4.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 5 Daily */
//                        Row row5 = (Row) table.getRows().get(6);
//                        Cell dataCell5 = row5.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp5().equalsIgnoreCase("Good")) {
//                            dataCell5.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell5.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell5.getFirstParagraph().getRuns().clear();
//                            dataCell5.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell5.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell5.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell5.getFirstParagraph().getRuns().clear();
//                            dataCell5.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 6 Daily */
//                        Row row6 = (Row) table.getRows().get(7);
//                        Cell dataCell6 = row6.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp6().equalsIgnoreCase("Good")) {
//                            dataCell6.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell6.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell6.getFirstParagraph().getRuns().clear();
//                            dataCell6.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell6.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell6.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell6.getFirstParagraph().getRuns().clear();
//                            dataCell6.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 7 Daily */
//                        Row row7 = (Row) table.getRows().get(8);
//                        Cell dataCell7 = row7.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp7().equalsIgnoreCase("Good")) {
//                            dataCell7.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell7.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell7.getFirstParagraph().getRuns().clear();
//                            dataCell7.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell7.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell7.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell7.getFirstParagraph().getRuns().clear();
//                            dataCell7.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 8 Daily */
//                        Row row8 = (Row) table.getRows().get(9);
//                        Cell dataCell8 = row8.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp8().equalsIgnoreCase("Good")) {
//                            dataCell8.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell8.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell8.getFirstParagraph().getRuns().clear();
//                            dataCell8.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell8.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell8.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell8.getFirstParagraph().getRuns().clear();
//                            dataCell8.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 9 Daily */
//                        Row row9 = (Row) table.getRows().get(10);
//                        Cell dataCell9 = row9.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp9().equalsIgnoreCase("Good")) {
//                            dataCell9.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell9.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell9.getFirstParagraph().getRuns().clear();
//                            dataCell9.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell9.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell9.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell9.getFirstParagraph().getRuns().clear();
//                            dataCell9.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 10 Daily */
//                        Row row10 = (Row) table.getRows().get(11);
//                        Cell dataCell10 = row10.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp10().equalsIgnoreCase("Good")) {
//                            dataCell10.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell10.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell10.getFirstParagraph().getRuns().clear();
//                            dataCell10.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell10.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell10.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell10.getFirstParagraph().getRuns().clear();
//                            dataCell10.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 11 Daily */
//                        Row row11 = (Row) table.getRows().get(12);
//                        Cell dataCell11 = row11.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp11().equalsIgnoreCase("Good")) {
//                            dataCell11.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell11.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell11.getFirstParagraph().getRuns().clear();
//                            dataCell11.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell11.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell11.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell11.getFirstParagraph().getRuns().clear();
//                            dataCell11.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 12 Daily */
//                        Row row12 = (Row) table.getRows().get(13);
//                        Cell dataCell12 = row12.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp12().equalsIgnoreCase("Good")) {
//                            dataCell12.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell12.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell12.getFirstParagraph().getRuns().clear();
//                            dataCell12.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell12.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell12.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell12.getFirstParagraph().getRuns().clear();
//                            dataCell12.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 13 Daily */
//                        Row row13 = (Row) table.getRows().get(14);
//                        Cell dataCell13 = row13.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp13().equalsIgnoreCase("Good")) {
//                            dataCell13.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell13.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell13.getFirstParagraph().getRuns().clear();
//                            dataCell13.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell13.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell13.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell13.getFirstParagraph().getRuns().clear();
//                            dataCell13.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 14 Daily */
//                        Row row14 = (Row) table.getRows().get(15);
//                        Cell dataCell14 = row14.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp14().equalsIgnoreCase("Good")) {
//                            dataCell14.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell14.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell14.getFirstParagraph().getRuns().clear();
//                            dataCell14.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell14.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell14.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell14.getFirstParagraph().getRuns().clear();
//                            dataCell14.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 15 Daily */
//                        Row row15 = (Row) table.getRows().get(16);
//                        Cell dataCell15 = row15.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp15().equalsIgnoreCase("Good")) {
//                            dataCell15.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell15.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell15.getFirstParagraph().getRuns().clear();
//                            dataCell15.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell15.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell15.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell15.getFirstParagraph().getRuns().clear();
//                            dataCell15.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 16 Daily */
//                        Row row16 = (Row) table.getRows().get(17);
//                        Cell dataCell16 = row16.getCells().get(2 + d);
//                        if (dailyModel.get(d).getInsp16().equalsIgnoreCase("Good")) {
//                            dataCell16.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell16.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell16.getFirstParagraph().getRuns().clear();
//                            dataCell16.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell16.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell16.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell16.getFirstParagraph().getRuns().clear();
//                            dataCell16.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                    }
//
//            /* Weekly Table */
//                    ArrayList<LVWeeklyModel> weeklyModel = ds.getAllDataLVWeekly(lvNo, pic, date);
//                    Table tableWeekly = (Table) doc.getChild(NodeType.TABLE, 1, true);
//
//                    for (int w = 0; w < weeklyModel.size(); w++) {
//                /* Inspection 1 Weekly */
//                        Row row1 = (Row) tableWeekly.getRows().get(1);
//                        Cell dataCell1 = row1.getCells().get(2 + w);
//                        if (weeklyModel.get(w).getInsp1().equalsIgnoreCase("Good")) {
//                            dataCell1.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell1.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell1.getFirstParagraph().getRuns().clear();
//                            dataCell1.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell1.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell1.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell1.getFirstParagraph().getRuns().clear();
//                            dataCell1.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                 /* Inspection 2 Weekly */
//                        Row row2 = (Row) tableWeekly.getRows().get(2);
//                        Cell dataCell2 = row2.getCells().get(2 + w);
//                        if (weeklyModel.get(w).getInsp2().equalsIgnoreCase("Good")) {
//                            dataCell2.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell2.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell2.getFirstParagraph().getRuns().clear();
//                            dataCell2.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell2.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell2.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell2.getFirstParagraph().getRuns().clear();
//                            dataCell2.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Inspection 2 Weekly */
//                        Row row3 = (Row) tableWeekly.getRows().get(3);
//                        Cell dataCell3 = row3.getCells().get(2 + w);
//                        if (weeklyModel.get(w).getInsp3().equalsIgnoreCase("Good")) {
//                            dataCell3.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell3.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell3.getFirstParagraph().getRuns().clear();
//                            dataCell3.getFirstParagraph().appendChild(new Run(doc, passCheck));
//                        } else {
//                            dataCell3.getCellFormat().getShading()
//                                    .setBackgroundPatternColor(Color.WHITE);
//                            dataCell3.getCellFormat().setVerticalAlignment(
//                                    CellVerticalAlignment.CENTER);
//                            dataCell3.getFirstParagraph().getRuns().clear();
//                            dataCell3.getFirstParagraph().appendChild(new Run(doc, "DMG"));
//                        }
//
//                /* Sign Driver Weekly */
//                        if (weeklyModel.get(w).getSignature() != null) {
//                            Row row4 = (Row) tableWeekly.getRows().get(4);
//                            Cell dataCell4 = row4.getCells().get(2 + w);
//                            CellFormat format = dataCell4.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell4.getFirstParagraph());
//                            Shape image = builder.insertImage(weeklyModel.get(w).getSignature());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//
//                /* Sign Supervisor Weekly */
//                        if (weeklyModel.get(w).getSignature1() != null) {
//                            Row row5 = (Row) tableWeekly.getRows().get(4);
//                            Cell dataCell5 = row5.getCells().get(2 + w);
//                            CellFormat format = dataCell5.getCellFormat();
//                            double width = format.getWidth();
//                            builder.moveTo(dataCell5.getFirstParagraph());
//                            Shape image = builder.insertImage(weeklyModel.get(w).getSignature1());
//
//                            double freePageWidth = width;
//
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData()
//                                    .getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth
//                                        / size.getWidthPoints();
//
//                                Log.d("IMAGE", "widthlonger : " + ""
//                                        + " | ratio : " + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//                    }
//
//            /* Damage Table */
//                    ArrayList<LVDamageModel> damageModel = ds.getAllDataLVDamage(lvNo, pic, date);
//                    Table tableDamage = (Table) doc.getChild(NodeType.TABLE, 1, true);
//
//                    for (int m = 0; m < damageModel.size(); m++) {
//                /* Inspection Item Damage */
//                        Row row1 = (Row) tableWeekly.getRows().get(1 + m);
//                        Cell dataCell1 = row1.getCells().get(5);
//                        dataCell1.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCell1.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCell1.getFirstParagraph().getRuns().clear();
//                        dataCell1.getFirstParagraph().appendChild(new Run(doc, damageModel.get(m).getItemDamage()));
//
//                 /* Inspection Date Damage */
//                        Row row2 = (Row) tableWeekly.getRows().get(1 + m);
//                        Cell dataCell2 = row2.getCells().get(6);
//                        dataCell2.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCell2.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCell2.getFirstParagraph().getRuns().clear();
//                        dataCell2.getFirstParagraph().appendChild(new Run(doc, damageModel.get(m).getDate()));
//
//                 /* Inspection Driver ID Damage */
//                        Row row3 = (Row) tableWeekly.getRows().get(1 + m);
//                        Cell dataCell3 = row3.getCells().get(6);
//                        dataCell3.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCell3.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCell3.getFirstParagraph().getRuns().clear();
//                        dataCell3.getFirstParagraph().appendChild(new Run(doc, damageModel.get(m).getDriverId()));
//
//                /* Inspection Driver ID Damage */
//                        Row row4 = (Row) tableWeekly.getRows().get(1 + m);
//                        Cell dataCell4 = row4.getCells().get(7);
//                        dataCell4.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        dataCell4.getCellFormat().setVerticalAlignment(
//                                CellVerticalAlignment.CENTER);
//                        dataCell4.getFirstParagraph().getRuns().clear();
//                        dataCell4.getFirstParagraph().appendChild(new Run(doc, damageModel.get(m).getAction()));
//
//                    }
//
//            /* Photo Table */
//                    Table photoTableDoc = (Table) doc.getChild(NodeType.TABLE, 2, true);
//                    RowCollection photoTableRows = photoTableDoc.getRows();
//
//                    ArrayList<PhotoModel> fotoModel = ds.getPhotoData(Constants.INSPECTION_LVI, date, pic);
//                    ds.close();
//
//                    int count = 0;
//                    for (int a = 0; a < fotoModel.size(); a++) {
//                        count++;
//                        Row dataRow;
//                        if (photoRowCount > 0) {
//                            // copy 1st data row
//                            dataRow = (Row) photoTableRows.get(photoRowDummyIndex);
//                            dataRow = (Row) dataRow.deepClone(true);
//                        } else {
//                            // 1st row of table is dummy row (for copy cell purpose)
//                            dataRow = (Row) photoTableRows.get(photoRowStartingIndex);
//                        }
//
//                        dataRow.getRowFormat().setAllowBreakAcrossPages(true);
//
//                        // get the cell for each attribute of foto (number, photo, and
//                        // comment)
//                        Cell photoNumberCell = dataRow.getCells().get(
//                                photoColStartingIndex + 0);
//                        Cell photoPhotoCell = dataRow.getCells().get(
//                                photoColStartingIndex + 1);
//                        Cell photoCommentCell = dataRow.getCells().get(
//                                photoColStartingIndex + 2);
//
//                        // set each cell background to white
//                        photoNumberCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        photoPhotoCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//                        photoCommentCell.getCellFormat().getShading()
//                                .setBackgroundPatternColor(Color.WHITE);
//
//                        // clear each cell
//                        photoNumberCell.getFirstParagraph().getRuns().clear();
//                        photoPhotoCell.getFirstParagraph().getRuns().clear();
//                        photoCommentCell.getFirstParagraph().getRuns().clear();
//
//                        // fill the data to each cell
//                        // number data
//                        photoNumberCell.getFirstParagraph().appendChild(
//                                new Run(doc, valueOf(count)));
//
//                        // photo
//                        CellFormat format = photoPhotoCell.getCellFormat();
//                        double width = format.getWidth();
//                        builder.moveTo(photoPhotoCell.getFirstParagraph());
//                        // Bitmap imgBitmap =
//                        // Helper.getBitmapFromFile(foto.getFotoPath());
//                        Bitmap imgBitmap = null;
//
//                        try {
//                            ExifInterface ei;
//                            ei = new ExifInterface(fotoModel.get(a).getFotoPath());
//                            int orientation = ei.getAttributeInt(
//                                    ExifInterface.TAG_ORIENTATION,
//                                    ExifInterface.ORIENTATION_NORMAL);
//
//                            switch (orientation) {
//                                case ExifInterface.ORIENTATION_ROTATE_90:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 90,
//                                            (int) width);
//                                    break;
//                                case ExifInterface.ORIENTATION_ROTATE_180:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 180,
//                                            (int) width);
//                                    break;
//                                default:
//                                    imgBitmap = Helper.rotateImage(fotoModel.get(a).getFotoPath(), 0,
//                                            (int) width);
//                                    break;
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        if (imgBitmap != null) {
//                            // Shape image = builder.insertImage(imgBitmap);
//                            ByteArrayOutputStream out = new ByteArrayOutputStream();
//                            imgBitmap.compress(Bitmap.CompressFormat.JPEG,
//                                    Constants.COMPRESS_PERCENTAGE, out);
//                            Bitmap compressedBitmap = BitmapFactory
//                                    .decodeStream(new ByteArrayInputStream(out
//                                            .toByteArray()));
//                            Shape image = builder.insertImage(compressedBitmap);
//
//                            double freePageWidth = width;
//                            // Is one of the sides of this image too big for the
//                            // page?
//                            ImageSize size = image.getImageData().getImageSize();
//                            boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                            if (exceedsMaxPageSize) {
//                                // Calculate the ratio to fit the page size
//                                double ratio = freePageWidth / size.getWidthPoints();
//
//                                Log.e("IMAGE", "widthlonger : " + "" + " | ratio : "
//                                        + ratio);
//
//                                // Set the new size.
//                                image.setWidth(size.getWidthPoints() * ratio);
//                                image.setHeight(size.getHeightPoints() * ratio);
//                            }
//                        }
//
//                        // comment
//                        photoCommentCell.getFirstParagraph().appendChild(
//                                new Run(doc, fotoModel.get(a).getComment()));
//
//                        if (photoRowCount > 0) {
//                            // insert row
//                            photoTableDoc.getRows().add(dataRow);
//                        }
//                        photoRowCount++;
//                    }
//
//                    Row dummyRow = photoTableRows.get(photoRowDummyIndex);
//                    dummyRow.remove();
//
//                    // CREATING FOLDER (DIRECTORY)
//
//                    if (isGenerate) {
//                        // GENERATE FILE NAME FORMAT
//                        String formattedID = pic;
//                        String currentTime = new SimpleDateFormat("yyyy_MM_dd_HH_mm")
//                                .format(new Date());
//                        String baseFolderName = currentTime + "_"
//                                + Constants.LVI_FOLDER_MARK_NAME + "_" + formattedID;
//                        String fileName = Constants.LVI_REPORT_FILE_NAME;
//
//                        String thisReportPath = baseReportPath + File.separator
//                                + baseFolderName;
//                        Helper.checkAndCreateDirectory(thisReportPath);
//                        folderDirectory = thisReportPath;
//
//                        doc.save(thisReportPath + "/" + fileName
//                                + Constants.FILE_TYPE_DOCX);
//
//                        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
//                            doc.save(thisReportPath + "/" + fileName
//                                + Constants.FILE_TYPE_PDF);
//
//                        String previewFolder = Helper.getPathForPreviewFile();
//                        String fileNamePdf = Constants.LVI_PREVIEW_FILE_NAME
//                                + Constants.FILE_TYPE_PDF;
//
//                        Helper.checkAndCreateDirectory(previewFolder);
//                        previewFilePath = previewFolder + "/" + fileNamePdf;
//
//                        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
//                            doc.save(previewFilePath);
//
//                        Helper.progressDialog.dismiss();
//
//                        // copy photos to folderDirectory
////                for (FotoModel foto : fotoList) {
////                    if (null != foto.getFotoPath()) {
////                        InputStream is;
////                        try {
////                            // trying to copy image to folderDirectory based on
////                            // its path
////                            is = new FileInputStream(foto.getFotoPath());
////
////                            File fileImage = new File(foto.getFotoPath());
////                            if (fileImage.exists()) {
////                                String photoDirectory = folderDirectory + "/"
////                                        + Constants.PHOTO_FOLDER_NAME;
////                                new File(photoDirectory).mkdirs();
////                                String outputPath = photoDirectory
////                                        + File.separator + foto.getId()
////                                        + ".jpg";
////                                OutputStream os = new FileOutputStream(
////                                        outputPath);
////                                Helper.copyFile(is, os);
////                                // foto.setFotoPath(outputPath);
////                            }
////
////                        } catch (FileNotFoundException e) {
////                            // TODO Auto-generated catch block
//////                            Log.e("Error create photo folder", e.getMessage());
////                            e.printStackTrace();
////                        } catch (IOException e) {
////                            // TODO Auto-generated catch block
////                            e.printStackTrace();
//////                            Log.e("Error create photo folder", e.getMessage());
////                        }
////                    }
////                }
//                        return Constants.REPORT_GENERATED;
//                    } else {
//                        // preview
//                        String previewFolder = Helper.getPathForPreviewFile();
//                        String fileNamePdf = Constants.LVI_PREVIEW_FILE_NAME
//                                + Constants.FILE_TYPE_PDF;
//
//                        Helper.checkAndCreateDirectory(previewFolder);
//                        previewFilePath = previewFolder + "/" + fileNamePdf;
//                        doc.save(previewFilePath);
//
//                        Helper.progressDialog.dismiss();
//
//                        return Constants.REPORT_PREVIEW;
//                    }
//
//                } catch (Exception ex) {
//                    return Constants.REPORT_FAILED;
//                }
//
//            }

//    GENERATE REPORT PLANNED SHE INSPECTION
//            public static void generateReportPlanned(final Activity mActivity, final boolean isGenerate, final String inspector, final String date, final String location, final String areaOwner) {
//
//                Helper.showProgressDialog(mActivity);
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        String reportPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//                        reportPath += "/" + Constants.ROOT_FOLDER_NAME + "/"
//                                + Constants.APP_FOLDER_NAME + "/"
//                                + Constants.TEMPLATE_FOLDER_NAME + "/"
//                                + Constants.TEMPLATE_FILE_NAME;
//
//                        //generate file name format
////        String formattedID = PlannedFragment.formatIDInspector(selectedInspectorList.get(0).getIdno());
//                        String time = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
//                        String fullPath = "";
//                        String fileName = time + "_PSI_" + inspector;
//                        if (isGenerate) {
//                            fullPath = Environment.getExternalStorageDirectory().getAbsolutePath()
//                                    + "/"
//                                    + Constants.ROOT_FOLDER_NAME
//                                    + "/"
//                                    + Constants.APP_FOLDER_NAME
//                                    + "/"
//                                    + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
//                            File exportFolder = new File(fullPath);
//                            exportFolder.mkdir();
//
//                        }
//
//                        //create image folder
//                        String imageFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath()
//                                + "/"
//                                + Constants.ROOT_FOLDER_NAME
//                                + "/"
//                                + Constants.APP_FOLDER_NAME
//                                + "/"
//                                + Constants.EXPORT_FOLDER_NAME + "/" + fileName;
//                        File imageFolder = new File(imageFolderPath);
//
//                        String docName = time + "_PSI_" + inspector;
//
//                        PlannedFragment.exportCsv(fullPath + "/", docName + ".csv");
//
//                        try {
//                            Document doc = new Document(reportPath);
//                            DocumentBuilder builder = new DocumentBuilder(doc);
//                            doc.getRange().replace("location_header_id", location, true, true);
//                            doc.getRange().replace("date_header_id", date, true, true);
//                            doc.getRange().replace("inspector_header_id", inspector, true, true);
//                            doc.getRange().replace("area_header_id", areaOwner, true, true);
//
//                            PackageInfo pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
//                            String version = pInfo.versionName;
//                            doc.getRange().replace("version_id", "Version : " + version, true, true);
//
//                            TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
//                            String deviceID = telephonyManager.getDeviceId();
//                            doc.getRange().replace("device_id", "Imei : " + deviceID, true, true);
//
//
//                            DataSource ds = new DataSource(mActivity);
//                            ds.open();
//
//            /* ==================================
//                                    Work Area
//            ================================== */
//                            final String potenWa1, potenWa2, potenWa3, potenWa4, potenWa5, potenWa6;
//                            final String ratingWa1, ratingWa2, ratingWa3, ratingWa4, ratingWa5, ratingWa6;
//                            PlannedInspectionModel workAreaData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Work Area");
//                            if (workAreaData != null) {
//                                if (workAreaData.getRisk1() == null) {
//                                    potenWa1 = "";
//                                } else {
//                                    potenWa1 = workAreaData.getRisk1();
//                                }
//                                if (workAreaData.getRisk2() == null) {
//                                    potenWa2 = "";
//                                } else {
//                                    potenWa2 = workAreaData.getRisk2();
//                                }
//                                if (workAreaData.getRisk3() == null) {
//                                    potenWa3 = "";
//                                } else {
//                                    potenWa3 = workAreaData.getRisk3();
//                                }
//                                if (workAreaData.getRisk4() == null) {
//                                    potenWa4 = "";
//                                } else {
//                                    potenWa4 = workAreaData.getRisk4();
//                                }
//                                if (workAreaData.getRisk5() == null) {
//                                    potenWa5 = "";
//                                } else {
//                                    potenWa5 = workAreaData.getRisk5();
//                                }
//                                if (workAreaData.getRisk6() == null) {
//                                    potenWa6 = "";
//                                } else {
//                                    potenWa6 = workAreaData.getRisk6();
//                                }
//
//                                doc.getRange().replace("pr_1", potenWa1, true, true);
//                                doc.getRange().replace("pr_2", potenWa2, true, true);
//                                doc.getRange().replace("pr_3", potenWa3, true, true);
//                                doc.getRange().replace("pr_4", potenWa4, true, true);
//                                doc.getRange().replace("pr_5", potenWa5, true, true);
//                                doc.getRange().replace("pr_6", potenWa6, true, true);
//
//                                if (workAreaData.getRating1() == null) {
//                                    ratingWa1 = "";
//                                } else {
//                                    ratingWa1 = workAreaData.getRating1();
//                                }
//                                if (workAreaData.getRating2() == null) {
//                                    ratingWa2 = "";
//                                } else {
//                                    ratingWa2 = workAreaData.getRating2();
//                                }
//                                if (workAreaData.getRating3() == null) {
//                                    ratingWa3 = "";
//                                } else {
//                                    ratingWa3 = workAreaData.getRating3();
//                                }
//                                if (workAreaData.getRating4() == null) {
//                                    ratingWa4 = "";
//                                } else {
//                                    ratingWa4 = workAreaData.getRating4();
//                                }
//                                if (workAreaData.getRating5() == null) {
//                                    ratingWa5 = "";
//                                } else {
//                                    ratingWa5 = workAreaData.getRating5();
//                                }
//                                if (workAreaData.getRating6() == null) {
//                                    ratingWa6 = "";
//                                } else {
//                                    ratingWa6 = workAreaData.getRating6();
//                                }
//
//                                if (ratingWa1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_1", passCheck, true, true);
//                                    doc.getRange().replace("g_1", "", true, true);
//                                    doc.getRange().replace("na1", "", true, true);
//                                } else if (ratingWa1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_1", "", true, true);
//                                    doc.getRange().replace("g_1", passCheck, true, true);
//                                    doc.getRange().replace("na1", "", true, true);
//                                } else if (ratingWa1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_1", "", true, true);
//                                    doc.getRange().replace("g_1", "", true, true);
//                                    doc.getRange().replace("na1", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_1", "", true, true);
//                                    doc.getRange().replace("g_1", "", true, true);
//                                    doc.getRange().replace("na1", "", true, true);
//                                }
//
//                                if (ratingWa2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_2", passCheck, true, true);
//                                    doc.getRange().replace("g_2", "", true, true);
//                                    doc.getRange().replace("na2", "", true, true);
//                                } else if (ratingWa2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_2", "", true, true);
//                                    doc.getRange().replace("g_2", passCheck, true, true);
//                                    doc.getRange().replace("na2", "", true, true);
//                                } else if (ratingWa2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_2", "", true, true);
//                                    doc.getRange().replace("g_2", "", true, true);
//                                    doc.getRange().replace("na2", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_2", "", true, true);
//                                    doc.getRange().replace("g_2", "", true, true);
//                                    doc.getRange().replace("na2", "", true, true);
//                                }
//
//                                if (ratingWa3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_3", passCheck, true, true);
//                                    doc.getRange().replace("g_3", "", true, true);
//                                    doc.getRange().replace("na3", "", true, true);
//                                } else if (ratingWa3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_3", "", true, true);
//                                    doc.getRange().replace("g_3", passCheck, true, true);
//                                    doc.getRange().replace("na3", "", true, true);
//                                } else if (ratingWa3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_3", "", true, true);
//                                    doc.getRange().replace("g_3", "", true, true);
//                                    doc.getRange().replace("na3", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_3", "", true, true);
//                                    doc.getRange().replace("g_3", "", true, true);
//                                    doc.getRange().replace("na3", "", true, true);
//                                }
//
//                                if (ratingWa4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_4", passCheck, true, true);
//                                    doc.getRange().replace("g_4", "", true, true);
//                                    doc.getRange().replace("na4", "", true, true);
//                                } else if (ratingWa4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_4", "", true, true);
//                                    doc.getRange().replace("g_4", passCheck, true, true);
//                                    doc.getRange().replace("na4", "", true, true);
//                                } else if (ratingWa4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_4", "", true, true);
//                                    doc.getRange().replace("g_4", "", true, true);
//                                    doc.getRange().replace("na4", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_4", "", true, true);
//                                    doc.getRange().replace("g_4", "", true, true);
//                                    doc.getRange().replace("na4", "", true, true);
//                                }
//
//                                if (ratingWa5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_5", passCheck, true, true);
//                                    doc.getRange().replace("g_5", "", true, true);
//                                    doc.getRange().replace("na5", "", true, true);
//                                } else if (ratingWa5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_5", "", true, true);
//                                    doc.getRange().replace("g_5", passCheck, true, true);
//                                    doc.getRange().replace("na5", "", true, true);
//                                } else if (ratingWa5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_5", "", true, true);
//                                    doc.getRange().replace("g_5", "", true, true);
//                                    doc.getRange().replace("na5", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_5", "", true, true);
//                                    doc.getRange().replace("g_5", "", true, true);
//                                    doc.getRange().replace("na5", "", true, true);
//                                }
//
//                                if (ratingWa6.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_6", passCheck, true, true);
//                                    doc.getRange().replace("g_6", "", true, true);
//                                    doc.getRange().replace("na6", "", true, true);
//                                } else if (ratingWa6.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_6", "", true, true);
//                                    doc.getRange().replace("g_6", passCheck, true, true);
//                                    doc.getRange().replace("na6", "", true, true);
//                                } else if (ratingWa6.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_6", "", true, true);
//                                    doc.getRange().replace("g_6", "", true, true);
//                                    doc.getRange().replace("na6", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_6", "", true, true);
//                                    doc.getRange().replace("g_6", "", true, true);
//                                    doc.getRange().replace("na6", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_1", "", true, true);
//                                doc.getRange().replace("pr_2", "", true, true);
//                                doc.getRange().replace("pr_3", "", true, true);
//                                doc.getRange().replace("pr_4", "", true, true);
//                                doc.getRange().replace("pr_5", "", true, true);
//                                doc.getRange().replace("pr_6", "", true, true);
//
//                                doc.getRange().replace("nw_1", "", true, true);
//                                doc.getRange().replace("g_1", "", true, true);
//                                doc.getRange().replace("na1", "", true, true);
//
//                                doc.getRange().replace("nw_2", "", true, true);
//                                doc.getRange().replace("g_2", "", true, true);
//                                doc.getRange().replace("na2", "", true, true);
//
//                                doc.getRange().replace("nw_3", "", true, true);
//                                doc.getRange().replace("g_3", "", true, true);
//                                doc.getRange().replace("na3", "", true, true);
//
//                                doc.getRange().replace("nw_4", "", true, true);
//                                doc.getRange().replace("g_4", "", true, true);
//                                doc.getRange().replace("na4", "", true, true);
//
//                                doc.getRange().replace("nw_5", "", true, true);
//                                doc.getRange().replace("g_5", "", true, true);
//                                doc.getRange().replace("na5", "", true, true);
//
//                                doc.getRange().replace("nw_6", "", true, true);
//                                doc.getRange().replace("g_6", "", true, true);
//                                doc.getRange().replace("na6", "", true, true);
//                            }
//
//              /* ==================================
//                                    Structures
//            ================================== */
//                            final String potenSt1, potenSt2, potenSt3, potenSt4, potenSt5;
//                            final String ratingSt1, ratingSt2, ratingSt3, ratingSt4, ratingSt5;
//                            PlannedInspectionModel structureData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Structure");
//                            if (structureData != null) {
//                                if (structureData.getRisk1() == null) {
//                                    potenSt1 = "";
//                                } else {
//                                    potenSt1 = structureData.getRisk1();
//                                }
//                                if (structureData.getRisk2() == null) {
//                                    potenSt2 = "";
//                                } else {
//                                    potenSt2 = structureData.getRisk2();
//                                }
//                                if (structureData.getRisk3() == null) {
//                                    potenSt3 = "";
//                                } else {
//                                    potenSt3 = structureData.getRisk3();
//                                }
//                                if (structureData.getRisk4() == null) {
//                                    potenSt4 = "";
//                                } else {
//                                    potenSt4 = structureData.getRisk4();
//                                }
//                                if (structureData.getRisk5() == null) {
//                                    potenSt5 = "";
//                                } else {
//                                    potenSt5 = structureData.getRisk5();
//                                }
//
//                                doc.getRange().replace("pr_7", potenSt1, true, true);
//                                doc.getRange().replace("pr_8", potenSt2, true, true);
//                                doc.getRange().replace("pr_9", potenSt3, true, true);
//                                doc.getRange().replace("pr_10", potenSt4, true, true);
//                                doc.getRange().replace("pr_11", potenSt5, true, true);
//
//                                if (structureData.getRating1() == null) {
//                                    ratingSt1 = "";
//                                } else {
//                                    ratingSt1 = structureData.getRating1();
//                                }
//                                if (structureData.getRating2() == null) {
//                                    ratingSt2 = "";
//                                } else {
//                                    ratingSt2 = structureData.getRating2();
//                                }
//                                if (structureData.getRating3() == null) {
//                                    ratingSt3 = "";
//                                } else {
//                                    ratingSt3 = structureData.getRating3();
//                                }
//                                if (structureData.getRating4() == null) {
//                                    ratingSt4 = "";
//                                } else {
//                                    ratingSt4 = structureData.getRating4();
//                                }
//                                if (structureData.getRating5() == null) {
//                                    ratingSt5 = "";
//                                } else {
//                                    ratingSt5 = structureData.getRating5();
//                                }
//
//                                if (ratingSt1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_7", passCheck, true, true);
//                                    doc.getRange().replace("g_7", "", true, true);
//                                    doc.getRange().replace("na7", "", true, true);
//                                } else if (ratingSt1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_7", "", true, true);
//                                    doc.getRange().replace("g_7", passCheck, true, true);
//                                    doc.getRange().replace("na7", "", true, true);
//                                } else if (ratingSt1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_7", "", true, true);
//                                    doc.getRange().replace("g_7", "", true, true);
//                                    doc.getRange().replace("na7", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_7", "", true, true);
//                                    doc.getRange().replace("g_7", "", true, true);
//                                    doc.getRange().replace("na7", "", true, true);
//                                }
//
//                                if (ratingSt2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_8", passCheck, true, true);
//                                    doc.getRange().replace("g_8", "", true, true);
//                                    doc.getRange().replace("na8", "", true, true);
//                                } else if (ratingSt2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_8", "", true, true);
//                                    doc.getRange().replace("g_8", passCheck, true, true);
//                                    doc.getRange().replace("na8", "", true, true);
//                                } else if (ratingSt2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_8", "", true, true);
//                                    doc.getRange().replace("g_8", "", true, true);
//                                    doc.getRange().replace("na8", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_8", "", true, true);
//                                    doc.getRange().replace("g_8", "", true, true);
//                                    doc.getRange().replace("na8", "", true, true);
//                                }
//
//                                if (ratingSt3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_9", passCheck, true, true);
//                                    doc.getRange().replace("g_9", "", true, true);
//                                    doc.getRange().replace("na9", "", true, true);
//                                } else if (ratingSt3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_9", "", true, true);
//                                    doc.getRange().replace("g_9", passCheck, true, true);
//                                    doc.getRange().replace("na9", "", true, true);
//                                } else if (ratingSt3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_9", "", true, true);
//                                    doc.getRange().replace("g_9", "", true, true);
//                                    doc.getRange().replace("na9", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_9", "", true, true);
//                                    doc.getRange().replace("g_9", "", true, true);
//                                    doc.getRange().replace("na9", "", true, true);
//                                }
//
//                                if (ratingSt4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_10", passCheck, true, true);
//                                    doc.getRange().replace("g_10", "", true, true);
//                                    doc.getRange().replace("na10", "", true, true);
//                                } else if (ratingSt4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_10", "", true, true);
//                                    doc.getRange().replace("g_10", passCheck, true, true);
//                                    doc.getRange().replace("na10", "", true, true);
//                                } else if (ratingSt4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_10", "", true, true);
//                                    doc.getRange().replace("g_10", "", true, true);
//                                    doc.getRange().replace("na10", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_10", "", true, true);
//                                    doc.getRange().replace("g_10", "", true, true);
//                                    doc.getRange().replace("na10", "", true, true);
//                                }
//
//                                if (ratingSt5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_11", passCheck, true, true);
//                                    doc.getRange().replace("g_11", "", true, true);
//                                    doc.getRange().replace("na11", "", true, true);
//                                } else if (ratingSt5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_11", "", true, true);
//                                    doc.getRange().replace("g_11", passCheck, true, true);
//                                    doc.getRange().replace("na11", "", true, true);
//                                } else if (ratingSt5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_11", "", true, true);
//                                    doc.getRange().replace("g_11", "", true, true);
//                                    doc.getRange().replace("na11", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_11", "", true, true);
//                                    doc.getRange().replace("g_11", "", true, true);
//                                    doc.getRange().replace("na11", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_7", "", true, true);
//                                doc.getRange().replace("pr_8", "", true, true);
//                                doc.getRange().replace("pr_9", "", true, true);
//                                doc.getRange().replace("pr_10", "", true, true);
//                                doc.getRange().replace("pr_11", "", true, true);
//
//                                doc.getRange().replace("nw_7", "", true, true);
//                                doc.getRange().replace("g_7", "", true, true);
//                                doc.getRange().replace("na7", "", true, true);
//
//                                doc.getRange().replace("nw_8", "", true, true);
//                                doc.getRange().replace("g_8", "", true, true);
//                                doc.getRange().replace("na8", "", true, true);
//
//                                doc.getRange().replace("nw_9", "", true, true);
//                                doc.getRange().replace("g_9", "", true, true);
//                                doc.getRange().replace("na9", "", true, true);
//
//                                doc.getRange().replace("nw_10", "", true, true);
//                                doc.getRange().replace("g_10", "", true, true);
//                                doc.getRange().replace("na10", "", true, true);
//
//                                doc.getRange().replace("nw_11", "", true, true);
//                                doc.getRange().replace("g_11", "", true, true);
//                                doc.getRange().replace("na11", "", true, true);
//                            }
//
//            /* ==================================
//                                    Machinery
//            ================================== */
//                            final String potenMt1, potenMt2, potenMt3, potenMt4, potenMt5, potenMt6, potenMt7;
//                            final String ratingMt1, ratingMt2, ratingMt3, ratingMt4, ratingMt5, ratingMt6, ratingMt7;
//                            PlannedInspectionModel machineryData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Machinery");
//                            if (machineryData != null) {
//                                if (machineryData.getRisk1() == null) {
//                                    potenMt1 = "";
//                                } else {
//                                    potenMt1 = machineryData.getRisk1();
//                                }
//                                if (machineryData.getRisk2() == null) {
//                                    potenMt2 = "";
//                                } else {
//                                    potenMt2 = machineryData.getRisk2();
//                                }
//                                if (machineryData.getRisk3() == null) {
//                                    potenMt3 = "";
//                                } else {
//                                    potenMt3 = machineryData.getRisk3();
//                                }
//                                if (machineryData.getRisk4() == null) {
//                                    potenMt4 = "";
//                                } else {
//                                    potenMt4 = machineryData.getRisk4();
//                                }
//                                if (machineryData.getRisk5() == null) {
//                                    potenMt5 = "";
//                                } else {
//                                    potenMt5 = machineryData.getRisk5();
//                                }
//                                if (machineryData.getRisk6() == null) {
//                                    potenMt6 = "";
//                                } else {
//                                    potenMt6 = machineryData.getRisk6();
//                                }
//                                if (machineryData.getRisk7() == null) {
//                                    potenMt7 = "";
//                                } else {
//                                    potenMt7 = machineryData.getRisk7();
//                                }
//
//                                doc.getRange().replace("pr_12", potenMt1, true, true);
//                                doc.getRange().replace("pr_13", potenMt2, true, true);
//                                doc.getRange().replace("pr_14", potenMt3, true, true);
//                                doc.getRange().replace("pr_15", potenMt4, true, true);
//                                doc.getRange().replace("pr_16", potenMt5, true, true);
//                                doc.getRange().replace("pr_17", potenMt6, true, true);
//                                doc.getRange().replace("pr_18", potenMt7, true, true);
//
//                                if (machineryData.getRating1() == null) {
//                                    ratingMt1 = "";
//                                } else {
//                                    ratingMt1 = machineryData.getRating1();
//                                }
//                                if (machineryData.getRating2() == null) {
//                                    ratingMt2 = "";
//                                } else {
//                                    ratingMt2 = machineryData.getRating2();
//                                }
//                                if (machineryData.getRating3() == null) {
//                                    ratingMt3 = "";
//                                } else {
//                                    ratingMt3 = machineryData.getRating3();
//                                }
//                                if (machineryData.getRating4() == null) {
//                                    ratingMt4 = "";
//                                } else {
//                                    ratingMt4 = machineryData.getRating4();
//                                }
//                                if (machineryData.getRating5() == null) {
//                                    ratingMt5 = "";
//                                } else {
//                                    ratingMt5 = machineryData.getRating5();
//                                }
//                                if (machineryData.getRating6() == null) {
//                                    ratingMt6 = "";
//                                } else {
//                                    ratingMt6 = machineryData.getRating6();
//                                }
//                                if (machineryData.getRating7() == null) {
//                                    ratingMt7 = "";
//                                } else {
//                                    ratingMt7 = machineryData.getRating7();
//                                }
//
//                                if (ratingMt1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_12", passCheck, true, true);
//                                    doc.getRange().replace("g_12", "", true, true);
//                                    doc.getRange().replace("na12", "", true, true);
//                                } else if (ratingMt1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_12", "", true, true);
//                                    doc.getRange().replace("g_12", passCheck, true, true);
//                                    doc.getRange().replace("na12", "", true, true);
//                                } else if (ratingMt1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_12", "", true, true);
//                                    doc.getRange().replace("g_12", "", true, true);
//                                    doc.getRange().replace("na12", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_12", "", true, true);
//                                    doc.getRange().replace("g_12", "", true, true);
//                                    doc.getRange().replace("na12", "", true, true);
//                                }
//
//                                if (ratingMt2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_13", passCheck, true, true);
//                                    doc.getRange().replace("g_13", "", true, true);
//                                    doc.getRange().replace("na13", "", true, true);
//                                } else if (ratingMt2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_13", "", true, true);
//                                    doc.getRange().replace("g_13", passCheck, true, true);
//                                    doc.getRange().replace("na13", "", true, true);
//                                } else if (ratingMt2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_13", "", true, true);
//                                    doc.getRange().replace("g_13", "", true, true);
//                                    doc.getRange().replace("na13", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_13", "", true, true);
//                                    doc.getRange().replace("g_13", "", true, true);
//                                    doc.getRange().replace("na13", "", true, true);
//                                }
//
//                                if (ratingMt3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_14", passCheck, true, true);
//                                    doc.getRange().replace("g_14", "", true, true);
//                                    doc.getRange().replace("na14", "", true, true);
//                                } else if (ratingMt3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_14", "", true, true);
//                                    doc.getRange().replace("g_14", passCheck, true, true);
//                                    doc.getRange().replace("na14", "", true, true);
//                                } else if (ratingMt3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_14", "", true, true);
//                                    doc.getRange().replace("g_14", "", true, true);
//                                    doc.getRange().replace("na14", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_14", "", true, true);
//                                    doc.getRange().replace("g_14", "", true, true);
//                                    doc.getRange().replace("na14", "", true, true);
//                                }
//
//                                if (ratingMt4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_15", passCheck, true, true);
//                                    doc.getRange().replace("g_15", "", true, true);
//                                    doc.getRange().replace("na15", "", true, true);
//                                } else if (ratingMt4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_15", "", true, true);
//                                    doc.getRange().replace("g_15", passCheck, true, true);
//                                    doc.getRange().replace("na15", "", true, true);
//                                } else if (ratingMt4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_15", "", true, true);
//                                    doc.getRange().replace("g_15", "", true, true);
//                                    doc.getRange().replace("na15", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_15", "", true, true);
//                                    doc.getRange().replace("g_15", "", true, true);
//                                    doc.getRange().replace("na15", "", true, true);
//                                }
//
//                                if (ratingMt5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_16", passCheck, true, true);
//                                    doc.getRange().replace("g_16", "", true, true);
//                                    doc.getRange().replace("na16", "", true, true);
//                                } else if (ratingMt5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_16", "", true, true);
//                                    doc.getRange().replace("g_16", passCheck, true, true);
//                                    doc.getRange().replace("na16", "", true, true);
//                                } else if (ratingMt5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_16", "", true, true);
//                                    doc.getRange().replace("g_16", "", true, true);
//                                    doc.getRange().replace("na16", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_16", "", true, true);
//                                    doc.getRange().replace("g_16", "", true, true);
//                                    doc.getRange().replace("na16", "", true, true);
//                                }
//
//                                if (ratingMt6.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_17", passCheck, true, true);
//                                    doc.getRange().replace("g_17", "", true, true);
//                                    doc.getRange().replace("na17", "", true, true);
//                                } else if (ratingMt6.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_17", "", true, true);
//                                    doc.getRange().replace("g_17", passCheck, true, true);
//                                    doc.getRange().replace("na17", "", true, true);
//                                } else if (ratingMt6.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_17", "", true, true);
//                                    doc.getRange().replace("g_17", "", true, true);
//                                    doc.getRange().replace("na17", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_17", "", true, true);
//                                    doc.getRange().replace("g_17", "", true, true);
//                                    doc.getRange().replace("na17", "", true, true);
//                                }
//
//                                if (ratingMt7.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_18", passCheck, true, true);
//                                    doc.getRange().replace("g_18", "", true, true);
//                                    doc.getRange().replace("na18", "", true, true);
//                                } else if (ratingMt7.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_18", "", true, true);
//                                    doc.getRange().replace("g_18", passCheck, true, true);
//                                    doc.getRange().replace("na18", "", true, true);
//                                } else if (ratingMt7.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_18", "", true, true);
//                                    doc.getRange().replace("g_18", "", true, true);
//                                    doc.getRange().replace("na18", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_18", "", true, true);
//                                    doc.getRange().replace("g_18", "", true, true);
//                                    doc.getRange().replace("na18", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_12", "", true, true);
//                                doc.getRange().replace("pr_13", "", true, true);
//                                doc.getRange().replace("pr_14", "", true, true);
//                                doc.getRange().replace("pr_15", "", true, true);
//                                doc.getRange().replace("pr_16", "", true, true);
//                                doc.getRange().replace("pr_17", "", true, true);
//                                doc.getRange().replace("pr_18", "", true, true);
//
//                                doc.getRange().replace("nw_12", "", true, true);
//                                doc.getRange().replace("g_12", "", true, true);
//                                doc.getRange().replace("na12", "", true, true);
//
//                                doc.getRange().replace("nw_13", "", true, true);
//                                doc.getRange().replace("g_13", "", true, true);
//                                doc.getRange().replace("na13", "", true, true);
//
//                                doc.getRange().replace("nw_14", "", true, true);
//                                doc.getRange().replace("g_14", "", true, true);
//                                doc.getRange().replace("na14", "", true, true);
//
//                                doc.getRange().replace("nw_15", "", true, true);
//                                doc.getRange().replace("g_15", "", true, true);
//                                doc.getRange().replace("na15", "", true, true);
//
//                                doc.getRange().replace("nw_16", "", true, true);
//                                doc.getRange().replace("g_16", "", true, true);
//                                doc.getRange().replace("na16", "", true, true);
//
//                                doc.getRange().replace("nw_17", "", true, true);
//                                doc.getRange().replace("g_17", "", true, true);
//                                doc.getRange().replace("na17", "", true, true);
//
//                                doc.getRange().replace("nw_18", "", true, true);
//                                doc.getRange().replace("g_18", "", true, true);
//                                doc.getRange().replace("na18", "", true, true);
//                            }
//
//            /* ==================================
//                                    Electrical
//            ================================== */
//                            final String potenEl1, potenEl2, potenEl3, potenEl4, potenEl5;
//                            final String ratingEl1, ratingEl2, ratingEl3, ratingEl4, ratingEl5;
//                            PlannedInspectionModel electricalData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Electrical");
//                            if (electricalData != null) {
//                                if (electricalData.getRisk1() == null) {
//                                    potenEl1 = "";
//                                } else {
//                                    potenEl1 = electricalData.getRisk1();
//                                }
//                                if (electricalData.getRisk2() == null) {
//                                    potenEl2 = "";
//                                } else {
//                                    potenEl2 = electricalData.getRisk2();
//                                }
//                                if (electricalData.getRisk3() == null) {
//                                    potenEl3 = "";
//                                } else {
//                                    potenEl3 = electricalData.getRisk3();
//                                }
//                                if (electricalData.getRisk4() == null) {
//                                    potenEl4 = "";
//                                } else {
//                                    potenEl4 = electricalData.getRisk4();
//                                }
//                                if (electricalData.getRisk5() == null) {
//                                    potenEl5 = "";
//                                } else {
//                                    potenEl5 = electricalData.getRisk5();
//                                }
//
//                                doc.getRange().replace("pr_19", potenEl1, true, true);
//                                doc.getRange().replace("pr_20", potenEl2, true, true);
//                                doc.getRange().replace("pr_21", potenEl3, true, true);
//                                doc.getRange().replace("pr_22", potenEl4, true, true);
//                                doc.getRange().replace("pr_23", potenEl5, true, true);
//
//                                if (electricalData.getRating1() == null) {
//                                    ratingEl1 = "";
//                                } else {
//                                    ratingEl1 = electricalData.getRating1();
//                                }
//                                if (electricalData.getRating2() == null) {
//                                    ratingEl2 = "";
//                                } else {
//                                    ratingEl2 = electricalData.getRating2();
//                                }
//                                if (electricalData.getRating3() == null) {
//                                    ratingEl3 = "";
//                                } else {
//                                    ratingEl3 = electricalData.getRating3();
//                                }
//                                if (electricalData.getRating4() == null) {
//                                    ratingEl4 = "";
//                                } else {
//                                    ratingEl4 = electricalData.getRating4();
//                                }
//                                if (electricalData.getRating5() == null) {
//                                    ratingEl5 = "";
//                                } else {
//                                    ratingEl5 = electricalData.getRating5();
//                                }
//
//                                if (ratingEl1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_19", passCheck, true, true);
//                                    doc.getRange().replace("g_19", "", true, true);
//                                    doc.getRange().replace("na19", "", true, true);
//                                } else if (ratingEl1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_19", "", true, true);
//                                    doc.getRange().replace("g_19", passCheck, true, true);
//                                    doc.getRange().replace("na19", "", true, true);
//                                } else if (ratingEl1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_19", "", true, true);
//                                    doc.getRange().replace("g_19", "", true, true);
//                                    doc.getRange().replace("na19", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_19", "", true, true);
//                                    doc.getRange().replace("g_19", "", true, true);
//                                    doc.getRange().replace("na19", "", true, true);
//                                }
//
//                                if (ratingEl2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_20", passCheck, true, true);
//                                    doc.getRange().replace("g_20", "", true, true);
//                                    doc.getRange().replace("na20", "", true, true);
//                                } else if (ratingEl2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_20", "", true, true);
//                                    doc.getRange().replace("g_20", passCheck, true, true);
//                                    doc.getRange().replace("na20", "", true, true);
//                                } else if (ratingEl2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_20", "", true, true);
//                                    doc.getRange().replace("g_20", "", true, true);
//                                    doc.getRange().replace("na20", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_20", "", true, true);
//                                    doc.getRange().replace("g_20", "", true, true);
//                                    doc.getRange().replace("na20", "", true, true);
//                                }
//
//                                if (ratingEl3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_21", passCheck, true, true);
//                                    doc.getRange().replace("g_21", "", true, true);
//                                    doc.getRange().replace("na21", "", true, true);
//                                } else if (ratingEl3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_21", "", true, true);
//                                    doc.getRange().replace("g_21", passCheck, true, true);
//                                    doc.getRange().replace("na21", "", true, true);
//                                } else if (ratingEl3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_21", "", true, true);
//                                    doc.getRange().replace("g_21", "", true, true);
//                                    doc.getRange().replace("na21", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_21", "", true, true);
//                                    doc.getRange().replace("g_21", "", true, true);
//                                    doc.getRange().replace("na21", "", true, true);
//                                }
//
//                                if (ratingEl4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_22", passCheck, true, true);
//                                    doc.getRange().replace("g_22", "", true, true);
//                                    doc.getRange().replace("na22", "", true, true);
//                                } else if (ratingEl4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_22", "", true, true);
//                                    doc.getRange().replace("g_22", passCheck, true, true);
//                                    doc.getRange().replace("na22", "", true, true);
//                                } else if (ratingEl4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_22", "", true, true);
//                                    doc.getRange().replace("g_22", "", true, true);
//                                    doc.getRange().replace("na22", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_22", "", true, true);
//                                    doc.getRange().replace("g_22", "", true, true);
//                                    doc.getRange().replace("na22", "", true, true);
//                                }
//
//                                if (ratingEl5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_23", passCheck, true, true);
//                                    doc.getRange().replace("g_23", "", true, true);
//                                    doc.getRange().replace("na23", "", true, true);
//                                } else if (ratingEl5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_23", "", true, true);
//                                    doc.getRange().replace("g_23", passCheck, true, true);
//                                    doc.getRange().replace("na23", "", true, true);
//                                } else if (ratingEl5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_23", "", true, true);
//                                    doc.getRange().replace("g_23", "", true, true);
//                                    doc.getRange().replace("na23", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_23", "", true, true);
//                                    doc.getRange().replace("g_23", "", true, true);
//                                    doc.getRange().replace("na23", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_19", "", true, true);
//                                doc.getRange().replace("pr_20", "", true, true);
//                                doc.getRange().replace("pr_21", "", true, true);
//                                doc.getRange().replace("pr_22", "", true, true);
//                                doc.getRange().replace("pr_23", "", true, true);
//
//                                doc.getRange().replace("nw_19", "", true, true);
//                                doc.getRange().replace("g_19", "", true, true);
//                                doc.getRange().replace("na19", "", true, true);
//
//                                doc.getRange().replace("nw_20", "", true, true);
//                                doc.getRange().replace("g_20", "", true, true);
//                                doc.getRange().replace("na20", "", true, true);
//
//                                doc.getRange().replace("nw_21", "", true, true);
//                                doc.getRange().replace("g_21", "", true, true);
//                                doc.getRange().replace("na21", "", true, true);
//
//                                doc.getRange().replace("nw_22", "", true, true);
//                                doc.getRange().replace("g_22", "", true, true);
//                                doc.getRange().replace("na22", "", true, true);
//
//                                doc.getRange().replace("nw_23", "", true, true);
//                                doc.getRange().replace("g_23", "", true, true);
//                                doc.getRange().replace("na23", "", true, true);
//                            }
//
//             /* ==================================
//                                    Personel
//            ================================== */
//                            final String potenPe1, potenPe2, potenPe3, potenPe4, potenPe5, potenPe6, potenPe7, potenPe8;
//                            final String ratingPe1, ratingPe2, ratingPe3, ratingPe4, ratingPe5, ratingPe6, ratingPe7, ratingPe8;
//                            PlannedInspectionModel peronelData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Personel");
//                            if (peronelData != null) {
//                                if (peronelData.getRisk1() == null) {
//                                    potenPe1 = "";
//                                } else {
//                                    potenPe1 = peronelData.getRisk1();
//                                }
//                                if (peronelData.getRisk2() == null) {
//                                    potenPe2 = "";
//                                } else {
//                                    potenPe2 = peronelData.getRisk2();
//                                }
//                                if (peronelData.getRisk3() == null) {
//                                    potenPe3 = "";
//                                } else {
//                                    potenPe3 = peronelData.getRisk3();
//                                }
//                                if (peronelData.getRisk4() == null) {
//                                    potenPe4 = "";
//                                } else {
//                                    potenPe4 = peronelData.getRisk4();
//                                }
//                                if (peronelData.getRisk5() == null) {
//                                    potenPe5 = "";
//                                } else {
//                                    potenPe5 = peronelData.getRisk5();
//                                }
//                                if (peronelData.getRisk6() == null) {
//                                    potenPe6 = "";
//                                } else {
//                                    potenPe6 = peronelData.getRisk6();
//                                }
//                                if (peronelData.getRisk7() == null) {
//                                    potenPe7 = "";
//                                } else {
//                                    potenPe7 = peronelData.getRisk7();
//                                }
//                                if (peronelData.getRisk8() == null) {
//                                    potenPe8 = "";
//                                } else {
//                                    potenPe8 = peronelData.getRisk8();
//                                }
//
//                                doc.getRange().replace("pr_24", potenPe1, true, true);
//                                doc.getRange().replace("pr_25", potenPe2, true, true);
//                                doc.getRange().replace("pr_26", potenPe3, true, true);
//                                doc.getRange().replace("pr_27", potenPe4, true, true);
//                                doc.getRange().replace("pr_28", potenPe5, true, true);
//                                doc.getRange().replace("pr_29", potenPe6, true, true);
//                                doc.getRange().replace("pr_30", potenPe7, true, true);
//                                doc.getRange().replace("pr_31", potenPe8, true, true);
//
//                                if (peronelData.getRating1() == null) {
//                                    ratingPe1 = "";
//                                } else {
//                                    ratingPe1 = peronelData.getRating1();
//                                }
//                                if (peronelData.getRating2() == null) {
//                                    ratingPe2 = "";
//                                } else {
//                                    ratingPe2 = peronelData.getRating2();
//                                }
//                                if (peronelData.getRating3() == null) {
//                                    ratingPe3 = "";
//                                } else {
//                                    ratingPe3 = peronelData.getRating3();
//                                }
//                                if (peronelData.getRating4() == null) {
//                                    ratingPe4 = "";
//                                } else {
//                                    ratingPe4 = peronelData.getRating4();
//                                }
//                                if (peronelData.getRating5() == null) {
//                                    ratingPe5 = "";
//                                } else {
//                                    ratingPe5 = peronelData.getRating5();
//                                }
//                                if (peronelData.getRating6() == null) {
//                                    ratingPe6 = "";
//                                } else {
//                                    ratingPe6 = peronelData.getRating6();
//                                }
//                                if (peronelData.getRating7() == null) {
//                                    ratingPe7 = "";
//                                } else {
//                                    ratingPe7 = peronelData.getRating7();
//                                }
//                                if (peronelData.getRating8() == null) {
//                                    ratingPe8 = "";
//                                } else {
//                                    ratingPe8 = peronelData.getRating8();
//                                }
//
//                                if (ratingPe1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_24", passCheck, true, true);
//                                    doc.getRange().replace("g_24", "", true, true);
//                                    doc.getRange().replace("na24", "", true, true);
//                                } else if (ratingPe1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_24", "", true, true);
//                                    doc.getRange().replace("g_24", passCheck, true, true);
//                                    doc.getRange().replace("na24", "", true, true);
//                                } else if (ratingPe1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_24", "", true, true);
//                                    doc.getRange().replace("g_24", "", true, true);
//                                    doc.getRange().replace("na24", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_24", "", true, true);
//                                    doc.getRange().replace("g_24", "", true, true);
//                                    doc.getRange().replace("na24", "", true, true);
//                                }
//
//                                if (ratingPe2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_25", passCheck, true, true);
//                                    doc.getRange().replace("g_25", "", true, true);
//                                    doc.getRange().replace("na25", "", true, true);
//                                } else if (ratingPe2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_25", "", true, true);
//                                    doc.getRange().replace("g_25", passCheck, true, true);
//                                    doc.getRange().replace("na25", "", true, true);
//                                } else if (ratingPe2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_25", "", true, true);
//                                    doc.getRange().replace("g_25", "", true, true);
//                                    doc.getRange().replace("na25", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_25", "", true, true);
//                                    doc.getRange().replace("g_25", "", true, true);
//                                    doc.getRange().replace("na25", "", true, true);
//                                }
//
//                                if (ratingPe3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_26", passCheck, true, true);
//                                    doc.getRange().replace("g_26", "", true, true);
//                                    doc.getRange().replace("na26", "", true, true);
//                                } else if (ratingPe3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_26", "", true, true);
//                                    doc.getRange().replace("g_26", passCheck, true, true);
//                                    doc.getRange().replace("na26", "", true, true);
//                                } else if (ratingPe3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_26", "", true, true);
//                                    doc.getRange().replace("g_26", "", true, true);
//                                    doc.getRange().replace("na26", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_26", "", true, true);
//                                    doc.getRange().replace("g_26", "", true, true);
//                                    doc.getRange().replace("na26", "", true, true);
//                                }
//
//                                if (ratingPe4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_27", passCheck, true, true);
//                                    doc.getRange().replace("g_27", "", true, true);
//                                    doc.getRange().replace("na27", "", true, true);
//                                } else if (ratingPe4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_27", "", true, true);
//                                    doc.getRange().replace("g_27", passCheck, true, true);
//                                    doc.getRange().replace("na27", "", true, true);
//                                } else if (ratingPe4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_27", "", true, true);
//                                    doc.getRange().replace("g_27", "", true, true);
//                                    doc.getRange().replace("na27", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_27", "", true, true);
//                                    doc.getRange().replace("g_27", "", true, true);
//                                    doc.getRange().replace("na27", "", true, true);
//                                }
//
//                                if (ratingPe5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_28", passCheck, true, true);
//                                    doc.getRange().replace("g_28", "", true, true);
//                                    doc.getRange().replace("na28", "", true, true);
//                                } else if (ratingPe5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_28", "", true, true);
//                                    doc.getRange().replace("g_28", passCheck, true, true);
//                                    doc.getRange().replace("na28", "", true, true);
//                                } else if (ratingPe5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_28", "", true, true);
//                                    doc.getRange().replace("g_28", "", true, true);
//                                    doc.getRange().replace("na28", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_28", "", true, true);
//                                    doc.getRange().replace("g_28", "", true, true);
//                                    doc.getRange().replace("na28", "", true, true);
//                                }
//
//                                if (ratingPe6.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_29", passCheck, true, true);
//                                    doc.getRange().replace("g_29", "", true, true);
//                                    doc.getRange().replace("na29", "", true, true);
//                                } else if (ratingPe6.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_29", "", true, true);
//                                    doc.getRange().replace("g_29", passCheck, true, true);
//                                    doc.getRange().replace("na29", "", true, true);
//                                } else if (ratingPe6.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_29", "", true, true);
//                                    doc.getRange().replace("g_29", "", true, true);
//                                    doc.getRange().replace("na29", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_29", "", true, true);
//                                    doc.getRange().replace("g_29", "", true, true);
//                                    doc.getRange().replace("na29", passCheck, true, true);
//                                }
//
//                                if (ratingPe7.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_30", passCheck, true, true);
//                                    doc.getRange().replace("g_30", "", true, true);
//                                    doc.getRange().replace("na30", "", true, true);
//                                } else if (ratingPe7.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_30", "", true, true);
//                                    doc.getRange().replace("g_30", passCheck, true, true);
//                                    doc.getRange().replace("na30", "", true, true);
//                                } else if (ratingPe7.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_30", "", true, true);
//                                    doc.getRange().replace("g_30", "", true, true);
//                                    doc.getRange().replace("na30", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_30", "", true, true);
//                                    doc.getRange().replace("g_30", "", true, true);
//                                    doc.getRange().replace("na30", "", true, true);
//                                }
//
//                                if (ratingPe8.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_31", passCheck, true, true);
//                                    doc.getRange().replace("g_31", "", true, true);
//                                    doc.getRange().replace("na31", "", true, true);
//                                } else if (ratingPe8.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_31", "", true, true);
//                                    doc.getRange().replace("g_31", passCheck, true, true);
//                                    doc.getRange().replace("na31", "", true, true);
//                                } else if (ratingPe8.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_31", "", true, true);
//                                    doc.getRange().replace("g_31", "", true, true);
//                                    doc.getRange().replace("na31", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_31", "", true, true);
//                                    doc.getRange().replace("g_31", "", true, true);
//                                    doc.getRange().replace("na31", passCheck, true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_24", "", true, true);
//                                doc.getRange().replace("pr_25", "", true, true);
//                                doc.getRange().replace("pr_26", "", true, true);
//                                doc.getRange().replace("pr_27", "", true, true);
//                                doc.getRange().replace("pr_28", "", true, true);
//                                doc.getRange().replace("pr_29", "", true, true);
//                                doc.getRange().replace("pr_30", "", true, true);
//                                doc.getRange().replace("pr_31", "", true, true);
//
//                                doc.getRange().replace("nw_24", "", true, true);
//                                doc.getRange().replace("g_24", "", true, true);
//                                doc.getRange().replace("na24", "", true, true);
//
//                                doc.getRange().replace("nw_2", "", true, true);
//                                doc.getRange().replace("g_25", "", true, true);
//                                doc.getRange().replace("na25", "", true, true);
//
//                                doc.getRange().replace("nw_26", "", true, true);
//                                doc.getRange().replace("g_26", "", true, true);
//                                doc.getRange().replace("na26", "", true, true);
//
//                                doc.getRange().replace("nw_27", "", true, true);
//                                doc.getRange().replace("g_27", "", true, true);
//                                doc.getRange().replace("na27", "", true, true);
//
//                                doc.getRange().replace("nw_28", "", true, true);
//                                doc.getRange().replace("g_28", "", true, true);
//                                doc.getRange().replace("na28", "", true, true);
//
//                                doc.getRange().replace("nw_29", "", true, true);
//                                doc.getRange().replace("g_29", "", true, true);
//                                doc.getRange().replace("na29", "", true, true);
//
//                                doc.getRange().replace("nw_30", "", true, true);
//                                doc.getRange().replace("g_30", "", true, true);
//                                doc.getRange().replace("na30", "", true, true);
//
//                                doc.getRange().replace("nw_31", "", true, true);
//                                doc.getRange().replace("g_31", "", true, true);
//                                doc.getRange().replace("na31", "", true, true);
//                            }
//
//            /* ==================================
//                                    Fire Safety
//            ================================== */
//                            final String potenFS1, potenFS2, potenFS3, potenFS4, potenFS5;
//                            final String ratingFS1, ratingFS2, ratingFS3, ratingFS4, ratingFS5;
//                            PlannedInspectionModel fireSafetyData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Fire Safety");
//                            if (fireSafetyData != null) {
//                                if (fireSafetyData.getRisk1() == null) {
//                                    potenFS1 = "";
//                                } else {
//                                    potenFS1 = fireSafetyData.getRisk1();
//                                }
//                                if (fireSafetyData.getRisk2() == null) {
//                                    potenFS2 = "";
//                                } else {
//                                    potenFS2 = fireSafetyData.getRisk2();
//                                }
//                                if (fireSafetyData.getRisk3() == null) {
//                                    potenFS3 = "";
//                                } else {
//                                    potenFS3 = fireSafetyData.getRisk3();
//                                }
//                                if (fireSafetyData.getRisk4() == null) {
//                                    potenFS4 = "";
//                                } else {
//                                    potenFS4 = fireSafetyData.getRisk4();
//                                }
//                                if (fireSafetyData.getRisk5() == null) {
//                                    potenFS5 = "";
//                                } else {
//                                    potenFS5 = fireSafetyData.getRisk5();
//                                }
//
//                                doc.getRange().replace("pr_32", potenFS1, true, true);
//                                doc.getRange().replace("pr_33", potenFS2, true, true);
//                                doc.getRange().replace("pr_34", potenFS3, true, true);
//                                doc.getRange().replace("pr_35", potenFS4, true, true);
//                                doc.getRange().replace("pr_36", potenFS5, true, true);
//
//                                if (fireSafetyData.getRating1() == null) {
//                                    ratingFS1 = "";
//                                } else {
//                                    ratingFS1 = fireSafetyData.getRating1();
//                                }
//                                if (fireSafetyData.getRating2() == null) {
//                                    ratingFS2 = "";
//                                } else {
//                                    ratingFS2 = fireSafetyData.getRating2();
//                                }
//                                if (fireSafetyData.getRating3() == null) {
//                                    ratingFS3 = "";
//                                } else {
//                                    ratingFS3 = fireSafetyData.getRating3();
//                                }
//                                if (fireSafetyData.getRating4() == null) {
//                                    ratingFS4 = "";
//                                } else {
//                                    ratingFS4 = fireSafetyData.getRating4();
//                                }
//                                if (fireSafetyData.getRating5() == null) {
//                                    ratingFS5 = "";
//                                } else {
//                                    ratingFS5 = fireSafetyData.getRating5();
//                                }
//
//                                if (ratingFS1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_32", passCheck, true, true);
//                                    doc.getRange().replace("g_32", "", true, true);
//                                    doc.getRange().replace("na32", "", true, true);
//                                } else if (ratingFS1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_32", "", true, true);
//                                    doc.getRange().replace("g_32", passCheck, true, true);
//                                    doc.getRange().replace("na32", "", true, true);
//                                } else if (ratingFS1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_32", "", true, true);
//                                    doc.getRange().replace("g_32", "", true, true);
//                                    doc.getRange().replace("na32", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_32", "", true, true);
//                                    doc.getRange().replace("g_32", "", true, true);
//                                    doc.getRange().replace("na32", "", true, true);
//                                }
//
//                                if (ratingFS2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_33", passCheck, true, true);
//                                    doc.getRange().replace("g_33", "", true, true);
//                                    doc.getRange().replace("na33", "", true, true);
//                                } else if (ratingFS2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_33", "", true, true);
//                                    doc.getRange().replace("g_33", passCheck, true, true);
//                                    doc.getRange().replace("na33", "", true, true);
//                                } else if (ratingFS2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_33", "", true, true);
//                                    doc.getRange().replace("g_33", "", true, true);
//                                    doc.getRange().replace("na33", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_33", "", true, true);
//                                    doc.getRange().replace("g_33", "", true, true);
//                                    doc.getRange().replace("na33", "", true, true);
//                                }
//
//                                if (ratingFS3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_34", passCheck, true, true);
//                                    doc.getRange().replace("g_34", "", true, true);
//                                    doc.getRange().replace("na34", "", true, true);
//                                } else if (ratingFS3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_34", "", true, true);
//                                    doc.getRange().replace("g_34", passCheck, true, true);
//                                    doc.getRange().replace("na34", "", true, true);
//                                } else if (ratingFS3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_34", "", true, true);
//                                    doc.getRange().replace("g_34", "", true, true);
//                                    doc.getRange().replace("na34", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_34", "", true, true);
//                                    doc.getRange().replace("g_34", "", true, true);
//                                    doc.getRange().replace("na34", "", true, true);
//                                }
//
//                                if (ratingFS4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_35", passCheck, true, true);
//                                    doc.getRange().replace("g_35", "", true, true);
//                                    doc.getRange().replace("na35", "", true, true);
//                                } else if (ratingFS4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_35", "", true, true);
//                                    doc.getRange().replace("g_35", passCheck, true, true);
//                                    doc.getRange().replace("na35", "", true, true);
//                                } else if (ratingFS4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_35", "", true, true);
//                                    doc.getRange().replace("g_35", "", true, true);
//                                    doc.getRange().replace("na35", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_35", "", true, true);
//                                    doc.getRange().replace("g_35", "", true, true);
//                                    doc.getRange().replace("na35", "", true, true);
//                                }
//
//                                if (ratingFS5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_36", passCheck, true, true);
//                                    doc.getRange().replace("g_36", "", true, true);
//                                    doc.getRange().replace("na36", "", true, true);
//                                } else if (ratingFS5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_36", "", true, true);
//                                    doc.getRange().replace("g_36", passCheck, true, true);
//                                    doc.getRange().replace("na36", "", true, true);
//                                } else if (ratingFS5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_36", "", true, true);
//                                    doc.getRange().replace("g_36", "", true, true);
//                                    doc.getRange().replace("na36", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_36", "", true, true);
//                                    doc.getRange().replace("g_36", "", true, true);
//                                    doc.getRange().replace("na36", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_32", "", true, true);
//                                doc.getRange().replace("pr_33", "", true, true);
//                                doc.getRange().replace("pr_34", "", true, true);
//                                doc.getRange().replace("pr_35", "", true, true);
//                                doc.getRange().replace("pr_36", "", true, true);
//
//                                doc.getRange().replace("nw_32", "", true, true);
//                                doc.getRange().replace("g_32", "", true, true);
//                                doc.getRange().replace("na32", "", true, true);
//
//                                doc.getRange().replace("nw_33", "", true, true);
//                                doc.getRange().replace("g_33", "", true, true);
//                                doc.getRange().replace("na33", "", true, true);
//
//                                doc.getRange().replace("nw_34", "", true, true);
//                                doc.getRange().replace("g_34", "", true, true);
//                                doc.getRange().replace("na34", "", true, true);
//
//                                doc.getRange().replace("nw_35", "", true, true);
//                                doc.getRange().replace("g_35", "", true, true);
//                                doc.getRange().replace("na35", "", true, true);
//
//                                doc.getRange().replace("nw_36", "", true, true);
//                                doc.getRange().replace("g_36", "", true, true);
//                                doc.getRange().replace("na36", "", true, true);
//                            }
//
//             /* ==================================
//                                     First Aid
//            ================================== */
//                            final String potenFA1, potenFA2, potenFA3;
//                            final String ratingFA1, ratingFA2, ratingFA3;
//                            PlannedInspectionModel firstAidData = ds.getLastDataInspection(inspector, areaOwner, location, date, "First Aid");
//                            if (firstAidData != null) {
//                                if (firstAidData.getRisk1() == null) {
//                                    potenFA1 = "";
//                                } else {
//                                    potenFA1 = firstAidData.getRisk1();
//                                }
//                                if (firstAidData.getRisk2() == null) {
//                                    potenFA2 = "";
//                                } else {
//                                    potenFA2 = firstAidData.getRisk2();
//                                }
//                                if (firstAidData.getRisk3() == null) {
//                                    potenFA3 = "";
//                                } else {
//                                    potenFA3 = firstAidData.getRisk3();
//                                }
//
//                                doc.getRange().replace("pr_37", potenFA1, true, true);
//                                doc.getRange().replace("pr_38", potenFA2, true, true);
//                                doc.getRange().replace("pr_39", potenFA3, true, true);
//
//                                if (firstAidData.getRating1() == null) {
//                                    ratingFA1 = "";
//                                } else {
//                                    ratingFA1 = firstAidData.getRating1();
//                                }
//                                if (firstAidData.getRating2() == null) {
//                                    ratingFA2 = "";
//                                } else {
//                                    ratingFA2 = firstAidData.getRating2();
//                                }
//                                if (firstAidData.getRating3() == null) {
//                                    ratingFA3 = "";
//                                } else {
//                                    ratingFA3 = firstAidData.getRating3();
//                                }
//
//                                if (ratingFA1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_37", passCheck, true, true);
//                                    doc.getRange().replace("g_37", "", true, true);
//                                    doc.getRange().replace("na37", "", true, true);
//                                } else if (ratingFA1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_37", "", true, true);
//                                    doc.getRange().replace("g_37", passCheck, true, true);
//                                    doc.getRange().replace("na37", "", true, true);
//                                } else if (ratingFA1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_37", "", true, true);
//                                    doc.getRange().replace("g_37", "", true, true);
//                                    doc.getRange().replace("na37", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_37", "", true, true);
//                                    doc.getRange().replace("g_37", "", true, true);
//                                    doc.getRange().replace("na37", "", true, true);
//                                }
//
//                                if (ratingFA2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_38", passCheck, true, true);
//                                    doc.getRange().replace("g_38", "", true, true);
//                                    doc.getRange().replace("na38", "", true, true);
//                                } else if (ratingFA2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_38", "", true, true);
//                                    doc.getRange().replace("g_38", passCheck, true, true);
//                                    doc.getRange().replace("na38", "", true, true);
//                                } else if (ratingFA2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_38", "", true, true);
//                                    doc.getRange().replace("g_38", "", true, true);
//                                    doc.getRange().replace("na38", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_38", "", true, true);
//                                    doc.getRange().replace("g_38", "", true, true);
//                                    doc.getRange().replace("na38", "", true, true);
//                                }
//
//                                if (ratingFA3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_39", passCheck, true, true);
//                                    doc.getRange().replace("g_39", "", true, true);
//                                    doc.getRange().replace("na39", "", true, true);
//                                } else if (ratingFA3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_39", "", true, true);
//                                    doc.getRange().replace("g_39", passCheck, true, true);
//                                    doc.getRange().replace("na39", "", true, true);
//                                } else if (ratingFA3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_39", "", true, true);
//                                    doc.getRange().replace("g_39", "", true, true);
//                                    doc.getRange().replace("na39", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_39", "", true, true);
//                                    doc.getRange().replace("g_39", "", true, true);
//                                    doc.getRange().replace("na39", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_37", "", true, true);
//                                doc.getRange().replace("pr_38", "", true, true);
//                                doc.getRange().replace("pr_39", "", true, true);
//
//                                doc.getRange().replace("nw_37", "", true, true);
//                                doc.getRange().replace("g_37", "", true, true);
//                                doc.getRange().replace("na37", "", true, true);
//
//                                doc.getRange().replace("nw_38", "", true, true);
//                                doc.getRange().replace("g_38", "", true, true);
//                                doc.getRange().replace("na38", "", true, true);
//
//                                doc.getRange().replace("nw_39", "", true, true);
//                                doc.getRange().replace("g_39", "", true, true);
//                                doc.getRange().replace("na39", "", true, true);
//                            }
//
//            /* ==================================
//                                    Enviromental
//            ================================== */
//                            final String potenEn1, potenEn2, potenEn3, potenEn4;
//                            final String ratingEn1, ratingEn2, ratingEn3, ratingEn4;
//                            PlannedInspectionModel enviroData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Environmental");
//                            if (enviroData != null) {
//                                if (enviroData.getRisk1() == null) {
//                                    potenEn1 = "";
//                                } else {
//                                    potenEn1 = enviroData.getRisk1();
//                                }
//                                if (enviroData.getRisk1() == null) {
//                                    potenEn2 = "";
//                                } else {
//                                    potenEn2 = enviroData.getRisk2();
//                                }
//                                if (enviroData.getRisk1() == null) {
//                                    potenEn3 = "";
//                                } else {
//                                    potenEn3 = enviroData.getRisk3();
//                                }
//                                if (enviroData.getRisk1() == null) {
//                                    potenEn4 = "";
//                                } else {
//                                    potenEn4 = enviroData.getRisk4();
//                                }
//
//                                doc.getRange().replace("pr_40", potenEn1, true, true);
//                                doc.getRange().replace("pr_41", potenEn2, true, true);
//                                doc.getRange().replace("pr_42", potenEn3, true, true);
//                                doc.getRange().replace("pr_43", potenEn4, true, true);
//
//                                if (enviroData.getRating1() == null) {
//                                    ratingEn1 = "";
//                                } else {
//                                    ratingEn1 = enviroData.getRating1();
//                                }
//                                if (enviroData.getRating2() == null) {
//                                    ratingEn2 = "";
//                                } else {
//                                    ratingEn2 = enviroData.getRating2();
//                                }
//                                if (enviroData.getRating3() == null) {
//                                    ratingEn3 = "";
//                                } else {
//                                    ratingEn3 = enviroData.getRating3();
//                                }
//                                if (enviroData.getRating4() == null) {
//                                    ratingEn4 = "";
//                                } else {
//                                    ratingEn4 = enviroData.getRating4();
//                                }
//
//                                if (ratingEn1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_40", passCheck, true, true);
//                                    doc.getRange().replace("g_40", "", true, true);
//                                    doc.getRange().replace("na40", "", true, true);
//                                } else if (ratingEn1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_40", "", true, true);
//                                    doc.getRange().replace("g_40", passCheck, true, true);
//                                    doc.getRange().replace("na40", "", true, true);
//                                } else if (ratingEn1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_40", "", true, true);
//                                    doc.getRange().replace("g_40", "", true, true);
//                                    doc.getRange().replace("na40", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_40", "", true, true);
//                                    doc.getRange().replace("g_40", "", true, true);
//                                    doc.getRange().replace("na40", "", true, true);
//                                }
//
//                                if (ratingEn2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_41", passCheck, true, true);
//                                    doc.getRange().replace("g_41", "", true, true);
//                                    doc.getRange().replace("na41", "", true, true);
//                                } else if (ratingEn2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_41", "", true, true);
//                                    doc.getRange().replace("g_41", passCheck, true, true);
//                                    doc.getRange().replace("na41", "", true, true);
//                                } else if (ratingEn2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_41", "", true, true);
//                                    doc.getRange().replace("g_41", "", true, true);
//                                    doc.getRange().replace("na41", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_41", "", true, true);
//                                    doc.getRange().replace("g_41", "", true, true);
//                                    doc.getRange().replace("na41", "", true, true);
//                                }
//
//                                if (ratingEn3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_42", passCheck, true, true);
//                                    doc.getRange().replace("g_42", "", true, true);
//                                    doc.getRange().replace("na42", "", true, true);
//                                } else if (ratingEn3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_42", "", true, true);
//                                    doc.getRange().replace("g_42", passCheck, true, true);
//                                    doc.getRange().replace("na42", "", true, true);
//                                } else if (ratingEn3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_42", "", true, true);
//                                    doc.getRange().replace("g_42", "", true, true);
//                                    doc.getRange().replace("na42", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_42", "", true, true);
//                                    doc.getRange().replace("g_42", "", true, true);
//                                    doc.getRange().replace("na42", "", true, true);
//                                }
//
//                                if (ratingEn4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_43", passCheck, true, true);
//                                    doc.getRange().replace("g_43", "", true, true);
//                                    doc.getRange().replace("na43", "", true, true);
//                                } else if (ratingEn4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_43", "", true, true);
//                                    doc.getRange().replace("g_43", passCheck, true, true);
//                                    doc.getRange().replace("na43", "", true, true);
//                                } else if (ratingEn4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_43", "", true, true);
//                                    doc.getRange().replace("g_43", "", true, true);
//                                    doc.getRange().replace("na43", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_43", "", true, true);
//                                    doc.getRange().replace("g_43", "", true, true);
//                                    doc.getRange().replace("na43", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_40", "", true, true);
//                                doc.getRange().replace("pr_41", "", true, true);
//                                doc.getRange().replace("pr_42", "", true, true);
//                                doc.getRange().replace("pr_43", "", true, true);
//
//                                doc.getRange().replace("nw_40", "", true, true);
//                                doc.getRange().replace("g_40", "", true, true);
//                                doc.getRange().replace("na40", "", true, true);
//
//                                doc.getRange().replace("nw_41", "", true, true);
//                                doc.getRange().replace("g_41", "", true, true);
//                                doc.getRange().replace("na41", "", true, true);
//
//                                doc.getRange().replace("nw_42", "", true, true);
//                                doc.getRange().replace("g_42", "", true, true);
//                                doc.getRange().replace("na42", "", true, true);
//
//                                doc.getRange().replace("nw_43", "", true, true);
//                                doc.getRange().replace("g_43", "", true, true);
//                                doc.getRange().replace("na43", "", true, true);
//                            }
//
//             /* ==================================
//                                   Communication
//            ================================== */
//                            final String potenCom1, potenCom2, potenCom3, potenCom4, potenCom5, potenCom6, potenCom7;
//                            final String ratingCom1, ratingCom2, ratingCom3, ratingCom4, ratingCom5, ratingCom6, ratingCom7;
//                            PlannedInspectionModel communicationData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Communication");
//                            if (communicationData != null) {
//                                if (communicationData.getRisk1() == null) {
//                                    potenCom1 = "";
//                                } else {
//                                    potenCom1 = communicationData.getRisk1();
//                                }
//                                if (communicationData.getRisk2() == null) {
//                                    potenCom2 = "";
//                                } else {
//                                    potenCom2 = communicationData.getRisk2();
//                                }
//                                if (communicationData.getRisk3() == null) {
//                                    potenCom3 = "";
//                                } else {
//                                    potenCom3 = communicationData.getRisk3();
//                                }
//                                if (communicationData.getRisk4() == null) {
//                                    potenCom4 = "";
//                                } else {
//                                    potenCom4 = communicationData.getRisk4();
//                                }
//                                if (communicationData.getRisk5() == null) {
//                                    potenCom5 = "";
//                                } else {
//                                    potenCom5 = communicationData.getRisk5();
//                                }
//                                if (communicationData.getRisk6() == null) {
//                                    potenCom6 = "";
//                                } else {
//                                    potenCom6 = communicationData.getRisk6();
//                                }
//                                if (communicationData.getRisk7() == null) {
//                                    potenCom7 = "";
//                                } else {
//                                    potenCom7 = communicationData.getRisk7();
//                                }
//
//                                doc.getRange().replace("pr_44", potenCom1, true, true);
//                                doc.getRange().replace("pr_45", potenCom2, true, true);
//                                doc.getRange().replace("pr_46", potenCom3, true, true);
//                                doc.getRange().replace("pr_47", potenCom4, true, true);
//                                doc.getRange().replace("pr_48", potenCom5, true, true);
//                                doc.getRange().replace("pr_49", potenCom6, true, true);
//                                doc.getRange().replace("pr_50", potenCom7, true, true);
//
//                                if (communicationData.getRating1() == null) {
//                                    ratingCom1 = "";
//                                } else {
//                                    ratingCom1 = communicationData.getRating1();
//                                }
//                                if (communicationData.getRating2() == null) {
//                                    ratingCom2 = "";
//                                } else {
//                                    ratingCom2 = communicationData.getRating2();
//                                }
//                                if (communicationData.getRating3() == null) {
//                                    ratingCom3 = "";
//                                } else {
//                                    ratingCom3 = communicationData.getRating3();
//                                }
//                                if (communicationData.getRating4() == null) {
//                                    ratingCom4 = "";
//                                } else {
//                                    ratingCom4 = communicationData.getRating4();
//                                }
//                                if (communicationData.getRating5() == null) {
//                                    ratingCom5 = "";
//                                } else {
//                                    ratingCom5 = communicationData.getRating5();
//                                }
//                                if (communicationData.getRating6() == null) {
//                                    ratingCom6 = "";
//                                } else {
//                                    ratingCom6 = communicationData.getRating6();
//                                }
//                                if (communicationData.getRating7() == null) {
//                                    ratingCom7 = "";
//                                } else {
//                                    ratingCom7 = communicationData.getRating7();
//                                }
//
//                                if (ratingCom1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_44", passCheck, true, true);
//                                    doc.getRange().replace("g_44", "", true, true);
//                                    doc.getRange().replace("na44", "", true, true);
//                                } else if (ratingCom1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_44", "", true, true);
//                                    doc.getRange().replace("g_44", passCheck, true, true);
//                                    doc.getRange().replace("na44", "", true, true);
//                                } else if (ratingCom1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_44", "", true, true);
//                                    doc.getRange().replace("g_44", "", true, true);
//                                    doc.getRange().replace("na44", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_44", "", true, true);
//                                    doc.getRange().replace("g_44", "", true, true);
//                                    doc.getRange().replace("na44", "", true, true);
//                                }
//
//                                if (ratingCom2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_45", passCheck, true, true);
//                                    doc.getRange().replace("g_45", "", true, true);
//                                    doc.getRange().replace("na45", "", true, true);
//                                } else if (ratingCom2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_45", "", true, true);
//                                    doc.getRange().replace("g_45", passCheck, true, true);
//                                    doc.getRange().replace("na45", "", true, true);
//                                } else if (ratingCom2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_45", "", true, true);
//                                    doc.getRange().replace("g_45", "", true, true);
//                                    doc.getRange().replace("na45", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_45", "", true, true);
//                                    doc.getRange().replace("g_45", "", true, true);
//                                    doc.getRange().replace("na45", "", true, true);
//                                }
//
//                                if (ratingCom3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_46", passCheck, true, true);
//                                    doc.getRange().replace("g_46", "", true, true);
//                                    doc.getRange().replace("na46", "", true, true);
//                                } else if (ratingCom3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_46", "", true, true);
//                                    doc.getRange().replace("g_46", passCheck, true, true);
//                                    doc.getRange().replace("na46", "", true, true);
//                                } else if (ratingCom3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_46", "", true, true);
//                                    doc.getRange().replace("g_46", "", true, true);
//                                    doc.getRange().replace("na46", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_46", "", true, true);
//                                    doc.getRange().replace("g_46", "", true, true);
//                                    doc.getRange().replace("na46", "", true, true);
//                                }
//
//                                if (ratingCom4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_47", passCheck, true, true);
//                                    doc.getRange().replace("g_47", "", true, true);
//                                    doc.getRange().replace("na47", "", true, true);
//                                } else if (ratingCom4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_47", "", true, true);
//                                    doc.getRange().replace("g_47", passCheck, true, true);
//                                    doc.getRange().replace("na47", "", true, true);
//                                } else if (ratingCom4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_47", "", true, true);
//                                    doc.getRange().replace("g_47", "", true, true);
//                                    doc.getRange().replace("na47", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_47", "", true, true);
//                                    doc.getRange().replace("g_47", "", true, true);
//                                    doc.getRange().replace("na47", "", true, true);
//                                }
//
//                                if (ratingCom5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_48", passCheck, true, true);
//                                    doc.getRange().replace("g_48", "", true, true);
//                                    doc.getRange().replace("na48", "", true, true);
//                                } else if (ratingCom5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_48", "", true, true);
//                                    doc.getRange().replace("g_48", passCheck, true, true);
//                                    doc.getRange().replace("na48", "", true, true);
//                                } else if (ratingCom5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_48", "", true, true);
//                                    doc.getRange().replace("g_48", "", true, true);
//                                    doc.getRange().replace("na48", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_48", "", true, true);
//                                    doc.getRange().replace("g_48", "", true, true);
//                                    doc.getRange().replace("na48", "", true, true);
//                                }
//
//                                if (ratingCom6.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_49", passCheck, true, true);
//                                    doc.getRange().replace("g_49", "", true, true);
//                                    doc.getRange().replace("na49", "", true, true);
//                                } else if (ratingCom6.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_49", "", true, true);
//                                    doc.getRange().replace("g_49", passCheck, true, true);
//                                    doc.getRange().replace("na49", "", true, true);
//                                } else if (ratingCom6.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_49", "", true, true);
//                                    doc.getRange().replace("g_49", "", true, true);
//                                    doc.getRange().replace("na49", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_49", "", true, true);
//                                    doc.getRange().replace("g_49", "", true, true);
//                                    doc.getRange().replace("na49", "", true, true);
//                                }
//
//                                if (ratingCom7.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_50", passCheck, true, true);
//                                    doc.getRange().replace("g_50", "", true, true);
//                                    doc.getRange().replace("na50", "", true, true);
//                                } else if (ratingCom7.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_50", "", true, true);
//                                    doc.getRange().replace("g_50", passCheck, true, true);
//                                    doc.getRange().replace("na50", "", true, true);
//                                } else if (ratingCom7.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_50", "", true, true);
//                                    doc.getRange().replace("g_50", "", true, true);
//                                    doc.getRange().replace("na50", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_50", "", true, true);
//                                    doc.getRange().replace("g_50", "", true, true);
//                                    doc.getRange().replace("na50", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_44", "", true, true);
//                                doc.getRange().replace("pr_45", "", true, true);
//                                doc.getRange().replace("pr_46", "", true, true);
//                                doc.getRange().replace("pr_47", "", true, true);
//                                doc.getRange().replace("pr_48", "", true, true);
//                                doc.getRange().replace("pr_49", "", true, true);
//                                doc.getRange().replace("pr_50", "", true, true);
//
//                                doc.getRange().replace("nw_44", "", true, true);
//                                doc.getRange().replace("g_44", "", true, true);
//                                doc.getRange().replace("na44", "", true, true);
//
//                                doc.getRange().replace("nw_45", "", true, true);
//                                doc.getRange().replace("g_45", "", true, true);
//                                doc.getRange().replace("na45", "", true, true);
//
//                                doc.getRange().replace("nw_46", "", true, true);
//                                doc.getRange().replace("g_46", "", true, true);
//                                doc.getRange().replace("na46", "", true, true);
//
//                                doc.getRange().replace("nw_47", "", true, true);
//                                doc.getRange().replace("g_47", "", true, true);
//                                doc.getRange().replace("na47", "", true, true);
//
//                                doc.getRange().replace("nw_48", "", true, true);
//                                doc.getRange().replace("g_48", "", true, true);
//                                doc.getRange().replace("na48", "", true, true);
//
//                                doc.getRange().replace("nw_49", "", true, true);
//                                doc.getRange().replace("g_49", "", true, true);
//                                doc.getRange().replace("na49", "", true, true);
//
//                                doc.getRange().replace("nw_50", "", true, true);
//                                doc.getRange().replace("g_50", "", true, true);
//                                doc.getRange().replace("na50", "", true, true);
//                            }
//
//            /* ==================================
//                                  Fall Protection
//            ================================== */
//                            final String potenFP1, potenFP2, potenFP3, potenFP4, potenFP5, potenFP6;
//                            final String ratingFP1, ratingFP2, ratingFP3, ratingFP4, ratingFP5, ratingFP6;
//                            PlannedInspectionModel fallProtectionData = ds.getLastDataInspection(inspector, areaOwner, location, date, "Fall Protection");
//                            if (fallProtectionData != null) {
//                                if (fallProtectionData.getRisk1() == null) {
//                                    potenFP1 = "";
//                                } else {
//                                    potenFP1 = fallProtectionData.getRisk1();
//                                }
//                                if (fallProtectionData.getRisk2() == null) {
//                                    potenFP2 = "";
//                                } else {
//                                    potenFP2 = fallProtectionData.getRisk2();
//                                }
//                                if (fallProtectionData.getRisk3() == null) {
//                                    potenFP3 = "";
//                                } else {
//                                    potenFP3 = fallProtectionData.getRisk3();
//                                }
//                                if (fallProtectionData.getRisk4() == null) {
//                                    potenFP4 = "";
//                                } else {
//                                    potenFP4 = fallProtectionData.getRisk4();
//                                }
//                                if (fallProtectionData.getRisk5() == null) {
//                                    potenFP5 = "";
//                                } else {
//                                    potenFP5 = fallProtectionData.getRisk5();
//                                }
//                                if (fallProtectionData.getRisk6() == null) {
//                                    potenFP6 = "";
//                                } else {
//                                    potenFP6 = fallProtectionData.getRisk6();
//                                }
//
//                                doc.getRange().replace("pr_51", potenFP1, true, true);
//                                doc.getRange().replace("pr_52", potenFP2, true, true);
//                                doc.getRange().replace("pr_53", potenFP3, true, true);
//                                doc.getRange().replace("pr_54", potenFP4, true, true);
//                                doc.getRange().replace("pr_55", potenFP5, true, true);
//                                doc.getRange().replace("pr_56", potenFP6, true, true);
//
//                                if (fallProtectionData.getRating1() == null) {
//                                    ratingFP1 = "";
//                                } else {
//                                    ratingFP1 = fallProtectionData.getRating1();
//                                }
//                                if (fallProtectionData.getRating2() == null) {
//                                    ratingFP2 = "";
//                                } else {
//                                    ratingFP2 = fallProtectionData.getRating2();
//                                }
//                                if (fallProtectionData.getRating3() == null) {
//                                    ratingFP3 = "";
//                                } else {
//                                    ratingFP3 = fallProtectionData.getRating3();
//                                }
//                                if (fallProtectionData.getRating4() == null) {
//                                    ratingFP4 = "";
//                                } else {
//                                    ratingFP4 = fallProtectionData.getRating4();
//                                }
//                                if (fallProtectionData.getRating5() == null) {
//                                    ratingFP5 = "";
//                                } else {
//                                    ratingFP5 = fallProtectionData.getRating5();
//                                }
//                                if (fallProtectionData.getRating6() == null) {
//                                    ratingFP6 = "";
//                                } else {
//                                    ratingFP6 = fallProtectionData.getRating6();
//                                }
//
//                                if (ratingFP1.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_51", passCheck, true, true);
//                                    doc.getRange().replace("g_51", "", true, true);
//                                    doc.getRange().replace("na51", "", true, true);
//                                } else if (ratingFP1.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_51", "", true, true);
//                                    doc.getRange().replace("g_51", passCheck, true, true);
//                                    doc.getRange().replace("na51", "", true, true);
//                                } else if (ratingFP1.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_51", "", true, true);
//                                    doc.getRange().replace("g_51", "", true, true);
//                                    doc.getRange().replace("na51", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_51", "", true, true);
//                                    doc.getRange().replace("g_51", "", true, true);
//                                    doc.getRange().replace("na51", "", true, true);
//                                }
//
//                                if (ratingFP2.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_52", passCheck, true, true);
//                                    doc.getRange().replace("g_52", "", true, true);
//                                    doc.getRange().replace("na52", "", true, true);
//                                } else if (ratingFP2.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_52", "", true, true);
//                                    doc.getRange().replace("g_52", passCheck, true, true);
//                                    doc.getRange().replace("na52", "", true, true);
//                                } else if (ratingFP2.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_52", "", true, true);
//                                    doc.getRange().replace("g_52", "", true, true);
//                                    doc.getRange().replace("na52", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_52", "", true, true);
//                                    doc.getRange().replace("g_52", "", true, true);
//                                    doc.getRange().replace("na52", "", true, true);
//                                }
//
//                                if (ratingFP3.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_53", passCheck, true, true);
//                                    doc.getRange().replace("g_53", "", true, true);
//                                    doc.getRange().replace("na53", "", true, true);
//                                } else if (ratingFP3.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_53", "", true, true);
//                                    doc.getRange().replace("g_53", passCheck, true, true);
//                                    doc.getRange().replace("na53", "", true, true);
//                                } else if (ratingFP3.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_53", "", true, true);
//                                    doc.getRange().replace("g_53", "", true, true);
//                                    doc.getRange().replace("na53", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_53", "", true, true);
//                                    doc.getRange().replace("g_53", "", true, true);
//                                    doc.getRange().replace("na53", "", true, true);
//                                }
//
//                                if (ratingFP4.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_54", passCheck, true, true);
//                                    doc.getRange().replace("g_54", "", true, true);
//                                    doc.getRange().replace("na54", "", true, true);
//                                } else if (ratingFP4.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_54", "", true, true);
//                                    doc.getRange().replace("g_54", passCheck, true, true);
//                                    doc.getRange().replace("na54", "", true, true);
//                                } else if (ratingFP4.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_54", "", true, true);
//                                    doc.getRange().replace("g_54", "", true, true);
//                                    doc.getRange().replace("na54", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_54", "", true, true);
//                                    doc.getRange().replace("g_54", "", true, true);
//                                    doc.getRange().replace("na54", "", true, true);
//                                }
//
//                                if (ratingFP5.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_55", passCheck, true, true);
//                                    doc.getRange().replace("g_55", "", true, true);
//                                    doc.getRange().replace("na55", "", true, true);
//                                } else if (ratingFP5.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_55", "", true, true);
//                                    doc.getRange().replace("g_55", passCheck, true, true);
//                                    doc.getRange().replace("na55", "", true, true);
//                                } else if (ratingFP5.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_55", "", true, true);
//                                    doc.getRange().replace("g_55", "", true, true);
//                                    doc.getRange().replace("na55", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_55", "", true, true);
//                                    doc.getRange().replace("g_55", "", true, true);
//                                    doc.getRange().replace("na55", "", true, true);
//                                }
//
//                                if (ratingFP6.equalsIgnoreCase("NW")) {
//                                    doc.getRange().replace("nw_56", passCheck, true, true);
//                                    doc.getRange().replace("g_56", "", true, true);
//                                    doc.getRange().replace("na56", "", true, true);
//                                } else if (ratingFP6.equalsIgnoreCase("Good")) {
//                                    doc.getRange().replace("nw_56", "", true, true);
//                                    doc.getRange().replace("g_56", passCheck, true, true);
//                                    doc.getRange().replace("na56", "", true, true);
//                                } else if (ratingFP6.equalsIgnoreCase("N/A")) {
//                                    doc.getRange().replace("nw_56", "", true, true);
//                                    doc.getRange().replace("g_56", "", true, true);
//                                    doc.getRange().replace("na56", passCheck, true, true);
//                                } else {
//                                    doc.getRange().replace("nw_56", "", true, true);
//                                    doc.getRange().replace("g_56", "", true, true);
//                                    doc.getRange().replace("na56", "", true, true);
//                                }
//                            } else {
//                                doc.getRange().replace("pr_51", "", true, true);
//                                doc.getRange().replace("pr_52", "", true, true);
//                                doc.getRange().replace("pr_53", "", true, true);
//                                doc.getRange().replace("pr_54", "", true, true);
//                                doc.getRange().replace("pr_55", "", true, true);
//                                doc.getRange().replace("pr_56", "", true, true);
//
//                                doc.getRange().replace("nw_51", "", true, true);
//                                doc.getRange().replace("g_51", "", true, true);
//                                doc.getRange().replace("na51", "", true, true);
//
//                                doc.getRange().replace("nw_52", "", true, true);
//                                doc.getRange().replace("g_52", "", true, true);
//                                doc.getRange().replace("na52", "", true, true);
//
//                                doc.getRange().replace("nw_53", "", true, true);
//                                doc.getRange().replace("g_53", "", true, true);
//                                doc.getRange().replace("na53", "", true, true);
//
//                                doc.getRange().replace("nw_54", "", true, true);
//                                doc.getRange().replace("g_54", "", true, true);
//                                doc.getRange().replace("na54", "", true, true);
//
//                                doc.getRange().replace("nw_55", "", true, true);
//                                doc.getRange().replace("g_55", "", true, true);
//                                doc.getRange().replace("na55", "", true, true);
//
//                                doc.getRange().replace("nw_5", "", true, true);
//                                doc.getRange().replace("g_56", "", true, true);
//                                doc.getRange().replace("na56", "", true, true);
//
//                                doc.getRange().replace("nw_57", "", true, true);
//                                doc.getRange().replace("g_57", "", true, true);
//                                doc.getRange().replace("na57", "", true, true);
//                            }
//
//                            Table sampleIdTable = (Table) doc.getChild(NodeType.TABLE, 2, true);
//
//                            ArrayList<PlannedFindingsModel> findingsData = ds.getAllPlannedFindings(areaOwner, date);
//                            Row templateRow = (Row) sampleIdTable.getLastRow();
//                            int count = 0;
//
//                            int photoDummyIndex = 1;
//                            Row dataRow;
//                            Row clonedRow;
//                            RowCollection photoTableRows = sampleIdTable.getRows();
//                            clonedRow = (Row) photoTableRows.get(photoDummyIndex);
//
//                            String categoryQuestion = "";
//
//                            int counts = 0;
//                            for (int i = 0; i < findingsData.size(); i++) {
//                                counts++;
//                                dataRow = (Row) clonedRow.deepClone(true);
//                                sampleIdTable.getRows().add(dataRow);
//
//                                clonedRow.getRowFormat().setAllowBreakAcrossPages(true);
////                    View v = dataFindings.getChildAt(i);
//
//                                // Items #
//                                Cell cell = dataRow.getCells().get(0);
////                for (Run run : cell.getFirstParagraph().getRuns()) {
////                    // Set some font formatting properties
////                    Font font = run.getFont();
////                    font.setColor(Color.BLACK);
////                }
////                cell.getFirstParagraph().getRuns().clear();
////                cell.getCellFormat().getShading()
////                        .setBackgroundPatternColor(Color.WHITE);
////                cell.getFirstParagraph().appendChild(
////                        new Run(doc, valueOf(counts)));
//
//                                if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Work Area & Housekeeping - Building, Floors, Mining)";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Work Area & Housekeeping - Stacking & Storage)";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Work Area & Housekeeping - Work Set Up (Ergonomics))";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Work Area & Housekeeping - Walkways)";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Work Area & Housekeeping - General Cleanliness)";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 6) {
//                                    categoryQuestion = "(Work Area & Housekeeping - Ground Support Conditions)";
//                                } else if (findingsData.get(i).getCode() == 1 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Work Area & Housekeeping)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Structures & Floors - Coloring & Demarcation)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Structures & Floors - Windows & Screens)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Structures & Floors - Drainage)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Structures & Floors - Water Lines & Valves)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Structures & Floors - Sanitary Facilities)";
//                                } else if (findingsData.get(i).getCode() == 2 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Structures & Floors)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Machinary Tools - Guard in place)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Machinary Tools - Serviceable Guard)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Machinary Tools - Pinch Point)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Machinary Tools - Correct for the job)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Machinary Tools - Good Condition)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 6) {
//                                    categoryQuestion = "(Machinary Tools - Pre-op Checklist)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 7) {
//                                    categoryQuestion = "(Machinary Tools - Licenses & Permits)";
//                                } else if (findingsData.get(i).getCode() == 3 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Machinary Tools)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Electrical - Power & Extention Cables)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Electrical - Panel, Switches, Plug & Outles)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Electrical - Power & Equipment Cables)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Electrical - Labeling)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Electrical - Color Coding & Demarcation)";
//                                } else if (findingsData.get(i).getCode() == 4 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Electrical)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Personel - Correct PPE in use)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Personel - Work Activity)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Personel - Supervisor Dialy Check)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Personel - Special Safety Instructions)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Personel - Licenses & Permits)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 6) {
//                                    categoryQuestion = "(Personel - Correct Lifting & Handling)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 7) {
//                                    categoryQuestion = "(Personel - JSA / SOP Available)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 8) {
//                                    categoryQuestion = "(Personel - Gas Detector)";
//                                } else if (findingsData.get(i).getCode() == 5 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Personel)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Fire Safety - Nearest Fire Extinguisher)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Fire Safety - Color Coding & Demarcation)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Fire Safety - Sufficient Number & Types)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Fire Safety - Visible & Accessible)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Fire Safety - ERG Inspection Tag)";
//                                } else if (findingsData.get(i).getCode() == 6 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Fire Safety)";
//                                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(First Aid - First Aid Kit)";
//                                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(First Aid - Eyewash Station)";
//                                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(First Aid - Stretcher)";
//                                } else if (findingsData.get(i).getCode() == 7 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(First Aid)";
//                                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Environmental - Lighting)";
//                                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Environmental - Ventilation)";
//                                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Environmental - Noise)";
//                                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Environmental - Hazard Materials)";
//                                } else if (findingsData.get(i).getCode() == 8 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Environmental)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Communication - Telephone / Radio)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Communication - Emergency Procedure)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Communication - Personal Protective Equipment)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Communication - No Smoke)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Communication - JSA / SOP Posted)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 6) {
//                                    categoryQuestion = "(Communication - Emergency Exit)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 7) {
//                                    categoryQuestion = "(Communication - Other)";
//                                } else if (findingsData.get(i).getCode() == 9 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Communication";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 1) {
//                                    categoryQuestion = "(Fall Protection - Ladders)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 2) {
//                                    categoryQuestion = "(Fall Protection - Scaffolds)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 3) {
//                                    categoryQuestion = "(Fall Protection - Stairways & Ramps)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 4) {
//                                    categoryQuestion = "(Fall Protection - Lanyards)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 5) {
//                                    categoryQuestion = "(Fall Protection - 4 Point Harness / Safety Belt)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 6) {
//                                    categoryQuestion = "(Fall Protection - Safety Blocks)";
//                                } else if (findingsData.get(i).getCode() == 10 && findingsData.get(i).getQuestionCode() == 0) {
//                                    categoryQuestion = "(Fall Protection)";
//                                } else if (findingsData.get(i).getCode() == 0) {
//                                    categoryQuestion = "";
//                                }
//
//                                // Findings
//                                Cell cellFindngs = dataRow.getCells().get(0);
//                                for (Run run : cellFindngs.getFirstParagraph().getRuns()) {
//                                    // Set some font formatting properties
//                                    Font font = run.getFont();
//                                    font.setColor(Color.BLACK);
//                                }
//                                cellFindngs.getFirstParagraph().getRuns().clear();
//                                cellFindngs.getCellFormat().getShading()
//                                        .setBackgroundPatternColor(Color.WHITE);
//                                cellFindngs.getFirstParagraph().appendChild(
//                                        new Run(doc, valueOf(findingsData.get(i).getFindings()) + " " + categoryQuestion));
//
//                                // Comments
//                                Cell cellComment = dataRow.getCells().get(1);
//                                for (Run run : cellComment.getFirstParagraph().getRuns()) {
//                                    // Set some font formatting properties
//                                    Font font = run.getFont();
//                                    font.setColor(Color.BLACK);
//                                }
//                                cellComment.getFirstParagraph().getRuns().clear();
//                                cellComment.getCellFormat().getShading()
//                                        .setBackgroundPatternColor(Color.WHITE);
//                                cellComment.getFirstParagraph().appendChild(
//                                        new Run(doc, valueOf(findingsData.get(i).getComment())));
//
//                                // Photo
//                                cell = dataRow.getCells().get(2);
//                                cell.getFirstParagraph().getRuns().clear();
//                                if (!findingsData.get(i).getPhotoPath()
//                                        .equalsIgnoreCase("")) {
//                                    DocumentBuilder builders = new DocumentBuilder(doc);
//                                    CellFormat format = cell.getCellFormat();
//                                    double width = format.getWidth();
//                                    double height = cell.getParentRow().getRowFormat()
//                                            .getHeight();
//                                    builder.moveTo(cell.getFirstChild());
//
//                                    Bitmap imgBitmap = null;
//                                    if (isGenerate) {
//                                        imgBitmap = BitmapFactory.decodeFile(findingsData.get(i).getPhotoPath());
//                                    } else {
//                                        try {
//                                            ExifInterface ei;
//                                            ei = new ExifInterface(findingsData.get(i).getPhotoPath());
//                                            int orientation = ei.getAttributeInt(
//                                                    ExifInterface.TAG_ORIENTATION,
//                                                    ExifInterface.ORIENTATION_NORMAL);
//
//                                            switch (orientation) {
//                                                case ExifInterface.ORIENTATION_ROTATE_90:
//                                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 90,
//                                                            150);
//                                                    break;
//                                                case ExifInterface.ORIENTATION_ROTATE_180:
//                                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 180,
//                                                            150);
//                                                    break;
//                                                default:
//                                                    imgBitmap = Helper.rotateImage(findingsData.get(i).getPhotoPath(), 0,
//                                                            150);
//                                                    break;
//                                            }
//                                        } catch (IOException e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//
//                                    // Shape image = builder.insertImage(imgBitmap);
//                                    ByteArrayOutputStream tempImage = new ByteArrayOutputStream();
//                                    imgBitmap.compress(Bitmap.CompressFormat.JPEG,
//                                            Constants.COMPRESS_PERCENTAGE, tempImage);
//                                    Bitmap compressedBitmap = BitmapFactory
//                                            .decodeStream(new ByteArrayInputStream(
//                                                    tempImage.toByteArray()));
//                                    Shape image = builder.insertImage(compressedBitmap);
//
//                                    double freePageWidth = width;
//                                    double freePageHeight = height;
//
//                                    // Is one of the sides of this image too big for the
//                                    // page?
//                                    ImageSize size = image.getImageData().getImageSize();
//                                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth
//                                            || size.getHeightPoints() > freePageHeight;
//
//                                    if (exceedsMaxPageSize) {
//                                        // Calculate the ratio to fit the page size
//                                        double ratio = freePageWidth
//                                                / size.getWidthPoints();
//
//                                        // Set the new size.
//                                        image.setWidth(size.getWidthPoints() * ratio);
//                                        image.setHeight(size.getHeightPoints() * ratio);
//                                    }
//
//                                    if (isGenerate) {
////                                        if (!imageFolder.exists()) {
////                                            imageFolder.mkdir();
////                                        }
////
////                                        // MOVE IMAGE TO REPORT FOLDER
////                                        File old = new File(findingsData.get(i).getPhotoPath());
////                                        String name = old.getName();
//////
////                                        InputStream in = new FileInputStream(findingsData.get(i).getPhotoPath());
////                                        OutputStream out = new FileOutputStream(
////                                                imageFolderPath + "/" + name);
////                                        Helper.copyFile(in, out);
////                                        old.delete();
////
////                                        PlannedFindingsModel fm = findingsData.get(i);
////                                        fm.setPhotoPath(imageFolderPath + "/" + name);
////                                        findingsData.set(i, fm);
//                                    } else {
//                                        for (Run run : cell.getFirstParagraph().getRuns()) {
////                            // Set some font formatting properties
//                                            Font font = run.getFont();
//                                            font.setColor(Color.BLACK);
//                                        }
//                                        cell.getCellFormat().getShading()
//                                                .setBackgroundPatternColor(Color.WHITE);
//                                        cell.getFirstParagraph().appendChild(new Run(doc, ""));
//                                    }
//                                }
//
//                                // Potential Risk
//                                Cell cellRisk = dataRow.getCells().get(3);
//                                for (Run run : cellRisk.getFirstParagraph().getRuns()) {
//                                    // Set some font formatting properties
//                                    Font font = run.getFont();
//                                    font.setColor(Color.BLACK);
//                                }
//                                cellRisk.getFirstParagraph().getRuns().clear();
//                                cellRisk.getCellFormat().getShading()
//                                        .setBackgroundPatternColor(Color.WHITE);
//                                cellRisk.getFirstParagraph().appendChild(
//                                        new Run(doc, valueOf(findingsData.get(i).getRisk())));
//
//                                // Potential Risk
//                                Cell cellRP = dataRow.getCells().get(4);
//                                for (Run run : cellRP.getFirstParagraph().getRuns()) {
//                                    // Set some font formatting properties
//                                    Font font = run.getFont();
//                                    font.setColor(Color.BLACK);
//                                }
//                                cellRP.getFirstParagraph().getRuns().clear();
//                                cellRP.getCellFormat().getShading()
//                                        .setBackgroundPatternColor(Color.WHITE);
//                                cellRP.getFirstParagraph().appendChild(
//                                        new Run(doc, valueOf(findingsData.get(i).getResponsible())));
//
//                                // Date Complete
//                                Cell cellDate = dataRow.getCells().get(5);
//                                for (Run run : cellDate.getFirstParagraph().getRuns()) {
//                                    // Set some font formatting properties
//                                    Font font = run.getFont();
//                                    font.setColor(Color.BLACK);
//                                }
//                                cellDate.getFirstParagraph().getRuns().clear();
//                                cellDate.getCellFormat().getShading()
//                                        .setBackgroundPatternColor(Color.WHITE);
//                                cellDate.getFirstParagraph().appendChild(
//                                        new Run(doc, valueOf(findingsData.get(i).getDate())));
//
////                sampleIdTable.appendChild(clonedRow);
//
//                            }
//
//                            Row dummyRow = photoTableRows.get(1);
//                            dummyRow.remove();
//
//                            String supervisorID, supervisorDate, managerID, managerDate, safetyID, safetyDate;
//                            Table signTable = (Table) doc.getChild(NodeType.TABLE, 3, true);
//
//                            PlannedSignModel supervisorData = ds.getAllPlannedSign(inspector, areaOwner, location, date, "Supervisor");
//                            if (supervisorData != null) {
//                                if (supervisorData.getIdNo() != null) {
//                                    supervisorID = supervisorData.getIdNo();
//                                } else {
//                                    supervisorID = "";
//                                }
//                                doc.getRange().replace("employee_id_1", supervisorID, true, true);
//
//                                if (supervisorData.getDate() != null) {
//                                    supervisorDate = supervisorData.getDate();
//                                } else {
//                                    supervisorDate = "";
//                                }
//                                doc.getRange().replace("date_id_1", supervisorDate, true, true);
//
//
//                                if (supervisorData.getSign() != null) {
//                                    Row row4 = (Row) signTable.getRows().get(1);
//                                    Cell dataCell4 = row4.getCells().get(1);
//                                    CellFormat format = dataCell4.getCellFormat();
//                                    double width = format.getWidth();
//                                    builder.moveTo(dataCell4.getFirstParagraph());
//                                    Shape image = builder.insertImage(supervisorData.getSign());
//
//                                    double freePageWidth = width;
//
//                                    // Is one of the sides of this image too big for the
//                                    // page?
//                                    ImageSize size = image.getImageData()
//                                            .getImageSize();
//                                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                                    if (exceedsMaxPageSize) {
//                                        // Calculate the ratio to fit the page size
//                                        double ratio = freePageWidth
//                                                / size.getWidthPoints();
//
//                                        Log.d("IMAGE", "widthlonger : " + ""
//                                                + " | ratio : " + ratio);
//
//                                        // Set the new size.
//                                        image.setWidth(size.getWidthPoints() * ratio);
//                                        image.setHeight(size.getHeightPoints() * ratio);
//                                    }
//                                }
//
//                            } else {
//                                doc.getRange().replace("employee_id_1", "", true, true);
//                                doc.getRange().replace("date_id_1", "", true, true);
//                            }
//
//                            PlannedSignModel managerData = ds.getAllPlannedSign(inspector, areaOwner, location, date, "Manager");
//                            if (managerData != null) {
//                                if (managerData.getIdNo() == null) {
//                                    managerID = "";
//                                } else {
//                                    managerID = managerData.getIdNo();
//                                }
//                                doc.getRange().replace("employee_id_2", managerID, true, true);
//
//                                if (managerData.getIdNo() == null) {
//                                    managerDate = "";
//                                } else {
//                                    managerDate = managerData.getDate();
//                                }
//
//                                doc.getRange().replace("date_id_2", managerDate, true, true);
//
//                                if (managerData.getSign() != null) {
//                                    Row row4 = (Row) signTable.getRows().get(2);
//                                    Cell dataCell4 = row4.getCells().get(1);
//                                    CellFormat format = dataCell4.getCellFormat();
//                                    double width = format.getWidth();
//                                    builder.moveTo(dataCell4.getFirstParagraph());
//                                    Shape image = builder.insertImage(managerData.getSign());
//
//                                    double freePageWidth = width;
//
//                                    // Is one of the sides of this image too big for the
//                                    // page?
//                                    ImageSize size = image.getImageData()
//                                            .getImageSize();
//                                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                                    if (exceedsMaxPageSize) {
//                                        // Calculate the ratio to fit the page size
//                                        double ratio = freePageWidth
//                                                / size.getWidthPoints();
//
//                                        Log.d("IMAGE", "widthlonger : " + ""
//                                                + " | ratio : " + ratio);
//
//                                        // Set the new size.
//                                        image.setWidth(size.getWidthPoints() * ratio);
//                                        image.setHeight(size.getHeightPoints() * ratio);
//                                    }
//                                }
//
//                            } else {
//                                doc.getRange().replace("employee_id_2", "", true, true);
//                                doc.getRange().replace("date_id_2", "", true, true);
//                            }
//
//                            PlannedSignModel safetyData = ds.getAllPlannedSign(inspector, areaOwner, location, date, "Safety");
//                            if (safetyData != null) {
//                                if (safetyData.getIdNo() == null) {
//                                    safetyID = "";
//                                } else {
//                                    safetyID = safetyData.getIdNo();
//                                }
//                                doc.getRange().replace("employee_id_3", safetyID, true, true);
//
//                                if (safetyData.getIdNo() == null) {
//                                    safetyDate = "";
//                                } else {
//                                    safetyDate = safetyData.getDate();
//                                }
//                                doc.getRange().replace("date_id_3", safetyDate, true, true);
//
//                                if (safetyData.getSign() != null) {
//                                    Row row4 = (Row) signTable.getRows().get(3);
//                                    Cell dataCell4 = row4.getCells().get(1);
//                                    CellFormat format = dataCell4.getCellFormat();
//                                    double width = format.getWidth();
//                                    builder.moveTo(dataCell4.getFirstParagraph());
//                                    Shape image = builder.insertImage(safetyData.getSign());
//
//                                    double freePageWidth = width;
//
//                                    // Is one of the sides of this image too big for the
//                                    // page?
//                                    ImageSize size = image.getImageData()
//                                            .getImageSize();
//                                    boolean exceedsMaxPageSize = size.getWidthPoints() > freePageWidth;
//
//                                    if (exceedsMaxPageSize) {
//                                        // Calculate the ratio to fit the page size
//                                        double ratio = freePageWidth
//                                                / size.getWidthPoints();
//
//                                        Log.d("IMAGE", "widthlonger : " + ""
//                                                + " | ratio : " + ratio);
//
//                                        // Set the new size.
//                                        image.setWidth(size.getWidthPoints() * ratio);
//                                        image.setHeight(size.getHeightPoints() * ratio);
//                                    }
//                                }
//                            } else {
//                                doc.getRange().replace("employee_id_3", "", true, true);
//                                doc.getRange().replace("date_id_3", "", true, true);
//                            }
//
//                            if (isGenerate) {
//                                doc.save(fullPath + ".doc");
//                                folderDirectory = fullPath;
//                                fullPath += "/" + fileName;
//                                Helper.showPopUpMessage(mActivity, "Success",
//                                        "Report has been successfully generated in "
//                                                + folderDirectory, null);
//
//                                String previewFolder = Helper.getPathForPreviewFile();
//                                String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
//                                        + Constants.FILE_TYPE_PDF;
//                                Helper.checkAndCreateDirectory(previewFolder);
//                                previewFilePath = previewFolder + "/" + fileNamePdf;
//                                doc.save(previewFilePath);
//
//                                File file = new File(previewFilePath);
//                                Intent intent = new Intent();
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.setAction(Intent.ACTION_VIEW);
//                                String type = "application/pdf";
//                                intent.setDataAndType(Uri.fromFile(file), type);
//                                mActivity.startActivityForResult(intent,
//                                        PREVIEW_CODE_SHE_PLANNED);
//                                Helper.progressDialog.dismiss();
//                                isPreview = true;
//                            } else {
//                                String previewFolder = Helper.getPathForPreviewFile();
//                                String fileNamePdf = Constants.SHE_PLANNED_PREVIEW_FILE_NAME
//                                        + Constants.FILE_TYPE_PDF;
//                                Helper.checkAndCreateDirectory(previewFolder);
//                                previewFilePath = previewFolder + "/" + fileNamePdf;
//                                doc.save(previewFilePath);
//
//                                File file = new File(previewFilePath);
//                                Intent intent = new Intent();
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.setAction(Intent.ACTION_VIEW);
//                                String type = "application/pdf";
//                                intent.setDataAndType(Uri.fromFile(file), type);
//                                mActivity.startActivityForResult(intent,
//                                        PREVIEW_CODE_SHE_PLANNED);
//                                Helper.progressDialog.dismiss();
//                                isPreview = true;
//                            }
//
////            if (isGenerate) {
////
////                return Constants.REPORT_GENERATED;
////            } else {
////
////                return Constants.REPORT_PREVIEW;
////            }
//
//                        } catch (Exception e) {
//
//                        }
//                    }
//                }, 500);
//            }

}
