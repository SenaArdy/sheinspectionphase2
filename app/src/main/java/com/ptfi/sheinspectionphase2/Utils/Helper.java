package com.ptfi.sheinspectionphase2.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Adapter.ListChangeLogAdapter;
import com.ptfi.sheinspectionphase2.Database.DataSource;
import com.ptfi.sheinspectionphase2.Fragments.DatePickerFragment;
import com.ptfi.sheinspectionphase2.Models.CustomSpinnerItem;
import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class Helper {

    public static Uri fileUri;

    public enum visiblePage {
        lv, planned, field, ug, grs, data_management, lvGrouping
    }

//    Switch (visiblePage){
//        case planned:
//        //koding
//        break;

    //    }
    public static visiblePage visiblePage;

    public enum Lookup {
        newData, newDataLookUp, findings
    }

    public static Lookup lookUp;

    public static ProgressDialog progressDialog;

    static String extStorageDirectory = Environment
            .getExternalStorageDirectory().getAbsolutePath();

    public static String basepath = extStorageDirectory + "/"
            + Constants.ROOT_FOLDER_NAME + "/";

    class CustomSpinnerViewHolder {
        TextView lookupTV;
        TextView descTV;
        LinearLayout lookupLayout;
        LinearLayout descLayout;
        ImageView lookupIV;
    }

    class CustomSpinnerSimpleHolder {
        TextView text;
        LinearLayout layout;
    }

    public class CustomSpinnerAdapter extends ArrayAdapter<CustomSpinnerItem> {
        private Activity context;
        ArrayList<CustomSpinnerItem> myItems;
        private LayoutInflater mInflater;

        public CustomSpinnerAdapter(Activity context, int textViewResourceId,
                                    ArrayList<CustomSpinnerItem> myItems) {
            super(context, textViewResourceId, myItems);
            this.context = context;
            this.myItems = myItems;
            mInflater = this.context.getLayoutInflater();
        }

        public void addItem(CustomSpinnerItem listitem) {
            myItems.add(listitem);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return myItems.size();
        }

        @Override
        public CustomSpinnerItem getItem(int position) {
            return myItems.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final CustomSpinnerSimpleHolder holder;
            if (convertView == null) {
                holder = new CustomSpinnerSimpleHolder();
                convertView = mInflater.inflate(
                        R.layout.custom_simple_listview_item, null);
                holder.text = (TextView) convertView.findViewById(R.id.text);
                holder.layout = (LinearLayout) convertView
                        .findViewById(R.id.layout);

                convertView.setTag(holder);
            } else {
                holder = (CustomSpinnerSimpleHolder) convertView.getTag();
            }

            // Fill TextView with the value you have in data source
            holder.text.setText(myItems.get(position).description);

            return convertView;
        }

        @Override
        public View getDropDownView(final int position, View convertView,
                                    ViewGroup parent) {
            final CustomSpinnerViewHolder holder;
            if (convertView == null) {
                holder = new CustomSpinnerViewHolder();
                convertView = mInflater.inflate(R.layout.custom_listview_item,
                        null);
                holder.lookupTV = (TextView) convertView
                        .findViewById(R.id.lookupTV);
                holder.descTV = (TextView) convertView
                        .findViewById(R.id.descTV);
                holder.lookupLayout = (LinearLayout) convertView
                        .findViewById(R.id.lookupLayout);
                holder.descLayout = (LinearLayout) convertView
                        .findViewById(R.id.descLayout);

                convertView.setTag(holder);
            } else {
                holder = (CustomSpinnerViewHolder) convertView.getTag();
            }

            // Fill TextView with the value you have in data source
            holder.lookupTV.setText(myItems.get(position).lookup);
            holder.descTV.setText(myItems.get(position).description);

            if (myItems.get(position).lookup.equalsIgnoreCase("Value")) {
                holder.lookupLayout
                        .setBackgroundResource(R.drawable.bordered_background_black);
                holder.lookupTV.setTextColor(Color.WHITE);
                holder.descLayout
                        .setBackgroundResource(R.drawable.bordered_background_black);
                holder.descTV.setTextColor(Color.WHITE);
            } else {
                holder.lookupLayout
                        .setBackgroundResource(R.drawable.bordered_background_white);
                holder.lookupTV.setTextColor(Color.BLACK);
                holder.descLayout
                        .setBackgroundResource(R.drawable.bordered_background_white);
                holder.descTV.setTextColor(Color.BLACK);
            }
            return convertView;
        }
    }

    /**
     * Copy file from asset folder to device
     *
     * @param pathFolder
     * @param pathAsset
     * @param activity
     * @param fileName
     */
    public static void copyFileFromAssets(String pathFolder, String pathAsset,
                                          Activity activity, String fileName) {
        AssetManager assetManager = activity.getResources().getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(pathAsset + "/" + fileName);
            out = new FileOutputStream(pathFolder + "/" + fileName);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("copy clipart ERROR", e.toString());
            // e.printStackTrace();
        }
    }

    public static boolean isExistPath(String preffix) {
        File scdbFolder = new File(
                Constants.EXTERNAL_ROOT_FOLDER_SCDB);

        if (scdbFolder.exists() && scdbFolder.isDirectory()) {
            ArrayList<File> inFiles = new ArrayList<File>();
            File[] files = scdbFolder.listFiles();
            for (File file : files) {
                if (!file.isDirectory() && file.getName().endsWith(".csv") && file.getName().contains(preffix)) {
                    return true;
                }
            }
            return false;
        } else return false;
    }

    /* =======================
                            Date and Time
    ======================= */
    public static String formatNumber(int number) {
        if (number < 10) {
            return "0" + String.valueOf(number);
        } else {
            return String.valueOf(number);
        }
    }

    public static String numToMonthName(int num) {
        switch (num) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "January";
        }
    }

    public static void showDatePicker(View v, FragmentActivity context,
                                      DatePickerDialog.OnDateSetListener callBack, int year, int month,
                                      int day) {
        DialogFragment newFragment = new DatePickerFragment(context, callBack,
                year, month, day);
        newFragment.show(context.getSupportFragmentManager(), "datePicker");
    }

    /* =======================
                     Manipulate Folder and Files
    ======================= */
    public static void prepareTemplateDirectory(Activity context) {
        // delete all templates file on template folder
        deleteAllFilesOnDirectory(Constants.extStorageDirectory + "/"
                + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.TEMPLATE_FOLDER_NAME);
        // copy back all templates on asset folder to template folder
        copyFolderAndContent(Constants.extStorageDirectory + "/"
                        + Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME,
                Constants.ROOT_FOLDER_NAME + "/"
                        + Constants.APP_FOLDER_NAME + "/"
                        + Constants.TEMPLATE_FOLDER_NAME, context);
    }

    public static String getPathForPreviewFile() {
        String path = Constants.extStorageDirectory + "/"
                + Constants.ROOT_FOLDER_NAME + "/"
                + Constants.APP_FOLDER_NAME + "/"
                + Constants.EXPORT_FOLDER_NAME + "/"
                + Constants.PREVIEW_FOLDER_NAME;

        new File(path).mkdirs();

        return path;
    }

    public static void deleteAllFilesOnDirectory(String directoryPath) {
        if (directoryPath != null) {
            if (directoryPath.length() > 0) {
                File dir = new File(directoryPath);
                if (dir.exists() && dir.isDirectory()) {
                    String[] files = dir.list();
                    if (files.length > 0) {
                        for (int i = 0; i < files.length; i++) {
                            new File(directoryPath, files[i]).delete();
                        }
                    }
                }
            }
        }
    }

    public static void copyFile(InputStream in, OutputStream out)
            throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static void delete(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                delete(c);
        }
        if (!f.delete())
            throw new FileNotFoundException("Failed to delete file: " + f);
    }

    public static void checkAndCreateDirectory(String pathToDirectory) {
        File directory = new File(pathToDirectory);
        if (!(directory.exists() && directory.isDirectory())) {
            directory.mkdirs();
        }
    }

    public static void copyFolderAndContent(String pathFolder,
                                            String pathAsset, Activity activity) {
        AssetManager assetManager = activity.getResources().getAssets();
        String[] filesAsset = null;
        File[] filesMemory = null;

        checkAndCreateDirectory(pathFolder);

        File folder = new File(pathFolder);
        try {
            filesAsset = assetManager.list(pathAsset);
            if (folder.exists())
                filesMemory = folder.listFiles();
            else
                folder.mkdirs();
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        if (filesMemory == null
                || (filesMemory != null && filesMemory.length != filesAsset.length)) {
            for (int i = 0; i < filesAsset.length; i++) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(pathAsset + "/" + filesAsset[i]);
                    out = new FileOutputStream(pathFolder + "/" + filesAsset[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    // Log.e("copy clipart ERROR", e.toString());
                    // e.printStackTrace();
                }
            }
        }
    }

    public static void copyFileAsset(String fileName, String folder,
                                     Context activity) {
        AssetManager assetManager = activity.getResources().getAssets();
        String[] files = null;
        try {
            files = assetManager.list(Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/" + folder);
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        for (int i = 0; i < files.length; i++) {
            File importFolder = new File(basepath + "/"
                    + Constants.APP_FOLDER_NAME + "/" + folder + "/" + fileName);
            if (files[i].equalsIgnoreCase(fileName) && !importFolder.exists()) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(Constants.ROOT_FOLDER_NAME + "/"
                            + Constants.APP_FOLDER_NAME + "/" + folder + "/"
                            + files[i]);
                    out = new FileOutputStream(basepath + "/"
                            + Constants.APP_FOLDER_NAME + "/" + folder + "/"
                            + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    Log.e("copy file ERROR", e.toString());
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void copyFileAssetSCDB(String fileName, String folder,
                                         Context activity) {
        AssetManager assetManager = activity.getResources().getAssets();
        String[] files = null;
        try {
            files = assetManager.list(Constants.ROOT_FOLDER_NAME + "/"
                    + Constants.APP_FOLDER_NAME + "/" + folder);
        } catch (Exception e) {
            // Log.e("read clipart ERROR", e.toString());
            // e.printStackTrace();
        }
        for (int i = 0; i < files.length; i++) {
            File importFolder = new File(basepath + "/"
                    + Constants.SCDB_LOOKUP_FOLDER_NAME + "/" + fileName);
            if (files[i].equalsIgnoreCase(fileName) && !importFolder.exists()) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(Constants.ROOT_FOLDER_NAME + "/"
                            + Constants.APP_FOLDER_NAME + "/" + folder + "/"
                            + files[i]);
                    out = new FileOutputStream(basepath + "/" + Constants.SCDB_LOOKUP_FOLDER_NAME + "/"
                            + files[i]);
                    copyFile(in, out);
                    in.close();
                    in = null;
                    out.flush();
                    out.close();
                    out = null;
                } catch (Exception e) {
                    Log.e("copy file ERROR", e.toString());
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void createDirectory(String path) {
        if (path != null) {
            if (path.length() > 0) {
                if (!new File(path).exists()) {
                    new File(path).mkdirs();
                }
            }
        }
    }

    public static List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
                if (file.getName().endsWith(".csv")) {
                    inFiles.add(file);
                }
            }
        }
        return inFiles;
    }

    public static ArrayList<String> getListFileName(List<File> listFiles, String prefix) {
        ArrayList<String> listFileName = new ArrayList<>();
        for (File file : listFiles) {
            if (file.getName().contains(prefix)) {
                listFileName.add(file.getName());
            }
        }
        return listFileName;
    }

    private static String getFileNameDateValue(String fileName) {
        String[] splitFileNameAndDate = fileName.split("_");
        return splitFileNameAndDate[1].substring(0, splitFileNameAndDate[1].length() - 4);
    }

    public static void deleteUnusedFile(List<String> listFileName) {
        List<Date> listFileNameDate = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        for (int i = 0; i < listFileName.size(); i++) {
            try {
                if (listFileName.get(i).split("_").length > 1)
                    listFileNameDate.add(sdf.parse(getFileNameDateValue(listFileName.get(i))));
                else {
                    File file = new File(Constants.EXTERNAL_ROOT_FOLDER_SCDB + listFileName.get(i));
                    file.delete();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String latestDate = "";
        if (listFileNameDate.size() > 1) {
            latestDate = new SimpleDateFormat("yyyyMMdd").format(Collections.max(listFileNameDate));

            for (String filename : listFileName) {
                if (!filename.contains(latestDate)) {
                    File file = new File(Constants.EXTERNAL_ROOT_FOLDER_SCDB + filename);
                    file.delete();
                }
            }
        }
    }

    public static String getLatestFileName(List<String> listFileName) {
        String fileNameReturn = "";
        List<Date> listFileNameDate = new ArrayList<>();
        String fileName = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        for (int i = 0; i < listFileName.size(); i++) {
            try {
                if (listFileName.get(i).split("_").length > 1)
                    listFileNameDate.add(sdf.parse(getFileNameDateValue(listFileName.get(i))));
                else fileName = listFileName.get(i);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (listFileNameDate.size() == 0) {
            return fileName;
        }

        String latestDate = new SimpleDateFormat("yyyyMMdd").format(Collections.max(listFileNameDate));

        for (String filename : listFileName) {
            if (filename.contains(latestDate)) {
                fileNameReturn = filename;
            }
        }

        return fileNameReturn;
    }

    /* =======================
                    SOP Agreement
    ======================= */
    public static void showPopupSOP(final Activity activity,
                                    final String typeDoc, final View.OnClickListener oc) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_sop_jsa);

                TextView titleET = (TextView) dialog.findViewById(R.id.title);
                titleET.setText(typeDoc);

                Spannable span = Spannable.Factory
                        .getInstance()
                        .newSpannable(
                                typeDoc.equalsIgnoreCase(Constants.SOP_EN) ? Constants.SOP_EN_PDF
                                        : Constants.SOP_ID_PDF);
                span.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        Helper.copyFolderAndContent(
                                Constants.EXTERNAL_ROOT_FOLDER
                                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                                        + Constants.SOP,
                                Constants.ROOT_FOLDER_NAME + "/"
                                        + Constants.APP_FOLDER_NAME + "/"
                                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                                        + Constants.SOP, activity);
                        if (typeDoc.equalsIgnoreCase(Constants.SOP_EN)
                                || typeDoc.equalsIgnoreCase(Constants.SOP_ID)) {

                            File file = new File(
                                    Constants.EXTERNAL_ROOT_FOLDER
                                            + Constants.DOCUMENTS_FOLDER_NAME
                                            + "/"
                                            + Constants.SOP
                                            + "/"
                                            + (typeDoc
                                            .equalsIgnoreCase(Constants.SOP_EN) ? Constants.SOP_EN_PDF
                                            : Constants.SOP_ID_PDF));
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            String type = "application/pdf";
                            intent.setDataAndType(Uri.fromFile(file), type);
                            activity.startActivity(intent);

                        } else {

                        }
                    }
                }, 0, span.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                // All the rest will have the same spannable.
                ClickableSpan cs = new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        Helper.copyFolderAndContent(
                                Constants.EXTERNAL_ROOT_FOLDER
                                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                                        + Constants.SOP,
                                Constants.ROOT_FOLDER_NAME + "/"
                                        + Constants.APP_FOLDER_NAME + "/"
                                        + Constants.DOCUMENTS_FOLDER_NAME + "/"
                                        + Constants.SOP, activity);
                        if (typeDoc.equalsIgnoreCase(Constants.SOP_EN)
                                || typeDoc.equalsIgnoreCase(Constants.SOP_ID)) {

                            File file = new File(
                                    Constants.EXTERNAL_ROOT_FOLDER
                                            + Constants.DOCUMENTS_FOLDER_NAME
                                            + "/"
                                            + Constants.SOP
                                            + "/"
                                            + (typeDoc
                                            .equalsIgnoreCase(Constants.SOP_EN) ? Constants.SOP_EN_PDF
                                            : Constants.SOP_ID_PDF));
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            String type = "application/pdf";
                            intent.setDataAndType(Uri.fromFile(file), type);
                            activity.startActivity(intent);

                        } else {

                        }
                    }
                };

                // set the "test " spannable.
                span.setSpan(cs, 0, span.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                TextView tv = (TextView) dialog.findViewById(R.id.link);
                tv.setText(span);
                tv.setMovementMethod(LinkMovementMethod.getInstance());

                final CheckBox checkBoxAggreement;
                checkBoxAggreement = (CheckBox) dialog
                        .findViewById(R.id.documentCheckBox);
                if (typeDoc.equalsIgnoreCase(Constants.SOP_EN)) {
                    checkBoxAggreement.setText(R.string.agree_doc_en);
                } else {
                    checkBoxAggreement.setText(R.string.agree_doc_id);
                }

                if (DataSingleton.getInstance().isAgreeSOP()) {
                    checkBoxAggreement.setChecked(true);
                } else {
                    checkBoxAggreement.setChecked(false);
                }

                checkBoxAggreement
                        .setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                DataSingleton.getInstance().setAgreeSOP(
                                        checkBoxAggreement.isChecked());

                            }
                        });

                ButtonRectangle okButton = (ButtonRectangle) dialog
                        .findViewById(R.id.documentOKButton);
                okButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }

    /* =======================
                            Export Data
    ======================= */
    public static void exportLookup(Activity activity, Lookup exportType) {
        DataSource dataSource = new DataSource(activity);
        dataSource.open();
        ArrayList<String[]> listLookUp = new ArrayList<String[]>();
        switch (exportType) {
            case newData:
                listLookUp = dataSource.getAllManpower();
                CSVHelper.exportLookup(activity, listLookUp, exportType);
                break;

            case newDataLookUp:
                listLookUp = dataSource.getAllExportLookUp();
                CSVHelper.exportLookup(activity, listLookUp, exportType);
                break;

//            case findings:
//                listLookUp = dataSource.getAllExportFieldFinding();
//                CSVHelper.exportLookup(activity, listLookUp, exportType);
//                break;
        }
        dataSource.close();
    }

    /* =======================
                    Custom Progress Dialog
   =======================  */
    public static void showProgressDialog(Activity context) {
        progressDialog = ProgressDialog.show(context, "Loading",
                "Please wait..", true, false);
    }

    public class CustomProgressDialog extends Dialog {
        private Dialog dialog;
        private TextView titleET;
        private TextView contentET;
        private ProgressBar progressBar;
        private Activity activity;

        public CustomProgressDialog(Context context) {
            super(context);
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.popup_progress_import_dialog);

            titleET = (TextView) dialog.findViewById(R.id.title);
            contentET = (TextView) dialog.findViewById(R.id.contentProgressBar);
            progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar);

            activity = (Activity) context;
        }

        public Dialog getDialog() {
            return dialog;
        }

        public void setDialog(Dialog dialog) {
            this.dialog = dialog;
        }

        public void setTextProgress(final String message) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    contentET.setText(message);
                }
            });
        }

        public void setTextTitle(final String title) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    titleET.setText(title);
                }
            });
        }

        public void setProgress(final int progress) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    progressBar.setProgress(progress);
                }
            });
        }
    }

    public static void showPositiveNegativeDialog(Activity activity,
                                                  String title, String message, String positiveText,
                                                  String negativeText,
                                                  DialogInterface.OnClickListener onClickLister, DialogInterface.OnClickListener positiveOnClickLister,
                                                  DialogInterface.OnClickListener negativeOnClickLister) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) {
            builder.setTitle(title);
        }

        if (message != null) {
            builder.setMessage(message);
        }
        builder.setCancelable(false);
        builder.setPositiveButton(positiveText != null ? positiveText : "OK",
                positiveOnClickLister);
        builder.setNegativeButton(negativeText != null ? negativeText
                : "Cancel", negativeOnClickLister);
        builder.show();
    }

    public static CustomProgressDialog getCostumProgressDialog(
            final Activity activity) {
        return new Helper().new CustomProgressDialog(activity);
    }

    public static void showPopUpMessage(final Activity activity,
                                        final String title, final String message, final View.OnClickListener oc) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_alert);

                TextView titleET = (TextView) dialog.findViewById(R.id.title);
                TextView messageET = (TextView) dialog
                        .findViewById(R.id.nameETHome);

                titleET.setText(title);
                messageET.setText(message);

                ButtonRectangle okButton = (ButtonRectangle) dialog.findViewById(R.id.button_ok);

                if (oc == null) {
                    okButton.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });

                } else {
                    okButton.setOnClickListener(oc);
                }
                dialog.show();
            }
        });
    }

    /* ==========================
                              Bitmap for Signature
    ========================== */
    public static String bitmapToString(Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap != null) {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return encodedImage;
    }

    public static Bitmap stringToBitmap(String imageString) {
        Bitmap bitmap = null;
        if (imageString != null) {
            byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
                    decodedString.length);
        }

        return bitmap;
    }

    public static int dpToPx(float dp, Resources resources) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
        return (int) px;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth) {
        // Raw height and width of image
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (width > reqWidth) {

            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap rotateImage(Bitmap img, float degree) {
        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        Bitmap rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    public static Bitmap rotateImage(String filePath, float degree, int reqWidth) {
        Bitmap rotatedBMP = null;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        bmOptions.inSampleSize = 5;
        bmOptions.inPurgeable = true;
        Bitmap img;
        BitmapFactory.decodeFile(filePath, bmOptions);

        // Calculate inSampleSize
        bmOptions.inSampleSize = calculateInSampleSize(bmOptions, reqWidth);

        // Decode bitmap with inSampleSize set
        bmOptions.inJustDecodeBounds = false;
        img = BitmapFactory.decodeFile(filePath, bmOptions);

        Matrix matrix = new Matrix();
        matrix.setRotate(degree, img.getWidth() / 2, img.getHeight() / 2);

        rotatedBMP = Bitmap.createBitmap(img, 0, 0, img.getWidth(),
                img.getHeight(), matrix, true);

        return rotatedBMP;
    }

    public static void setPic(ImageView iv, String filePath) {
        if (filePath == null || filePath.equalsIgnoreCase("")) {
            iv.setImageResource(R.drawable.default_nopicture);
        } else {
            // Get the dimensions of the View
            int targetW = iv.getWidth();
            int targetH = iv.getHeight();

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, bmOptions);

            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int scaleFactor;

            // Determine how much to scale down the image
            if (targetW == 0 || targetH == 0) {
                scaleFactor = 3;
            } else {
                scaleFactor = Math.min(photoW / targetW, photoH / targetH);
            }

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
            try {
                ExifInterface ei;
                ei = new ExifInterface(filePath);
                int orientation = ei.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotateImage(bitmap, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotateImage(bitmap, 180);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (bitmap == null) {
                iv.setImageResource(R.drawable.default_nopicture);
            } else {
                iv.setImageBitmap(bitmap);
            }
        }
    }

    public static void showAbout(final Activity activity) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.about);
                ListView listAbout = (ListView) dialog
                        .findViewById(R.id.list_about);

                String versionName;
                try {
                    versionName = activity.getPackageManager().getPackageInfo(
                            activity.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    versionName = "version name cannot be detected";
                }

				/* this array list is consist of title and content */
                ArrayList<String[]> listAboutContent = new ArrayList<String[]>();
                listAboutContent.add(new String[]{"Version", versionName});
                listAboutContent
                        .add(new String[]{"About",
                                "SHE Inspection 2.0 mobile application is collaboration project with"});
                listAboutContent.add(new String[]{
                        "PTFI",
                        "1.\t\tWahyu Sunyoto" + "\n2.\t\tAnton Perdana"
                                + "\n3.\t\tMunsi Nasution"
                                + "\n4.\t\tIwan Sriyanto"
                                + "\n5.\t\tSurya Nugraha"
                                + "\n6.\t\tHeri Setiono"
                                + "\n7.\t\tAkhwan Muntaha"
                                + "\n8.\t\tPersi Laseria"
                                + "\n9.\t\tIrwan Hakim"
                                + "\n10.\tHanny Runtuwene"
                                + "\n11.\tEdi Lesnusa"});
                listAboutContent.add(new String[]{
                        "ENJ",
                        "1.\t\tAditya Pringgoprawiro"
                                + "\n2.\t\tTommy Kurniawan"
                                + "\n3.\t\tSemara Rasmaranti"
                                + "\n4.\t\tSena Ardy"
                                + "\n5.\t\tChandra Isefa"});
                listAboutContent.add(new String[]{"Release Date",
                        "Tembagapura, Dec 2016"});
                ListChangeLogAdapter customAdapter = new ListChangeLogAdapter(activity,
                        listAboutContent);

                listAbout.setAdapter(customAdapter);
                dialog.show();
            }
        });
    }

    public static void showChangelogs(final Activity activity) {
        // TODO Auto-generated method stub
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.about);
                ListView listAbout = (ListView) dialog
                        .findViewById(R.id.list_about);

                TextView title = (TextView) dialog.findViewById(R.id.title);
                title.setText("CHANGELOGS");

                String versionName;
                try {
                    versionName = activity.getPackageManager().getPackageInfo(
                            activity.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    versionName = "version name cannot be detected";
                }

				/* this array list is consist of title and content */
                ArrayList<String[]> listAboutContent = new ArrayList<String[]>();
                listAboutContent.add(new String[]{"Version", versionName});
                listAboutContent
                        .add(new String[]{
                                "Global",
                                " - Fresh look for better use"
                        });

                ListChangeLogAdapter customAdapter = new ListChangeLogAdapter(activity,
                        listAboutContent);

                listAbout.setAdapter(customAdapter);
                dialog.show();
            }
        });
    }

    public static String ddMMMYYYYtoYYYYMMdd(String date) {
        String result = "";
        if (date.split(" ").length > 2) {
            result += date.split(" ")[2];
            result += shortMonthToNum(date.split(" ")[1]);
            result += date.split(" ")[0];
            return result;
        }
        return date;
    }

    public static String shortMonthToNum(String month) {
        String numMonth = "00";
        if (month.equalsIgnoreCase("January")) {
            numMonth = "01";
        } else if (month.equalsIgnoreCase("February")) {
            numMonth = "02";
        } else if (month.equalsIgnoreCase("March")) {
            numMonth = "03";
        } else if (month.equalsIgnoreCase("April")) {
            numMonth = "04";
        } else if (month.equalsIgnoreCase("May")) {
            numMonth = "05";
        } else if (month.equalsIgnoreCase("June")) {
            numMonth = "06";
        } else if (month.equalsIgnoreCase("July")) {
            numMonth = "07";
        } else if (month.equalsIgnoreCase("August")) {
            numMonth = "08";
        } else if (month.equalsIgnoreCase("September")) {
            numMonth = "09";
        } else if (month.equalsIgnoreCase("October")) {
            numMonth = "10";
        } else if (month.equalsIgnoreCase("November")) {
            numMonth = "11";
        } else {
            numMonth = "12";
        }
        return numMonth;
    }

    public static String trimString(String stringToTrim) {
        if (stringToTrim.length() < 23) {
            return stringToTrim;
        } else {
            return stringToTrim.substring(0,23) + "...";
        }
    }

    /**
     * Get application version
     *
     * @param activity
     * @return
     */
    public static String getAppVersion(Activity activity) {
        String appVersion = "";

        try {
            appVersion = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            appVersion = "Version of application is cannot be detected";
            e.printStackTrace();
        }

        return appVersion;
    }
}
