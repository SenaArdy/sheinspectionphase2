package com.ptfi.sheinspectionphase2;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ptfi.sheinspectionphase2.Models.DataSingleton;
import com.ptfi.sheinspectionphase2.Utils.Constants;
import com.ptfi.sheinspectionphase2.Utils.Helper;

public class MainMenu extends AppCompatActivity {

    Activity mActivity = MainMenu.this;

    TextView indonesianBtn, englishBtn, imei, versionTV;

    LinearLayout inspectionMenu, plannedInspection, ugDrilling, grsDrilling, lvInspection;

    boolean trueManpower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                trueManpower = true;
            } else {
                trueManpower= extras.getBoolean("trueManpower");
            }
        } else {
            trueManpower = (boolean) savedInstanceState.getSerializable("trueManpower");
        }

        setContentView(R.layout.mainmenu);

        if (trueManpower == false) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
            builder.setIcon(R.drawable.warning_logo);
            builder.setTitle("Warning");
            builder.setMessage("Wrong Format Manpower .CSV from SCDB");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setCancelable(false);
            builder.show();
        }

        versionTV = (TextView) findViewById(R.id.version);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionTV.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imei = (TextView) findViewById(R.id.imei);
        imei.setText("Device IMEI : " + deviceID);

        initComponent();
    }

    public void initComponent() {
        inspectionMenu = (LinearLayout) findViewById(R.id.inspectionMenu);
        plannedInspection = (LinearLayout) findViewById(R.id.plannedInspection);
        ugDrilling = (LinearLayout) findViewById(R.id.ugDrilling);
        grsDrilling = (LinearLayout) findViewById(R.id.grsDrilling);
//        lvInspection = (LinearLayout) findViewById(R.id.lvInspection);

        indonesianBtn = (TextView) findViewById(R.id.indonesianBtn);
        englishBtn = (TextView) findViewById(R.id.englishBtn);

        indonesianBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    Intent intent = new Intent(mActivity, SopActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("language", "Indonesia");
//                    intent.putExtras(bundle);
//                    startActivity(intent);
//                    mActivity.finish();
//                } else {
                Helper.showPopupSOP(MainMenu.this, Constants.SOP_ID, null);
//                }
            }
        });

        englishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    Intent intent = new Intent(mActivity, SopActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("language", "English");
//                    intent.putExtras(bundle);
//                    startActivity(intent);
//                    mActivity.finish();
//                } else {
                Helper.showPopupSOP(MainMenu.this, Constants.SOP_ID, null);
//                }
            }
        });

        inspectionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgreeSOP()) {
                    DataSingleton.getInstance().setAgreeSOP(false);
                    Intent intent = new Intent(mActivity, FieldActivity.class);
                    mActivity.finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    Helper.showPopUpMessage(
                            MainMenu.this,
                            "Warning",
                            "You must agree to the contents of the SOP document",
                            null);
                }
            }
        });

        plannedInspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgreeSOP()) {
                    DataSingleton.getInstance().setAgreeSOP(false);
                    Intent intent = new Intent(mActivity, PlannedActivity.class);
                    mActivity.finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    Helper.showPopUpMessage(
                            MainMenu.this,
                            "Warning",
                            "You must agree to the contents of the SOP document",
                            null);
                }
            }
        });

        ugDrilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgreeSOP()) {
                    DataSingleton.getInstance().setAgreeSOP(false);
                    Intent intent = new Intent(mActivity, UGActivity.class);
                    mActivity.finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    Helper.showPopUpMessage(
                            MainMenu.this,
                            "Warning",
                            "You must agree to the contents of the SOP document",
                            null);
                }
            }
        });

        grsDrilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAgreeSOP()) {
                    DataSingleton.getInstance().setAgreeSOP(false);
                    Intent intent = new Intent(MainMenu.this, GRSActivity.class);
                    startActivity(intent);
                    mActivity.finish();
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else {
                    Helper.showPopUpMessage(
                            MainMenu.this,
                            "Warning",
                            "You must agree to the contents of the SOP document",
                            null);
                }
            }
        });


//        lvInspection.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MainMenu.this, LVActivity.class);
//                startActivity(intent);
//                mActivity.finish();
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//            }
//        });

    }

    private boolean isAgreeSOP() {
        return DataSingleton.getInstance().isAgreeSOP();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_about:
                Helper.showAbout(MainMenu.this);
                break;
//            case R.id.action_backmenu:
//                Helper.showChangelogs(MainMenu.this);
//                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from SHE Inspection Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }
}
