package com.ptfi.sheinspectionphase2.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.ptfi.sheinspectionphase2.Models.CustomSpinnerItem;
import com.ptfi.sheinspectionphase2.Models.FieldFindingsModel;
import com.ptfi.sheinspectionphase2.Models.FieldMainModel;
import com.ptfi.sheinspectionphase2.Models.GrsDetailModel;
import com.ptfi.sheinspectionphase2.Models.GrsDrillingModel;
import com.ptfi.sheinspectionphase2.Models.InspectorUGModel;
import com.ptfi.sheinspectionphase2.Models.LVDailyModel;
import com.ptfi.sheinspectionphase2.Models.LVDamageModel;
import com.ptfi.sheinspectionphase2.Models.LVGroupModel;
import com.ptfi.sheinspectionphase2.Models.LVInspectionModel;
import com.ptfi.sheinspectionphase2.Models.LVWeeklyModel;
import com.ptfi.sheinspectionphase2.Models.LookUpModel;
import com.ptfi.sheinspectionphase2.Models.ManpowerModel;
import com.ptfi.sheinspectionphase2.Models.PhotoModel;
import com.ptfi.sheinspectionphase2.Models.PlannedFindingsModel;
import com.ptfi.sheinspectionphase2.Models.PlannedInspectionModel;
import com.ptfi.sheinspectionphase2.Models.PlannedMainModel;
import com.ptfi.sheinspectionphase2.Models.PlannedSignModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingInspectionModel;
import com.ptfi.sheinspectionphase2.Models.UGDrillingMainModel;
import com.ptfi.sheinspectionphase2.Utils.Helper;

import java.util.ArrayList;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class DataSource {

    private static SQLiteDatabase database;
    private DataHelper dbHelper;

    /* ==========================
                        Datasource Controller
    ========================== */
    public DataSource(Context context) {
        dbHelper = new DataHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public boolean isTableExists(String tableName, boolean openDb) {
        if (openDb) {
            if (database == null || !database.isOpen()) {
                database = dbHelper.getReadableDatabase();
            }

            if (!database.isReadOnly()) {
                database.close();
                database = dbHelper.getReadableDatabase();
            }
        }

        Cursor cursor = database.rawQuery(
                "select DISTINCT tbl_name from sqlite_master where tbl_name = '"
                        + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public boolean isContentOfTableExist(String tableName) {
        if (!isTableExists(tableName, false))
            return false;
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = database.rawQuery(query, new String[]{});
        if (cursor.getCount() > 0)
            return true;

        cursor.close();
        return false;
    }

    /* ===========================
                     Query Database for Look Up
    =========================== */
    private String[] allColumnsObs = {DataHelper.COLUMN_ID,
            DataHelper.COLUMN_OBS_ID, DataHelper.COLUMN_OBS_NAME,
            DataHelper.COLUMN_OBS_TITLE, DataHelper.COLUMN_OBS_ORG,
            DataHelper.COLUMN_OBS_COMP, DataHelper.COLUMN_OBS_DIVISI,
            DataHelper.COLUMN_OBS_DEPT, DataHelper.COLUMN_OBS_SECTION};

    // COMPANY
    private String[] allColumnsCompany = {DataHelper.COLUMN_ID,
            DataHelper.COL_LOOKUP, DataHelper.COL_DESC};

    // BUSINESS_UNIT
    private String[] allColumnsBusiness = {DataHelper.COLUMN_ID,
            DataHelper.COL_LOOKUP, DataHelper.COL_DESC,
            DataHelper.COL_REF};

    // DEPARTMENT
    private String[] allColumnsDepartment = {DataHelper.COLUMN_ID,
            DataHelper.COL_LOOKUP, DataHelper.COL_DESC,
            DataHelper.COL_REF};

    // SECTION
    private String[] allColumnsSection = {DataHelper.COLUMN_ID,
            DataHelper.COL_LOOKUP, DataHelper.COL_DESC,
            DataHelper.COL_REF};

    // POTENTIAL RISK
    private String[] allColumnsPRisk = {DataHelper.COLUMN_ID,
            DataHelper.COL_VAL};

    // LOOK UP
    private String[] allColumnsLookUp = {DataHelper.COLUMN_ID,
            DataHelper.COL_POTETIAL_RISK, DataHelper.COL_RATING};

    // RATING
    private String[] allColumnsRating = {DataHelper.COLUMN_ID,
            DataHelper.COL_VAL};

    //AVAILABILITY
    private String[] allColumnsAvailability = {DataHelper.COLUMN_AVAILABILITY_ID,
            DataHelper.COLUMN_AVAILABILITY_NAME};

    // DELETE ALL DATA LOOK UP
    public void deleteAllDataLookUp() {
        if (isTableExists(DataHelper.TABLE_LOOKUP, false)) {
            database.delete(DataHelper.TABLE_LOOKUP, null, null);
        }
    }

    public void deleteAllAvailability() {
        database.delete(DataHelper.TABLE_AVAILABILITY, null, null);
    }

    public void deleteAllDataCompany() {
        if (isTableExists(DataHelper.TABLE_COMPANY, false)) {
            database.delete(DataHelper.TABLE_COMPANY, null, null);
        }
    }

    public void deleteAllDataAvailability() {
        if (isTableExists(DataHelper.TABLE_AVAILABILITY, false)) {
            database.delete(DataHelper.TABLE_AVAILABILITY, null, null);
        }
    }

    public void deleteAllDataBusiness() {
        if (isTableExists(DataHelper.TABLE_BUSINESS_UNIT, false)) {
            database.delete(DataHelper.TABLE_BUSINESS_UNIT, null, null);
        }
    }

    public void deleteAllDataDepartment() {
        if (isTableExists(DataHelper.TABLE_DEPARTMENT, false)) {
            database.delete(DataHelper.TABLE_DEPARTMENT, null, null);
        }
    }

    public void deleteAllDataSection() {
        if (isTableExists(DataHelper.TABLE_SECTION, false)) {
            database.delete(DataHelper.TABLE_SECTION, null, null);
        }
    }

    public void deleteAllDataPRisk() {
        if (isTableExists(DataHelper.TABLE_POTENTIAL_RISK, false)) {
            database.delete(DataHelper.TABLE_POTENTIAL_RISK, null, null);
        }
    }

    public void deleteAllDataRating() {
        if (isTableExists(DataHelper.TABLE_RATING, false)) {
            database.delete(DataHelper.TABLE_RATING, null, null);
        }
    }

    // CREATE DATA LOOK UP
    public long createCompany(CustomSpinnerItem model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_LOOKUP, model.lookup);
        values.put(DataHelper.COL_DESC, model.description);

        long insertId = database.insert(DataHelper.TABLE_COMPANY, null,
                values);
        return insertId;
    }

    public long createBusinessUnit(CustomSpinnerItem model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_LOOKUP, model.lookup);
        values.put(DataHelper.COL_DESC, model.description);
        values.put(DataHelper.COL_REF, model.ref);

        long insertId = database.insert(DataHelper.TABLE_BUSINESS_UNIT,
                null, values);
        return insertId;
    }

    public long createDepartment(CustomSpinnerItem model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_LOOKUP, model.lookup);
        values.put(DataHelper.COL_DESC, model.description);
        values.put(DataHelper.COL_REF, model.ref);

        long insertId = database.insert(DataHelper.TABLE_DEPARTMENT, null,
                values);
        return insertId;
    }

    public long createSection(CustomSpinnerItem model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_LOOKUP, model.lookup);
        values.put(DataHelper.COL_DESC, model.description);
        values.put(DataHelper.COL_REF, model.ref);

        long insertId = database.insert(DataHelper.TABLE_SECTION, null,
                values);
        return insertId;
    }

    public long createPRisk(String val) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_VAL, val);

        long insertId = database.insert(DataHelper.TABLE_POTENTIAL_RISK,
                null, values);
        return insertId;
    }

    public long createRating(String val) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_VAL, val);

        long insertId = database.insert(DataHelper.TABLE_RATING, null,
                values);
        return insertId;
    }

    public long createAvailabilty(String val) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COLUMN_AVAILABILITY_NAME, val);

        long insertId = database.insert(DataHelper.TABLE_AVAILABILITY,
                null, values);
        return insertId;
    }

    public ArrayList<String[]> getAllManpower() {
        ArrayList<String[]> data = new ArrayList<String[]>();

        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER,
                allColumnsObs, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String[] model = new String[4];
            model[0] = cursor.getString(0);
            model[1] = cursor.getString(1);
            model[2] = cursor.getString(2);
            model[3] = cursor.getString(3);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<String[]> getAllExportLookUp() {
        ArrayList<String[]> data = new ArrayList<String[]>();

        Cursor cursor = database.query(DataHelper.TABLE_LOOKUP,
                allColumnsLookUp, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String[] model = new String[2];
            model[0] = cursor.getString(0);
            model[1] = cursor.getString(1);

            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<String> getManpowerName() {
        ArrayList<String> results = new ArrayList<String>();

        String[] findFor = {DataHelper.COLUMN_OBS_NAME};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public String getManpowerId() {
//        ArrayList<String> results = new ArrayList<String>();
        String results = "";
        String[] findFor = {DataHelper.COLUMN_OBS_ID};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        if (cursor.getCount() > 1) {
            results = cursor.getString(0);
        }

        cursor.close();
        return results;
    }

    public String getManpowerIdData(final String name) {
//        ArrayList<String> results = new ArrayList<String>();
        String results = "";
//        String[] findFor = {DataHelper.COLUMN_OBS_ID};
        String query = "SELECT " + DataHelper.COLUMN_OBS_ID + " FROM " + DataHelper.TABLE_OBSERVER + " WHERE " + DataHelper.COLUMN_OBS_NAME + " = '" + name + "'";
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            results = cursor.getString(0);
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreId() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_ID};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreOrg() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_ORG};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreCompany() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_COMP};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreDivisi() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_DIVISI};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreDept() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_DEPT};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<String> getManpowerMoreSection() {
        ArrayList<String> results = new ArrayList<String>();
        String[] findFor = {DataHelper.COLUMN_OBS_SECTION};
        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, findFor,
                null, null, null, null, DataHelper.COLUMN_ID);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            results.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    public ArrayList<ManpowerModel> getAllManpowerData() {
        ArrayList<ManpowerModel> results = new ArrayList<ManpowerModel>();

        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER, allColumnsObs,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ManpowerModel model = cursorToDataModel(cursor);
            results.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return results;
    }

    private ManpowerModel cursorToDataModel(Cursor cursor) {
        ManpowerModel model = new ManpowerModel();
        model.setId(cursor.getLong(0));
        model.setIdno(cursor.getString(1));
        model.setName(cursor.getString(2));
        model.setTitle(cursor.getString(3));
        model.setOrg(cursor.getString(4));
        model.setCompany(cursor.getString(5));
        model.setDivisi(cursor.getString(6));
        model.setDepartment(cursor.getString(7));
        model.setSection(cursor.getString(8));

        return model;
    }

    public ArrayList<String> getPotentialRisk() {
        ArrayList<String> data = new ArrayList<String>();

        String query = "SELECT " + DataHelper.COL_POTETIAL_RISK + " FROM " + DataHelper.TABLE_LOOKUP;
        Cursor cursor = database.rawQuery(query, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String datas = cursor.getString(0);
            data.add(datas);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public ArrayList<String> getRating() {
        ArrayList<String> data = new ArrayList<String>();

        String query = "SELECT " + DataHelper.COL_RATING + " FROM " + DataHelper.TABLE_LOOKUP;
        Cursor cursor = database.rawQuery(query, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String datas = cursor.getString(0);
            data.add(datas);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    private CustomSpinnerItem cursorToCustomModel(Cursor cursor) {
        CustomSpinnerItem model = new CustomSpinnerItem();
        model.lookup = cursor.getString(1);
        model.description = (cursor.getString(2));
        if (cursor.getColumnCount() > 3) {
            model.ref = (cursor.getString(3));
        } else {
            model.ref = "";
        }
        return model;
    }

    public ArrayList<CustomSpinnerItem> getAllCompany() {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_COMPANY,
                allColumnsCompany, null, null, null, null, null);

        data.add(new CustomSpinnerItem("Value", "Description"));
        data.add(new CustomSpinnerItem("-", ""));
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getBusinessByCompany(String ref) {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_BUSINESS_UNIT,
                allColumnsBusiness, DataHelper.COL_REF + " = '" + ref
                        + "' ", null, null, null, null);

        data.add(new CustomSpinnerItem("Value", "Description"));
        data.add(new CustomSpinnerItem("-", ""));
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getAllBusiness() {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_BUSINESS_UNIT,
                allColumnsBusiness, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getDepartmentByBusiness(String ref) {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_DEPARTMENT,
                allColumnsDepartment, DataHelper.COL_REF + " = '" + ref
                        + "' ", null, null, null, null);

        data.add(new CustomSpinnerItem("Value", "Description"));
        data.add(new CustomSpinnerItem("-", ""));
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getAllDepartment() {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_DEPARTMENT,
                allColumnsDepartment, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getSectionByDepartment(String ref) {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_SECTION,
                allColumnsSection, DataHelper.COL_REF + " = '" + ref
                        + "' ", null, null, null, null);

        data.add(new CustomSpinnerItem("Value", "Description"));
        data.add(new CustomSpinnerItem("-", ""));
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<CustomSpinnerItem> getAllSection() {
        ArrayList<CustomSpinnerItem> data = new ArrayList<CustomSpinnerItem>();

        Cursor cursor = database.query(DataHelper.TABLE_SECTION,
                allColumnsSection, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CustomSpinnerItem model = cursorToCustomModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<String> getAllPotentialRisk() {
        ArrayList<String> data = new ArrayList<String>();

        Cursor cursor = database.query(DataHelper.TABLE_POTENTIAL_RISK,
                allColumnsPRisk, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            data.add(cursor.getString(1));
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<ManpowerModel> getAllManpowerResources() {
        ArrayList<ManpowerModel> data = new ArrayList<ManpowerModel>();

        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER,
                allColumnsObs, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ManpowerModel model = cursorToDataModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    private LookUpModel cursorToLookUpModel(Cursor cursor) {
        LookUpModel model = new LookUpModel();
        model.setId(cursor.getLong(0));
        model.setPotential_risk(cursor.getString(1));
        model.setRating(cursor.getString(2));

        return model;
    }

    public ArrayList<LookUpModel> getAllLookUp() {
        ArrayList<LookUpModel> data = new ArrayList<LookUpModel>();

        Cursor cursor = database.query(DataHelper.TABLE_LOOKUP,
                allColumnsLookUp, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            LookUpModel model = cursorToLookUpModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<String> getAllAvailibity() {
        ArrayList<String> data = new ArrayList<String>();

        Cursor cursor = database.query(
                DataHelper.TABLE_AVAILABILITY, allColumnsAvailability,
                null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            data.add(cursor.getString(1));
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<String> getAllRating() {
        ArrayList<String> data = new ArrayList<String>();

        Cursor cursor = database.query(DataHelper.TABLE_RATING,
                allColumnsRating, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            data.add(cursor.getString(1));
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }


    /* ===========================
                  Query Database Manpower & Observer
     ===========================*/
    public void deleteAllDataObserver() {
        if (isTableExists(DataHelper.TABLE_OBSERVER, false)) {
            database.delete(DataHelper.TABLE_OBSERVER, null, null);
        }
    }

    public long createLookUp(LookUpModel model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COL_POTETIAL_RISK, model.getPotential_risk());
        values.put(DataHelper.COL_RATING, model.getRating());

        long dataLookUp = database.insert(DataHelper.TABLE_LOOKUP, null, values);

        return dataLookUp;
    }

    public ArrayList<ManpowerModel> getAllDataObserver() {
        ArrayList<ManpowerModel> data = new ArrayList<ManpowerModel>();

        Cursor cursor = database.query(DataHelper.TABLE_OBSERVER,
                allColumnsObs, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ManpowerModel model = cursorToDataModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public long createDataObserver(ManpowerModel model) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COLUMN_OBS_ID, model.getIdno());
        values.put(DataHelper.COLUMN_OBS_NAME, model.getName());
        values.put(DataHelper.COLUMN_OBS_TITLE, model.getTitle());
        values.put(DataHelper.COLUMN_OBS_ORG, model.getOrg());
        values.put(DataHelper.COLUMN_OBS_COMP, model.getCompany());
        values.put(DataHelper.COLUMN_OBS_DIVISI, model.getDivisi());
        values.put(DataHelper.COLUMN_OBS_DEPT, model.getDepartment());
        values.put(DataHelper.COLUMN_OBS_SECTION, model.getSection());

        long insertId = database.insert(DataHelper.TABLE_OBSERVER, null,
                values);
        return insertId;
    }

    /* ====================
               Grassberg Drilling Inspection
    ==================== */
    private String[] allColumnGrsDrill = {DataHelper.COLUMN_GRS_DRILL_ID,
            DataHelper.COLUMN_GRS_DRILL_PIC, DataHelper.COLUMN_GRS_DRILL_PIC_ID,
            DataHelper.COLUMN_GRS_DRILL_DATE};

    public long insertGrsDrilling(GrsDrillingModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_GRS_DRILL_PIC, model.getPicName());
        insert.put(DataHelper.COLUMN_GRS_DRILL_PIC_ID, model.getPicId());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DATE, model.getDate());

        long insertData = database.insert(DataHelper.TABLE_GRS_DRILL, null, insert);

        return insertData;
    }

    private GrsDrillingModel cursorToGRSDrillModel(Cursor cursor) {
        GrsDrillingModel model = new GrsDrillingModel();
        model.setGsId(cursor.getInt(0));
        model.setPicName(cursor.getString(1));
        model.setPicId(cursor.getString(2));
        model.setDate(cursor.getString(3));

        return model;
    }

    public GrsDrillingModel getLatestGRSDrill() {
        GrsDrillingModel data = new GrsDrillingModel();

        Cursor cursor = database.query(DataHelper.TABLE_GRS_DRILL,
                allColumnGrsDrill, DataHelper.COLUMN_GRS_DRILL_ID + " = (SELECT MAX(" + DataHelper.COLUMN_GRS_DRILL_ID + ") FROM " + DataHelper.TABLE_GRS_DRILL + ")", null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursorToGRSDrillModel(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return data;
    }


    /* =======================
            Grassberg Drilling Detail Inspection
    ======================= */
    private String[] allColumnGrsDetail = {DataHelper.COLUMN_GRS_DRILL_DETAIL_ID,
            DataHelper.COLUMN_GRS_DRILL_DETAIL_PIC, DataHelper.COLUMN_GRS_DRILL_DETAIL_DATE_INS,
            DataHelper.COLUMN_GRS_DRILL_DETAIL_DEPARTMENT, DataHelper.COLUMN_GRS_DRILL_DETAIL_COMMENT,
            DataHelper.COLUMN_GRS_DRILL_DETAIL_DATE, DataHelper.COLUMN_GRS_DRILL_DETAIL_SIGNER,
            DataHelper.COLUMN_GRS_DRILL_DETAIL_SIGN};

    public long insertGrsDetailData(GrsDetailModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_PIC, model.getPic());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_DATE_INS, model.getDateIns());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_DEPARTMENT, model.getDepartment());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_COMMENT, model.getComment());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_SIGNER, model.getSigner());
        insert.put(DataHelper.COLUMN_GRS_DRILL_DETAIL_SIGN, model.getSign());

        long insertData = database.insert(DataHelper.TABLE_GRS_DRILL_DETAIL, null, insert);

        return insertData;
    }

    private GrsDetailModel cursorToGRSDetailModel(Cursor cursor) {
        GrsDetailModel model = new GrsDetailModel();
        model.setId(cursor.getLong(0));
        model.setPic(cursor.getString(1));
        model.setDateIns(cursor.getString(2));
        model.setComment(cursor.getString(3));
        model.setDate(cursor.getString(4));
        model.setSign(cursor.getString(5));
        model.setSigner(cursor.getString(6));
        model.setDepartment(cursor.getString(7));


        return model;
    }

    public GrsDetailModel getLatestGRSDetail(String department, String date, String pic) {
        GrsDetailModel data = new GrsDetailModel();

        String queryGeology = "SELECT * FROM " + DataHelper.TABLE_GRS_DRILL_DETAIL + " WHERE " + DataHelper.COLUMN_GRS_DRILL_DETAIL_DEPARTMENT + " = '"
                + department + "' AND " + DataHelper.COLUMN_GRS_DRILL_DETAIL_DATE_INS + " = '" + date + "' AND "
                + DataHelper.COLUMN_GRS_DRILL_DETAIL_PIC + " = '" + pic + "'";

        Cursor cursor = database.rawQuery(queryGeology, null);
//        Cursor cursor = database.query(DataHelper.TABLE_GRS_DRILL_DETAIL,
//                allColumnGrsDrill, "SELECT * " + FROM, null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursorToGRSDetailModel(cursor);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    /* ===================
                        Photo Datasource
    =================== */
    private String[] allColumnsPhoto = {DataHelper.COLUMN_PHOTO_ID,
            DataHelper.COLUMN_PHOTO_ROW,
            DataHelper.COLUMN_PHOTO_INSPECTION_CODE,
            DataHelper.COLUMN_PHOTO_REFERENCE,
            DataHelper.COLUMN_PHOTO_PATH,
            DataHelper.COLUMN_PHOTO_COMMENT,
            DataHelper.COLUMN_PHOTO_DATE, DataHelper.COLUMN_PHOTO_PIC};

    public long insertPhotoData(PhotoModel model, int inspectionCode) {
        ContentValues values = new ContentValues();

        values.put(DataHelper.COLUMN_PHOTO_ROW, (int) model.getId());
        values.put(DataHelper.COLUMN_PHOTO_INSPECTION_CODE, inspectionCode);
        values.put(DataHelper.COLUMN_PHOTO_PATH, model.getFotoPath());
        values.put(DataHelper.COLUMN_PHOTO_COMMENT, model.getComment());
        values.put(DataHelper.COLUMN_PHOTO_DATE, model.getDate());
        values.put(DataHelper.COLUMN_PHOTO_PIC, model.getPic());


        long insertId = database.insert(
                DataHelper.TABLE_PHOTO, null, values);
        return insertId;
    }

    private PhotoModel cursortoDataFotoModel(Cursor cursor) {
        PhotoModel model = new PhotoModel();
        model.setId(cursor.getInt(1));
        model.setFotoPath(cursor.getString(4));
        model.setComment(cursor.getString(5));
        return model;
    }

    public ArrayList<PhotoModel> getPhotoData(int inspectionCode, String date, String projectOwner) {
        ArrayList<PhotoModel> data = new ArrayList<PhotoModel>();

        Cursor cursor = database.query(DataHelper.TABLE_PHOTO,
                allColumnsPhoto, DataHelper.COLUMN_PHOTO_INSPECTION_CODE + " = " + inspectionCode
                        + " AND " + DataHelper.COLUMN_PHOTO_DATE + " = '" + date + "' AND " + DataHelper.COLUMN_PHOTO_PIC
                        + " = '" + projectOwner + "'", null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PhotoModel model = cursortoDataFotoModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public void insertPhotoData(ContentValues values, PhotoModel model, int inspectionCode) {
        values.put(DataHelper.COLUMN_PHOTO_ROW, (int) model.getId());
        values.put(DataHelper.COLUMN_PHOTO_INSPECTION_CODE, inspectionCode);
        values.put(DataHelper.COLUMN_PHOTO_PATH, model.getFotoPath());
        values.put(DataHelper.COLUMN_PHOTO_COMMENT, model.getComment());
        values.put(DataHelper.COLUMN_PHOTO_DATE, model.getDate());
        values.put(DataHelper.COLUMN_PHOTO_PIC, model.getPic());
    }

    public long updatePhotoData(final PhotoModel model, final String photoPath, final String comment, final int inspectionCode) {
        ContentValues insert = new ContentValues();
        insertPhotoData(insert, model, inspectionCode);
        long createID = 0;
        database.update(DataHelper.TABLE_PHOTO, insert, DataHelper.COLUMN_PHOTO_PATH + " = '" + photoPath + "' AND "
                + DataHelper.COLUMN_PHOTO_COMMENT + " = '" + comment + "' AND "
                + DataHelper.COLUMN_PHOTO_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COLUMN_PHOTO_PIC + " = '" + model.getPic() + "' AND "
                + DataHelper.COLUMN_PHOTO_INSPECTION_CODE + " = '" + inspectionCode + "'", null);

        return createID;
    }

    public static void deletePhoto(String photoPath, String comment, String dateIns, String pic, int inspectionCode) {
        database.delete(DataHelper.TABLE_PHOTO, DataHelper.COLUMN_PHOTO_PATH + " = '" + photoPath + "' AND "
                + DataHelper.COLUMN_PHOTO_COMMENT + " = '" + comment + "' AND " + DataHelper.COLUMN_PHOTO_DATE + " = '" + dateIns + "' AND "
                + DataHelper.COLUMN_PHOTO_PIC + " = '" + pic + "' AND " + DataHelper.COLUMN_PHOTO_INSPECTION_CODE + " = '" + inspectionCode + "'", null);
    }

    /* =======================
                                Field Inspection
    ======================= */
    private String[] allColumnsFieldMain = {DataHelper.COLUMN_FIELD_ID,
            DataHelper.COLUMN_FIELD_COMPANY,
            DataHelper.COLUMN_FIELD_BUSINESS,
            DataHelper.COLUMN_FIELD_DEPARTMENT,
            DataHelper.COLUMN_FIELD_SECTION,
            DataHelper.COLUMN_FIELD_DATE,
            DataHelper.COLUMN_FIELD_INSPECTOR,
            DataHelper.COLUMN_FIELD_WORKAREA};

    public long insertFieldMainData(FieldMainModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_FIELD_COMPANY, model.getCompany());
        insert.put(DataHelper.COLUMN_FIELD_BUSINESS, model.getBusiness());
        insert.put(DataHelper.COLUMN_FIELD_DEPARTMENT, model.getDepartment());
        insert.put(DataHelper.COLUMN_FIELD_SECTION, model.getSection());
        insert.put(DataHelper.COLUMN_FIELD_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_FIELD_INSPECTOR, model.getInspector());
        insert.put(DataHelper.COLUMN_FIELD_WORKAREA, model.getWorkArea());

        long insertData = database.insert(DataHelper.TABLE_FIELD_INSPECTION, null, insert);

        return insertData;
    }

    private FieldMainModel cursorToFieldMainModel(Cursor cursor) {
        FieldMainModel model = new FieldMainModel();
        model.setId(cursor.getLong(0));
        model.setCompany(cursor.getString(1));
        model.setBusiness(cursor.getString(2));
        model.setDepartment(cursor.getString(3));
        model.setSection(cursor.getString(4));
        model.setDate(cursor.getString(5));
        model.setInspector(cursor.getString(6));
        model.setWorkArea(cursor.getString(7));

        return model;
    }

    public FieldMainModel getLastesFieldMain() {
        FieldMainModel data = new FieldMainModel();

        Cursor cursor = database.query(DataHelper.TABLE_FIELD_INSPECTION,
                allColumnsFieldMain, DataHelper.COLUMN_FIELD_ID + " = (SELECT MAX(" + DataHelper.COLUMN_FIELD_ID + ") FROM "
                        + DataHelper.TABLE_FIELD_INSPECTION + ")", null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursorToFieldMainModel(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<FieldMainModel> getAllDataFieldMain() {
        ArrayList<FieldMainModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_FIELD_INSPECTION;

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FieldMainModel data = cursorToFieldMainModel(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ========================
                               Field Finding
    ======================== */
    private String[] allColumnsFieldFindings = {DataHelper.COLUMN_FIELD_FINDING_ID,
            DataHelper.COLUMN_FIELD_FINDING_MAIN_ID,
            DataHelper.COLUMN_FIELD_FINDING,
            DataHelper.COLUMN_FIELD_ACTION,
            DataHelper.COLUMN_FIELD_WORK_AREA, DataHelper.COLUMN_FIELD_PICTURE,
            DataHelper.COLUMN_FIELD_PICTURE_PATH, DataHelper.COLUMN_FIELD_RESPONSIBLE,
            DataHelper.COLUMN_FIELD_DATE_COMPLETE, DataHelper.COLUMN_FIELD_INSPECTION_REF};

    public long insertFieldFindingsData(FieldFindingsModel model, long mainId) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_FIELD_FINDING_MAIN_ID, mainId);
        insert.put(DataHelper.COLUMN_FIELD_FINDING, model.getFindings());
        insert.put(DataHelper.COLUMN_FIELD_ACTION, model.getAction());
        insert.put(DataHelper.COLUMN_FIELD_WORK_AREA, model.getWorkArea());
        insert.put(DataHelper.COLUMN_FIELD_PICTURE, model.getPicture());
        insert.put(DataHelper.COLUMN_FIELD_PICTURE_PATH, model.getPicturePath());
        insert.put(DataHelper.COLUMN_FIELD_RESPONSIBLE, model.getResponsible());
        insert.put(DataHelper.COLUMN_FIELD_DATE_COMPLETE, model.getDateComplete());
        insert.put(DataHelper.COLUMN_FIELD_INSPECTION_REF, model.getReference());

        long insertData = database.insert(DataHelper.TABLE_FIELD_INSPECTION_FINDING, null, insert);

        return insertData;
    }

    private FieldFindingsModel cursorToFieldFindingsModel(Cursor cursor) {
        FieldFindingsModel model = new FieldFindingsModel();
        model.setId(cursor.getInt(0));
        model.setMainId(cursor.getLong(1));
        model.setFindings(cursor.getString(2));
        model.setAction(cursor.getString(3));
        model.setWorkArea(cursor.getString(4));
        model.setPicture(cursor.getString(5));
        model.setPicturePath(cursor.getString(6));
        model.setResponsible(cursor.getString(7));
        model.setDateComplete(cursor.getString(8));
        model.setReference(cursor.getLong(9));

        return model;
    }

    public ArrayList<FieldFindingsModel> getFieldFindingsData(long mainId) {
        ArrayList<FieldFindingsModel> data = new ArrayList<FieldFindingsModel>();

        Cursor cursor = database.query(DataHelper.TABLE_FIELD_INSPECTION_FINDING,
                allColumnsFieldFindings, DataHelper.COLUMN_FIELD_FINDING_MAIN_ID + " = " + mainId + "",
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FieldFindingsModel model = cursorToFieldFindingsModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<FieldFindingsModel> getFieldFindingsData(int referenceCode) {
        ArrayList<FieldFindingsModel> data = new ArrayList<FieldFindingsModel>();

        Cursor cursor = database.query(DataHelper.TABLE_FIELD_INSPECTION_FINDING,
                allColumnsFieldFindings, DataHelper.COLUMN_FIELD_INSPECTION_REF + " = " + referenceCode + "",
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            FieldFindingsModel model = cursorToFieldFindingsModel(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public void updateFieldFindingsData(final ContentValues insert, final FieldFindingsModel model, final int referenceCode) {
        insert.put(DataHelper.COLUMN_FIELD_FINDING, model.getFindings());
        insert.put(DataHelper.COLUMN_FIELD_ACTION, model.getAction());
        insert.put(DataHelper.COLUMN_FIELD_WORK_AREA, model.getWorkArea());
        insert.put(DataHelper.COLUMN_FIELD_PICTURE, model.getPicture());
        insert.put(DataHelper.COLUMN_FIELD_PICTURE_PATH, model.getPicturePath());
        insert.put(DataHelper.COLUMN_FIELD_RESPONSIBLE, model.getResponsible());
        insert.put(DataHelper.COLUMN_FIELD_DATE_COMPLETE, model.getDateComplete());
        insert.put(DataHelper.COLUMN_FIELD_INSPECTION_REF, referenceCode);
    }


    /* =========================
                  UG Drilling Inspection
    ========================= */
    private String[] allColumnsUGMain = {DataHelper.UG_ID,
            DataHelper.UG_DATE_INS,
            DataHelper.UG_PROJECT_NAME,
            DataHelper.UG_HOLE_ID,
            DataHelper.UG_PROJECT_OWNER,
            DataHelper.UG_AREA_OWNER,
            DataHelper.UG_DRILLING_COOR,
            DataHelper.UG_DRILLING_CONT,
            DataHelper.UG_GEOTECH,
            DataHelper.UG_HYDRO,
            DataHelper.UG_GEOLOGY,
            DataHelper.UG_SHE_GEO,
            DataHelper.UG_VENTILATION,
            DataHelper.UG_GD_TYPE,
            DataHelper.UG_GD_ID,
            DataHelper.UG_GD_LAST_CALIBRATION};

    public long insertUGDrillMain(final UGDrillingMainModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_DATE_INS, model.getDateIns());
        insert.put(DataHelper.UG_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_HOLE_ID, model.getHoleId());
        insert.put(DataHelper.UG_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_AREA_OWNER, model.getAreaOwner());
        insert.put(DataHelper.UG_DRILLING_COOR, model.getDrillingCoor());
        insert.put(DataHelper.UG_DRILLING_CONT, model.getDrillingCont());
        insert.put(DataHelper.UG_GEOTECH, model.getUgGeoTech());
        insert.put(DataHelper.UG_HYDRO, model.getUgHydro());
        insert.put(DataHelper.UG_GEOLOGY, model.getUgGeo());
        insert.put(DataHelper.UG_SHE_GEO, model.getUgShe());
        insert.put(DataHelper.UG_VENTILATION, model.getUgVentilation());
        insert.put(DataHelper.UG_GD_TYPE, model.getTypeGD());
        insert.put(DataHelper.UG_GD_ID, model.getIdGD());
        insert.put(DataHelper.UG_GD_LAST_CALIBRATION, model.getLastCalibGD());

        long insertData = database.insert(DataHelper.TABLE_UG_DRILLING_MAIN, null, insert);

        return insertData;
    }

    private UGDrillingMainModel cursortoUGDrillingMain(Cursor cursor) {
        UGDrillingMainModel model = new UGDrillingMainModel();
        model.setId(cursor.getLong(0));
        model.setDateIns(cursor.getString(1));
        model.setProjectName(cursor.getString(2));
        model.setHoleId(cursor.getString(3));
        model.setProjectOwner(cursor.getString(4));
        model.setAreaOwner(cursor.getString(5));
        model.setDrillingCoor(cursor.getString(6));
        model.setDrillingCont(cursor.getString(7));
        model.setUgGeoTech(cursor.getString(8));
        model.setUgHydro(cursor.getString(9));
        model.setUgGeo(cursor.getString(10));
        model.setUgShe(cursor.getString(11));
        model.setUgVentilation(cursor.getString(12));
        model.setTypeGD(cursor.getString(13));
        model.setIdGD(cursor.getString(14));
        model.setLastCalibGD(cursor.getString(15));

        return model;
    }

    public UGDrillingMainModel getLastestUGDrilling() {
        UGDrillingMainModel data = new UGDrillingMainModel();

        Cursor cursor = database.query(DataHelper.TABLE_UG_DRILLING_MAIN,
                allColumnsUGMain, DataHelper.UG_ID + " = (SELECT MAX(" + DataHelper.UG_ID + ") FROM "
                        + DataHelper.TABLE_UG_DRILLING_MAIN + ")", null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursortoUGDrillingMain(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public UGDrillingMainModel getDataUGReport(String projOwner, String projName, String dateIns, String holeId) {
        UGDrillingMainModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_DRILLING_MAIN + " WHERE " + DataHelper.UG_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_DATE_INS
                + " = '" + dateIns + "' AND " + DataHelper.UG_HOLE_ID + " = '" + holeId + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingMainModel data = cursortoUGDrillingMain(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                         UG Signs
   ========================== */
    public long insertSignUG(InspectorUGModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_UG_PROJ_OWNER, model.getProjOwner());
        insert.put(DataHelper.COLUMN_UG_PROJ_NAME, model.getProjName());
        insert.put(DataHelper.COLUMN_UG_DATE_INS, model.getDateIns());
        insert.put(DataHelper.COLUMN_UG_NAME, model.getName());
        insert.put(DataHelper.COLUMN_UG_ID, model.getIdMan());
        insert.put(DataHelper.COLUMN_UG_COMMENT, model.getComment());
        insert.put(DataHelper.COLUMN_UG_SIGN, Helper.bitmapToString(model.getSign()));
        insert.put(DataHelper.COLUMN_UG_DEPT, model.getDept());

        long insertData = database.insert(DataHelper.TABLE_UG_DRILLING_SIGN, null, insert);

        return insertData;
    }

    private InspectorUGModel cursortoUGInspectorSigns(Cursor cursor) {
        InspectorUGModel model = new InspectorUGModel();
        model.setId(cursor.getLong(0));
        model.setProjOwner(cursor.getString(1));
        model.setProjName(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setName(cursor.getString(4));
        model.setIdMan(cursor.getString(5));
        model.setComment(cursor.getString(6));
        model.setSign(Helper.stringToBitmap(cursor.getString(7)));
        model.setDept(cursor.getString(8));

        return model;
    }

    public InspectorUGModel getInspectorUG(final String projOwner, final String projName, final String dateIns, final String dept) {
        InspectorUGModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_DRILLING_SIGN + " WHERE " + DataHelper.COLUMN_UG_PROJ_OWNER
                + " = '" + projOwner + "' AND " + DataHelper.COLUMN_UG_PROJ_NAME + " = '" + projName + "' AND "
                + DataHelper.COLUMN_UG_DATE_INS + " = '" + dateIns + "' AND " + DataHelper.COLUMN_UG_DEPT + " = '" + dept + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            InspectorUGModel data = cursortoUGInspectorSigns(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;

    }

    /* ==========================
              UG Daily Pre-Start Check
  ========================== */
    public long insertUGDailyPre(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_DP_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_DP_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_DP_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_DP_INS1, model.getInspection1());
        insert.put(DataHelper.UG_DP_REM1, model.getRemark1());
        insert.put(DataHelper.UG_DP_INS2, model.getInspection2());
        insert.put(DataHelper.UG_DP_REM2, model.getRemark2());

        long insertData = database.insert(DataHelper.TABLE_UG_DAILY_PRECHECK, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGDailyPre(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        return model;
    }

    /*== barusan di komentt jangan lupa ganti lagi kalo ga bener ==*/
    public UGDrillingInspectionModel getDataPre(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_DAILY_PRECHECK + " WHERE " + DataHelper.UG_DP_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_DP_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_DP_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGDailyPre(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    // yg tadi di koment
//    public ArrayList<UGDrillingInspectionModel> getAllDataPre(String projOwner, String projName, String dateIns) {
//        ArrayList<UGDrillingInspectionModel> model = new ArrayList<UGDrillingInspectionModel>();
//
//        String query = "SELECT * FROM " + DataHelper.TABLE_UG_DAILY_PRECHECK + " WHERE " + DataHelper.UG_DP_PROJECT_NAME + " = '"
//                + projName + "' AND " + DataHelper.UG_DP_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_DP_PROJECT_DATE
//                + " = '" + dateIns + "'";
//
//        Cursor cursor = database.rawQuery(query, null);
//
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {
//            UGDrillingInspectionModel data = cursortoUGDailyPre(cursor);
//            model.add(data);
//            cursor.moveToNext();
//        }
//
//        // make sure to close the cursor
//        cursor.close();
//        return model;
//    }
// yg tadi dikoment
    /* ==========================
                         UG Electrical
   ========================== */
    public long insertUGElectrical(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_ELEC_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_ELEC_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_ELEC_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_ELEC_INS1, model.getInspection1());
        insert.put(DataHelper.UG_ELEC_REM1, model.getRemark1());
        insert.put(DataHelper.UG_ELEC_INS2, model.getInspection2());
        insert.put(DataHelper.UG_ELEC_REM2, model.getRemark2());
        insert.put(DataHelper.UG_ELEC_INS3, model.getInspection3());
        insert.put(DataHelper.UG_ELEC_REM3, model.getRemark3());
        insert.put(DataHelper.UG_ELEC_INS4, model.getInspection4());
        insert.put(DataHelper.UG_ELEC_REM4, model.getRemark4());
        insert.put(DataHelper.UG_ELEC_INS5, model.getInspection5());
        insert.put(DataHelper.UG_ELEC_REM5, model.getRemark5());
        insert.put(DataHelper.UG_ELEC_INS6, model.getInspection6());
        insert.put(DataHelper.UG_ELEC_REM6, model.getRemark6());
        insert.put(DataHelper.UG_ELEC_INS7, model.getInspection7());
        insert.put(DataHelper.UG_ELEC_REM7, model.getRemark7());

        long insertData = database.insert(DataHelper.TABLE_UG_ELECTRICAL, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGElectrical(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));

        return model;
    }

    public UGDrillingInspectionModel getDataElectrical(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_ELECTRICAL + " WHERE " + DataHelper.UG_ELEC_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_ELEC_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_ELEC_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGElectrical(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                  UG Safety Equipment
    ========================== */
    public long insertUGSafertyEquip(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_SE_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_SE_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_SE_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_SE_INS1, model.getInspection1());
        insert.put(DataHelper.UG_SE_REM1, model.getRemark1());
        insert.put(DataHelper.UG_SE_INS2, model.getInspection2());
        insert.put(DataHelper.UG_SE_REM2, model.getRemark2());
        insert.put(DataHelper.UG_SE_INS3, model.getInspection3());
        insert.put(DataHelper.UG_SE_REM3, model.getRemark3());
        insert.put(DataHelper.UG_SE_INS4, model.getInspection4());
        insert.put(DataHelper.UG_SE_REM4, model.getRemark4());
        insert.put(DataHelper.UG_SE_INS5, model.getInspection5());
        insert.put(DataHelper.UG_SE_REM5, model.getRemark5());
        insert.put(DataHelper.UG_SE_INS6, model.getInspection6());
        insert.put(DataHelper.UG_SE_REM6, model.getRemark6());
        insert.put(DataHelper.UG_SE_INS7, model.getInspection7());
        insert.put(DataHelper.UG_SE_REM7, model.getRemark7());
        insert.put(DataHelper.UG_SE_INS8, model.getInspection8());
        insert.put(DataHelper.UG_SE_REM8, model.getRemark8());
        insert.put(DataHelper.UG_SE_INS9, model.getInspection9());
        insert.put(DataHelper.UG_SE_REM9, model.getRemark9());
        insert.put(DataHelper.UG_SE_INS10, model.getInspection10());
        insert.put(DataHelper.UG_SE_REM10, model.getRemark10());

        long insertData = database.insert(DataHelper.TABLE_UG_SAFETY_EQUIP, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGSafetyEquipment(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));
        model.setInspection8(cursor.getString(18));
        model.setRemark8(cursor.getString(19));
        model.setInspection9(cursor.getString(20));
        model.setRemark9(cursor.getString(21));
        model.setInspection10(cursor.getString(22));
        model.setRemark10(cursor.getString(23));

        return model;
    }

    public UGDrillingInspectionModel getDataSafetyEquipment(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_SAFETY_EQUIP + " WHERE " + DataHelper.UG_SE_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_SE_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_SE_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGSafetyEquipment(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                            UG Signs
    ========================== */
    public long insertUGSigns(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_SI_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_SI_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_SI_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_SI_INS1, model.getInspection1());
        insert.put(DataHelper.UG_SI_REM1, model.getRemark1());
        insert.put(DataHelper.UG_SI_INS2, model.getInspection2());
        insert.put(DataHelper.UG_SI_REM2, model.getRemark2());
        insert.put(DataHelper.UG_SI_INS3, model.getInspection3());
        insert.put(DataHelper.UG_SI_REM3, model.getRemark3());
        insert.put(DataHelper.UG_SI_INS4, model.getInspection4());
        insert.put(DataHelper.UG_SI_REM4, model.getRemark4());
        insert.put(DataHelper.UG_SI_INS5, model.getInspection5());
        insert.put(DataHelper.UG_SI_REM5, model.getRemark5());
        insert.put(DataHelper.UG_SI_INS6, model.getInspection6());
        insert.put(DataHelper.UG_SI_REM6, model.getRemark6());
        insert.put(DataHelper.UG_SI_INS7, model.getInspection7());
        insert.put(DataHelper.UG_SI_REM7, model.getRemark7());

        long insertData = database.insert(DataHelper.TABLE_UG_SIGNS, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGSigns(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));

        return model;
    }

    public UGDrillingInspectionModel getDataSigns(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_SIGNS + " WHERE " + DataHelper.UG_SI_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_SI_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_SI_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGSigns(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                        UG Cleanliness
    ========================== */
    public long insertUGCleanliness(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_CL_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_CL_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_CL_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_CL_INS1, model.getInspection1());
        insert.put(DataHelper.UG_CL_REM1, model.getRemark1());
        insert.put(DataHelper.UG_CL_INS2, model.getInspection2());
        insert.put(DataHelper.UG_CL_REM2, model.getRemark2());
        insert.put(DataHelper.UG_CL_INS3, model.getInspection3());
        insert.put(DataHelper.UG_CL_REM3, model.getRemark3());
        insert.put(DataHelper.UG_CL_INS4, model.getInspection4());
        insert.put(DataHelper.UG_CL_REM4, model.getRemark4());
        insert.put(DataHelper.UG_CL_INS5, model.getInspection5());
        insert.put(DataHelper.UG_CL_REM5, model.getRemark5());
        insert.put(DataHelper.UG_CL_INS6, model.getInspection6());
        insert.put(DataHelper.UG_CL_REM6, model.getRemark6());
        insert.put(DataHelper.UG_CL_INS7, model.getInspection7());
        insert.put(DataHelper.UG_CL_REM7, model.getRemark7());
        insert.put(DataHelper.UG_CL_INS8, model.getInspection8());
        insert.put(DataHelper.UG_CL_REM8, model.getRemark8());
        insert.put(DataHelper.UG_CL_INS9, model.getInspection9());
        insert.put(DataHelper.UG_CL_REM9, model.getRemark9());
        insert.put(DataHelper.UG_CL_INS10, model.getInspection10());
        insert.put(DataHelper.UG_CL_REM10, model.getRemark10());

        long insertData = database.insert(DataHelper.TABLE_UG_CLEANLINESS, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGCleanliness(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));
        model.setInspection8(cursor.getString(18));
        model.setRemark8(cursor.getString(19));
        model.setInspection9(cursor.getString(20));
        model.setRemark9(cursor.getString(21));
        model.setInspection10(cursor.getString(22));
        model.setRemark10(cursor.getString(23));

        return model;
    }

    public UGDrillingInspectionModel getDataCleanliness(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_CLEANLINESS + " WHERE " + DataHelper.UG_CL_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_CL_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_CL_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGCleanliness(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                        UG Feed Frame
    ========================== */
    public long insertUGFeedFrame(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_FF_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_FF_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_FF_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_FF_INS1, model.getInspection1());
        insert.put(DataHelper.UG_FF_REM1, model.getRemark1());
        insert.put(DataHelper.UG_FF_INS2, model.getInspection2());
        insert.put(DataHelper.UG_FF_REM2, model.getRemark2());
        insert.put(DataHelper.UG_FF_INS3, model.getInspection3());
        insert.put(DataHelper.UG_FF_REM3, model.getRemark3());
        insert.put(DataHelper.UG_FF_INS4, model.getInspection4());
        insert.put(DataHelper.UG_FF_REM4, model.getRemark4());
        insert.put(DataHelper.UG_FF_INS5, model.getInspection5());
        insert.put(DataHelper.UG_FF_REM5, model.getRemark5());
        insert.put(DataHelper.UG_FF_INS6, model.getInspection6());
        insert.put(DataHelper.UG_FF_REM6, model.getRemark6());
        insert.put(DataHelper.UG_FF_INS7, model.getInspection7());
        insert.put(DataHelper.UG_FF_REM7, model.getRemark7());
        insert.put(DataHelper.UG_FF_INS8, model.getInspection8());
        insert.put(DataHelper.UG_FF_REM8, model.getRemark8());
        insert.put(DataHelper.UG_FF_INS9, model.getInspection9());
        insert.put(DataHelper.UG_FF_REM9, model.getRemark9());
        insert.put(DataHelper.UG_FF_INS10, model.getInspection10());
        insert.put(DataHelper.UG_FF_REM10, model.getRemark10());

        long insertData = database.insert(DataHelper.TABLE_UG_FEED_FRAME, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGFeedFrame(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));
        model.setInspection8(cursor.getString(18));
        model.setRemark8(cursor.getString(19));
        model.setInspection9(cursor.getString(20));
        model.setRemark9(cursor.getString(21));
        model.setInspection10(cursor.getString(22));
        model.setRemark10(cursor.getString(23));

        return model;
    }

    public UGDrillingInspectionModel getDataFeedFrame(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_FEED_FRAME + " WHERE " + DataHelper.UG_FF_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_FF_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_FF_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGFeedFrame(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                            UG Control
    ========================== */
    public long insertUGControl(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_CNT_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_CNT_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_CNT_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_CNT_INS1, model.getInspection1());
        insert.put(DataHelper.UG_CNT_REM1, model.getRemark1());
        insert.put(DataHelper.UG_CNT_INS2, model.getInspection2());
        insert.put(DataHelper.UG_CNT_REM2, model.getRemark2());
        insert.put(DataHelper.UG_CNT_INS3, model.getInspection3());
        insert.put(DataHelper.UG_CNT_REM3, model.getRemark3());

        long insertData = database.insert(DataHelper.TABLE_UG_CONTROL, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGControl(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));

        return model;
    }

    public UGDrillingInspectionModel getDataControl(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_CONTROL + " WHERE " + DataHelper.UG_CNT_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_CNT_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_CNT_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGControl(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }


    /* ==========================
                  UG Wireline Equipment
    ========================== */
    public long insertUGWireline(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_WE_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_WE_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_WE_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_WE_INS1, model.getInspection1());
        insert.put(DataHelper.UG_WE_REM1, model.getRemark1());
        insert.put(DataHelper.UG_WE_INS2, model.getInspection2());
        insert.put(DataHelper.UG_WE_REM2, model.getRemark2());
        insert.put(DataHelper.UG_WE_INS3, model.getInspection3());
        insert.put(DataHelper.UG_WE_REM3, model.getRemark3());
        insert.put(DataHelper.UG_WE_INS4, model.getInspection4());
        insert.put(DataHelper.UG_WE_REM4, model.getRemark4());

        long insertData = database.insert(DataHelper.TABLE_UG_WIREEQUIPMENT, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGWireline(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));

        return model;
    }

    public UGDrillingInspectionModel getDataWireline(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_WIREEQUIPMENT + " WHERE " + DataHelper.UG_WE_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_WE_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_WE_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGWireline(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                    UG Circulation System
    ========================== */
    public long insertUGCirculation(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_CS_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_CS_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_CS_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_CS_INS1, model.getInspection1());
        insert.put(DataHelper.UG_CS_REM1, model.getRemark1());
        insert.put(DataHelper.UG_CS_INS2, model.getInspection2());
        insert.put(DataHelper.UG_CS_REM2, model.getRemark2());
        insert.put(DataHelper.UG_CS_INS3, model.getInspection3());
        insert.put(DataHelper.UG_CS_REM3, model.getRemark3());
        insert.put(DataHelper.UG_CS_INS4, model.getInspection4());
        insert.put(DataHelper.UG_CS_REM4, model.getRemark4());
        insert.put(DataHelper.UG_CS_INS5, model.getInspection5());
        insert.put(DataHelper.UG_CS_REM5, model.getRemark5());
        insert.put(DataHelper.UG_CS_INS6, model.getInspection6());
        insert.put(DataHelper.UG_CS_REM6, model.getRemark6());
        insert.put(DataHelper.UG_CS_INS7, model.getInspection7());
        insert.put(DataHelper.UG_CS_REM7, model.getRemark7());

        long insertData = database.insert(DataHelper.TABLE_UG_CIRCULATION_SYSTEM, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGCirculation(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));

        return model;
    }

    public UGDrillingInspectionModel getDataCirculation(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_CIRCULATION_SYSTEM + " WHERE " + DataHelper.UG_CS_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_CS_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_CS_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGCirculation(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                      UG Control Panel
    ========================== */
    public long insertUGControlPanel(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_CP_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_CP_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_CP_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_CP_INS1, model.getInspection1());
        insert.put(DataHelper.UG_CP_REM1, model.getRemark1());
        insert.put(DataHelper.UG_CP_INS2, model.getInspection2());
        insert.put(DataHelper.UG_CP_REM2, model.getRemark2());
        insert.put(DataHelper.UG_CP_INS3, model.getInspection3());
        insert.put(DataHelper.UG_CP_REM3, model.getRemark3());
        insert.put(DataHelper.UG_CP_INS4, model.getInspection4());
        insert.put(DataHelper.UG_CP_REM4, model.getRemark4());
        insert.put(DataHelper.UG_CP_INS5, model.getInspection5());
        insert.put(DataHelper.UG_CP_REM5, model.getRemark5());
        insert.put(DataHelper.UG_CP_INS6, model.getInspection6());
        insert.put(DataHelper.UG_CP_REM6, model.getRemark6());
        insert.put(DataHelper.UG_CP_INS7, model.getInspection7());
        insert.put(DataHelper.UG_CP_REM7, model.getRemark7());
        insert.put(DataHelper.UG_CP_INS8, model.getInspection8());
        insert.put(DataHelper.UG_CP_REM8, model.getRemark8());
        insert.put(DataHelper.UG_CP_INS9, model.getInspection9());
        insert.put(DataHelper.UG_CP_REM9, model.getRemark9());

        long insertData = database.insert(DataHelper.TABLE_UG_POWER_PACK, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGControlPanel(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));
        model.setInspection6(cursor.getString(14));
        model.setRemark6(cursor.getString(15));
        model.setInspection7(cursor.getString(16));
        model.setRemark7(cursor.getString(17));
        model.setInspection8(cursor.getString(18));
        model.setRemark8(cursor.getString(19));
        model.setInspection9(cursor.getString(20));
        model.setRemark9(cursor.getString(21));

        return model;
    }

    public UGDrillingInspectionModel getDataControlPanel(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_POWER_PACK + " WHERE " + DataHelper.UG_CP_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_CP_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_CP_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGControlPanel(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                        UG Handtools
    ========================== */
    public long insertUGHandtools(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_HT_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_HT_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_HT_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_HT_INS1, model.getInspection1());
        insert.put(DataHelper.UG_HT_REM1, model.getRemark1());
        insert.put(DataHelper.UG_HT_INS2, model.getInspection2());
        insert.put(DataHelper.UG_HT_REM2, model.getRemark2());
        insert.put(DataHelper.UG_HT_INS3, model.getInspection3());
        insert.put(DataHelper.UG_HT_REM3, model.getRemark3());
        insert.put(DataHelper.UG_HT_INS4, model.getInspection4());
        insert.put(DataHelper.UG_HT_REM4, model.getRemark4());

        long insertData = database.insert(DataHelper.TABLE_UG_HANDTOOLS, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGHandtools(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));

        return model;
    }

    public UGDrillingInspectionModel getDataHandtools(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_HANDTOOLS + " WHERE " + DataHelper.UG_HT_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_HT_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_HT_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGHandtools(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                    UG Work Procedure
    ========================== */
    public long insertUGWorkProcedure(final UGDrillingInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.UG_WP_PROJECT_NAME, model.getProjectName());
        insert.put(DataHelper.UG_WP_PROJECT_OWNER, model.getProjectOwner());
        insert.put(DataHelper.UG_WP_PROJECT_DATE, model.getDateIns());
        insert.put(DataHelper.UG_WP_INS1, model.getInspection1());
        insert.put(DataHelper.UG_WP_REM1, model.getRemark1());
        insert.put(DataHelper.UG_WP_INS2, model.getInspection2());
        insert.put(DataHelper.UG_WP_REM2, model.getRemark2());
        insert.put(DataHelper.UG_WP_INS3, model.getInspection3());
        insert.put(DataHelper.UG_WP_REM3, model.getRemark3());
        insert.put(DataHelper.UG_WP_INS4, model.getInspection4());
        insert.put(DataHelper.UG_WP_REM4, model.getRemark4());
        insert.put(DataHelper.UG_WP_INS5, model.getInspection5());
        insert.put(DataHelper.UG_WP_REM5, model.getRemark5());

        long insertData = database.insert(DataHelper.TABLE_UG_WORK_PROCEDURE, null, insert);

        return insertData;
    }

    private UGDrillingInspectionModel cursortoUGWorkProcedure(Cursor cursor) {
        UGDrillingInspectionModel model = new UGDrillingInspectionModel();
        model.setId(cursor.getLong(0));
        model.setProjectName(cursor.getString(1));
        model.setProjectOwner(cursor.getString(2));
        model.setDateIns(cursor.getString(3));
        model.setInspection1(cursor.getString(4));
        model.setRemark1(cursor.getString(5));
        model.setInspection2(cursor.getString(6));
        model.setRemark2(cursor.getString(7));
        model.setInspection3(cursor.getString(8));
        model.setRemark3(cursor.getString(9));
        model.setInspection4(cursor.getString(10));
        model.setRemark4(cursor.getString(11));
        model.setInspection5(cursor.getString(12));
        model.setRemark5(cursor.getString(13));

        return model;
    }

    public UGDrillingInspectionModel getDataWorkProcedure(String projOwner, String projName, String dateIns) {
        UGDrillingInspectionModel model = null;

        String query = "SELECT * FROM " + DataHelper.TABLE_UG_WORK_PROCEDURE + " WHERE " + DataHelper.UG_WP_PROJECT_NAME + " = '"
                + projName + "' AND " + DataHelper.UG_WP_PROJECT_OWNER + " = '" + projOwner + "' AND " + DataHelper.UG_WP_PROJECT_DATE
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UGDrillingInspectionModel data = cursortoUGWorkProcedure(cursor);
            model = data;
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ==========================
                        LV Inspection
    ========================== */
    private String[] allColumnsLVMain = {DataHelper.COLUMN_LV_ID,
            DataHelper.COLUMN_LV_NO,
            DataHelper.COLUMN_LV_PIC,
            DataHelper.COLUMN_LV_ID_NO,
            DataHelper.COLUMN_LV_DEPT,
            DataHelper.COLUMN_LV_DATE,
            DataHelper.COLUMN_LV_NEXT_PM};

    public long insertLVInspection(final LVInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_LV_NO, model.getLvNo());
        insert.put(DataHelper.COLUMN_LV_PIC, model.getPic());
        insert.put(DataHelper.COLUMN_LV_ID_NO, model.getIdNo());
        insert.put(DataHelper.COLUMN_LV_DEPT, model.getDepartment());
        insert.put(DataHelper.COLUMN_LV_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_LV_NEXT_PM, model.getNextPM());

        long insertData = database.insert(DataHelper.TABLE_LV_INSPECTION, null, insert);

        return insertData;
    }

    private LVInspectionModel cursorToLVMain(Cursor cursor) {
        LVInspectionModel model = null;
        model.setId(cursor.getInt(0));
        model.setLvNo(cursor.getString(1));
        model.setPic(cursor.getString(2));
        model.setIdNo(cursor.getString(3));
        model.setDepartment(cursor.getString(4));
        model.setDate(cursor.getString(5));
        model.setNextPM(cursor.getString(6));

        return model;
    }

    public LVInspectionModel getLastestLVMain() {
        LVInspectionModel data = new LVInspectionModel();

        Cursor cursor = database.query(DataHelper.TABLE_LV_INSPECTION,
                allColumnsLVMain, DataHelper.COLUMN_LV_ID + " = (SELECT MAX(" + DataHelper.COLUMN_LV_ID + ") FROM "
                        + DataHelper.TABLE_LV_INSPECTION + ")", null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursorToLVMain(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return data;
    }

    /* ==========================
                        LV Daily
    ========================== */
    public long insertLVDaily(final LVDailyModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_LV_DAILY_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_LV_DAILY_TIME, model.getTime());
        insert.put(DataHelper.COLUMN_LV_DAILY_LV_NUMBER, model.getLvNo());
        insert.put(DataHelper.COLUMN_LV_DAILY_PIC, model.getPic());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP1, model.getInsp1());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP2, model.getInsp2());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP3, model.getInsp3());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP4, model.getInsp4());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP5, model.getInsp5());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP6, model.getInsp6());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP7, model.getInsp7());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP8, model.getInsp8());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP9, model.getInsp9());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP10, model.getInsp10());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP11, model.getInsp11());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP12, model.getInsp12());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP13, model.getInsp13());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP14, model.getInsp14());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP15, model.getInsp15());
        insert.put(DataHelper.COLUMN_LV_DAILY_INSP16, model.getInsp16());
        insert.put(DataHelper.COLUMN_LV_DAILY_DRIVER_ID, model.getDriverId());
        insert.put(DataHelper.COLUMN_LV_DAILY_SIGNATURE, model.getSignature());
        insert.put(DataHelper.COLUMN_LV_DAILY_DAT_INS, model.getDateIns());

        long insertData = database.insert(DataHelper.TABLE_LV_DAILY, null, insert);

        return insertData;
    }

    private LVDailyModel cursorToLVDaily(Cursor cursor) {
        LVDailyModel model = null;
        model.setId(cursor.getInt(0));
        model.setLvNo(cursor.getString(1));
        model.setPic(cursor.getString(2));
        model.setDate(cursor.getString(3));
        model.setTime(cursor.getString(4));
        model.setInsp1(cursor.getString(5));
        model.setInsp2(cursor.getString(6));
        model.setInsp3(cursor.getString(7));
        model.setInsp4(cursor.getString(8));
        model.setInsp5(cursor.getString(9));
        model.setInsp6(cursor.getString(10));
        model.setInsp7(cursor.getString(11));
        model.setInsp8(cursor.getString(12));
        model.setInsp9(cursor.getString(13));
        model.setInsp10(cursor.getString(14));
        model.setInsp11(cursor.getString(15));
        model.setInsp12(cursor.getString(16));
        model.setInsp13(cursor.getString(17));
        model.setInsp14(cursor.getString(18));
        model.setInsp15(cursor.getString(19));
        model.setInsp16(cursor.getString(20));
        model.setDriverId(cursor.getString(21));
        model.setSignature(cursor.getString(22));

        return model;
    }

    public ArrayList<LVDailyModel> getAllDataLVDaily(String lvNo, String pic, String dateIns) {
        ArrayList<LVDailyModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_LV_DAILY + " WHERE " + DataHelper.COLUMN_LV_DAILY_LV_NUMBER
                + " = '" + lvNo + "' AND " + DataHelper.COLUMN_LV_DAILY_PIC + " = '" + pic + "' AND " + DataHelper.COLUMN_LV_DAILY_DAT_INS
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            LVDailyModel data = cursorToLVDaily(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        return model;
    }

    /* ==========================
                        LV Weekly
    ========================== */
    public long insertLVWeekly(final LVWeeklyModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_LV_WEEKLY_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_LV_NUMBER, model.getLvNo());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_PIC, model.getPic());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_INSP1, model.getInsp1());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_INSP2, model.getInsp2());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_INSP3, model.getInsp3());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_INSP4, model.getInsp4());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_DRIVER_ID, model.getDriverId());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_ACTION, model.getSignature());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_DRIVER_ID, model.getSuperVisor());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_ACTION, model.getSignature1());
        insert.put(DataHelper.COLUMN_LV_WEEKLY_DATE_INS, model.getDateIns());

        long insertData = database.insert(DataHelper.TABLE_LV_WEEKLY, null, insert);

        return insertData;
    }

    private LVWeeklyModel cursorToLVWeekly(Cursor cursor) {
        LVWeeklyModel model = null;
        model.setId(cursor.getInt(0));
        model.setLvNo(cursor.getString(1));
        model.setPic(cursor.getString(2));
        model.setDate(cursor.getString(3));
        model.setInsp1(cursor.getString(4));
        model.setInsp2(cursor.getString(5));
        model.setInsp3(cursor.getString(6));
        model.setInsp4(cursor.getString(7));
        model.setDriverId(cursor.getString(8));
        model.setSignature(cursor.getString(9));
        model.setSuperVisor(cursor.getString(10));
        model.setSignature1(cursor.getString(11));

        return model;
    }

    public ArrayList<LVWeeklyModel> getAllDataLVWeekly(String lvNo, String pic, String dateIns) {
        ArrayList<LVWeeklyModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_LV_WEEKLY + " WHERE " + DataHelper.COLUMN_LV_WEEKLY_LV_NUMBER
                + " = '" + lvNo + "' AND " + DataHelper.COLUMN_LV_WEEKLY_PIC + " = '" + pic + "' AND " + DataHelper.COLUMN_LV_WEEKLY_DATE_INS
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            LVWeeklyModel data = cursorToLVWeekly(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        return model;
    }

    /* ==========================
                        LV Damage
    ========================== */
    public long insertLVDamage(final LVDamageModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_LV_DAMAGE_LV_NUMBER, model.getLvNo());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_PIC, model.getPic());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_ITEM, model.getItemDamage());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_DRIVER_ID, model.getDriverId());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_ACTION, model.getAction());
        insert.put(DataHelper.COLUMN_LV_DAMAGE_DATE_INS, model.getDateIns());

        long insertData = database.insert(DataHelper.TABLE_LV_DAMAGE, null, insert);

        return insertData;
    }

    private LVDamageModel cursorToLVDamage(Cursor cursor) {
        LVDamageModel model = null;
        model.setId(cursor.getInt(0));
        model.setLvNo(cursor.getString(1));
        model.setPic(cursor.getString(2));
        model.setItemDamage(cursor.getString(3));
        model.setDate(cursor.getString(6));
        model.setDriverId(cursor.getString(4));
        model.setAction(cursor.getString(5));

        return model;
    }

    public ArrayList<LVDamageModel> getAllDataLVDamage(String lvNo, String pic, String dateIns) {
        ArrayList<LVDamageModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_LV_DAMAGE + " WHERE " + DataHelper.COLUMN_LV_DAMAGE_LV_NUMBER
                + " = '" + lvNo + "' AND " + DataHelper.COLUMN_LV_DAMAGE_PIC + " = '" + pic + "' AND " + DataHelper.COLUMN_LV_DAMAGE_DATE_INS
                + " = '" + dateIns + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            LVDamageModel data = cursorToLVDamage(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        return model;
    }

    /* ==========================
                    LV Inspection Group
    ========================== */
    public long insertLVGroup(final LVGroupModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_GROUP_INSPECTOR, model.getInspector());
        insert.put(DataHelper.COLUMN_GROUP_DEPARTMENT, model.getDepartment());
        insert.put(DataHelper.COLUMN_GROUP_DATE, model.getDate());

        long insertData = database.insert(DataHelper.TABLE_LV_GROUP, null, insert);

        return insertData;
    }

    /* ===========================
               Planned SHE Inspection Main
    =========================== */

    private String[] allColumnsPlannedMain = {DataHelper.COLUMN_PLANNED_ID,
            DataHelper.COLUMN_PLANNED_DATE, DataHelper.COLUMN_PLANNED_INSPECTOR, DataHelper.COLUMN_PLANNED_INSPECTOR_ID, DataHelper.COLUMN_PLANNED_LOCATION,
            DataHelper.COLUMN_PLANNED_AREA_OWNER};


    public long insertPlannedMain(PlannedMainModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_PLANNED_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_PLANNED_INSPECTOR, model.getInspector());
        insert.put(DataHelper.COLUMN_PLANNED_INSPECTOR_ID, model.getInspectorID());
        insert.put(DataHelper.COLUMN_PLANNED_LOCATION, model.getLocation());
        insert.put(DataHelper.COLUMN_PLANNED_AREA_OWNER, model.getAreaOwner());

        long insertData = database.insert(DataHelper.TABLE_PLANNED_MAIN, null, insert);

        return insertData;
    }

    public PlannedMainModel cursorPlannedMain(Cursor cursor) {
        PlannedMainModel model = new PlannedMainModel();

        model.setId(cursor.getInt(0));
        model.setDate(cursor.getString(1));
        model.setInspector(cursor.getString(2));
        model.setInspectorID(cursor.getString(3));
        model.setLocation(cursor.getString(4));
        model.setAreaOwner(cursor.getString(5));

        return model;
    }

    public PlannedMainModel getLastestPlannedMain() {
        PlannedMainModel data = new PlannedMainModel();

        Cursor cursor = database.query(DataHelper.TABLE_PLANNED_MAIN,
                allColumnsPlannedMain, DataHelper.COLUMN_PLANNED_ID + " = (SELECT MAX(" + DataHelper.COLUMN_PLANNED_ID + ") FROM "
                        + DataHelper.TABLE_PLANNED_MAIN + ")", null, null, null, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            data = cursorPlannedMain(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<PlannedMainModel> getAllPlannedMain() {
        ArrayList<PlannedMainModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_MAIN;
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlannedMainModel data = cursorPlannedMain(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return model;
    }

    /* ===========================
          Planned SHE Inspection Signature
    =========================== */
    public long insertPlannedSign(PlannedSignModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_PLANNED_SIGN_INSPECTOR, model.getInspector());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN_AREA_OWNER, model.getAreaOwner());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN_LOCATION, model.getLocation());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN_DATE_INS, model.getDateIns());
        insert.put(DataHelper.COLUMN_PLANNED_NAME, model.getName());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN_IDNO, model.getIdNo());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN_DATE, model.getDate());
        insert.put(DataHelper.COLUMN_PLANNED_SIGN, Helper.bitmapToString(model.getSign()));
        insert.put(DataHelper.COLUMN_PLANNED_TITLE, model.getTitle());
        insert.put(DataHelper.COLUMN_PLANNED_CODE, model.getCode());

        long insertData = database.insert(DataHelper.TABLE_PLANNED_SIGN, null, insert);

        return insertData;
    }

    public PlannedSignModel cursorPlannedSign(Cursor cursor) {
        PlannedSignModel model = new PlannedSignModel();

        model.setId(cursor.getInt(0));
        model.setInspector(cursor.getString(1));
        model.setAreaOwner(cursor.getString(2));
        model.setLocation(cursor.getString(3));
        model.setDateIns(cursor.getString(4));
        model.setName(cursor.getString(5));
        model.setIdNo(cursor.getString(6));
        model.setDate(cursor.getString(7));
        model.setSign(Helper.stringToBitmap(cursor.getString(8)));
        model.setTitle(cursor.getString(9));
        model.setCode(cursor.getString(10));

        return model;
    }

    public ArrayList<PlannedSignModel> getAllPlannedSign(final String inspector, final String areaOwner, final String location, final String date) {
        ArrayList<PlannedSignModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_SIGN + " WHERE " + DataHelper.COLUMN_PLANNED_SIGN_INSPECTOR
                + " = '" + inspector + "' AND " + DataHelper.COLUMN_PLANNED_SIGN_AREA_OWNER + " = '" + areaOwner + "' AND "
                + DataHelper.COLUMN_PLANNED_SIGN_LOCATION + " = '" + location + "' AND " + DataHelper.COLUMN_PLANNED_SIGN_DATE_INS
                + " = '" + date + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlannedSignModel data = cursorPlannedSign(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        cursor.close();
        return model;
    }

    public PlannedSignModel getAllPlannedSign(final String inspector, final String areaOwner,
                                              final String location, final String date, final String code) {
        PlannedSignModel model = null;
        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_SIGN + " WHERE " + DataHelper.COLUMN_PLANNED_SIGN_INSPECTOR
                + " = '" + inspector + "' AND " + DataHelper.COLUMN_PLANNED_SIGN_AREA_OWNER + " = '" + areaOwner + "' AND "
                + DataHelper.COLUMN_PLANNED_SIGN_LOCATION + " = '" + location + "' AND " + DataHelper.COLUMN_PLANNED_SIGN_DATE_INS + " = '"
                + date + "' AND " + DataHelper.COLUMN_PLANNED_CODE + " = '" + code + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            model = cursorPlannedSign(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return model;

    }

    /* ===========================
           Planned SHE Inspection Findings
   =========================== */
    public ArrayList<PlannedFindingsModel> checkTableFindings() {
        ArrayList<PlannedFindingsModel> data = new ArrayList<PlannedFindingsModel>();

        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_FINDING;
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlannedFindingsModel model = cursorPlannedFindings(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return data;
    }

    public long insertPlannedFindings(PlannedFindingsModel model, long mainId) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COLUMN_PLANNED_FINDING_MAIN_ID, mainId);
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_FINDINGS, model.getFindings());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_COMMENTS, model.getComment());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_RISKS, model.getRisk());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_RESPONSIBLE, model.getResponsible());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_DATE, model.getDateIns());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_PHOTO, model.getPhotoPath());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_CODE, model.getCode());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_QUESTION_CODE, model.getQuestionCode());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_INSPECTION_CODE, model.getInspectionCode());

        long insertData = database.insert(DataHelper.TABLE_PLANNED_FINDING, null, insert);

        return insertData;
    }

    public PlannedFindingsModel cursorPlannedFindings(Cursor cursor) {
        PlannedFindingsModel model = new PlannedFindingsModel();

        model.setId(cursor.getInt(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_ID)));
        model.setPlannedMainId(cursor.getLong(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_MAIN_ID)));
        model.setDateIns(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_DATE)));
        model.setFindings(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_FINDINGS)));
        model.setComment(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_COMMENTS)));
        model.setRisk(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_RISKS)));
        model.setResponsible(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_RESPONSIBLE)));
        model.setPhotoPath(cursor.getString(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_PHOTO)));
        model.setCode(cursor.getInt(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_CODE)));
        model.setQuestionCode(cursor.getInt(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_QUESTION_CODE)));
        model.setInspectionCode(cursor.getInt(cursor.getColumnIndex(DataHelper.COLUMN_PLANNED_FINDING_INSPECTION_CODE)));

        return model;
    }

    public ArrayList<PlannedFindingsModel> getAllPlannedFindings(final long plannedMainId) {
        ArrayList<PlannedFindingsModel> model = new ArrayList<>();

        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_FINDING + " WHERE "
                + DataHelper.COLUMN_PLANNED_FINDING_MAIN_ID
                + " = '" + plannedMainId + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PlannedFindingsModel data = cursorPlannedFindings(cursor);
            model.add(data);
            cursor.moveToNext();
        }

        cursor.close();
        return model;
    }

    public void updatePlannedFindings(ContentValues insert, PlannedFindingsModel model) {
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_FINDINGS, model.getFindings());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_COMMENTS, model.getComment());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_RISKS, model.getRisk());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_RESPONSIBLE, model.getResponsible());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_DATE, model.getDateIns());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_PHOTO, model.getPhotoPath());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_CODE, model.getCode());
        insert.put(DataHelper.COLUMN_PLANNED_FINDING_QUESTION_CODE, model.getQuestionCode());
    }

    public String getLastPlannedFindingCode() {
        String inspectionCode = "";
        String query = "SELECT " + DataHelper.COLUMN_PLANNED_FINDING_INSPECTION_CODE + " FROM " + DataHelper.TABLE_PLANNED_FINDING;

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            inspectionCode = cursor.getString(0);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return inspectionCode;

    }

    /* ===========================
          Planned SHE Inspection Inspection
    =========================== */
    public long insertPlannedInspection(PlannedInspectionModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.PLANNED_INSPECTION_MAIN_ID, model.getMainId());
        insert.put(DataHelper.PLANNED_INSPECTION_INSPECTOR, model.getInspector());
        insert.put(DataHelper.PLANNED_INSPECTION_AREA_OWNER, model.getAreaOwner());
        insert.put(DataHelper.PLANNED_INSPECTION_LOCATION, model.getLocation());
        insert.put(DataHelper.PLANNED_INSPECTION_DATE, model.getDate());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK1, model.getRisk1());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING1, model.getRating1());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK2, model.getRisk2());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING2, model.getRating2());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK3, model.getRisk3());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING3, model.getRating3());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK4, model.getRisk4());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING4, model.getRating4());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK5, model.getRisk5());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING5, model.getRating5());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK6, model.getRisk6());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING6, model.getRating6());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK7, model.getRisk7());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING7, model.getRating7());
        insert.put(DataHelper.PLANNED_INSPECTION_RISK8, model.getRisk8());
        insert.put(DataHelper.PLANNED_INSPECTION_RATING8, model.getRating8());
        insert.put(DataHelper.PLANNED_INSPECTION_CODE, model.getCode());

        long insertData = database.insert(DataHelper.TABLE_PLANNED_INSPECTION, null, insert);

        return insertData;
    }

    public PlannedInspectionModel cursorPlannedInspection(Cursor cursor) {
        PlannedInspectionModel model = new PlannedInspectionModel();

        model.setId(cursor.getLong(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_ID)));
        model.setMainId(cursor.getLong(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_MAIN_ID)));
        model.setInspector(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_INSPECTOR)));
        model.setAreaOwner(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_AREA_OWNER)));
        model.setLocation(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_LOCATION)));
        model.setDate(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_DATE)));
        model.setRisk1(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK1)));
        model.setRating1(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING1)));
        model.setRisk2(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK2)));
        model.setRating2(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING2)));
        model.setRisk3(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK3)));
        model.setRating3(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING3)));
        model.setRisk4(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK4)));
        model.setRating4(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING4)));
        model.setRisk5(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK5)));
        model.setRating5(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING5)));
        model.setRisk6(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK6)));
        model.setRating6(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING6)));
        model.setRisk7(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK7)));
        model.setRating7(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING7)));
        model.setRisk8(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RISK8)));
        model.setRating8(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_RATING8)));
        model.setCode(cursor.getString(cursor.getColumnIndex(DataHelper.PLANNED_INSPECTION_CODE)));

        return model;
    }

    public PlannedInspectionModel getLastDataInspection(final long mainId, final String code) {
        PlannedInspectionModel model = null;
        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_INSPECTION + " WHERE "
                + DataHelper.PLANNED_INSPECTION_MAIN_ID + " = '" + mainId + "' AND "
                + DataHelper.PLANNED_INSPECTION_CODE + " = '" + code + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            model = cursorPlannedInspection(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return model;

    }

    public PlannedInspectionModel getLastDataInspection(final String inspector, final String areaOwner,
                                                        final String location, final String date, final String code) {
        PlannedInspectionModel model = null;
        String query = "SELECT * FROM " + DataHelper.TABLE_PLANNED_INSPECTION + " WHERE " + DataHelper.PLANNED_INSPECTION_INSPECTOR
                + " = '" + inspector + "' AND " + DataHelper.PLANNED_INSPECTION_AREA_OWNER + " = '" + areaOwner + "' AND "
                + DataHelper.PLANNED_INSPECTION_LOCATION + " = '" + location + "' AND " + DataHelper.PLANNED_INSPECTION_DATE + " = '"
                + date + "' AND " + DataHelper.PLANNED_INSPECTION_CODE + " = '" + code + "'";

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            model = cursorPlannedInspection(cursor);
            cursor.moveToNext();
        }

        // make sure to close the cursor
        cursor.close();
        return model;

    }

}

