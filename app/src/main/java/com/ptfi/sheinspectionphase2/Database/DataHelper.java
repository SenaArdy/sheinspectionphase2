package com.ptfi.sheinspectionphase2.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ptfi.sheinspectionphase2.Utils.Constants;

/**
 * Created by senaardyputra on 11/10/16.
 */

public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "SheInspection2.db";
    private static final int DATABASE_VERSION = 14;

    /* ==========================
                         Grassberg Drilling Main
    ========================== */
    public static final String TABLE_GRS_DRILL = "grs_drill";
    public static final String COLUMN_GRS_DRILL_ID = "grs_drill_id";
    public static final String COLUMN_GRS_DRILL_PIC = "grs_drill_pic";
    public static final String COLUMN_GRS_DRILL_PIC_ID = "grs_drill_pic_id";
    public static final String COLUMN_GRS_DRILL_DATE = "grs_drill_date";

    private static final String DATABASE_GRS_DRILLING = "create table "
            + TABLE_GRS_DRILL + "(" + COLUMN_GRS_DRILL_ID
            + " integer primary key autoincrement, " + COLUMN_GRS_DRILL_PIC
            + " text, " + COLUMN_GRS_DRILL_PIC_ID + " text, "
            + COLUMN_GRS_DRILL_DATE + " text " + ");";


    /* ========================
                       Grassberg Drilling Detail
    ======================== */
    public static final String TABLE_GRS_DRILL_DETAIL = "grs_drill_detail";
    public static final String COLUMN_GRS_DRILL_DETAIL_ID = "grs_drill_detail_id";
    public static final String COLUMN_GRS_DRILL_DETAIL_PIC = "grs_drill_detail_pic";
    public static final String COLUMN_GRS_DRILL_DETAIL_DATE_INS = "grs_drill_detail_date_ins";
    public static final String COLUMN_GRS_DRILL_DETAIL_DEPARTMENT = "grs_drill_department";
    public static final String COLUMN_GRS_DRILL_DETAIL_COMMENT = "grs_drill_detail_comment";
    public static final String COLUMN_GRS_DRILL_DETAIL_DATE = "grs_drill_detail_date";
    public static final String COLUMN_GRS_DRILL_DETAIL_SIGNER = "grs_drill_detail_signer";
    public static final String COLUMN_GRS_DRILL_DETAIL_SIGN = "grs_drill_detail_sign";

    private static final String DATABASE_GRS_DRILLING_DETAIL = "create table "
            + TABLE_GRS_DRILL_DETAIL + "(" + COLUMN_GRS_DRILL_DETAIL_ID
            + " integer primary key autoincrement, "
            + COLUMN_GRS_DRILL_DETAIL_PIC + " text, "
            + COLUMN_GRS_DRILL_DETAIL_DATE_INS + " text, "
            + COLUMN_GRS_DRILL_DETAIL_COMMENT + " text, "
            + COLUMN_GRS_DRILL_DETAIL_DATE + " text,"
            + COLUMN_GRS_DRILL_DETAIL_SIGN + " text,"
            + COLUMN_GRS_DRILL_DETAIL_SIGNER + " text,"
            + COLUMN_GRS_DRILL_DETAIL_DEPARTMENT + " text " + ");";

    /* ======================
                              Photo Table
    ====================== */
    public static final String TABLE_PHOTO = "photo_inspection";
    public static final String COLUMN_PHOTO_ID = "_id";
    public static final String COLUMN_PHOTO_ROW = "photo_row";
    public static final String COLUMN_PHOTO_INSPECTION_CODE = "photo_inspection_code";
    public static final String COLUMN_PHOTO_REFERENCE = "inspection_ref";
    public static final String COLUMN_PHOTO_PATH = "photo_path";
    public static final String COLUMN_PHOTO_COMMENT = "photo_comment";
    public static final String COLUMN_PHOTO_DATE = "photo_date";
    public static final String COLUMN_PHOTO_PIC = "photo_pic";


    private static final String DATABASE_PHOTO_CREATE = "create table "
            + TABLE_PHOTO + "(" + COLUMN_PHOTO_ID
            + " integer primary key autoincrement, "
            + COLUMN_PHOTO_INSPECTION_CODE + " integer , "
            + COLUMN_PHOTO_REFERENCE + " integer , " + COLUMN_PHOTO_ROW
            + " integer , " + COLUMN_PHOTO_PATH + " text , "
            + COLUMN_PHOTO_COMMENT + " text , " + COLUMN_PHOTO_DATE + " text , "
            + COLUMN_PHOTO_PIC + " text " + ");";

    /* ======================
                     Observer & Data Look Up
    ====================== */

    public static final String TABLE_OBSERVER = "observer";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_OBS_ID = "observer_id";
    public static final String COLUMN_OBS_NAME = "observer_name";
    public static final String COLUMN_OBS_TITLE = "observer_title";
    public static final String COLUMN_OBS_ORG = "observer_org";
    public static final String COLUMN_OBS_COMP = "observer_comp";
    public static final String COLUMN_OBS_DIVISI = "observer_division";
    public static final String COLUMN_OBS_DEPT = "observer_dept";
    public static final String COLUMN_OBS_SECTION = "observer_section";

    private static final String DATABASE_OBSERVER_CREATE = "create table "
            + TABLE_OBSERVER + "(" + COLUMN_ID
            + " integer primary key autoincrement , " + COLUMN_OBS_ID
            + " text NOT NULL," + COLUMN_OBS_NAME + " text , "
            + COLUMN_OBS_TITLE + " text , " + COLUMN_OBS_ORG + " text , "
            + COLUMN_OBS_COMP + " text , " + COLUMN_OBS_DIVISI + " text , "
            + COLUMN_OBS_DEPT + " text , " + COLUMN_OBS_SECTION + " text " + ");";

    // COMPANY
    public static final String TABLE_COMPANY = "company";
    public static final String COL_LOOKUP = "lookup";
    public static final String COL_DESC = "description";

    private static final String DATABASE_COMPANY_CREATE = "create table "
            + TABLE_COMPANY + "(" + COLUMN_ID
            + " integer primary key autoincrement," + COL_LOOKUP
            + " text NOT NULL," + COL_DESC + " text" + ");";

    // Look Up Table
    public static final String TABLE_LOOKUP = "lookup";
    public static final String COL_POTETIAL_RISK = "potential_risk";
    public static final String COL_RATING = "rating";

    private static final String DATABASE_LOOKUP_CREATE = "create table "
            + TABLE_LOOKUP + "(" + COLUMN_ID
            + " integer primary key autoincrement , " + COL_POTETIAL_RISK
            + " text NOT NULL , " + COL_RATING + " text " + ");";

    // BUSINESS UNIT
    public static final String TABLE_BUSINESS_UNIT = "business_unit";
    public static final String COL_REF = "refference";

    private static final String DATABASE_BUSINESS_UNIT_CREATE = "create table "
            + TABLE_BUSINESS_UNIT + "(" + COLUMN_ID
            + " integer primary key autoincrement , " + COL_LOOKUP
            + " text NOT NULL," + COL_DESC + " text , " + COL_REF + " text"
            + ");";

    // DEPARTMENT
    public static final String TABLE_DEPARTMENT = "department";

    private static final String DATABASE_DEPARTMENT_CREATE = "create table "
            + TABLE_DEPARTMENT + "(" + COLUMN_ID
            + " integer primary key autoincrement," + COL_LOOKUP
            + " text NOT NULL," + COL_DESC + " text ," + COL_REF + " text"
            + ");";

    // SECTION
    public static final String TABLE_SECTION = "section";

    private static final String DATABASE_SECTION_CREATE = "create table "
            + TABLE_SECTION + "(" + COLUMN_ID
            + " integer primary key autoincrement , " + COL_LOOKUP
            + " text NOT NULL," + COL_DESC + " text , " + COL_REF + " text"
            + ");";

    // POTENTIAL RISK
    public static final String TABLE_POTENTIAL_RISK = "potential_risk";
    public static final String COL_VAL = "vals";

    private static final String DATABASE_POTENTIAL_RISK_CREATE = "create table "
            + TABLE_POTENTIAL_RISK
            + "("
            + COLUMN_ID
            + " integer primary key autoincrement , "
            + COL_VAL
            + " text NOT NULL" + ");";

    // TABLE AVAILABILITY
    public static final String TABLE_AVAILABILITY = "availability";
    public static final String COLUMN_AVAILABILITY_ID = "_id";
    public static final String COLUMN_AVAILABILITY_NAME = "availability_name";

    private static final String DATABASE_AVAILABILITY_CREATE = "create table "
            + TABLE_AVAILABILITY + "(" + COLUMN_AVAILABILITY_ID
            + " integer primary key autoincrement," + COLUMN_AVAILABILITY_NAME
            + " text NOT NULL" + ");";

    // RATING
    public static final String TABLE_RATING = "rating";

    private static final String DATABASE_RATING_CREATE = "create table "
            + TABLE_RATING + "(" + COLUMN_ID
            + " integer primary key autoincrement , " + COL_VAL
            + " text NOT NULL" + ");";

    /* ======================
                          Field Inspection
    ====================== */
    public static final String TABLE_FIELD_INSPECTION = "field_inspection";
    public static final String COLUMN_FIELD_ID = "_id";
    public static final String COLUMN_FIELD_COMPANY = "company";
    public static final String COLUMN_FIELD_BUSINESS = "business";
    public static final String COLUMN_FIELD_DEPARTMENT = "department";
    public static final String COLUMN_FIELD_SECTION = "section";
    public static final String COLUMN_FIELD_DATE = "date";
    public static final String COLUMN_FIELD_INSPECTOR = "inspector";
    public static final String COLUMN_FIELD_WORKAREA = "work_area";

    private static final String DATABASE_FIELD_INSPECTION = "create table "
            + TABLE_FIELD_INSPECTION + "(" + COLUMN_FIELD_ID
            + " integer primary key autoincrement , " + COLUMN_FIELD_COMPANY
            + " text , " + COLUMN_FIELD_BUSINESS + " text , "
            + COLUMN_FIELD_DEPARTMENT + " text , " + COLUMN_FIELD_SECTION
            + " text , " + COLUMN_FIELD_DATE + " text , "
            + COLUMN_FIELD_INSPECTOR + " text , " + COLUMN_FIELD_WORKAREA
            + " text " + ");";

    /* ======================
                     Findings Field Inspection
    ====================== */
    public static final String TABLE_FIELD_INSPECTION_FINDING = "field_inspection_finding";
    public static final String COLUMN_FIELD_FINDING_ID = "_id";
    public static final String COLUMN_FIELD_FINDING_MAIN_ID = "main_id";
    public static final String COLUMN_FIELD_FINDING = "finding";
    public static final String COLUMN_FIELD_ACTION = "action";
    public static final String COLUMN_FIELD_WORK_AREA = "work_area";
    public static final String COLUMN_FIELD_PICTURE = "picture";
    public static final String COLUMN_FIELD_PICTURE_PATH = "picture_path";
    public static final String COLUMN_FIELD_RESPONSIBLE = "responsible";
    public static final String COLUMN_FIELD_DATE_COMPLETE = "date_complete";
    public static final String COLUMN_FIELD_INSPECTION_REF = "field_ref";

    private static final String DATABASE_FIELD_INSPECTION_FINDING = "create table "
            + TABLE_FIELD_INSPECTION_FINDING
            + "("
            + COLUMN_FIELD_FINDING_ID
            + " integer primary key autoincrement , "
            + COLUMN_FIELD_FINDING_MAIN_ID
            + " integer, "
            + COLUMN_FIELD_FINDING
            + " text , "
            + COLUMN_FIELD_ACTION
            + " text , "
            + COLUMN_FIELD_WORK_AREA
            + " text , "
            + COLUMN_FIELD_PICTURE
            + " text , "
            + COLUMN_FIELD_PICTURE_PATH
            + " text , "
            + COLUMN_FIELD_RESPONSIBLE
            + " text , "
            + COLUMN_FIELD_DATE_COMPLETE
            + " text, "
            + COLUMN_FIELD_INSPECTION_REF + " integer " + ");";

    /* ==========================
                   UG Drilling Main
   ========================== */
    public static final String TABLE_UG_DRILLING_MAIN = "ug_drilling_main";
    public static final String UG_ID = "_id";
    public static final String UG_DATE_INS = "date_inspection";
    public static final String UG_PROJECT_NAME = "project_name";
    public static final String UG_HOLE_ID = "hole_id";
    public static final String UG_PROJECT_OWNER = "project_owner";
    public static final String UG_AREA_OWNER = "area_owner";
    public static final String UG_DRILLING_COOR = "drilling_coordinator";
    public static final String UG_DRILLING_CONT = "drilling_contractor";
    public static final String UG_GEOTECH = "ug_geotech";
    public static final String UG_HYDRO = "ug_hydrology";
    public static final String UG_GEOLOGY = "ug_geology";
    public static final String UG_SHE_GEO = "ug_she_geoservices";
    public static final String UG_VENTILATION = "ug_ventilation";
    public static final String UG_GD_TYPE = "gas_detector_type";
    public static final String UG_GD_ID = "gas_detector_id";
    public static final String UG_GD_LAST_CALIBRATION = "gas_detector_last_calibration";

    private static final String DATABASE_UG_DRILLING_MAIN = "create table "
            + TABLE_UG_DRILLING_MAIN
            + "("
            + UG_ID
            + " integer primary key autoincrement , "
            + UG_DATE_INS
            + " text , "
            + UG_PROJECT_NAME
            + " text , "
            + UG_HOLE_ID
            + " text , "
            + UG_PROJECT_OWNER
            + " text , "
            + UG_AREA_OWNER
            + " text , "
            + UG_DRILLING_COOR
            + " text , "
            + UG_DRILLING_CONT
            + " text , "
            + UG_GEOTECH
            + " text , "
            + UG_HYDRO
            + " text , "
            + UG_GEOLOGY
            + " text , "
            + UG_SHE_GEO
            + " text , "
            + UG_VENTILATION
            + " text , "
            + UG_GD_TYPE
            + " text , "
            + UG_GD_ID
            + " text , "
            + UG_GD_LAST_CALIBRATION
            + " text " + ");";

    /* ==========================
                   UG Drilling Sign
   ========================== */
    public static final String TABLE_UG_DRILLING_SIGN = "ug_drilling_sign";
    public static final String COLUMN_UG_DRILLING_ID = "_ID";
    public static final String COLUMN_UG_PROJ_OWNER = "ug_drilling_owner";
    public static final String COLUMN_UG_PROJ_NAME = "ug_drilling_name";
    public static final String COLUMN_UG_DATE_INS = "ug_drilling_date";
    public static final String COLUMN_UG_NAME = "ug_name";
    public static final String COLUMN_UG_ID = "ug_id";
    public static final String COLUMN_UG_COMMENT = "ug_comment";
    public static final String COLUMN_UG_SIGN = "ug_sign";
    public static final String COLUMN_UG_DEPT = "ug_dept";

    public static final String DATABASE_DRILLING_SIGN = "create table " + TABLE_UG_DRILLING_SIGN + "("
            + COLUMN_UG_DRILLING_ID + " integer primary key autoincrement , " + COLUMN_UG_PROJ_OWNER + " text , "
            + COLUMN_UG_PROJ_NAME + " text , " + COLUMN_UG_DATE_INS + " text , "
            + COLUMN_UG_NAME + " text , "
            + COLUMN_UG_ID + " text , "
            + COLUMN_UG_COMMENT + " text , "
            + COLUMN_UG_SIGN + " text , "
            + COLUMN_UG_DEPT + " text " + ");";


    /* ==========================
                UG Daily Pre-Start Check
    ========================== */
    public static final String TABLE_UG_DAILY_PRECHECK = "ug_daily_precheck";
    public static final String UG_DP_ID = "_id";
    public static final String UG_DP_PROJECT_NAME = "project_name";
    public static final String UG_DP_PROJECT_OWNER = "project_owner";
    public static final String UG_DP_PROJECT_DATE = "date";
    public static final String UG_DP_INS1 = "inspection1";
    public static final String UG_DP_REM1 = "remark1";
    public static final String UG_DP_INS2 = "inspection2";
    public static final String UG_DP_REM2 = "remark2";

    private static final String DATABASE_UG_DAILY_PRECHECK = "create table "
            + TABLE_UG_DAILY_PRECHECK
            + "("
            + UG_DP_ID
            + " integer primary key autoincrement, "
            + UG_DP_PROJECT_NAME
            + " text , "
            + UG_DP_PROJECT_OWNER
            + " text , "
            + UG_DP_PROJECT_DATE
            + " text , "
            + UG_DP_INS1
            + " text , "
            + UG_DP_REM1
            + " text , "
            + UG_DP_INS2
            + " text , "
            + UG_DP_REM2
            + " text " + ");";

    /* ==========================
                          UG Electrical
    ========================== */
    public static final String TABLE_UG_ELECTRICAL = "ug_electrical";
    public static final String UG_ELEC_ID = "_id";
    public static final String UG_ELEC_PROJECT_NAME = "project_name";
    public static final String UG_ELEC_PROJECT_OWNER = "project_owner";
    public static final String UG_ELEC_PROJECT_DATE = "date";
    public static final String UG_ELEC_INS1 = "inspection1";
    public static final String UG_ELEC_REM1 = "remark1";
    public static final String UG_ELEC_INS2 = "inspection2";
    public static final String UG_ELEC_REM2 = "remark2";
    public static final String UG_ELEC_INS3 = "inspection3";
    public static final String UG_ELEC_REM3 = "remark3";
    public static final String UG_ELEC_INS4 = "inspection4";
    public static final String UG_ELEC_REM4 = "remark4";
    public static final String UG_ELEC_INS5 = "inspection5";
    public static final String UG_ELEC_REM5 = "remark5";
    public static final String UG_ELEC_INS6 = "inspection6";
    public static final String UG_ELEC_REM6 = "remark6";
    public static final String UG_ELEC_INS7 = "inspection7";
    public static final String UG_ELEC_REM7 = "remark7";

    private static final String DATABASE_UG_ELECTRICAL = "create table "
            + TABLE_UG_ELECTRICAL
            + "("
            + UG_ELEC_ID
            + " integer primary key autoincrement, "
            + UG_ELEC_PROJECT_NAME
            + " text , "
            + UG_ELEC_PROJECT_OWNER
            + " text , "
            + UG_ELEC_PROJECT_DATE
            + " text , "
            + UG_ELEC_INS1
            + " text , "
            + UG_ELEC_REM1
            + " text , "
            + UG_ELEC_INS2
            + " text , "
            + UG_ELEC_REM2
            + " text , "
            + UG_ELEC_INS3
            + " text , "
            + UG_ELEC_REM3
            + " text , "
            + UG_ELEC_INS4
            + " text , "
            + UG_ELEC_REM4
            + " text , "
            + UG_ELEC_INS5
            + " text , "
            + UG_ELEC_REM5
            + " text , "
            + UG_ELEC_INS6
            + " text , "
            + UG_ELEC_REM6
            + " text , "
            + UG_ELEC_INS7
            + " text , "
            + UG_ELEC_REM7
            + " text " + ");";

    /* ==========================
                    UG Safety Equipment
   ========================== */
    public static final String TABLE_UG_SAFETY_EQUIP = "ug_saferty_equipment";
    public static final String UG_SE_ID = "_id";
    public static final String UG_SE_PROJECT_NAME = "project_name";
    public static final String UG_SE_PROJECT_OWNER = "project_owner";
    public static final String UG_SE_PROJECT_DATE = "date";
    public static final String UG_SE_INS1 = "inspection1";
    public static final String UG_SE_REM1 = "remark1";
    public static final String UG_SE_INS2 = "inspection2";
    public static final String UG_SE_REM2 = "remark2";
    public static final String UG_SE_INS3 = "inspection3";
    public static final String UG_SE_REM3 = "remark3";
    public static final String UG_SE_INS4 = "inspection4";
    public static final String UG_SE_REM4 = "remark4";
    public static final String UG_SE_INS5 = "inspection5";
    public static final String UG_SE_REM5 = "remark5";
    public static final String UG_SE_INS6 = "inspection6";
    public static final String UG_SE_REM6 = "remark6";
    public static final String UG_SE_INS7 = "inspection7";
    public static final String UG_SE_REM7 = "remark7";
    public static final String UG_SE_INS8 = "inspection8";
    public static final String UG_SE_REM8 = "remark8";
    public static final String UG_SE_INS9 = "inspection9";
    public static final String UG_SE_REM9 = "remark9";
    public static final String UG_SE_INS10 = "inspection10";
    public static final String UG_SE_REM10 = "remark10";

    private static final String DATABASE_UG_SAFETY_EQUIP = "create table "
            + TABLE_UG_SAFETY_EQUIP
            + "("
            + UG_SE_ID
            + " integer primary key autoincrement, "
            + UG_SE_PROJECT_NAME
            + " text , "
            + UG_SE_PROJECT_OWNER
            + " text , "
            + UG_SE_PROJECT_DATE
            + " text , "
            + UG_SE_INS1
            + " text , "
            + UG_SE_REM1
            + " text , "
            + UG_SE_INS2
            + " text , "
            + UG_SE_REM2
            + " text , "
            + UG_SE_INS3
            + " text , "
            + UG_SE_REM3
            + " text , "
            + UG_SE_INS4
            + " text , "
            + UG_SE_REM4
            + " text , "
            + UG_SE_INS5
            + " text , "
            + UG_SE_REM5
            + " text , "
            + UG_SE_INS6
            + " text , "
            + UG_SE_REM6
            + " text , "
            + UG_SE_INS7
            + " text , "
            + UG_SE_REM7
            + " text , "
            + UG_SE_INS8
            + " text , "
            + UG_SE_REM8
            + " text , "
            + UG_SE_INS9
            + " text , "
            + UG_SE_REM9
            + " text , "
            + UG_SE_INS10
            + " text , "
            + UG_SE_REM10
            + " text " + ");";

    /* ==========================
                            UG Signs
   ========================== */
    public static final String TABLE_UG_SIGNS = "ug_signs";
    public static final String UG_SI_ID = "_id";
    public static final String UG_SI_PROJECT_NAME = "project_name";
    public static final String UG_SI_PROJECT_OWNER = "project_owner";
    public static final String UG_SI_PROJECT_DATE = "date";
    public static final String UG_SI_INS1 = "inspection1";
    public static final String UG_SI_REM1 = "remark1";
    public static final String UG_SI_INS2 = "inspection2";
    public static final String UG_SI_REM2 = "remark2";
    public static final String UG_SI_INS3 = "inspection3";
    public static final String UG_SI_REM3 = "remark3";
    public static final String UG_SI_INS4 = "inspection4";
    public static final String UG_SI_REM4 = "remark4";
    public static final String UG_SI_INS5 = "inspection5";
    public static final String UG_SI_REM5 = "remark5";
    public static final String UG_SI_INS6 = "inspection6";
    public static final String UG_SI_REM6 = "remark6";
    public static final String UG_SI_INS7 = "inspection7";
    public static final String UG_SI_REM7 = "remark7";

    private static final String DATABASE_UG_SIGNS = "create table "
            + TABLE_UG_SIGNS
            + "("
            + UG_SI_ID
            + " integer primary key autoincrement, "
            + UG_SI_PROJECT_NAME
            + " text , "
            + UG_SI_PROJECT_OWNER
            + " text , "
            + UG_SI_PROJECT_DATE
            + " text , "
            + UG_SI_INS1
            + " text , "
            + UG_SI_REM1
            + " text , "
            + UG_SI_INS2
            + " text , "
            + UG_SI_REM2
            + " text , "
            + UG_SI_INS3
            + " text , "
            + UG_SI_REM3
            + " text , "
            + UG_SI_INS4
            + " text , "
            + UG_SI_REM4
            + " text , "
            + UG_SI_INS5
            + " text , "
            + UG_SI_REM5
            + " text , "
            + UG_SI_INS6
            + " text , "
            + UG_SI_REM6
            + " text , "
            + UG_SI_INS7
            + " text , "
            + UG_SI_REM7
            + " text " + ");";

    /* ==========================
                        UG Cleanliness
   ========================== */
    public static final String TABLE_UG_CLEANLINESS = "ug_cleanliness";
    public static final String UG_CL_ID = "_id";
    public static final String UG_CL_PROJECT_NAME = "project_name";
    public static final String UG_CL_PROJECT_OWNER = "project_owner";
    public static final String UG_CL_PROJECT_DATE = "date";
    public static final String UG_CL_INS1 = "inspection1";
    public static final String UG_CL_REM1 = "remark1";
    public static final String UG_CL_INS2 = "inspection2";
    public static final String UG_CL_REM2 = "remark2";
    public static final String UG_CL_INS3 = "inspection3";
    public static final String UG_CL_REM3 = "remark3";
    public static final String UG_CL_INS4 = "inspection4";
    public static final String UG_CL_REM4 = "remark4";
    public static final String UG_CL_INS5 = "inspection5";
    public static final String UG_CL_REM5 = "remark5";
    public static final String UG_CL_INS6 = "inspection6";
    public static final String UG_CL_REM6 = "remark6";
    public static final String UG_CL_INS7 = "inspection7";
    public static final String UG_CL_REM7 = "remark7";
    public static final String UG_CL_INS8 = "inspection8";
    public static final String UG_CL_REM8 = "remark8";
    public static final String UG_CL_INS9 = "inspection9";
    public static final String UG_CL_REM9 = "remarK9";
    public static final String UG_CL_INS10 = "inspection10";
    public static final String UG_CL_REM10 = "remark10";

    private static final String DATABASE_UG_CLEANLINESS = "create table "
            + TABLE_UG_CLEANLINESS
            + "("
            + UG_CL_ID
            + " integer primary key autoincrement, "
            + UG_CL_PROJECT_NAME
            + " text , "
            + UG_CL_PROJECT_OWNER
            + " text , "
            + UG_CL_PROJECT_DATE
            + " text , "
            + UG_CL_INS1
            + " text , "
            + UG_CL_REM1
            + " text , "
            + UG_CL_INS2
            + " text , "
            + UG_CL_REM2
            + " text , "
            + UG_CL_INS3
            + " text , "
            + UG_CL_REM3
            + " text , "
            + UG_CL_INS4
            + " text , "
            + UG_CL_REM4
            + " text , "
            + UG_CL_INS5
            + " text , "
            + UG_CL_REM5
            + " text , "
            + UG_CL_INS6
            + " text , "
            + UG_CL_REM6
            + " text , "
            + UG_CL_INS7
            + " text , "
            + UG_CL_REM7
            + " text , "
            + UG_CL_INS8
            + " text , "
            + UG_CL_REM8
            + " text , "
            + UG_CL_INS9
            + " text , "
            + UG_CL_REM9
            + " text , "
            + UG_CL_INS10
            + " text , "
            + UG_CL_REM10
            + " text " + ");";

    /* ==========================
                       UG Feed Frame
   ========================== */
    public static final String TABLE_UG_FEED_FRAME = "ug_feed_frame";
    public static final String UG_FF_ID = "_id";
    public static final String UG_FF_PROJECT_NAME = "project_name";
    public static final String UG_FF_PROJECT_OWNER = "project_owner";
    public static final String UG_FF_PROJECT_DATE = "date";
    public static final String UG_FF_INS1 = "inspection1";
    public static final String UG_FF_REM1 = "remark1";
    public static final String UG_FF_INS2 = "inspection2";
    public static final String UG_FF_REM2 = "remark2";
    public static final String UG_FF_INS3 = "inspection3";
    public static final String UG_FF_REM3 = "remark3";
    public static final String UG_FF_INS4 = "inspection4";
    public static final String UG_FF_REM4 = "remark4";
    public static final String UG_FF_INS5 = "inspection5";
    public static final String UG_FF_REM5 = "remark5";
    public static final String UG_FF_INS6 = "inspection6";
    public static final String UG_FF_REM6 = "remark6";
    public static final String UG_FF_INS7 = "inspection7";
    public static final String UG_FF_REM7 = "remark7";
    public static final String UG_FF_INS8 = "inspection8";
    public static final String UG_FF_REM8 = "remark8";
    public static final String UG_FF_INS9 = "inspection9";
    public static final String UG_FF_REM9 = "remark9";
    public static final String UG_FF_INS10 = "inspection10";
    public static final String UG_FF_REM10 = "remark10";

    private static final String DATABASE_UG_FEED_FRAME = "create table "
            + TABLE_UG_FEED_FRAME
            + "("
            + UG_FF_ID
            + " integer primary key autoincrement, "
            + UG_FF_PROJECT_NAME
            + " text , "
            + UG_FF_PROJECT_OWNER
            + " text , "
            + UG_FF_PROJECT_DATE
            + " text , "
            + UG_FF_INS1
            + " text , "
            + UG_FF_REM1
            + " text , "
            + UG_FF_INS2
            + " text , "
            + UG_FF_REM2
            + " text , "
            + UG_FF_INS3
            + " text , "
            + UG_FF_REM3
            + " text , "
            + UG_FF_INS4
            + " text , "
            + UG_FF_REM4
            + " text , "
            + UG_FF_INS5
            + " text , "
            + UG_FF_REM5
            + " text , "
            + UG_FF_INS6
            + " text , "
            + UG_FF_REM6
            + " text , "
            + UG_FF_INS7
            + " text , "
            + UG_FF_REM7
            + " text , "
            + UG_FF_INS8
            + " text , "
            + UG_FF_REM8
            + " text , "
            + UG_FF_INS9
            + " text , "
            + UG_FF_REM9
            + " text , "
            + UG_FF_INS10
            + " text , "
            + UG_FF_REM10
            + " text " + ");";

    /* ==========================
                          UG Control
   ========================== */
    public static final String TABLE_UG_CONTROL = "ug_control";
    public static final String UG_CNT_ID = "_id";
    public static final String UG_CNT_PROJECT_NAME = "project_name";
    public static final String UG_CNT_PROJECT_OWNER = "project_owner";
    public static final String UG_CNT_PROJECT_DATE = "date";
    public static final String UG_CNT_INS1 = "inspection1";
    public static final String UG_CNT_REM1 = "remark1";
    public static final String UG_CNT_INS2 = "inspection2";
    public static final String UG_CNT_REM2 = "remark2";
    public static final String UG_CNT_INS3 = "inspection3";
    public static final String UG_CNT_REM3 = "remark3";

    private static final String DATABASE_UG_CONTROL = "create table "
            + TABLE_UG_CONTROL
            + "("
            + UG_CNT_ID
            + " integer primary key autoincrement, "
            + UG_CNT_PROJECT_NAME
            + " text , "
            + UG_CNT_PROJECT_OWNER
            + " text , "
            + UG_CNT_PROJECT_DATE
            + " text , "
            + UG_CNT_INS1
            + " text , "
            + UG_CNT_REM1
            + " text , "
            + UG_CNT_INS2
            + " text , "
            + UG_CNT_REM2
            + " text , "
            + UG_CNT_INS3
            + " text , "
            + UG_CNT_REM3
            + " text " + ");";

    /* ==========================
                  UG Wireline Equipment
   ========================== */
    public static final String TABLE_UG_WIREEQUIPMENT = "ug_wire_equipment";
    public static final String UG_WE_ID = "_id";
    public static final String UG_WE_PROJECT_NAME = "project_name";
    public static final String UG_WE_PROJECT_OWNER = "project_owner";
    public static final String UG_WE_PROJECT_DATE = "date";
    public static final String UG_WE_INS1 = "inspection1";
    public static final String UG_WE_REM1 = "remark1";
    public static final String UG_WE_INS2 = "inspection2";
    public static final String UG_WE_REM2 = "remark2";
    public static final String UG_WE_INS3 = "inspection3";
    public static final String UG_WE_REM3 = "remark3";
    public static final String UG_WE_INS4 = "inspection4";
    public static final String UG_WE_REM4 = "remark4";

    private static final String DATABASE_UG_WIREEQUIPMENT = "create table "
            + TABLE_UG_WIREEQUIPMENT
            + "("
            + UG_WE_ID
            + " integer primary key autoincrement, "
            + UG_WE_PROJECT_NAME
            + " text , "
            + UG_WE_PROJECT_OWNER
            + " text , "
            + UG_WE_PROJECT_DATE
            + " text , "
            + UG_WE_INS1
            + " text , "
            + UG_WE_REM1
            + " text , "
            + UG_WE_INS2
            + " text , "
            + UG_WE_REM2
            + " text , "
            + UG_WE_INS3
            + " text , "
            + UG_WE_REM3
            + " text , "
            + UG_WE_INS4
            + " text , "
            + UG_WE_REM4
            + " text " + ");";

    /* ==========================
                   UG Circulation System
   ========================== */
    public static final String TABLE_UG_CIRCULATION_SYSTEM = "ug_circulation_system";
    public static final String UG_CS_ID = "_id";
    public static final String UG_CS_PROJECT_NAME = "project_name";
    public static final String UG_CS_PROJECT_OWNER = "project_owner";
    public static final String UG_CS_PROJECT_DATE = "date";
    public static final String UG_CS_INS1 = "inspection1";
    public static final String UG_CS_REM1 = "remark1";
    public static final String UG_CS_INS2 = "inspection2";
    public static final String UG_CS_REM2 = "remark2";
    public static final String UG_CS_INS3 = "inspection3";
    public static final String UG_CS_REM3 = "remark3";
    public static final String UG_CS_INS4 = "inspection4";
    public static final String UG_CS_REM4 = "remark4";
    public static final String UG_CS_INS5 = "inspection5";
    public static final String UG_CS_REM5 = "remark5";
    public static final String UG_CS_INS6 = "inspection6";
    public static final String UG_CS_REM6 = "remark6";
    public static final String UG_CS_INS7 = "inspection7";
    public static final String UG_CS_REM7 = "remark7";

    private static final String DATABASE_UG_CIRCULATION_SYSTEM = "create table "
            + TABLE_UG_CIRCULATION_SYSTEM
            + "("
            + UG_CS_ID
            + " integer primary key autoincrement, "
            + UG_CS_PROJECT_NAME
            + " text , "
            + UG_CS_PROJECT_OWNER
            + " text , "
            + UG_CS_PROJECT_DATE
            + " text , "
            + UG_CS_INS1
            + " text , "
            + UG_CS_REM1
            + " text , "
            + UG_CS_INS2
            + " text , "
            + UG_CS_REM2
            + " text , "
            + UG_CS_INS3
            + " text , "
            + UG_CS_REM3
            + " text , "
            + UG_CS_INS4
            + " text , "
            + UG_CS_REM4
            + " text , "
            + UG_CS_INS5
            + " text , "
            + UG_CS_REM5
            + " text , "
            + UG_CS_INS6
            + " text , "
            + UG_CS_REM6
            + " text , "
            + UG_CS_INS7
            + " text , "
            + UG_CS_REM7
            + " text " + ");";

    /* ==========================
            UG Power Pack - Control Panel
   ========================== */
    public static final String TABLE_UG_POWER_PACK = "ug_power_pack";
    public static final String UG_CP_ID = "_id";
    public static final String UG_CP_PROJECT_NAME = "project_name";
    public static final String UG_CP_PROJECT_OWNER = "project_owner";
    public static final String UG_CP_PROJECT_DATE = "date";
    public static final String UG_CP_INS1 = "inspection1";
    public static final String UG_CP_REM1 = "remark1";
    public static final String UG_CP_INS2 = "inspection2";
    public static final String UG_CP_REM2 = "remark2";
    public static final String UG_CP_INS3 = "inspection3";
    public static final String UG_CP_REM3 = "remark3";
    public static final String UG_CP_INS4 = "inspection4";
    public static final String UG_CP_REM4 = "remark4";
    public static final String UG_CP_INS5 = "inspection5";
    public static final String UG_CP_REM5 = "remark5";
    public static final String UG_CP_INS6 = "inspection6";
    public static final String UG_CP_REM6 = "remark6";
    public static final String UG_CP_INS7 = "inspection7";
    public static final String UG_CP_REM7 = "remark7";
    public static final String UG_CP_INS8 = "inspection8";
    public static final String UG_CP_REM8 = "remark8";
    public static final String UG_CP_INS9 = "inspection9";
    public static final String UG_CP_REM9 = "remark9";

    private static final String DATABASE_UG_POWER_PACK = "create table "
            + TABLE_UG_POWER_PACK
            + "("
            + UG_CP_ID
            + " integer primary key autoincrement, "
            + UG_CP_PROJECT_NAME
            + " text , "
            + UG_CP_PROJECT_OWNER
            + " text , "
            + UG_CP_PROJECT_DATE
            + " text , "
            + UG_CP_INS1
            + " text , "
            + UG_CP_REM1
            + " text , "
            + UG_CP_INS2
            + " text , "
            + UG_CP_REM2
            + " text , "
            + UG_CP_INS3
            + " text , "
            + UG_CP_REM3
            + " text , "
            + UG_CP_INS4
            + " text , "
            + UG_CP_REM4
            + " text , "
            + UG_CP_INS5
            + " text , "
            + UG_CP_REM5
            + " text , "
            + UG_CP_INS6
            + " text , "
            + UG_CP_REM6
            + " text , "
            + UG_CP_INS7
            + " text , "
            + UG_CP_REM7
            + " text , "
            + UG_CP_INS8
            + " text , "
            + UG_CP_REM8
            + " text , "
            + UG_CP_INS9
            + " text , "
            + UG_CP_REM9
            + " text " + ");";

    /* ==========================
                       UG Hand Tools
   ========================== */
    public static final String TABLE_UG_HANDTOOLS = "ug_handtools";
    public static final String UG_HT_ID = "_id";
    public static final String UG_HT_PROJECT_NAME = "project_name";
    public static final String UG_HT_PROJECT_OWNER = "project_owner";
    public static final String UG_HT_PROJECT_DATE = "date";
    public static final String UG_HT_INS1 = "inspection1";
    public static final String UG_HT_REM1 = "remark1";
    public static final String UG_HT_INS2 = "inspection2";
    public static final String UG_HT_REM2 = "remark2";
    public static final String UG_HT_INS3 = "inspection3";
    public static final String UG_HT_REM3 = "remark3";
    public static final String UG_HT_INS4 = "inspection4";
    public static final String UG_HT_REM4 = "remark4";

    private static final String DATABASE_UG_HANDTOOLS = "create table "
            + TABLE_UG_HANDTOOLS
            + "("
            + UG_HT_ID
            + " integer primary key autoincrement, "
            + UG_HT_PROJECT_NAME
            + " text , "
            + UG_HT_PROJECT_OWNER
            + " text , "
            + UG_HT_PROJECT_DATE
            + " text , "
            + UG_HT_INS1
            + " text , "
            + UG_HT_REM1
            + " text , "
            + UG_HT_INS2
            + " text , "
            + UG_HT_REM2
            + " text , "
            + UG_HT_INS3
            + " text , "
            + UG_HT_REM3
            + " text , "
            + UG_HT_INS4
            + " text , "
            + UG_HT_REM4
            + " text " + ");";

    /* ==========================
                     UG Work Procedure
   ========================== */
    public static final String TABLE_UG_WORK_PROCEDURE = "ug_work_procedure";
    public static final String UG_WP_ID = "_id";
    public static final String UG_WP_PROJECT_NAME = "project_name";
    public static final String UG_WP_PROJECT_OWNER = "project_owner";
    public static final String UG_WP_PROJECT_DATE = "date";
    public static final String UG_WP_INS1 = "inspection1";
    public static final String UG_WP_REM1 = "remark1";
    public static final String UG_WP_INS2 = "inspection2";
    public static final String UG_WP_REM2 = "remark2";
    public static final String UG_WP_INS3 = "inspection3";
    public static final String UG_WP_REM3 = "remark3";
    public static final String UG_WP_INS4 = "inspection4";
    public static final String UG_WP_REM4 = "remark4";
    public static final String UG_WP_INS5 = "inspection5";
    public static final String UG_WP_REM5 = "remark5";

    private static final String DATABASE_UG_WORK_PROCEDURE = "create table "
            + TABLE_UG_WORK_PROCEDURE
            + "("
            + UG_WP_ID
            + " integer primary key autoincrement, "
            + UG_WP_PROJECT_NAME
            + " text , "
            + UG_WP_PROJECT_OWNER
            + " text , "
            + UG_WP_PROJECT_DATE
            + " text , "
            + UG_WP_INS1
            + " text , "
            + UG_WP_REM1
            + " text , "
            + UG_WP_INS2
            + " text , "
            + UG_WP_REM2
            + " text , "
            + UG_WP_INS3
            + " text , "
            + UG_WP_REM3
            + " text , "
            + UG_WP_INS4
            + " text , "
            + UG_WP_REM4
            + " text , "
            + UG_WP_INS5
            + " text , "
            + UG_WP_REM5
            + " text " + ");";

    /* ==========================
                        LV Inspection
   ========================== */
    public static final String TABLE_LV_INSPECTION = "lv_inspection";
    public static final String COLUMN_LV_ID = "_id";
    public static final String COLUMN_LV_NO = "lv_no";
    public static final String COLUMN_LV_PIC = "pic";
    public static final String COLUMN_LV_ID_NO = "idno";
    public static final String COLUMN_LV_DEPT = "department";
    public static final String COLUMN_LV_DATE = "date";
    public static final String COLUMN_LV_NEXT_PM = "next_pm";

    public static final String DATABASE_LV_INSPECTION = "create table " + TABLE_LV_INSPECTION + "("
            + COLUMN_LV_ID + " integer primary key autoincrement, "
            + COLUMN_LV_NO + " text , "
            + COLUMN_LV_PIC + " text , "
            + COLUMN_LV_ID_NO + " text , "
            + COLUMN_LV_DEPT + " text , "
            + COLUMN_LV_DATE + " text , "
            + COLUMN_LV_NEXT_PM + " text " + ");";

    /* ==========================
                     LV Inspection Daily
   ========================== */
    public static final String TABLE_LV_DAILY = "lv_dialy";
    public static final String COLUMN_LV_DAILY_ID = "_id";
    public static final String COLUMN_LV_DAILY_LV_NUMBER = "lv_number";
    public static final String COLUMN_LV_DAILY_PIC = "pic";
    public static final String COLUMN_LV_DAILY_DATE = "date";
    public static final String COLUMN_LV_DAILY_TIME = "time";
    public static final String COLUMN_LV_DAILY_INSP1 = "insp1";
    public static final String COLUMN_LV_DAILY_INSP2 = "insp2";
    public static final String COLUMN_LV_DAILY_INSP3 = "insp3";
    public static final String COLUMN_LV_DAILY_INSP4 = "insp4";
    public static final String COLUMN_LV_DAILY_INSP5 = "insp5";
    public static final String COLUMN_LV_DAILY_INSP6 = "insp6";
    public static final String COLUMN_LV_DAILY_INSP7 = "insp7";
    public static final String COLUMN_LV_DAILY_INSP8 = "insp8";
    public static final String COLUMN_LV_DAILY_INSP9 = "insp9";
    public static final String COLUMN_LV_DAILY_INSP10 = "insp10";
    public static final String COLUMN_LV_DAILY_INSP11 = "insp11";
    public static final String COLUMN_LV_DAILY_INSP12 = "insp12";
    public static final String COLUMN_LV_DAILY_INSP13 = "insp13";
    public static final String COLUMN_LV_DAILY_INSP14 = "insp14";
    public static final String COLUMN_LV_DAILY_INSP15 = "insp15";
    public static final String COLUMN_LV_DAILY_INSP16 = "insp16";
    public static final String COLUMN_LV_DAILY_DRIVER_ID = "driver_id";
    public static final String COLUMN_LV_DAILY_SIGNATURE = "signature";
    public static final String COLUMN_LV_DAILY_DAT_INS = "date_ins";

    public static final String DATABASE_LV_DAILY = "create table " + TABLE_LV_DAILY
            + "(" + COLUMN_LV_DAILY_ID + " integer primary key autoincrement , "
            + COLUMN_LV_DAILY_LV_NUMBER + " text , "
            + COLUMN_LV_DAILY_PIC + " text , "
            + COLUMN_LV_DAILY_DATE + " text , "
            + COLUMN_LV_DAILY_TIME + " text , "
            + COLUMN_LV_DAILY_INSP1 + " text "
            + COLUMN_LV_DAILY_INSP2 + " text , "
            + COLUMN_LV_DAILY_INSP3 + " text , "
            + COLUMN_LV_DAILY_INSP4 + " text , "
            + COLUMN_LV_DAILY_INSP5 + " text , "
            + COLUMN_LV_DAILY_INSP6 + " text , "
            + COLUMN_LV_DAILY_INSP7 + " text , "
            + COLUMN_LV_DAILY_INSP8 + " text , "
            + COLUMN_LV_DAILY_INSP9 + " text , "
            + COLUMN_LV_DAILY_INSP10 + " text , "
            + COLUMN_LV_DAILY_INSP11 + " text , "
            + COLUMN_LV_DAILY_INSP12 + " text , "
            + COLUMN_LV_DAILY_INSP13 + " text , "
            + COLUMN_LV_DAILY_INSP14 + " text , "
            + COLUMN_LV_DAILY_INSP15 + " text , "
            + COLUMN_LV_DAILY_INSP16 + " text , "
            + COLUMN_LV_DAILY_DRIVER_ID + " text , "
            + COLUMN_LV_DAILY_SIGNATURE + " text , "
            + COLUMN_LV_DAILY_DAT_INS + " text " + ");";

    /* ==========================
                    LV Inspection Weekly
   ========================== */
    public static final String TABLE_LV_WEEKLY = "lv_weekly";
    public static final String COLUMN_LV_WEEKLY_ID = "_id";
    public static final String COLUMN_LV_WEEKLY_LV_NUMBER = "lv_number";
    public static final String COLUMN_LV_WEEKLY_PIC = "pic";
    public static final String COLUMN_LV_WEEKLY_DATE = "date";
    public static final String COLUMN_LV_WEEKLY_INSP1 = "insp1";
    public static final String COLUMN_LV_WEEKLY_INSP2 = "insp2";
    public static final String COLUMN_LV_WEEKLY_INSP3 = "insp3";
    public static final String COLUMN_LV_WEEKLY_INSP4 = "insp4";
    public static final String COLUMN_LV_WEEKLY_DRIVER_ID = "driver_id";
    public static final String COLUMN_LV_WEEKLY_SIGNATURE = "signature";
    public static final String COLUMN_LV_WEEKLY_SUPERVISOR = "supervisor";
    public static final String COLUMN_LV_WEEKLY_SINGATURE1 = "signature_supervisor";
    public static final String COLUMN_LV_WEEKLY_DATE_INS = "date_ins";

    public static final String DATABASE_LV_WEEKLY = "create table " + TABLE_LV_WEEKLY
            + "(" + COLUMN_LV_WEEKLY_ID + " integer primary key autoincrement , "
            + COLUMN_LV_WEEKLY_LV_NUMBER + " text , "
            + COLUMN_LV_WEEKLY_PIC + " text , "
            + COLUMN_LV_WEEKLY_DATE + " text , "
            + COLUMN_LV_WEEKLY_INSP1 + " text , "
            + COLUMN_LV_WEEKLY_INSP2 + " text "
            + COLUMN_LV_WEEKLY_INSP3 + " text , "
            + COLUMN_LV_WEEKLY_INSP4 + " text , "
            + COLUMN_LV_WEEKLY_DRIVER_ID + " text , "
            + COLUMN_LV_WEEKLY_SIGNATURE + " text , "
            + COLUMN_LV_WEEKLY_SUPERVISOR + " text , "
            + COLUMN_LV_WEEKLY_SINGATURE1 + " text , "
            + COLUMN_LV_WEEKLY_DATE_INS + " text " + ");";

    /* ==========================
                  LV Inspection Damage
   ========================== */
    public static final String TABLE_LV_DAMAGE = "lv_damage";
    public static final String COLUMN_LV_DAMAGE_ID = "_id";
    public static final String COLUMN_LV_DAMAGE_LV_NUMBER = "lv_number";
    public static final String COLUMN_LV_DAMAGE_PIC = "pic";
    public static final String COLUMN_LV_DAMAGE_ITEM = "item";
    public static final String COLUMN_LV_DAMAGE_DRIVER_ID = "driver_id";
    public static final String COLUMN_LV_DAMAGE_DATE = "date";
    public static final String COLUMN_LV_DAMAGE_ACTION = "action";
    public static final String COLUMN_LV_DAMAGE_DATE_INS = "date_ins";

    public static final String DATABASE_LV_DAMAGE = "create table " + TABLE_LV_DAMAGE
            + "(" + COLUMN_LV_DAMAGE_ID + " integer primary key autoincrement , "
            + COLUMN_LV_DAMAGE_LV_NUMBER + " text , "
            + COLUMN_LV_DAMAGE_PIC + " text , "
            + COLUMN_LV_DAMAGE_ITEM + " text , "
            + COLUMN_LV_DAMAGE_DRIVER_ID + " text , "
            + COLUMN_LV_DAMAGE_ACTION + " text , "
            + COLUMN_LV_DAMAGE_DATE + " text , "
            + COLUMN_LV_DAMAGE_DATE_INS + " text " + ");";

    /* ==========================
                  LV Inspection Group
   ========================== */
    public static final String TABLE_LV_GROUP = "lv_group";
    public static final String COLUMN_GROUP_ID = "_id";
    public static final String COLUMN_GROUP_INSPECTOR = "inspector";
    public static final String COLUMN_GROUP_DEPARTMENT = "department";
    public static final String COLUMN_GROUP_DATE = "date";

    public static final String DATABASE_LV_GROUP = "create table " + TABLE_LV_GROUP + "("
            + COLUMN_GROUP_ID + " integer primary key autoincrement , "
            + COLUMN_GROUP_INSPECTOR + " text , "
            + COLUMN_GROUP_DEPARTMENT + " text , "
            + COLUMN_GROUP_DATE + " text " + ");";

    /* ==========================
            Planned SHE Inspection Main
  ========================== */
    public static final String TABLE_PLANNED_MAIN = "planned_main";
    public static final String COLUMN_PLANNED_ID = "_id";
    public static final String COLUMN_PLANNED_DATE = "planned_date";
    public static final String COLUMN_PLANNED_INSPECTOR = "planned_inspector";
    public static final String COLUMN_PLANNED_INSPECTOR_ID = "planned_inspector_id";
    public static final String COLUMN_PLANNED_LOCATION = "planned_location";
    public static final String COLUMN_PLANNED_AREA_OWNER = "planned_area_owner";

    public static final String DATABASE_PLANNED_MAIN = "create table " + TABLE_PLANNED_MAIN + "("
            + COLUMN_PLANNED_ID + " integer primary key autoincrement , " + COLUMN_PLANNED_DATE + " text , "
            + COLUMN_PLANNED_INSPECTOR + " text , "
            + COLUMN_PLANNED_INSPECTOR_ID + " text , "
            + COLUMN_PLANNED_LOCATION + " text , "
            + COLUMN_PLANNED_AREA_OWNER + " text " + ");";

    /* ==========================
        Planned SHE Inspection Signature
 ========================== */
    public static final String TABLE_PLANNED_SIGN = "planned_signed";
    public static final String COLUMN_PLANNED_SIGN_ID = "_id";
    public static final String COLUMN_PLANNED_SIGN_INSPECTOR = "inspector";
    public static final String COLUMN_PLANNED_SIGN_AREA_OWNER = "area_owner";
    public static final String COLUMN_PLANNED_SIGN_LOCATION = "location";
    public static final String COLUMN_PLANNED_SIGN_DATE_INS = "date_ins";
    public static final String COLUMN_PLANNED_NAME = "planned_name";
    public static final String COLUMN_PLANNED_SIGN_IDNO = "planned_id";
    public static final String COLUMN_PLANNED_SIGN_DATE = "planned_date";
    public static final String COLUMN_PLANNED_SIGN = "planned_sign";
    public static final String COLUMN_PLANNED_TITLE = "planned_title";
    public static final String COLUMN_PLANNED_CODE = "code";

    public static final String DATABASE_PLANNED_SIGN = "create table " + TABLE_PLANNED_SIGN + "("
            + COLUMN_PLANNED_SIGN_ID + " integer primary key autoincrement , "
            + COLUMN_PLANNED_SIGN_INSPECTOR + " text , "
            + COLUMN_PLANNED_SIGN_AREA_OWNER + " text , "
            + COLUMN_PLANNED_SIGN_LOCATION + " text , "
            + COLUMN_PLANNED_SIGN_DATE_INS + " text , "
            + COLUMN_PLANNED_NAME + " text , "
            + COLUMN_PLANNED_SIGN_IDNO + " text , "
            + COLUMN_PLANNED_SIGN_DATE + " text , "
            + COLUMN_PLANNED_SIGN + " text , "
            + COLUMN_PLANNED_TITLE + " text , "
            + COLUMN_PLANNED_CODE + " text " + ");";

    /* ==========================
         Planned SHE Inspection FINDING
  ========================== */
    public static final String TABLE_PLANNED_FINDING = "planned_finding";
    public static final String COLUMN_PLANNED_FINDING_ID = "_id";
    static final String COLUMN_PLANNED_FINDING_MAIN_ID = "main_id";
    public static final String COLUMN_PLANNED_FINDING_FINDINGS = "findings";
    public static final String COLUMN_PLANNED_FINDING_COMMENTS = "comments";
    public static final String COLUMN_PLANNED_FINDING_RISKS = "risks";
    public static final String COLUMN_PLANNED_FINDING_RESPONSIBLE = "responsible";
    public static final String COLUMN_PLANNED_FINDING_DATE = "date_complete";
    public static final String COLUMN_PLANNED_FINDING_PHOTO = "photo_path";
    public static final String COLUMN_PLANNED_FINDING_CODE = "category_code";
    public static final String COLUMN_PLANNED_FINDING_QUESTION_CODE = "question_code";
    public static final String COLUMN_PLANNED_FINDING_INSPECTION_CODE = "inspection_code";

    public static final String DATABASE_PLANNED_FINDINGS = "create table " + TABLE_PLANNED_FINDING + "("
            + COLUMN_PLANNED_FINDING_ID + " integer primary key autoincrement , "
            + COLUMN_PLANNED_FINDING_MAIN_ID + " integer, "
            + COLUMN_PLANNED_FINDING_FINDINGS + " text , "
            + COLUMN_PLANNED_FINDING_COMMENTS + " text , "
            + COLUMN_PLANNED_FINDING_RISKS + " text , "
            + COLUMN_PLANNED_FINDING_RESPONSIBLE + " text , "
            + COLUMN_PLANNED_FINDING_DATE + " text , "
            + COLUMN_PLANNED_FINDING_PHOTO + " text , "
            + COLUMN_PLANNED_FINDING_CODE + " text , "
            + COLUMN_PLANNED_FINDING_QUESTION_CODE + " text , "
            + COLUMN_PLANNED_FINDING_INSPECTION_CODE + " text " + ");";

    /* ==========================
      Planned SHE Inspection Inspection
========================== */
    public static final String TABLE_PLANNED_INSPECTION = "planned_inspection";
    public static final String PLANNED_INSPECTION_ID = "_id";
    public static final String PLANNED_INSPECTION_INSPECTOR = "inspector";
    public static final String PLANNED_INSPECTION_AREA_OWNER = "area_owner";
    public static final String PLANNED_INSPECTION_LOCATION = "location";
    public static final String PLANNED_INSPECTION_DATE = "date";
    public static final String PLANNED_INSPECTION_RISK1 = "risk1";
    public static final String PLANNED_INSPECTION_RATING1 = "rating1";
    public static final String PLANNED_INSPECTION_RISK2 = "risk2";
    public static final String PLANNED_INSPECTION_RATING2 = "rating2";
    public static final String PLANNED_INSPECTION_RISK3 = "risk3";
    public static final String PLANNED_INSPECTION_RATING3 = "rating3";
    public static final String PLANNED_INSPECTION_RISK4 = "risk4";
    public static final String PLANNED_INSPECTION_RATING4 = "rating4";
    public static final String PLANNED_INSPECTION_RISK5 = "risk5";
    public static final String PLANNED_INSPECTION_RATING5 = "rating5";
    public static final String PLANNED_INSPECTION_RISK6 = "risk6";
    public static final String PLANNED_INSPECTION_RATING6 = "rating6";
    public static final String PLANNED_INSPECTION_RISK7 = "risk7";
    public static final String PLANNED_INSPECTION_RATING7 = "rating7";
    public static final String PLANNED_INSPECTION_RISK8 = "risk8";
    public static final String PLANNED_INSPECTION_RATING8 = "rating8";
    public static final String PLANNED_INSPECTION_CODE = "code";
    static final String PLANNED_INSPECTION_MAIN_ID = "main_id";

    public static final String DATABASE_PLANNED_INSPECTION = "create table " + TABLE_PLANNED_INSPECTION + "("
            + PLANNED_INSPECTION_ID + " integer primary key autoincrement , "
            + PLANNED_INSPECTION_MAIN_ID + " integer, "
            + PLANNED_INSPECTION_INSPECTOR + " text , "
            + PLANNED_INSPECTION_AREA_OWNER + " text , "
            + PLANNED_INSPECTION_LOCATION + " text , "
            + PLANNED_INSPECTION_DATE + " text , "
            + PLANNED_INSPECTION_RISK1 + " text , "
            + PLANNED_INSPECTION_RATING1 + " text , "
            + PLANNED_INSPECTION_RISK2 + " text , "
            + PLANNED_INSPECTION_RATING2 + " text , "
            + PLANNED_INSPECTION_RISK3 + " text , "
            + PLANNED_INSPECTION_RATING3 + " text , "
            + PLANNED_INSPECTION_RISK4 + " text , "
            + PLANNED_INSPECTION_RATING4 + " text , "
            + PLANNED_INSPECTION_RISK5 + " text , "
            + PLANNED_INSPECTION_RATING5 + " text , "
            + PLANNED_INSPECTION_RISK6 + " text , "
            + PLANNED_INSPECTION_RATING6 + " text , "
            + PLANNED_INSPECTION_RISK7 + " text , "
            + PLANNED_INSPECTION_RATING7 + " text , "
            + PLANNED_INSPECTION_RISK8 + " text , "
            + PLANNED_INSPECTION_RATING8 + " text , "
            + PLANNED_INSPECTION_CODE + " text " + ");";


    public DataHelper(Context context) {
        super(context, Constants.DB_FOLDER_ON_EXTERNAL_PATH + "/"
                + DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_OBSERVER_CREATE);
        database.execSQL(DATABASE_COMPANY_CREATE);
        database.execSQL(DATABASE_BUSINESS_UNIT_CREATE);
        database.execSQL(DATABASE_DEPARTMENT_CREATE);
        database.execSQL(DATABASE_SECTION_CREATE);
//        database.execSQL(DATABASE_POTENTIAL_RISK_CREATE);
        database.execSQL(DATABASE_RATING_CREATE);
        database.execSQL(DATABASE_AVAILABILITY_CREATE);
        database.execSQL(DATABASE_GRS_DRILLING);
        database.execSQL(DATABASE_GRS_DRILLING_DETAIL);
        database.execSQL(DATABASE_PHOTO_CREATE);
        database.execSQL(DATABASE_LOOKUP_CREATE);
        database.execSQL(DATABASE_FIELD_INSPECTION);
        database.execSQL(DATABASE_FIELD_INSPECTION_FINDING);
        database.execSQL(DATABASE_UG_DRILLING_MAIN);
        database.execSQL(DATABASE_UG_DAILY_PRECHECK);
        database.execSQL(DATABASE_UG_ELECTRICAL);
        database.execSQL(DATABASE_UG_SAFETY_EQUIP);
        database.execSQL(DATABASE_UG_SIGNS);
        database.execSQL(DATABASE_UG_CLEANLINESS);
        database.execSQL(DATABASE_UG_FEED_FRAME);
        database.execSQL(DATABASE_UG_CONTROL);
        database.execSQL(DATABASE_UG_WIREEQUIPMENT);
        database.execSQL(DATABASE_UG_CIRCULATION_SYSTEM);
        database.execSQL(DATABASE_UG_POWER_PACK);
        database.execSQL(DATABASE_UG_HANDTOOLS);
        database.execSQL(DATABASE_UG_WORK_PROCEDURE);
        database.execSQL(DATABASE_LV_INSPECTION);
        database.execSQL(DATABASE_LV_GROUP);
        database.execSQL(DATABASE_LV_DAILY);
        database.execSQL(DATABASE_LV_WEEKLY);
        database.execSQL(DATABASE_LV_DAMAGE);
        database.execSQL(DATABASE_DRILLING_SIGN);
        database.execSQL(DATABASE_PLANNED_MAIN);
        database.execSQL(DATABASE_PLANNED_SIGN);
        database.execSQL(DATABASE_PLANNED_FINDINGS);
        database.execSQL(DATABASE_PLANNED_INSPECTION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OBSERVER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUSINESS_UNIT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEPARTMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SECTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POTENTIAL_RISK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GRS_DRILL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GRS_DRILL_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOOKUP);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POTENTIAL_RISK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AVAILABILITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RATING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FIELD_INSPECTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FIELD_INSPECTION_FINDING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_DRILLING_MAIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_DAILY_PRECHECK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_ELECTRICAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_SAFETY_EQUIP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_SIGNS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_CLEANLINESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_FEED_FRAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_CONTROL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_WIREEQUIPMENT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_CIRCULATION_SYSTEM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_POWER_PACK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_HANDTOOLS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_WORK_PROCEDURE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LV_INSPECTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LV_GROUP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LV_DAILY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LV_WEEKLY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LV_DAMAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UG_DRILLING_SIGN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANNED_MAIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANNED_SIGN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANNED_FINDING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLANNED_INSPECTION);
        onCreate(db);
    }
}
