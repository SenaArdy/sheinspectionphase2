package com.ptfi.sheinspectionphase2.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ptfi.sheinspectionphase2.Utils.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssetDatabaseOpenHelper {

	private static String TAG = "DataBaseHelper";
	private static final String DB_NAME = "manpower.db";
	private static String DB_PATH = "";

	private Context context;

	public AssetDatabaseOpenHelper(Context context) {
		if (android.os.Build.VERSION.SDK_INT >= 17) {
//			DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
			DB_PATH = Constants.extStorageDirectory + "/" + Constants.ROOT_FOLDER_NAME
					+ "/" + Constants.APP_FOLDER_NAME + "/" + Constants.DB_FOLDER_NAME + "/";
		} else {
			DB_PATH = Constants.extStorageDirectory + "/" + Constants.ROOT_FOLDER_NAME
					+ "/" + Constants.APP_FOLDER_NAME + "/" + Constants.DB_FOLDER_NAME + "/";
		}
		this.context = context;
	}

	// Open the database, so we can query it
	public SQLiteDatabase openDataBase() throws SQLException {
		String mPath = DB_PATH + DB_NAME;
		// Log.v("mPath", mPath);
		SQLiteDatabase mDataBase = SQLiteDatabase.openDatabase(mPath, null,
				SQLiteDatabase.CREATE_IF_NECESSARY);

		return mDataBase;
	}

	public void createDataBase() throws IOException {
		// If database not exists copy it from the assets
		boolean mDataBaseExist = checkDataBase();
		if (!mDataBaseExist) {
			try {
				// Copy the database from assests
				copyDataBase();
				Log.e(TAG, "createDatabase database created");
			} catch (IOException mIOException) {
				throw new Error("ErrorCopyingDataBase");
			}
		}
	}

	// Check that the database exists here: /data/data/your package/databases/Da
	// Name
	private boolean checkDataBase() {
		File dbFile = new File(DB_PATH + DB_NAME);
		// Log.v("dbFile", dbFile + "   "+ dbFile.exists());
		return dbFile.exists();
	}

	// Copy the database from assets
	private void copyDataBase() throws IOException {
		InputStream mInput = context.getAssets().open(DB_NAME);
		String outFileName = DB_PATH + DB_NAME;
		OutputStream mOutput = new FileOutputStream(outFileName);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}
}