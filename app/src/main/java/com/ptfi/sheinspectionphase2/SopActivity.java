package com.ptfi.sheinspectionphase2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ptfi.sheinspectionphase2.Utils.Constants;

import java.io.File;

/**
 * Created by senaardyputra on 11/11/16.
 */

public class SopActivity extends AppCompatActivity {

    private ImageView sopPdfIV;
    private int currentPage = 0;
    private Button previousBtn, nextBtn;

    File file;

    String language;

    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sop_activity);

        Bundle args = new Bundle();
        language = args.getString("language");

        sopPdfIV = (ImageView) findViewById(R.id.sopPdfIV);

        previousBtn = (Button) findViewById(R.id.previousBtn);
        nextBtn = (Button) findViewById(R.id.nextBtn);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPage++;
                renderPdf(language);
            }
        });

        previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPage--;
                renderPdf(language);
            }
        });

        renderPdf(language);
    }

    private void renderPdf(final String language) {
        try {
//            LinearLayout beginLL = (LinearLayout) findViewById(R.id.beginLL);
//            ViewTreeObserver vto = beginLL.getViewTreeObserver();
//
//            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    REQ_WIDTH = sopPdfIV.getWidth();
//                    REQ_HEIGHT = sopPdfIV.getHeight();
//                }
//            });

            bitmap = Bitmap.createBitmap(599, 900, Bitmap.Config.ARGB_4444);

            if (language.equals("Indonesia")) {
                file = new File(Constants.basepath + "/" + Constants.APP_FOLDER_NAME + "/" + Constants.DOCUMENTS_FOLDER_NAME + "/" +
                        Constants.SOP + "/" + Constants.SOP_ID_PDF);
            } else if (language.equals("English")) {
                file = new File(Constants.basepath + "/" + Constants.APP_FOLDER_NAME + "/" + Constants.DOCUMENTS_FOLDER_NAME + "/" +
                        Constants.SOP + "/" + Constants.SOP_EN_PDF);
            }
            final PdfRenderer pdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY));

            if (currentPage < 0) {
                currentPage = 0;
            } else if (currentPage > pdfRenderer.getPageCount()) {
                currentPage = pdfRenderer.getPageCount() - 1;
            }

            Matrix m = sopPdfIV.getImageMatrix();
            Rect rect = new Rect(0, 0, 599, 900);
            pdfRenderer.openPage(currentPage).render(bitmap, rect, m, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            sopPdfIV.setImageMatrix(m);
            sopPdfIV.setImageBitmap(bitmap);
            sopPdfIV.invalidate();

            pdfRenderer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SopActivity.this, MainMenu.class);
        SopActivity.this.finish();
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

}
